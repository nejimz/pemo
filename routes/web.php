<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'DashboardController@login')->name('login');
Route::post('authenticate', 'DashboardController@authenticate')->name('authenticate');

Route::group(['prefix' => 'portal', 'middleware' => 'auth'], function(){
	Route::get('authenticate', 'DashboardController@logout')->name('logout');

	Route::get('/', 'DashboardController@dashboard')->name('dashboard');

	Route::group(['prefix' => 'city'], function(){
		Route::get('/', 'CityController@index')->name('city');
		Route::get('/create', 'CityController@create')->name('city_create');
		Route::post('/store', 'CityController@store')->name('city_store');
		Route::get('/edit/{id}', 'CityController@edit')->name('city_edit');
		Route::post('/update/{id}', 'CityController@update')->name('city_update');

		Route::get('summary', 'CityController@summary')->name('city_summary');

		Route::group(['prefix' => 'area'], function(){
			Route::get('/', 'AreaController@index')->name('area');
			Route::get('/create', 'AreaController@create')->name('area_create');
			Route::post('/store', 'AreaController@store')->name('area_store');
			Route::get('/edit/{id}', 'AreaController@edit')->name('area_edit');
			Route::post('/update/{id}', 'AreaController@update')->name('area_update');

			Route::get('/xhr', 'AreaController@xhr')->name('area_xhr');
		});
	});

	Route::group(['prefix' => 'mangrove'], function(){
		Route::get('/', 'MangroveController@index')->name('mangrove');
		Route::get('/create', 'MangroveController@create')->name('mangrove_create');
		Route::post('/store', 'MangroveController@store')->name('mangrove_store');
		Route::get('/edit/{id}', 'MangroveController@edit')->name('mangrove_edit');
		Route::post('/update/{id}', 'MangroveController@update')->name('mangrove_update');
	});

	Route::group(['prefix' => 'inventory'], function(){
		Route::get('/', 'InventoryController@index')->name('inventory');
		Route::get('/create', 'InventoryController@create')->name('inventory_create');
		Route::post('/store', 'InventoryController@store')->name('inventory_store');
		Route::get('/edit/{id}', 'InventoryController@edit')->name('inventory_edit');
		Route::post('/update/{id}', 'InventoryController@update')->name('inventory_update');
	});

	Route::group(['prefix' => 'reports'], function(){
		Route::get('/', 'InventoryController@index')->name('reports');

		Route::group(['prefix' => 'inventory'], function(){
			Route::get('/', 'ReportsInventoryController@index')->name('report_inventory');
			Route::get('/generate-inventory', 'ReportsInventoryController@generate_inventory')->name('report_inventory_generate');

			Route::get('/summary', 'ReportsInventoryController@summary_index')->name('report_summary');
			Route::get('/generate-summary', 'ReportsInventoryController@generate_inventory_summary')->name('report_summary_generate');

			Route::get('/inventory-count', 'ReportsInventoryController@inventory_count')->name('report_inventory_summary');
			Route::get('/generate-inventory-count', 'ReportsInventoryController@generate_inventory_count')->name('report_inventory_count_generate');
		});
	});

	Route::group(['prefix' => 'user'], function(){
		Route::get('/', 'UserController@index')->name('user');
		Route::get('/create', 'UserController@create')->name('user_create');
		Route::post('/store', 'UserController@store')->name('user_store');
		Route::get('/edit/{id}', 'UserController@edit')->name('user_edit');
		Route::post('/update/{id}', 'UserController@update')->name('user_update');
		Route::get('/reset-password', 'UserController@reset_password')->name('user_reset_password');
		Route::post('/reset-password-submit', 'UserController@reset_password_submit')->name('user_reset_password_submit');

		Route::get('/generate-passwrod/{id}', 'UserController@generate_password')->name('generate_password');
	});

});
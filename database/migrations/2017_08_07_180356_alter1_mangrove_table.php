<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter1MangroveTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('mangrove'))
        {
            Schema::table('mangrove', function (Blueprint $table) {
                $table->text('details');
            });
        }
    }

    public function down()
    {
    }
}

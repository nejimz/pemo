<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration
{
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('mangrove_id')->unsigned();

            $table->date('record_date');
            $table->integer('number_of_segment');
            $table->integer('number_of_tree');
            $table->double('diameter');
            $table->double('height');
            $table->double('crown');
            $table->double('seedling');
            $table->double('sapling');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('city');
            $table->foreign('area_id')->references('id')->on('area');
            $table->foreign('mangrove_id')->references('id')->on('mangrove');

            $table->index( [ 'id', 'city_id', 'area_id', 'mangrove_id' ] );
        });
    }

    public function down()
    {
        Schema::drop('inventory');
    }
}

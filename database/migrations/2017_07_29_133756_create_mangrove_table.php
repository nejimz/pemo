<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMangroveTable extends Migration
{
    public function up()
    {
        Schema::create('mangrove', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('scientific_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('mangrove');
    }
}

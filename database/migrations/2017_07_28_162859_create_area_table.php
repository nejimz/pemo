<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    public function up()
    {
        Schema::create('area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('city');
            $table->index( [ 'id', 'city_id' ] );
        });
    }

    public function down()
    {
        Schema::drop('area');
    }
}

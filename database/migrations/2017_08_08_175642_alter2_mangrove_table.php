<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2MangroveTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('mangrove'))
        {
            Schema::table('mangrove', function (Blueprint $table) {
                $table->text('image');
            });
        }
    }

    public function down()
    {
    }
}

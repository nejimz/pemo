@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-user"></i> User</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-user"></i><a href="{{ route('user') }}">User</a></li>
        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <form method="get" action="" class="form-inline">
          <div class="form-group">
            <input type="text" class="form-control" name="search" value="{{ $search }}" autofocus="" placeholder="Search User...">
          </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
        </form>
        <br>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="5%" class="text-center">
                <a href="{{ route('user_create') }}" title="Add a City"><i class="fa fa-plus"></i></a>
              </th>
              <th width="20%">Username</th>
              <th width="35%">Name</th>
              <th width="20%">Created At</th>
              <th width="20%">Updated At</th>
            </tr>
          </thead>
          <tbody>
            @foreach($rows as $row)
            <tr>
              <td>
                <a href="{{ route('user_edit', $row->id) }}" title="Edit"><i class="fa fa-edit"></i></a> 
              </td>
              <td>{{ $row->username }}</td>
              <td>{{ $row->name }}</td>
              <td>{{ $row->created_at }}</td>
              <td>{{ $row->updated_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $rows->appends(['search'=>$search])->links() }}
      </div>
    </div>

  <script type="text/javascript">
    

  </script>
@stop
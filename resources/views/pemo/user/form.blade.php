@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-users"></i> User</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-users"></i><a href="{{ route('user') }}">User</a></li>
          <li><i class="fa fa-file"></i>Form</li>
        </ol>
      </div>
    </div>
    @include('pemo.layouts.messages')
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Add User</header>
          <div class="panel-body">

            <form action="{{ $action }}" method="post" class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="username">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="username" value="{{ $username }}" autofocus="">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="name" value="{{ $name }}">
                </div>
              </div>
              @if(isset($id))
              <div class="form-group">
                <div class="col-sm-2 columns">&nbsp;</div>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="password" name="password" readonly="">
                </div>
                <div class="col-sm-2 columns">
                  <a id="generatePassword" href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
              <input type="hidden" id="generatePasswordUrl" value="{{ route('generate_password', $id) }}">
              @endif
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  {!! csrf_field() !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </form>

          </div>
        </section>
      </div>
    </div>
  <script type="text/javascript">
    $(function(){
      var xhr_generatePassword = null;
      $('#generatePassword').click(function(){
          xhr_generatePassword = $.ajax({
              type : 'get',
              url : $("#generatePasswordUrl").val(),
              cache : false,
              dataType : "json",
              beforeSend: function(xhr){
                  if (xhr_generatePassword != null)
                  {
                      xhr_generatePassword.abort();
                  }
              }
          }).done(function(data){
            $('#password').val(data['password']);
          }).fail(function(jqXHR, textStatus){
            //console.log('Request failed: ' + textStatus);
          });
      });
    });
  </script>
@stop
@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-file-picture-o"></i> Assessment</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-file-picture-o"></i><a href="{{ route('inventory') }}">Assessment</a></li>
        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <form method="get" action="" class="form-inline">
          <div class="form-group">
            <input type="text" class="form-control" name="search" value="{{ $search }}" autofocus="" placeholder="Search City, Area, Mangrove">
          </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
        </form>
        <br>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th width="5%" class="text-center">
                <a href="{{ route('inventory_create') }}" title="Add Inventory"><i class="fa fa-plus"></i></a>
              </th>
              <th width="20%">City</th>
              <th width="20%">Sitio</th>
              <th width="16%">Mangrove Name</th>
              <th width="6%" class="text-center"><abbr title="Number of Segment">N.o.S.</abbr></th>
              <th width="6%" class="text-center"><abbr title="Number of Tree">N.o.T.</abbr></th>
              <th width="5%" class="text-center"><abbr title="Diameter">Dia.</abbr></th>
              <th width="5%" class="text-center"><abbr title="Height">Hgt.</abbr></th>
              <th width="7%" class="text-center">Crown</th>
              <th width="15%">Record Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($rows as $row)
            <tr>
              <td class="text-center">
                <a href="{{ route('inventory_edit', $row->id) }}" title="Edit"><i class="fa fa-edit"></i></a> 
              </td>
              <td>{{ $row->city->name }}</td>
              <td>{{ $row->area->name }}</td>
              <td>{{ $row->mangrove->name }} ({{ $row->mangrove->scientific_name }})</td>
              <td class="text-center">{{ $row->number_of_segment }}</td>
              <td class="text-center">{{ $row->number_of_tree }}</td>
              <td class="text-center">{{ $row->diameter }}</td>
              <td class="text-center">{{ $row->height }}</td>
              <td class="text-center">{{ $row->crown }}</td>
              <td>{{ $row->record_date }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $rows->appends(['search'=>$search])->links() }}
      </div>
    </div>

  <script type="text/javascript">
    

  </script>
@stop
@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-file-picture-o"></i> Assessment</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-file-picture-o"></i><a href="{{ route('inventory') }}">Assessment</a></li>
          <li><i class="fa fa-file"></i>Form</li>
        </ol>
      </div>
    </div>
    @include('pemo.layouts.messages')
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Add Inventory</header>
          <div class="panel-body">

            <form action="{{ $action }}" method="post" class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="mangrove_id">Mangrove</label>
                <div class="col-sm-3">
                  <select class="form-control" name="mangrove_id" id="mangrove_id">
                  <option value="">-- Select --</option>
                  @foreach($mangroves as $mangrove)
                  <option value="{{ $mangrove->id }}">{{ $mangrove->name }}</option>
                  @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">City</label>
                <div class="col-sm-3">
                  <select class="form-control" name="city_id" id="city_id">
                  <option value="">-- Select --</option>
                  @foreach($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="area_id">Area</label>
                <div class="col-sm-3">
                  <select class="form-control" name="area_id" id="area_id">
                  <option value="">-- Select --</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="record_date">Record Date</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control idatepicker" name="record_date" id="record_date" value="{{ $record_date }}" readonly>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="number_of_segment">Number of Segment</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="number_of_segment" id="number_of_segment" value="{{ $number_of_segment }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="number_of_tree">Number of Tree</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="number_of_tree" id="number_of_tree" value="{{ $number_of_tree }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="diameter">Diameter</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="diameter" id="diameter" value="{{ $diameter }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="height">Height</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="height" id="height" value="{{ $height }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="crown">Crown</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="crown" id="crown" value="{{ $crown }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="seedling">Seedling</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="seedling" id="seedling" value="{{ $seedling }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="sapling">Sapling</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" name="sapling" id="sapling" value="{{ $sapling }}">
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  {!! csrf_field() !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </form>

          </div>
        </section>
      </div>
    </div>
    <input type="hidden" id="area_xhr" value="{{ route('area_xhr') }}">
  <script type="text/javascript">
    var xhr_area_list = null;
    $('#city_id').val('<?php echo $city_id; ?>');
    $('#area_id').val('<?php echo $area_id; ?>');
    $('#mangrove_id').val('<?php echo $mangrove_id; ?>');
    <?php
    if($city_id != '')
    {
      echo 'area_list(' . $city_id . ', ' . $area_id . ');';
    }
    ?>

    $(function(){
      $('#city_id').change(function(){
        area_list($(this).val(), <?php $area_id; ?>);
      });
    });
  </script>
@stop
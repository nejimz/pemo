@extends('pemo.layouts.print')

@section('container')
@parent

<?php

$grand_total_segment = 0;
$grand_total_tree = 0;
$grand_total_basal_area = 0;
$grand_total_frequency = 0;
$grand_total_relative_frequency = 0;
$grand_total_relative_density = 0;
$grand_total_relative_dominance = 0;
$grand_total_importance_value = 0;

$grand_total_canopy_cover = 0;
$grand_total_canopy_cover_percentage = 0;

$grand_total_abundance = 0;
$grand_basal_area = 0;

$i = 0;
$data = [];
$data_total = [];

foreach($rows as $row)
{
	$total_segment = $row->total_segment($city_id, $area_id, $date_from, $date_to);
	$total_tree = $row->total_tree($city_id, $area_id, $date_from, $date_to);
	$total_basal_area = $row->total_basal_area($city_id, $area_id, $date_from, $date_to);
	$frequency = $row->frequency($total_segment);
	$abundance = $row->abundance($total_tree);
	$basal_area = $row->basal_area($total_basal_area);
	// Start Grand Total
	$grand_total_segment += $total_segment;
	$grand_total_tree += $total_tree;
	$grand_total_basal_area += $total_basal_area;
	$grand_total_frequency += $frequency;
	// End Grand Total
	$canopy_cover = $row->total_canopy_cover($city_id, $area_id, $date_from, $date_to);

	$grand_total_canopy_cover += $canopy_cover;

	$grand_total_abundance += $abundance;
	$grand_basal_area += $basal_area;

	$data[$i] = [
		'name'=>$row->name,
		'scientific_name'=>$row->scientific_name,
		'total_segment'=>$total_segment,
		'total_tree'=>$total_tree,
		'total_basal_area'=>$total_basal_area,
		'frequency'=>$frequency,

		'relative_frequency'=>0,
		'relative_density'=>0,
		'relative_dominance'=>0,
		'importance_value'=>0,
		
		'canopy_cover'=>$canopy_cover,
		'canopy_cover_percentage'=>0,

		'abundance'=>$abundance,
		'basal_area'=>$basal_area,
	];
	$i++;
}

$data['total'] = [
	'grand_total_segment'=>$grand_total_segment,
	'grand_total_tree'=>$grand_total_tree,
	'grand_total_basal_area'=>$grand_total_basal_area,
	'grand_total_frequency'=>$grand_total_frequency,
	'grand_total_relative_frequency'=>$grand_total_relative_frequency,
	'grand_total_relative_density'=>$grand_total_relative_density,
	'grand_total_relative_dominance'=>$grand_total_relative_dominance,
	'grand_total_importance_value'=>$grand_total_importance_value,
	'grand_total_canopy_cover'=>$grand_total_canopy_cover,
	'grand_total_canopy_cover_percentage'=>$grand_total_canopy_cover_percentage,
	'grand_total_abundance'=>$grand_total_abundance,
	'grand_basal_area'=>$grand_basal_area,
];

// Relative Frequency and Grand Total
for($counter=0; $counter < count($data)-1; $counter++)
{
	$relative_frequency = ($data[$counter]['frequency'] / $data['total']['grand_total_frequency']) * 100;
	$data[$counter]['relative_frequency'] = $relative_frequency;
	$grand_total_relative_frequency += $relative_frequency;
}
$data['total']['grand_total_relative_frequency'] = $grand_total_relative_frequency;
// Relative Density and Grand Total
for($counter=0; $counter < count($data)-1; $counter++)
{
	$relative_density = ($data[$counter]['total_tree'] / $data['total']['grand_total_tree']) * 100;
	$data[$counter]['relative_density'] = $relative_density;
	$grand_total_relative_density += $relative_density;
}
$data['total']['grand_total_relative_density'] = $grand_total_relative_density;
// Relative Dominance and Grand Total
for($counter=0; $counter < count($data)-1; $counter++)
{
	$relative_dominance = ($data[$counter]['total_basal_area'] / $data['total']['grand_total_basal_area']) * 100;
	$relative_dominance = $relative_dominance;
	$data[$counter]['relative_dominance'] = $relative_dominance;
	$grand_total_relative_dominance += $relative_dominance;
}
$data['total']['grand_total_relative_dominance'] = $grand_total_relative_dominance;
#dump($data);

?>
	<style type="text/css">
		@media print {
			#print_div {
				display: none;
			}
		}
	</style>
	<div id="print_div" class="row">
		<div class="col-sm-12">
			<button class="button" onclick="window.print()">Print</button>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Location:</div>
		<div class="col-sm-10">{{ $area->name . ', ' . $area->city->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Recorder:</div>
		<div class="col-sm-10">{{ Auth::user()->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Date:</div>
		<div class="col-sm-10">{{ date('F d, Y', strtotime($date_from)) . ' - ' . date('F d, Y', strtotime($date_to)) }}</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered" width="100%">
				<thead>
					<tr>
						<th colspan="2" rowspan="1">Species</th>
						<th rowspan="1">No. of Segments</th>
						<th rowspan="1">No. of Trees</th>
						<th class="text-center" rowspan="1"><abbr title="Total Basal Area">T.B.A.</abbr></th>

						<th class="text-center" rowspan="1"><abbr title="Frequency">F.</abbr></th>
						<th class="text-center" rowspan="1"><abbr title="Relative Frequency">R.F.</abbr></th>
						<th class="text-center" rowspan="1"><abbr title="Relative Density">R.De.</abbr></th>
						<th class="text-center" rowspan="1"><abbr title="Relative Dominance">R.Do.</abbr></th>
						<th class="text-center" rowspan="1"><abbr title="Importance Value">I.V.</abbr></th>
						<th class="text-center" rowspan="1" colspan="2">Canopy Cover</th>
						<th class="text-center" rowspan="1">Abundance</th>
						<th class="text-center" rowspan="1">Basal Area</th>
						<!--th class="text-center" colspan="6">Regeneration</th-->
					</tr>
					<!--tr>
						<th class="text-center" colspan="3">Seedlings</th>
						<th class="text-center" colspan="3">Sapling</th>
					</tr-->
					<tr>
						<th class="text-center">Common Name</th>
						<th class="text-center">Scientific Name</th>
						<th class="text-center"></th>
						<th class="text-center"></th>
						<th class="text-center"></th>

						<th class="text-center">%</th>
						<th class="text-center">%</th>
						<th class="text-center">%</th>
						<th class="text-center">%</th>
						<th class="text-center"></th>

						<th class="text-center">Total</th>
						<th class="text-center">%</th>
						<th class="text-center">trees/ha</th>
						<th class="text-center">m2 / ha</th>
						
						<!--th class="text-center">count</th>
						<th class="text-center">%</th>
						<th class="text-center">seedlings / ha</th>
						<th class="text-center">count</th>
						<th class="text-center">%</th>
						<th class="text-center">sapling/ha</th-->
					</tr>
				</thead>
				<tbody>
				@for($counter=0; $counter < count($data)-1; $counter++)
				<?php

				$importance_value = $data[$counter]['relative_frequency'] +  $data[$counter]['relative_density'] + $data[$counter]['relative_dominance'];
				$grand_total_importance_value += $importance_value;

				$canopy_cover_percentage = $data[$counter]['canopy_cover'] / $data['total']['grand_total_canopy_cover'];
				$grand_total_canopy_cover_percentage += $canopy_cover_percentage;
				?>
				<tr>
					<td>{{ $data[$counter]['name'] }}</td>
					<td>{{ $data[$counter]['scientific_name'] }}</td>
					<td>{{ $data[$counter]['total_segment'] }}</td>
					<td>{{ $data[$counter]['total_tree'] }}</td>
					<td>{{ round($data[$counter]['total_basal_area'], 7) }}</td>
					<td>{{ $data[$counter]['frequency'] }}</td>

					<td>{{ $data[$counter]['relative_frequency'] }}</td>
					<td>{{ $data[$counter]['relative_density'] }}</td>
					<td>{{ round($data[$counter]['relative_dominance'], 2) }}</td>
					<td>{{ round($importance_value, 2) }}</td>
					<td>{{ $data[$counter]['canopy_cover'] }}</td>

					<td>{{ round($canopy_cover_percentage, 2) }}</td>
					<td>{{ $data[$counter]['abundance'] }}</td>
					<td>{{ round($data[$counter]['basal_area'], 6) }}</td>
					
					<!--td></td>
					<td></td>
					<td></td>
					
					<td></td>
					<td></td>
					<td></td-->
				</tr>
				@endfor
				<tr>

					<th></th>
					<th></th>
					<th>{{ $data['total']['grand_total_segment'] }}</th>
					<th>{{ $data['total']['grand_total_tree'] }}</th>
					<th>{{ round($data['total']['grand_total_basal_area'], 7) }}</th>

					<th>{{ $data['total']['grand_total_frequency'] }}</th>
					<th>{{ $data['total']['grand_total_relative_frequency'] }}</th>
					<th>{{ $data['total']['grand_total_relative_density'] }}</th>
					<th>{{ $data['total']['grand_total_relative_dominance'] }}</th>
					<th>{{ $grand_total_importance_value }}</th>

					<th>{{ $data['total']['grand_total_canopy_cover'] }}</th>
					<th>{{ $grand_total_canopy_cover_percentage }}</th>
					<th>{{ $grand_total_abundance }}</th>
					<th>{{ round($data['total']['grand_basal_area'], 6) }}</th>
					
					<!--th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th-->
				</tr>
				</tbody>
			</table>
		</div>
	</div>
@stop
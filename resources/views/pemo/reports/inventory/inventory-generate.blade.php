@extends('pemo.layouts.print')

@section('container')
@parent
	<style type="text/css">
		@media print {
			#print_div {
				display: none;
			}
		}
	</style>
	<div id="print_div" class="row">
		<div class="col-sm-12">
			<button class="button" onclick="window.print()">Print</button>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Location:</div>
		<div class="col-sm-10">{{ $area->name . ', ' . $area->city->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Beneficiary:</div>
		<div class="col-sm-10">{{ $beneficiary }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Recorder:</div>
		<div class="col-sm-10">{{ Auth::user()->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Date:</div>
		<div class="col-sm-10">{{ date('F d, Y', strtotime($date_from)) . ' - ' . date('F d, Y', strtotime($date_to)) }}</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered" width="100%">
				<thead>
					<tr>
						<th class="text-center">Segment</th>
						<th class="text-center">Tree</th>
						<th rowspan="2">Species</th>
						<th rowspan="2">Scientific Name</th>
						<th class="text-center">DIA</th>
						<th class="text-center">DIA</th>
						<th class="text-center">Basal Area</th>
						<th class="text-center">Total</th>
						<th class="text-center">Crown</th>
						<th class="text-center">Crown Cover</th>
						<th class="text-center" colspan="2">Regeneration</th>
						<!--th>Saplin</th-->
					</tr>
					<tr>
						<th class="text-center">No.</th>
						<th class="text-center">No.</th>
						<!--th>Species</th-->
						<!--th>Scientific Name</th-->
						<th class="text-center"><abbr title="Centimeter">cm</abbr></th>
						<th class="text-center"><abbr title="Meter">m</abbr></th>
						<th class="text-center">m2</th>
						<th class="text-center">Height (m)</th>
						<th class="text-center">DIA (m)</th>
						<th class="text-center">m2</th>
						<th class="text-center">Seedlings</th>
						<th class="text-center">Saplin</th>
					</tr>
				</thead>
				<tbody>
				@foreach($rows as $row)
				<tr>
					<td class="text-center">{{ $row->number_of_segment }}</td>
					<td class="text-center">{{ $row->number_of_tree }}</td>
					<td>{{ $row->mangrove->name }}</td>
					<td>{{ $row->mangrove->scientific_name }}</td>
					<td class="text-center">{{ $row->diameter }}</td>
					<td class="text-center">{{ $row->meter }}</td>
					<td class="text-center">{{ $row->basal_area }}</td>
					<td class="text-center">{{ $row->height }}</td>
					<td class="text-center">{{ $row->crown }}</td>
					<td class="text-center">{{ $row->crown_cover }}</td>
					<td class="text-center">{{ $row->seedling }}</td>
					<td class="text-center">{{ $row->sapling }}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop
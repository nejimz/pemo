@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-text-file-o"></i> Reports</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-tree"></i><a href="{{ route('mangrove') }}">Reports</a></li>
          <li><i class="fa fa-dropbox"></i>Summary</li>
        </ol>
      </div>
    </div>
    @include('pemo.layouts.messages')
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Generate Inventory</header>
          <div class="panel-body">

            <form action="{{ $action }}" method="get" target="_blank" class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">City</label>
                <div class="col-sm-3">
                  <select class="form-control" name="city_id" id="city_id">
                  <option value="">-- Select --</option>
                  @foreach($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="area_id">Area</label>
                <div class="col-sm-3">
                  <select class="form-control" name="area_id" id="area_id">
                  <option value="">-- Select --</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="date_from">From</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control idatepicker" name="date_from" value="" autofocus="" readonly="">
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-2 control-label" for="date_to">To</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control idatepicker" name="date_to" value="" autofocus="" readonly="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  {!! csrf_field() !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </form>

          </div>
        </section>
      </div>
    </div>
  <script type="text/javascript">
    var xhr_area_list = null;

    $(function(){
      $('#city_id').change(function(){
        area_list($(this).val(), '');
      });
    });
  </script>
@stop
@extends('pemo.layouts.print')

@section('container')
@parent
	<div class="row">
		<div class="col-sm-2">Location:</div>
		<div class="col-sm-10">{{ $area->city->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Recorder:</div>
		<div class="col-sm-10">{{ Auth::user()->name }}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">Date:</div>
		<div class="col-sm-10">{{ date('F d, Y', strtotime($date_from)) . ' - ' . date('F d, Y', strtotime($date_to)) }}</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered" width="100%">
				<thead>
					<tr>
						<th>Species</th>
						<th class="text-center">Basal Area</th>
						<th class="text-center">Crown Cover</th>
					</tr>
				</thead>
				<tbody>
				@foreach($rows as $row)
				<tr>
					<td>{{ $row->name }}</td>
					<td class="text-center">{{ $row->total_basal_area($city_id, $date_from, $date_to) }}</td>
					<td class="text-center">{{ $row->total_crown_cover($city_id, $date_from, $date_to) }}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop
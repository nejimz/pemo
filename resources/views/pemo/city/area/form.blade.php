@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-map-marker"></i> Area</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-map-marker"></i><a href="{{ route('area') }}">Area</a></li>
          <li><i class="fa fa-file"></i>Form</li>
        </ol>
      </div>
    </div>
    @include('pemo.layouts.messages')
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Add Area</header>
          <div class="panel-body">

            <form action="{{ $action }}" method="post" class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">City</label>
                <div class="col-sm-5">
                  <select class="form-control" name="city_id">
                  <option value="0">-- Select --</option>
                  @foreach($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="name" value="{{ $name }}">
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  {!! csrf_field() !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </form>

          </div>
        </section>
      </div>
    </div>


  <script type="text/javascript">
    
  $('#city_id').val('<?php echo $city_id; ?>');

  </script>
@stop
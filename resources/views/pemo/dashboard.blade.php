@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-laptop"></i> Home</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="{{ route('dashboard') }}">Home</a></li>
        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2><i class="fa fa-map-marker red"></i><strong>Negros Occidental</strong></h2>
          </div>
          <div id="map-container" class="panel-body-map" style="height: 650px;">
          </div>
        </div>
      </div>
    </div>

  </section><!--section class="wrapper"-->
  <input type="hidden" id="city_summary" value="{{ route('city_summary') }}">
  <script type="text/javascript">
  //console.log(series[0]['data'][0]['value']);
  var chart, 
      H = Highcharts;
  var mapData = [
        {
          code: "NOR",
          name: "Negros Oriental",
          color: '#8fbc8f',
          path: "M370,-578,476,-552,477,-509,431,-375,389,-321,381,-277,392,-211,387,-187,367,-188,363,-171,385,-159,373,-142,397,-128,405,-103,424,-93,463,-33L463,8L414,94,379,119,344,116,316,122,276,105,244,12,246,-18,205,-42,147,-50,110,-75,156,-138,183,-168,300,-320,311,-331,321,-376,379,-463,408,-506,397,-524z",
          value: null
          //value: [10, 20, 30]
        },
        {
          code: "HINO",
          name: "Hinoba-an",
          //color: "#8fbc8f",
          path: "M110,-75,141,-119,158,-142,160,-204,25,-200,43,-192,32,-180z",
          value: null
        },
        {
          code: "CAN",
          name: "Candoni",
          //color: "#8fbc8f",
          path: "M166,-288,160,-204,97,-203,91,-286z",
          value: null
        },
        {
          code: "SIP",
          name: "Sipalay",
          //color: "#8fbc8f",
          path: "M91,-286,97,-203,25,-200,16,-192,11,-203,23,-214,15,-219,5,-214,0,-221,7,-242,1,-282z",
          value: null
        },
        {
          code: "CAU",
          name: "Cauayan",
          //color: "#8fbc8f",
          path: "M137,-355,170,-342,166,-288,1,-282,33,-352,62,-359z",
          value: null
        },
        {
          code: "IL",
          name: "Ilog",
          //color: "#8fbc8f",
          path: "M183,-168,156,-138,170,-342,137,-355,168,-364,202,-400,217,-398,208,-356,189,-331z",
          value: null
        },
        {
          code: "KAB",
          name: "Kabankalan",
          //color: "#8fbc8f",
          path: "M229,-405,217,-398,208,-356,189,-331,183,-168,300,-320,222,-380z",
          value: null
        },
        {
          code: "HIM",
          name: "Himamaylan",
          //color: "#8fbc8f",
          path: "M300,-320,311,-331,321,-376,266,-468,231,-465,246,-413,229,-405,222,-380z",
          value: null
        },
        {
          code: "BIN",
          name: "Binalbagan",
          //color: "#8fbc8f",
          path: "M232,-488,231,-465,266,-468,288,-465,292,-477,286,-496,256,-499z",
          value: null
        },
        {
          code: "HIN",
          name: "Hinigaran",
          //color: "#8fbc8f",
          path: "M247,-495,240,-506L229,-506L232,-488z",
          value: null
        },
        {
          code: "ISA",
          name: "Isabela",
          //color: "#8fbc8f",
          path: "M321,-376,265,-467,288,-465,293,-477,302,-497,310,-502,312,-492,341,-488,369,-493,379,-463z",
          value: null
        },
        {
          code: "MP",
          name: "Moises Padilla",
          //color: "#8fbc8f",
          path: "M379,-463,408,-506,397,-524,361,-515,327,-511,310,-502,312,-492,341,-488,369,-493z",
          value: null
        },
        {
          code: "PON",
          name: "Pontevedra",
          //color: "#8fbc8f",
          path: "M229,-506,240,-506,247,-495,256,-499,286,-496,302,-522,298,-546,315,-558,298,-568,282,-564,257,-574,228,-572,241,-549,228,-518,229,-506z",
          value: null
        },
        {
          code: "LCT",
          name: "La Castellana",
          //color: "#8fbc8f",
          path: "M370,-578,397,-524,361,-515,327,-511,302,-497,292,-477,286,-496,302,-522,298,-546,315,-558z",
          value: null
        },
        {
          code: "LCR",
          name: "La Carlota",
          //color: "#8fbc8f",
          path: "M257,-574,282,-564,298,-568,315,-558,370,-578,300,-585,267,-599,257,-590z",
          value: null
        },
        {
          code: "SE",
          name: "San Enrique",
          //color: "#8fbc8f",
          path: "M228,-572,257,-574L257,-574L257,-590,229,-596,222,-587z",
          value: null
        },
        {
          code: "VIL",
          name: "Villadolid",
          //color: "#8fbc8f",
          path: "M222,-587,217,-603,225,-625,267,-599,257,-590,229,-596z",
          value: null
        },
        {
          code: "PUL",
          name: "Pulupandan",
          //color: "#8fbc8f",
          path: "M217,-603,225,-625,234,-634,224,-653,205,-635z",
          value: null
        },
        {
          code: "BAG",
          name: "Bago",
          //color: "#8fbc8f",
          path: "M343,-581,349,-644,342,-653,315,-645,294,-646,295,-666,257,-671,237,-650,224,-653,234,-634,225,-625,267,-599,300,-585z",
          value: null
        },
        {
          code: "MUR",
          name: "Murcia",
          //color: "#8fbc8f",
          path: "M343,-581,370,-578,426,-633,384,-654,425,-691,352,-706,342,-702,324,-684,295,-666,294,-646,315,-645,342,-653,349,-644z",
          value: null
        },
        {
          code: "DSB",
          name: "Don Salvador Benedicto",
          //color: "#8fbc8f",
          path: "M426,-633,384,-654,425,-691,459,-676,483,-655,479,-638z",
          value: null
        },
        {
          code: "SC",
          name: "San Carlos",
          //color: "#8fbc8f",
          path: "M370,-578,476,-552,494,-590,512,-602,534,-624,516,-658,511,-642,479,-638,426,-633z",
          value: null
        },
        {
          code: "CAL",
          name: "Calatrava",
          //color: "#8fbc8f",
          path: "M570,-735,543,-632,534,-624,516,-658,511,-642,479,-638,483,-655,459,-676,425,-691,482,-753,486,-732,523,-731,544,-726z",
          value: null
        },
        {
          code: "TOB",
          name: "Toboso",
          //color: "#8fbc8f",
          path: "M570,-735,587,-752,556,-764,546,-753,486,-742,486,-732,523,-731,544,-726z",
          value: null
        },
        {
          code: "ESC",
          name: "Escalante",
          //color: "#8fbc8f",
          path: "M587,-752,596,-767,593,-790,577,-822,560,-822,539,-807,540,-800,525,-792,527,-784,482,-753,486,-742,546,-753,556,-764z",
          value: null
        },
        {
          code: "SAG",
          name: "Sagay",
          //color: "#8fbc8f",
          path: "M508,-856,425,-691,482,-753,527,-784,525,-792,540,-800,539,-807,560,-822,577,-822,563,-847,549,-840z",
          value: null
        },
        {
          code: "BAC",
          name: "Bacolod",
          //color: "#8fbc8f",
          path: "M257,-671,295,-666,324,-684,342,-702,352,-706L352,-706L342,-714,313,-718,284,-733,283,-717,273,-684z",
          value: null
        },
        {
          code: "TAL",
          name: "Talisay",
          //color: "#8fbc8f",
          path: "M284,-733,291,-763,434,-709,425,-691,352,-706,342,-714,313,-718z",
          value: null
        },
        {
          code: "SIL",
          name: "Silay",
          //color: "#8fbc8f",
          path: "M291,-763L291,-778L282,-802,309,-791,317,-793,328,-783,391,-767,439,-719,434,-709z",
          value: null
        },
        {
          code: "EBM",
          name: "E. B. Magalona",
          //color: "#8fbc8f",
          path: "M282,-802,309,-791,317,-793,328,-783,391,-767,389,-776,347,-788,329,-809,331,-835,290,-825z",
          value: null
        },
        {
          code: "VIC",
          name: "Victorias",
          //color: "#8fbc8f",
          path: "M331,-835,329,-809,347,-788,389,-776,387,-793L379,-793L362,-833,354,-837z",
          value: null
        },
        {
          code: "MAN",
          name: "Manapla",
          //color: "#8fbc8f",
          path: "M354,-837,362,-833,379,-793L387,-793L414,-856,426,-878L404,-878z",
          value: null
        },
        {
          code: "CAD",
          name: "Cadiz",
          //color: "#8fbc8f",
          path: "M439,-719,391,-767,387,-793,414,-856,426,-878,482,-853,508,-856z",
          value: null
        }
      ];
  var series = [{
      name: "Number of Mangrove Trees",
      dataLabels: { enabled: true, format : '{point.code}' }, 
      borderColor: '#555555',
      tooltip: {
        useHTML: true,
        headerFormat: '{series.name}<br />',
        pointFormatter: function(){
          var text = '';
          $.each(this.value, function(index, value){
            text += '' + value[0] + ': ' + value[1] + '<br />';
          });
          return text;
        },
        pointFormat: '{point.value.1.0}: {point.value.1.1}<br />' + 
                     '{point.value.2.0}: {point.value.2.1}<br />' + 
                     '{point.value.3.0}: {point.value.3.1}<br />' + 
                     '{point.value.4.0}: {point.value.4.1}<br />' + 
                     '{point.value.5.0}: {point.value.5.1}<br />' + 
                     '{point.value.6.0}: {point.value.6.1}<br />' + 
                     'Total: {point.value.0}<br />'
      },
      data: mapData,
      showInLegend: false
      //nullColor: 'rgba(200, 200, 200, 0.2)',
    }
  ];
  //series[0]['data'][0]['value'] = 10; // Access and Change VALUE of City
  //console.log(series[0]['data'][0]['value']);
  // Instanciate the map
  $(function(){
    var options = {
      chart: {
        plotBackgroundColor: '#E0EEEE',
      },

      credits: {
          enabled: false
      },

      title: {
          text: "Negros Occidental"
      },

      mapNavigation: {
          enabled: true
      },

      colorAxis: {
          /*
          min: 50,
          max: 1000,
          stops: [ [0, '#FF6961'], [0.5,'#FFB347 '], [1, '#5f9e5f '] ]
          */
          dataClasses: [{
            from: 0,
            to: 0,
            color: '#FFFFFF',
            name: 'Empty'
          }, {
            from: 1,
            to: 100,
            color: '#FF6961',
            name: 'Danger'
          }, {
            from: 101,
            to: 200,
            color: '#FFB347',
            name: 'Healthy'
          }, {
            from: 201,
            to: 1000,
            color: '#5f9e5f',
            name: 'Healthier'
          }]
      },

      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'bottom',
          floating: true
      },

      series: series
    };

    load_map(options, series);

  });

  var xhr_load_map = null;
  function load_map(options, series)
  {
    var negros_data;

    xhr_area_list = $.ajax({
        type : 'get',
        url : $("#city_summary").val(),
        cache : false,
        dataType : "json",
        beforeSend: function(xhr){
            if (xhr_load_map != null)
            {
                xhr_load_map.abort();
            }
        }
    }).done(function(data){
      negros_data = data;

      $.each(negros_data, function(index, value){
        $.each(series[0]['data'], function(index2, value2){
          if(value['name'].toLowerCase() == value2['name'].toLowerCase())
          {
            var total_count = value['value'][0][1];

            series[0]['data'][index2]['value'] = value['value'];

            if(total_count > 500)
            {
              series[0]['data'][index2]['color'] = '#5f9e5f';
            }
            else if(total_count > 100 && total_count < 300)
            {
              series[0]['data'][index2]['color'] = '#FFB347';
            }
            else
            {
              series[0]['data'][index2]['color'] = '#FF6961';
            }
          }
        });
      });

      var chart = Highcharts.mapChart('map-container', options);

    }).fail(function(jqXHR, textStatus){
      //console.log('Request failed: ' + textStatus);
    });
  }

  </script>
@stop
@extends('pemo.layouts.master')

@section('container')
@parent

  <section class="wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-tree"></i> Mangrove</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-tree"></i><a href="{{ route('mangrove') }}">Mangrove</a></li>
          <li><i class="fa fa-file"></i>Form</li>
        </ol>
      </div>
    </div>
    @include('pemo.layouts.messages')
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Add a Mangrove</header>
          <div class="panel-body">

            <form action="{{ $action }}" method="post" class="form-horizontal" enctype="multipart/form-data">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="name">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="name" value="{{ $name }}" autofocus="">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="scientific_name">Scientific Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="scientific_name" value="{{ $scientific_name }}">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="scientific_name">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="image" name="image">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="scientific_name">Details</label>
                <div class="col-sm-10">
                  <textarea class="summernote" name="details">{{ $details }}</textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  {!! csrf_field() !!}
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>

            </form>

          </div>
        </section>
      </div>
    </div>

  <script type="text/javascript">
    

  </script>
@stop
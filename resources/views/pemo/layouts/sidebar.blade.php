<?php 
$segment = Request::segment(2); 
$segment_3 = Request::segment(3); 
?>
<aside>
	<div id="sidebar"  class="nav-collapse ">
		<ul class="sidebar-menu">
			<li class="{{ (($segment == '')? 'active' : '') }}">
	          	<a href="{{ route('dashboard') }}">
	              	<i class="icon_house_alt"></i>
	              	<span>Home</span>
	          	</a>
	      	</li>
			<li class="{{ (($segment == 'city' && $segment_3 != 'area')? 'active' : '') }}">
	          	<a href="{{ route('city') }}">
				    <i class="icon_building"></i>
					<span>Cities</span>
				</a>   
			</li>
			<li class="{{ (($segment == 'area')? 'active' : '') }}">
	          	<a href="{{ route('area') }}">
				    <i class="fa fa-map-marker"></i>
					<span>Area</span>
				</a>   
			</li>
			<li class="{{ (($segment == 'mangrove')? 'active' : '') }}">
	          	<a href="{{ route('mangrove') }}">
				    <i class="fa fa-tree"></i>
					<span>Mangrove</span>
				</a>   
			</li>
			<li class="{{ (($segment == 'inventory')? 'active' : '') }}">
	          	<a href="{{ route('inventory') }}">
				    <i class="fa fa-dropbox"></i>
					<span>Assessment </span>
				</a>   
			</li>
          	<li class="sub-menu {{ (($segment == 'reports')? 'active' : '') }}">
				<a href="javascript::void(0)" class="">
				  	<i class="icon_document_alt"></i>
				  	<span>Reports</span>
				  	<span class="menu-arrow arrow_carrot-right"></span>
				</a>
				<ul class="sub">
				  	<li><a href="{{ route('report_inventory') }}"><i class="fa fa-dropbox"></i> Assessment</a></li>
				  	<li><a href="{{ route('report_inventory_summary') }}" title="Assessment Summary"><i class="fa fa-list"></i> Assess. Sum.</a></li>
				  	<li><a href="{{ route('report_summary') }}"><i class="fa fa-file-text-o"></i> Summary</a></li>
				</ul>
  			</li>  
	  	</ul>
	</div>
</aside>
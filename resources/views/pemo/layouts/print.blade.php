<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="PEMO - Provincial Environment Management Office">
	    <meta name="author" content="GeeksLabs">
	    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
	    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
	    <title>Provincial Environment Management</title>
	    <!-- Bootstrap CSS -->    
	    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
	    <!-- bootstrap theme -->
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}" />
	    <!-- font icon -->
	    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/style-responsive.cs') }}s" />
	    <!-- javascripts -->
	    <script type="text/javascript" src="{{ asset('js/jquery-1.12.4.js') }}"></script>
	</head>

	<body>
	<!-- container section start -->
	<section id="container">
	    @yield('container', '')
	    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	</body>
</html>

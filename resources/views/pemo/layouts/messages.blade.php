@if (count($errors) > 0)
    <div class="alert alert-block alert-danger fade in">
        <ul class="no-bullet">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (Session::has('success'))
    <div class="alert alert-success fade in">
        {{ Session::get('success') }}
    </div>
@endif
@if (Session::has('custom_error'))
    <div class="alert alert-block alert-danger fade in">
        {{ Session::get('custom_error') }}
    </div>
@endif
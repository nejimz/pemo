<header class="header dark-bg">
    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
    </div>
    <!--logo start-->
    <a href="{{ route('dashboard') }}" class="logo" title="Provincial Environment Management Office">PEMO</a>
    <!--logo end-->
    <div class="top-nav notification-row">                
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="profile-ava">
                        <i class="fa fa-user fa-lg"></i>
                    </span>
                    <span class="username">{{ Auth::user()->name }}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout">
                    <div class="log-arrow-up"></div>
                    <li>
                        <a href="{{ route('user') }}"><i class="fa fa-users"></i> User</a>
                    </li>
                    <div class="log-arrow-up"></div>
                    <li>
                        <a href="{{ route('user_reset_password') }}"><i class="fa fa-refresh"></i> Reset Password</a>
                    </li>
                    <div class="log-arrow-up"></div>
                    <li>
                        <a href="{{ route('logout') }}"><i class="icon_key_alt"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
    </div>
</header>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="PEMO - Provincial Environment Management Office">
	    <meta name="author" content="GeeksLabs">
	    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
	    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
	    <title>Provincial Environment Management</title>
	    <!-- Bootstrap CSS -->    
	    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
	    <!-- bootstrap theme -->
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}" />
	    <!-- font icon -->
	    <link rel="stylesheet" href="{{ asset('css/elegant-icons-style.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/style-responsive.cs') }}s" />
		<link rel="stylesheet" href="{{ asset('css/jquery-ui-1.10.4.min.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/summernote.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
	    <!-- javascripts -->
	    <script type="text/javascript" src="{{ asset('js/jquery-1.12.4.js') }}"></script>
	    <!--script type="text/javascript" src="{{ asset('js/app.js') }}"></script-->
		<script src="{{ asset('js/proj4.js') }}"></script>
	    <script src="{{ asset('js/highmaps.js') }}"></script>
		<script src="{{ asset('js/exporting.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	    <script type="text/javascript">
		    function area_list(city_id, area_id)
		    {
		      xhr_area_list = $.ajax({
		          type : 'get',
		          url : $("#area_xhr").val(),
		          data: 'city_id=' + city_id,
		          cache : false,
		          dataType : "json",
		          beforeSend: function(xhr){
		              if (xhr_area_list != null)
		              {
		                  xhr_area_list.abort();
		              }
		          }
		      }).done(function(data){
		        var str_area = '<option value="">-- Select --</option>';
		        $.each(data, function(index, value){
		          str_area += '<option value="' + index + '">' + value + '</option>';
		        });

		        $('#area_id').html(str_area);
		        $('#area_id').val(area_id);
		      }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		      });
		    }
	    </script>
	</head>

	<body>
	<!-- container section start -->
	<section id="container" class="">

    	<input type="hidden" id="area_xhr" value="{{ route('area_xhr') }}">

		@include('pemo.layouts.header')
		@include('pemo.layouts.sidebar')
		<section id="main-content">
	        @yield('container', '')
	    </section><!-- section id="main-content" -->

		<!--script type="text/javascript" src="{{ asset('js/jquery-ui-1.10.4.min.js') }}"></script-->
	    <!--script type="text/javascript" src="{{ asset('js/jquery-1.8.3.min.js') }}"></script-->
	    <!--script type="text/javascript" src="{{ asset('js/jquery-ui-1.9.2.custom.min.js') }}"></script-->
	    <!-- bootstrap -->
		<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
		<script src="{{ asset('js/summer-note/summernote.min.js') }}"></script>
	    <!-- nice scroll -->
	    <script type="text/javascript" src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('js/jquery.nicescroll.js') }}"></script>
	    <!--custome script for all page-->
	    <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
	    <script type="text/javascript">
	    $(function(){
	    	$('.idatepicker').datepicker(function(){
	    		//Default: false,
	    		format: 'mm/dd/yyyy'
	    	});

			$('.summernote').summernote({
				height: 300,                 // set editor height
				minHeight: null,             // set minimum height of editor
				maxHeight: null,             // set maximum height of editor
				focus: true                  // set focus to editable area after initializing summernote
			});
	    });
	    </script>
	</body>
</html>

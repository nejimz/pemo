<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
	    <meta name="author" content="GeeksLabs">
	    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
	    <link rel="shortcut icon" href="img/favicon.png') }}">

	    <title>PEMO</title>

	    <!-- Bootstrap CSS -->    
	    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	    <!-- bootstrap theme -->
	    <link href="{{ asset('css/bootstrap-theme.css') }}" rel="stylesheet">
	    <!--external css-->
	    <!-- font icon -->
	    <link href="{{ asset('css/elegant-icons-style.css') }}" rel="stylesheet" />
	    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" />
	    <!-- Custom styles -->
	    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
	    <!--[if lt IE 9]>
	    <script src="{{ asset('js/html5shiv.js') }}"></script>
	    <script src="{{ asset('js/respond.min.js') }}"></script>
	    <![endif]-->
	</head>
	<body class="login-img3-body">
		<div class="container">
		  	<form class="login-form" action="{{ route('authenticate') }}" method="post">        
			    <div class="login-wrap">
			        <p class="login-img"><i class="icon_lock_alt"></i></p>
    				@include('pemo.layouts.messages')
			        <div class="input-group">
			          	<span class="input-group-addon"><i class="icon_profile"></i></span>
			          	<input type="text" class="form-control" placeholder="Username" name="username" autofocus />
			        </div>
			        <div class="input-group">
			            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
			            <input type="password" class="form-control" placeholder="Password" name="password" />
			        </div>
			        {!! csrf_field() !!}
			        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
			        <!--button class="btn btn-info btn-lg btn-block" type="submit">Signup</button-->
			    </div>
		  	</form>
	    </div>
	</body>
</html>

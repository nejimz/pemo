/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(8);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

__webpack_require__(2);

//window.Vue = require('vue');
__webpack_require__(6);
__webpack_require__(7);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {


window._ = __webpack_require__(3);

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  //    window.$ = window.jQuery = require('jquery');

  //require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

// window.axios = require('axios');

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, module) {var __WEBPACK_AMD_DEFINE_RESULT__;/**
 * @license
 * Lodash <https://lodash.com/>
 * Copyright JS Foundation and other contributors <https://js.foundation/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */
;(function() {

  /** Used as a safe reference for `undefined` in pre-ES5 environments. */
  var undefined;

  /** Used as the semantic version number. */
  var VERSION = '4.17.4';

  /** Used as the size to enable large array optimizations. */
  var LARGE_ARRAY_SIZE = 200;

  /** Error message constants. */
  var CORE_ERROR_TEXT = 'Unsupported core-js use. Try https://npms.io/search?q=ponyfill.',
      FUNC_ERROR_TEXT = 'Expected a function';

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED = '__lodash_hash_undefined__';

  /** Used as the maximum memoize cache size. */
  var MAX_MEMOIZE_SIZE = 500;

  /** Used as the internal argument placeholder. */
  var PLACEHOLDER = '__lodash_placeholder__';

  /** Used to compose bitmasks for cloning. */
  var CLONE_DEEP_FLAG = 1,
      CLONE_FLAT_FLAG = 2,
      CLONE_SYMBOLS_FLAG = 4;

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG = 1,
      COMPARE_UNORDERED_FLAG = 2;

  /** Used to compose bitmasks for function metadata. */
  var WRAP_BIND_FLAG = 1,
      WRAP_BIND_KEY_FLAG = 2,
      WRAP_CURRY_BOUND_FLAG = 4,
      WRAP_CURRY_FLAG = 8,
      WRAP_CURRY_RIGHT_FLAG = 16,
      WRAP_PARTIAL_FLAG = 32,
      WRAP_PARTIAL_RIGHT_FLAG = 64,
      WRAP_ARY_FLAG = 128,
      WRAP_REARG_FLAG = 256,
      WRAP_FLIP_FLAG = 512;

  /** Used as default options for `_.truncate`. */
  var DEFAULT_TRUNC_LENGTH = 30,
      DEFAULT_TRUNC_OMISSION = '...';

  /** Used to detect hot functions by number of calls within a span of milliseconds. */
  var HOT_COUNT = 800,
      HOT_SPAN = 16;

  /** Used to indicate the type of lazy iteratees. */
  var LAZY_FILTER_FLAG = 1,
      LAZY_MAP_FLAG = 2,
      LAZY_WHILE_FLAG = 3;

  /** Used as references for various `Number` constants. */
  var INFINITY = 1 / 0,
      MAX_SAFE_INTEGER = 9007199254740991,
      MAX_INTEGER = 1.7976931348623157e+308,
      NAN = 0 / 0;

  /** Used as references for the maximum length and index of an array. */
  var MAX_ARRAY_LENGTH = 4294967295,
      MAX_ARRAY_INDEX = MAX_ARRAY_LENGTH - 1,
      HALF_MAX_ARRAY_LENGTH = MAX_ARRAY_LENGTH >>> 1;

  /** Used to associate wrap methods with their bit flags. */
  var wrapFlags = [
    ['ary', WRAP_ARY_FLAG],
    ['bind', WRAP_BIND_FLAG],
    ['bindKey', WRAP_BIND_KEY_FLAG],
    ['curry', WRAP_CURRY_FLAG],
    ['curryRight', WRAP_CURRY_RIGHT_FLAG],
    ['flip', WRAP_FLIP_FLAG],
    ['partial', WRAP_PARTIAL_FLAG],
    ['partialRight', WRAP_PARTIAL_RIGHT_FLAG],
    ['rearg', WRAP_REARG_FLAG]
  ];

  /** `Object#toString` result references. */
  var argsTag = '[object Arguments]',
      arrayTag = '[object Array]',
      asyncTag = '[object AsyncFunction]',
      boolTag = '[object Boolean]',
      dateTag = '[object Date]',
      domExcTag = '[object DOMException]',
      errorTag = '[object Error]',
      funcTag = '[object Function]',
      genTag = '[object GeneratorFunction]',
      mapTag = '[object Map]',
      numberTag = '[object Number]',
      nullTag = '[object Null]',
      objectTag = '[object Object]',
      promiseTag = '[object Promise]',
      proxyTag = '[object Proxy]',
      regexpTag = '[object RegExp]',
      setTag = '[object Set]',
      stringTag = '[object String]',
      symbolTag = '[object Symbol]',
      undefinedTag = '[object Undefined]',
      weakMapTag = '[object WeakMap]',
      weakSetTag = '[object WeakSet]';

  var arrayBufferTag = '[object ArrayBuffer]',
      dataViewTag = '[object DataView]',
      float32Tag = '[object Float32Array]',
      float64Tag = '[object Float64Array]',
      int8Tag = '[object Int8Array]',
      int16Tag = '[object Int16Array]',
      int32Tag = '[object Int32Array]',
      uint8Tag = '[object Uint8Array]',
      uint8ClampedTag = '[object Uint8ClampedArray]',
      uint16Tag = '[object Uint16Array]',
      uint32Tag = '[object Uint32Array]';

  /** Used to match empty string literals in compiled template source. */
  var reEmptyStringLeading = /\b__p \+= '';/g,
      reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
      reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;

  /** Used to match HTML entities and HTML characters. */
  var reEscapedHtml = /&(?:amp|lt|gt|quot|#39);/g,
      reUnescapedHtml = /[&<>"']/g,
      reHasEscapedHtml = RegExp(reEscapedHtml.source),
      reHasUnescapedHtml = RegExp(reUnescapedHtml.source);

  /** Used to match template delimiters. */
  var reEscape = /<%-([\s\S]+?)%>/g,
      reEvaluate = /<%([\s\S]+?)%>/g,
      reInterpolate = /<%=([\s\S]+?)%>/g;

  /** Used to match property names within property paths. */
  var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
      reIsPlainProp = /^\w*$/,
      reLeadingDot = /^\./,
      rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

  /**
   * Used to match `RegExp`
   * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
   */
  var reRegExpChar = /[\\^$.*+?()[\]{}|]/g,
      reHasRegExpChar = RegExp(reRegExpChar.source);

  /** Used to match leading and trailing whitespace. */
  var reTrim = /^\s+|\s+$/g,
      reTrimStart = /^\s+/,
      reTrimEnd = /\s+$/;

  /** Used to match wrap detail comments. */
  var reWrapComment = /\{(?:\n\/\* \[wrapped with .+\] \*\/)?\n?/,
      reWrapDetails = /\{\n\/\* \[wrapped with (.+)\] \*/,
      reSplitDetails = /,? & /;

  /** Used to match words composed of alphanumeric characters. */
  var reAsciiWord = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g;

  /** Used to match backslashes in property paths. */
  var reEscapeChar = /\\(\\)?/g;

  /**
   * Used to match
   * [ES template delimiters](http://ecma-international.org/ecma-262/7.0/#sec-template-literal-lexical-components).
   */
  var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;

  /** Used to match `RegExp` flags from their coerced string values. */
  var reFlags = /\w*$/;

  /** Used to detect bad signed hexadecimal string values. */
  var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

  /** Used to detect binary string values. */
  var reIsBinary = /^0b[01]+$/i;

  /** Used to detect host constructors (Safari). */
  var reIsHostCtor = /^\[object .+?Constructor\]$/;

  /** Used to detect octal string values. */
  var reIsOctal = /^0o[0-7]+$/i;

  /** Used to detect unsigned integer values. */
  var reIsUint = /^(?:0|[1-9]\d*)$/;

  /** Used to match Latin Unicode letters (excluding mathematical operators). */
  var reLatin = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g;

  /** Used to ensure capturing order of template delimiters. */
  var reNoMatch = /($^)/;

  /** Used to match unescaped characters in compiled string literals. */
  var reUnescapedString = /['\n\r\u2028\u2029\\]/g;

  /** Used to compose unicode character classes. */
  var rsAstralRange = '\\ud800-\\udfff',
      rsComboMarksRange = '\\u0300-\\u036f',
      reComboHalfMarksRange = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange = '\\u20d0-\\u20ff',
      rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
      rsDingbatRange = '\\u2700-\\u27bf',
      rsLowerRange = 'a-z\\xdf-\\xf6\\xf8-\\xff',
      rsMathOpRange = '\\xac\\xb1\\xd7\\xf7',
      rsNonCharRange = '\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf',
      rsPunctuationRange = '\\u2000-\\u206f',
      rsSpaceRange = ' \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000',
      rsUpperRange = 'A-Z\\xc0-\\xd6\\xd8-\\xde',
      rsVarRange = '\\ufe0e\\ufe0f',
      rsBreakRange = rsMathOpRange + rsNonCharRange + rsPunctuationRange + rsSpaceRange;

  /** Used to compose unicode capture groups. */
  var rsApos = "['\u2019]",
      rsAstral = '[' + rsAstralRange + ']',
      rsBreak = '[' + rsBreakRange + ']',
      rsCombo = '[' + rsComboRange + ']',
      rsDigits = '\\d+',
      rsDingbat = '[' + rsDingbatRange + ']',
      rsLower = '[' + rsLowerRange + ']',
      rsMisc = '[^' + rsAstralRange + rsBreakRange + rsDigits + rsDingbatRange + rsLowerRange + rsUpperRange + ']',
      rsFitz = '\\ud83c[\\udffb-\\udfff]',
      rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
      rsNonAstral = '[^' + rsAstralRange + ']',
      rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
      rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
      rsUpper = '[' + rsUpperRange + ']',
      rsZWJ = '\\u200d';

  /** Used to compose unicode regexes. */
  var rsMiscLower = '(?:' + rsLower + '|' + rsMisc + ')',
      rsMiscUpper = '(?:' + rsUpper + '|' + rsMisc + ')',
      rsOptContrLower = '(?:' + rsApos + '(?:d|ll|m|re|s|t|ve))?',
      rsOptContrUpper = '(?:' + rsApos + '(?:D|LL|M|RE|S|T|VE))?',
      reOptMod = rsModifier + '?',
      rsOptVar = '[' + rsVarRange + ']?',
      rsOptJoin = '(?:' + rsZWJ + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
      rsOrdLower = '\\d*(?:(?:1st|2nd|3rd|(?![123])\\dth)\\b)',
      rsOrdUpper = '\\d*(?:(?:1ST|2ND|3RD|(?![123])\\dTH)\\b)',
      rsSeq = rsOptVar + reOptMod + rsOptJoin,
      rsEmoji = '(?:' + [rsDingbat, rsRegional, rsSurrPair].join('|') + ')' + rsSeq,
      rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

  /** Used to match apostrophes. */
  var reApos = RegExp(rsApos, 'g');

  /**
   * Used to match [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks) and
   * [combining diacritical marks for symbols](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks_for_Symbols).
   */
  var reComboMark = RegExp(rsCombo, 'g');

  /** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
  var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

  /** Used to match complex or compound words. */
  var reUnicodeWord = RegExp([
    rsUpper + '?' + rsLower + '+' + rsOptContrLower + '(?=' + [rsBreak, rsUpper, '$'].join('|') + ')',
    rsMiscUpper + '+' + rsOptContrUpper + '(?=' + [rsBreak, rsUpper + rsMiscLower, '$'].join('|') + ')',
    rsUpper + '?' + rsMiscLower + '+' + rsOptContrLower,
    rsUpper + '+' + rsOptContrUpper,
    rsOrdUpper,
    rsOrdLower,
    rsDigits,
    rsEmoji
  ].join('|'), 'g');

  /** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
  var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

  /** Used to detect strings that need a more robust regexp to match words. */
  var reHasUnicodeWord = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;

  /** Used to assign default `context` object properties. */
  var contextProps = [
    'Array', 'Buffer', 'DataView', 'Date', 'Error', 'Float32Array', 'Float64Array',
    'Function', 'Int8Array', 'Int16Array', 'Int32Array', 'Map', 'Math', 'Object',
    'Promise', 'RegExp', 'Set', 'String', 'Symbol', 'TypeError', 'Uint8Array',
    'Uint8ClampedArray', 'Uint16Array', 'Uint32Array', 'WeakMap',
    '_', 'clearTimeout', 'isFinite', 'parseInt', 'setTimeout'
  ];

  /** Used to make template sourceURLs easier to identify. */
  var templateCounter = -1;

  /** Used to identify `toStringTag` values of typed arrays. */
  var typedArrayTags = {};
  typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
  typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
  typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
  typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
  typedArrayTags[uint32Tag] = true;
  typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
  typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
  typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
  typedArrayTags[errorTag] = typedArrayTags[funcTag] =
  typedArrayTags[mapTag] = typedArrayTags[numberTag] =
  typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
  typedArrayTags[setTag] = typedArrayTags[stringTag] =
  typedArrayTags[weakMapTag] = false;

  /** Used to identify `toStringTag` values supported by `_.clone`. */
  var cloneableTags = {};
  cloneableTags[argsTag] = cloneableTags[arrayTag] =
  cloneableTags[arrayBufferTag] = cloneableTags[dataViewTag] =
  cloneableTags[boolTag] = cloneableTags[dateTag] =
  cloneableTags[float32Tag] = cloneableTags[float64Tag] =
  cloneableTags[int8Tag] = cloneableTags[int16Tag] =
  cloneableTags[int32Tag] = cloneableTags[mapTag] =
  cloneableTags[numberTag] = cloneableTags[objectTag] =
  cloneableTags[regexpTag] = cloneableTags[setTag] =
  cloneableTags[stringTag] = cloneableTags[symbolTag] =
  cloneableTags[uint8Tag] = cloneableTags[uint8ClampedTag] =
  cloneableTags[uint16Tag] = cloneableTags[uint32Tag] = true;
  cloneableTags[errorTag] = cloneableTags[funcTag] =
  cloneableTags[weakMapTag] = false;

  /** Used to map Latin Unicode letters to basic Latin letters. */
  var deburredLetters = {
    // Latin-1 Supplement block.
    '\xc0': 'A',  '\xc1': 'A', '\xc2': 'A', '\xc3': 'A', '\xc4': 'A', '\xc5': 'A',
    '\xe0': 'a',  '\xe1': 'a', '\xe2': 'a', '\xe3': 'a', '\xe4': 'a', '\xe5': 'a',
    '\xc7': 'C',  '\xe7': 'c',
    '\xd0': 'D',  '\xf0': 'd',
    '\xc8': 'E',  '\xc9': 'E', '\xca': 'E', '\xcb': 'E',
    '\xe8': 'e',  '\xe9': 'e', '\xea': 'e', '\xeb': 'e',
    '\xcc': 'I',  '\xcd': 'I', '\xce': 'I', '\xcf': 'I',
    '\xec': 'i',  '\xed': 'i', '\xee': 'i', '\xef': 'i',
    '\xd1': 'N',  '\xf1': 'n',
    '\xd2': 'O',  '\xd3': 'O', '\xd4': 'O', '\xd5': 'O', '\xd6': 'O', '\xd8': 'O',
    '\xf2': 'o',  '\xf3': 'o', '\xf4': 'o', '\xf5': 'o', '\xf6': 'o', '\xf8': 'o',
    '\xd9': 'U',  '\xda': 'U', '\xdb': 'U', '\xdc': 'U',
    '\xf9': 'u',  '\xfa': 'u', '\xfb': 'u', '\xfc': 'u',
    '\xdd': 'Y',  '\xfd': 'y', '\xff': 'y',
    '\xc6': 'Ae', '\xe6': 'ae',
    '\xde': 'Th', '\xfe': 'th',
    '\xdf': 'ss',
    // Latin Extended-A block.
    '\u0100': 'A',  '\u0102': 'A', '\u0104': 'A',
    '\u0101': 'a',  '\u0103': 'a', '\u0105': 'a',
    '\u0106': 'C',  '\u0108': 'C', '\u010a': 'C', '\u010c': 'C',
    '\u0107': 'c',  '\u0109': 'c', '\u010b': 'c', '\u010d': 'c',
    '\u010e': 'D',  '\u0110': 'D', '\u010f': 'd', '\u0111': 'd',
    '\u0112': 'E',  '\u0114': 'E', '\u0116': 'E', '\u0118': 'E', '\u011a': 'E',
    '\u0113': 'e',  '\u0115': 'e', '\u0117': 'e', '\u0119': 'e', '\u011b': 'e',
    '\u011c': 'G',  '\u011e': 'G', '\u0120': 'G', '\u0122': 'G',
    '\u011d': 'g',  '\u011f': 'g', '\u0121': 'g', '\u0123': 'g',
    '\u0124': 'H',  '\u0126': 'H', '\u0125': 'h', '\u0127': 'h',
    '\u0128': 'I',  '\u012a': 'I', '\u012c': 'I', '\u012e': 'I', '\u0130': 'I',
    '\u0129': 'i',  '\u012b': 'i', '\u012d': 'i', '\u012f': 'i', '\u0131': 'i',
    '\u0134': 'J',  '\u0135': 'j',
    '\u0136': 'K',  '\u0137': 'k', '\u0138': 'k',
    '\u0139': 'L',  '\u013b': 'L', '\u013d': 'L', '\u013f': 'L', '\u0141': 'L',
    '\u013a': 'l',  '\u013c': 'l', '\u013e': 'l', '\u0140': 'l', '\u0142': 'l',
    '\u0143': 'N',  '\u0145': 'N', '\u0147': 'N', '\u014a': 'N',
    '\u0144': 'n',  '\u0146': 'n', '\u0148': 'n', '\u014b': 'n',
    '\u014c': 'O',  '\u014e': 'O', '\u0150': 'O',
    '\u014d': 'o',  '\u014f': 'o', '\u0151': 'o',
    '\u0154': 'R',  '\u0156': 'R', '\u0158': 'R',
    '\u0155': 'r',  '\u0157': 'r', '\u0159': 'r',
    '\u015a': 'S',  '\u015c': 'S', '\u015e': 'S', '\u0160': 'S',
    '\u015b': 's',  '\u015d': 's', '\u015f': 's', '\u0161': 's',
    '\u0162': 'T',  '\u0164': 'T', '\u0166': 'T',
    '\u0163': 't',  '\u0165': 't', '\u0167': 't',
    '\u0168': 'U',  '\u016a': 'U', '\u016c': 'U', '\u016e': 'U', '\u0170': 'U', '\u0172': 'U',
    '\u0169': 'u',  '\u016b': 'u', '\u016d': 'u', '\u016f': 'u', '\u0171': 'u', '\u0173': 'u',
    '\u0174': 'W',  '\u0175': 'w',
    '\u0176': 'Y',  '\u0177': 'y', '\u0178': 'Y',
    '\u0179': 'Z',  '\u017b': 'Z', '\u017d': 'Z',
    '\u017a': 'z',  '\u017c': 'z', '\u017e': 'z',
    '\u0132': 'IJ', '\u0133': 'ij',
    '\u0152': 'Oe', '\u0153': 'oe',
    '\u0149': "'n", '\u017f': 's'
  };

  /** Used to map characters to HTML entities. */
  var htmlEscapes = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'
  };

  /** Used to map HTML entities to characters. */
  var htmlUnescapes = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '"',
    '&#39;': "'"
  };

  /** Used to escape characters for inclusion in compiled string literals. */
  var stringEscapes = {
    '\\': '\\',
    "'": "'",
    '\n': 'n',
    '\r': 'r',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  /** Built-in method references without a dependency on `root`. */
  var freeParseFloat = parseFloat,
      freeParseInt = parseInt;

  /** Detect free variable `global` from Node.js. */
  var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

  /** Detect free variable `self`. */
  var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

  /** Used as a reference to the global object. */
  var root = freeGlobal || freeSelf || Function('return this')();

  /** Detect free variable `exports`. */
  var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports = freeModule && freeModule.exports === freeExports;

  /** Detect free variable `process` from Node.js. */
  var freeProcess = moduleExports && freeGlobal.process;

  /** Used to access faster Node.js helpers. */
  var nodeUtil = (function() {
    try {
      return freeProcess && freeProcess.binding && freeProcess.binding('util');
    } catch (e) {}
  }());

  /* Node.js helper references. */
  var nodeIsArrayBuffer = nodeUtil && nodeUtil.isArrayBuffer,
      nodeIsDate = nodeUtil && nodeUtil.isDate,
      nodeIsMap = nodeUtil && nodeUtil.isMap,
      nodeIsRegExp = nodeUtil && nodeUtil.isRegExp,
      nodeIsSet = nodeUtil && nodeUtil.isSet,
      nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

  /*--------------------------------------------------------------------------*/

  /**
   * Adds the key-value `pair` to `map`.
   *
   * @private
   * @param {Object} map The map to modify.
   * @param {Array} pair The key-value pair to add.
   * @returns {Object} Returns `map`.
   */
  function addMapEntry(map, pair) {
    // Don't return `map.set` because it's not chainable in IE 11.
    map.set(pair[0], pair[1]);
    return map;
  }

  /**
   * Adds `value` to `set`.
   *
   * @private
   * @param {Object} set The set to modify.
   * @param {*} value The value to add.
   * @returns {Object} Returns `set`.
   */
  function addSetEntry(set, value) {
    // Don't return `set.add` because it's not chainable in IE 11.
    set.add(value);
    return set;
  }

  /**
   * A faster alternative to `Function#apply`, this function invokes `func`
   * with the `this` binding of `thisArg` and the arguments of `args`.
   *
   * @private
   * @param {Function} func The function to invoke.
   * @param {*} thisArg The `this` binding of `func`.
   * @param {Array} args The arguments to invoke `func` with.
   * @returns {*} Returns the result of `func`.
   */
  function apply(func, thisArg, args) {
    switch (args.length) {
      case 0: return func.call(thisArg);
      case 1: return func.call(thisArg, args[0]);
      case 2: return func.call(thisArg, args[0], args[1]);
      case 3: return func.call(thisArg, args[0], args[1], args[2]);
    }
    return func.apply(thisArg, args);
  }

  /**
   * A specialized version of `baseAggregator` for arrays.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} setter The function to set `accumulator` values.
   * @param {Function} iteratee The iteratee to transform keys.
   * @param {Object} accumulator The initial aggregated object.
   * @returns {Function} Returns `accumulator`.
   */
  function arrayAggregator(array, setter, iteratee, accumulator) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      var value = array[index];
      setter(accumulator, value, iteratee(value), array);
    }
    return accumulator;
  }

  /**
   * A specialized version of `_.forEach` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns `array`.
   */
  function arrayEach(array, iteratee) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (iteratee(array[index], index, array) === false) {
        break;
      }
    }
    return array;
  }

  /**
   * A specialized version of `_.forEachRight` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns `array`.
   */
  function arrayEachRight(array, iteratee) {
    var length = array == null ? 0 : array.length;

    while (length--) {
      if (iteratee(array[length], length, array) === false) {
        break;
      }
    }
    return array;
  }

  /**
   * A specialized version of `_.every` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {boolean} Returns `true` if all elements pass the predicate check,
   *  else `false`.
   */
  function arrayEvery(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (!predicate(array[index], index, array)) {
        return false;
      }
    }
    return true;
  }

  /**
   * A specialized version of `_.filter` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {Array} Returns the new filtered array.
   */
  function arrayFilter(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length,
        resIndex = 0,
        result = [];

    while (++index < length) {
      var value = array[index];
      if (predicate(value, index, array)) {
        result[resIndex++] = value;
      }
    }
    return result;
  }

  /**
   * A specialized version of `_.includes` for arrays without support for
   * specifying an index to search from.
   *
   * @private
   * @param {Array} [array] The array to inspect.
   * @param {*} target The value to search for.
   * @returns {boolean} Returns `true` if `target` is found, else `false`.
   */
  function arrayIncludes(array, value) {
    var length = array == null ? 0 : array.length;
    return !!length && baseIndexOf(array, value, 0) > -1;
  }

  /**
   * This function is like `arrayIncludes` except that it accepts a comparator.
   *
   * @private
   * @param {Array} [array] The array to inspect.
   * @param {*} target The value to search for.
   * @param {Function} comparator The comparator invoked per element.
   * @returns {boolean} Returns `true` if `target` is found, else `false`.
   */
  function arrayIncludesWith(array, value, comparator) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (comparator(value, array[index])) {
        return true;
      }
    }
    return false;
  }

  /**
   * A specialized version of `_.map` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the new mapped array.
   */
  function arrayMap(array, iteratee) {
    var index = -1,
        length = array == null ? 0 : array.length,
        result = Array(length);

    while (++index < length) {
      result[index] = iteratee(array[index], index, array);
    }
    return result;
  }

  /**
   * Appends the elements of `values` to `array`.
   *
   * @private
   * @param {Array} array The array to modify.
   * @param {Array} values The values to append.
   * @returns {Array} Returns `array`.
   */
  function arrayPush(array, values) {
    var index = -1,
        length = values.length,
        offset = array.length;

    while (++index < length) {
      array[offset + index] = values[index];
    }
    return array;
  }

  /**
   * A specialized version of `_.reduce` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {*} [accumulator] The initial value.
   * @param {boolean} [initAccum] Specify using the first element of `array` as
   *  the initial value.
   * @returns {*} Returns the accumulated value.
   */
  function arrayReduce(array, iteratee, accumulator, initAccum) {
    var index = -1,
        length = array == null ? 0 : array.length;

    if (initAccum && length) {
      accumulator = array[++index];
    }
    while (++index < length) {
      accumulator = iteratee(accumulator, array[index], index, array);
    }
    return accumulator;
  }

  /**
   * A specialized version of `_.reduceRight` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {*} [accumulator] The initial value.
   * @param {boolean} [initAccum] Specify using the last element of `array` as
   *  the initial value.
   * @returns {*} Returns the accumulated value.
   */
  function arrayReduceRight(array, iteratee, accumulator, initAccum) {
    var length = array == null ? 0 : array.length;
    if (initAccum && length) {
      accumulator = array[--length];
    }
    while (length--) {
      accumulator = iteratee(accumulator, array[length], length, array);
    }
    return accumulator;
  }

  /**
   * A specialized version of `_.some` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {boolean} Returns `true` if any element passes the predicate check,
   *  else `false`.
   */
  function arraySome(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (predicate(array[index], index, array)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the size of an ASCII `string`.
   *
   * @private
   * @param {string} string The string inspect.
   * @returns {number} Returns the string size.
   */
  var asciiSize = baseProperty('length');

  /**
   * Converts an ASCII `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function asciiToArray(string) {
    return string.split('');
  }

  /**
   * Splits an ASCII `string` into an array of its words.
   *
   * @private
   * @param {string} The string to inspect.
   * @returns {Array} Returns the words of `string`.
   */
  function asciiWords(string) {
    return string.match(reAsciiWord) || [];
  }

  /**
   * The base implementation of methods like `_.findKey` and `_.findLastKey`,
   * without support for iteratee shorthands, which iterates over `collection`
   * using `eachFunc`.
   *
   * @private
   * @param {Array|Object} collection The collection to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {Function} eachFunc The function to iterate over `collection`.
   * @returns {*} Returns the found element or its key, else `undefined`.
   */
  function baseFindKey(collection, predicate, eachFunc) {
    var result;
    eachFunc(collection, function(value, key, collection) {
      if (predicate(value, key, collection)) {
        result = key;
        return false;
      }
    });
    return result;
  }

  /**
   * The base implementation of `_.findIndex` and `_.findLastIndex` without
   * support for iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {number} fromIndex The index to search from.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseFindIndex(array, predicate, fromIndex, fromRight) {
    var length = array.length,
        index = fromIndex + (fromRight ? 1 : -1);

    while ((fromRight ? index-- : ++index < length)) {
      if (predicate(array[index], index, array)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    return value === value
      ? strictIndexOf(array, value, fromIndex)
      : baseFindIndex(array, baseIsNaN, fromIndex);
  }

  /**
   * This function is like `baseIndexOf` except that it accepts a comparator.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @param {Function} comparator The comparator invoked per element.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOfWith(array, value, fromIndex, comparator) {
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (comparator(array[index], value)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.isNaN` without support for number objects.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   */
  function baseIsNaN(value) {
    return value !== value;
  }

  /**
   * The base implementation of `_.mean` and `_.meanBy` without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {number} Returns the mean.
   */
  function baseMean(array, iteratee) {
    var length = array == null ? 0 : array.length;
    return length ? (baseSum(array, iteratee) / length) : NAN;
  }

  /**
   * The base implementation of `_.property` without support for deep paths.
   *
   * @private
   * @param {string} key The key of the property to get.
   * @returns {Function} Returns the new accessor function.
   */
  function baseProperty(key) {
    return function(object) {
      return object == null ? undefined : object[key];
    };
  }

  /**
   * The base implementation of `_.propertyOf` without support for deep paths.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Function} Returns the new accessor function.
   */
  function basePropertyOf(object) {
    return function(key) {
      return object == null ? undefined : object[key];
    };
  }

  /**
   * The base implementation of `_.reduce` and `_.reduceRight`, without support
   * for iteratee shorthands, which iterates over `collection` using `eachFunc`.
   *
   * @private
   * @param {Array|Object} collection The collection to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {*} accumulator The initial value.
   * @param {boolean} initAccum Specify using the first or last element of
   *  `collection` as the initial value.
   * @param {Function} eachFunc The function to iterate over `collection`.
   * @returns {*} Returns the accumulated value.
   */
  function baseReduce(collection, iteratee, accumulator, initAccum, eachFunc) {
    eachFunc(collection, function(value, index, collection) {
      accumulator = initAccum
        ? (initAccum = false, value)
        : iteratee(accumulator, value, index, collection);
    });
    return accumulator;
  }

  /**
   * The base implementation of `_.sortBy` which uses `comparer` to define the
   * sort order of `array` and replaces criteria objects with their corresponding
   * values.
   *
   * @private
   * @param {Array} array The array to sort.
   * @param {Function} comparer The function to define sort order.
   * @returns {Array} Returns `array`.
   */
  function baseSortBy(array, comparer) {
    var length = array.length;

    array.sort(comparer);
    while (length--) {
      array[length] = array[length].value;
    }
    return array;
  }

  /**
   * The base implementation of `_.sum` and `_.sumBy` without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {number} Returns the sum.
   */
  function baseSum(array, iteratee) {
    var result,
        index = -1,
        length = array.length;

    while (++index < length) {
      var current = iteratee(array[index]);
      if (current !== undefined) {
        result = result === undefined ? current : (result + current);
      }
    }
    return result;
  }

  /**
   * The base implementation of `_.times` without support for iteratee shorthands
   * or max array length checks.
   *
   * @private
   * @param {number} n The number of times to invoke `iteratee`.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the array of results.
   */
  function baseTimes(n, iteratee) {
    var index = -1,
        result = Array(n);

    while (++index < n) {
      result[index] = iteratee(index);
    }
    return result;
  }

  /**
   * The base implementation of `_.toPairs` and `_.toPairsIn` which creates an array
   * of key-value pairs for `object` corresponding to the property names of `props`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array} props The property names to get values for.
   * @returns {Object} Returns the key-value pairs.
   */
  function baseToPairs(object, props) {
    return arrayMap(props, function(key) {
      return [key, object[key]];
    });
  }

  /**
   * The base implementation of `_.unary` without support for storing metadata.
   *
   * @private
   * @param {Function} func The function to cap arguments for.
   * @returns {Function} Returns the new capped function.
   */
  function baseUnary(func) {
    return function(value) {
      return func(value);
    };
  }

  /**
   * The base implementation of `_.values` and `_.valuesIn` which creates an
   * array of `object` property values corresponding to the property names
   * of `props`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array} props The property names to get values for.
   * @returns {Object} Returns the array of property values.
   */
  function baseValues(object, props) {
    return arrayMap(props, function(key) {
      return object[key];
    });
  }

  /**
   * Checks if a `cache` value for `key` exists.
   *
   * @private
   * @param {Object} cache The cache to query.
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function cacheHas(cache, key) {
    return cache.has(key);
  }

  /**
   * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the first unmatched string symbol.
   */
  function charsStartIndex(strSymbols, chrSymbols) {
    var index = -1,
        length = strSymbols.length;

    while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the last unmatched string symbol.
   */
  function charsEndIndex(strSymbols, chrSymbols) {
    var index = strSymbols.length;

    while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Gets the number of `placeholder` occurrences in `array`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} placeholder The placeholder to search for.
   * @returns {number} Returns the placeholder count.
   */
  function countHolders(array, placeholder) {
    var length = array.length,
        result = 0;

    while (length--) {
      if (array[length] === placeholder) {
        ++result;
      }
    }
    return result;
  }

  /**
   * Used by `_.deburr` to convert Latin-1 Supplement and Latin Extended-A
   * letters to basic Latin letters.
   *
   * @private
   * @param {string} letter The matched letter to deburr.
   * @returns {string} Returns the deburred letter.
   */
  var deburrLetter = basePropertyOf(deburredLetters);

  /**
   * Used by `_.escape` to convert characters to HTML entities.
   *
   * @private
   * @param {string} chr The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  var escapeHtmlChar = basePropertyOf(htmlEscapes);

  /**
   * Used by `_.template` to escape characters for inclusion in compiled string literals.
   *
   * @private
   * @param {string} chr The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  function escapeStringChar(chr) {
    return '\\' + stringEscapes[chr];
  }

  /**
   * Gets the value at `key` of `object`.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function getValue(object, key) {
    return object == null ? undefined : object[key];
  }

  /**
   * Checks if `string` contains Unicode symbols.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {boolean} Returns `true` if a symbol is found, else `false`.
   */
  function hasUnicode(string) {
    return reHasUnicode.test(string);
  }

  /**
   * Checks if `string` contains a word composed of Unicode symbols.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {boolean} Returns `true` if a word is found, else `false`.
   */
  function hasUnicodeWord(string) {
    return reHasUnicodeWord.test(string);
  }

  /**
   * Converts `iterator` to an array.
   *
   * @private
   * @param {Object} iterator The iterator to convert.
   * @returns {Array} Returns the converted array.
   */
  function iteratorToArray(iterator) {
    var data,
        result = [];

    while (!(data = iterator.next()).done) {
      result.push(data.value);
    }
    return result;
  }

  /**
   * Converts `map` to its key-value pairs.
   *
   * @private
   * @param {Object} map The map to convert.
   * @returns {Array} Returns the key-value pairs.
   */
  function mapToArray(map) {
    var index = -1,
        result = Array(map.size);

    map.forEach(function(value, key) {
      result[++index] = [key, value];
    });
    return result;
  }

  /**
   * Creates a unary function that invokes `func` with its argument transformed.
   *
   * @private
   * @param {Function} func The function to wrap.
   * @param {Function} transform The argument transform.
   * @returns {Function} Returns the new function.
   */
  function overArg(func, transform) {
    return function(arg) {
      return func(transform(arg));
    };
  }

  /**
   * Replaces all `placeholder` elements in `array` with an internal placeholder
   * and returns an array of their indexes.
   *
   * @private
   * @param {Array} array The array to modify.
   * @param {*} placeholder The placeholder to replace.
   * @returns {Array} Returns the new array of placeholder indexes.
   */
  function replaceHolders(array, placeholder) {
    var index = -1,
        length = array.length,
        resIndex = 0,
        result = [];

    while (++index < length) {
      var value = array[index];
      if (value === placeholder || value === PLACEHOLDER) {
        array[index] = PLACEHOLDER;
        result[resIndex++] = index;
      }
    }
    return result;
  }

  /**
   * Converts `set` to an array of its values.
   *
   * @private
   * @param {Object} set The set to convert.
   * @returns {Array} Returns the values.
   */
  function setToArray(set) {
    var index = -1,
        result = Array(set.size);

    set.forEach(function(value) {
      result[++index] = value;
    });
    return result;
  }

  /**
   * Converts `set` to its value-value pairs.
   *
   * @private
   * @param {Object} set The set to convert.
   * @returns {Array} Returns the value-value pairs.
   */
  function setToPairs(set) {
    var index = -1,
        result = Array(set.size);

    set.forEach(function(value) {
      result[++index] = [value, value];
    });
    return result;
  }

  /**
   * A specialized version of `_.indexOf` which performs strict equality
   * comparisons of values, i.e. `===`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function strictIndexOf(array, value, fromIndex) {
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * A specialized version of `_.lastIndexOf` which performs strict equality
   * comparisons of values, i.e. `===`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function strictLastIndexOf(array, value, fromIndex) {
    var index = fromIndex + 1;
    while (index--) {
      if (array[index] === value) {
        return index;
      }
    }
    return index;
  }

  /**
   * Gets the number of symbols in `string`.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {number} Returns the string size.
   */
  function stringSize(string) {
    return hasUnicode(string)
      ? unicodeSize(string)
      : asciiSize(string);
  }

  /**
   * Converts `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function stringToArray(string) {
    return hasUnicode(string)
      ? unicodeToArray(string)
      : asciiToArray(string);
  }

  /**
   * Used by `_.unescape` to convert HTML entities to characters.
   *
   * @private
   * @param {string} chr The matched character to unescape.
   * @returns {string} Returns the unescaped character.
   */
  var unescapeHtmlChar = basePropertyOf(htmlUnescapes);

  /**
   * Gets the size of a Unicode `string`.
   *
   * @private
   * @param {string} string The string inspect.
   * @returns {number} Returns the string size.
   */
  function unicodeSize(string) {
    var result = reUnicode.lastIndex = 0;
    while (reUnicode.test(string)) {
      ++result;
    }
    return result;
  }

  /**
   * Converts a Unicode `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function unicodeToArray(string) {
    return string.match(reUnicode) || [];
  }

  /**
   * Splits a Unicode `string` into an array of its words.
   *
   * @private
   * @param {string} The string to inspect.
   * @returns {Array} Returns the words of `string`.
   */
  function unicodeWords(string) {
    return string.match(reUnicodeWord) || [];
  }

  /*--------------------------------------------------------------------------*/

  /**
   * Create a new pristine `lodash` function using the `context` object.
   *
   * @static
   * @memberOf _
   * @since 1.1.0
   * @category Util
   * @param {Object} [context=root] The context object.
   * @returns {Function} Returns a new `lodash` function.
   * @example
   *
   * _.mixin({ 'foo': _.constant('foo') });
   *
   * var lodash = _.runInContext();
   * lodash.mixin({ 'bar': lodash.constant('bar') });
   *
   * _.isFunction(_.foo);
   * // => true
   * _.isFunction(_.bar);
   * // => false
   *
   * lodash.isFunction(lodash.foo);
   * // => false
   * lodash.isFunction(lodash.bar);
   * // => true
   *
   * // Create a suped-up `defer` in Node.js.
   * var defer = _.runInContext({ 'setTimeout': setImmediate }).defer;
   */
  var runInContext = (function runInContext(context) {
    context = context == null ? root : _.defaults(root.Object(), context, _.pick(root, contextProps));

    /** Built-in constructor references. */
    var Array = context.Array,
        Date = context.Date,
        Error = context.Error,
        Function = context.Function,
        Math = context.Math,
        Object = context.Object,
        RegExp = context.RegExp,
        String = context.String,
        TypeError = context.TypeError;

    /** Used for built-in method references. */
    var arrayProto = Array.prototype,
        funcProto = Function.prototype,
        objectProto = Object.prototype;

    /** Used to detect overreaching core-js shims. */
    var coreJsData = context['__core-js_shared__'];

    /** Used to resolve the decompiled source of functions. */
    var funcToString = funcProto.toString;

    /** Used to check objects for own properties. */
    var hasOwnProperty = objectProto.hasOwnProperty;

    /** Used to generate unique IDs. */
    var idCounter = 0;

    /** Used to detect methods masquerading as native. */
    var maskSrcKey = (function() {
      var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
      return uid ? ('Symbol(src)_1.' + uid) : '';
    }());

    /**
     * Used to resolve the
     * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
     * of values.
     */
    var nativeObjectToString = objectProto.toString;

    /** Used to infer the `Object` constructor. */
    var objectCtorString = funcToString.call(Object);

    /** Used to restore the original `_` reference in `_.noConflict`. */
    var oldDash = root._;

    /** Used to detect if a method is native. */
    var reIsNative = RegExp('^' +
      funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
      .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
    );

    /** Built-in value references. */
    var Buffer = moduleExports ? context.Buffer : undefined,
        Symbol = context.Symbol,
        Uint8Array = context.Uint8Array,
        allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined,
        getPrototype = overArg(Object.getPrototypeOf, Object),
        objectCreate = Object.create,
        propertyIsEnumerable = objectProto.propertyIsEnumerable,
        splice = arrayProto.splice,
        spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined,
        symIterator = Symbol ? Symbol.iterator : undefined,
        symToStringTag = Symbol ? Symbol.toStringTag : undefined;

    var defineProperty = (function() {
      try {
        var func = getNative(Object, 'defineProperty');
        func({}, '', {});
        return func;
      } catch (e) {}
    }());

    /** Mocked built-ins. */
    var ctxClearTimeout = context.clearTimeout !== root.clearTimeout && context.clearTimeout,
        ctxNow = Date && Date.now !== root.Date.now && Date.now,
        ctxSetTimeout = context.setTimeout !== root.setTimeout && context.setTimeout;

    /* Built-in method references for those with the same name as other `lodash` methods. */
    var nativeCeil = Math.ceil,
        nativeFloor = Math.floor,
        nativeGetSymbols = Object.getOwnPropertySymbols,
        nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined,
        nativeIsFinite = context.isFinite,
        nativeJoin = arrayProto.join,
        nativeKeys = overArg(Object.keys, Object),
        nativeMax = Math.max,
        nativeMin = Math.min,
        nativeNow = Date.now,
        nativeParseInt = context.parseInt,
        nativeRandom = Math.random,
        nativeReverse = arrayProto.reverse;

    /* Built-in method references that are verified to be native. */
    var DataView = getNative(context, 'DataView'),
        Map = getNative(context, 'Map'),
        Promise = getNative(context, 'Promise'),
        Set = getNative(context, 'Set'),
        WeakMap = getNative(context, 'WeakMap'),
        nativeCreate = getNative(Object, 'create');

    /** Used to store function metadata. */
    var metaMap = WeakMap && new WeakMap;

    /** Used to lookup unminified function names. */
    var realNames = {};

    /** Used to detect maps, sets, and weakmaps. */
    var dataViewCtorString = toSource(DataView),
        mapCtorString = toSource(Map),
        promiseCtorString = toSource(Promise),
        setCtorString = toSource(Set),
        weakMapCtorString = toSource(WeakMap);

    /** Used to convert symbols to primitives and strings. */
    var symbolProto = Symbol ? Symbol.prototype : undefined,
        symbolValueOf = symbolProto ? symbolProto.valueOf : undefined,
        symbolToString = symbolProto ? symbolProto.toString : undefined;

    /*------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object which wraps `value` to enable implicit method
     * chain sequences. Methods that operate on and return arrays, collections,
     * and functions can be chained together. Methods that retrieve a single value
     * or may return a primitive value will automatically end the chain sequence
     * and return the unwrapped value. Otherwise, the value must be unwrapped
     * with `_#value`.
     *
     * Explicit chain sequences, which must be unwrapped with `_#value`, may be
     * enabled using `_.chain`.
     *
     * The execution of chained methods is lazy, that is, it's deferred until
     * `_#value` is implicitly or explicitly called.
     *
     * Lazy evaluation allows several methods to support shortcut fusion.
     * Shortcut fusion is an optimization to merge iteratee calls; this avoids
     * the creation of intermediate arrays and can greatly reduce the number of
     * iteratee executions. Sections of a chain sequence qualify for shortcut
     * fusion if the section is applied to an array and iteratees accept only
     * one argument. The heuristic for whether a section qualifies for shortcut
     * fusion is subject to change.
     *
     * Chaining is supported in custom builds as long as the `_#value` method is
     * directly or indirectly included in the build.
     *
     * In addition to lodash methods, wrappers have `Array` and `String` methods.
     *
     * The wrapper `Array` methods are:
     * `concat`, `join`, `pop`, `push`, `shift`, `sort`, `splice`, and `unshift`
     *
     * The wrapper `String` methods are:
     * `replace` and `split`
     *
     * The wrapper methods that support shortcut fusion are:
     * `at`, `compact`, `drop`, `dropRight`, `dropWhile`, `filter`, `find`,
     * `findLast`, `head`, `initial`, `last`, `map`, `reject`, `reverse`, `slice`,
     * `tail`, `take`, `takeRight`, `takeRightWhile`, `takeWhile`, and `toArray`
     *
     * The chainable wrapper methods are:
     * `after`, `ary`, `assign`, `assignIn`, `assignInWith`, `assignWith`, `at`,
     * `before`, `bind`, `bindAll`, `bindKey`, `castArray`, `chain`, `chunk`,
     * `commit`, `compact`, `concat`, `conforms`, `constant`, `countBy`, `create`,
     * `curry`, `debounce`, `defaults`, `defaultsDeep`, `defer`, `delay`,
     * `difference`, `differenceBy`, `differenceWith`, `drop`, `dropRight`,
     * `dropRightWhile`, `dropWhile`, `extend`, `extendWith`, `fill`, `filter`,
     * `flatMap`, `flatMapDeep`, `flatMapDepth`, `flatten`, `flattenDeep`,
     * `flattenDepth`, `flip`, `flow`, `flowRight`, `fromPairs`, `functions`,
     * `functionsIn`, `groupBy`, `initial`, `intersection`, `intersectionBy`,
     * `intersectionWith`, `invert`, `invertBy`, `invokeMap`, `iteratee`, `keyBy`,
     * `keys`, `keysIn`, `map`, `mapKeys`, `mapValues`, `matches`, `matchesProperty`,
     * `memoize`, `merge`, `mergeWith`, `method`, `methodOf`, `mixin`, `negate`,
     * `nthArg`, `omit`, `omitBy`, `once`, `orderBy`, `over`, `overArgs`,
     * `overEvery`, `overSome`, `partial`, `partialRight`, `partition`, `pick`,
     * `pickBy`, `plant`, `property`, `propertyOf`, `pull`, `pullAll`, `pullAllBy`,
     * `pullAllWith`, `pullAt`, `push`, `range`, `rangeRight`, `rearg`, `reject`,
     * `remove`, `rest`, `reverse`, `sampleSize`, `set`, `setWith`, `shuffle`,
     * `slice`, `sort`, `sortBy`, `splice`, `spread`, `tail`, `take`, `takeRight`,
     * `takeRightWhile`, `takeWhile`, `tap`, `throttle`, `thru`, `toArray`,
     * `toPairs`, `toPairsIn`, `toPath`, `toPlainObject`, `transform`, `unary`,
     * `union`, `unionBy`, `unionWith`, `uniq`, `uniqBy`, `uniqWith`, `unset`,
     * `unshift`, `unzip`, `unzipWith`, `update`, `updateWith`, `values`,
     * `valuesIn`, `without`, `wrap`, `xor`, `xorBy`, `xorWith`, `zip`,
     * `zipObject`, `zipObjectDeep`, and `zipWith`
     *
     * The wrapper methods that are **not** chainable by default are:
     * `add`, `attempt`, `camelCase`, `capitalize`, `ceil`, `clamp`, `clone`,
     * `cloneDeep`, `cloneDeepWith`, `cloneWith`, `conformsTo`, `deburr`,
     * `defaultTo`, `divide`, `each`, `eachRight`, `endsWith`, `eq`, `escape`,
     * `escapeRegExp`, `every`, `find`, `findIndex`, `findKey`, `findLast`,
     * `findLastIndex`, `findLastKey`, `first`, `floor`, `forEach`, `forEachRight`,
     * `forIn`, `forInRight`, `forOwn`, `forOwnRight`, `get`, `gt`, `gte`, `has`,
     * `hasIn`, `head`, `identity`, `includes`, `indexOf`, `inRange`, `invoke`,
     * `isArguments`, `isArray`, `isArrayBuffer`, `isArrayLike`, `isArrayLikeObject`,
     * `isBoolean`, `isBuffer`, `isDate`, `isElement`, `isEmpty`, `isEqual`,
     * `isEqualWith`, `isError`, `isFinite`, `isFunction`, `isInteger`, `isLength`,
     * `isMap`, `isMatch`, `isMatchWith`, `isNaN`, `isNative`, `isNil`, `isNull`,
     * `isNumber`, `isObject`, `isObjectLike`, `isPlainObject`, `isRegExp`,
     * `isSafeInteger`, `isSet`, `isString`, `isUndefined`, `isTypedArray`,
     * `isWeakMap`, `isWeakSet`, `join`, `kebabCase`, `last`, `lastIndexOf`,
     * `lowerCase`, `lowerFirst`, `lt`, `lte`, `max`, `maxBy`, `mean`, `meanBy`,
     * `min`, `minBy`, `multiply`, `noConflict`, `noop`, `now`, `nth`, `pad`,
     * `padEnd`, `padStart`, `parseInt`, `pop`, `random`, `reduce`, `reduceRight`,
     * `repeat`, `result`, `round`, `runInContext`, `sample`, `shift`, `size`,
     * `snakeCase`, `some`, `sortedIndex`, `sortedIndexBy`, `sortedLastIndex`,
     * `sortedLastIndexBy`, `startCase`, `startsWith`, `stubArray`, `stubFalse`,
     * `stubObject`, `stubString`, `stubTrue`, `subtract`, `sum`, `sumBy`,
     * `template`, `times`, `toFinite`, `toInteger`, `toJSON`, `toLength`,
     * `toLower`, `toNumber`, `toSafeInteger`, `toString`, `toUpper`, `trim`,
     * `trimEnd`, `trimStart`, `truncate`, `unescape`, `uniqueId`, `upperCase`,
     * `upperFirst`, `value`, and `words`
     *
     * @name _
     * @constructor
     * @category Seq
     * @param {*} value The value to wrap in a `lodash` instance.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var wrapped = _([1, 2, 3]);
     *
     * // Returns an unwrapped value.
     * wrapped.reduce(_.add);
     * // => 6
     *
     * // Returns a wrapped value.
     * var squares = wrapped.map(square);
     *
     * _.isArray(squares);
     * // => false
     *
     * _.isArray(squares.value());
     * // => true
     */
    function lodash(value) {
      if (isObjectLike(value) && !isArray(value) && !(value instanceof LazyWrapper)) {
        if (value instanceof LodashWrapper) {
          return value;
        }
        if (hasOwnProperty.call(value, '__wrapped__')) {
          return wrapperClone(value);
        }
      }
      return new LodashWrapper(value);
    }

    /**
     * The base implementation of `_.create` without support for assigning
     * properties to the created object.
     *
     * @private
     * @param {Object} proto The object to inherit from.
     * @returns {Object} Returns the new object.
     */
    var baseCreate = (function() {
      function object() {}
      return function(proto) {
        if (!isObject(proto)) {
          return {};
        }
        if (objectCreate) {
          return objectCreate(proto);
        }
        object.prototype = proto;
        var result = new object;
        object.prototype = undefined;
        return result;
      };
    }());

    /**
     * The function whose prototype chain sequence wrappers inherit from.
     *
     * @private
     */
    function baseLodash() {
      // No operation performed.
    }

    /**
     * The base constructor for creating `lodash` wrapper objects.
     *
     * @private
     * @param {*} value The value to wrap.
     * @param {boolean} [chainAll] Enable explicit method chain sequences.
     */
    function LodashWrapper(value, chainAll) {
      this.__wrapped__ = value;
      this.__actions__ = [];
      this.__chain__ = !!chainAll;
      this.__index__ = 0;
      this.__values__ = undefined;
    }

    /**
     * By default, the template delimiters used by lodash are like those in
     * embedded Ruby (ERB) as well as ES2015 template strings. Change the
     * following template settings to use alternative delimiters.
     *
     * @static
     * @memberOf _
     * @type {Object}
     */
    lodash.templateSettings = {

      /**
       * Used to detect `data` property values to be HTML-escaped.
       *
       * @memberOf _.templateSettings
       * @type {RegExp}
       */
      'escape': reEscape,

      /**
       * Used to detect code to be evaluated.
       *
       * @memberOf _.templateSettings
       * @type {RegExp}
       */
      'evaluate': reEvaluate,

      /**
       * Used to detect `data` property values to inject.
       *
       * @memberOf _.templateSettings
       * @type {RegExp}
       */
      'interpolate': reInterpolate,

      /**
       * Used to reference the data object in the template text.
       *
       * @memberOf _.templateSettings
       * @type {string}
       */
      'variable': '',

      /**
       * Used to import variables into the compiled template.
       *
       * @memberOf _.templateSettings
       * @type {Object}
       */
      'imports': {

        /**
         * A reference to the `lodash` function.
         *
         * @memberOf _.templateSettings.imports
         * @type {Function}
         */
        '_': lodash
      }
    };

    // Ensure wrappers are instances of `baseLodash`.
    lodash.prototype = baseLodash.prototype;
    lodash.prototype.constructor = lodash;

    LodashWrapper.prototype = baseCreate(baseLodash.prototype);
    LodashWrapper.prototype.constructor = LodashWrapper;

    /*------------------------------------------------------------------------*/

    /**
     * Creates a lazy wrapper object which wraps `value` to enable lazy evaluation.
     *
     * @private
     * @constructor
     * @param {*} value The value to wrap.
     */
    function LazyWrapper(value) {
      this.__wrapped__ = value;
      this.__actions__ = [];
      this.__dir__ = 1;
      this.__filtered__ = false;
      this.__iteratees__ = [];
      this.__takeCount__ = MAX_ARRAY_LENGTH;
      this.__views__ = [];
    }

    /**
     * Creates a clone of the lazy wrapper object.
     *
     * @private
     * @name clone
     * @memberOf LazyWrapper
     * @returns {Object} Returns the cloned `LazyWrapper` object.
     */
    function lazyClone() {
      var result = new LazyWrapper(this.__wrapped__);
      result.__actions__ = copyArray(this.__actions__);
      result.__dir__ = this.__dir__;
      result.__filtered__ = this.__filtered__;
      result.__iteratees__ = copyArray(this.__iteratees__);
      result.__takeCount__ = this.__takeCount__;
      result.__views__ = copyArray(this.__views__);
      return result;
    }

    /**
     * Reverses the direction of lazy iteration.
     *
     * @private
     * @name reverse
     * @memberOf LazyWrapper
     * @returns {Object} Returns the new reversed `LazyWrapper` object.
     */
    function lazyReverse() {
      if (this.__filtered__) {
        var result = new LazyWrapper(this);
        result.__dir__ = -1;
        result.__filtered__ = true;
      } else {
        result = this.clone();
        result.__dir__ *= -1;
      }
      return result;
    }

    /**
     * Extracts the unwrapped value from its lazy wrapper.
     *
     * @private
     * @name value
     * @memberOf LazyWrapper
     * @returns {*} Returns the unwrapped value.
     */
    function lazyValue() {
      var array = this.__wrapped__.value(),
          dir = this.__dir__,
          isArr = isArray(array),
          isRight = dir < 0,
          arrLength = isArr ? array.length : 0,
          view = getView(0, arrLength, this.__views__),
          start = view.start,
          end = view.end,
          length = end - start,
          index = isRight ? end : (start - 1),
          iteratees = this.__iteratees__,
          iterLength = iteratees.length,
          resIndex = 0,
          takeCount = nativeMin(length, this.__takeCount__);

      if (!isArr || (!isRight && arrLength == length && takeCount == length)) {
        return baseWrapperValue(array, this.__actions__);
      }
      var result = [];

      outer:
      while (length-- && resIndex < takeCount) {
        index += dir;

        var iterIndex = -1,
            value = array[index];

        while (++iterIndex < iterLength) {
          var data = iteratees[iterIndex],
              iteratee = data.iteratee,
              type = data.type,
              computed = iteratee(value);

          if (type == LAZY_MAP_FLAG) {
            value = computed;
          } else if (!computed) {
            if (type == LAZY_FILTER_FLAG) {
              continue outer;
            } else {
              break outer;
            }
          }
        }
        result[resIndex++] = value;
      }
      return result;
    }

    // Ensure `LazyWrapper` is an instance of `baseLodash`.
    LazyWrapper.prototype = baseCreate(baseLodash.prototype);
    LazyWrapper.prototype.constructor = LazyWrapper;

    /*------------------------------------------------------------------------*/

    /**
     * Creates a hash object.
     *
     * @private
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    function Hash(entries) {
      var index = -1,
          length = entries == null ? 0 : entries.length;

      this.clear();
      while (++index < length) {
        var entry = entries[index];
        this.set(entry[0], entry[1]);
      }
    }

    /**
     * Removes all key-value entries from the hash.
     *
     * @private
     * @name clear
     * @memberOf Hash
     */
    function hashClear() {
      this.__data__ = nativeCreate ? nativeCreate(null) : {};
      this.size = 0;
    }

    /**
     * Removes `key` and its value from the hash.
     *
     * @private
     * @name delete
     * @memberOf Hash
     * @param {Object} hash The hash to modify.
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    function hashDelete(key) {
      var result = this.has(key) && delete this.__data__[key];
      this.size -= result ? 1 : 0;
      return result;
    }

    /**
     * Gets the hash value for `key`.
     *
     * @private
     * @name get
     * @memberOf Hash
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    function hashGet(key) {
      var data = this.__data__;
      if (nativeCreate) {
        var result = data[key];
        return result === HASH_UNDEFINED ? undefined : result;
      }
      return hasOwnProperty.call(data, key) ? data[key] : undefined;
    }

    /**
     * Checks if a hash value for `key` exists.
     *
     * @private
     * @name has
     * @memberOf Hash
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    function hashHas(key) {
      var data = this.__data__;
      return nativeCreate ? (data[key] !== undefined) : hasOwnProperty.call(data, key);
    }

    /**
     * Sets the hash `key` to `value`.
     *
     * @private
     * @name set
     * @memberOf Hash
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the hash instance.
     */
    function hashSet(key, value) {
      var data = this.__data__;
      this.size += this.has(key) ? 0 : 1;
      data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED : value;
      return this;
    }

    // Add methods to `Hash`.
    Hash.prototype.clear = hashClear;
    Hash.prototype['delete'] = hashDelete;
    Hash.prototype.get = hashGet;
    Hash.prototype.has = hashHas;
    Hash.prototype.set = hashSet;

    /*------------------------------------------------------------------------*/

    /**
     * Creates an list cache object.
     *
     * @private
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    function ListCache(entries) {
      var index = -1,
          length = entries == null ? 0 : entries.length;

      this.clear();
      while (++index < length) {
        var entry = entries[index];
        this.set(entry[0], entry[1]);
      }
    }

    /**
     * Removes all key-value entries from the list cache.
     *
     * @private
     * @name clear
     * @memberOf ListCache
     */
    function listCacheClear() {
      this.__data__ = [];
      this.size = 0;
    }

    /**
     * Removes `key` and its value from the list cache.
     *
     * @private
     * @name delete
     * @memberOf ListCache
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    function listCacheDelete(key) {
      var data = this.__data__,
          index = assocIndexOf(data, key);

      if (index < 0) {
        return false;
      }
      var lastIndex = data.length - 1;
      if (index == lastIndex) {
        data.pop();
      } else {
        splice.call(data, index, 1);
      }
      --this.size;
      return true;
    }

    /**
     * Gets the list cache value for `key`.
     *
     * @private
     * @name get
     * @memberOf ListCache
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    function listCacheGet(key) {
      var data = this.__data__,
          index = assocIndexOf(data, key);

      return index < 0 ? undefined : data[index][1];
    }

    /**
     * Checks if a list cache value for `key` exists.
     *
     * @private
     * @name has
     * @memberOf ListCache
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    function listCacheHas(key) {
      return assocIndexOf(this.__data__, key) > -1;
    }

    /**
     * Sets the list cache `key` to `value`.
     *
     * @private
     * @name set
     * @memberOf ListCache
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the list cache instance.
     */
    function listCacheSet(key, value) {
      var data = this.__data__,
          index = assocIndexOf(data, key);

      if (index < 0) {
        ++this.size;
        data.push([key, value]);
      } else {
        data[index][1] = value;
      }
      return this;
    }

    // Add methods to `ListCache`.
    ListCache.prototype.clear = listCacheClear;
    ListCache.prototype['delete'] = listCacheDelete;
    ListCache.prototype.get = listCacheGet;
    ListCache.prototype.has = listCacheHas;
    ListCache.prototype.set = listCacheSet;

    /*------------------------------------------------------------------------*/

    /**
     * Creates a map cache object to store key-value pairs.
     *
     * @private
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    function MapCache(entries) {
      var index = -1,
          length = entries == null ? 0 : entries.length;

      this.clear();
      while (++index < length) {
        var entry = entries[index];
        this.set(entry[0], entry[1]);
      }
    }

    /**
     * Removes all key-value entries from the map.
     *
     * @private
     * @name clear
     * @memberOf MapCache
     */
    function mapCacheClear() {
      this.size = 0;
      this.__data__ = {
        'hash': new Hash,
        'map': new (Map || ListCache),
        'string': new Hash
      };
    }

    /**
     * Removes `key` and its value from the map.
     *
     * @private
     * @name delete
     * @memberOf MapCache
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    function mapCacheDelete(key) {
      var result = getMapData(this, key)['delete'](key);
      this.size -= result ? 1 : 0;
      return result;
    }

    /**
     * Gets the map value for `key`.
     *
     * @private
     * @name get
     * @memberOf MapCache
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    function mapCacheGet(key) {
      return getMapData(this, key).get(key);
    }

    /**
     * Checks if a map value for `key` exists.
     *
     * @private
     * @name has
     * @memberOf MapCache
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    function mapCacheHas(key) {
      return getMapData(this, key).has(key);
    }

    /**
     * Sets the map `key` to `value`.
     *
     * @private
     * @name set
     * @memberOf MapCache
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the map cache instance.
     */
    function mapCacheSet(key, value) {
      var data = getMapData(this, key),
          size = data.size;

      data.set(key, value);
      this.size += data.size == size ? 0 : 1;
      return this;
    }

    // Add methods to `MapCache`.
    MapCache.prototype.clear = mapCacheClear;
    MapCache.prototype['delete'] = mapCacheDelete;
    MapCache.prototype.get = mapCacheGet;
    MapCache.prototype.has = mapCacheHas;
    MapCache.prototype.set = mapCacheSet;

    /*------------------------------------------------------------------------*/

    /**
     *
     * Creates an array cache object to store unique values.
     *
     * @private
     * @constructor
     * @param {Array} [values] The values to cache.
     */
    function SetCache(values) {
      var index = -1,
          length = values == null ? 0 : values.length;

      this.__data__ = new MapCache;
      while (++index < length) {
        this.add(values[index]);
      }
    }

    /**
     * Adds `value` to the array cache.
     *
     * @private
     * @name add
     * @memberOf SetCache
     * @alias push
     * @param {*} value The value to cache.
     * @returns {Object} Returns the cache instance.
     */
    function setCacheAdd(value) {
      this.__data__.set(value, HASH_UNDEFINED);
      return this;
    }

    /**
     * Checks if `value` is in the array cache.
     *
     * @private
     * @name has
     * @memberOf SetCache
     * @param {*} value The value to search for.
     * @returns {number} Returns `true` if `value` is found, else `false`.
     */
    function setCacheHas(value) {
      return this.__data__.has(value);
    }

    // Add methods to `SetCache`.
    SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
    SetCache.prototype.has = setCacheHas;

    /*------------------------------------------------------------------------*/

    /**
     * Creates a stack cache object to store key-value pairs.
     *
     * @private
     * @constructor
     * @param {Array} [entries] The key-value pairs to cache.
     */
    function Stack(entries) {
      var data = this.__data__ = new ListCache(entries);
      this.size = data.size;
    }

    /**
     * Removes all key-value entries from the stack.
     *
     * @private
     * @name clear
     * @memberOf Stack
     */
    function stackClear() {
      this.__data__ = new ListCache;
      this.size = 0;
    }

    /**
     * Removes `key` and its value from the stack.
     *
     * @private
     * @name delete
     * @memberOf Stack
     * @param {string} key The key of the value to remove.
     * @returns {boolean} Returns `true` if the entry was removed, else `false`.
     */
    function stackDelete(key) {
      var data = this.__data__,
          result = data['delete'](key);

      this.size = data.size;
      return result;
    }

    /**
     * Gets the stack value for `key`.
     *
     * @private
     * @name get
     * @memberOf Stack
     * @param {string} key The key of the value to get.
     * @returns {*} Returns the entry value.
     */
    function stackGet(key) {
      return this.__data__.get(key);
    }

    /**
     * Checks if a stack value for `key` exists.
     *
     * @private
     * @name has
     * @memberOf Stack
     * @param {string} key The key of the entry to check.
     * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
     */
    function stackHas(key) {
      return this.__data__.has(key);
    }

    /**
     * Sets the stack `key` to `value`.
     *
     * @private
     * @name set
     * @memberOf Stack
     * @param {string} key The key of the value to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns the stack cache instance.
     */
    function stackSet(key, value) {
      var data = this.__data__;
      if (data instanceof ListCache) {
        var pairs = data.__data__;
        if (!Map || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
          pairs.push([key, value]);
          this.size = ++data.size;
          return this;
        }
        data = this.__data__ = new MapCache(pairs);
      }
      data.set(key, value);
      this.size = data.size;
      return this;
    }

    // Add methods to `Stack`.
    Stack.prototype.clear = stackClear;
    Stack.prototype['delete'] = stackDelete;
    Stack.prototype.get = stackGet;
    Stack.prototype.has = stackHas;
    Stack.prototype.set = stackSet;

    /*------------------------------------------------------------------------*/

    /**
     * Creates an array of the enumerable property names of the array-like `value`.
     *
     * @private
     * @param {*} value The value to query.
     * @param {boolean} inherited Specify returning inherited property names.
     * @returns {Array} Returns the array of property names.
     */
    function arrayLikeKeys(value, inherited) {
      var isArr = isArray(value),
          isArg = !isArr && isArguments(value),
          isBuff = !isArr && !isArg && isBuffer(value),
          isType = !isArr && !isArg && !isBuff && isTypedArray(value),
          skipIndexes = isArr || isArg || isBuff || isType,
          result = skipIndexes ? baseTimes(value.length, String) : [],
          length = result.length;

      for (var key in value) {
        if ((inherited || hasOwnProperty.call(value, key)) &&
            !(skipIndexes && (
               // Safari 9 has enumerable `arguments.length` in strict mode.
               key == 'length' ||
               // Node.js 0.10 has enumerable non-index properties on buffers.
               (isBuff && (key == 'offset' || key == 'parent')) ||
               // PhantomJS 2 has enumerable non-index properties on typed arrays.
               (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
               // Skip index properties.
               isIndex(key, length)
            ))) {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * A specialized version of `_.sample` for arrays.
     *
     * @private
     * @param {Array} array The array to sample.
     * @returns {*} Returns the random element.
     */
    function arraySample(array) {
      var length = array.length;
      return length ? array[baseRandom(0, length - 1)] : undefined;
    }

    /**
     * A specialized version of `_.sampleSize` for arrays.
     *
     * @private
     * @param {Array} array The array to sample.
     * @param {number} n The number of elements to sample.
     * @returns {Array} Returns the random elements.
     */
    function arraySampleSize(array, n) {
      return shuffleSelf(copyArray(array), baseClamp(n, 0, array.length));
    }

    /**
     * A specialized version of `_.shuffle` for arrays.
     *
     * @private
     * @param {Array} array The array to shuffle.
     * @returns {Array} Returns the new shuffled array.
     */
    function arrayShuffle(array) {
      return shuffleSelf(copyArray(array));
    }

    /**
     * This function is like `assignValue` except that it doesn't assign
     * `undefined` values.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {string} key The key of the property to assign.
     * @param {*} value The value to assign.
     */
    function assignMergeValue(object, key, value) {
      if ((value !== undefined && !eq(object[key], value)) ||
          (value === undefined && !(key in object))) {
        baseAssignValue(object, key, value);
      }
    }

    /**
     * Assigns `value` to `key` of `object` if the existing value is not equivalent
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {string} key The key of the property to assign.
     * @param {*} value The value to assign.
     */
    function assignValue(object, key, value) {
      var objValue = object[key];
      if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
          (value === undefined && !(key in object))) {
        baseAssignValue(object, key, value);
      }
    }

    /**
     * Gets the index at which the `key` is found in `array` of key-value pairs.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {*} key The key to search for.
     * @returns {number} Returns the index of the matched value, else `-1`.
     */
    function assocIndexOf(array, key) {
      var length = array.length;
      while (length--) {
        if (eq(array[length][0], key)) {
          return length;
        }
      }
      return -1;
    }

    /**
     * Aggregates elements of `collection` on `accumulator` with keys transformed
     * by `iteratee` and values set by `setter`.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} setter The function to set `accumulator` values.
     * @param {Function} iteratee The iteratee to transform keys.
     * @param {Object} accumulator The initial aggregated object.
     * @returns {Function} Returns `accumulator`.
     */
    function baseAggregator(collection, setter, iteratee, accumulator) {
      baseEach(collection, function(value, key, collection) {
        setter(accumulator, value, iteratee(value), collection);
      });
      return accumulator;
    }

    /**
     * The base implementation of `_.assign` without support for multiple sources
     * or `customizer` functions.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @returns {Object} Returns `object`.
     */
    function baseAssign(object, source) {
      return object && copyObject(source, keys(source), object);
    }

    /**
     * The base implementation of `_.assignIn` without support for multiple sources
     * or `customizer` functions.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @returns {Object} Returns `object`.
     */
    function baseAssignIn(object, source) {
      return object && copyObject(source, keysIn(source), object);
    }

    /**
     * The base implementation of `assignValue` and `assignMergeValue` without
     * value checks.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {string} key The key of the property to assign.
     * @param {*} value The value to assign.
     */
    function baseAssignValue(object, key, value) {
      if (key == '__proto__' && defineProperty) {
        defineProperty(object, key, {
          'configurable': true,
          'enumerable': true,
          'value': value,
          'writable': true
        });
      } else {
        object[key] = value;
      }
    }

    /**
     * The base implementation of `_.at` without support for individual paths.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {string[]} paths The property paths to pick.
     * @returns {Array} Returns the picked elements.
     */
    function baseAt(object, paths) {
      var index = -1,
          length = paths.length,
          result = Array(length),
          skip = object == null;

      while (++index < length) {
        result[index] = skip ? undefined : get(object, paths[index]);
      }
      return result;
    }

    /**
     * The base implementation of `_.clamp` which doesn't coerce arguments.
     *
     * @private
     * @param {number} number The number to clamp.
     * @param {number} [lower] The lower bound.
     * @param {number} upper The upper bound.
     * @returns {number} Returns the clamped number.
     */
    function baseClamp(number, lower, upper) {
      if (number === number) {
        if (upper !== undefined) {
          number = number <= upper ? number : upper;
        }
        if (lower !== undefined) {
          number = number >= lower ? number : lower;
        }
      }
      return number;
    }

    /**
     * The base implementation of `_.clone` and `_.cloneDeep` which tracks
     * traversed objects.
     *
     * @private
     * @param {*} value The value to clone.
     * @param {boolean} bitmask The bitmask flags.
     *  1 - Deep clone
     *  2 - Flatten inherited properties
     *  4 - Clone symbols
     * @param {Function} [customizer] The function to customize cloning.
     * @param {string} [key] The key of `value`.
     * @param {Object} [object] The parent object of `value`.
     * @param {Object} [stack] Tracks traversed objects and their clone counterparts.
     * @returns {*} Returns the cloned value.
     */
    function baseClone(value, bitmask, customizer, key, object, stack) {
      var result,
          isDeep = bitmask & CLONE_DEEP_FLAG,
          isFlat = bitmask & CLONE_FLAT_FLAG,
          isFull = bitmask & CLONE_SYMBOLS_FLAG;

      if (customizer) {
        result = object ? customizer(value, key, object, stack) : customizer(value);
      }
      if (result !== undefined) {
        return result;
      }
      if (!isObject(value)) {
        return value;
      }
      var isArr = isArray(value);
      if (isArr) {
        result = initCloneArray(value);
        if (!isDeep) {
          return copyArray(value, result);
        }
      } else {
        var tag = getTag(value),
            isFunc = tag == funcTag || tag == genTag;

        if (isBuffer(value)) {
          return cloneBuffer(value, isDeep);
        }
        if (tag == objectTag || tag == argsTag || (isFunc && !object)) {
          result = (isFlat || isFunc) ? {} : initCloneObject(value);
          if (!isDeep) {
            return isFlat
              ? copySymbolsIn(value, baseAssignIn(result, value))
              : copySymbols(value, baseAssign(result, value));
          }
        } else {
          if (!cloneableTags[tag]) {
            return object ? value : {};
          }
          result = initCloneByTag(value, tag, baseClone, isDeep);
        }
      }
      // Check for circular references and return its corresponding clone.
      stack || (stack = new Stack);
      var stacked = stack.get(value);
      if (stacked) {
        return stacked;
      }
      stack.set(value, result);

      var keysFunc = isFull
        ? (isFlat ? getAllKeysIn : getAllKeys)
        : (isFlat ? keysIn : keys);

      var props = isArr ? undefined : keysFunc(value);
      arrayEach(props || value, function(subValue, key) {
        if (props) {
          key = subValue;
          subValue = value[key];
        }
        // Recursively populate clone (susceptible to call stack limits).
        assignValue(result, key, baseClone(subValue, bitmask, customizer, key, value, stack));
      });
      return result;
    }

    /**
     * The base implementation of `_.conforms` which doesn't clone `source`.
     *
     * @private
     * @param {Object} source The object of property predicates to conform to.
     * @returns {Function} Returns the new spec function.
     */
    function baseConforms(source) {
      var props = keys(source);
      return function(object) {
        return baseConformsTo(object, source, props);
      };
    }

    /**
     * The base implementation of `_.conformsTo` which accepts `props` to check.
     *
     * @private
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property predicates to conform to.
     * @returns {boolean} Returns `true` if `object` conforms, else `false`.
     */
    function baseConformsTo(object, source, props) {
      var length = props.length;
      if (object == null) {
        return !length;
      }
      object = Object(object);
      while (length--) {
        var key = props[length],
            predicate = source[key],
            value = object[key];

        if ((value === undefined && !(key in object)) || !predicate(value)) {
          return false;
        }
      }
      return true;
    }

    /**
     * The base implementation of `_.delay` and `_.defer` which accepts `args`
     * to provide to `func`.
     *
     * @private
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay invocation.
     * @param {Array} args The arguments to provide to `func`.
     * @returns {number|Object} Returns the timer id or timeout object.
     */
    function baseDelay(func, wait, args) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      return setTimeout(function() { func.apply(undefined, args); }, wait);
    }

    /**
     * The base implementation of methods like `_.difference` without support
     * for excluding multiple arrays or iteratee shorthands.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {Array} values The values to exclude.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of filtered values.
     */
    function baseDifference(array, values, iteratee, comparator) {
      var index = -1,
          includes = arrayIncludes,
          isCommon = true,
          length = array.length,
          result = [],
          valuesLength = values.length;

      if (!length) {
        return result;
      }
      if (iteratee) {
        values = arrayMap(values, baseUnary(iteratee));
      }
      if (comparator) {
        includes = arrayIncludesWith;
        isCommon = false;
      }
      else if (values.length >= LARGE_ARRAY_SIZE) {
        includes = cacheHas;
        isCommon = false;
        values = new SetCache(values);
      }
      outer:
      while (++index < length) {
        var value = array[index],
            computed = iteratee == null ? value : iteratee(value);

        value = (comparator || value !== 0) ? value : 0;
        if (isCommon && computed === computed) {
          var valuesIndex = valuesLength;
          while (valuesIndex--) {
            if (values[valuesIndex] === computed) {
              continue outer;
            }
          }
          result.push(value);
        }
        else if (!includes(values, computed, comparator)) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.forEach` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array|Object} Returns `collection`.
     */
    var baseEach = createBaseEach(baseForOwn);

    /**
     * The base implementation of `_.forEachRight` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array|Object} Returns `collection`.
     */
    var baseEachRight = createBaseEach(baseForOwnRight, true);

    /**
     * The base implementation of `_.every` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if all elements pass the predicate check,
     *  else `false`
     */
    function baseEvery(collection, predicate) {
      var result = true;
      baseEach(collection, function(value, index, collection) {
        result = !!predicate(value, index, collection);
        return result;
      });
      return result;
    }

    /**
     * The base implementation of methods like `_.max` and `_.min` which accepts a
     * `comparator` to determine the extremum value.
     *
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} iteratee The iteratee invoked per iteration.
     * @param {Function} comparator The comparator used to compare values.
     * @returns {*} Returns the extremum value.
     */
    function baseExtremum(array, iteratee, comparator) {
      var index = -1,
          length = array.length;

      while (++index < length) {
        var value = array[index],
            current = iteratee(value);

        if (current != null && (computed === undefined
              ? (current === current && !isSymbol(current))
              : comparator(current, computed)
            )) {
          var computed = current,
              result = value;
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.fill` without an iteratee call guard.
     *
     * @private
     * @param {Array} array The array to fill.
     * @param {*} value The value to fill `array` with.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns `array`.
     */
    function baseFill(array, value, start, end) {
      var length = array.length;

      start = toInteger(start);
      if (start < 0) {
        start = -start > length ? 0 : (length + start);
      }
      end = (end === undefined || end > length) ? length : toInteger(end);
      if (end < 0) {
        end += length;
      }
      end = start > end ? 0 : toLength(end);
      while (start < end) {
        array[start++] = value;
      }
      return array;
    }

    /**
     * The base implementation of `_.filter` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {Array} Returns the new filtered array.
     */
    function baseFilter(collection, predicate) {
      var result = [];
      baseEach(collection, function(value, index, collection) {
        if (predicate(value, index, collection)) {
          result.push(value);
        }
      });
      return result;
    }

    /**
     * The base implementation of `_.flatten` with support for restricting flattening.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {number} depth The maximum recursion depth.
     * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
     * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
     * @param {Array} [result=[]] The initial result value.
     * @returns {Array} Returns the new flattened array.
     */
    function baseFlatten(array, depth, predicate, isStrict, result) {
      var index = -1,
          length = array.length;

      predicate || (predicate = isFlattenable);
      result || (result = []);

      while (++index < length) {
        var value = array[index];
        if (depth > 0 && predicate(value)) {
          if (depth > 1) {
            // Recursively flatten arrays (susceptible to call stack limits).
            baseFlatten(value, depth - 1, predicate, isStrict, result);
          } else {
            arrayPush(result, value);
          }
        } else if (!isStrict) {
          result[result.length] = value;
        }
      }
      return result;
    }

    /**
     * The base implementation of `baseForOwn` which iterates over `object`
     * properties returned by `keysFunc` and invokes `iteratee` for each property.
     * Iteratee functions may exit iteration early by explicitly returning `false`.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} keysFunc The function to get the keys of `object`.
     * @returns {Object} Returns `object`.
     */
    var baseFor = createBaseFor();

    /**
     * This function is like `baseFor` except that it iterates over properties
     * in the opposite order.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @param {Function} keysFunc The function to get the keys of `object`.
     * @returns {Object} Returns `object`.
     */
    var baseForRight = createBaseFor(true);

    /**
     * The base implementation of `_.forOwn` without support for iteratee shorthands.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Object} Returns `object`.
     */
    function baseForOwn(object, iteratee) {
      return object && baseFor(object, iteratee, keys);
    }

    /**
     * The base implementation of `_.forOwnRight` without support for iteratee shorthands.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Object} Returns `object`.
     */
    function baseForOwnRight(object, iteratee) {
      return object && baseForRight(object, iteratee, keys);
    }

    /**
     * The base implementation of `_.functions` which creates an array of
     * `object` function property names filtered from `props`.
     *
     * @private
     * @param {Object} object The object to inspect.
     * @param {Array} props The property names to filter.
     * @returns {Array} Returns the function names.
     */
    function baseFunctions(object, props) {
      return arrayFilter(props, function(key) {
        return isFunction(object[key]);
      });
    }

    /**
     * The base implementation of `_.get` without support for default values.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to get.
     * @returns {*} Returns the resolved value.
     */
    function baseGet(object, path) {
      path = castPath(path, object);

      var index = 0,
          length = path.length;

      while (object != null && index < length) {
        object = object[toKey(path[index++])];
      }
      return (index && index == length) ? object : undefined;
    }

    /**
     * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
     * `keysFunc` and `symbolsFunc` to get the enumerable property names and
     * symbols of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Function} keysFunc The function to get the keys of `object`.
     * @param {Function} symbolsFunc The function to get the symbols of `object`.
     * @returns {Array} Returns the array of property names and symbols.
     */
    function baseGetAllKeys(object, keysFunc, symbolsFunc) {
      var result = keysFunc(object);
      return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
    }

    /**
     * The base implementation of `getTag` without fallbacks for buggy environments.
     *
     * @private
     * @param {*} value The value to query.
     * @returns {string} Returns the `toStringTag`.
     */
    function baseGetTag(value) {
      if (value == null) {
        return value === undefined ? undefinedTag : nullTag;
      }
      return (symToStringTag && symToStringTag in Object(value))
        ? getRawTag(value)
        : objectToString(value);
    }

    /**
     * The base implementation of `_.gt` which doesn't coerce arguments.
     *
     * @private
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is greater than `other`,
     *  else `false`.
     */
    function baseGt(value, other) {
      return value > other;
    }

    /**
     * The base implementation of `_.has` without support for deep paths.
     *
     * @private
     * @param {Object} [object] The object to query.
     * @param {Array|string} key The key to check.
     * @returns {boolean} Returns `true` if `key` exists, else `false`.
     */
    function baseHas(object, key) {
      return object != null && hasOwnProperty.call(object, key);
    }

    /**
     * The base implementation of `_.hasIn` without support for deep paths.
     *
     * @private
     * @param {Object} [object] The object to query.
     * @param {Array|string} key The key to check.
     * @returns {boolean} Returns `true` if `key` exists, else `false`.
     */
    function baseHasIn(object, key) {
      return object != null && key in Object(object);
    }

    /**
     * The base implementation of `_.inRange` which doesn't coerce arguments.
     *
     * @private
     * @param {number} number The number to check.
     * @param {number} start The start of the range.
     * @param {number} end The end of the range.
     * @returns {boolean} Returns `true` if `number` is in the range, else `false`.
     */
    function baseInRange(number, start, end) {
      return number >= nativeMin(start, end) && number < nativeMax(start, end);
    }

    /**
     * The base implementation of methods like `_.intersection`, without support
     * for iteratee shorthands, that accepts an array of arrays to inspect.
     *
     * @private
     * @param {Array} arrays The arrays to inspect.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of shared values.
     */
    function baseIntersection(arrays, iteratee, comparator) {
      var includes = comparator ? arrayIncludesWith : arrayIncludes,
          length = arrays[0].length,
          othLength = arrays.length,
          othIndex = othLength,
          caches = Array(othLength),
          maxLength = Infinity,
          result = [];

      while (othIndex--) {
        var array = arrays[othIndex];
        if (othIndex && iteratee) {
          array = arrayMap(array, baseUnary(iteratee));
        }
        maxLength = nativeMin(array.length, maxLength);
        caches[othIndex] = !comparator && (iteratee || (length >= 120 && array.length >= 120))
          ? new SetCache(othIndex && array)
          : undefined;
      }
      array = arrays[0];

      var index = -1,
          seen = caches[0];

      outer:
      while (++index < length && result.length < maxLength) {
        var value = array[index],
            computed = iteratee ? iteratee(value) : value;

        value = (comparator || value !== 0) ? value : 0;
        if (!(seen
              ? cacheHas(seen, computed)
              : includes(result, computed, comparator)
            )) {
          othIndex = othLength;
          while (--othIndex) {
            var cache = caches[othIndex];
            if (!(cache
                  ? cacheHas(cache, computed)
                  : includes(arrays[othIndex], computed, comparator))
                ) {
              continue outer;
            }
          }
          if (seen) {
            seen.push(computed);
          }
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.invert` and `_.invertBy` which inverts
     * `object` with values transformed by `iteratee` and set by `setter`.
     *
     * @private
     * @param {Object} object The object to iterate over.
     * @param {Function} setter The function to set `accumulator` values.
     * @param {Function} iteratee The iteratee to transform values.
     * @param {Object} accumulator The initial inverted object.
     * @returns {Function} Returns `accumulator`.
     */
    function baseInverter(object, setter, iteratee, accumulator) {
      baseForOwn(object, function(value, key, object) {
        setter(accumulator, iteratee(value), key, object);
      });
      return accumulator;
    }

    /**
     * The base implementation of `_.invoke` without support for individual
     * method arguments.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the method to invoke.
     * @param {Array} args The arguments to invoke the method with.
     * @returns {*} Returns the result of the invoked method.
     */
    function baseInvoke(object, path, args) {
      path = castPath(path, object);
      object = parent(object, path);
      var func = object == null ? object : object[toKey(last(path))];
      return func == null ? undefined : apply(func, object, args);
    }

    /**
     * The base implementation of `_.isArguments`.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an `arguments` object,
     */
    function baseIsArguments(value) {
      return isObjectLike(value) && baseGetTag(value) == argsTag;
    }

    /**
     * The base implementation of `_.isArrayBuffer` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an array buffer, else `false`.
     */
    function baseIsArrayBuffer(value) {
      return isObjectLike(value) && baseGetTag(value) == arrayBufferTag;
    }

    /**
     * The base implementation of `_.isDate` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a date object, else `false`.
     */
    function baseIsDate(value) {
      return isObjectLike(value) && baseGetTag(value) == dateTag;
    }

    /**
     * The base implementation of `_.isEqual` which supports partial comparisons
     * and tracks traversed objects.
     *
     * @private
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @param {boolean} bitmask The bitmask flags.
     *  1 - Unordered comparison
     *  2 - Partial comparison
     * @param {Function} [customizer] The function to customize comparisons.
     * @param {Object} [stack] Tracks traversed `value` and `other` objects.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     */
    function baseIsEqual(value, other, bitmask, customizer, stack) {
      if (value === other) {
        return true;
      }
      if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
        return value !== value && other !== other;
      }
      return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
    }

    /**
     * A specialized version of `baseIsEqual` for arrays and objects which performs
     * deep comparisons and tracks traversed objects enabling objects with circular
     * references to be compared.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
     * @param {Function} customizer The function to customize comparisons.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Object} [stack] Tracks traversed `object` and `other` objects.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
      var objIsArr = isArray(object),
          othIsArr = isArray(other),
          objTag = objIsArr ? arrayTag : getTag(object),
          othTag = othIsArr ? arrayTag : getTag(other);

      objTag = objTag == argsTag ? objectTag : objTag;
      othTag = othTag == argsTag ? objectTag : othTag;

      var objIsObj = objTag == objectTag,
          othIsObj = othTag == objectTag,
          isSameTag = objTag == othTag;

      if (isSameTag && isBuffer(object)) {
        if (!isBuffer(other)) {
          return false;
        }
        objIsArr = true;
        objIsObj = false;
      }
      if (isSameTag && !objIsObj) {
        stack || (stack = new Stack);
        return (objIsArr || isTypedArray(object))
          ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
          : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
      }
      if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
        var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
            othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

        if (objIsWrapped || othIsWrapped) {
          var objUnwrapped = objIsWrapped ? object.value() : object,
              othUnwrapped = othIsWrapped ? other.value() : other;

          stack || (stack = new Stack);
          return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
        }
      }
      if (!isSameTag) {
        return false;
      }
      stack || (stack = new Stack);
      return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
    }

    /**
     * The base implementation of `_.isMap` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a map, else `false`.
     */
    function baseIsMap(value) {
      return isObjectLike(value) && getTag(value) == mapTag;
    }

    /**
     * The base implementation of `_.isMatch` without support for iteratee shorthands.
     *
     * @private
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property values to match.
     * @param {Array} matchData The property names, values, and compare flags to match.
     * @param {Function} [customizer] The function to customize comparisons.
     * @returns {boolean} Returns `true` if `object` is a match, else `false`.
     */
    function baseIsMatch(object, source, matchData, customizer) {
      var index = matchData.length,
          length = index,
          noCustomizer = !customizer;

      if (object == null) {
        return !length;
      }
      object = Object(object);
      while (index--) {
        var data = matchData[index];
        if ((noCustomizer && data[2])
              ? data[1] !== object[data[0]]
              : !(data[0] in object)
            ) {
          return false;
        }
      }
      while (++index < length) {
        data = matchData[index];
        var key = data[0],
            objValue = object[key],
            srcValue = data[1];

        if (noCustomizer && data[2]) {
          if (objValue === undefined && !(key in object)) {
            return false;
          }
        } else {
          var stack = new Stack;
          if (customizer) {
            var result = customizer(objValue, srcValue, key, object, source, stack);
          }
          if (!(result === undefined
                ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack)
                : result
              )) {
            return false;
          }
        }
      }
      return true;
    }

    /**
     * The base implementation of `_.isNative` without bad shim checks.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a native function,
     *  else `false`.
     */
    function baseIsNative(value) {
      if (!isObject(value) || isMasked(value)) {
        return false;
      }
      var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
      return pattern.test(toSource(value));
    }

    /**
     * The base implementation of `_.isRegExp` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a regexp, else `false`.
     */
    function baseIsRegExp(value) {
      return isObjectLike(value) && baseGetTag(value) == regexpTag;
    }

    /**
     * The base implementation of `_.isSet` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a set, else `false`.
     */
    function baseIsSet(value) {
      return isObjectLike(value) && getTag(value) == setTag;
    }

    /**
     * The base implementation of `_.isTypedArray` without Node.js optimizations.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
     */
    function baseIsTypedArray(value) {
      return isObjectLike(value) &&
        isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
    }

    /**
     * The base implementation of `_.iteratee`.
     *
     * @private
     * @param {*} [value=_.identity] The value to convert to an iteratee.
     * @returns {Function} Returns the iteratee.
     */
    function baseIteratee(value) {
      // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
      // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
      if (typeof value == 'function') {
        return value;
      }
      if (value == null) {
        return identity;
      }
      if (typeof value == 'object') {
        return isArray(value)
          ? baseMatchesProperty(value[0], value[1])
          : baseMatches(value);
      }
      return property(value);
    }

    /**
     * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     */
    function baseKeys(object) {
      if (!isPrototype(object)) {
        return nativeKeys(object);
      }
      var result = [];
      for (var key in Object(object)) {
        if (hasOwnProperty.call(object, key) && key != 'constructor') {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     */
    function baseKeysIn(object) {
      if (!isObject(object)) {
        return nativeKeysIn(object);
      }
      var isProto = isPrototype(object),
          result = [];

      for (var key in object) {
        if (!(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.lt` which doesn't coerce arguments.
     *
     * @private
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is less than `other`,
     *  else `false`.
     */
    function baseLt(value, other) {
      return value < other;
    }

    /**
     * The base implementation of `_.map` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} iteratee The function invoked per iteration.
     * @returns {Array} Returns the new mapped array.
     */
    function baseMap(collection, iteratee) {
      var index = -1,
          result = isArrayLike(collection) ? Array(collection.length) : [];

      baseEach(collection, function(value, key, collection) {
        result[++index] = iteratee(value, key, collection);
      });
      return result;
    }

    /**
     * The base implementation of `_.matches` which doesn't clone `source`.
     *
     * @private
     * @param {Object} source The object of property values to match.
     * @returns {Function} Returns the new spec function.
     */
    function baseMatches(source) {
      var matchData = getMatchData(source);
      if (matchData.length == 1 && matchData[0][2]) {
        return matchesStrictComparable(matchData[0][0], matchData[0][1]);
      }
      return function(object) {
        return object === source || baseIsMatch(object, source, matchData);
      };
    }

    /**
     * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
     *
     * @private
     * @param {string} path The path of the property to get.
     * @param {*} srcValue The value to match.
     * @returns {Function} Returns the new spec function.
     */
    function baseMatchesProperty(path, srcValue) {
      if (isKey(path) && isStrictComparable(srcValue)) {
        return matchesStrictComparable(toKey(path), srcValue);
      }
      return function(object) {
        var objValue = get(object, path);
        return (objValue === undefined && objValue === srcValue)
          ? hasIn(object, path)
          : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
      };
    }

    /**
     * The base implementation of `_.merge` without support for multiple sources.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {number} srcIndex The index of `source`.
     * @param {Function} [customizer] The function to customize merged values.
     * @param {Object} [stack] Tracks traversed source values and their merged
     *  counterparts.
     */
    function baseMerge(object, source, srcIndex, customizer, stack) {
      if (object === source) {
        return;
      }
      baseFor(source, function(srcValue, key) {
        if (isObject(srcValue)) {
          stack || (stack = new Stack);
          baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
        }
        else {
          var newValue = customizer
            ? customizer(object[key], srcValue, (key + ''), object, source, stack)
            : undefined;

          if (newValue === undefined) {
            newValue = srcValue;
          }
          assignMergeValue(object, key, newValue);
        }
      }, keysIn);
    }

    /**
     * A specialized version of `baseMerge` for arrays and objects which performs
     * deep merges and tracks traversed objects enabling objects with circular
     * references to be merged.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {string} key The key of the value to merge.
     * @param {number} srcIndex The index of `source`.
     * @param {Function} mergeFunc The function to merge values.
     * @param {Function} [customizer] The function to customize assigned values.
     * @param {Object} [stack] Tracks traversed source values and their merged
     *  counterparts.
     */
    function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
      var objValue = object[key],
          srcValue = source[key],
          stacked = stack.get(srcValue);

      if (stacked) {
        assignMergeValue(object, key, stacked);
        return;
      }
      var newValue = customizer
        ? customizer(objValue, srcValue, (key + ''), object, source, stack)
        : undefined;

      var isCommon = newValue === undefined;

      if (isCommon) {
        var isArr = isArray(srcValue),
            isBuff = !isArr && isBuffer(srcValue),
            isTyped = !isArr && !isBuff && isTypedArray(srcValue);

        newValue = srcValue;
        if (isArr || isBuff || isTyped) {
          if (isArray(objValue)) {
            newValue = objValue;
          }
          else if (isArrayLikeObject(objValue)) {
            newValue = copyArray(objValue);
          }
          else if (isBuff) {
            isCommon = false;
            newValue = cloneBuffer(srcValue, true);
          }
          else if (isTyped) {
            isCommon = false;
            newValue = cloneTypedArray(srcValue, true);
          }
          else {
            newValue = [];
          }
        }
        else if (isPlainObject(srcValue) || isArguments(srcValue)) {
          newValue = objValue;
          if (isArguments(objValue)) {
            newValue = toPlainObject(objValue);
          }
          else if (!isObject(objValue) || (srcIndex && isFunction(objValue))) {
            newValue = initCloneObject(srcValue);
          }
        }
        else {
          isCommon = false;
        }
      }
      if (isCommon) {
        // Recursively merge objects and arrays (susceptible to call stack limits).
        stack.set(srcValue, newValue);
        mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
        stack['delete'](srcValue);
      }
      assignMergeValue(object, key, newValue);
    }

    /**
     * The base implementation of `_.nth` which doesn't coerce arguments.
     *
     * @private
     * @param {Array} array The array to query.
     * @param {number} n The index of the element to return.
     * @returns {*} Returns the nth element of `array`.
     */
    function baseNth(array, n) {
      var length = array.length;
      if (!length) {
        return;
      }
      n += n < 0 ? length : 0;
      return isIndex(n, length) ? array[n] : undefined;
    }

    /**
     * The base implementation of `_.orderBy` without param guards.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function[]|Object[]|string[]} iteratees The iteratees to sort by.
     * @param {string[]} orders The sort orders of `iteratees`.
     * @returns {Array} Returns the new sorted array.
     */
    function baseOrderBy(collection, iteratees, orders) {
      var index = -1;
      iteratees = arrayMap(iteratees.length ? iteratees : [identity], baseUnary(getIteratee()));

      var result = baseMap(collection, function(value, key, collection) {
        var criteria = arrayMap(iteratees, function(iteratee) {
          return iteratee(value);
        });
        return { 'criteria': criteria, 'index': ++index, 'value': value };
      });

      return baseSortBy(result, function(object, other) {
        return compareMultiple(object, other, orders);
      });
    }

    /**
     * The base implementation of `_.pick` without support for individual
     * property identifiers.
     *
     * @private
     * @param {Object} object The source object.
     * @param {string[]} paths The property paths to pick.
     * @returns {Object} Returns the new object.
     */
    function basePick(object, paths) {
      return basePickBy(object, paths, function(value, path) {
        return hasIn(object, path);
      });
    }

    /**
     * The base implementation of  `_.pickBy` without support for iteratee shorthands.
     *
     * @private
     * @param {Object} object The source object.
     * @param {string[]} paths The property paths to pick.
     * @param {Function} predicate The function invoked per property.
     * @returns {Object} Returns the new object.
     */
    function basePickBy(object, paths, predicate) {
      var index = -1,
          length = paths.length,
          result = {};

      while (++index < length) {
        var path = paths[index],
            value = baseGet(object, path);

        if (predicate(value, path)) {
          baseSet(result, castPath(path, object), value);
        }
      }
      return result;
    }

    /**
     * A specialized version of `baseProperty` which supports deep paths.
     *
     * @private
     * @param {Array|string} path The path of the property to get.
     * @returns {Function} Returns the new accessor function.
     */
    function basePropertyDeep(path) {
      return function(object) {
        return baseGet(object, path);
      };
    }

    /**
     * The base implementation of `_.pullAllBy` without support for iteratee
     * shorthands.
     *
     * @private
     * @param {Array} array The array to modify.
     * @param {Array} values The values to remove.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns `array`.
     */
    function basePullAll(array, values, iteratee, comparator) {
      var indexOf = comparator ? baseIndexOfWith : baseIndexOf,
          index = -1,
          length = values.length,
          seen = array;

      if (array === values) {
        values = copyArray(values);
      }
      if (iteratee) {
        seen = arrayMap(array, baseUnary(iteratee));
      }
      while (++index < length) {
        var fromIndex = 0,
            value = values[index],
            computed = iteratee ? iteratee(value) : value;

        while ((fromIndex = indexOf(seen, computed, fromIndex, comparator)) > -1) {
          if (seen !== array) {
            splice.call(seen, fromIndex, 1);
          }
          splice.call(array, fromIndex, 1);
        }
      }
      return array;
    }

    /**
     * The base implementation of `_.pullAt` without support for individual
     * indexes or capturing the removed elements.
     *
     * @private
     * @param {Array} array The array to modify.
     * @param {number[]} indexes The indexes of elements to remove.
     * @returns {Array} Returns `array`.
     */
    function basePullAt(array, indexes) {
      var length = array ? indexes.length : 0,
          lastIndex = length - 1;

      while (length--) {
        var index = indexes[length];
        if (length == lastIndex || index !== previous) {
          var previous = index;
          if (isIndex(index)) {
            splice.call(array, index, 1);
          } else {
            baseUnset(array, index);
          }
        }
      }
      return array;
    }

    /**
     * The base implementation of `_.random` without support for returning
     * floating-point numbers.
     *
     * @private
     * @param {number} lower The lower bound.
     * @param {number} upper The upper bound.
     * @returns {number} Returns the random number.
     */
    function baseRandom(lower, upper) {
      return lower + nativeFloor(nativeRandom() * (upper - lower + 1));
    }

    /**
     * The base implementation of `_.range` and `_.rangeRight` which doesn't
     * coerce arguments.
     *
     * @private
     * @param {number} start The start of the range.
     * @param {number} end The end of the range.
     * @param {number} step The value to increment or decrement by.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Array} Returns the range of numbers.
     */
    function baseRange(start, end, step, fromRight) {
      var index = -1,
          length = nativeMax(nativeCeil((end - start) / (step || 1)), 0),
          result = Array(length);

      while (length--) {
        result[fromRight ? length : ++index] = start;
        start += step;
      }
      return result;
    }

    /**
     * The base implementation of `_.repeat` which doesn't coerce arguments.
     *
     * @private
     * @param {string} string The string to repeat.
     * @param {number} n The number of times to repeat the string.
     * @returns {string} Returns the repeated string.
     */
    function baseRepeat(string, n) {
      var result = '';
      if (!string || n < 1 || n > MAX_SAFE_INTEGER) {
        return result;
      }
      // Leverage the exponentiation by squaring algorithm for a faster repeat.
      // See https://en.wikipedia.org/wiki/Exponentiation_by_squaring for more details.
      do {
        if (n % 2) {
          result += string;
        }
        n = nativeFloor(n / 2);
        if (n) {
          string += string;
        }
      } while (n);

      return result;
    }

    /**
     * The base implementation of `_.rest` which doesn't validate or coerce arguments.
     *
     * @private
     * @param {Function} func The function to apply a rest parameter to.
     * @param {number} [start=func.length-1] The start position of the rest parameter.
     * @returns {Function} Returns the new function.
     */
    function baseRest(func, start) {
      return setToString(overRest(func, start, identity), func + '');
    }

    /**
     * The base implementation of `_.sample`.
     *
     * @private
     * @param {Array|Object} collection The collection to sample.
     * @returns {*} Returns the random element.
     */
    function baseSample(collection) {
      return arraySample(values(collection));
    }

    /**
     * The base implementation of `_.sampleSize` without param guards.
     *
     * @private
     * @param {Array|Object} collection The collection to sample.
     * @param {number} n The number of elements to sample.
     * @returns {Array} Returns the random elements.
     */
    function baseSampleSize(collection, n) {
      var array = values(collection);
      return shuffleSelf(array, baseClamp(n, 0, array.length));
    }

    /**
     * The base implementation of `_.set`.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to set.
     * @param {*} value The value to set.
     * @param {Function} [customizer] The function to customize path creation.
     * @returns {Object} Returns `object`.
     */
    function baseSet(object, path, value, customizer) {
      if (!isObject(object)) {
        return object;
      }
      path = castPath(path, object);

      var index = -1,
          length = path.length,
          lastIndex = length - 1,
          nested = object;

      while (nested != null && ++index < length) {
        var key = toKey(path[index]),
            newValue = value;

        if (index != lastIndex) {
          var objValue = nested[key];
          newValue = customizer ? customizer(objValue, key, nested) : undefined;
          if (newValue === undefined) {
            newValue = isObject(objValue)
              ? objValue
              : (isIndex(path[index + 1]) ? [] : {});
          }
        }
        assignValue(nested, key, newValue);
        nested = nested[key];
      }
      return object;
    }

    /**
     * The base implementation of `setData` without support for hot loop shorting.
     *
     * @private
     * @param {Function} func The function to associate metadata with.
     * @param {*} data The metadata.
     * @returns {Function} Returns `func`.
     */
    var baseSetData = !metaMap ? identity : function(func, data) {
      metaMap.set(func, data);
      return func;
    };

    /**
     * The base implementation of `setToString` without support for hot loop shorting.
     *
     * @private
     * @param {Function} func The function to modify.
     * @param {Function} string The `toString` result.
     * @returns {Function} Returns `func`.
     */
    var baseSetToString = !defineProperty ? identity : function(func, string) {
      return defineProperty(func, 'toString', {
        'configurable': true,
        'enumerable': false,
        'value': constant(string),
        'writable': true
      });
    };

    /**
     * The base implementation of `_.shuffle`.
     *
     * @private
     * @param {Array|Object} collection The collection to shuffle.
     * @returns {Array} Returns the new shuffled array.
     */
    function baseShuffle(collection) {
      return shuffleSelf(values(collection));
    }

    /**
     * The base implementation of `_.slice` without an iteratee call guard.
     *
     * @private
     * @param {Array} array The array to slice.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns the slice of `array`.
     */
    function baseSlice(array, start, end) {
      var index = -1,
          length = array.length;

      if (start < 0) {
        start = -start > length ? 0 : (length + start);
      }
      end = end > length ? length : end;
      if (end < 0) {
        end += length;
      }
      length = start > end ? 0 : ((end - start) >>> 0);
      start >>>= 0;

      var result = Array(length);
      while (++index < length) {
        result[index] = array[index + start];
      }
      return result;
    }

    /**
     * The base implementation of `_.some` without support for iteratee shorthands.
     *
     * @private
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} predicate The function invoked per iteration.
     * @returns {boolean} Returns `true` if any element passes the predicate check,
     *  else `false`.
     */
    function baseSome(collection, predicate) {
      var result;

      baseEach(collection, function(value, index, collection) {
        result = predicate(value, index, collection);
        return !result;
      });
      return !!result;
    }

    /**
     * The base implementation of `_.sortedIndex` and `_.sortedLastIndex` which
     * performs a binary search of `array` to determine the index at which `value`
     * should be inserted into `array` in order to maintain its sort order.
     *
     * @private
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {boolean} [retHighest] Specify returning the highest qualified index.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     */
    function baseSortedIndex(array, value, retHighest) {
      var low = 0,
          high = array == null ? low : array.length;

      if (typeof value == 'number' && value === value && high <= HALF_MAX_ARRAY_LENGTH) {
        while (low < high) {
          var mid = (low + high) >>> 1,
              computed = array[mid];

          if (computed !== null && !isSymbol(computed) &&
              (retHighest ? (computed <= value) : (computed < value))) {
            low = mid + 1;
          } else {
            high = mid;
          }
        }
        return high;
      }
      return baseSortedIndexBy(array, value, identity, retHighest);
    }

    /**
     * The base implementation of `_.sortedIndexBy` and `_.sortedLastIndexBy`
     * which invokes `iteratee` for `value` and each element of `array` to compute
     * their sort ranking. The iteratee is invoked with one argument; (value).
     *
     * @private
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function} iteratee The iteratee invoked per element.
     * @param {boolean} [retHighest] Specify returning the highest qualified index.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     */
    function baseSortedIndexBy(array, value, iteratee, retHighest) {
      value = iteratee(value);

      var low = 0,
          high = array == null ? 0 : array.length,
          valIsNaN = value !== value,
          valIsNull = value === null,
          valIsSymbol = isSymbol(value),
          valIsUndefined = value === undefined;

      while (low < high) {
        var mid = nativeFloor((low + high) / 2),
            computed = iteratee(array[mid]),
            othIsDefined = computed !== undefined,
            othIsNull = computed === null,
            othIsReflexive = computed === computed,
            othIsSymbol = isSymbol(computed);

        if (valIsNaN) {
          var setLow = retHighest || othIsReflexive;
        } else if (valIsUndefined) {
          setLow = othIsReflexive && (retHighest || othIsDefined);
        } else if (valIsNull) {
          setLow = othIsReflexive && othIsDefined && (retHighest || !othIsNull);
        } else if (valIsSymbol) {
          setLow = othIsReflexive && othIsDefined && !othIsNull && (retHighest || !othIsSymbol);
        } else if (othIsNull || othIsSymbol) {
          setLow = false;
        } else {
          setLow = retHighest ? (computed <= value) : (computed < value);
        }
        if (setLow) {
          low = mid + 1;
        } else {
          high = mid;
        }
      }
      return nativeMin(high, MAX_ARRAY_INDEX);
    }

    /**
     * The base implementation of `_.sortedUniq` and `_.sortedUniqBy` without
     * support for iteratee shorthands.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @returns {Array} Returns the new duplicate free array.
     */
    function baseSortedUniq(array, iteratee) {
      var index = -1,
          length = array.length,
          resIndex = 0,
          result = [];

      while (++index < length) {
        var value = array[index],
            computed = iteratee ? iteratee(value) : value;

        if (!index || !eq(computed, seen)) {
          var seen = computed;
          result[resIndex++] = value === 0 ? 0 : value;
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.toNumber` which doesn't ensure correct
     * conversions of binary, hexadecimal, or octal string values.
     *
     * @private
     * @param {*} value The value to process.
     * @returns {number} Returns the number.
     */
    function baseToNumber(value) {
      if (typeof value == 'number') {
        return value;
      }
      if (isSymbol(value)) {
        return NAN;
      }
      return +value;
    }

    /**
     * The base implementation of `_.toString` which doesn't convert nullish
     * values to empty strings.
     *
     * @private
     * @param {*} value The value to process.
     * @returns {string} Returns the string.
     */
    function baseToString(value) {
      // Exit early for strings to avoid a performance hit in some environments.
      if (typeof value == 'string') {
        return value;
      }
      if (isArray(value)) {
        // Recursively convert values (susceptible to call stack limits).
        return arrayMap(value, baseToString) + '';
      }
      if (isSymbol(value)) {
        return symbolToString ? symbolToString.call(value) : '';
      }
      var result = (value + '');
      return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
    }

    /**
     * The base implementation of `_.uniqBy` without support for iteratee shorthands.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new duplicate free array.
     */
    function baseUniq(array, iteratee, comparator) {
      var index = -1,
          includes = arrayIncludes,
          length = array.length,
          isCommon = true,
          result = [],
          seen = result;

      if (comparator) {
        isCommon = false;
        includes = arrayIncludesWith;
      }
      else if (length >= LARGE_ARRAY_SIZE) {
        var set = iteratee ? null : createSet(array);
        if (set) {
          return setToArray(set);
        }
        isCommon = false;
        includes = cacheHas;
        seen = new SetCache;
      }
      else {
        seen = iteratee ? [] : result;
      }
      outer:
      while (++index < length) {
        var value = array[index],
            computed = iteratee ? iteratee(value) : value;

        value = (comparator || value !== 0) ? value : 0;
        if (isCommon && computed === computed) {
          var seenIndex = seen.length;
          while (seenIndex--) {
            if (seen[seenIndex] === computed) {
              continue outer;
            }
          }
          if (iteratee) {
            seen.push(computed);
          }
          result.push(value);
        }
        else if (!includes(seen, computed, comparator)) {
          if (seen !== result) {
            seen.push(computed);
          }
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.unset`.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {Array|string} path The property path to unset.
     * @returns {boolean} Returns `true` if the property is deleted, else `false`.
     */
    function baseUnset(object, path) {
      path = castPath(path, object);
      object = parent(object, path);
      return object == null || delete object[toKey(last(path))];
    }

    /**
     * The base implementation of `_.update`.
     *
     * @private
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to update.
     * @param {Function} updater The function to produce the updated value.
     * @param {Function} [customizer] The function to customize path creation.
     * @returns {Object} Returns `object`.
     */
    function baseUpdate(object, path, updater, customizer) {
      return baseSet(object, path, updater(baseGet(object, path)), customizer);
    }

    /**
     * The base implementation of methods like `_.dropWhile` and `_.takeWhile`
     * without support for iteratee shorthands.
     *
     * @private
     * @param {Array} array The array to query.
     * @param {Function} predicate The function invoked per iteration.
     * @param {boolean} [isDrop] Specify dropping elements instead of taking them.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Array} Returns the slice of `array`.
     */
    function baseWhile(array, predicate, isDrop, fromRight) {
      var length = array.length,
          index = fromRight ? length : -1;

      while ((fromRight ? index-- : ++index < length) &&
        predicate(array[index], index, array)) {}

      return isDrop
        ? baseSlice(array, (fromRight ? 0 : index), (fromRight ? index + 1 : length))
        : baseSlice(array, (fromRight ? index + 1 : 0), (fromRight ? length : index));
    }

    /**
     * The base implementation of `wrapperValue` which returns the result of
     * performing a sequence of actions on the unwrapped `value`, where each
     * successive action is supplied the return value of the previous.
     *
     * @private
     * @param {*} value The unwrapped value.
     * @param {Array} actions Actions to perform to resolve the unwrapped value.
     * @returns {*} Returns the resolved value.
     */
    function baseWrapperValue(value, actions) {
      var result = value;
      if (result instanceof LazyWrapper) {
        result = result.value();
      }
      return arrayReduce(actions, function(result, action) {
        return action.func.apply(action.thisArg, arrayPush([result], action.args));
      }, result);
    }

    /**
     * The base implementation of methods like `_.xor`, without support for
     * iteratee shorthands, that accepts an array of arrays to inspect.
     *
     * @private
     * @param {Array} arrays The arrays to inspect.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of values.
     */
    function baseXor(arrays, iteratee, comparator) {
      var length = arrays.length;
      if (length < 2) {
        return length ? baseUniq(arrays[0]) : [];
      }
      var index = -1,
          result = Array(length);

      while (++index < length) {
        var array = arrays[index],
            othIndex = -1;

        while (++othIndex < length) {
          if (othIndex != index) {
            result[index] = baseDifference(result[index] || array, arrays[othIndex], iteratee, comparator);
          }
        }
      }
      return baseUniq(baseFlatten(result, 1), iteratee, comparator);
    }

    /**
     * This base implementation of `_.zipObject` which assigns values using `assignFunc`.
     *
     * @private
     * @param {Array} props The property identifiers.
     * @param {Array} values The property values.
     * @param {Function} assignFunc The function to assign values.
     * @returns {Object} Returns the new object.
     */
    function baseZipObject(props, values, assignFunc) {
      var index = -1,
          length = props.length,
          valsLength = values.length,
          result = {};

      while (++index < length) {
        var value = index < valsLength ? values[index] : undefined;
        assignFunc(result, props[index], value);
      }
      return result;
    }

    /**
     * Casts `value` to an empty array if it's not an array like object.
     *
     * @private
     * @param {*} value The value to inspect.
     * @returns {Array|Object} Returns the cast array-like object.
     */
    function castArrayLikeObject(value) {
      return isArrayLikeObject(value) ? value : [];
    }

    /**
     * Casts `value` to `identity` if it's not a function.
     *
     * @private
     * @param {*} value The value to inspect.
     * @returns {Function} Returns cast function.
     */
    function castFunction(value) {
      return typeof value == 'function' ? value : identity;
    }

    /**
     * Casts `value` to a path array if it's not one.
     *
     * @private
     * @param {*} value The value to inspect.
     * @param {Object} [object] The object to query keys on.
     * @returns {Array} Returns the cast property path array.
     */
    function castPath(value, object) {
      if (isArray(value)) {
        return value;
      }
      return isKey(value, object) ? [value] : stringToPath(toString(value));
    }

    /**
     * A `baseRest` alias which can be replaced with `identity` by module
     * replacement plugins.
     *
     * @private
     * @type {Function}
     * @param {Function} func The function to apply a rest parameter to.
     * @returns {Function} Returns the new function.
     */
    var castRest = baseRest;

    /**
     * Casts `array` to a slice if it's needed.
     *
     * @private
     * @param {Array} array The array to inspect.
     * @param {number} start The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns the cast slice.
     */
    function castSlice(array, start, end) {
      var length = array.length;
      end = end === undefined ? length : end;
      return (!start && end >= length) ? array : baseSlice(array, start, end);
    }

    /**
     * A simple wrapper around the global [`clearTimeout`](https://mdn.io/clearTimeout).
     *
     * @private
     * @param {number|Object} id The timer id or timeout object of the timer to clear.
     */
    var clearTimeout = ctxClearTimeout || function(id) {
      return root.clearTimeout(id);
    };

    /**
     * Creates a clone of  `buffer`.
     *
     * @private
     * @param {Buffer} buffer The buffer to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Buffer} Returns the cloned buffer.
     */
    function cloneBuffer(buffer, isDeep) {
      if (isDeep) {
        return buffer.slice();
      }
      var length = buffer.length,
          result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

      buffer.copy(result);
      return result;
    }

    /**
     * Creates a clone of `arrayBuffer`.
     *
     * @private
     * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
     * @returns {ArrayBuffer} Returns the cloned array buffer.
     */
    function cloneArrayBuffer(arrayBuffer) {
      var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
      new Uint8Array(result).set(new Uint8Array(arrayBuffer));
      return result;
    }

    /**
     * Creates a clone of `dataView`.
     *
     * @private
     * @param {Object} dataView The data view to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the cloned data view.
     */
    function cloneDataView(dataView, isDeep) {
      var buffer = isDeep ? cloneArrayBuffer(dataView.buffer) : dataView.buffer;
      return new dataView.constructor(buffer, dataView.byteOffset, dataView.byteLength);
    }

    /**
     * Creates a clone of `map`.
     *
     * @private
     * @param {Object} map The map to clone.
     * @param {Function} cloneFunc The function to clone values.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the cloned map.
     */
    function cloneMap(map, isDeep, cloneFunc) {
      var array = isDeep ? cloneFunc(mapToArray(map), CLONE_DEEP_FLAG) : mapToArray(map);
      return arrayReduce(array, addMapEntry, new map.constructor);
    }

    /**
     * Creates a clone of `regexp`.
     *
     * @private
     * @param {Object} regexp The regexp to clone.
     * @returns {Object} Returns the cloned regexp.
     */
    function cloneRegExp(regexp) {
      var result = new regexp.constructor(regexp.source, reFlags.exec(regexp));
      result.lastIndex = regexp.lastIndex;
      return result;
    }

    /**
     * Creates a clone of `set`.
     *
     * @private
     * @param {Object} set The set to clone.
     * @param {Function} cloneFunc The function to clone values.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the cloned set.
     */
    function cloneSet(set, isDeep, cloneFunc) {
      var array = isDeep ? cloneFunc(setToArray(set), CLONE_DEEP_FLAG) : setToArray(set);
      return arrayReduce(array, addSetEntry, new set.constructor);
    }

    /**
     * Creates a clone of the `symbol` object.
     *
     * @private
     * @param {Object} symbol The symbol object to clone.
     * @returns {Object} Returns the cloned symbol object.
     */
    function cloneSymbol(symbol) {
      return symbolValueOf ? Object(symbolValueOf.call(symbol)) : {};
    }

    /**
     * Creates a clone of `typedArray`.
     *
     * @private
     * @param {Object} typedArray The typed array to clone.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the cloned typed array.
     */
    function cloneTypedArray(typedArray, isDeep) {
      var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
      return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
    }

    /**
     * Compares values to sort them in ascending order.
     *
     * @private
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {number} Returns the sort order indicator for `value`.
     */
    function compareAscending(value, other) {
      if (value !== other) {
        var valIsDefined = value !== undefined,
            valIsNull = value === null,
            valIsReflexive = value === value,
            valIsSymbol = isSymbol(value);

        var othIsDefined = other !== undefined,
            othIsNull = other === null,
            othIsReflexive = other === other,
            othIsSymbol = isSymbol(other);

        if ((!othIsNull && !othIsSymbol && !valIsSymbol && value > other) ||
            (valIsSymbol && othIsDefined && othIsReflexive && !othIsNull && !othIsSymbol) ||
            (valIsNull && othIsDefined && othIsReflexive) ||
            (!valIsDefined && othIsReflexive) ||
            !valIsReflexive) {
          return 1;
        }
        if ((!valIsNull && !valIsSymbol && !othIsSymbol && value < other) ||
            (othIsSymbol && valIsDefined && valIsReflexive && !valIsNull && !valIsSymbol) ||
            (othIsNull && valIsDefined && valIsReflexive) ||
            (!othIsDefined && valIsReflexive) ||
            !othIsReflexive) {
          return -1;
        }
      }
      return 0;
    }

    /**
     * Used by `_.orderBy` to compare multiple properties of a value to another
     * and stable sort them.
     *
     * If `orders` is unspecified, all values are sorted in ascending order. Otherwise,
     * specify an order of "desc" for descending or "asc" for ascending sort order
     * of corresponding values.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {boolean[]|string[]} orders The order to sort by for each property.
     * @returns {number} Returns the sort order indicator for `object`.
     */
    function compareMultiple(object, other, orders) {
      var index = -1,
          objCriteria = object.criteria,
          othCriteria = other.criteria,
          length = objCriteria.length,
          ordersLength = orders.length;

      while (++index < length) {
        var result = compareAscending(objCriteria[index], othCriteria[index]);
        if (result) {
          if (index >= ordersLength) {
            return result;
          }
          var order = orders[index];
          return result * (order == 'desc' ? -1 : 1);
        }
      }
      // Fixes an `Array#sort` bug in the JS engine embedded in Adobe applications
      // that causes it, under certain circumstances, to provide the same value for
      // `object` and `other`. See https://github.com/jashkenas/underscore/pull/1247
      // for more details.
      //
      // This also ensures a stable sort in V8 and other engines.
      // See https://bugs.chromium.org/p/v8/issues/detail?id=90 for more details.
      return object.index - other.index;
    }

    /**
     * Creates an array that is the composition of partially applied arguments,
     * placeholders, and provided arguments into a single array of arguments.
     *
     * @private
     * @param {Array} args The provided arguments.
     * @param {Array} partials The arguments to prepend to those provided.
     * @param {Array} holders The `partials` placeholder indexes.
     * @params {boolean} [isCurried] Specify composing for a curried function.
     * @returns {Array} Returns the new array of composed arguments.
     */
    function composeArgs(args, partials, holders, isCurried) {
      var argsIndex = -1,
          argsLength = args.length,
          holdersLength = holders.length,
          leftIndex = -1,
          leftLength = partials.length,
          rangeLength = nativeMax(argsLength - holdersLength, 0),
          result = Array(leftLength + rangeLength),
          isUncurried = !isCurried;

      while (++leftIndex < leftLength) {
        result[leftIndex] = partials[leftIndex];
      }
      while (++argsIndex < holdersLength) {
        if (isUncurried || argsIndex < argsLength) {
          result[holders[argsIndex]] = args[argsIndex];
        }
      }
      while (rangeLength--) {
        result[leftIndex++] = args[argsIndex++];
      }
      return result;
    }

    /**
     * This function is like `composeArgs` except that the arguments composition
     * is tailored for `_.partialRight`.
     *
     * @private
     * @param {Array} args The provided arguments.
     * @param {Array} partials The arguments to append to those provided.
     * @param {Array} holders The `partials` placeholder indexes.
     * @params {boolean} [isCurried] Specify composing for a curried function.
     * @returns {Array} Returns the new array of composed arguments.
     */
    function composeArgsRight(args, partials, holders, isCurried) {
      var argsIndex = -1,
          argsLength = args.length,
          holdersIndex = -1,
          holdersLength = holders.length,
          rightIndex = -1,
          rightLength = partials.length,
          rangeLength = nativeMax(argsLength - holdersLength, 0),
          result = Array(rangeLength + rightLength),
          isUncurried = !isCurried;

      while (++argsIndex < rangeLength) {
        result[argsIndex] = args[argsIndex];
      }
      var offset = argsIndex;
      while (++rightIndex < rightLength) {
        result[offset + rightIndex] = partials[rightIndex];
      }
      while (++holdersIndex < holdersLength) {
        if (isUncurried || argsIndex < argsLength) {
          result[offset + holders[holdersIndex]] = args[argsIndex++];
        }
      }
      return result;
    }

    /**
     * Copies the values of `source` to `array`.
     *
     * @private
     * @param {Array} source The array to copy values from.
     * @param {Array} [array=[]] The array to copy values to.
     * @returns {Array} Returns `array`.
     */
    function copyArray(source, array) {
      var index = -1,
          length = source.length;

      array || (array = Array(length));
      while (++index < length) {
        array[index] = source[index];
      }
      return array;
    }

    /**
     * Copies properties of `source` to `object`.
     *
     * @private
     * @param {Object} source The object to copy properties from.
     * @param {Array} props The property identifiers to copy.
     * @param {Object} [object={}] The object to copy properties to.
     * @param {Function} [customizer] The function to customize copied values.
     * @returns {Object} Returns `object`.
     */
    function copyObject(source, props, object, customizer) {
      var isNew = !object;
      object || (object = {});

      var index = -1,
          length = props.length;

      while (++index < length) {
        var key = props[index];

        var newValue = customizer
          ? customizer(object[key], source[key], key, object, source)
          : undefined;

        if (newValue === undefined) {
          newValue = source[key];
        }
        if (isNew) {
          baseAssignValue(object, key, newValue);
        } else {
          assignValue(object, key, newValue);
        }
      }
      return object;
    }

    /**
     * Copies own symbols of `source` to `object`.
     *
     * @private
     * @param {Object} source The object to copy symbols from.
     * @param {Object} [object={}] The object to copy symbols to.
     * @returns {Object} Returns `object`.
     */
    function copySymbols(source, object) {
      return copyObject(source, getSymbols(source), object);
    }

    /**
     * Copies own and inherited symbols of `source` to `object`.
     *
     * @private
     * @param {Object} source The object to copy symbols from.
     * @param {Object} [object={}] The object to copy symbols to.
     * @returns {Object} Returns `object`.
     */
    function copySymbolsIn(source, object) {
      return copyObject(source, getSymbolsIn(source), object);
    }

    /**
     * Creates a function like `_.groupBy`.
     *
     * @private
     * @param {Function} setter The function to set accumulator values.
     * @param {Function} [initializer] The accumulator object initializer.
     * @returns {Function} Returns the new aggregator function.
     */
    function createAggregator(setter, initializer) {
      return function(collection, iteratee) {
        var func = isArray(collection) ? arrayAggregator : baseAggregator,
            accumulator = initializer ? initializer() : {};

        return func(collection, setter, getIteratee(iteratee, 2), accumulator);
      };
    }

    /**
     * Creates a function like `_.assign`.
     *
     * @private
     * @param {Function} assigner The function to assign values.
     * @returns {Function} Returns the new assigner function.
     */
    function createAssigner(assigner) {
      return baseRest(function(object, sources) {
        var index = -1,
            length = sources.length,
            customizer = length > 1 ? sources[length - 1] : undefined,
            guard = length > 2 ? sources[2] : undefined;

        customizer = (assigner.length > 3 && typeof customizer == 'function')
          ? (length--, customizer)
          : undefined;

        if (guard && isIterateeCall(sources[0], sources[1], guard)) {
          customizer = length < 3 ? undefined : customizer;
          length = 1;
        }
        object = Object(object);
        while (++index < length) {
          var source = sources[index];
          if (source) {
            assigner(object, source, index, customizer);
          }
        }
        return object;
      });
    }

    /**
     * Creates a `baseEach` or `baseEachRight` function.
     *
     * @private
     * @param {Function} eachFunc The function to iterate over a collection.
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new base function.
     */
    function createBaseEach(eachFunc, fromRight) {
      return function(collection, iteratee) {
        if (collection == null) {
          return collection;
        }
        if (!isArrayLike(collection)) {
          return eachFunc(collection, iteratee);
        }
        var length = collection.length,
            index = fromRight ? length : -1,
            iterable = Object(collection);

        while ((fromRight ? index-- : ++index < length)) {
          if (iteratee(iterable[index], index, iterable) === false) {
            break;
          }
        }
        return collection;
      };
    }

    /**
     * Creates a base function for methods like `_.forIn` and `_.forOwn`.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new base function.
     */
    function createBaseFor(fromRight) {
      return function(object, iteratee, keysFunc) {
        var index = -1,
            iterable = Object(object),
            props = keysFunc(object),
            length = props.length;

        while (length--) {
          var key = props[fromRight ? length : ++index];
          if (iteratee(iterable[key], key, iterable) === false) {
            break;
          }
        }
        return object;
      };
    }

    /**
     * Creates a function that wraps `func` to invoke it with the optional `this`
     * binding of `thisArg`.
     *
     * @private
     * @param {Function} func The function to wrap.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createBind(func, bitmask, thisArg) {
      var isBind = bitmask & WRAP_BIND_FLAG,
          Ctor = createCtor(func);

      function wrapper() {
        var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
        return fn.apply(isBind ? thisArg : this, arguments);
      }
      return wrapper;
    }

    /**
     * Creates a function like `_.lowerFirst`.
     *
     * @private
     * @param {string} methodName The name of the `String` case method to use.
     * @returns {Function} Returns the new case function.
     */
    function createCaseFirst(methodName) {
      return function(string) {
        string = toString(string);

        var strSymbols = hasUnicode(string)
          ? stringToArray(string)
          : undefined;

        var chr = strSymbols
          ? strSymbols[0]
          : string.charAt(0);

        var trailing = strSymbols
          ? castSlice(strSymbols, 1).join('')
          : string.slice(1);

        return chr[methodName]() + trailing;
      };
    }

    /**
     * Creates a function like `_.camelCase`.
     *
     * @private
     * @param {Function} callback The function to combine each word.
     * @returns {Function} Returns the new compounder function.
     */
    function createCompounder(callback) {
      return function(string) {
        return arrayReduce(words(deburr(string).replace(reApos, '')), callback, '');
      };
    }

    /**
     * Creates a function that produces an instance of `Ctor` regardless of
     * whether it was invoked as part of a `new` expression or by `call` or `apply`.
     *
     * @private
     * @param {Function} Ctor The constructor to wrap.
     * @returns {Function} Returns the new wrapped function.
     */
    function createCtor(Ctor) {
      return function() {
        // Use a `switch` statement to work with class constructors. See
        // http://ecma-international.org/ecma-262/7.0/#sec-ecmascript-function-objects-call-thisargument-argumentslist
        // for more details.
        var args = arguments;
        switch (args.length) {
          case 0: return new Ctor;
          case 1: return new Ctor(args[0]);
          case 2: return new Ctor(args[0], args[1]);
          case 3: return new Ctor(args[0], args[1], args[2]);
          case 4: return new Ctor(args[0], args[1], args[2], args[3]);
          case 5: return new Ctor(args[0], args[1], args[2], args[3], args[4]);
          case 6: return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5]);
          case 7: return new Ctor(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
        }
        var thisBinding = baseCreate(Ctor.prototype),
            result = Ctor.apply(thisBinding, args);

        // Mimic the constructor's `return` behavior.
        // See https://es5.github.io/#x13.2.2 for more details.
        return isObject(result) ? result : thisBinding;
      };
    }

    /**
     * Creates a function that wraps `func` to enable currying.
     *
     * @private
     * @param {Function} func The function to wrap.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @param {number} arity The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createCurry(func, bitmask, arity) {
      var Ctor = createCtor(func);

      function wrapper() {
        var length = arguments.length,
            args = Array(length),
            index = length,
            placeholder = getHolder(wrapper);

        while (index--) {
          args[index] = arguments[index];
        }
        var holders = (length < 3 && args[0] !== placeholder && args[length - 1] !== placeholder)
          ? []
          : replaceHolders(args, placeholder);

        length -= holders.length;
        if (length < arity) {
          return createRecurry(
            func, bitmask, createHybrid, wrapper.placeholder, undefined,
            args, holders, undefined, undefined, arity - length);
        }
        var fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;
        return apply(fn, this, args);
      }
      return wrapper;
    }

    /**
     * Creates a `_.find` or `_.findLast` function.
     *
     * @private
     * @param {Function} findIndexFunc The function to find the collection index.
     * @returns {Function} Returns the new find function.
     */
    function createFind(findIndexFunc) {
      return function(collection, predicate, fromIndex) {
        var iterable = Object(collection);
        if (!isArrayLike(collection)) {
          var iteratee = getIteratee(predicate, 3);
          collection = keys(collection);
          predicate = function(key) { return iteratee(iterable[key], key, iterable); };
        }
        var index = findIndexFunc(collection, predicate, fromIndex);
        return index > -1 ? iterable[iteratee ? collection[index] : index] : undefined;
      };
    }

    /**
     * Creates a `_.flow` or `_.flowRight` function.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new flow function.
     */
    function createFlow(fromRight) {
      return flatRest(function(funcs) {
        var length = funcs.length,
            index = length,
            prereq = LodashWrapper.prototype.thru;

        if (fromRight) {
          funcs.reverse();
        }
        while (index--) {
          var func = funcs[index];
          if (typeof func != 'function') {
            throw new TypeError(FUNC_ERROR_TEXT);
          }
          if (prereq && !wrapper && getFuncName(func) == 'wrapper') {
            var wrapper = new LodashWrapper([], true);
          }
        }
        index = wrapper ? index : length;
        while (++index < length) {
          func = funcs[index];

          var funcName = getFuncName(func),
              data = funcName == 'wrapper' ? getData(func) : undefined;

          if (data && isLaziable(data[0]) &&
                data[1] == (WRAP_ARY_FLAG | WRAP_CURRY_FLAG | WRAP_PARTIAL_FLAG | WRAP_REARG_FLAG) &&
                !data[4].length && data[9] == 1
              ) {
            wrapper = wrapper[getFuncName(data[0])].apply(wrapper, data[3]);
          } else {
            wrapper = (func.length == 1 && isLaziable(func))
              ? wrapper[funcName]()
              : wrapper.thru(func);
          }
        }
        return function() {
          var args = arguments,
              value = args[0];

          if (wrapper && args.length == 1 && isArray(value)) {
            return wrapper.plant(value).value();
          }
          var index = 0,
              result = length ? funcs[index].apply(this, args) : value;

          while (++index < length) {
            result = funcs[index].call(this, result);
          }
          return result;
        };
      });
    }

    /**
     * Creates a function that wraps `func` to invoke it with optional `this`
     * binding of `thisArg`, partial application, and currying.
     *
     * @private
     * @param {Function|string} func The function or method name to wrap.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {Array} [partials] The arguments to prepend to those provided to
     *  the new function.
     * @param {Array} [holders] The `partials` placeholder indexes.
     * @param {Array} [partialsRight] The arguments to append to those provided
     *  to the new function.
     * @param {Array} [holdersRight] The `partialsRight` placeholder indexes.
     * @param {Array} [argPos] The argument positions of the new function.
     * @param {number} [ary] The arity cap of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createHybrid(func, bitmask, thisArg, partials, holders, partialsRight, holdersRight, argPos, ary, arity) {
      var isAry = bitmask & WRAP_ARY_FLAG,
          isBind = bitmask & WRAP_BIND_FLAG,
          isBindKey = bitmask & WRAP_BIND_KEY_FLAG,
          isCurried = bitmask & (WRAP_CURRY_FLAG | WRAP_CURRY_RIGHT_FLAG),
          isFlip = bitmask & WRAP_FLIP_FLAG,
          Ctor = isBindKey ? undefined : createCtor(func);

      function wrapper() {
        var length = arguments.length,
            args = Array(length),
            index = length;

        while (index--) {
          args[index] = arguments[index];
        }
        if (isCurried) {
          var placeholder = getHolder(wrapper),
              holdersCount = countHolders(args, placeholder);
        }
        if (partials) {
          args = composeArgs(args, partials, holders, isCurried);
        }
        if (partialsRight) {
          args = composeArgsRight(args, partialsRight, holdersRight, isCurried);
        }
        length -= holdersCount;
        if (isCurried && length < arity) {
          var newHolders = replaceHolders(args, placeholder);
          return createRecurry(
            func, bitmask, createHybrid, wrapper.placeholder, thisArg,
            args, newHolders, argPos, ary, arity - length
          );
        }
        var thisBinding = isBind ? thisArg : this,
            fn = isBindKey ? thisBinding[func] : func;

        length = args.length;
        if (argPos) {
          args = reorder(args, argPos);
        } else if (isFlip && length > 1) {
          args.reverse();
        }
        if (isAry && ary < length) {
          args.length = ary;
        }
        if (this && this !== root && this instanceof wrapper) {
          fn = Ctor || createCtor(fn);
        }
        return fn.apply(thisBinding, args);
      }
      return wrapper;
    }

    /**
     * Creates a function like `_.invertBy`.
     *
     * @private
     * @param {Function} setter The function to set accumulator values.
     * @param {Function} toIteratee The function to resolve iteratees.
     * @returns {Function} Returns the new inverter function.
     */
    function createInverter(setter, toIteratee) {
      return function(object, iteratee) {
        return baseInverter(object, setter, toIteratee(iteratee), {});
      };
    }

    /**
     * Creates a function that performs a mathematical operation on two values.
     *
     * @private
     * @param {Function} operator The function to perform the operation.
     * @param {number} [defaultValue] The value used for `undefined` arguments.
     * @returns {Function} Returns the new mathematical operation function.
     */
    function createMathOperation(operator, defaultValue) {
      return function(value, other) {
        var result;
        if (value === undefined && other === undefined) {
          return defaultValue;
        }
        if (value !== undefined) {
          result = value;
        }
        if (other !== undefined) {
          if (result === undefined) {
            return other;
          }
          if (typeof value == 'string' || typeof other == 'string') {
            value = baseToString(value);
            other = baseToString(other);
          } else {
            value = baseToNumber(value);
            other = baseToNumber(other);
          }
          result = operator(value, other);
        }
        return result;
      };
    }

    /**
     * Creates a function like `_.over`.
     *
     * @private
     * @param {Function} arrayFunc The function to iterate over iteratees.
     * @returns {Function} Returns the new over function.
     */
    function createOver(arrayFunc) {
      return flatRest(function(iteratees) {
        iteratees = arrayMap(iteratees, baseUnary(getIteratee()));
        return baseRest(function(args) {
          var thisArg = this;
          return arrayFunc(iteratees, function(iteratee) {
            return apply(iteratee, thisArg, args);
          });
        });
      });
    }

    /**
     * Creates the padding for `string` based on `length`. The `chars` string
     * is truncated if the number of characters exceeds `length`.
     *
     * @private
     * @param {number} length The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padding for `string`.
     */
    function createPadding(length, chars) {
      chars = chars === undefined ? ' ' : baseToString(chars);

      var charsLength = chars.length;
      if (charsLength < 2) {
        return charsLength ? baseRepeat(chars, length) : chars;
      }
      var result = baseRepeat(chars, nativeCeil(length / stringSize(chars)));
      return hasUnicode(chars)
        ? castSlice(stringToArray(result), 0, length).join('')
        : result.slice(0, length);
    }

    /**
     * Creates a function that wraps `func` to invoke it with the `this` binding
     * of `thisArg` and `partials` prepended to the arguments it receives.
     *
     * @private
     * @param {Function} func The function to wrap.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @param {*} thisArg The `this` binding of `func`.
     * @param {Array} partials The arguments to prepend to those provided to
     *  the new function.
     * @returns {Function} Returns the new wrapped function.
     */
    function createPartial(func, bitmask, thisArg, partials) {
      var isBind = bitmask & WRAP_BIND_FLAG,
          Ctor = createCtor(func);

      function wrapper() {
        var argsIndex = -1,
            argsLength = arguments.length,
            leftIndex = -1,
            leftLength = partials.length,
            args = Array(leftLength + argsLength),
            fn = (this && this !== root && this instanceof wrapper) ? Ctor : func;

        while (++leftIndex < leftLength) {
          args[leftIndex] = partials[leftIndex];
        }
        while (argsLength--) {
          args[leftIndex++] = arguments[++argsIndex];
        }
        return apply(fn, isBind ? thisArg : this, args);
      }
      return wrapper;
    }

    /**
     * Creates a `_.range` or `_.rangeRight` function.
     *
     * @private
     * @param {boolean} [fromRight] Specify iterating from right to left.
     * @returns {Function} Returns the new range function.
     */
    function createRange(fromRight) {
      return function(start, end, step) {
        if (step && typeof step != 'number' && isIterateeCall(start, end, step)) {
          end = step = undefined;
        }
        // Ensure the sign of `-0` is preserved.
        start = toFinite(start);
        if (end === undefined) {
          end = start;
          start = 0;
        } else {
          end = toFinite(end);
        }
        step = step === undefined ? (start < end ? 1 : -1) : toFinite(step);
        return baseRange(start, end, step, fromRight);
      };
    }

    /**
     * Creates a function that performs a relational operation on two values.
     *
     * @private
     * @param {Function} operator The function to perform the operation.
     * @returns {Function} Returns the new relational operation function.
     */
    function createRelationalOperation(operator) {
      return function(value, other) {
        if (!(typeof value == 'string' && typeof other == 'string')) {
          value = toNumber(value);
          other = toNumber(other);
        }
        return operator(value, other);
      };
    }

    /**
     * Creates a function that wraps `func` to continue currying.
     *
     * @private
     * @param {Function} func The function to wrap.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @param {Function} wrapFunc The function to create the `func` wrapper.
     * @param {*} placeholder The placeholder value.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {Array} [partials] The arguments to prepend to those provided to
     *  the new function.
     * @param {Array} [holders] The `partials` placeholder indexes.
     * @param {Array} [argPos] The argument positions of the new function.
     * @param {number} [ary] The arity cap of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createRecurry(func, bitmask, wrapFunc, placeholder, thisArg, partials, holders, argPos, ary, arity) {
      var isCurry = bitmask & WRAP_CURRY_FLAG,
          newHolders = isCurry ? holders : undefined,
          newHoldersRight = isCurry ? undefined : holders,
          newPartials = isCurry ? partials : undefined,
          newPartialsRight = isCurry ? undefined : partials;

      bitmask |= (isCurry ? WRAP_PARTIAL_FLAG : WRAP_PARTIAL_RIGHT_FLAG);
      bitmask &= ~(isCurry ? WRAP_PARTIAL_RIGHT_FLAG : WRAP_PARTIAL_FLAG);

      if (!(bitmask & WRAP_CURRY_BOUND_FLAG)) {
        bitmask &= ~(WRAP_BIND_FLAG | WRAP_BIND_KEY_FLAG);
      }
      var newData = [
        func, bitmask, thisArg, newPartials, newHolders, newPartialsRight,
        newHoldersRight, argPos, ary, arity
      ];

      var result = wrapFunc.apply(undefined, newData);
      if (isLaziable(func)) {
        setData(result, newData);
      }
      result.placeholder = placeholder;
      return setWrapToString(result, func, bitmask);
    }

    /**
     * Creates a function like `_.round`.
     *
     * @private
     * @param {string} methodName The name of the `Math` method to use when rounding.
     * @returns {Function} Returns the new round function.
     */
    function createRound(methodName) {
      var func = Math[methodName];
      return function(number, precision) {
        number = toNumber(number);
        precision = precision == null ? 0 : nativeMin(toInteger(precision), 292);
        if (precision) {
          // Shift with exponential notation to avoid floating-point issues.
          // See [MDN](https://mdn.io/round#Examples) for more details.
          var pair = (toString(number) + 'e').split('e'),
              value = func(pair[0] + 'e' + (+pair[1] + precision));

          pair = (toString(value) + 'e').split('e');
          return +(pair[0] + 'e' + (+pair[1] - precision));
        }
        return func(number);
      };
    }

    /**
     * Creates a set object of `values`.
     *
     * @private
     * @param {Array} values The values to add to the set.
     * @returns {Object} Returns the new set.
     */
    var createSet = !(Set && (1 / setToArray(new Set([,-0]))[1]) == INFINITY) ? noop : function(values) {
      return new Set(values);
    };

    /**
     * Creates a `_.toPairs` or `_.toPairsIn` function.
     *
     * @private
     * @param {Function} keysFunc The function to get the keys of a given object.
     * @returns {Function} Returns the new pairs function.
     */
    function createToPairs(keysFunc) {
      return function(object) {
        var tag = getTag(object);
        if (tag == mapTag) {
          return mapToArray(object);
        }
        if (tag == setTag) {
          return setToPairs(object);
        }
        return baseToPairs(object, keysFunc(object));
      };
    }

    /**
     * Creates a function that either curries or invokes `func` with optional
     * `this` binding and partially applied arguments.
     *
     * @private
     * @param {Function|string} func The function or method name to wrap.
     * @param {number} bitmask The bitmask flags.
     *    1 - `_.bind`
     *    2 - `_.bindKey`
     *    4 - `_.curry` or `_.curryRight` of a bound function
     *    8 - `_.curry`
     *   16 - `_.curryRight`
     *   32 - `_.partial`
     *   64 - `_.partialRight`
     *  128 - `_.rearg`
     *  256 - `_.ary`
     *  512 - `_.flip`
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {Array} [partials] The arguments to be partially applied.
     * @param {Array} [holders] The `partials` placeholder indexes.
     * @param {Array} [argPos] The argument positions of the new function.
     * @param {number} [ary] The arity cap of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new wrapped function.
     */
    function createWrap(func, bitmask, thisArg, partials, holders, argPos, ary, arity) {
      var isBindKey = bitmask & WRAP_BIND_KEY_FLAG;
      if (!isBindKey && typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      var length = partials ? partials.length : 0;
      if (!length) {
        bitmask &= ~(WRAP_PARTIAL_FLAG | WRAP_PARTIAL_RIGHT_FLAG);
        partials = holders = undefined;
      }
      ary = ary === undefined ? ary : nativeMax(toInteger(ary), 0);
      arity = arity === undefined ? arity : toInteger(arity);
      length -= holders ? holders.length : 0;

      if (bitmask & WRAP_PARTIAL_RIGHT_FLAG) {
        var partialsRight = partials,
            holdersRight = holders;

        partials = holders = undefined;
      }
      var data = isBindKey ? undefined : getData(func);

      var newData = [
        func, bitmask, thisArg, partials, holders, partialsRight, holdersRight,
        argPos, ary, arity
      ];

      if (data) {
        mergeData(newData, data);
      }
      func = newData[0];
      bitmask = newData[1];
      thisArg = newData[2];
      partials = newData[3];
      holders = newData[4];
      arity = newData[9] = newData[9] === undefined
        ? (isBindKey ? 0 : func.length)
        : nativeMax(newData[9] - length, 0);

      if (!arity && bitmask & (WRAP_CURRY_FLAG | WRAP_CURRY_RIGHT_FLAG)) {
        bitmask &= ~(WRAP_CURRY_FLAG | WRAP_CURRY_RIGHT_FLAG);
      }
      if (!bitmask || bitmask == WRAP_BIND_FLAG) {
        var result = createBind(func, bitmask, thisArg);
      } else if (bitmask == WRAP_CURRY_FLAG || bitmask == WRAP_CURRY_RIGHT_FLAG) {
        result = createCurry(func, bitmask, arity);
      } else if ((bitmask == WRAP_PARTIAL_FLAG || bitmask == (WRAP_BIND_FLAG | WRAP_PARTIAL_FLAG)) && !holders.length) {
        result = createPartial(func, bitmask, thisArg, partials);
      } else {
        result = createHybrid.apply(undefined, newData);
      }
      var setter = data ? baseSetData : setData;
      return setWrapToString(setter(result, newData), func, bitmask);
    }

    /**
     * Used by `_.defaults` to customize its `_.assignIn` use to assign properties
     * of source objects to the destination object for all destination properties
     * that resolve to `undefined`.
     *
     * @private
     * @param {*} objValue The destination value.
     * @param {*} srcValue The source value.
     * @param {string} key The key of the property to assign.
     * @param {Object} object The parent object of `objValue`.
     * @returns {*} Returns the value to assign.
     */
    function customDefaultsAssignIn(objValue, srcValue, key, object) {
      if (objValue === undefined ||
          (eq(objValue, objectProto[key]) && !hasOwnProperty.call(object, key))) {
        return srcValue;
      }
      return objValue;
    }

    /**
     * Used by `_.defaultsDeep` to customize its `_.merge` use to merge source
     * objects into destination objects that are passed thru.
     *
     * @private
     * @param {*} objValue The destination value.
     * @param {*} srcValue The source value.
     * @param {string} key The key of the property to merge.
     * @param {Object} object The parent object of `objValue`.
     * @param {Object} source The parent object of `srcValue`.
     * @param {Object} [stack] Tracks traversed source values and their merged
     *  counterparts.
     * @returns {*} Returns the value to assign.
     */
    function customDefaultsMerge(objValue, srcValue, key, object, source, stack) {
      if (isObject(objValue) && isObject(srcValue)) {
        // Recursively merge objects and arrays (susceptible to call stack limits).
        stack.set(srcValue, objValue);
        baseMerge(objValue, srcValue, undefined, customDefaultsMerge, stack);
        stack['delete'](srcValue);
      }
      return objValue;
    }

    /**
     * Used by `_.omit` to customize its `_.cloneDeep` use to only clone plain
     * objects.
     *
     * @private
     * @param {*} value The value to inspect.
     * @param {string} key The key of the property to inspect.
     * @returns {*} Returns the uncloned value or `undefined` to defer cloning to `_.cloneDeep`.
     */
    function customOmitClone(value) {
      return isPlainObject(value) ? undefined : value;
    }

    /**
     * A specialized version of `baseIsEqualDeep` for arrays with support for
     * partial deep comparisons.
     *
     * @private
     * @param {Array} array The array to compare.
     * @param {Array} other The other array to compare.
     * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
     * @param {Function} customizer The function to customize comparisons.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Object} stack Tracks traversed `array` and `other` objects.
     * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
     */
    function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
          arrLength = array.length,
          othLength = other.length;

      if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(array);
      if (stacked && stack.get(other)) {
        return stacked == other;
      }
      var index = -1,
          result = true,
          seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

      stack.set(array, other);
      stack.set(other, array);

      // Ignore non-index properties.
      while (++index < arrLength) {
        var arrValue = array[index],
            othValue = other[index];

        if (customizer) {
          var compared = isPartial
            ? customizer(othValue, arrValue, index, other, array, stack)
            : customizer(arrValue, othValue, index, array, other, stack);
        }
        if (compared !== undefined) {
          if (compared) {
            continue;
          }
          result = false;
          break;
        }
        // Recursively compare arrays (susceptible to call stack limits).
        if (seen) {
          if (!arraySome(other, function(othValue, othIndex) {
                if (!cacheHas(seen, othIndex) &&
                    (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
                  return seen.push(othIndex);
                }
              })) {
            result = false;
            break;
          }
        } else if (!(
              arrValue === othValue ||
                equalFunc(arrValue, othValue, bitmask, customizer, stack)
            )) {
          result = false;
          break;
        }
      }
      stack['delete'](array);
      stack['delete'](other);
      return result;
    }

    /**
     * A specialized version of `baseIsEqualDeep` for comparing objects of
     * the same `toStringTag`.
     *
     * **Note:** This function only supports comparing values with tags of
     * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {string} tag The `toStringTag` of the objects to compare.
     * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
     * @param {Function} customizer The function to customize comparisons.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Object} stack Tracks traversed `object` and `other` objects.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
      switch (tag) {
        case dataViewTag:
          if ((object.byteLength != other.byteLength) ||
              (object.byteOffset != other.byteOffset)) {
            return false;
          }
          object = object.buffer;
          other = other.buffer;

        case arrayBufferTag:
          if ((object.byteLength != other.byteLength) ||
              !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
            return false;
          }
          return true;

        case boolTag:
        case dateTag:
        case numberTag:
          // Coerce booleans to `1` or `0` and dates to milliseconds.
          // Invalid dates are coerced to `NaN`.
          return eq(+object, +other);

        case errorTag:
          return object.name == other.name && object.message == other.message;

        case regexpTag:
        case stringTag:
          // Coerce regexes to strings and treat strings, primitives and objects,
          // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
          // for more details.
          return object == (other + '');

        case mapTag:
          var convert = mapToArray;

        case setTag:
          var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
          convert || (convert = setToArray);

          if (object.size != other.size && !isPartial) {
            return false;
          }
          // Assume cyclic values are equal.
          var stacked = stack.get(object);
          if (stacked) {
            return stacked == other;
          }
          bitmask |= COMPARE_UNORDERED_FLAG;

          // Recursively compare objects (susceptible to call stack limits).
          stack.set(object, other);
          var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
          stack['delete'](object);
          return result;

        case symbolTag:
          if (symbolValueOf) {
            return symbolValueOf.call(object) == symbolValueOf.call(other);
          }
      }
      return false;
    }

    /**
     * A specialized version of `baseIsEqualDeep` for objects with support for
     * partial deep comparisons.
     *
     * @private
     * @param {Object} object The object to compare.
     * @param {Object} other The other object to compare.
     * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
     * @param {Function} customizer The function to customize comparisons.
     * @param {Function} equalFunc The function to determine equivalents of values.
     * @param {Object} stack Tracks traversed `object` and `other` objects.
     * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
     */
    function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
          objProps = getAllKeys(object),
          objLength = objProps.length,
          othProps = getAllKeys(other),
          othLength = othProps.length;

      if (objLength != othLength && !isPartial) {
        return false;
      }
      var index = objLength;
      while (index--) {
        var key = objProps[index];
        if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
          return false;
        }
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked && stack.get(other)) {
        return stacked == other;
      }
      var result = true;
      stack.set(object, other);
      stack.set(other, object);

      var skipCtor = isPartial;
      while (++index < objLength) {
        key = objProps[index];
        var objValue = object[key],
            othValue = other[key];

        if (customizer) {
          var compared = isPartial
            ? customizer(othValue, objValue, key, other, object, stack)
            : customizer(objValue, othValue, key, object, other, stack);
        }
        // Recursively compare objects (susceptible to call stack limits).
        if (!(compared === undefined
              ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
              : compared
            )) {
          result = false;
          break;
        }
        skipCtor || (skipCtor = key == 'constructor');
      }
      if (result && !skipCtor) {
        var objCtor = object.constructor,
            othCtor = other.constructor;

        // Non `Object` object instances with different constructors are not equal.
        if (objCtor != othCtor &&
            ('constructor' in object && 'constructor' in other) &&
            !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
              typeof othCtor == 'function' && othCtor instanceof othCtor)) {
          result = false;
        }
      }
      stack['delete'](object);
      stack['delete'](other);
      return result;
    }

    /**
     * A specialized version of `baseRest` which flattens the rest array.
     *
     * @private
     * @param {Function} func The function to apply a rest parameter to.
     * @returns {Function} Returns the new function.
     */
    function flatRest(func) {
      return setToString(overRest(func, undefined, flatten), func + '');
    }

    /**
     * Creates an array of own enumerable property names and symbols of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names and symbols.
     */
    function getAllKeys(object) {
      return baseGetAllKeys(object, keys, getSymbols);
    }

    /**
     * Creates an array of own and inherited enumerable property names and
     * symbols of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names and symbols.
     */
    function getAllKeysIn(object) {
      return baseGetAllKeys(object, keysIn, getSymbolsIn);
    }

    /**
     * Gets metadata for `func`.
     *
     * @private
     * @param {Function} func The function to query.
     * @returns {*} Returns the metadata for `func`.
     */
    var getData = !metaMap ? noop : function(func) {
      return metaMap.get(func);
    };

    /**
     * Gets the name of `func`.
     *
     * @private
     * @param {Function} func The function to query.
     * @returns {string} Returns the function name.
     */
    function getFuncName(func) {
      var result = (func.name + ''),
          array = realNames[result],
          length = hasOwnProperty.call(realNames, result) ? array.length : 0;

      while (length--) {
        var data = array[length],
            otherFunc = data.func;
        if (otherFunc == null || otherFunc == func) {
          return data.name;
        }
      }
      return result;
    }

    /**
     * Gets the argument placeholder value for `func`.
     *
     * @private
     * @param {Function} func The function to inspect.
     * @returns {*} Returns the placeholder value.
     */
    function getHolder(func) {
      var object = hasOwnProperty.call(lodash, 'placeholder') ? lodash : func;
      return object.placeholder;
    }

    /**
     * Gets the appropriate "iteratee" function. If `_.iteratee` is customized,
     * this function returns the custom method, otherwise it returns `baseIteratee`.
     * If arguments are provided, the chosen function is invoked with them and
     * its result is returned.
     *
     * @private
     * @param {*} [value] The value to convert to an iteratee.
     * @param {number} [arity] The arity of the created iteratee.
     * @returns {Function} Returns the chosen function or its result.
     */
    function getIteratee() {
      var result = lodash.iteratee || iteratee;
      result = result === iteratee ? baseIteratee : result;
      return arguments.length ? result(arguments[0], arguments[1]) : result;
    }

    /**
     * Gets the data for `map`.
     *
     * @private
     * @param {Object} map The map to query.
     * @param {string} key The reference key.
     * @returns {*} Returns the map data.
     */
    function getMapData(map, key) {
      var data = map.__data__;
      return isKeyable(key)
        ? data[typeof key == 'string' ? 'string' : 'hash']
        : data.map;
    }

    /**
     * Gets the property names, values, and compare flags of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the match data of `object`.
     */
    function getMatchData(object) {
      var result = keys(object),
          length = result.length;

      while (length--) {
        var key = result[length],
            value = object[key];

        result[length] = [key, value, isStrictComparable(value)];
      }
      return result;
    }

    /**
     * Gets the native function at `key` of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {string} key The key of the method to get.
     * @returns {*} Returns the function if it's native, else `undefined`.
     */
    function getNative(object, key) {
      var value = getValue(object, key);
      return baseIsNative(value) ? value : undefined;
    }

    /**
     * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
     *
     * @private
     * @param {*} value The value to query.
     * @returns {string} Returns the raw `toStringTag`.
     */
    function getRawTag(value) {
      var isOwn = hasOwnProperty.call(value, symToStringTag),
          tag = value[symToStringTag];

      try {
        value[symToStringTag] = undefined;
        var unmasked = true;
      } catch (e) {}

      var result = nativeObjectToString.call(value);
      if (unmasked) {
        if (isOwn) {
          value[symToStringTag] = tag;
        } else {
          delete value[symToStringTag];
        }
      }
      return result;
    }

    /**
     * Creates an array of the own enumerable symbols of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of symbols.
     */
    var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
      if (object == null) {
        return [];
      }
      object = Object(object);
      return arrayFilter(nativeGetSymbols(object), function(symbol) {
        return propertyIsEnumerable.call(object, symbol);
      });
    };

    /**
     * Creates an array of the own and inherited enumerable symbols of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of symbols.
     */
    var getSymbolsIn = !nativeGetSymbols ? stubArray : function(object) {
      var result = [];
      while (object) {
        arrayPush(result, getSymbols(object));
        object = getPrototype(object);
      }
      return result;
    };

    /**
     * Gets the `toStringTag` of `value`.
     *
     * @private
     * @param {*} value The value to query.
     * @returns {string} Returns the `toStringTag`.
     */
    var getTag = baseGetTag;

    // Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
    if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag) ||
        (Map && getTag(new Map) != mapTag) ||
        (Promise && getTag(Promise.resolve()) != promiseTag) ||
        (Set && getTag(new Set) != setTag) ||
        (WeakMap && getTag(new WeakMap) != weakMapTag)) {
      getTag = function(value) {
        var result = baseGetTag(value),
            Ctor = result == objectTag ? value.constructor : undefined,
            ctorString = Ctor ? toSource(Ctor) : '';

        if (ctorString) {
          switch (ctorString) {
            case dataViewCtorString: return dataViewTag;
            case mapCtorString: return mapTag;
            case promiseCtorString: return promiseTag;
            case setCtorString: return setTag;
            case weakMapCtorString: return weakMapTag;
          }
        }
        return result;
      };
    }

    /**
     * Gets the view, applying any `transforms` to the `start` and `end` positions.
     *
     * @private
     * @param {number} start The start of the view.
     * @param {number} end The end of the view.
     * @param {Array} transforms The transformations to apply to the view.
     * @returns {Object} Returns an object containing the `start` and `end`
     *  positions of the view.
     */
    function getView(start, end, transforms) {
      var index = -1,
          length = transforms.length;

      while (++index < length) {
        var data = transforms[index],
            size = data.size;

        switch (data.type) {
          case 'drop':      start += size; break;
          case 'dropRight': end -= size; break;
          case 'take':      end = nativeMin(end, start + size); break;
          case 'takeRight': start = nativeMax(start, end - size); break;
        }
      }
      return { 'start': start, 'end': end };
    }

    /**
     * Extracts wrapper details from the `source` body comment.
     *
     * @private
     * @param {string} source The source to inspect.
     * @returns {Array} Returns the wrapper details.
     */
    function getWrapDetails(source) {
      var match = source.match(reWrapDetails);
      return match ? match[1].split(reSplitDetails) : [];
    }

    /**
     * Checks if `path` exists on `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array|string} path The path to check.
     * @param {Function} hasFunc The function to check properties.
     * @returns {boolean} Returns `true` if `path` exists, else `false`.
     */
    function hasPath(object, path, hasFunc) {
      path = castPath(path, object);

      var index = -1,
          length = path.length,
          result = false;

      while (++index < length) {
        var key = toKey(path[index]);
        if (!(result = object != null && hasFunc(object, key))) {
          break;
        }
        object = object[key];
      }
      if (result || ++index != length) {
        return result;
      }
      length = object == null ? 0 : object.length;
      return !!length && isLength(length) && isIndex(key, length) &&
        (isArray(object) || isArguments(object));
    }

    /**
     * Initializes an array clone.
     *
     * @private
     * @param {Array} array The array to clone.
     * @returns {Array} Returns the initialized clone.
     */
    function initCloneArray(array) {
      var length = array.length,
          result = array.constructor(length);

      // Add properties assigned by `RegExp#exec`.
      if (length && typeof array[0] == 'string' && hasOwnProperty.call(array, 'index')) {
        result.index = array.index;
        result.input = array.input;
      }
      return result;
    }

    /**
     * Initializes an object clone.
     *
     * @private
     * @param {Object} object The object to clone.
     * @returns {Object} Returns the initialized clone.
     */
    function initCloneObject(object) {
      return (typeof object.constructor == 'function' && !isPrototype(object))
        ? baseCreate(getPrototype(object))
        : {};
    }

    /**
     * Initializes an object clone based on its `toStringTag`.
     *
     * **Note:** This function only supports cloning values with tags of
     * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
     *
     * @private
     * @param {Object} object The object to clone.
     * @param {string} tag The `toStringTag` of the object to clone.
     * @param {Function} cloneFunc The function to clone values.
     * @param {boolean} [isDeep] Specify a deep clone.
     * @returns {Object} Returns the initialized clone.
     */
    function initCloneByTag(object, tag, cloneFunc, isDeep) {
      var Ctor = object.constructor;
      switch (tag) {
        case arrayBufferTag:
          return cloneArrayBuffer(object);

        case boolTag:
        case dateTag:
          return new Ctor(+object);

        case dataViewTag:
          return cloneDataView(object, isDeep);

        case float32Tag: case float64Tag:
        case int8Tag: case int16Tag: case int32Tag:
        case uint8Tag: case uint8ClampedTag: case uint16Tag: case uint32Tag:
          return cloneTypedArray(object, isDeep);

        case mapTag:
          return cloneMap(object, isDeep, cloneFunc);

        case numberTag:
        case stringTag:
          return new Ctor(object);

        case regexpTag:
          return cloneRegExp(object);

        case setTag:
          return cloneSet(object, isDeep, cloneFunc);

        case symbolTag:
          return cloneSymbol(object);
      }
    }

    /**
     * Inserts wrapper `details` in a comment at the top of the `source` body.
     *
     * @private
     * @param {string} source The source to modify.
     * @returns {Array} details The details to insert.
     * @returns {string} Returns the modified source.
     */
    function insertWrapDetails(source, details) {
      var length = details.length;
      if (!length) {
        return source;
      }
      var lastIndex = length - 1;
      details[lastIndex] = (length > 1 ? '& ' : '') + details[lastIndex];
      details = details.join(length > 2 ? ', ' : ' ');
      return source.replace(reWrapComment, '{\n/* [wrapped with ' + details + '] */\n');
    }

    /**
     * Checks if `value` is a flattenable `arguments` object or array.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
     */
    function isFlattenable(value) {
      return isArray(value) || isArguments(value) ||
        !!(spreadableSymbol && value && value[spreadableSymbol]);
    }

    /**
     * Checks if `value` is a valid array-like index.
     *
     * @private
     * @param {*} value The value to check.
     * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
     * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
     */
    function isIndex(value, length) {
      length = length == null ? MAX_SAFE_INTEGER : length;
      return !!length &&
        (typeof value == 'number' || reIsUint.test(value)) &&
        (value > -1 && value % 1 == 0 && value < length);
    }

    /**
     * Checks if the given arguments are from an iteratee call.
     *
     * @private
     * @param {*} value The potential iteratee value argument.
     * @param {*} index The potential iteratee index or key argument.
     * @param {*} object The potential iteratee object argument.
     * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
     *  else `false`.
     */
    function isIterateeCall(value, index, object) {
      if (!isObject(object)) {
        return false;
      }
      var type = typeof index;
      if (type == 'number'
            ? (isArrayLike(object) && isIndex(index, object.length))
            : (type == 'string' && index in object)
          ) {
        return eq(object[index], value);
      }
      return false;
    }

    /**
     * Checks if `value` is a property name and not a property path.
     *
     * @private
     * @param {*} value The value to check.
     * @param {Object} [object] The object to query keys on.
     * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
     */
    function isKey(value, object) {
      if (isArray(value)) {
        return false;
      }
      var type = typeof value;
      if (type == 'number' || type == 'symbol' || type == 'boolean' ||
          value == null || isSymbol(value)) {
        return true;
      }
      return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
        (object != null && value in Object(object));
    }

    /**
     * Checks if `value` is suitable for use as unique object key.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
     */
    function isKeyable(value) {
      var type = typeof value;
      return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
        ? (value !== '__proto__')
        : (value === null);
    }

    /**
     * Checks if `func` has a lazy counterpart.
     *
     * @private
     * @param {Function} func The function to check.
     * @returns {boolean} Returns `true` if `func` has a lazy counterpart,
     *  else `false`.
     */
    function isLaziable(func) {
      var funcName = getFuncName(func),
          other = lodash[funcName];

      if (typeof other != 'function' || !(funcName in LazyWrapper.prototype)) {
        return false;
      }
      if (func === other) {
        return true;
      }
      var data = getData(other);
      return !!data && func === data[0];
    }

    /**
     * Checks if `func` has its source masked.
     *
     * @private
     * @param {Function} func The function to check.
     * @returns {boolean} Returns `true` if `func` is masked, else `false`.
     */
    function isMasked(func) {
      return !!maskSrcKey && (maskSrcKey in func);
    }

    /**
     * Checks if `func` is capable of being masked.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `func` is maskable, else `false`.
     */
    var isMaskable = coreJsData ? isFunction : stubFalse;

    /**
     * Checks if `value` is likely a prototype object.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
     */
    function isPrototype(value) {
      var Ctor = value && value.constructor,
          proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

      return value === proto;
    }

    /**
     * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` if suitable for strict
     *  equality comparisons, else `false`.
     */
    function isStrictComparable(value) {
      return value === value && !isObject(value);
    }

    /**
     * A specialized version of `matchesProperty` for source values suitable
     * for strict equality comparisons, i.e. `===`.
     *
     * @private
     * @param {string} key The key of the property to get.
     * @param {*} srcValue The value to match.
     * @returns {Function} Returns the new spec function.
     */
    function matchesStrictComparable(key, srcValue) {
      return function(object) {
        if (object == null) {
          return false;
        }
        return object[key] === srcValue &&
          (srcValue !== undefined || (key in Object(object)));
      };
    }

    /**
     * A specialized version of `_.memoize` which clears the memoized function's
     * cache when it exceeds `MAX_MEMOIZE_SIZE`.
     *
     * @private
     * @param {Function} func The function to have its output memoized.
     * @returns {Function} Returns the new memoized function.
     */
    function memoizeCapped(func) {
      var result = memoize(func, function(key) {
        if (cache.size === MAX_MEMOIZE_SIZE) {
          cache.clear();
        }
        return key;
      });

      var cache = result.cache;
      return result;
    }

    /**
     * Merges the function metadata of `source` into `data`.
     *
     * Merging metadata reduces the number of wrappers used to invoke a function.
     * This is possible because methods like `_.bind`, `_.curry`, and `_.partial`
     * may be applied regardless of execution order. Methods like `_.ary` and
     * `_.rearg` modify function arguments, making the order in which they are
     * executed important, preventing the merging of metadata. However, we make
     * an exception for a safe combined case where curried functions have `_.ary`
     * and or `_.rearg` applied.
     *
     * @private
     * @param {Array} data The destination metadata.
     * @param {Array} source The source metadata.
     * @returns {Array} Returns `data`.
     */
    function mergeData(data, source) {
      var bitmask = data[1],
          srcBitmask = source[1],
          newBitmask = bitmask | srcBitmask,
          isCommon = newBitmask < (WRAP_BIND_FLAG | WRAP_BIND_KEY_FLAG | WRAP_ARY_FLAG);

      var isCombo =
        ((srcBitmask == WRAP_ARY_FLAG) && (bitmask == WRAP_CURRY_FLAG)) ||
        ((srcBitmask == WRAP_ARY_FLAG) && (bitmask == WRAP_REARG_FLAG) && (data[7].length <= source[8])) ||
        ((srcBitmask == (WRAP_ARY_FLAG | WRAP_REARG_FLAG)) && (source[7].length <= source[8]) && (bitmask == WRAP_CURRY_FLAG));

      // Exit early if metadata can't be merged.
      if (!(isCommon || isCombo)) {
        return data;
      }
      // Use source `thisArg` if available.
      if (srcBitmask & WRAP_BIND_FLAG) {
        data[2] = source[2];
        // Set when currying a bound function.
        newBitmask |= bitmask & WRAP_BIND_FLAG ? 0 : WRAP_CURRY_BOUND_FLAG;
      }
      // Compose partial arguments.
      var value = source[3];
      if (value) {
        var partials = data[3];
        data[3] = partials ? composeArgs(partials, value, source[4]) : value;
        data[4] = partials ? replaceHolders(data[3], PLACEHOLDER) : source[4];
      }
      // Compose partial right arguments.
      value = source[5];
      if (value) {
        partials = data[5];
        data[5] = partials ? composeArgsRight(partials, value, source[6]) : value;
        data[6] = partials ? replaceHolders(data[5], PLACEHOLDER) : source[6];
      }
      // Use source `argPos` if available.
      value = source[7];
      if (value) {
        data[7] = value;
      }
      // Use source `ary` if it's smaller.
      if (srcBitmask & WRAP_ARY_FLAG) {
        data[8] = data[8] == null ? source[8] : nativeMin(data[8], source[8]);
      }
      // Use source `arity` if one is not provided.
      if (data[9] == null) {
        data[9] = source[9];
      }
      // Use source `func` and merge bitmasks.
      data[0] = source[0];
      data[1] = newBitmask;

      return data;
    }

    /**
     * This function is like
     * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
     * except that it includes inherited enumerable properties.
     *
     * @private
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     */
    function nativeKeysIn(object) {
      var result = [];
      if (object != null) {
        for (var key in Object(object)) {
          result.push(key);
        }
      }
      return result;
    }

    /**
     * Converts `value` to a string using `Object.prototype.toString`.
     *
     * @private
     * @param {*} value The value to convert.
     * @returns {string} Returns the converted string.
     */
    function objectToString(value) {
      return nativeObjectToString.call(value);
    }

    /**
     * A specialized version of `baseRest` which transforms the rest array.
     *
     * @private
     * @param {Function} func The function to apply a rest parameter to.
     * @param {number} [start=func.length-1] The start position of the rest parameter.
     * @param {Function} transform The rest array transform.
     * @returns {Function} Returns the new function.
     */
    function overRest(func, start, transform) {
      start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
      return function() {
        var args = arguments,
            index = -1,
            length = nativeMax(args.length - start, 0),
            array = Array(length);

        while (++index < length) {
          array[index] = args[start + index];
        }
        index = -1;
        var otherArgs = Array(start + 1);
        while (++index < start) {
          otherArgs[index] = args[index];
        }
        otherArgs[start] = transform(array);
        return apply(func, this, otherArgs);
      };
    }

    /**
     * Gets the parent value at `path` of `object`.
     *
     * @private
     * @param {Object} object The object to query.
     * @param {Array} path The path to get the parent value of.
     * @returns {*} Returns the parent value.
     */
    function parent(object, path) {
      return path.length < 2 ? object : baseGet(object, baseSlice(path, 0, -1));
    }

    /**
     * Reorder `array` according to the specified indexes where the element at
     * the first index is assigned as the first element, the element at
     * the second index is assigned as the second element, and so on.
     *
     * @private
     * @param {Array} array The array to reorder.
     * @param {Array} indexes The arranged array indexes.
     * @returns {Array} Returns `array`.
     */
    function reorder(array, indexes) {
      var arrLength = array.length,
          length = nativeMin(indexes.length, arrLength),
          oldArray = copyArray(array);

      while (length--) {
        var index = indexes[length];
        array[length] = isIndex(index, arrLength) ? oldArray[index] : undefined;
      }
      return array;
    }

    /**
     * Sets metadata for `func`.
     *
     * **Note:** If this function becomes hot, i.e. is invoked a lot in a short
     * period of time, it will trip its breaker and transition to an identity
     * function to avoid garbage collection pauses in V8. See
     * [V8 issue 2070](https://bugs.chromium.org/p/v8/issues/detail?id=2070)
     * for more details.
     *
     * @private
     * @param {Function} func The function to associate metadata with.
     * @param {*} data The metadata.
     * @returns {Function} Returns `func`.
     */
    var setData = shortOut(baseSetData);

    /**
     * A simple wrapper around the global [`setTimeout`](https://mdn.io/setTimeout).
     *
     * @private
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay invocation.
     * @returns {number|Object} Returns the timer id or timeout object.
     */
    var setTimeout = ctxSetTimeout || function(func, wait) {
      return root.setTimeout(func, wait);
    };

    /**
     * Sets the `toString` method of `func` to return `string`.
     *
     * @private
     * @param {Function} func The function to modify.
     * @param {Function} string The `toString` result.
     * @returns {Function} Returns `func`.
     */
    var setToString = shortOut(baseSetToString);

    /**
     * Sets the `toString` method of `wrapper` to mimic the source of `reference`
     * with wrapper details in a comment at the top of the source body.
     *
     * @private
     * @param {Function} wrapper The function to modify.
     * @param {Function} reference The reference function.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @returns {Function} Returns `wrapper`.
     */
    function setWrapToString(wrapper, reference, bitmask) {
      var source = (reference + '');
      return setToString(wrapper, insertWrapDetails(source, updateWrapDetails(getWrapDetails(source), bitmask)));
    }

    /**
     * Creates a function that'll short out and invoke `identity` instead
     * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
     * milliseconds.
     *
     * @private
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new shortable function.
     */
    function shortOut(func) {
      var count = 0,
          lastCalled = 0;

      return function() {
        var stamp = nativeNow(),
            remaining = HOT_SPAN - (stamp - lastCalled);

        lastCalled = stamp;
        if (remaining > 0) {
          if (++count >= HOT_COUNT) {
            return arguments[0];
          }
        } else {
          count = 0;
        }
        return func.apply(undefined, arguments);
      };
    }

    /**
     * A specialized version of `_.shuffle` which mutates and sets the size of `array`.
     *
     * @private
     * @param {Array} array The array to shuffle.
     * @param {number} [size=array.length] The size of `array`.
     * @returns {Array} Returns `array`.
     */
    function shuffleSelf(array, size) {
      var index = -1,
          length = array.length,
          lastIndex = length - 1;

      size = size === undefined ? length : size;
      while (++index < size) {
        var rand = baseRandom(index, lastIndex),
            value = array[rand];

        array[rand] = array[index];
        array[index] = value;
      }
      array.length = size;
      return array;
    }

    /**
     * Converts `string` to a property path array.
     *
     * @private
     * @param {string} string The string to convert.
     * @returns {Array} Returns the property path array.
     */
    var stringToPath = memoizeCapped(function(string) {
      var result = [];
      if (reLeadingDot.test(string)) {
        result.push('');
      }
      string.replace(rePropName, function(match, number, quote, string) {
        result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
      });
      return result;
    });

    /**
     * Converts `value` to a string key if it's not a string or symbol.
     *
     * @private
     * @param {*} value The value to inspect.
     * @returns {string|symbol} Returns the key.
     */
    function toKey(value) {
      if (typeof value == 'string' || isSymbol(value)) {
        return value;
      }
      var result = (value + '');
      return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
    }

    /**
     * Converts `func` to its source code.
     *
     * @private
     * @param {Function} func The function to convert.
     * @returns {string} Returns the source code.
     */
    function toSource(func) {
      if (func != null) {
        try {
          return funcToString.call(func);
        } catch (e) {}
        try {
          return (func + '');
        } catch (e) {}
      }
      return '';
    }

    /**
     * Updates wrapper `details` based on `bitmask` flags.
     *
     * @private
     * @returns {Array} details The details to modify.
     * @param {number} bitmask The bitmask flags. See `createWrap` for more details.
     * @returns {Array} Returns `details`.
     */
    function updateWrapDetails(details, bitmask) {
      arrayEach(wrapFlags, function(pair) {
        var value = '_.' + pair[0];
        if ((bitmask & pair[1]) && !arrayIncludes(details, value)) {
          details.push(value);
        }
      });
      return details.sort();
    }

    /**
     * Creates a clone of `wrapper`.
     *
     * @private
     * @param {Object} wrapper The wrapper to clone.
     * @returns {Object} Returns the cloned wrapper.
     */
    function wrapperClone(wrapper) {
      if (wrapper instanceof LazyWrapper) {
        return wrapper.clone();
      }
      var result = new LodashWrapper(wrapper.__wrapped__, wrapper.__chain__);
      result.__actions__ = copyArray(wrapper.__actions__);
      result.__index__  = wrapper.__index__;
      result.__values__ = wrapper.__values__;
      return result;
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates an array of elements split into groups the length of `size`.
     * If `array` can't be split evenly, the final chunk will be the remaining
     * elements.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to process.
     * @param {number} [size=1] The length of each chunk
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the new array of chunks.
     * @example
     *
     * _.chunk(['a', 'b', 'c', 'd'], 2);
     * // => [['a', 'b'], ['c', 'd']]
     *
     * _.chunk(['a', 'b', 'c', 'd'], 3);
     * // => [['a', 'b', 'c'], ['d']]
     */
    function chunk(array, size, guard) {
      if ((guard ? isIterateeCall(array, size, guard) : size === undefined)) {
        size = 1;
      } else {
        size = nativeMax(toInteger(size), 0);
      }
      var length = array == null ? 0 : array.length;
      if (!length || size < 1) {
        return [];
      }
      var index = 0,
          resIndex = 0,
          result = Array(nativeCeil(length / size));

      while (index < length) {
        result[resIndex++] = baseSlice(array, index, (index += size));
      }
      return result;
    }

    /**
     * Creates an array with all falsey values removed. The values `false`, `null`,
     * `0`, `""`, `undefined`, and `NaN` are falsey.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to compact.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.compact([0, 1, false, 2, '', 3]);
     * // => [1, 2, 3]
     */
    function compact(array) {
      var index = -1,
          length = array == null ? 0 : array.length,
          resIndex = 0,
          result = [];

      while (++index < length) {
        var value = array[index];
        if (value) {
          result[resIndex++] = value;
        }
      }
      return result;
    }

    /**
     * Creates a new array concatenating `array` with any additional arrays
     * and/or values.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to concatenate.
     * @param {...*} [values] The values to concatenate.
     * @returns {Array} Returns the new concatenated array.
     * @example
     *
     * var array = [1];
     * var other = _.concat(array, 2, [3], [[4]]);
     *
     * console.log(other);
     * // => [1, 2, 3, [4]]
     *
     * console.log(array);
     * // => [1]
     */
    function concat() {
      var length = arguments.length;
      if (!length) {
        return [];
      }
      var args = Array(length - 1),
          array = arguments[0],
          index = length;

      while (index--) {
        args[index - 1] = arguments[index];
      }
      return arrayPush(isArray(array) ? copyArray(array) : [array], baseFlatten(args, 1));
    }

    /**
     * Creates an array of `array` values not included in the other given arrays
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons. The order and references of result values are
     * determined by the first array.
     *
     * **Note:** Unlike `_.pullAll`, this method returns a new array.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {...Array} [values] The values to exclude.
     * @returns {Array} Returns the new array of filtered values.
     * @see _.without, _.xor
     * @example
     *
     * _.difference([2, 1], [2, 3]);
     * // => [1]
     */
    var difference = baseRest(function(array, values) {
      return isArrayLikeObject(array)
        ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true))
        : [];
    });

    /**
     * This method is like `_.difference` except that it accepts `iteratee` which
     * is invoked for each element of `array` and `values` to generate the criterion
     * by which they're compared. The order and references of result values are
     * determined by the first array. The iteratee is invoked with one argument:
     * (value).
     *
     * **Note:** Unlike `_.pullAllBy`, this method returns a new array.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {...Array} [values] The values to exclude.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.differenceBy([2.1, 1.2], [2.3, 3.4], Math.floor);
     * // => [1.2]
     *
     * // The `_.property` iteratee shorthand.
     * _.differenceBy([{ 'x': 2 }, { 'x': 1 }], [{ 'x': 1 }], 'x');
     * // => [{ 'x': 2 }]
     */
    var differenceBy = baseRest(function(array, values) {
      var iteratee = last(values);
      if (isArrayLikeObject(iteratee)) {
        iteratee = undefined;
      }
      return isArrayLikeObject(array)
        ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true), getIteratee(iteratee, 2))
        : [];
    });

    /**
     * This method is like `_.difference` except that it accepts `comparator`
     * which is invoked to compare elements of `array` to `values`. The order and
     * references of result values are determined by the first array. The comparator
     * is invoked with two arguments: (arrVal, othVal).
     *
     * **Note:** Unlike `_.pullAllWith`, this method returns a new array.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {...Array} [values] The values to exclude.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
     *
     * _.differenceWith(objects, [{ 'x': 1, 'y': 2 }], _.isEqual);
     * // => [{ 'x': 2, 'y': 1 }]
     */
    var differenceWith = baseRest(function(array, values) {
      var comparator = last(values);
      if (isArrayLikeObject(comparator)) {
        comparator = undefined;
      }
      return isArrayLikeObject(array)
        ? baseDifference(array, baseFlatten(values, 1, isArrayLikeObject, true), undefined, comparator)
        : [];
    });

    /**
     * Creates a slice of `array` with `n` elements dropped from the beginning.
     *
     * @static
     * @memberOf _
     * @since 0.5.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to drop.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.drop([1, 2, 3]);
     * // => [2, 3]
     *
     * _.drop([1, 2, 3], 2);
     * // => [3]
     *
     * _.drop([1, 2, 3], 5);
     * // => []
     *
     * _.drop([1, 2, 3], 0);
     * // => [1, 2, 3]
     */
    function drop(array, n, guard) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      n = (guard || n === undefined) ? 1 : toInteger(n);
      return baseSlice(array, n < 0 ? 0 : n, length);
    }

    /**
     * Creates a slice of `array` with `n` elements dropped from the end.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to drop.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.dropRight([1, 2, 3]);
     * // => [1, 2]
     *
     * _.dropRight([1, 2, 3], 2);
     * // => [1]
     *
     * _.dropRight([1, 2, 3], 5);
     * // => []
     *
     * _.dropRight([1, 2, 3], 0);
     * // => [1, 2, 3]
     */
    function dropRight(array, n, guard) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      n = (guard || n === undefined) ? 1 : toInteger(n);
      n = length - n;
      return baseSlice(array, 0, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` excluding elements dropped from the end.
     * Elements are dropped until `predicate` returns falsey. The predicate is
     * invoked with three arguments: (value, index, array).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * _.dropRightWhile(users, function(o) { return !o.active; });
     * // => objects for ['barney']
     *
     * // The `_.matches` iteratee shorthand.
     * _.dropRightWhile(users, { 'user': 'pebbles', 'active': false });
     * // => objects for ['barney', 'fred']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.dropRightWhile(users, ['active', false]);
     * // => objects for ['barney']
     *
     * // The `_.property` iteratee shorthand.
     * _.dropRightWhile(users, 'active');
     * // => objects for ['barney', 'fred', 'pebbles']
     */
    function dropRightWhile(array, predicate) {
      return (array && array.length)
        ? baseWhile(array, getIteratee(predicate, 3), true, true)
        : [];
    }

    /**
     * Creates a slice of `array` excluding elements dropped from the beginning.
     * Elements are dropped until `predicate` returns falsey. The predicate is
     * invoked with three arguments: (value, index, array).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * _.dropWhile(users, function(o) { return !o.active; });
     * // => objects for ['pebbles']
     *
     * // The `_.matches` iteratee shorthand.
     * _.dropWhile(users, { 'user': 'barney', 'active': false });
     * // => objects for ['fred', 'pebbles']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.dropWhile(users, ['active', false]);
     * // => objects for ['pebbles']
     *
     * // The `_.property` iteratee shorthand.
     * _.dropWhile(users, 'active');
     * // => objects for ['barney', 'fred', 'pebbles']
     */
    function dropWhile(array, predicate) {
      return (array && array.length)
        ? baseWhile(array, getIteratee(predicate, 3), true)
        : [];
    }

    /**
     * Fills elements of `array` with `value` from `start` up to, but not
     * including, `end`.
     *
     * **Note:** This method mutates `array`.
     *
     * @static
     * @memberOf _
     * @since 3.2.0
     * @category Array
     * @param {Array} array The array to fill.
     * @param {*} value The value to fill `array` with.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3];
     *
     * _.fill(array, 'a');
     * console.log(array);
     * // => ['a', 'a', 'a']
     *
     * _.fill(Array(3), 2);
     * // => [2, 2, 2]
     *
     * _.fill([4, 6, 8, 10], '*', 1, 3);
     * // => [4, '*', '*', 10]
     */
    function fill(array, value, start, end) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      if (start && typeof start != 'number' && isIterateeCall(array, value, start)) {
        start = 0;
        end = length;
      }
      return baseFill(array, value, start, end);
    }

    /**
     * This method is like `_.find` except that it returns the index of the first
     * element `predicate` returns truthy for instead of the element itself.
     *
     * @static
     * @memberOf _
     * @since 1.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param {number} [fromIndex=0] The index to search from.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * _.findIndex(users, function(o) { return o.user == 'barney'; });
     * // => 0
     *
     * // The `_.matches` iteratee shorthand.
     * _.findIndex(users, { 'user': 'fred', 'active': false });
     * // => 1
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.findIndex(users, ['active', false]);
     * // => 0
     *
     * // The `_.property` iteratee shorthand.
     * _.findIndex(users, 'active');
     * // => 2
     */
    function findIndex(array, predicate, fromIndex) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return -1;
      }
      var index = fromIndex == null ? 0 : toInteger(fromIndex);
      if (index < 0) {
        index = nativeMax(length + index, 0);
      }
      return baseFindIndex(array, getIteratee(predicate, 3), index);
    }

    /**
     * This method is like `_.findIndex` except that it iterates over elements
     * of `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param {number} [fromIndex=array.length-1] The index to search from.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * _.findLastIndex(users, function(o) { return o.user == 'pebbles'; });
     * // => 2
     *
     * // The `_.matches` iteratee shorthand.
     * _.findLastIndex(users, { 'user': 'barney', 'active': true });
     * // => 0
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.findLastIndex(users, ['active', false]);
     * // => 2
     *
     * // The `_.property` iteratee shorthand.
     * _.findLastIndex(users, 'active');
     * // => 0
     */
    function findLastIndex(array, predicate, fromIndex) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return -1;
      }
      var index = length - 1;
      if (fromIndex !== undefined) {
        index = toInteger(fromIndex);
        index = fromIndex < 0
          ? nativeMax(length + index, 0)
          : nativeMin(index, length - 1);
      }
      return baseFindIndex(array, getIteratee(predicate, 3), index, true);
    }

    /**
     * Flattens `array` a single level deep.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to flatten.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flatten([1, [2, [3, [4]], 5]]);
     * // => [1, 2, [3, [4]], 5]
     */
    function flatten(array) {
      var length = array == null ? 0 : array.length;
      return length ? baseFlatten(array, 1) : [];
    }

    /**
     * Recursively flattens `array`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to flatten.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * _.flattenDeep([1, [2, [3, [4]], 5]]);
     * // => [1, 2, 3, 4, 5]
     */
    function flattenDeep(array) {
      var length = array == null ? 0 : array.length;
      return length ? baseFlatten(array, INFINITY) : [];
    }

    /**
     * Recursively flatten `array` up to `depth` times.
     *
     * @static
     * @memberOf _
     * @since 4.4.0
     * @category Array
     * @param {Array} array The array to flatten.
     * @param {number} [depth=1] The maximum recursion depth.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * var array = [1, [2, [3, [4]], 5]];
     *
     * _.flattenDepth(array, 1);
     * // => [1, 2, [3, [4]], 5]
     *
     * _.flattenDepth(array, 2);
     * // => [1, 2, 3, [4], 5]
     */
    function flattenDepth(array, depth) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      depth = depth === undefined ? 1 : toInteger(depth);
      return baseFlatten(array, depth);
    }

    /**
     * The inverse of `_.toPairs`; this method returns an object composed
     * from key-value `pairs`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} pairs The key-value pairs.
     * @returns {Object} Returns the new object.
     * @example
     *
     * _.fromPairs([['a', 1], ['b', 2]]);
     * // => { 'a': 1, 'b': 2 }
     */
    function fromPairs(pairs) {
      var index = -1,
          length = pairs == null ? 0 : pairs.length,
          result = {};

      while (++index < length) {
        var pair = pairs[index];
        result[pair[0]] = pair[1];
      }
      return result;
    }

    /**
     * Gets the first element of `array`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @alias first
     * @category Array
     * @param {Array} array The array to query.
     * @returns {*} Returns the first element of `array`.
     * @example
     *
     * _.head([1, 2, 3]);
     * // => 1
     *
     * _.head([]);
     * // => undefined
     */
    function head(array) {
      return (array && array.length) ? array[0] : undefined;
    }

    /**
     * Gets the index at which the first occurrence of `value` is found in `array`
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons. If `fromIndex` is negative, it's used as the
     * offset from the end of `array`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {*} value The value to search for.
     * @param {number} [fromIndex=0] The index to search from.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.indexOf([1, 2, 1, 2], 2);
     * // => 1
     *
     * // Search from the `fromIndex`.
     * _.indexOf([1, 2, 1, 2], 2, 2);
     * // => 3
     */
    function indexOf(array, value, fromIndex) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return -1;
      }
      var index = fromIndex == null ? 0 : toInteger(fromIndex);
      if (index < 0) {
        index = nativeMax(length + index, 0);
      }
      return baseIndexOf(array, value, index);
    }

    /**
     * Gets all but the last element of `array`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to query.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.initial([1, 2, 3]);
     * // => [1, 2]
     */
    function initial(array) {
      var length = array == null ? 0 : array.length;
      return length ? baseSlice(array, 0, -1) : [];
    }

    /**
     * Creates an array of unique values that are included in all given arrays
     * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons. The order and references of result values are
     * determined by the first array.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of intersecting values.
     * @example
     *
     * _.intersection([2, 1], [2, 3]);
     * // => [2]
     */
    var intersection = baseRest(function(arrays) {
      var mapped = arrayMap(arrays, castArrayLikeObject);
      return (mapped.length && mapped[0] === arrays[0])
        ? baseIntersection(mapped)
        : [];
    });

    /**
     * This method is like `_.intersection` except that it accepts `iteratee`
     * which is invoked for each element of each `arrays` to generate the criterion
     * by which they're compared. The order and references of result values are
     * determined by the first array. The iteratee is invoked with one argument:
     * (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns the new array of intersecting values.
     * @example
     *
     * _.intersectionBy([2.1, 1.2], [2.3, 3.4], Math.floor);
     * // => [2.1]
     *
     * // The `_.property` iteratee shorthand.
     * _.intersectionBy([{ 'x': 1 }], [{ 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }]
     */
    var intersectionBy = baseRest(function(arrays) {
      var iteratee = last(arrays),
          mapped = arrayMap(arrays, castArrayLikeObject);

      if (iteratee === last(mapped)) {
        iteratee = undefined;
      } else {
        mapped.pop();
      }
      return (mapped.length && mapped[0] === arrays[0])
        ? baseIntersection(mapped, getIteratee(iteratee, 2))
        : [];
    });

    /**
     * This method is like `_.intersection` except that it accepts `comparator`
     * which is invoked to compare elements of `arrays`. The order and references
     * of result values are determined by the first array. The comparator is
     * invoked with two arguments: (arrVal, othVal).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of intersecting values.
     * @example
     *
     * var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
     * var others = [{ 'x': 1, 'y': 1 }, { 'x': 1, 'y': 2 }];
     *
     * _.intersectionWith(objects, others, _.isEqual);
     * // => [{ 'x': 1, 'y': 2 }]
     */
    var intersectionWith = baseRest(function(arrays) {
      var comparator = last(arrays),
          mapped = arrayMap(arrays, castArrayLikeObject);

      comparator = typeof comparator == 'function' ? comparator : undefined;
      if (comparator) {
        mapped.pop();
      }
      return (mapped.length && mapped[0] === arrays[0])
        ? baseIntersection(mapped, undefined, comparator)
        : [];
    });

    /**
     * Converts all elements in `array` into a string separated by `separator`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to convert.
     * @param {string} [separator=','] The element separator.
     * @returns {string} Returns the joined string.
     * @example
     *
     * _.join(['a', 'b', 'c'], '~');
     * // => 'a~b~c'
     */
    function join(array, separator) {
      return array == null ? '' : nativeJoin.call(array, separator);
    }

    /**
     * Gets the last element of `array`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to query.
     * @returns {*} Returns the last element of `array`.
     * @example
     *
     * _.last([1, 2, 3]);
     * // => 3
     */
    function last(array) {
      var length = array == null ? 0 : array.length;
      return length ? array[length - 1] : undefined;
    }

    /**
     * This method is like `_.indexOf` except that it iterates over elements of
     * `array` from right to left.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {*} value The value to search for.
     * @param {number} [fromIndex=array.length-1] The index to search from.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.lastIndexOf([1, 2, 1, 2], 2);
     * // => 3
     *
     * // Search from the `fromIndex`.
     * _.lastIndexOf([1, 2, 1, 2], 2, 2);
     * // => 1
     */
    function lastIndexOf(array, value, fromIndex) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return -1;
      }
      var index = length;
      if (fromIndex !== undefined) {
        index = toInteger(fromIndex);
        index = index < 0 ? nativeMax(length + index, 0) : nativeMin(index, length - 1);
      }
      return value === value
        ? strictLastIndexOf(array, value, index)
        : baseFindIndex(array, baseIsNaN, index, true);
    }

    /**
     * Gets the element at index `n` of `array`. If `n` is negative, the nth
     * element from the end is returned.
     *
     * @static
     * @memberOf _
     * @since 4.11.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=0] The index of the element to return.
     * @returns {*} Returns the nth element of `array`.
     * @example
     *
     * var array = ['a', 'b', 'c', 'd'];
     *
     * _.nth(array, 1);
     * // => 'b'
     *
     * _.nth(array, -2);
     * // => 'c';
     */
    function nth(array, n) {
      return (array && array.length) ? baseNth(array, toInteger(n)) : undefined;
    }

    /**
     * Removes all given values from `array` using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * **Note:** Unlike `_.without`, this method mutates `array`. Use `_.remove`
     * to remove elements from an array by predicate.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {...*} [values] The values to remove.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = ['a', 'b', 'c', 'a', 'b', 'c'];
     *
     * _.pull(array, 'a', 'c');
     * console.log(array);
     * // => ['b', 'b']
     */
    var pull = baseRest(pullAll);

    /**
     * This method is like `_.pull` except that it accepts an array of values to remove.
     *
     * **Note:** Unlike `_.difference`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {Array} values The values to remove.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = ['a', 'b', 'c', 'a', 'b', 'c'];
     *
     * _.pullAll(array, ['a', 'c']);
     * console.log(array);
     * // => ['b', 'b']
     */
    function pullAll(array, values) {
      return (array && array.length && values && values.length)
        ? basePullAll(array, values)
        : array;
    }

    /**
     * This method is like `_.pullAll` except that it accepts `iteratee` which is
     * invoked for each element of `array` and `values` to generate the criterion
     * by which they're compared. The iteratee is invoked with one argument: (value).
     *
     * **Note:** Unlike `_.differenceBy`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {Array} values The values to remove.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [{ 'x': 1 }, { 'x': 2 }, { 'x': 3 }, { 'x': 1 }];
     *
     * _.pullAllBy(array, [{ 'x': 1 }, { 'x': 3 }], 'x');
     * console.log(array);
     * // => [{ 'x': 2 }]
     */
    function pullAllBy(array, values, iteratee) {
      return (array && array.length && values && values.length)
        ? basePullAll(array, values, getIteratee(iteratee, 2))
        : array;
    }

    /**
     * This method is like `_.pullAll` except that it accepts `comparator` which
     * is invoked to compare elements of `array` to `values`. The comparator is
     * invoked with two arguments: (arrVal, othVal).
     *
     * **Note:** Unlike `_.differenceWith`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @since 4.6.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {Array} values The values to remove.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [{ 'x': 1, 'y': 2 }, { 'x': 3, 'y': 4 }, { 'x': 5, 'y': 6 }];
     *
     * _.pullAllWith(array, [{ 'x': 3, 'y': 4 }], _.isEqual);
     * console.log(array);
     * // => [{ 'x': 1, 'y': 2 }, { 'x': 5, 'y': 6 }]
     */
    function pullAllWith(array, values, comparator) {
      return (array && array.length && values && values.length)
        ? basePullAll(array, values, undefined, comparator)
        : array;
    }

    /**
     * Removes elements from `array` corresponding to `indexes` and returns an
     * array of removed elements.
     *
     * **Note:** Unlike `_.at`, this method mutates `array`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {...(number|number[])} [indexes] The indexes of elements to remove.
     * @returns {Array} Returns the new array of removed elements.
     * @example
     *
     * var array = ['a', 'b', 'c', 'd'];
     * var pulled = _.pullAt(array, [1, 3]);
     *
     * console.log(array);
     * // => ['a', 'c']
     *
     * console.log(pulled);
     * // => ['b', 'd']
     */
    var pullAt = flatRest(function(array, indexes) {
      var length = array == null ? 0 : array.length,
          result = baseAt(array, indexes);

      basePullAt(array, arrayMap(indexes, function(index) {
        return isIndex(index, length) ? +index : index;
      }).sort(compareAscending));

      return result;
    });

    /**
     * Removes all elements from `array` that `predicate` returns truthy for
     * and returns an array of the removed elements. The predicate is invoked
     * with three arguments: (value, index, array).
     *
     * **Note:** Unlike `_.filter`, this method mutates `array`. Use `_.pull`
     * to pull elements from an array by value.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new array of removed elements.
     * @example
     *
     * var array = [1, 2, 3, 4];
     * var evens = _.remove(array, function(n) {
     *   return n % 2 == 0;
     * });
     *
     * console.log(array);
     * // => [1, 3]
     *
     * console.log(evens);
     * // => [2, 4]
     */
    function remove(array, predicate) {
      var result = [];
      if (!(array && array.length)) {
        return result;
      }
      var index = -1,
          indexes = [],
          length = array.length;

      predicate = getIteratee(predicate, 3);
      while (++index < length) {
        var value = array[index];
        if (predicate(value, index, array)) {
          result.push(value);
          indexes.push(index);
        }
      }
      basePullAt(array, indexes);
      return result;
    }

    /**
     * Reverses `array` so that the first element becomes the last, the second
     * element becomes the second to last, and so on.
     *
     * **Note:** This method mutates `array` and is based on
     * [`Array#reverse`](https://mdn.io/Array/reverse).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to modify.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3];
     *
     * _.reverse(array);
     * // => [3, 2, 1]
     *
     * console.log(array);
     * // => [3, 2, 1]
     */
    function reverse(array) {
      return array == null ? array : nativeReverse.call(array);
    }

    /**
     * Creates a slice of `array` from `start` up to, but not including, `end`.
     *
     * **Note:** This method is used instead of
     * [`Array#slice`](https://mdn.io/Array/slice) to ensure dense arrays are
     * returned.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to slice.
     * @param {number} [start=0] The start position.
     * @param {number} [end=array.length] The end position.
     * @returns {Array} Returns the slice of `array`.
     */
    function slice(array, start, end) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      if (end && typeof end != 'number' && isIterateeCall(array, start, end)) {
        start = 0;
        end = length;
      }
      else {
        start = start == null ? 0 : toInteger(start);
        end = end === undefined ? length : toInteger(end);
      }
      return baseSlice(array, start, end);
    }

    /**
     * Uses a binary search to determine the lowest index at which `value`
     * should be inserted into `array` in order to maintain its sort order.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedIndex([30, 50], 40);
     * // => 1
     */
    function sortedIndex(array, value) {
      return baseSortedIndex(array, value);
    }

    /**
     * This method is like `_.sortedIndex` except that it accepts `iteratee`
     * which is invoked for `value` and each element of `array` to compute their
     * sort ranking. The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * var objects = [{ 'x': 4 }, { 'x': 5 }];
     *
     * _.sortedIndexBy(objects, { 'x': 4 }, function(o) { return o.x; });
     * // => 0
     *
     * // The `_.property` iteratee shorthand.
     * _.sortedIndexBy(objects, { 'x': 4 }, 'x');
     * // => 0
     */
    function sortedIndexBy(array, value, iteratee) {
      return baseSortedIndexBy(array, value, getIteratee(iteratee, 2));
    }

    /**
     * This method is like `_.indexOf` except that it performs a binary
     * search on a sorted `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {*} value The value to search for.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.sortedIndexOf([4, 5, 5, 5, 6], 5);
     * // => 1
     */
    function sortedIndexOf(array, value) {
      var length = array == null ? 0 : array.length;
      if (length) {
        var index = baseSortedIndex(array, value);
        if (index < length && eq(array[index], value)) {
          return index;
        }
      }
      return -1;
    }

    /**
     * This method is like `_.sortedIndex` except that it returns the highest
     * index at which `value` should be inserted into `array` in order to
     * maintain its sort order.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedLastIndex([4, 5, 5, 5, 6], 5);
     * // => 4
     */
    function sortedLastIndex(array, value) {
      return baseSortedIndex(array, value, true);
    }

    /**
     * This method is like `_.sortedLastIndex` except that it accepts `iteratee`
     * which is invoked for `value` and each element of `array` to compute their
     * sort ranking. The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The sorted array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * var objects = [{ 'x': 4 }, { 'x': 5 }];
     *
     * _.sortedLastIndexBy(objects, { 'x': 4 }, function(o) { return o.x; });
     * // => 1
     *
     * // The `_.property` iteratee shorthand.
     * _.sortedLastIndexBy(objects, { 'x': 4 }, 'x');
     * // => 1
     */
    function sortedLastIndexBy(array, value, iteratee) {
      return baseSortedIndexBy(array, value, getIteratee(iteratee, 2), true);
    }

    /**
     * This method is like `_.lastIndexOf` except that it performs a binary
     * search on a sorted `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {*} value The value to search for.
     * @returns {number} Returns the index of the matched value, else `-1`.
     * @example
     *
     * _.sortedLastIndexOf([4, 5, 5, 5, 6], 5);
     * // => 3
     */
    function sortedLastIndexOf(array, value) {
      var length = array == null ? 0 : array.length;
      if (length) {
        var index = baseSortedIndex(array, value, true) - 1;
        if (eq(array[index], value)) {
          return index;
        }
      }
      return -1;
    }

    /**
     * This method is like `_.uniq` except that it's designed and optimized
     * for sorted arrays.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @returns {Array} Returns the new duplicate free array.
     * @example
     *
     * _.sortedUniq([1, 1, 2]);
     * // => [1, 2]
     */
    function sortedUniq(array) {
      return (array && array.length)
        ? baseSortedUniq(array)
        : [];
    }

    /**
     * This method is like `_.uniqBy` except that it's designed and optimized
     * for sorted arrays.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {Function} [iteratee] The iteratee invoked per element.
     * @returns {Array} Returns the new duplicate free array.
     * @example
     *
     * _.sortedUniqBy([1.1, 1.2, 2.3, 2.4], Math.floor);
     * // => [1.1, 2.3]
     */
    function sortedUniqBy(array, iteratee) {
      return (array && array.length)
        ? baseSortedUniq(array, getIteratee(iteratee, 2))
        : [];
    }

    /**
     * Gets all but the first element of `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.tail([1, 2, 3]);
     * // => [2, 3]
     */
    function tail(array) {
      var length = array == null ? 0 : array.length;
      return length ? baseSlice(array, 1, length) : [];
    }

    /**
     * Creates a slice of `array` with `n` elements taken from the beginning.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to take.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.take([1, 2, 3]);
     * // => [1]
     *
     * _.take([1, 2, 3], 2);
     * // => [1, 2]
     *
     * _.take([1, 2, 3], 5);
     * // => [1, 2, 3]
     *
     * _.take([1, 2, 3], 0);
     * // => []
     */
    function take(array, n, guard) {
      if (!(array && array.length)) {
        return [];
      }
      n = (guard || n === undefined) ? 1 : toInteger(n);
      return baseSlice(array, 0, n < 0 ? 0 : n);
    }

    /**
     * Creates a slice of `array` with `n` elements taken from the end.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {number} [n=1] The number of elements to take.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * _.takeRight([1, 2, 3]);
     * // => [3]
     *
     * _.takeRight([1, 2, 3], 2);
     * // => [2, 3]
     *
     * _.takeRight([1, 2, 3], 5);
     * // => [1, 2, 3]
     *
     * _.takeRight([1, 2, 3], 0);
     * // => []
     */
    function takeRight(array, n, guard) {
      var length = array == null ? 0 : array.length;
      if (!length) {
        return [];
      }
      n = (guard || n === undefined) ? 1 : toInteger(n);
      n = length - n;
      return baseSlice(array, n < 0 ? 0 : n, length);
    }

    /**
     * Creates a slice of `array` with elements taken from the end. Elements are
     * taken until `predicate` returns falsey. The predicate is invoked with
     * three arguments: (value, index, array).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': true },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': false }
     * ];
     *
     * _.takeRightWhile(users, function(o) { return !o.active; });
     * // => objects for ['fred', 'pebbles']
     *
     * // The `_.matches` iteratee shorthand.
     * _.takeRightWhile(users, { 'user': 'pebbles', 'active': false });
     * // => objects for ['pebbles']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.takeRightWhile(users, ['active', false]);
     * // => objects for ['fred', 'pebbles']
     *
     * // The `_.property` iteratee shorthand.
     * _.takeRightWhile(users, 'active');
     * // => []
     */
    function takeRightWhile(array, predicate) {
      return (array && array.length)
        ? baseWhile(array, getIteratee(predicate, 3), false, true)
        : [];
    }

    /**
     * Creates a slice of `array` with elements taken from the beginning. Elements
     * are taken until `predicate` returns falsey. The predicate is invoked with
     * three arguments: (value, index, array).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Array
     * @param {Array} array The array to query.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the slice of `array`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'active': false },
     *   { 'user': 'fred',    'active': false },
     *   { 'user': 'pebbles', 'active': true }
     * ];
     *
     * _.takeWhile(users, function(o) { return !o.active; });
     * // => objects for ['barney', 'fred']
     *
     * // The `_.matches` iteratee shorthand.
     * _.takeWhile(users, { 'user': 'barney', 'active': false });
     * // => objects for ['barney']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.takeWhile(users, ['active', false]);
     * // => objects for ['barney', 'fred']
     *
     * // The `_.property` iteratee shorthand.
     * _.takeWhile(users, 'active');
     * // => []
     */
    function takeWhile(array, predicate) {
      return (array && array.length)
        ? baseWhile(array, getIteratee(predicate, 3))
        : [];
    }

    /**
     * Creates an array of unique values, in order, from all given arrays using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of combined values.
     * @example
     *
     * _.union([2], [1, 2]);
     * // => [2, 1]
     */
    var union = baseRest(function(arrays) {
      return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true));
    });

    /**
     * This method is like `_.union` except that it accepts `iteratee` which is
     * invoked for each element of each `arrays` to generate the criterion by
     * which uniqueness is computed. Result values are chosen from the first
     * array in which the value occurs. The iteratee is invoked with one argument:
     * (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns the new array of combined values.
     * @example
     *
     * _.unionBy([2.1], [1.2, 2.3], Math.floor);
     * // => [2.1, 1.2]
     *
     * // The `_.property` iteratee shorthand.
     * _.unionBy([{ 'x': 1 }], [{ 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }, { 'x': 2 }]
     */
    var unionBy = baseRest(function(arrays) {
      var iteratee = last(arrays);
      if (isArrayLikeObject(iteratee)) {
        iteratee = undefined;
      }
      return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true), getIteratee(iteratee, 2));
    });

    /**
     * This method is like `_.union` except that it accepts `comparator` which
     * is invoked to compare elements of `arrays`. Result values are chosen from
     * the first array in which the value occurs. The comparator is invoked
     * with two arguments: (arrVal, othVal).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of combined values.
     * @example
     *
     * var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
     * var others = [{ 'x': 1, 'y': 1 }, { 'x': 1, 'y': 2 }];
     *
     * _.unionWith(objects, others, _.isEqual);
     * // => [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }, { 'x': 1, 'y': 1 }]
     */
    var unionWith = baseRest(function(arrays) {
      var comparator = last(arrays);
      comparator = typeof comparator == 'function' ? comparator : undefined;
      return baseUniq(baseFlatten(arrays, 1, isArrayLikeObject, true), undefined, comparator);
    });

    /**
     * Creates a duplicate-free version of an array, using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons, in which only the first occurrence of each element
     * is kept. The order of result values is determined by the order they occur
     * in the array.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @returns {Array} Returns the new duplicate free array.
     * @example
     *
     * _.uniq([2, 1, 2]);
     * // => [2, 1]
     */
    function uniq(array) {
      return (array && array.length) ? baseUniq(array) : [];
    }

    /**
     * This method is like `_.uniq` except that it accepts `iteratee` which is
     * invoked for each element in `array` to generate the criterion by which
     * uniqueness is computed. The order of result values is determined by the
     * order they occur in the array. The iteratee is invoked with one argument:
     * (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns the new duplicate free array.
     * @example
     *
     * _.uniqBy([2.1, 1.2, 2.3], Math.floor);
     * // => [2.1, 1.2]
     *
     * // The `_.property` iteratee shorthand.
     * _.uniqBy([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }, { 'x': 2 }]
     */
    function uniqBy(array, iteratee) {
      return (array && array.length) ? baseUniq(array, getIteratee(iteratee, 2)) : [];
    }

    /**
     * This method is like `_.uniq` except that it accepts `comparator` which
     * is invoked to compare elements of `array`. The order of result values is
     * determined by the order they occur in the array.The comparator is invoked
     * with two arguments: (arrVal, othVal).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new duplicate free array.
     * @example
     *
     * var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }, { 'x': 1, 'y': 2 }];
     *
     * _.uniqWith(objects, _.isEqual);
     * // => [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }]
     */
    function uniqWith(array, comparator) {
      comparator = typeof comparator == 'function' ? comparator : undefined;
      return (array && array.length) ? baseUniq(array, undefined, comparator) : [];
    }

    /**
     * This method is like `_.zip` except that it accepts an array of grouped
     * elements and creates an array regrouping the elements to their pre-zip
     * configuration.
     *
     * @static
     * @memberOf _
     * @since 1.2.0
     * @category Array
     * @param {Array} array The array of grouped elements to process.
     * @returns {Array} Returns the new array of regrouped elements.
     * @example
     *
     * var zipped = _.zip(['a', 'b'], [1, 2], [true, false]);
     * // => [['a', 1, true], ['b', 2, false]]
     *
     * _.unzip(zipped);
     * // => [['a', 'b'], [1, 2], [true, false]]
     */
    function unzip(array) {
      if (!(array && array.length)) {
        return [];
      }
      var length = 0;
      array = arrayFilter(array, function(group) {
        if (isArrayLikeObject(group)) {
          length = nativeMax(group.length, length);
          return true;
        }
      });
      return baseTimes(length, function(index) {
        return arrayMap(array, baseProperty(index));
      });
    }

    /**
     * This method is like `_.unzip` except that it accepts `iteratee` to specify
     * how regrouped values should be combined. The iteratee is invoked with the
     * elements of each group: (...group).
     *
     * @static
     * @memberOf _
     * @since 3.8.0
     * @category Array
     * @param {Array} array The array of grouped elements to process.
     * @param {Function} [iteratee=_.identity] The function to combine
     *  regrouped values.
     * @returns {Array} Returns the new array of regrouped elements.
     * @example
     *
     * var zipped = _.zip([1, 2], [10, 20], [100, 200]);
     * // => [[1, 10, 100], [2, 20, 200]]
     *
     * _.unzipWith(zipped, _.add);
     * // => [3, 30, 300]
     */
    function unzipWith(array, iteratee) {
      if (!(array && array.length)) {
        return [];
      }
      var result = unzip(array);
      if (iteratee == null) {
        return result;
      }
      return arrayMap(result, function(group) {
        return apply(iteratee, undefined, group);
      });
    }

    /**
     * Creates an array excluding all given values using
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * for equality comparisons.
     *
     * **Note:** Unlike `_.pull`, this method returns a new array.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {Array} array The array to inspect.
     * @param {...*} [values] The values to exclude.
     * @returns {Array} Returns the new array of filtered values.
     * @see _.difference, _.xor
     * @example
     *
     * _.without([2, 1, 2, 3], 1, 2);
     * // => [3]
     */
    var without = baseRest(function(array, values) {
      return isArrayLikeObject(array)
        ? baseDifference(array, values)
        : [];
    });

    /**
     * Creates an array of unique values that is the
     * [symmetric difference](https://en.wikipedia.org/wiki/Symmetric_difference)
     * of the given arrays. The order of result values is determined by the order
     * they occur in the arrays.
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @returns {Array} Returns the new array of filtered values.
     * @see _.difference, _.without
     * @example
     *
     * _.xor([2, 1], [2, 3]);
     * // => [1, 3]
     */
    var xor = baseRest(function(arrays) {
      return baseXor(arrayFilter(arrays, isArrayLikeObject));
    });

    /**
     * This method is like `_.xor` except that it accepts `iteratee` which is
     * invoked for each element of each `arrays` to generate the criterion by
     * which by which they're compared. The order of result values is determined
     * by the order they occur in the arrays. The iteratee is invoked with one
     * argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * _.xorBy([2.1, 1.2], [2.3, 3.4], Math.floor);
     * // => [1.2, 3.4]
     *
     * // The `_.property` iteratee shorthand.
     * _.xorBy([{ 'x': 1 }], [{ 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 2 }]
     */
    var xorBy = baseRest(function(arrays) {
      var iteratee = last(arrays);
      if (isArrayLikeObject(iteratee)) {
        iteratee = undefined;
      }
      return baseXor(arrayFilter(arrays, isArrayLikeObject), getIteratee(iteratee, 2));
    });

    /**
     * This method is like `_.xor` except that it accepts `comparator` which is
     * invoked to compare elements of `arrays`. The order of result values is
     * determined by the order they occur in the arrays. The comparator is invoked
     * with two arguments: (arrVal, othVal).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Array
     * @param {...Array} [arrays] The arrays to inspect.
     * @param {Function} [comparator] The comparator invoked per element.
     * @returns {Array} Returns the new array of filtered values.
     * @example
     *
     * var objects = [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }];
     * var others = [{ 'x': 1, 'y': 1 }, { 'x': 1, 'y': 2 }];
     *
     * _.xorWith(objects, others, _.isEqual);
     * // => [{ 'x': 2, 'y': 1 }, { 'x': 1, 'y': 1 }]
     */
    var xorWith = baseRest(function(arrays) {
      var comparator = last(arrays);
      comparator = typeof comparator == 'function' ? comparator : undefined;
      return baseXor(arrayFilter(arrays, isArrayLikeObject), undefined, comparator);
    });

    /**
     * Creates an array of grouped elements, the first of which contains the
     * first elements of the given arrays, the second of which contains the
     * second elements of the given arrays, and so on.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Array
     * @param {...Array} [arrays] The arrays to process.
     * @returns {Array} Returns the new array of grouped elements.
     * @example
     *
     * _.zip(['a', 'b'], [1, 2], [true, false]);
     * // => [['a', 1, true], ['b', 2, false]]
     */
    var zip = baseRest(unzip);

    /**
     * This method is like `_.fromPairs` except that it accepts two arrays,
     * one of property identifiers and one of corresponding values.
     *
     * @static
     * @memberOf _
     * @since 0.4.0
     * @category Array
     * @param {Array} [props=[]] The property identifiers.
     * @param {Array} [values=[]] The property values.
     * @returns {Object} Returns the new object.
     * @example
     *
     * _.zipObject(['a', 'b'], [1, 2]);
     * // => { 'a': 1, 'b': 2 }
     */
    function zipObject(props, values) {
      return baseZipObject(props || [], values || [], assignValue);
    }

    /**
     * This method is like `_.zipObject` except that it supports property paths.
     *
     * @static
     * @memberOf _
     * @since 4.1.0
     * @category Array
     * @param {Array} [props=[]] The property identifiers.
     * @param {Array} [values=[]] The property values.
     * @returns {Object} Returns the new object.
     * @example
     *
     * _.zipObjectDeep(['a.b[0].c', 'a.b[1].d'], [1, 2]);
     * // => { 'a': { 'b': [{ 'c': 1 }, { 'd': 2 }] } }
     */
    function zipObjectDeep(props, values) {
      return baseZipObject(props || [], values || [], baseSet);
    }

    /**
     * This method is like `_.zip` except that it accepts `iteratee` to specify
     * how grouped values should be combined. The iteratee is invoked with the
     * elements of each group: (...group).
     *
     * @static
     * @memberOf _
     * @since 3.8.0
     * @category Array
     * @param {...Array} [arrays] The arrays to process.
     * @param {Function} [iteratee=_.identity] The function to combine
     *  grouped values.
     * @returns {Array} Returns the new array of grouped elements.
     * @example
     *
     * _.zipWith([1, 2], [10, 20], [100, 200], function(a, b, c) {
     *   return a + b + c;
     * });
     * // => [111, 222]
     */
    var zipWith = baseRest(function(arrays) {
      var length = arrays.length,
          iteratee = length > 1 ? arrays[length - 1] : undefined;

      iteratee = typeof iteratee == 'function' ? (arrays.pop(), iteratee) : undefined;
      return unzipWith(arrays, iteratee);
    });

    /*------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` wrapper instance that wraps `value` with explicit method
     * chain sequences enabled. The result of such sequences must be unwrapped
     * with `_#value`.
     *
     * @static
     * @memberOf _
     * @since 1.3.0
     * @category Seq
     * @param {*} value The value to wrap.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36 },
     *   { 'user': 'fred',    'age': 40 },
     *   { 'user': 'pebbles', 'age': 1 }
     * ];
     *
     * var youngest = _
     *   .chain(users)
     *   .sortBy('age')
     *   .map(function(o) {
     *     return o.user + ' is ' + o.age;
     *   })
     *   .head()
     *   .value();
     * // => 'pebbles is 1'
     */
    function chain(value) {
      var result = lodash(value);
      result.__chain__ = true;
      return result;
    }

    /**
     * This method invokes `interceptor` and returns `value`. The interceptor
     * is invoked with one argument; (value). The purpose of this method is to
     * "tap into" a method chain sequence in order to modify intermediate results.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Seq
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @returns {*} Returns `value`.
     * @example
     *
     * _([1, 2, 3])
     *  .tap(function(array) {
     *    // Mutate input array.
     *    array.pop();
     *  })
     *  .reverse()
     *  .value();
     * // => [2, 1]
     */
    function tap(value, interceptor) {
      interceptor(value);
      return value;
    }

    /**
     * This method is like `_.tap` except that it returns the result of `interceptor`.
     * The purpose of this method is to "pass thru" values replacing intermediate
     * results in a method chain sequence.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Seq
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @returns {*} Returns the result of `interceptor`.
     * @example
     *
     * _('  abc  ')
     *  .chain()
     *  .trim()
     *  .thru(function(value) {
     *    return [value];
     *  })
     *  .value();
     * // => ['abc']
     */
    function thru(value, interceptor) {
      return interceptor(value);
    }

    /**
     * This method is the wrapper version of `_.at`.
     *
     * @name at
     * @memberOf _
     * @since 1.0.0
     * @category Seq
     * @param {...(string|string[])} [paths] The property paths to pick.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }, 4] };
     *
     * _(object).at(['a[0].b.c', 'a[1]']).value();
     * // => [3, 4]
     */
    var wrapperAt = flatRest(function(paths) {
      var length = paths.length,
          start = length ? paths[0] : 0,
          value = this.__wrapped__,
          interceptor = function(object) { return baseAt(object, paths); };

      if (length > 1 || this.__actions__.length ||
          !(value instanceof LazyWrapper) || !isIndex(start)) {
        return this.thru(interceptor);
      }
      value = value.slice(start, +start + (length ? 1 : 0));
      value.__actions__.push({
        'func': thru,
        'args': [interceptor],
        'thisArg': undefined
      });
      return new LodashWrapper(value, this.__chain__).thru(function(array) {
        if (length && !array.length) {
          array.push(undefined);
        }
        return array;
      });
    });

    /**
     * Creates a `lodash` wrapper instance with explicit method chain sequences enabled.
     *
     * @name chain
     * @memberOf _
     * @since 0.1.0
     * @category Seq
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 }
     * ];
     *
     * // A sequence without explicit chaining.
     * _(users).head();
     * // => { 'user': 'barney', 'age': 36 }
     *
     * // A sequence with explicit chaining.
     * _(users)
     *   .chain()
     *   .head()
     *   .pick('user')
     *   .value();
     * // => { 'user': 'barney' }
     */
    function wrapperChain() {
      return chain(this);
    }

    /**
     * Executes the chain sequence and returns the wrapped result.
     *
     * @name commit
     * @memberOf _
     * @since 3.2.0
     * @category Seq
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var array = [1, 2];
     * var wrapped = _(array).push(3);
     *
     * console.log(array);
     * // => [1, 2]
     *
     * wrapped = wrapped.commit();
     * console.log(array);
     * // => [1, 2, 3]
     *
     * wrapped.last();
     * // => 3
     *
     * console.log(array);
     * // => [1, 2, 3]
     */
    function wrapperCommit() {
      return new LodashWrapper(this.value(), this.__chain__);
    }

    /**
     * Gets the next value on a wrapped object following the
     * [iterator protocol](https://mdn.io/iteration_protocols#iterator).
     *
     * @name next
     * @memberOf _
     * @since 4.0.0
     * @category Seq
     * @returns {Object} Returns the next iterator value.
     * @example
     *
     * var wrapped = _([1, 2]);
     *
     * wrapped.next();
     * // => { 'done': false, 'value': 1 }
     *
     * wrapped.next();
     * // => { 'done': false, 'value': 2 }
     *
     * wrapped.next();
     * // => { 'done': true, 'value': undefined }
     */
    function wrapperNext() {
      if (this.__values__ === undefined) {
        this.__values__ = toArray(this.value());
      }
      var done = this.__index__ >= this.__values__.length,
          value = done ? undefined : this.__values__[this.__index__++];

      return { 'done': done, 'value': value };
    }

    /**
     * Enables the wrapper to be iterable.
     *
     * @name Symbol.iterator
     * @memberOf _
     * @since 4.0.0
     * @category Seq
     * @returns {Object} Returns the wrapper object.
     * @example
     *
     * var wrapped = _([1, 2]);
     *
     * wrapped[Symbol.iterator]() === wrapped;
     * // => true
     *
     * Array.from(wrapped);
     * // => [1, 2]
     */
    function wrapperToIterator() {
      return this;
    }

    /**
     * Creates a clone of the chain sequence planting `value` as the wrapped value.
     *
     * @name plant
     * @memberOf _
     * @since 3.2.0
     * @category Seq
     * @param {*} value The value to plant.
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var wrapped = _([1, 2]).map(square);
     * var other = wrapped.plant([3, 4]);
     *
     * other.value();
     * // => [9, 16]
     *
     * wrapped.value();
     * // => [1, 4]
     */
    function wrapperPlant(value) {
      var result,
          parent = this;

      while (parent instanceof baseLodash) {
        var clone = wrapperClone(parent);
        clone.__index__ = 0;
        clone.__values__ = undefined;
        if (result) {
          previous.__wrapped__ = clone;
        } else {
          result = clone;
        }
        var previous = clone;
        parent = parent.__wrapped__;
      }
      previous.__wrapped__ = value;
      return result;
    }

    /**
     * This method is the wrapper version of `_.reverse`.
     *
     * **Note:** This method mutates the wrapped array.
     *
     * @name reverse
     * @memberOf _
     * @since 0.1.0
     * @category Seq
     * @returns {Object} Returns the new `lodash` wrapper instance.
     * @example
     *
     * var array = [1, 2, 3];
     *
     * _(array).reverse().value()
     * // => [3, 2, 1]
     *
     * console.log(array);
     * // => [3, 2, 1]
     */
    function wrapperReverse() {
      var value = this.__wrapped__;
      if (value instanceof LazyWrapper) {
        var wrapped = value;
        if (this.__actions__.length) {
          wrapped = new LazyWrapper(this);
        }
        wrapped = wrapped.reverse();
        wrapped.__actions__.push({
          'func': thru,
          'args': [reverse],
          'thisArg': undefined
        });
        return new LodashWrapper(wrapped, this.__chain__);
      }
      return this.thru(reverse);
    }

    /**
     * Executes the chain sequence to resolve the unwrapped value.
     *
     * @name value
     * @memberOf _
     * @since 0.1.0
     * @alias toJSON, valueOf
     * @category Seq
     * @returns {*} Returns the resolved unwrapped value.
     * @example
     *
     * _([1, 2, 3]).value();
     * // => [1, 2, 3]
     */
    function wrapperValue() {
      return baseWrapperValue(this.__wrapped__, this.__actions__);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` thru `iteratee`. The corresponding value of
     * each key is the number of times the key was returned by `iteratee`. The
     * iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 0.5.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee to transform keys.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.countBy([6.1, 4.2, 6.3], Math.floor);
     * // => { '4': 1, '6': 2 }
     *
     * // The `_.property` iteratee shorthand.
     * _.countBy(['one', 'two', 'three'], 'length');
     * // => { '3': 2, '5': 1 }
     */
    var countBy = createAggregator(function(result, value, key) {
      if (hasOwnProperty.call(result, key)) {
        ++result[key];
      } else {
        baseAssignValue(result, key, 1);
      }
    });

    /**
     * Checks if `predicate` returns truthy for **all** elements of `collection`.
     * Iteration is stopped once `predicate` returns falsey. The predicate is
     * invoked with three arguments: (value, index|key, collection).
     *
     * **Note:** This method returns `true` for
     * [empty collections](https://en.wikipedia.org/wiki/Empty_set) because
     * [everything is true](https://en.wikipedia.org/wiki/Vacuous_truth) of
     * elements of empty collections.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {boolean} Returns `true` if all elements pass the predicate check,
     *  else `false`.
     * @example
     *
     * _.every([true, 1, null, 'yes'], Boolean);
     * // => false
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': false },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * // The `_.matches` iteratee shorthand.
     * _.every(users, { 'user': 'barney', 'active': false });
     * // => false
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.every(users, ['active', false]);
     * // => true
     *
     * // The `_.property` iteratee shorthand.
     * _.every(users, 'active');
     * // => false
     */
    function every(collection, predicate, guard) {
      var func = isArray(collection) ? arrayEvery : baseEvery;
      if (guard && isIterateeCall(collection, predicate, guard)) {
        predicate = undefined;
      }
      return func(collection, getIteratee(predicate, 3));
    }

    /**
     * Iterates over elements of `collection`, returning an array of all elements
     * `predicate` returns truthy for. The predicate is invoked with three
     * arguments: (value, index|key, collection).
     *
     * **Note:** Unlike `_.remove`, this method returns a new array.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new filtered array.
     * @see _.reject
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': true },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * _.filter(users, function(o) { return !o.active; });
     * // => objects for ['fred']
     *
     * // The `_.matches` iteratee shorthand.
     * _.filter(users, { 'age': 36, 'active': true });
     * // => objects for ['barney']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.filter(users, ['active', false]);
     * // => objects for ['fred']
     *
     * // The `_.property` iteratee shorthand.
     * _.filter(users, 'active');
     * // => objects for ['barney']
     */
    function filter(collection, predicate) {
      var func = isArray(collection) ? arrayFilter : baseFilter;
      return func(collection, getIteratee(predicate, 3));
    }

    /**
     * Iterates over elements of `collection`, returning the first element
     * `predicate` returns truthy for. The predicate is invoked with three
     * arguments: (value, index|key, collection).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param {number} [fromIndex=0] The index to search from.
     * @returns {*} Returns the matched element, else `undefined`.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36, 'active': true },
     *   { 'user': 'fred',    'age': 40, 'active': false },
     *   { 'user': 'pebbles', 'age': 1,  'active': true }
     * ];
     *
     * _.find(users, function(o) { return o.age < 40; });
     * // => object for 'barney'
     *
     * // The `_.matches` iteratee shorthand.
     * _.find(users, { 'age': 1, 'active': true });
     * // => object for 'pebbles'
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.find(users, ['active', false]);
     * // => object for 'fred'
     *
     * // The `_.property` iteratee shorthand.
     * _.find(users, 'active');
     * // => object for 'barney'
     */
    var find = createFind(findIndex);

    /**
     * This method is like `_.find` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param {number} [fromIndex=collection.length-1] The index to search from.
     * @returns {*} Returns the matched element, else `undefined`.
     * @example
     *
     * _.findLast([1, 2, 3, 4], function(n) {
     *   return n % 2 == 1;
     * });
     * // => 3
     */
    var findLast = createFind(findLastIndex);

    /**
     * Creates a flattened array of values by running each element in `collection`
     * thru `iteratee` and flattening the mapped results. The iteratee is invoked
     * with three arguments: (value, index|key, collection).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * function duplicate(n) {
     *   return [n, n];
     * }
     *
     * _.flatMap([1, 2], duplicate);
     * // => [1, 1, 2, 2]
     */
    function flatMap(collection, iteratee) {
      return baseFlatten(map(collection, iteratee), 1);
    }

    /**
     * This method is like `_.flatMap` except that it recursively flattens the
     * mapped results.
     *
     * @static
     * @memberOf _
     * @since 4.7.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * function duplicate(n) {
     *   return [[[n, n]]];
     * }
     *
     * _.flatMapDeep([1, 2], duplicate);
     * // => [1, 1, 2, 2]
     */
    function flatMapDeep(collection, iteratee) {
      return baseFlatten(map(collection, iteratee), INFINITY);
    }

    /**
     * This method is like `_.flatMap` except that it recursively flattens the
     * mapped results up to `depth` times.
     *
     * @static
     * @memberOf _
     * @since 4.7.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {number} [depth=1] The maximum recursion depth.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * function duplicate(n) {
     *   return [[[n, n]]];
     * }
     *
     * _.flatMapDepth([1, 2], duplicate, 2);
     * // => [[1, 1], [2, 2]]
     */
    function flatMapDepth(collection, iteratee, depth) {
      depth = depth === undefined ? 1 : toInteger(depth);
      return baseFlatten(map(collection, iteratee), depth);
    }

    /**
     * Iterates over elements of `collection` and invokes `iteratee` for each element.
     * The iteratee is invoked with three arguments: (value, index|key, collection).
     * Iteratee functions may exit iteration early by explicitly returning `false`.
     *
     * **Note:** As with other "Collections" methods, objects with a "length"
     * property are iterated like arrays. To avoid this behavior use `_.forIn`
     * or `_.forOwn` for object iteration.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @alias each
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array|Object} Returns `collection`.
     * @see _.forEachRight
     * @example
     *
     * _.forEach([1, 2], function(value) {
     *   console.log(value);
     * });
     * // => Logs `1` then `2`.
     *
     * _.forEach({ 'a': 1, 'b': 2 }, function(value, key) {
     *   console.log(key);
     * });
     * // => Logs 'a' then 'b' (iteration order is not guaranteed).
     */
    function forEach(collection, iteratee) {
      var func = isArray(collection) ? arrayEach : baseEach;
      return func(collection, getIteratee(iteratee, 3));
    }

    /**
     * This method is like `_.forEach` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @alias eachRight
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array|Object} Returns `collection`.
     * @see _.forEach
     * @example
     *
     * _.forEachRight([1, 2], function(value) {
     *   console.log(value);
     * });
     * // => Logs `2` then `1`.
     */
    function forEachRight(collection, iteratee) {
      var func = isArray(collection) ? arrayEachRight : baseEachRight;
      return func(collection, getIteratee(iteratee, 3));
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` thru `iteratee`. The order of grouped values
     * is determined by the order they occur in `collection`. The corresponding
     * value of each key is an array of elements responsible for generating the
     * key. The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee to transform keys.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.groupBy([6.1, 4.2, 6.3], Math.floor);
     * // => { '4': [4.2], '6': [6.1, 6.3] }
     *
     * // The `_.property` iteratee shorthand.
     * _.groupBy(['one', 'two', 'three'], 'length');
     * // => { '3': ['one', 'two'], '5': ['three'] }
     */
    var groupBy = createAggregator(function(result, value, key) {
      if (hasOwnProperty.call(result, key)) {
        result[key].push(value);
      } else {
        baseAssignValue(result, key, [value]);
      }
    });

    /**
     * Checks if `value` is in `collection`. If `collection` is a string, it's
     * checked for a substring of `value`, otherwise
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * is used for equality comparisons. If `fromIndex` is negative, it's used as
     * the offset from the end of `collection`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object|string} collection The collection to inspect.
     * @param {*} value The value to search for.
     * @param {number} [fromIndex=0] The index to search from.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.reduce`.
     * @returns {boolean} Returns `true` if `value` is found, else `false`.
     * @example
     *
     * _.includes([1, 2, 3], 1);
     * // => true
     *
     * _.includes([1, 2, 3], 1, 2);
     * // => false
     *
     * _.includes({ 'a': 1, 'b': 2 }, 1);
     * // => true
     *
     * _.includes('abcd', 'bc');
     * // => true
     */
    function includes(collection, value, fromIndex, guard) {
      collection = isArrayLike(collection) ? collection : values(collection);
      fromIndex = (fromIndex && !guard) ? toInteger(fromIndex) : 0;

      var length = collection.length;
      if (fromIndex < 0) {
        fromIndex = nativeMax(length + fromIndex, 0);
      }
      return isString(collection)
        ? (fromIndex <= length && collection.indexOf(value, fromIndex) > -1)
        : (!!length && baseIndexOf(collection, value, fromIndex) > -1);
    }

    /**
     * Invokes the method at `path` of each element in `collection`, returning
     * an array of the results of each invoked method. Any additional arguments
     * are provided to each invoked method. If `path` is a function, it's invoked
     * for, and `this` bound to, each element in `collection`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Array|Function|string} path The path of the method to invoke or
     *  the function invoked per iteration.
     * @param {...*} [args] The arguments to invoke each method with.
     * @returns {Array} Returns the array of results.
     * @example
     *
     * _.invokeMap([[5, 1, 7], [3, 2, 1]], 'sort');
     * // => [[1, 5, 7], [1, 2, 3]]
     *
     * _.invokeMap([123, 456], String.prototype.split, '');
     * // => [['1', '2', '3'], ['4', '5', '6']]
     */
    var invokeMap = baseRest(function(collection, path, args) {
      var index = -1,
          isFunc = typeof path == 'function',
          result = isArrayLike(collection) ? Array(collection.length) : [];

      baseEach(collection, function(value) {
        result[++index] = isFunc ? apply(path, value, args) : baseInvoke(value, path, args);
      });
      return result;
    });

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` thru `iteratee`. The corresponding value of
     * each key is the last element responsible for generating the key. The
     * iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee to transform keys.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * var array = [
     *   { 'dir': 'left', 'code': 97 },
     *   { 'dir': 'right', 'code': 100 }
     * ];
     *
     * _.keyBy(array, function(o) {
     *   return String.fromCharCode(o.code);
     * });
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     *
     * _.keyBy(array, 'dir');
     * // => { 'left': { 'dir': 'left', 'code': 97 }, 'right': { 'dir': 'right', 'code': 100 } }
     */
    var keyBy = createAggregator(function(result, value, key) {
      baseAssignValue(result, key, value);
    });

    /**
     * Creates an array of values by running each element in `collection` thru
     * `iteratee`. The iteratee is invoked with three arguments:
     * (value, index|key, collection).
     *
     * Many lodash methods are guarded to work as iteratees for methods like
     * `_.every`, `_.filter`, `_.map`, `_.mapValues`, `_.reject`, and `_.some`.
     *
     * The guarded methods are:
     * `ary`, `chunk`, `curry`, `curryRight`, `drop`, `dropRight`, `every`,
     * `fill`, `invert`, `parseInt`, `random`, `range`, `rangeRight`, `repeat`,
     * `sampleSize`, `slice`, `some`, `sortBy`, `split`, `take`, `takeRight`,
     * `template`, `trim`, `trimEnd`, `trimStart`, and `words`
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new mapped array.
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * _.map([4, 8], square);
     * // => [16, 64]
     *
     * _.map({ 'a': 4, 'b': 8 }, square);
     * // => [16, 64] (iteration order is not guaranteed)
     *
     * var users = [
     *   { 'user': 'barney' },
     *   { 'user': 'fred' }
     * ];
     *
     * // The `_.property` iteratee shorthand.
     * _.map(users, 'user');
     * // => ['barney', 'fred']
     */
    function map(collection, iteratee) {
      var func = isArray(collection) ? arrayMap : baseMap;
      return func(collection, getIteratee(iteratee, 3));
    }

    /**
     * This method is like `_.sortBy` except that it allows specifying the sort
     * orders of the iteratees to sort by. If `orders` is unspecified, all values
     * are sorted in ascending order. Otherwise, specify an order of "desc" for
     * descending or "asc" for ascending sort order of corresponding values.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Array[]|Function[]|Object[]|string[]} [iteratees=[_.identity]]
     *  The iteratees to sort by.
     * @param {string[]} [orders] The sort orders of `iteratees`.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.reduce`.
     * @returns {Array} Returns the new sorted array.
     * @example
     *
     * var users = [
     *   { 'user': 'fred',   'age': 48 },
     *   { 'user': 'barney', 'age': 34 },
     *   { 'user': 'fred',   'age': 40 },
     *   { 'user': 'barney', 'age': 36 }
     * ];
     *
     * // Sort by `user` in ascending order and by `age` in descending order.
     * _.orderBy(users, ['user', 'age'], ['asc', 'desc']);
     * // => objects for [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 40]]
     */
    function orderBy(collection, iteratees, orders, guard) {
      if (collection == null) {
        return [];
      }
      if (!isArray(iteratees)) {
        iteratees = iteratees == null ? [] : [iteratees];
      }
      orders = guard ? undefined : orders;
      if (!isArray(orders)) {
        orders = orders == null ? [] : [orders];
      }
      return baseOrderBy(collection, iteratees, orders);
    }

    /**
     * Creates an array of elements split into two groups, the first of which
     * contains elements `predicate` returns truthy for, the second of which
     * contains elements `predicate` returns falsey for. The predicate is
     * invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the array of grouped elements.
     * @example
     *
     * var users = [
     *   { 'user': 'barney',  'age': 36, 'active': false },
     *   { 'user': 'fred',    'age': 40, 'active': true },
     *   { 'user': 'pebbles', 'age': 1,  'active': false }
     * ];
     *
     * _.partition(users, function(o) { return o.active; });
     * // => objects for [['fred'], ['barney', 'pebbles']]
     *
     * // The `_.matches` iteratee shorthand.
     * _.partition(users, { 'age': 1, 'active': false });
     * // => objects for [['pebbles'], ['barney', 'fred']]
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.partition(users, ['active', false]);
     * // => objects for [['barney', 'pebbles'], ['fred']]
     *
     * // The `_.property` iteratee shorthand.
     * _.partition(users, 'active');
     * // => objects for [['fred'], ['barney', 'pebbles']]
     */
    var partition = createAggregator(function(result, value, key) {
      result[key ? 0 : 1].push(value);
    }, function() { return [[], []]; });

    /**
     * Reduces `collection` to a value which is the accumulated result of running
     * each element in `collection` thru `iteratee`, where each successive
     * invocation is supplied the return value of the previous. If `accumulator`
     * is not given, the first element of `collection` is used as the initial
     * value. The iteratee is invoked with four arguments:
     * (accumulator, value, index|key, collection).
     *
     * Many lodash methods are guarded to work as iteratees for methods like
     * `_.reduce`, `_.reduceRight`, and `_.transform`.
     *
     * The guarded methods are:
     * `assign`, `defaults`, `defaultsDeep`, `includes`, `merge`, `orderBy`,
     * and `sortBy`
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @returns {*} Returns the accumulated value.
     * @see _.reduceRight
     * @example
     *
     * _.reduce([1, 2], function(sum, n) {
     *   return sum + n;
     * }, 0);
     * // => 3
     *
     * _.reduce({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
     *   (result[value] || (result[value] = [])).push(key);
     *   return result;
     * }, {});
     * // => { '1': ['a', 'c'], '2': ['b'] } (iteration order is not guaranteed)
     */
    function reduce(collection, iteratee, accumulator) {
      var func = isArray(collection) ? arrayReduce : baseReduce,
          initAccum = arguments.length < 3;

      return func(collection, getIteratee(iteratee, 4), accumulator, initAccum, baseEach);
    }

    /**
     * This method is like `_.reduce` except that it iterates over elements of
     * `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The initial value.
     * @returns {*} Returns the accumulated value.
     * @see _.reduce
     * @example
     *
     * var array = [[0, 1], [2, 3], [4, 5]];
     *
     * _.reduceRight(array, function(flattened, other) {
     *   return flattened.concat(other);
     * }, []);
     * // => [4, 5, 2, 3, 0, 1]
     */
    function reduceRight(collection, iteratee, accumulator) {
      var func = isArray(collection) ? arrayReduceRight : baseReduce,
          initAccum = arguments.length < 3;

      return func(collection, getIteratee(iteratee, 4), accumulator, initAccum, baseEachRight);
    }

    /**
     * The opposite of `_.filter`; this method returns the elements of `collection`
     * that `predicate` does **not** return truthy for.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the new filtered array.
     * @see _.filter
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': false },
     *   { 'user': 'fred',   'age': 40, 'active': true }
     * ];
     *
     * _.reject(users, function(o) { return !o.active; });
     * // => objects for ['fred']
     *
     * // The `_.matches` iteratee shorthand.
     * _.reject(users, { 'age': 40, 'active': true });
     * // => objects for ['barney']
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.reject(users, ['active', false]);
     * // => objects for ['fred']
     *
     * // The `_.property` iteratee shorthand.
     * _.reject(users, 'active');
     * // => objects for ['barney']
     */
    function reject(collection, predicate) {
      var func = isArray(collection) ? arrayFilter : baseFilter;
      return func(collection, negate(getIteratee(predicate, 3)));
    }

    /**
     * Gets a random element from `collection`.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to sample.
     * @returns {*} Returns the random element.
     * @example
     *
     * _.sample([1, 2, 3, 4]);
     * // => 2
     */
    function sample(collection) {
      var func = isArray(collection) ? arraySample : baseSample;
      return func(collection);
    }

    /**
     * Gets `n` random elements at unique keys from `collection` up to the
     * size of `collection`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Collection
     * @param {Array|Object} collection The collection to sample.
     * @param {number} [n=1] The number of elements to sample.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the random elements.
     * @example
     *
     * _.sampleSize([1, 2, 3], 2);
     * // => [3, 1]
     *
     * _.sampleSize([1, 2, 3], 4);
     * // => [2, 3, 1]
     */
    function sampleSize(collection, n, guard) {
      if ((guard ? isIterateeCall(collection, n, guard) : n === undefined)) {
        n = 1;
      } else {
        n = toInteger(n);
      }
      var func = isArray(collection) ? arraySampleSize : baseSampleSize;
      return func(collection, n);
    }

    /**
     * Creates an array of shuffled values, using a version of the
     * [Fisher-Yates shuffle](https://en.wikipedia.org/wiki/Fisher-Yates_shuffle).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to shuffle.
     * @returns {Array} Returns the new shuffled array.
     * @example
     *
     * _.shuffle([1, 2, 3, 4]);
     * // => [4, 1, 3, 2]
     */
    function shuffle(collection) {
      var func = isArray(collection) ? arrayShuffle : baseShuffle;
      return func(collection);
    }

    /**
     * Gets the size of `collection` by returning its length for array-like
     * values or the number of own enumerable string keyed properties for objects.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object|string} collection The collection to inspect.
     * @returns {number} Returns the collection size.
     * @example
     *
     * _.size([1, 2, 3]);
     * // => 3
     *
     * _.size({ 'a': 1, 'b': 2 });
     * // => 2
     *
     * _.size('pebbles');
     * // => 7
     */
    function size(collection) {
      if (collection == null) {
        return 0;
      }
      if (isArrayLike(collection)) {
        return isString(collection) ? stringSize(collection) : collection.length;
      }
      var tag = getTag(collection);
      if (tag == mapTag || tag == setTag) {
        return collection.size;
      }
      return baseKeys(collection).length;
    }

    /**
     * Checks if `predicate` returns truthy for **any** element of `collection`.
     * Iteration is stopped once `predicate` returns truthy. The predicate is
     * invoked with three arguments: (value, index|key, collection).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {boolean} Returns `true` if any element passes the predicate check,
     *  else `false`.
     * @example
     *
     * _.some([null, 0, 'yes', false], Boolean);
     * // => true
     *
     * var users = [
     *   { 'user': 'barney', 'active': true },
     *   { 'user': 'fred',   'active': false }
     * ];
     *
     * // The `_.matches` iteratee shorthand.
     * _.some(users, { 'user': 'barney', 'active': false });
     * // => false
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.some(users, ['active', false]);
     * // => true
     *
     * // The `_.property` iteratee shorthand.
     * _.some(users, 'active');
     * // => true
     */
    function some(collection, predicate, guard) {
      var func = isArray(collection) ? arraySome : baseSome;
      if (guard && isIterateeCall(collection, predicate, guard)) {
        predicate = undefined;
      }
      return func(collection, getIteratee(predicate, 3));
    }

    /**
     * Creates an array of elements, sorted in ascending order by the results of
     * running each element in a collection thru each iteratee. This method
     * performs a stable sort, that is, it preserves the original sort order of
     * equal elements. The iteratees are invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Collection
     * @param {Array|Object} collection The collection to iterate over.
     * @param {...(Function|Function[])} [iteratees=[_.identity]]
     *  The iteratees to sort by.
     * @returns {Array} Returns the new sorted array.
     * @example
     *
     * var users = [
     *   { 'user': 'fred',   'age': 48 },
     *   { 'user': 'barney', 'age': 36 },
     *   { 'user': 'fred',   'age': 40 },
     *   { 'user': 'barney', 'age': 34 }
     * ];
     *
     * _.sortBy(users, [function(o) { return o.user; }]);
     * // => objects for [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 40]]
     *
     * _.sortBy(users, ['user', 'age']);
     * // => objects for [['barney', 34], ['barney', 36], ['fred', 40], ['fred', 48]]
     */
    var sortBy = baseRest(function(collection, iteratees) {
      if (collection == null) {
        return [];
      }
      var length = iteratees.length;
      if (length > 1 && isIterateeCall(collection, iteratees[0], iteratees[1])) {
        iteratees = [];
      } else if (length > 2 && isIterateeCall(iteratees[0], iteratees[1], iteratees[2])) {
        iteratees = [iteratees[0]];
      }
      return baseOrderBy(collection, baseFlatten(iteratees, 1), []);
    });

    /*------------------------------------------------------------------------*/

    /**
     * Gets the timestamp of the number of milliseconds that have elapsed since
     * the Unix epoch (1 January 1970 00:00:00 UTC).
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Date
     * @returns {number} Returns the timestamp.
     * @example
     *
     * _.defer(function(stamp) {
     *   console.log(_.now() - stamp);
     * }, _.now());
     * // => Logs the number of milliseconds it took for the deferred invocation.
     */
    var now = ctxNow || function() {
      return root.Date.now();
    };

    /*------------------------------------------------------------------------*/

    /**
     * The opposite of `_.before`; this method creates a function that invokes
     * `func` once it's called `n` or more times.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {number} n The number of calls before `func` is invoked.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var saves = ['profile', 'settings'];
     *
     * var done = _.after(saves.length, function() {
     *   console.log('done saving!');
     * });
     *
     * _.forEach(saves, function(type) {
     *   asyncSave({ 'type': type, 'complete': done });
     * });
     * // => Logs 'done saving!' after the two async saves have completed.
     */
    function after(n, func) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      n = toInteger(n);
      return function() {
        if (--n < 1) {
          return func.apply(this, arguments);
        }
      };
    }

    /**
     * Creates a function that invokes `func`, with up to `n` arguments,
     * ignoring any additional arguments.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Function
     * @param {Function} func The function to cap arguments for.
     * @param {number} [n=func.length] The arity cap.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Function} Returns the new capped function.
     * @example
     *
     * _.map(['6', '8', '10'], _.ary(parseInt, 1));
     * // => [6, 8, 10]
     */
    function ary(func, n, guard) {
      n = guard ? undefined : n;
      n = (func && n == null) ? func.length : n;
      return createWrap(func, WRAP_ARY_FLAG, undefined, undefined, undefined, undefined, n);
    }

    /**
     * Creates a function that invokes `func`, with the `this` binding and arguments
     * of the created function, while it's called less than `n` times. Subsequent
     * calls to the created function return the result of the last `func` invocation.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Function
     * @param {number} n The number of calls at which `func` is no longer invoked.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * jQuery(element).on('click', _.before(5, addContactToList));
     * // => Allows adding up to 4 contacts to the list.
     */
    function before(n, func) {
      var result;
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      n = toInteger(n);
      return function() {
        if (--n > 0) {
          result = func.apply(this, arguments);
        }
        if (n <= 1) {
          func = undefined;
        }
        return result;
      };
    }

    /**
     * Creates a function that invokes `func` with the `this` binding of `thisArg`
     * and `partials` prepended to the arguments it receives.
     *
     * The `_.bind.placeholder` value, which defaults to `_` in monolithic builds,
     * may be used as a placeholder for partially applied arguments.
     *
     * **Note:** Unlike native `Function#bind`, this method doesn't set the "length"
     * property of bound functions.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to bind.
     * @param {*} thisArg The `this` binding of `func`.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * function greet(greeting, punctuation) {
     *   return greeting + ' ' + this.user + punctuation;
     * }
     *
     * var object = { 'user': 'fred' };
     *
     * var bound = _.bind(greet, object, 'hi');
     * bound('!');
     * // => 'hi fred!'
     *
     * // Bound with placeholders.
     * var bound = _.bind(greet, object, _, '!');
     * bound('hi');
     * // => 'hi fred!'
     */
    var bind = baseRest(function(func, thisArg, partials) {
      var bitmask = WRAP_BIND_FLAG;
      if (partials.length) {
        var holders = replaceHolders(partials, getHolder(bind));
        bitmask |= WRAP_PARTIAL_FLAG;
      }
      return createWrap(func, bitmask, thisArg, partials, holders);
    });

    /**
     * Creates a function that invokes the method at `object[key]` with `partials`
     * prepended to the arguments it receives.
     *
     * This method differs from `_.bind` by allowing bound functions to reference
     * methods that may be redefined or don't yet exist. See
     * [Peter Michaux's article](http://peter.michaux.ca/articles/lazy-function-definition-pattern)
     * for more details.
     *
     * The `_.bindKey.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * @static
     * @memberOf _
     * @since 0.10.0
     * @category Function
     * @param {Object} object The object to invoke the method on.
     * @param {string} key The key of the method.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var object = {
     *   'user': 'fred',
     *   'greet': function(greeting, punctuation) {
     *     return greeting + ' ' + this.user + punctuation;
     *   }
     * };
     *
     * var bound = _.bindKey(object, 'greet', 'hi');
     * bound('!');
     * // => 'hi fred!'
     *
     * object.greet = function(greeting, punctuation) {
     *   return greeting + 'ya ' + this.user + punctuation;
     * };
     *
     * bound('!');
     * // => 'hiya fred!'
     *
     * // Bound with placeholders.
     * var bound = _.bindKey(object, 'greet', _, '!');
     * bound('hi');
     * // => 'hiya fred!'
     */
    var bindKey = baseRest(function(object, key, partials) {
      var bitmask = WRAP_BIND_FLAG | WRAP_BIND_KEY_FLAG;
      if (partials.length) {
        var holders = replaceHolders(partials, getHolder(bindKey));
        bitmask |= WRAP_PARTIAL_FLAG;
      }
      return createWrap(key, bitmask, object, partials, holders);
    });

    /**
     * Creates a function that accepts arguments of `func` and either invokes
     * `func` returning its result, if at least `arity` number of arguments have
     * been provided, or returns a function that accepts the remaining `func`
     * arguments, and so on. The arity of `func` may be specified if `func.length`
     * is not sufficient.
     *
     * The `_.curry.placeholder` value, which defaults to `_` in monolithic builds,
     * may be used as a placeholder for provided arguments.
     *
     * **Note:** This method doesn't set the "length" property of curried functions.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Function
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var abc = function(a, b, c) {
     *   return [a, b, c];
     * };
     *
     * var curried = _.curry(abc);
     *
     * curried(1)(2)(3);
     * // => [1, 2, 3]
     *
     * curried(1, 2)(3);
     * // => [1, 2, 3]
     *
     * curried(1, 2, 3);
     * // => [1, 2, 3]
     *
     * // Curried with placeholders.
     * curried(1)(_, 3)(2);
     * // => [1, 2, 3]
     */
    function curry(func, arity, guard) {
      arity = guard ? undefined : arity;
      var result = createWrap(func, WRAP_CURRY_FLAG, undefined, undefined, undefined, undefined, undefined, arity);
      result.placeholder = curry.placeholder;
      return result;
    }

    /**
     * This method is like `_.curry` except that arguments are applied to `func`
     * in the manner of `_.partialRight` instead of `_.partial`.
     *
     * The `_.curryRight.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for provided arguments.
     *
     * **Note:** This method doesn't set the "length" property of curried functions.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Function
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var abc = function(a, b, c) {
     *   return [a, b, c];
     * };
     *
     * var curried = _.curryRight(abc);
     *
     * curried(3)(2)(1);
     * // => [1, 2, 3]
     *
     * curried(2, 3)(1);
     * // => [1, 2, 3]
     *
     * curried(1, 2, 3);
     * // => [1, 2, 3]
     *
     * // Curried with placeholders.
     * curried(3)(1, _)(2);
     * // => [1, 2, 3]
     */
    function curryRight(func, arity, guard) {
      arity = guard ? undefined : arity;
      var result = createWrap(func, WRAP_CURRY_RIGHT_FLAG, undefined, undefined, undefined, undefined, undefined, arity);
      result.placeholder = curryRight.placeholder;
      return result;
    }

    /**
     * Creates a debounced function that delays invoking `func` until after `wait`
     * milliseconds have elapsed since the last time the debounced function was
     * invoked. The debounced function comes with a `cancel` method to cancel
     * delayed `func` invocations and a `flush` method to immediately invoke them.
     * Provide `options` to indicate whether `func` should be invoked on the
     * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
     * with the last arguments provided to the debounced function. Subsequent
     * calls to the debounced function return the result of the last `func`
     * invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is
     * invoked on the trailing edge of the timeout only if the debounced function
     * is invoked more than once during the `wait` timeout.
     *
     * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
     * until to the next tick, similar to `setTimeout` with a timeout of `0`.
     *
     * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
     * for details over the differences between `_.debounce` and `_.throttle`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to debounce.
     * @param {number} [wait=0] The number of milliseconds to delay.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.leading=false]
     *  Specify invoking on the leading edge of the timeout.
     * @param {number} [options.maxWait]
     *  The maximum time `func` is allowed to be delayed before it's invoked.
     * @param {boolean} [options.trailing=true]
     *  Specify invoking on the trailing edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // Avoid costly calculations while the window size is in flux.
     * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
     *
     * // Invoke `sendMail` when clicked, debouncing subsequent calls.
     * jQuery(element).on('click', _.debounce(sendMail, 300, {
     *   'leading': true,
     *   'trailing': false
     * }));
     *
     * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
     * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
     * var source = new EventSource('/stream');
     * jQuery(source).on('message', debounced);
     *
     * // Cancel the trailing debounced invocation.
     * jQuery(window).on('popstate', debounced.cancel);
     */
    function debounce(func, wait, options) {
      var lastArgs,
          lastThis,
          maxWait,
          result,
          timerId,
          lastCallTime,
          lastInvokeTime = 0,
          leading = false,
          maxing = false,
          trailing = true;

      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      wait = toNumber(wait) || 0;
      if (isObject(options)) {
        leading = !!options.leading;
        maxing = 'maxWait' in options;
        maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
        trailing = 'trailing' in options ? !!options.trailing : trailing;
      }

      function invokeFunc(time) {
        var args = lastArgs,
            thisArg = lastThis;

        lastArgs = lastThis = undefined;
        lastInvokeTime = time;
        result = func.apply(thisArg, args);
        return result;
      }

      function leadingEdge(time) {
        // Reset any `maxWait` timer.
        lastInvokeTime = time;
        // Start the timer for the trailing edge.
        timerId = setTimeout(timerExpired, wait);
        // Invoke the leading edge.
        return leading ? invokeFunc(time) : result;
      }

      function remainingWait(time) {
        var timeSinceLastCall = time - lastCallTime,
            timeSinceLastInvoke = time - lastInvokeTime,
            result = wait - timeSinceLastCall;

        return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
      }

      function shouldInvoke(time) {
        var timeSinceLastCall = time - lastCallTime,
            timeSinceLastInvoke = time - lastInvokeTime;

        // Either this is the first call, activity has stopped and we're at the
        // trailing edge, the system time has gone backwards and we're treating
        // it as the trailing edge, or we've hit the `maxWait` limit.
        return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
          (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
      }

      function timerExpired() {
        var time = now();
        if (shouldInvoke(time)) {
          return trailingEdge(time);
        }
        // Restart the timer.
        timerId = setTimeout(timerExpired, remainingWait(time));
      }

      function trailingEdge(time) {
        timerId = undefined;

        // Only invoke if we have `lastArgs` which means `func` has been
        // debounced at least once.
        if (trailing && lastArgs) {
          return invokeFunc(time);
        }
        lastArgs = lastThis = undefined;
        return result;
      }

      function cancel() {
        if (timerId !== undefined) {
          clearTimeout(timerId);
        }
        lastInvokeTime = 0;
        lastArgs = lastCallTime = lastThis = timerId = undefined;
      }

      function flush() {
        return timerId === undefined ? result : trailingEdge(now());
      }

      function debounced() {
        var time = now(),
            isInvoking = shouldInvoke(time);

        lastArgs = arguments;
        lastThis = this;
        lastCallTime = time;

        if (isInvoking) {
          if (timerId === undefined) {
            return leadingEdge(lastCallTime);
          }
          if (maxing) {
            // Handle invocations in a tight loop.
            timerId = setTimeout(timerExpired, wait);
            return invokeFunc(lastCallTime);
          }
        }
        if (timerId === undefined) {
          timerId = setTimeout(timerExpired, wait);
        }
        return result;
      }
      debounced.cancel = cancel;
      debounced.flush = flush;
      return debounced;
    }

    /**
     * Defers invoking the `func` until the current call stack has cleared. Any
     * additional arguments are provided to `func` when it's invoked.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to defer.
     * @param {...*} [args] The arguments to invoke `func` with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.defer(function(text) {
     *   console.log(text);
     * }, 'deferred');
     * // => Logs 'deferred' after one millisecond.
     */
    var defer = baseRest(function(func, args) {
      return baseDelay(func, 1, args);
    });

    /**
     * Invokes `func` after `wait` milliseconds. Any additional arguments are
     * provided to `func` when it's invoked.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay invocation.
     * @param {...*} [args] The arguments to invoke `func` with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.delay(function(text) {
     *   console.log(text);
     * }, 1000, 'later');
     * // => Logs 'later' after one second.
     */
    var delay = baseRest(function(func, wait, args) {
      return baseDelay(func, toNumber(wait) || 0, args);
    });

    /**
     * Creates a function that invokes `func` with arguments reversed.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Function
     * @param {Function} func The function to flip arguments for.
     * @returns {Function} Returns the new flipped function.
     * @example
     *
     * var flipped = _.flip(function() {
     *   return _.toArray(arguments);
     * });
     *
     * flipped('a', 'b', 'c', 'd');
     * // => ['d', 'c', 'b', 'a']
     */
    function flip(func) {
      return createWrap(func, WRAP_FLIP_FLAG);
    }

    /**
     * Creates a function that memoizes the result of `func`. If `resolver` is
     * provided, it determines the cache key for storing the result based on the
     * arguments provided to the memoized function. By default, the first argument
     * provided to the memoized function is used as the map cache key. The `func`
     * is invoked with the `this` binding of the memoized function.
     *
     * **Note:** The cache is exposed as the `cache` property on the memoized
     * function. Its creation may be customized by replacing the `_.memoize.Cache`
     * constructor with one whose instances implement the
     * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
     * method interface of `clear`, `delete`, `get`, `has`, and `set`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to have its output memoized.
     * @param {Function} [resolver] The function to resolve the cache key.
     * @returns {Function} Returns the new memoized function.
     * @example
     *
     * var object = { 'a': 1, 'b': 2 };
     * var other = { 'c': 3, 'd': 4 };
     *
     * var values = _.memoize(_.values);
     * values(object);
     * // => [1, 2]
     *
     * values(other);
     * // => [3, 4]
     *
     * object.a = 2;
     * values(object);
     * // => [1, 2]
     *
     * // Modify the result cache.
     * values.cache.set(object, ['a', 'b']);
     * values(object);
     * // => ['a', 'b']
     *
     * // Replace `_.memoize.Cache`.
     * _.memoize.Cache = WeakMap;
     */
    function memoize(func, resolver) {
      if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      var memoized = function() {
        var args = arguments,
            key = resolver ? resolver.apply(this, args) : args[0],
            cache = memoized.cache;

        if (cache.has(key)) {
          return cache.get(key);
        }
        var result = func.apply(this, args);
        memoized.cache = cache.set(key, result) || cache;
        return result;
      };
      memoized.cache = new (memoize.Cache || MapCache);
      return memoized;
    }

    // Expose `MapCache`.
    memoize.Cache = MapCache;

    /**
     * Creates a function that negates the result of the predicate `func`. The
     * `func` predicate is invoked with the `this` binding and arguments of the
     * created function.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Function
     * @param {Function} predicate The predicate to negate.
     * @returns {Function} Returns the new negated function.
     * @example
     *
     * function isEven(n) {
     *   return n % 2 == 0;
     * }
     *
     * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
     * // => [1, 3, 5]
     */
    function negate(predicate) {
      if (typeof predicate != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      return function() {
        var args = arguments;
        switch (args.length) {
          case 0: return !predicate.call(this);
          case 1: return !predicate.call(this, args[0]);
          case 2: return !predicate.call(this, args[0], args[1]);
          case 3: return !predicate.call(this, args[0], args[1], args[2]);
        }
        return !predicate.apply(this, args);
      };
    }

    /**
     * Creates a function that is restricted to invoking `func` once. Repeat calls
     * to the function return the value of the first invocation. The `func` is
     * invoked with the `this` binding and arguments of the created function.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var initialize = _.once(createApplication);
     * initialize();
     * initialize();
     * // => `createApplication` is invoked once
     */
    function once(func) {
      return before(2, func);
    }

    /**
     * Creates a function that invokes `func` with its arguments transformed.
     *
     * @static
     * @since 4.0.0
     * @memberOf _
     * @category Function
     * @param {Function} func The function to wrap.
     * @param {...(Function|Function[])} [transforms=[_.identity]]
     *  The argument transforms.
     * @returns {Function} Returns the new function.
     * @example
     *
     * function doubled(n) {
     *   return n * 2;
     * }
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var func = _.overArgs(function(x, y) {
     *   return [x, y];
     * }, [square, doubled]);
     *
     * func(9, 3);
     * // => [81, 6]
     *
     * func(10, 5);
     * // => [100, 10]
     */
    var overArgs = castRest(function(func, transforms) {
      transforms = (transforms.length == 1 && isArray(transforms[0]))
        ? arrayMap(transforms[0], baseUnary(getIteratee()))
        : arrayMap(baseFlatten(transforms, 1), baseUnary(getIteratee()));

      var funcsLength = transforms.length;
      return baseRest(function(args) {
        var index = -1,
            length = nativeMin(args.length, funcsLength);

        while (++index < length) {
          args[index] = transforms[index].call(this, args[index]);
        }
        return apply(func, this, args);
      });
    });

    /**
     * Creates a function that invokes `func` with `partials` prepended to the
     * arguments it receives. This method is like `_.bind` except it does **not**
     * alter the `this` binding.
     *
     * The `_.partial.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * **Note:** This method doesn't set the "length" property of partially
     * applied functions.
     *
     * @static
     * @memberOf _
     * @since 0.2.0
     * @category Function
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * function greet(greeting, name) {
     *   return greeting + ' ' + name;
     * }
     *
     * var sayHelloTo = _.partial(greet, 'hello');
     * sayHelloTo('fred');
     * // => 'hello fred'
     *
     * // Partially applied with placeholders.
     * var greetFred = _.partial(greet, _, 'fred');
     * greetFred('hi');
     * // => 'hi fred'
     */
    var partial = baseRest(function(func, partials) {
      var holders = replaceHolders(partials, getHolder(partial));
      return createWrap(func, WRAP_PARTIAL_FLAG, undefined, partials, holders);
    });

    /**
     * This method is like `_.partial` except that partially applied arguments
     * are appended to the arguments it receives.
     *
     * The `_.partialRight.placeholder` value, which defaults to `_` in monolithic
     * builds, may be used as a placeholder for partially applied arguments.
     *
     * **Note:** This method doesn't set the "length" property of partially
     * applied functions.
     *
     * @static
     * @memberOf _
     * @since 1.0.0
     * @category Function
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [partials] The arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * function greet(greeting, name) {
     *   return greeting + ' ' + name;
     * }
     *
     * var greetFred = _.partialRight(greet, 'fred');
     * greetFred('hi');
     * // => 'hi fred'
     *
     * // Partially applied with placeholders.
     * var sayHelloTo = _.partialRight(greet, 'hello', _);
     * sayHelloTo('fred');
     * // => 'hello fred'
     */
    var partialRight = baseRest(function(func, partials) {
      var holders = replaceHolders(partials, getHolder(partialRight));
      return createWrap(func, WRAP_PARTIAL_RIGHT_FLAG, undefined, partials, holders);
    });

    /**
     * Creates a function that invokes `func` with arguments arranged according
     * to the specified `indexes` where the argument value at the first index is
     * provided as the first argument, the argument value at the second index is
     * provided as the second argument, and so on.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Function
     * @param {Function} func The function to rearrange arguments for.
     * @param {...(number|number[])} indexes The arranged argument indexes.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var rearged = _.rearg(function(a, b, c) {
     *   return [a, b, c];
     * }, [2, 0, 1]);
     *
     * rearged('b', 'c', 'a')
     * // => ['a', 'b', 'c']
     */
    var rearg = flatRest(function(func, indexes) {
      return createWrap(func, WRAP_REARG_FLAG, undefined, undefined, undefined, indexes);
    });

    /**
     * Creates a function that invokes `func` with the `this` binding of the
     * created function and arguments from `start` and beyond provided as
     * an array.
     *
     * **Note:** This method is based on the
     * [rest parameter](https://mdn.io/rest_parameters).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Function
     * @param {Function} func The function to apply a rest parameter to.
     * @param {number} [start=func.length-1] The start position of the rest parameter.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var say = _.rest(function(what, names) {
     *   return what + ' ' + _.initial(names).join(', ') +
     *     (_.size(names) > 1 ? ', & ' : '') + _.last(names);
     * });
     *
     * say('hello', 'fred', 'barney', 'pebbles');
     * // => 'hello fred, barney, & pebbles'
     */
    function rest(func, start) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      start = start === undefined ? start : toInteger(start);
      return baseRest(func, start);
    }

    /**
     * Creates a function that invokes `func` with the `this` binding of the
     * create function and an array of arguments much like
     * [`Function#apply`](http://www.ecma-international.org/ecma-262/7.0/#sec-function.prototype.apply).
     *
     * **Note:** This method is based on the
     * [spread operator](https://mdn.io/spread_operator).
     *
     * @static
     * @memberOf _
     * @since 3.2.0
     * @category Function
     * @param {Function} func The function to spread arguments over.
     * @param {number} [start=0] The start position of the spread.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var say = _.spread(function(who, what) {
     *   return who + ' says ' + what;
     * });
     *
     * say(['fred', 'hello']);
     * // => 'fred says hello'
     *
     * var numbers = Promise.all([
     *   Promise.resolve(40),
     *   Promise.resolve(36)
     * ]);
     *
     * numbers.then(_.spread(function(x, y) {
     *   return x + y;
     * }));
     * // => a Promise of 76
     */
    function spread(func, start) {
      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      start = start == null ? 0 : nativeMax(toInteger(start), 0);
      return baseRest(function(args) {
        var array = args[start],
            otherArgs = castSlice(args, 0, start);

        if (array) {
          arrayPush(otherArgs, array);
        }
        return apply(func, this, otherArgs);
      });
    }

    /**
     * Creates a throttled function that only invokes `func` at most once per
     * every `wait` milliseconds. The throttled function comes with a `cancel`
     * method to cancel delayed `func` invocations and a `flush` method to
     * immediately invoke them. Provide `options` to indicate whether `func`
     * should be invoked on the leading and/or trailing edge of the `wait`
     * timeout. The `func` is invoked with the last arguments provided to the
     * throttled function. Subsequent calls to the throttled function return the
     * result of the last `func` invocation.
     *
     * **Note:** If `leading` and `trailing` options are `true`, `func` is
     * invoked on the trailing edge of the timeout only if the throttled function
     * is invoked more than once during the `wait` timeout.
     *
     * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
     * until to the next tick, similar to `setTimeout` with a timeout of `0`.
     *
     * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
     * for details over the differences between `_.throttle` and `_.debounce`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {Function} func The function to throttle.
     * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.leading=true]
     *  Specify invoking on the leading edge of the timeout.
     * @param {boolean} [options.trailing=true]
     *  Specify invoking on the trailing edge of the timeout.
     * @returns {Function} Returns the new throttled function.
     * @example
     *
     * // Avoid excessively updating the position while scrolling.
     * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
     *
     * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
     * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
     * jQuery(element).on('click', throttled);
     *
     * // Cancel the trailing throttled invocation.
     * jQuery(window).on('popstate', throttled.cancel);
     */
    function throttle(func, wait, options) {
      var leading = true,
          trailing = true;

      if (typeof func != 'function') {
        throw new TypeError(FUNC_ERROR_TEXT);
      }
      if (isObject(options)) {
        leading = 'leading' in options ? !!options.leading : leading;
        trailing = 'trailing' in options ? !!options.trailing : trailing;
      }
      return debounce(func, wait, {
        'leading': leading,
        'maxWait': wait,
        'trailing': trailing
      });
    }

    /**
     * Creates a function that accepts up to one argument, ignoring any
     * additional arguments.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Function
     * @param {Function} func The function to cap arguments for.
     * @returns {Function} Returns the new capped function.
     * @example
     *
     * _.map(['6', '8', '10'], _.unary(parseInt));
     * // => [6, 8, 10]
     */
    function unary(func) {
      return ary(func, 1);
    }

    /**
     * Creates a function that provides `value` to `wrapper` as its first
     * argument. Any additional arguments provided to the function are appended
     * to those provided to the `wrapper`. The wrapper is invoked with the `this`
     * binding of the created function.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Function
     * @param {*} value The value to wrap.
     * @param {Function} [wrapper=identity] The wrapper function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var p = _.wrap(_.escape, function(func, text) {
     *   return '<p>' + func(text) + '</p>';
     * });
     *
     * p('fred, barney, & pebbles');
     * // => '<p>fred, barney, &amp; pebbles</p>'
     */
    function wrap(value, wrapper) {
      return partial(castFunction(wrapper), value);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Casts `value` as an array if it's not one.
     *
     * @static
     * @memberOf _
     * @since 4.4.0
     * @category Lang
     * @param {*} value The value to inspect.
     * @returns {Array} Returns the cast array.
     * @example
     *
     * _.castArray(1);
     * // => [1]
     *
     * _.castArray({ 'a': 1 });
     * // => [{ 'a': 1 }]
     *
     * _.castArray('abc');
     * // => ['abc']
     *
     * _.castArray(null);
     * // => [null]
     *
     * _.castArray(undefined);
     * // => [undefined]
     *
     * _.castArray();
     * // => []
     *
     * var array = [1, 2, 3];
     * console.log(_.castArray(array) === array);
     * // => true
     */
    function castArray() {
      if (!arguments.length) {
        return [];
      }
      var value = arguments[0];
      return isArray(value) ? value : [value];
    }

    /**
     * Creates a shallow clone of `value`.
     *
     * **Note:** This method is loosely based on the
     * [structured clone algorithm](https://mdn.io/Structured_clone_algorithm)
     * and supports cloning arrays, array buffers, booleans, date objects, maps,
     * numbers, `Object` objects, regexes, sets, strings, symbols, and typed
     * arrays. The own enumerable properties of `arguments` objects are cloned
     * as plain objects. An empty object is returned for uncloneable values such
     * as error objects, functions, DOM nodes, and WeakMaps.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to clone.
     * @returns {*} Returns the cloned value.
     * @see _.cloneDeep
     * @example
     *
     * var objects = [{ 'a': 1 }, { 'b': 2 }];
     *
     * var shallow = _.clone(objects);
     * console.log(shallow[0] === objects[0]);
     * // => true
     */
    function clone(value) {
      return baseClone(value, CLONE_SYMBOLS_FLAG);
    }

    /**
     * This method is like `_.clone` except that it accepts `customizer` which
     * is invoked to produce the cloned value. If `customizer` returns `undefined`,
     * cloning is handled by the method instead. The `customizer` is invoked with
     * up to four arguments; (value [, index|key, object, stack]).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to clone.
     * @param {Function} [customizer] The function to customize cloning.
     * @returns {*} Returns the cloned value.
     * @see _.cloneDeepWith
     * @example
     *
     * function customizer(value) {
     *   if (_.isElement(value)) {
     *     return value.cloneNode(false);
     *   }
     * }
     *
     * var el = _.cloneWith(document.body, customizer);
     *
     * console.log(el === document.body);
     * // => false
     * console.log(el.nodeName);
     * // => 'BODY'
     * console.log(el.childNodes.length);
     * // => 0
     */
    function cloneWith(value, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      return baseClone(value, CLONE_SYMBOLS_FLAG, customizer);
    }

    /**
     * This method is like `_.clone` except that it recursively clones `value`.
     *
     * @static
     * @memberOf _
     * @since 1.0.0
     * @category Lang
     * @param {*} value The value to recursively clone.
     * @returns {*} Returns the deep cloned value.
     * @see _.clone
     * @example
     *
     * var objects = [{ 'a': 1 }, { 'b': 2 }];
     *
     * var deep = _.cloneDeep(objects);
     * console.log(deep[0] === objects[0]);
     * // => false
     */
    function cloneDeep(value) {
      return baseClone(value, CLONE_DEEP_FLAG | CLONE_SYMBOLS_FLAG);
    }

    /**
     * This method is like `_.cloneWith` except that it recursively clones `value`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to recursively clone.
     * @param {Function} [customizer] The function to customize cloning.
     * @returns {*} Returns the deep cloned value.
     * @see _.cloneWith
     * @example
     *
     * function customizer(value) {
     *   if (_.isElement(value)) {
     *     return value.cloneNode(true);
     *   }
     * }
     *
     * var el = _.cloneDeepWith(document.body, customizer);
     *
     * console.log(el === document.body);
     * // => false
     * console.log(el.nodeName);
     * // => 'BODY'
     * console.log(el.childNodes.length);
     * // => 20
     */
    function cloneDeepWith(value, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      return baseClone(value, CLONE_DEEP_FLAG | CLONE_SYMBOLS_FLAG, customizer);
    }

    /**
     * Checks if `object` conforms to `source` by invoking the predicate
     * properties of `source` with the corresponding property values of `object`.
     *
     * **Note:** This method is equivalent to `_.conforms` when `source` is
     * partially applied.
     *
     * @static
     * @memberOf _
     * @since 4.14.0
     * @category Lang
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property predicates to conform to.
     * @returns {boolean} Returns `true` if `object` conforms, else `false`.
     * @example
     *
     * var object = { 'a': 1, 'b': 2 };
     *
     * _.conformsTo(object, { 'b': function(n) { return n > 1; } });
     * // => true
     *
     * _.conformsTo(object, { 'b': function(n) { return n > 2; } });
     * // => false
     */
    function conformsTo(object, source) {
      return source == null || baseConformsTo(object, source, keys(source));
    }

    /**
     * Performs a
     * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
     * comparison between two values to determine if they are equivalent.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * var object = { 'a': 1 };
     * var other = { 'a': 1 };
     *
     * _.eq(object, object);
     * // => true
     *
     * _.eq(object, other);
     * // => false
     *
     * _.eq('a', 'a');
     * // => true
     *
     * _.eq('a', Object('a'));
     * // => false
     *
     * _.eq(NaN, NaN);
     * // => true
     */
    function eq(value, other) {
      return value === other || (value !== value && other !== other);
    }

    /**
     * Checks if `value` is greater than `other`.
     *
     * @static
     * @memberOf _
     * @since 3.9.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is greater than `other`,
     *  else `false`.
     * @see _.lt
     * @example
     *
     * _.gt(3, 1);
     * // => true
     *
     * _.gt(3, 3);
     * // => false
     *
     * _.gt(1, 3);
     * // => false
     */
    var gt = createRelationalOperation(baseGt);

    /**
     * Checks if `value` is greater than or equal to `other`.
     *
     * @static
     * @memberOf _
     * @since 3.9.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is greater than or equal to
     *  `other`, else `false`.
     * @see _.lte
     * @example
     *
     * _.gte(3, 1);
     * // => true
     *
     * _.gte(3, 3);
     * // => true
     *
     * _.gte(1, 3);
     * // => false
     */
    var gte = createRelationalOperation(function(value, other) {
      return value >= other;
    });

    /**
     * Checks if `value` is likely an `arguments` object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an `arguments` object,
     *  else `false`.
     * @example
     *
     * _.isArguments(function() { return arguments; }());
     * // => true
     *
     * _.isArguments([1, 2, 3]);
     * // => false
     */
    var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
      return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
        !propertyIsEnumerable.call(value, 'callee');
    };

    /**
     * Checks if `value` is classified as an `Array` object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an array, else `false`.
     * @example
     *
     * _.isArray([1, 2, 3]);
     * // => true
     *
     * _.isArray(document.body.children);
     * // => false
     *
     * _.isArray('abc');
     * // => false
     *
     * _.isArray(_.noop);
     * // => false
     */
    var isArray = Array.isArray;

    /**
     * Checks if `value` is classified as an `ArrayBuffer` object.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an array buffer, else `false`.
     * @example
     *
     * _.isArrayBuffer(new ArrayBuffer(2));
     * // => true
     *
     * _.isArrayBuffer(new Array(2));
     * // => false
     */
    var isArrayBuffer = nodeIsArrayBuffer ? baseUnary(nodeIsArrayBuffer) : baseIsArrayBuffer;

    /**
     * Checks if `value` is array-like. A value is considered array-like if it's
     * not a function and has a `value.length` that's an integer greater than or
     * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
     * @example
     *
     * _.isArrayLike([1, 2, 3]);
     * // => true
     *
     * _.isArrayLike(document.body.children);
     * // => true
     *
     * _.isArrayLike('abc');
     * // => true
     *
     * _.isArrayLike(_.noop);
     * // => false
     */
    function isArrayLike(value) {
      return value != null && isLength(value.length) && !isFunction(value);
    }

    /**
     * This method is like `_.isArrayLike` except that it also checks if `value`
     * is an object.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an array-like object,
     *  else `false`.
     * @example
     *
     * _.isArrayLikeObject([1, 2, 3]);
     * // => true
     *
     * _.isArrayLikeObject(document.body.children);
     * // => true
     *
     * _.isArrayLikeObject('abc');
     * // => false
     *
     * _.isArrayLikeObject(_.noop);
     * // => false
     */
    function isArrayLikeObject(value) {
      return isObjectLike(value) && isArrayLike(value);
    }

    /**
     * Checks if `value` is classified as a boolean primitive or object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a boolean, else `false`.
     * @example
     *
     * _.isBoolean(false);
     * // => true
     *
     * _.isBoolean(null);
     * // => false
     */
    function isBoolean(value) {
      return value === true || value === false ||
        (isObjectLike(value) && baseGetTag(value) == boolTag);
    }

    /**
     * Checks if `value` is a buffer.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
     * @example
     *
     * _.isBuffer(new Buffer(2));
     * // => true
     *
     * _.isBuffer(new Uint8Array(2));
     * // => false
     */
    var isBuffer = nativeIsBuffer || stubFalse;

    /**
     * Checks if `value` is classified as a `Date` object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a date object, else `false`.
     * @example
     *
     * _.isDate(new Date);
     * // => true
     *
     * _.isDate('Mon April 23 2012');
     * // => false
     */
    var isDate = nodeIsDate ? baseUnary(nodeIsDate) : baseIsDate;

    /**
     * Checks if `value` is likely a DOM element.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a DOM element, else `false`.
     * @example
     *
     * _.isElement(document.body);
     * // => true
     *
     * _.isElement('<body>');
     * // => false
     */
    function isElement(value) {
      return isObjectLike(value) && value.nodeType === 1 && !isPlainObject(value);
    }

    /**
     * Checks if `value` is an empty object, collection, map, or set.
     *
     * Objects are considered empty if they have no own enumerable string keyed
     * properties.
     *
     * Array-like values such as `arguments` objects, arrays, buffers, strings, or
     * jQuery-like collections are considered empty if they have a `length` of `0`.
     * Similarly, maps and sets are considered empty if they have a `size` of `0`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is empty, else `false`.
     * @example
     *
     * _.isEmpty(null);
     * // => true
     *
     * _.isEmpty(true);
     * // => true
     *
     * _.isEmpty(1);
     * // => true
     *
     * _.isEmpty([1, 2, 3]);
     * // => false
     *
     * _.isEmpty({ 'a': 1 });
     * // => false
     */
    function isEmpty(value) {
      if (value == null) {
        return true;
      }
      if (isArrayLike(value) &&
          (isArray(value) || typeof value == 'string' || typeof value.splice == 'function' ||
            isBuffer(value) || isTypedArray(value) || isArguments(value))) {
        return !value.length;
      }
      var tag = getTag(value);
      if (tag == mapTag || tag == setTag) {
        return !value.size;
      }
      if (isPrototype(value)) {
        return !baseKeys(value).length;
      }
      for (var key in value) {
        if (hasOwnProperty.call(value, key)) {
          return false;
        }
      }
      return true;
    }

    /**
     * Performs a deep comparison between two values to determine if they are
     * equivalent.
     *
     * **Note:** This method supports comparing arrays, array buffers, booleans,
     * date objects, error objects, maps, numbers, `Object` objects, regexes,
     * sets, strings, symbols, and typed arrays. `Object` objects are compared
     * by their own, not inherited, enumerable properties. Functions and DOM
     * nodes are compared by strict equality, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * var object = { 'a': 1 };
     * var other = { 'a': 1 };
     *
     * _.isEqual(object, other);
     * // => true
     *
     * object === other;
     * // => false
     */
    function isEqual(value, other) {
      return baseIsEqual(value, other);
    }

    /**
     * This method is like `_.isEqual` except that it accepts `customizer` which
     * is invoked to compare values. If `customizer` returns `undefined`, comparisons
     * are handled by the method instead. The `customizer` is invoked with up to
     * six arguments: (objValue, othValue [, index|key, object, other, stack]).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @param {Function} [customizer] The function to customize comparisons.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * function isGreeting(value) {
     *   return /^h(?:i|ello)$/.test(value);
     * }
     *
     * function customizer(objValue, othValue) {
     *   if (isGreeting(objValue) && isGreeting(othValue)) {
     *     return true;
     *   }
     * }
     *
     * var array = ['hello', 'goodbye'];
     * var other = ['hi', 'goodbye'];
     *
     * _.isEqualWith(array, other, customizer);
     * // => true
     */
    function isEqualWith(value, other, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      var result = customizer ? customizer(value, other) : undefined;
      return result === undefined ? baseIsEqual(value, other, undefined, customizer) : !!result;
    }

    /**
     * Checks if `value` is an `Error`, `EvalError`, `RangeError`, `ReferenceError`,
     * `SyntaxError`, `TypeError`, or `URIError` object.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an error object, else `false`.
     * @example
     *
     * _.isError(new Error);
     * // => true
     *
     * _.isError(Error);
     * // => false
     */
    function isError(value) {
      if (!isObjectLike(value)) {
        return false;
      }
      var tag = baseGetTag(value);
      return tag == errorTag || tag == domExcTag ||
        (typeof value.message == 'string' && typeof value.name == 'string' && !isPlainObject(value));
    }

    /**
     * Checks if `value` is a finite primitive number.
     *
     * **Note:** This method is based on
     * [`Number.isFinite`](https://mdn.io/Number/isFinite).
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a finite number, else `false`.
     * @example
     *
     * _.isFinite(3);
     * // => true
     *
     * _.isFinite(Number.MIN_VALUE);
     * // => true
     *
     * _.isFinite(Infinity);
     * // => false
     *
     * _.isFinite('3');
     * // => false
     */
    function isFinite(value) {
      return typeof value == 'number' && nativeIsFinite(value);
    }

    /**
     * Checks if `value` is classified as a `Function` object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a function, else `false`.
     * @example
     *
     * _.isFunction(_);
     * // => true
     *
     * _.isFunction(/abc/);
     * // => false
     */
    function isFunction(value) {
      if (!isObject(value)) {
        return false;
      }
      // The use of `Object#toString` avoids issues with the `typeof` operator
      // in Safari 9 which returns 'object' for typed arrays and other constructors.
      var tag = baseGetTag(value);
      return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
    }

    /**
     * Checks if `value` is an integer.
     *
     * **Note:** This method is based on
     * [`Number.isInteger`](https://mdn.io/Number/isInteger).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an integer, else `false`.
     * @example
     *
     * _.isInteger(3);
     * // => true
     *
     * _.isInteger(Number.MIN_VALUE);
     * // => false
     *
     * _.isInteger(Infinity);
     * // => false
     *
     * _.isInteger('3');
     * // => false
     */
    function isInteger(value) {
      return typeof value == 'number' && value == toInteger(value);
    }

    /**
     * Checks if `value` is a valid array-like length.
     *
     * **Note:** This method is loosely based on
     * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
     * @example
     *
     * _.isLength(3);
     * // => true
     *
     * _.isLength(Number.MIN_VALUE);
     * // => false
     *
     * _.isLength(Infinity);
     * // => false
     *
     * _.isLength('3');
     * // => false
     */
    function isLength(value) {
      return typeof value == 'number' &&
        value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
    }

    /**
     * Checks if `value` is the
     * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
     * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(_.noop);
     * // => true
     *
     * _.isObject(null);
     * // => false
     */
    function isObject(value) {
      var type = typeof value;
      return value != null && (type == 'object' || type == 'function');
    }

    /**
     * Checks if `value` is object-like. A value is object-like if it's not `null`
     * and has a `typeof` result of "object".
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
     * @example
     *
     * _.isObjectLike({});
     * // => true
     *
     * _.isObjectLike([1, 2, 3]);
     * // => true
     *
     * _.isObjectLike(_.noop);
     * // => false
     *
     * _.isObjectLike(null);
     * // => false
     */
    function isObjectLike(value) {
      return value != null && typeof value == 'object';
    }

    /**
     * Checks if `value` is classified as a `Map` object.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a map, else `false`.
     * @example
     *
     * _.isMap(new Map);
     * // => true
     *
     * _.isMap(new WeakMap);
     * // => false
     */
    var isMap = nodeIsMap ? baseUnary(nodeIsMap) : baseIsMap;

    /**
     * Performs a partial deep comparison between `object` and `source` to
     * determine if `object` contains equivalent property values.
     *
     * **Note:** This method is equivalent to `_.matches` when `source` is
     * partially applied.
     *
     * Partial comparisons will match empty array and empty object `source`
     * values against any array or object value, respectively. See `_.isEqual`
     * for a list of supported value comparisons.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Lang
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property values to match.
     * @returns {boolean} Returns `true` if `object` is a match, else `false`.
     * @example
     *
     * var object = { 'a': 1, 'b': 2 };
     *
     * _.isMatch(object, { 'b': 2 });
     * // => true
     *
     * _.isMatch(object, { 'b': 1 });
     * // => false
     */
    function isMatch(object, source) {
      return object === source || baseIsMatch(object, source, getMatchData(source));
    }

    /**
     * This method is like `_.isMatch` except that it accepts `customizer` which
     * is invoked to compare values. If `customizer` returns `undefined`, comparisons
     * are handled by the method instead. The `customizer` is invoked with five
     * arguments: (objValue, srcValue, index|key, object, source).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {Object} object The object to inspect.
     * @param {Object} source The object of property values to match.
     * @param {Function} [customizer] The function to customize comparisons.
     * @returns {boolean} Returns `true` if `object` is a match, else `false`.
     * @example
     *
     * function isGreeting(value) {
     *   return /^h(?:i|ello)$/.test(value);
     * }
     *
     * function customizer(objValue, srcValue) {
     *   if (isGreeting(objValue) && isGreeting(srcValue)) {
     *     return true;
     *   }
     * }
     *
     * var object = { 'greeting': 'hello' };
     * var source = { 'greeting': 'hi' };
     *
     * _.isMatchWith(object, source, customizer);
     * // => true
     */
    function isMatchWith(object, source, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      return baseIsMatch(object, source, getMatchData(source), customizer);
    }

    /**
     * Checks if `value` is `NaN`.
     *
     * **Note:** This method is based on
     * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
     * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
     * `undefined` and other non-number values.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
     * @example
     *
     * _.isNaN(NaN);
     * // => true
     *
     * _.isNaN(new Number(NaN));
     * // => true
     *
     * isNaN(undefined);
     * // => true
     *
     * _.isNaN(undefined);
     * // => false
     */
    function isNaN(value) {
      // An `NaN` primitive is the only value that is not equal to itself.
      // Perform the `toStringTag` check first to avoid errors with some
      // ActiveX objects in IE.
      return isNumber(value) && value != +value;
    }

    /**
     * Checks if `value` is a pristine native function.
     *
     * **Note:** This method can't reliably detect native functions in the presence
     * of the core-js package because core-js circumvents this kind of detection.
     * Despite multiple requests, the core-js maintainer has made it clear: any
     * attempt to fix the detection will be obstructed. As a result, we're left
     * with little choice but to throw an error. Unfortunately, this also affects
     * packages, like [babel-polyfill](https://www.npmjs.com/package/babel-polyfill),
     * which rely on core-js.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a native function,
     *  else `false`.
     * @example
     *
     * _.isNative(Array.prototype.push);
     * // => true
     *
     * _.isNative(_);
     * // => false
     */
    function isNative(value) {
      if (isMaskable(value)) {
        throw new Error(CORE_ERROR_TEXT);
      }
      return baseIsNative(value);
    }

    /**
     * Checks if `value` is `null`.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `null`, else `false`.
     * @example
     *
     * _.isNull(null);
     * // => true
     *
     * _.isNull(void 0);
     * // => false
     */
    function isNull(value) {
      return value === null;
    }

    /**
     * Checks if `value` is `null` or `undefined`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is nullish, else `false`.
     * @example
     *
     * _.isNil(null);
     * // => true
     *
     * _.isNil(void 0);
     * // => true
     *
     * _.isNil(NaN);
     * // => false
     */
    function isNil(value) {
      return value == null;
    }

    /**
     * Checks if `value` is classified as a `Number` primitive or object.
     *
     * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
     * classified as numbers, use the `_.isFinite` method.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a number, else `false`.
     * @example
     *
     * _.isNumber(3);
     * // => true
     *
     * _.isNumber(Number.MIN_VALUE);
     * // => true
     *
     * _.isNumber(Infinity);
     * // => true
     *
     * _.isNumber('3');
     * // => false
     */
    function isNumber(value) {
      return typeof value == 'number' ||
        (isObjectLike(value) && baseGetTag(value) == numberTag);
    }

    /**
     * Checks if `value` is a plain object, that is, an object created by the
     * `Object` constructor or one with a `[[Prototype]]` of `null`.
     *
     * @static
     * @memberOf _
     * @since 0.8.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     * }
     *
     * _.isPlainObject(new Foo);
     * // => false
     *
     * _.isPlainObject([1, 2, 3]);
     * // => false
     *
     * _.isPlainObject({ 'x': 0, 'y': 0 });
     * // => true
     *
     * _.isPlainObject(Object.create(null));
     * // => true
     */
    function isPlainObject(value) {
      if (!isObjectLike(value) || baseGetTag(value) != objectTag) {
        return false;
      }
      var proto = getPrototype(value);
      if (proto === null) {
        return true;
      }
      var Ctor = hasOwnProperty.call(proto, 'constructor') && proto.constructor;
      return typeof Ctor == 'function' && Ctor instanceof Ctor &&
        funcToString.call(Ctor) == objectCtorString;
    }

    /**
     * Checks if `value` is classified as a `RegExp` object.
     *
     * @static
     * @memberOf _
     * @since 0.1.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a regexp, else `false`.
     * @example
     *
     * _.isRegExp(/abc/);
     * // => true
     *
     * _.isRegExp('/abc/');
     * // => false
     */
    var isRegExp = nodeIsRegExp ? baseUnary(nodeIsRegExp) : baseIsRegExp;

    /**
     * Checks if `value` is a safe integer. An integer is safe if it's an IEEE-754
     * double precision number which isn't the result of a rounded unsafe integer.
     *
     * **Note:** This method is based on
     * [`Number.isSafeInteger`](https://mdn.io/Number/isSafeInteger).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a safe integer, else `false`.
     * @example
     *
     * _.isSafeInteger(3);
     * // => true
     *
     * _.isSafeInteger(Number.MIN_VALUE);
     * // => false
     *
     * _.isSafeInteger(Infinity);
     * // => false
     *
     * _.isSafeInteger('3');
     * // => false
     */
    function isSafeInteger(value) {
      return isInteger(value) && value >= -MAX_SAFE_INTEGER && value <= MAX_SAFE_INTEGER;
    }

    /**
     * Checks if `value` is classified as a `Set` object.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a set, else `false`.
     * @example
     *
     * _.isSet(new Set);
     * // => true
     *
     * _.isSet(new WeakSet);
     * // => false
     */
    var isSet = nodeIsSet ? baseUnary(nodeIsSet) : baseIsSet;

    /**
     * Checks if `value` is classified as a `String` primitive or object.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a string, else `false`.
     * @example
     *
     * _.isString('abc');
     * // => true
     *
     * _.isString(1);
     * // => false
     */
    function isString(value) {
      return typeof value == 'string' ||
        (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag);
    }

    /**
     * Checks if `value` is classified as a `Symbol` primitive or object.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
     * @example
     *
     * _.isSymbol(Symbol.iterator);
     * // => true
     *
     * _.isSymbol('abc');
     * // => false
     */
    function isSymbol(value) {
      return typeof value == 'symbol' ||
        (isObjectLike(value) && baseGetTag(value) == symbolTag);
    }

    /**
     * Checks if `value` is classified as a typed array.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
     * @example
     *
     * _.isTypedArray(new Uint8Array);
     * // => true
     *
     * _.isTypedArray([]);
     * // => false
     */
    var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

    /**
     * Checks if `value` is `undefined`.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is `undefined`, else `false`.
     * @example
     *
     * _.isUndefined(void 0);
     * // => true
     *
     * _.isUndefined(null);
     * // => false
     */
    function isUndefined(value) {
      return value === undefined;
    }

    /**
     * Checks if `value` is classified as a `WeakMap` object.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a weak map, else `false`.
     * @example
     *
     * _.isWeakMap(new WeakMap);
     * // => true
     *
     * _.isWeakMap(new Map);
     * // => false
     */
    function isWeakMap(value) {
      return isObjectLike(value) && getTag(value) == weakMapTag;
    }

    /**
     * Checks if `value` is classified as a `WeakSet` object.
     *
     * @static
     * @memberOf _
     * @since 4.3.0
     * @category Lang
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a weak set, else `false`.
     * @example
     *
     * _.isWeakSet(new WeakSet);
     * // => true
     *
     * _.isWeakSet(new Set);
     * // => false
     */
    function isWeakSet(value) {
      return isObjectLike(value) && baseGetTag(value) == weakSetTag;
    }

    /**
     * Checks if `value` is less than `other`.
     *
     * @static
     * @memberOf _
     * @since 3.9.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is less than `other`,
     *  else `false`.
     * @see _.gt
     * @example
     *
     * _.lt(1, 3);
     * // => true
     *
     * _.lt(3, 3);
     * // => false
     *
     * _.lt(3, 1);
     * // => false
     */
    var lt = createRelationalOperation(baseLt);

    /**
     * Checks if `value` is less than or equal to `other`.
     *
     * @static
     * @memberOf _
     * @since 3.9.0
     * @category Lang
     * @param {*} value The value to compare.
     * @param {*} other The other value to compare.
     * @returns {boolean} Returns `true` if `value` is less than or equal to
     *  `other`, else `false`.
     * @see _.gte
     * @example
     *
     * _.lte(1, 3);
     * // => true
     *
     * _.lte(3, 3);
     * // => true
     *
     * _.lte(3, 1);
     * // => false
     */
    var lte = createRelationalOperation(function(value, other) {
      return value <= other;
    });

    /**
     * Converts `value` to an array.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {Array} Returns the converted array.
     * @example
     *
     * _.toArray({ 'a': 1, 'b': 2 });
     * // => [1, 2]
     *
     * _.toArray('abc');
     * // => ['a', 'b', 'c']
     *
     * _.toArray(1);
     * // => []
     *
     * _.toArray(null);
     * // => []
     */
    function toArray(value) {
      if (!value) {
        return [];
      }
      if (isArrayLike(value)) {
        return isString(value) ? stringToArray(value) : copyArray(value);
      }
      if (symIterator && value[symIterator]) {
        return iteratorToArray(value[symIterator]());
      }
      var tag = getTag(value),
          func = tag == mapTag ? mapToArray : (tag == setTag ? setToArray : values);

      return func(value);
    }

    /**
     * Converts `value` to a finite number.
     *
     * @static
     * @memberOf _
     * @since 4.12.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {number} Returns the converted number.
     * @example
     *
     * _.toFinite(3.2);
     * // => 3.2
     *
     * _.toFinite(Number.MIN_VALUE);
     * // => 5e-324
     *
     * _.toFinite(Infinity);
     * // => 1.7976931348623157e+308
     *
     * _.toFinite('3.2');
     * // => 3.2
     */
    function toFinite(value) {
      if (!value) {
        return value === 0 ? value : 0;
      }
      value = toNumber(value);
      if (value === INFINITY || value === -INFINITY) {
        var sign = (value < 0 ? -1 : 1);
        return sign * MAX_INTEGER;
      }
      return value === value ? value : 0;
    }

    /**
     * Converts `value` to an integer.
     *
     * **Note:** This method is loosely based on
     * [`ToInteger`](http://www.ecma-international.org/ecma-262/7.0/#sec-tointeger).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {number} Returns the converted integer.
     * @example
     *
     * _.toInteger(3.2);
     * // => 3
     *
     * _.toInteger(Number.MIN_VALUE);
     * // => 0
     *
     * _.toInteger(Infinity);
     * // => 1.7976931348623157e+308
     *
     * _.toInteger('3.2');
     * // => 3
     */
    function toInteger(value) {
      var result = toFinite(value),
          remainder = result % 1;

      return result === result ? (remainder ? result - remainder : result) : 0;
    }

    /**
     * Converts `value` to an integer suitable for use as the length of an
     * array-like object.
     *
     * **Note:** This method is based on
     * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {number} Returns the converted integer.
     * @example
     *
     * _.toLength(3.2);
     * // => 3
     *
     * _.toLength(Number.MIN_VALUE);
     * // => 0
     *
     * _.toLength(Infinity);
     * // => 4294967295
     *
     * _.toLength('3.2');
     * // => 3
     */
    function toLength(value) {
      return value ? baseClamp(toInteger(value), 0, MAX_ARRAY_LENGTH) : 0;
    }

    /**
     * Converts `value` to a number.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to process.
     * @returns {number} Returns the number.
     * @example
     *
     * _.toNumber(3.2);
     * // => 3.2
     *
     * _.toNumber(Number.MIN_VALUE);
     * // => 5e-324
     *
     * _.toNumber(Infinity);
     * // => Infinity
     *
     * _.toNumber('3.2');
     * // => 3.2
     */
    function toNumber(value) {
      if (typeof value == 'number') {
        return value;
      }
      if (isSymbol(value)) {
        return NAN;
      }
      if (isObject(value)) {
        var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
        value = isObject(other) ? (other + '') : other;
      }
      if (typeof value != 'string') {
        return value === 0 ? value : +value;
      }
      value = value.replace(reTrim, '');
      var isBinary = reIsBinary.test(value);
      return (isBinary || reIsOctal.test(value))
        ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
        : (reIsBadHex.test(value) ? NAN : +value);
    }

    /**
     * Converts `value` to a plain object flattening inherited enumerable string
     * keyed properties of `value` to own properties of the plain object.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {Object} Returns the converted plain object.
     * @example
     *
     * function Foo() {
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.assign({ 'a': 1 }, new Foo);
     * // => { 'a': 1, 'b': 2 }
     *
     * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
     * // => { 'a': 1, 'b': 2, 'c': 3 }
     */
    function toPlainObject(value) {
      return copyObject(value, keysIn(value));
    }

    /**
     * Converts `value` to a safe integer. A safe integer can be compared and
     * represented correctly.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {number} Returns the converted integer.
     * @example
     *
     * _.toSafeInteger(3.2);
     * // => 3
     *
     * _.toSafeInteger(Number.MIN_VALUE);
     * // => 0
     *
     * _.toSafeInteger(Infinity);
     * // => 9007199254740991
     *
     * _.toSafeInteger('3.2');
     * // => 3
     */
    function toSafeInteger(value) {
      return value
        ? baseClamp(toInteger(value), -MAX_SAFE_INTEGER, MAX_SAFE_INTEGER)
        : (value === 0 ? value : 0);
    }

    /**
     * Converts `value` to a string. An empty string is returned for `null`
     * and `undefined` values. The sign of `-0` is preserved.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Lang
     * @param {*} value The value to convert.
     * @returns {string} Returns the converted string.
     * @example
     *
     * _.toString(null);
     * // => ''
     *
     * _.toString(-0);
     * // => '-0'
     *
     * _.toString([1, 2, 3]);
     * // => '1,2,3'
     */
    function toString(value) {
      return value == null ? '' : baseToString(value);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Assigns own enumerable string keyed properties of source objects to the
     * destination object. Source objects are applied from left to right.
     * Subsequent sources overwrite property assignments of previous sources.
     *
     * **Note:** This method mutates `object` and is loosely based on
     * [`Object.assign`](https://mdn.io/Object/assign).
     *
     * @static
     * @memberOf _
     * @since 0.10.0
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @see _.assignIn
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     * }
     *
     * function Bar() {
     *   this.c = 3;
     * }
     *
     * Foo.prototype.b = 2;
     * Bar.prototype.d = 4;
     *
     * _.assign({ 'a': 0 }, new Foo, new Bar);
     * // => { 'a': 1, 'c': 3 }
     */
    var assign = createAssigner(function(object, source) {
      if (isPrototype(source) || isArrayLike(source)) {
        copyObject(source, keys(source), object);
        return;
      }
      for (var key in source) {
        if (hasOwnProperty.call(source, key)) {
          assignValue(object, key, source[key]);
        }
      }
    });

    /**
     * This method is like `_.assign` except that it iterates over own and
     * inherited source properties.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @alias extend
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @see _.assign
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     * }
     *
     * function Bar() {
     *   this.c = 3;
     * }
     *
     * Foo.prototype.b = 2;
     * Bar.prototype.d = 4;
     *
     * _.assignIn({ 'a': 0 }, new Foo, new Bar);
     * // => { 'a': 1, 'b': 2, 'c': 3, 'd': 4 }
     */
    var assignIn = createAssigner(function(object, source) {
      copyObject(source, keysIn(source), object);
    });

    /**
     * This method is like `_.assignIn` except that it accepts `customizer`
     * which is invoked to produce the assigned values. If `customizer` returns
     * `undefined`, assignment is handled by the method instead. The `customizer`
     * is invoked with five arguments: (objValue, srcValue, key, object, source).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @alias extendWith
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} sources The source objects.
     * @param {Function} [customizer] The function to customize assigned values.
     * @returns {Object} Returns `object`.
     * @see _.assignWith
     * @example
     *
     * function customizer(objValue, srcValue) {
     *   return _.isUndefined(objValue) ? srcValue : objValue;
     * }
     *
     * var defaults = _.partialRight(_.assignInWith, customizer);
     *
     * defaults({ 'a': 1 }, { 'b': 2 }, { 'a': 3 });
     * // => { 'a': 1, 'b': 2 }
     */
    var assignInWith = createAssigner(function(object, source, srcIndex, customizer) {
      copyObject(source, keysIn(source), object, customizer);
    });

    /**
     * This method is like `_.assign` except that it accepts `customizer`
     * which is invoked to produce the assigned values. If `customizer` returns
     * `undefined`, assignment is handled by the method instead. The `customizer`
     * is invoked with five arguments: (objValue, srcValue, key, object, source).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} sources The source objects.
     * @param {Function} [customizer] The function to customize assigned values.
     * @returns {Object} Returns `object`.
     * @see _.assignInWith
     * @example
     *
     * function customizer(objValue, srcValue) {
     *   return _.isUndefined(objValue) ? srcValue : objValue;
     * }
     *
     * var defaults = _.partialRight(_.assignWith, customizer);
     *
     * defaults({ 'a': 1 }, { 'b': 2 }, { 'a': 3 });
     * // => { 'a': 1, 'b': 2 }
     */
    var assignWith = createAssigner(function(object, source, srcIndex, customizer) {
      copyObject(source, keys(source), object, customizer);
    });

    /**
     * Creates an array of values corresponding to `paths` of `object`.
     *
     * @static
     * @memberOf _
     * @since 1.0.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {...(string|string[])} [paths] The property paths to pick.
     * @returns {Array} Returns the picked values.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }, 4] };
     *
     * _.at(object, ['a[0].b.c', 'a[1]']);
     * // => [3, 4]
     */
    var at = flatRest(baseAt);

    /**
     * Creates an object that inherits from the `prototype` object. If a
     * `properties` object is given, its own enumerable string keyed properties
     * are assigned to the created object.
     *
     * @static
     * @memberOf _
     * @since 2.3.0
     * @category Object
     * @param {Object} prototype The object to inherit from.
     * @param {Object} [properties] The properties to assign to the object.
     * @returns {Object} Returns the new object.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * function Circle() {
     *   Shape.call(this);
     * }
     *
     * Circle.prototype = _.create(Shape.prototype, {
     *   'constructor': Circle
     * });
     *
     * var circle = new Circle;
     * circle instanceof Circle;
     * // => true
     *
     * circle instanceof Shape;
     * // => true
     */
    function create(prototype, properties) {
      var result = baseCreate(prototype);
      return properties == null ? result : baseAssign(result, properties);
    }

    /**
     * Assigns own and inherited enumerable string keyed properties of source
     * objects to the destination object for all destination properties that
     * resolve to `undefined`. Source objects are applied from left to right.
     * Once a property is set, additional values of the same property are ignored.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @see _.defaultsDeep
     * @example
     *
     * _.defaults({ 'a': 1 }, { 'b': 2 }, { 'a': 3 });
     * // => { 'a': 1, 'b': 2 }
     */
    var defaults = baseRest(function(args) {
      args.push(undefined, customDefaultsAssignIn);
      return apply(assignInWith, undefined, args);
    });

    /**
     * This method is like `_.defaults` except that it recursively assigns
     * default properties.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 3.10.0
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @see _.defaults
     * @example
     *
     * _.defaultsDeep({ 'a': { 'b': 2 } }, { 'a': { 'b': 1, 'c': 3 } });
     * // => { 'a': { 'b': 2, 'c': 3 } }
     */
    var defaultsDeep = baseRest(function(args) {
      args.push(undefined, customDefaultsMerge);
      return apply(mergeWith, undefined, args);
    });

    /**
     * This method is like `_.find` except that it returns the key of the first
     * element `predicate` returns truthy for instead of the element itself.
     *
     * @static
     * @memberOf _
     * @since 1.1.0
     * @category Object
     * @param {Object} object The object to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {string|undefined} Returns the key of the matched element,
     *  else `undefined`.
     * @example
     *
     * var users = {
     *   'barney':  { 'age': 36, 'active': true },
     *   'fred':    { 'age': 40, 'active': false },
     *   'pebbles': { 'age': 1,  'active': true }
     * };
     *
     * _.findKey(users, function(o) { return o.age < 40; });
     * // => 'barney' (iteration order is not guaranteed)
     *
     * // The `_.matches` iteratee shorthand.
     * _.findKey(users, { 'age': 1, 'active': true });
     * // => 'pebbles'
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.findKey(users, ['active', false]);
     * // => 'fred'
     *
     * // The `_.property` iteratee shorthand.
     * _.findKey(users, 'active');
     * // => 'barney'
     */
    function findKey(object, predicate) {
      return baseFindKey(object, getIteratee(predicate, 3), baseForOwn);
    }

    /**
     * This method is like `_.findKey` except that it iterates over elements of
     * a collection in the opposite order.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Object
     * @param {Object} object The object to inspect.
     * @param {Function} [predicate=_.identity] The function invoked per iteration.
     * @returns {string|undefined} Returns the key of the matched element,
     *  else `undefined`.
     * @example
     *
     * var users = {
     *   'barney':  { 'age': 36, 'active': true },
     *   'fred':    { 'age': 40, 'active': false },
     *   'pebbles': { 'age': 1,  'active': true }
     * };
     *
     * _.findLastKey(users, function(o) { return o.age < 40; });
     * // => returns 'pebbles' assuming `_.findKey` returns 'barney'
     *
     * // The `_.matches` iteratee shorthand.
     * _.findLastKey(users, { 'age': 36, 'active': true });
     * // => 'barney'
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.findLastKey(users, ['active', false]);
     * // => 'fred'
     *
     * // The `_.property` iteratee shorthand.
     * _.findLastKey(users, 'active');
     * // => 'pebbles'
     */
    function findLastKey(object, predicate) {
      return baseFindKey(object, getIteratee(predicate, 3), baseForOwnRight);
    }

    /**
     * Iterates over own and inherited enumerable string keyed properties of an
     * object and invokes `iteratee` for each property. The iteratee is invoked
     * with three arguments: (value, key, object). Iteratee functions may exit
     * iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @since 0.3.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns `object`.
     * @see _.forInRight
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forIn(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => Logs 'a', 'b', then 'c' (iteration order is not guaranteed).
     */
    function forIn(object, iteratee) {
      return object == null
        ? object
        : baseFor(object, getIteratee(iteratee, 3), keysIn);
    }

    /**
     * This method is like `_.forIn` except that it iterates over properties of
     * `object` in the opposite order.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns `object`.
     * @see _.forIn
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forInRight(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => Logs 'c', 'b', then 'a' assuming `_.forIn` logs 'a', 'b', then 'c'.
     */
    function forInRight(object, iteratee) {
      return object == null
        ? object
        : baseForRight(object, getIteratee(iteratee, 3), keysIn);
    }

    /**
     * Iterates over own enumerable string keyed properties of an object and
     * invokes `iteratee` for each property. The iteratee is invoked with three
     * arguments: (value, key, object). Iteratee functions may exit iteration
     * early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @since 0.3.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns `object`.
     * @see _.forOwnRight
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forOwn(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => Logs 'a' then 'b' (iteration order is not guaranteed).
     */
    function forOwn(object, iteratee) {
      return object && baseForOwn(object, getIteratee(iteratee, 3));
    }

    /**
     * This method is like `_.forOwn` except that it iterates over properties of
     * `object` in the opposite order.
     *
     * @static
     * @memberOf _
     * @since 2.0.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns `object`.
     * @see _.forOwn
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.forOwnRight(new Foo, function(value, key) {
     *   console.log(key);
     * });
     * // => Logs 'b' then 'a' assuming `_.forOwn` logs 'a' then 'b'.
     */
    function forOwnRight(object, iteratee) {
      return object && baseForOwnRight(object, getIteratee(iteratee, 3));
    }

    /**
     * Creates an array of function property names from own enumerable properties
     * of `object`.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns the function names.
     * @see _.functionsIn
     * @example
     *
     * function Foo() {
     *   this.a = _.constant('a');
     *   this.b = _.constant('b');
     * }
     *
     * Foo.prototype.c = _.constant('c');
     *
     * _.functions(new Foo);
     * // => ['a', 'b']
     */
    function functions(object) {
      return object == null ? [] : baseFunctions(object, keys(object));
    }

    /**
     * Creates an array of function property names from own and inherited
     * enumerable properties of `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns the function names.
     * @see _.functions
     * @example
     *
     * function Foo() {
     *   this.a = _.constant('a');
     *   this.b = _.constant('b');
     * }
     *
     * Foo.prototype.c = _.constant('c');
     *
     * _.functionsIn(new Foo);
     * // => ['a', 'b', 'c']
     */
    function functionsIn(object) {
      return object == null ? [] : baseFunctions(object, keysIn(object));
    }

    /**
     * Gets the value at `path` of `object`. If the resolved value is
     * `undefined`, the `defaultValue` is returned in its place.
     *
     * @static
     * @memberOf _
     * @since 3.7.0
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to get.
     * @param {*} [defaultValue] The value returned for `undefined` resolved values.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.get(object, 'a[0].b.c');
     * // => 3
     *
     * _.get(object, ['a', '0', 'b', 'c']);
     * // => 3
     *
     * _.get(object, 'a.b.c', 'default');
     * // => 'default'
     */
    function get(object, path, defaultValue) {
      var result = object == null ? undefined : baseGet(object, path);
      return result === undefined ? defaultValue : result;
    }

    /**
     * Checks if `path` is a direct property of `object`.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path to check.
     * @returns {boolean} Returns `true` if `path` exists, else `false`.
     * @example
     *
     * var object = { 'a': { 'b': 2 } };
     * var other = _.create({ 'a': _.create({ 'b': 2 }) });
     *
     * _.has(object, 'a');
     * // => true
     *
     * _.has(object, 'a.b');
     * // => true
     *
     * _.has(object, ['a', 'b']);
     * // => true
     *
     * _.has(other, 'a');
     * // => false
     */
    function has(object, path) {
      return object != null && hasPath(object, path, baseHas);
    }

    /**
     * Checks if `path` is a direct or inherited property of `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path to check.
     * @returns {boolean} Returns `true` if `path` exists, else `false`.
     * @example
     *
     * var object = _.create({ 'a': _.create({ 'b': 2 }) });
     *
     * _.hasIn(object, 'a');
     * // => true
     *
     * _.hasIn(object, 'a.b');
     * // => true
     *
     * _.hasIn(object, ['a', 'b']);
     * // => true
     *
     * _.hasIn(object, 'b');
     * // => false
     */
    function hasIn(object, path) {
      return object != null && hasPath(object, path, baseHasIn);
    }

    /**
     * Creates an object composed of the inverted keys and values of `object`.
     * If `object` contains duplicate values, subsequent values overwrite
     * property assignments of previous values.
     *
     * @static
     * @memberOf _
     * @since 0.7.0
     * @category Object
     * @param {Object} object The object to invert.
     * @returns {Object} Returns the new inverted object.
     * @example
     *
     * var object = { 'a': 1, 'b': 2, 'c': 1 };
     *
     * _.invert(object);
     * // => { '1': 'c', '2': 'b' }
     */
    var invert = createInverter(function(result, value, key) {
      result[value] = key;
    }, constant(identity));

    /**
     * This method is like `_.invert` except that the inverted object is generated
     * from the results of running each element of `object` thru `iteratee`. The
     * corresponding inverted value of each inverted key is an array of keys
     * responsible for generating the inverted value. The iteratee is invoked
     * with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.1.0
     * @category Object
     * @param {Object} object The object to invert.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {Object} Returns the new inverted object.
     * @example
     *
     * var object = { 'a': 1, 'b': 2, 'c': 1 };
     *
     * _.invertBy(object);
     * // => { '1': ['a', 'c'], '2': ['b'] }
     *
     * _.invertBy(object, function(value) {
     *   return 'group' + value;
     * });
     * // => { 'group1': ['a', 'c'], 'group2': ['b'] }
     */
    var invertBy = createInverter(function(result, value, key) {
      if (hasOwnProperty.call(result, value)) {
        result[value].push(key);
      } else {
        result[value] = [key];
      }
    }, getIteratee);

    /**
     * Invokes the method at `path` of `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the method to invoke.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {*} Returns the result of the invoked method.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': [1, 2, 3, 4] } }] };
     *
     * _.invoke(object, 'a[0].b.c.slice', 1, 3);
     * // => [2, 3]
     */
    var invoke = baseRest(baseInvoke);

    /**
     * Creates an array of the own enumerable property names of `object`.
     *
     * **Note:** Non-object values are coerced to objects. See the
     * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
     * for more details.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.keys(new Foo);
     * // => ['a', 'b'] (iteration order is not guaranteed)
     *
     * _.keys('hi');
     * // => ['0', '1']
     */
    function keys(object) {
      return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
    }

    /**
     * Creates an array of the own and inherited enumerable property names of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property names.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.keysIn(new Foo);
     * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
     */
    function keysIn(object) {
      return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
    }

    /**
     * The opposite of `_.mapValues`; this method creates an object with the
     * same values as `object` and keys generated by running each own enumerable
     * string keyed property of `object` thru `iteratee`. The iteratee is invoked
     * with three arguments: (value, key, object).
     *
     * @static
     * @memberOf _
     * @since 3.8.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns the new mapped object.
     * @see _.mapValues
     * @example
     *
     * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
     *   return key + value;
     * });
     * // => { 'a1': 1, 'b2': 2 }
     */
    function mapKeys(object, iteratee) {
      var result = {};
      iteratee = getIteratee(iteratee, 3);

      baseForOwn(object, function(value, key, object) {
        baseAssignValue(result, iteratee(value, key, object), value);
      });
      return result;
    }

    /**
     * Creates an object with the same keys as `object` and values generated
     * by running each own enumerable string keyed property of `object` thru
     * `iteratee`. The iteratee is invoked with three arguments:
     * (value, key, object).
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Object} Returns the new mapped object.
     * @see _.mapKeys
     * @example
     *
     * var users = {
     *   'fred':    { 'user': 'fred',    'age': 40 },
     *   'pebbles': { 'user': 'pebbles', 'age': 1 }
     * };
     *
     * _.mapValues(users, function(o) { return o.age; });
     * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
     *
     * // The `_.property` iteratee shorthand.
     * _.mapValues(users, 'age');
     * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
     */
    function mapValues(object, iteratee) {
      var result = {};
      iteratee = getIteratee(iteratee, 3);

      baseForOwn(object, function(value, key, object) {
        baseAssignValue(result, key, iteratee(value, key, object));
      });
      return result;
    }

    /**
     * This method is like `_.assign` except that it recursively merges own and
     * inherited enumerable string keyed properties of source objects into the
     * destination object. Source properties that resolve to `undefined` are
     * skipped if a destination value exists. Array and plain object properties
     * are merged recursively. Other objects and value types are overridden by
     * assignment. Source objects are applied from left to right. Subsequent
     * sources overwrite property assignments of previous sources.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 0.5.0
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} [sources] The source objects.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = {
     *   'a': [{ 'b': 2 }, { 'd': 4 }]
     * };
     *
     * var other = {
     *   'a': [{ 'c': 3 }, { 'e': 5 }]
     * };
     *
     * _.merge(object, other);
     * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
     */
    var merge = createAssigner(function(object, source, srcIndex) {
      baseMerge(object, source, srcIndex);
    });

    /**
     * This method is like `_.merge` except that it accepts `customizer` which
     * is invoked to produce the merged values of the destination and source
     * properties. If `customizer` returns `undefined`, merging is handled by the
     * method instead. The `customizer` is invoked with six arguments:
     * (objValue, srcValue, key, object, source, stack).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The destination object.
     * @param {...Object} sources The source objects.
     * @param {Function} customizer The function to customize assigned values.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function customizer(objValue, srcValue) {
     *   if (_.isArray(objValue)) {
     *     return objValue.concat(srcValue);
     *   }
     * }
     *
     * var object = { 'a': [1], 'b': [2] };
     * var other = { 'a': [3], 'b': [4] };
     *
     * _.mergeWith(object, other, customizer);
     * // => { 'a': [1, 3], 'b': [2, 4] }
     */
    var mergeWith = createAssigner(function(object, source, srcIndex, customizer) {
      baseMerge(object, source, srcIndex, customizer);
    });

    /**
     * The opposite of `_.pick`; this method creates an object composed of the
     * own and inherited enumerable property paths of `object` that are not omitted.
     *
     * **Note:** This method is considerably slower than `_.pick`.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The source object.
     * @param {...(string|string[])} [paths] The property paths to omit.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'a': 1, 'b': '2', 'c': 3 };
     *
     * _.omit(object, ['a', 'c']);
     * // => { 'b': '2' }
     */
    var omit = flatRest(function(object, paths) {
      var result = {};
      if (object == null) {
        return result;
      }
      var isDeep = false;
      paths = arrayMap(paths, function(path) {
        path = castPath(path, object);
        isDeep || (isDeep = path.length > 1);
        return path;
      });
      copyObject(object, getAllKeysIn(object), result);
      if (isDeep) {
        result = baseClone(result, CLONE_DEEP_FLAG | CLONE_FLAT_FLAG | CLONE_SYMBOLS_FLAG, customOmitClone);
      }
      var length = paths.length;
      while (length--) {
        baseUnset(result, paths[length]);
      }
      return result;
    });

    /**
     * The opposite of `_.pickBy`; this method creates an object composed of
     * the own and inherited enumerable string keyed properties of `object` that
     * `predicate` doesn't return truthy for. The predicate is invoked with two
     * arguments: (value, key).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The source object.
     * @param {Function} [predicate=_.identity] The function invoked per property.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'a': 1, 'b': '2', 'c': 3 };
     *
     * _.omitBy(object, _.isNumber);
     * // => { 'b': '2' }
     */
    function omitBy(object, predicate) {
      return pickBy(object, negate(getIteratee(predicate)));
    }

    /**
     * Creates an object composed of the picked `object` properties.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The source object.
     * @param {...(string|string[])} [paths] The property paths to pick.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'a': 1, 'b': '2', 'c': 3 };
     *
     * _.pick(object, ['a', 'c']);
     * // => { 'a': 1, 'c': 3 }
     */
    var pick = flatRest(function(object, paths) {
      return object == null ? {} : basePick(object, paths);
    });

    /**
     * Creates an object composed of the `object` properties `predicate` returns
     * truthy for. The predicate is invoked with two arguments: (value, key).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The source object.
     * @param {Function} [predicate=_.identity] The function invoked per property.
     * @returns {Object} Returns the new object.
     * @example
     *
     * var object = { 'a': 1, 'b': '2', 'c': 3 };
     *
     * _.pickBy(object, _.isNumber);
     * // => { 'a': 1, 'c': 3 }
     */
    function pickBy(object, predicate) {
      if (object == null) {
        return {};
      }
      var props = arrayMap(getAllKeysIn(object), function(prop) {
        return [prop];
      });
      predicate = getIteratee(predicate);
      return basePickBy(object, props, function(value, path) {
        return predicate(value, path[0]);
      });
    }

    /**
     * This method is like `_.get` except that if the resolved value is a
     * function it's invoked with the `this` binding of its parent object and
     * its result is returned.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @param {Array|string} path The path of the property to resolve.
     * @param {*} [defaultValue] The value returned for `undefined` resolved values.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c1': 3, 'c2': _.constant(4) } }] };
     *
     * _.result(object, 'a[0].b.c1');
     * // => 3
     *
     * _.result(object, 'a[0].b.c2');
     * // => 4
     *
     * _.result(object, 'a[0].b.c3', 'default');
     * // => 'default'
     *
     * _.result(object, 'a[0].b.c3', _.constant('default'));
     * // => 'default'
     */
    function result(object, path, defaultValue) {
      path = castPath(path, object);

      var index = -1,
          length = path.length;

      // Ensure the loop is entered when path is empty.
      if (!length) {
        length = 1;
        object = undefined;
      }
      while (++index < length) {
        var value = object == null ? undefined : object[toKey(path[index])];
        if (value === undefined) {
          index = length;
          value = defaultValue;
        }
        object = isFunction(value) ? value.call(object) : value;
      }
      return object;
    }

    /**
     * Sets the value at `path` of `object`. If a portion of `path` doesn't exist,
     * it's created. Arrays are created for missing index properties while objects
     * are created for all other missing properties. Use `_.setWith` to customize
     * `path` creation.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 3.7.0
     * @category Object
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to set.
     * @param {*} value The value to set.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.set(object, 'a[0].b.c', 4);
     * console.log(object.a[0].b.c);
     * // => 4
     *
     * _.set(object, ['x', '0', 'y', 'z'], 5);
     * console.log(object.x[0].y.z);
     * // => 5
     */
    function set(object, path, value) {
      return object == null ? object : baseSet(object, path, value);
    }

    /**
     * This method is like `_.set` except that it accepts `customizer` which is
     * invoked to produce the objects of `path`.  If `customizer` returns `undefined`
     * path creation is handled by the method instead. The `customizer` is invoked
     * with three arguments: (nsValue, key, nsObject).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to set.
     * @param {*} value The value to set.
     * @param {Function} [customizer] The function to customize assigned values.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = {};
     *
     * _.setWith(object, '[0][1]', 'a', Object);
     * // => { '0': { '1': 'a' } }
     */
    function setWith(object, path, value, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      return object == null ? object : baseSet(object, path, value, customizer);
    }

    /**
     * Creates an array of own enumerable string keyed-value pairs for `object`
     * which can be consumed by `_.fromPairs`. If `object` is a map or set, its
     * entries are returned.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @alias entries
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the key-value pairs.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.toPairs(new Foo);
     * // => [['a', 1], ['b', 2]] (iteration order is not guaranteed)
     */
    var toPairs = createToPairs(keys);

    /**
     * Creates an array of own and inherited enumerable string keyed-value pairs
     * for `object` which can be consumed by `_.fromPairs`. If `object` is a map
     * or set, its entries are returned.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @alias entriesIn
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the key-value pairs.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.toPairsIn(new Foo);
     * // => [['a', 1], ['b', 2], ['c', 3]] (iteration order is not guaranteed)
     */
    var toPairsIn = createToPairs(keysIn);

    /**
     * An alternative to `_.reduce`; this method transforms `object` to a new
     * `accumulator` object which is the result of running each of its own
     * enumerable string keyed properties thru `iteratee`, with each invocation
     * potentially mutating the `accumulator` object. If `accumulator` is not
     * provided, a new object with the same `[[Prototype]]` will be used. The
     * iteratee is invoked with four arguments: (accumulator, value, key, object).
     * Iteratee functions may exit iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @since 1.3.0
     * @category Object
     * @param {Object} object The object to iterate over.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @param {*} [accumulator] The custom accumulator value.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * _.transform([2, 3, 4], function(result, n) {
     *   result.push(n *= n);
     *   return n % 2 == 0;
     * }, []);
     * // => [4, 9]
     *
     * _.transform({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
     *   (result[value] || (result[value] = [])).push(key);
     * }, {});
     * // => { '1': ['a', 'c'], '2': ['b'] }
     */
    function transform(object, iteratee, accumulator) {
      var isArr = isArray(object),
          isArrLike = isArr || isBuffer(object) || isTypedArray(object);

      iteratee = getIteratee(iteratee, 4);
      if (accumulator == null) {
        var Ctor = object && object.constructor;
        if (isArrLike) {
          accumulator = isArr ? new Ctor : [];
        }
        else if (isObject(object)) {
          accumulator = isFunction(Ctor) ? baseCreate(getPrototype(object)) : {};
        }
        else {
          accumulator = {};
        }
      }
      (isArrLike ? arrayEach : baseForOwn)(object, function(value, index, object) {
        return iteratee(accumulator, value, index, object);
      });
      return accumulator;
    }

    /**
     * Removes the property at `path` of `object`.
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Object
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to unset.
     * @returns {boolean} Returns `true` if the property is deleted, else `false`.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 7 } }] };
     * _.unset(object, 'a[0].b.c');
     * // => true
     *
     * console.log(object);
     * // => { 'a': [{ 'b': {} }] };
     *
     * _.unset(object, ['a', '0', 'b', 'c']);
     * // => true
     *
     * console.log(object);
     * // => { 'a': [{ 'b': {} }] };
     */
    function unset(object, path) {
      return object == null ? true : baseUnset(object, path);
    }

    /**
     * This method is like `_.set` except that accepts `updater` to produce the
     * value to set. Use `_.updateWith` to customize `path` creation. The `updater`
     * is invoked with one argument: (value).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.6.0
     * @category Object
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to set.
     * @param {Function} updater The function to produce the updated value.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = { 'a': [{ 'b': { 'c': 3 } }] };
     *
     * _.update(object, 'a[0].b.c', function(n) { return n * n; });
     * console.log(object.a[0].b.c);
     * // => 9
     *
     * _.update(object, 'x[0].y.z', function(n) { return n ? n + 1 : 0; });
     * console.log(object.x[0].y.z);
     * // => 0
     */
    function update(object, path, updater) {
      return object == null ? object : baseUpdate(object, path, castFunction(updater));
    }

    /**
     * This method is like `_.update` except that it accepts `customizer` which is
     * invoked to produce the objects of `path`.  If `customizer` returns `undefined`
     * path creation is handled by the method instead. The `customizer` is invoked
     * with three arguments: (nsValue, key, nsObject).
     *
     * **Note:** This method mutates `object`.
     *
     * @static
     * @memberOf _
     * @since 4.6.0
     * @category Object
     * @param {Object} object The object to modify.
     * @param {Array|string} path The path of the property to set.
     * @param {Function} updater The function to produce the updated value.
     * @param {Function} [customizer] The function to customize assigned values.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var object = {};
     *
     * _.updateWith(object, '[0][1]', _.constant('a'), Object);
     * // => { '0': { '1': 'a' } }
     */
    function updateWith(object, path, updater, customizer) {
      customizer = typeof customizer == 'function' ? customizer : undefined;
      return object == null ? object : baseUpdate(object, path, castFunction(updater), customizer);
    }

    /**
     * Creates an array of the own enumerable string keyed property values of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property values.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.values(new Foo);
     * // => [1, 2] (iteration order is not guaranteed)
     *
     * _.values('hi');
     * // => ['h', 'i']
     */
    function values(object) {
      return object == null ? [] : baseValues(object, keys(object));
    }

    /**
     * Creates an array of the own and inherited enumerable string keyed property
     * values of `object`.
     *
     * **Note:** Non-object values are coerced to objects.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Object
     * @param {Object} object The object to query.
     * @returns {Array} Returns the array of property values.
     * @example
     *
     * function Foo() {
     *   this.a = 1;
     *   this.b = 2;
     * }
     *
     * Foo.prototype.c = 3;
     *
     * _.valuesIn(new Foo);
     * // => [1, 2, 3] (iteration order is not guaranteed)
     */
    function valuesIn(object) {
      return object == null ? [] : baseValues(object, keysIn(object));
    }

    /*------------------------------------------------------------------------*/

    /**
     * Clamps `number` within the inclusive `lower` and `upper` bounds.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Number
     * @param {number} number The number to clamp.
     * @param {number} [lower] The lower bound.
     * @param {number} upper The upper bound.
     * @returns {number} Returns the clamped number.
     * @example
     *
     * _.clamp(-10, -5, 5);
     * // => -5
     *
     * _.clamp(10, -5, 5);
     * // => 5
     */
    function clamp(number, lower, upper) {
      if (upper === undefined) {
        upper = lower;
        lower = undefined;
      }
      if (upper !== undefined) {
        upper = toNumber(upper);
        upper = upper === upper ? upper : 0;
      }
      if (lower !== undefined) {
        lower = toNumber(lower);
        lower = lower === lower ? lower : 0;
      }
      return baseClamp(toNumber(number), lower, upper);
    }

    /**
     * Checks if `n` is between `start` and up to, but not including, `end`. If
     * `end` is not specified, it's set to `start` with `start` then set to `0`.
     * If `start` is greater than `end` the params are swapped to support
     * negative ranges.
     *
     * @static
     * @memberOf _
     * @since 3.3.0
     * @category Number
     * @param {number} number The number to check.
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @returns {boolean} Returns `true` if `number` is in the range, else `false`.
     * @see _.range, _.rangeRight
     * @example
     *
     * _.inRange(3, 2, 4);
     * // => true
     *
     * _.inRange(4, 8);
     * // => true
     *
     * _.inRange(4, 2);
     * // => false
     *
     * _.inRange(2, 2);
     * // => false
     *
     * _.inRange(1.2, 2);
     * // => true
     *
     * _.inRange(5.2, 4);
     * // => false
     *
     * _.inRange(-3, -2, -6);
     * // => true
     */
    function inRange(number, start, end) {
      start = toFinite(start);
      if (end === undefined) {
        end = start;
        start = 0;
      } else {
        end = toFinite(end);
      }
      number = toNumber(number);
      return baseInRange(number, start, end);
    }

    /**
     * Produces a random number between the inclusive `lower` and `upper` bounds.
     * If only one argument is provided a number between `0` and the given number
     * is returned. If `floating` is `true`, or either `lower` or `upper` are
     * floats, a floating-point number is returned instead of an integer.
     *
     * **Note:** JavaScript follows the IEEE-754 standard for resolving
     * floating-point values which can produce unexpected results.
     *
     * @static
     * @memberOf _
     * @since 0.7.0
     * @category Number
     * @param {number} [lower=0] The lower bound.
     * @param {number} [upper=1] The upper bound.
     * @param {boolean} [floating] Specify returning a floating-point number.
     * @returns {number} Returns the random number.
     * @example
     *
     * _.random(0, 5);
     * // => an integer between 0 and 5
     *
     * _.random(5);
     * // => also an integer between 0 and 5
     *
     * _.random(5, true);
     * // => a floating-point number between 0 and 5
     *
     * _.random(1.2, 5.2);
     * // => a floating-point number between 1.2 and 5.2
     */
    function random(lower, upper, floating) {
      if (floating && typeof floating != 'boolean' && isIterateeCall(lower, upper, floating)) {
        upper = floating = undefined;
      }
      if (floating === undefined) {
        if (typeof upper == 'boolean') {
          floating = upper;
          upper = undefined;
        }
        else if (typeof lower == 'boolean') {
          floating = lower;
          lower = undefined;
        }
      }
      if (lower === undefined && upper === undefined) {
        lower = 0;
        upper = 1;
      }
      else {
        lower = toFinite(lower);
        if (upper === undefined) {
          upper = lower;
          lower = 0;
        } else {
          upper = toFinite(upper);
        }
      }
      if (lower > upper) {
        var temp = lower;
        lower = upper;
        upper = temp;
      }
      if (floating || lower % 1 || upper % 1) {
        var rand = nativeRandom();
        return nativeMin(lower + (rand * (upper - lower + freeParseFloat('1e-' + ((rand + '').length - 1)))), upper);
      }
      return baseRandom(lower, upper);
    }

    /*------------------------------------------------------------------------*/

    /**
     * Converts `string` to [camel case](https://en.wikipedia.org/wiki/CamelCase).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the camel cased string.
     * @example
     *
     * _.camelCase('Foo Bar');
     * // => 'fooBar'
     *
     * _.camelCase('--foo-bar--');
     * // => 'fooBar'
     *
     * _.camelCase('__FOO_BAR__');
     * // => 'fooBar'
     */
    var camelCase = createCompounder(function(result, word, index) {
      word = word.toLowerCase();
      return result + (index ? capitalize(word) : word);
    });

    /**
     * Converts the first character of `string` to upper case and the remaining
     * to lower case.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to capitalize.
     * @returns {string} Returns the capitalized string.
     * @example
     *
     * _.capitalize('FRED');
     * // => 'Fred'
     */
    function capitalize(string) {
      return upperFirst(toString(string).toLowerCase());
    }

    /**
     * Deburrs `string` by converting
     * [Latin-1 Supplement](https://en.wikipedia.org/wiki/Latin-1_Supplement_(Unicode_block)#Character_table)
     * and [Latin Extended-A](https://en.wikipedia.org/wiki/Latin_Extended-A)
     * letters to basic Latin letters and removing
     * [combining diacritical marks](https://en.wikipedia.org/wiki/Combining_Diacritical_Marks).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to deburr.
     * @returns {string} Returns the deburred string.
     * @example
     *
     * _.deburr('déjà vu');
     * // => 'deja vu'
     */
    function deburr(string) {
      string = toString(string);
      return string && string.replace(reLatin, deburrLetter).replace(reComboMark, '');
    }

    /**
     * Checks if `string` ends with the given target string.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to inspect.
     * @param {string} [target] The string to search for.
     * @param {number} [position=string.length] The position to search up to.
     * @returns {boolean} Returns `true` if `string` ends with `target`,
     *  else `false`.
     * @example
     *
     * _.endsWith('abc', 'c');
     * // => true
     *
     * _.endsWith('abc', 'b');
     * // => false
     *
     * _.endsWith('abc', 'b', 2);
     * // => true
     */
    function endsWith(string, target, position) {
      string = toString(string);
      target = baseToString(target);

      var length = string.length;
      position = position === undefined
        ? length
        : baseClamp(toInteger(position), 0, length);

      var end = position;
      position -= target.length;
      return position >= 0 && string.slice(position, end) == target;
    }

    /**
     * Converts the characters "&", "<", ">", '"', and "'" in `string` to their
     * corresponding HTML entities.
     *
     * **Note:** No other characters are escaped. To escape additional
     * characters use a third-party library like [_he_](https://mths.be/he).
     *
     * Though the ">" character is escaped for symmetry, characters like
     * ">" and "/" don't need escaping in HTML and have no special meaning
     * unless they're part of a tag or unquoted attribute value. See
     * [Mathias Bynens's article](https://mathiasbynens.be/notes/ambiguous-ampersands)
     * (under "semi-related fun fact") for more details.
     *
     * When working with HTML you should always
     * [quote attribute values](http://wonko.com/post/html-escaping) to reduce
     * XSS vectors.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category String
     * @param {string} [string=''] The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escape('fred, barney, & pebbles');
     * // => 'fred, barney, &amp; pebbles'
     */
    function escape(string) {
      string = toString(string);
      return (string && reHasUnescapedHtml.test(string))
        ? string.replace(reUnescapedHtml, escapeHtmlChar)
        : string;
    }

    /**
     * Escapes the `RegExp` special characters "^", "$", "\", ".", "*", "+",
     * "?", "(", ")", "[", "]", "{", "}", and "|" in `string`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escapeRegExp('[lodash](https://lodash.com/)');
     * // => '\[lodash\]\(https://lodash\.com/\)'
     */
    function escapeRegExp(string) {
      string = toString(string);
      return (string && reHasRegExpChar.test(string))
        ? string.replace(reRegExpChar, '\\$&')
        : string;
    }

    /**
     * Converts `string` to
     * [kebab case](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the kebab cased string.
     * @example
     *
     * _.kebabCase('Foo Bar');
     * // => 'foo-bar'
     *
     * _.kebabCase('fooBar');
     * // => 'foo-bar'
     *
     * _.kebabCase('__FOO_BAR__');
     * // => 'foo-bar'
     */
    var kebabCase = createCompounder(function(result, word, index) {
      return result + (index ? '-' : '') + word.toLowerCase();
    });

    /**
     * Converts `string`, as space separated words, to lower case.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the lower cased string.
     * @example
     *
     * _.lowerCase('--Foo-Bar--');
     * // => 'foo bar'
     *
     * _.lowerCase('fooBar');
     * // => 'foo bar'
     *
     * _.lowerCase('__FOO_BAR__');
     * // => 'foo bar'
     */
    var lowerCase = createCompounder(function(result, word, index) {
      return result + (index ? ' ' : '') + word.toLowerCase();
    });

    /**
     * Converts the first character of `string` to lower case.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the converted string.
     * @example
     *
     * _.lowerFirst('Fred');
     * // => 'fred'
     *
     * _.lowerFirst('FRED');
     * // => 'fRED'
     */
    var lowerFirst = createCaseFirst('toLowerCase');

    /**
     * Pads `string` on the left and right sides if it's shorter than `length`.
     * Padding characters are truncated if they can't be evenly divided by `length`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.pad('abc', 8);
     * // => '  abc   '
     *
     * _.pad('abc', 8, '_-');
     * // => '_-abc_-_'
     *
     * _.pad('abc', 3);
     * // => 'abc'
     */
    function pad(string, length, chars) {
      string = toString(string);
      length = toInteger(length);

      var strLength = length ? stringSize(string) : 0;
      if (!length || strLength >= length) {
        return string;
      }
      var mid = (length - strLength) / 2;
      return (
        createPadding(nativeFloor(mid), chars) +
        string +
        createPadding(nativeCeil(mid), chars)
      );
    }

    /**
     * Pads `string` on the right side if it's shorter than `length`. Padding
     * characters are truncated if they exceed `length`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.padEnd('abc', 6);
     * // => 'abc   '
     *
     * _.padEnd('abc', 6, '_-');
     * // => 'abc_-_'
     *
     * _.padEnd('abc', 3);
     * // => 'abc'
     */
    function padEnd(string, length, chars) {
      string = toString(string);
      length = toInteger(length);

      var strLength = length ? stringSize(string) : 0;
      return (length && strLength < length)
        ? (string + createPadding(length - strLength, chars))
        : string;
    }

    /**
     * Pads `string` on the left side if it's shorter than `length`. Padding
     * characters are truncated if they exceed `length`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to pad.
     * @param {number} [length=0] The padding length.
     * @param {string} [chars=' '] The string used as padding.
     * @returns {string} Returns the padded string.
     * @example
     *
     * _.padStart('abc', 6);
     * // => '   abc'
     *
     * _.padStart('abc', 6, '_-');
     * // => '_-_abc'
     *
     * _.padStart('abc', 3);
     * // => 'abc'
     */
    function padStart(string, length, chars) {
      string = toString(string);
      length = toInteger(length);

      var strLength = length ? stringSize(string) : 0;
      return (length && strLength < length)
        ? (createPadding(length - strLength, chars) + string)
        : string;
    }

    /**
     * Converts `string` to an integer of the specified radix. If `radix` is
     * `undefined` or `0`, a `radix` of `10` is used unless `value` is a
     * hexadecimal, in which case a `radix` of `16` is used.
     *
     * **Note:** This method aligns with the
     * [ES5 implementation](https://es5.github.io/#x15.1.2.2) of `parseInt`.
     *
     * @static
     * @memberOf _
     * @since 1.1.0
     * @category String
     * @param {string} string The string to convert.
     * @param {number} [radix=10] The radix to interpret `value` by.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {number} Returns the converted integer.
     * @example
     *
     * _.parseInt('08');
     * // => 8
     *
     * _.map(['6', '08', '10'], _.parseInt);
     * // => [6, 8, 10]
     */
    function parseInt(string, radix, guard) {
      if (guard || radix == null) {
        radix = 0;
      } else if (radix) {
        radix = +radix;
      }
      return nativeParseInt(toString(string).replace(reTrimStart, ''), radix || 0);
    }

    /**
     * Repeats the given string `n` times.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to repeat.
     * @param {number} [n=1] The number of times to repeat the string.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {string} Returns the repeated string.
     * @example
     *
     * _.repeat('*', 3);
     * // => '***'
     *
     * _.repeat('abc', 2);
     * // => 'abcabc'
     *
     * _.repeat('abc', 0);
     * // => ''
     */
    function repeat(string, n, guard) {
      if ((guard ? isIterateeCall(string, n, guard) : n === undefined)) {
        n = 1;
      } else {
        n = toInteger(n);
      }
      return baseRepeat(toString(string), n);
    }

    /**
     * Replaces matches for `pattern` in `string` with `replacement`.
     *
     * **Note:** This method is based on
     * [`String#replace`](https://mdn.io/String/replace).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to modify.
     * @param {RegExp|string} pattern The pattern to replace.
     * @param {Function|string} replacement The match replacement.
     * @returns {string} Returns the modified string.
     * @example
     *
     * _.replace('Hi Fred', 'Fred', 'Barney');
     * // => 'Hi Barney'
     */
    function replace() {
      var args = arguments,
          string = toString(args[0]);

      return args.length < 3 ? string : string.replace(args[1], args[2]);
    }

    /**
     * Converts `string` to
     * [snake case](https://en.wikipedia.org/wiki/Snake_case).
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the snake cased string.
     * @example
     *
     * _.snakeCase('Foo Bar');
     * // => 'foo_bar'
     *
     * _.snakeCase('fooBar');
     * // => 'foo_bar'
     *
     * _.snakeCase('--FOO-BAR--');
     * // => 'foo_bar'
     */
    var snakeCase = createCompounder(function(result, word, index) {
      return result + (index ? '_' : '') + word.toLowerCase();
    });

    /**
     * Splits `string` by `separator`.
     *
     * **Note:** This method is based on
     * [`String#split`](https://mdn.io/String/split).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to split.
     * @param {RegExp|string} separator The separator pattern to split by.
     * @param {number} [limit] The length to truncate results to.
     * @returns {Array} Returns the string segments.
     * @example
     *
     * _.split('a-b-c', '-', 2);
     * // => ['a', 'b']
     */
    function split(string, separator, limit) {
      if (limit && typeof limit != 'number' && isIterateeCall(string, separator, limit)) {
        separator = limit = undefined;
      }
      limit = limit === undefined ? MAX_ARRAY_LENGTH : limit >>> 0;
      if (!limit) {
        return [];
      }
      string = toString(string);
      if (string && (
            typeof separator == 'string' ||
            (separator != null && !isRegExp(separator))
          )) {
        separator = baseToString(separator);
        if (!separator && hasUnicode(string)) {
          return castSlice(stringToArray(string), 0, limit);
        }
      }
      return string.split(separator, limit);
    }

    /**
     * Converts `string` to
     * [start case](https://en.wikipedia.org/wiki/Letter_case#Stylistic_or_specialised_usage).
     *
     * @static
     * @memberOf _
     * @since 3.1.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the start cased string.
     * @example
     *
     * _.startCase('--foo-bar--');
     * // => 'Foo Bar'
     *
     * _.startCase('fooBar');
     * // => 'Foo Bar'
     *
     * _.startCase('__FOO_BAR__');
     * // => 'FOO BAR'
     */
    var startCase = createCompounder(function(result, word, index) {
      return result + (index ? ' ' : '') + upperFirst(word);
    });

    /**
     * Checks if `string` starts with the given target string.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to inspect.
     * @param {string} [target] The string to search for.
     * @param {number} [position=0] The position to search from.
     * @returns {boolean} Returns `true` if `string` starts with `target`,
     *  else `false`.
     * @example
     *
     * _.startsWith('abc', 'a');
     * // => true
     *
     * _.startsWith('abc', 'b');
     * // => false
     *
     * _.startsWith('abc', 'b', 1);
     * // => true
     */
    function startsWith(string, target, position) {
      string = toString(string);
      position = position == null
        ? 0
        : baseClamp(toInteger(position), 0, string.length);

      target = baseToString(target);
      return string.slice(position, position + target.length) == target;
    }

    /**
     * Creates a compiled template function that can interpolate data properties
     * in "interpolate" delimiters, HTML-escape interpolated data properties in
     * "escape" delimiters, and execute JavaScript in "evaluate" delimiters. Data
     * properties may be accessed as free variables in the template. If a setting
     * object is given, it takes precedence over `_.templateSettings` values.
     *
     * **Note:** In the development build `_.template` utilizes
     * [sourceURLs](http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl)
     * for easier debugging.
     *
     * For more information on precompiling templates see
     * [lodash's custom builds documentation](https://lodash.com/custom-builds).
     *
     * For more information on Chrome extension sandboxes see
     * [Chrome's extensions documentation](https://developer.chrome.com/extensions/sandboxingEval).
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category String
     * @param {string} [string=''] The template string.
     * @param {Object} [options={}] The options object.
     * @param {RegExp} [options.escape=_.templateSettings.escape]
     *  The HTML "escape" delimiter.
     * @param {RegExp} [options.evaluate=_.templateSettings.evaluate]
     *  The "evaluate" delimiter.
     * @param {Object} [options.imports=_.templateSettings.imports]
     *  An object to import into the template as free variables.
     * @param {RegExp} [options.interpolate=_.templateSettings.interpolate]
     *  The "interpolate" delimiter.
     * @param {string} [options.sourceURL='lodash.templateSources[n]']
     *  The sourceURL of the compiled template.
     * @param {string} [options.variable='obj']
     *  The data object variable name.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Function} Returns the compiled template function.
     * @example
     *
     * // Use the "interpolate" delimiter to create a compiled template.
     * var compiled = _.template('hello <%= user %>!');
     * compiled({ 'user': 'fred' });
     * // => 'hello fred!'
     *
     * // Use the HTML "escape" delimiter to escape data property values.
     * var compiled = _.template('<b><%- value %></b>');
     * compiled({ 'value': '<script>' });
     * // => '<b>&lt;script&gt;</b>'
     *
     * // Use the "evaluate" delimiter to execute JavaScript and generate HTML.
     * var compiled = _.template('<% _.forEach(users, function(user) { %><li><%- user %></li><% }); %>');
     * compiled({ 'users': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // Use the internal `print` function in "evaluate" delimiters.
     * var compiled = _.template('<% print("hello " + user); %>!');
     * compiled({ 'user': 'barney' });
     * // => 'hello barney!'
     *
     * // Use the ES template literal delimiter as an "interpolate" delimiter.
     * // Disable support by replacing the "interpolate" delimiter.
     * var compiled = _.template('hello ${ user }!');
     * compiled({ 'user': 'pebbles' });
     * // => 'hello pebbles!'
     *
     * // Use backslashes to treat delimiters as plain text.
     * var compiled = _.template('<%= "\\<%- value %\\>" %>');
     * compiled({ 'value': 'ignored' });
     * // => '<%- value %>'
     *
     * // Use the `imports` option to import `jQuery` as `jq`.
     * var text = '<% jq.each(users, function(user) { %><li><%- user %></li><% }); %>';
     * var compiled = _.template(text, { 'imports': { 'jq': jQuery } });
     * compiled({ 'users': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // Use the `sourceURL` option to specify a custom sourceURL for the template.
     * var compiled = _.template('hello <%= user %>!', { 'sourceURL': '/basic/greeting.jst' });
     * compiled(data);
     * // => Find the source of "greeting.jst" under the Sources tab or Resources panel of the web inspector.
     *
     * // Use the `variable` option to ensure a with-statement isn't used in the compiled template.
     * var compiled = _.template('hi <%= data.user %>!', { 'variable': 'data' });
     * compiled.source;
     * // => function(data) {
     * //   var __t, __p = '';
     * //   __p += 'hi ' + ((__t = ( data.user )) == null ? '' : __t) + '!';
     * //   return __p;
     * // }
     *
     * // Use custom template delimiters.
     * _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
     * var compiled = _.template('hello {{ user }}!');
     * compiled({ 'user': 'mustache' });
     * // => 'hello mustache!'
     *
     * // Use the `source` property to inline compiled templates for meaningful
     * // line numbers in error messages and stack traces.
     * fs.writeFileSync(path.join(process.cwd(), 'jst.js'), '\
     *   var JST = {\
     *     "main": ' + _.template(mainText).source + '\
     *   };\
     * ');
     */
    function template(string, options, guard) {
      // Based on John Resig's `tmpl` implementation
      // (http://ejohn.org/blog/javascript-micro-templating/)
      // and Laura Doktorova's doT.js (https://github.com/olado/doT).
      var settings = lodash.templateSettings;

      if (guard && isIterateeCall(string, options, guard)) {
        options = undefined;
      }
      string = toString(string);
      options = assignInWith({}, options, settings, customDefaultsAssignIn);

      var imports = assignInWith({}, options.imports, settings.imports, customDefaultsAssignIn),
          importsKeys = keys(imports),
          importsValues = baseValues(imports, importsKeys);

      var isEscaping,
          isEvaluating,
          index = 0,
          interpolate = options.interpolate || reNoMatch,
          source = "__p += '";

      // Compile the regexp to match each delimiter.
      var reDelimiters = RegExp(
        (options.escape || reNoMatch).source + '|' +
        interpolate.source + '|' +
        (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' +
        (options.evaluate || reNoMatch).source + '|$'
      , 'g');

      // Use a sourceURL for easier debugging.
      var sourceURL = '//# sourceURL=' +
        ('sourceURL' in options
          ? options.sourceURL
          : ('lodash.templateSources[' + (++templateCounter) + ']')
        ) + '\n';

      string.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
        interpolateValue || (interpolateValue = esTemplateValue);

        // Escape characters that can't be included in string literals.
        source += string.slice(index, offset).replace(reUnescapedString, escapeStringChar);

        // Replace delimiters with snippets.
        if (escapeValue) {
          isEscaping = true;
          source += "' +\n__e(" + escapeValue + ") +\n'";
        }
        if (evaluateValue) {
          isEvaluating = true;
          source += "';\n" + evaluateValue + ";\n__p += '";
        }
        if (interpolateValue) {
          source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
        }
        index = offset + match.length;

        // The JS engine embedded in Adobe products needs `match` returned in
        // order to produce the correct `offset` value.
        return match;
      });

      source += "';\n";

      // If `variable` is not specified wrap a with-statement around the generated
      // code to add the data object to the top of the scope chain.
      var variable = options.variable;
      if (!variable) {
        source = 'with (obj) {\n' + source + '\n}\n';
      }
      // Cleanup code by stripping empty strings.
      source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source)
        .replace(reEmptyStringMiddle, '$1')
        .replace(reEmptyStringTrailing, '$1;');

      // Frame code as the function body.
      source = 'function(' + (variable || 'obj') + ') {\n' +
        (variable
          ? ''
          : 'obj || (obj = {});\n'
        ) +
        "var __t, __p = ''" +
        (isEscaping
           ? ', __e = _.escape'
           : ''
        ) +
        (isEvaluating
          ? ', __j = Array.prototype.join;\n' +
            "function print() { __p += __j.call(arguments, '') }\n"
          : ';\n'
        ) +
        source +
        'return __p\n}';

      var result = attempt(function() {
        return Function(importsKeys, sourceURL + 'return ' + source)
          .apply(undefined, importsValues);
      });

      // Provide the compiled function's source by its `toString` method or
      // the `source` property as a convenience for inlining compiled templates.
      result.source = source;
      if (isError(result)) {
        throw result;
      }
      return result;
    }

    /**
     * Converts `string`, as a whole, to lower case just like
     * [String#toLowerCase](https://mdn.io/toLowerCase).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the lower cased string.
     * @example
     *
     * _.toLower('--Foo-Bar--');
     * // => '--foo-bar--'
     *
     * _.toLower('fooBar');
     * // => 'foobar'
     *
     * _.toLower('__FOO_BAR__');
     * // => '__foo_bar__'
     */
    function toLower(value) {
      return toString(value).toLowerCase();
    }

    /**
     * Converts `string`, as a whole, to upper case just like
     * [String#toUpperCase](https://mdn.io/toUpperCase).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the upper cased string.
     * @example
     *
     * _.toUpper('--foo-bar--');
     * // => '--FOO-BAR--'
     *
     * _.toUpper('fooBar');
     * // => 'FOOBAR'
     *
     * _.toUpper('__foo_bar__');
     * // => '__FOO_BAR__'
     */
    function toUpper(value) {
      return toString(value).toUpperCase();
    }

    /**
     * Removes leading and trailing whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trim('  abc  ');
     * // => 'abc'
     *
     * _.trim('-_-abc-_-', '_-');
     * // => 'abc'
     *
     * _.map(['  foo  ', '  bar  '], _.trim);
     * // => ['foo', 'bar']
     */
    function trim(string, chars, guard) {
      string = toString(string);
      if (string && (guard || chars === undefined)) {
        return string.replace(reTrim, '');
      }
      if (!string || !(chars = baseToString(chars))) {
        return string;
      }
      var strSymbols = stringToArray(string),
          chrSymbols = stringToArray(chars),
          start = charsStartIndex(strSymbols, chrSymbols),
          end = charsEndIndex(strSymbols, chrSymbols) + 1;

      return castSlice(strSymbols, start, end).join('');
    }

    /**
     * Removes trailing whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trimEnd('  abc  ');
     * // => '  abc'
     *
     * _.trimEnd('-_-abc-_-', '_-');
     * // => '-_-abc'
     */
    function trimEnd(string, chars, guard) {
      string = toString(string);
      if (string && (guard || chars === undefined)) {
        return string.replace(reTrimEnd, '');
      }
      if (!string || !(chars = baseToString(chars))) {
        return string;
      }
      var strSymbols = stringToArray(string),
          end = charsEndIndex(strSymbols, stringToArray(chars)) + 1;

      return castSlice(strSymbols, 0, end).join('');
    }

    /**
     * Removes leading whitespace or specified characters from `string`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to trim.
     * @param {string} [chars=whitespace] The characters to trim.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {string} Returns the trimmed string.
     * @example
     *
     * _.trimStart('  abc  ');
     * // => 'abc  '
     *
     * _.trimStart('-_-abc-_-', '_-');
     * // => 'abc-_-'
     */
    function trimStart(string, chars, guard) {
      string = toString(string);
      if (string && (guard || chars === undefined)) {
        return string.replace(reTrimStart, '');
      }
      if (!string || !(chars = baseToString(chars))) {
        return string;
      }
      var strSymbols = stringToArray(string),
          start = charsStartIndex(strSymbols, stringToArray(chars));

      return castSlice(strSymbols, start).join('');
    }

    /**
     * Truncates `string` if it's longer than the given maximum string length.
     * The last characters of the truncated string are replaced with the omission
     * string which defaults to "...".
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to truncate.
     * @param {Object} [options={}] The options object.
     * @param {number} [options.length=30] The maximum string length.
     * @param {string} [options.omission='...'] The string to indicate text is omitted.
     * @param {RegExp|string} [options.separator] The separator pattern to truncate to.
     * @returns {string} Returns the truncated string.
     * @example
     *
     * _.truncate('hi-diddly-ho there, neighborino');
     * // => 'hi-diddly-ho there, neighbo...'
     *
     * _.truncate('hi-diddly-ho there, neighborino', {
     *   'length': 24,
     *   'separator': ' '
     * });
     * // => 'hi-diddly-ho there,...'
     *
     * _.truncate('hi-diddly-ho there, neighborino', {
     *   'length': 24,
     *   'separator': /,? +/
     * });
     * // => 'hi-diddly-ho there...'
     *
     * _.truncate('hi-diddly-ho there, neighborino', {
     *   'omission': ' [...]'
     * });
     * // => 'hi-diddly-ho there, neig [...]'
     */
    function truncate(string, options) {
      var length = DEFAULT_TRUNC_LENGTH,
          omission = DEFAULT_TRUNC_OMISSION;

      if (isObject(options)) {
        var separator = 'separator' in options ? options.separator : separator;
        length = 'length' in options ? toInteger(options.length) : length;
        omission = 'omission' in options ? baseToString(options.omission) : omission;
      }
      string = toString(string);

      var strLength = string.length;
      if (hasUnicode(string)) {
        var strSymbols = stringToArray(string);
        strLength = strSymbols.length;
      }
      if (length >= strLength) {
        return string;
      }
      var end = length - stringSize(omission);
      if (end < 1) {
        return omission;
      }
      var result = strSymbols
        ? castSlice(strSymbols, 0, end).join('')
        : string.slice(0, end);

      if (separator === undefined) {
        return result + omission;
      }
      if (strSymbols) {
        end += (result.length - end);
      }
      if (isRegExp(separator)) {
        if (string.slice(end).search(separator)) {
          var match,
              substring = result;

          if (!separator.global) {
            separator = RegExp(separator.source, toString(reFlags.exec(separator)) + 'g');
          }
          separator.lastIndex = 0;
          while ((match = separator.exec(substring))) {
            var newEnd = match.index;
          }
          result = result.slice(0, newEnd === undefined ? end : newEnd);
        }
      } else if (string.indexOf(baseToString(separator), end) != end) {
        var index = result.lastIndexOf(separator);
        if (index > -1) {
          result = result.slice(0, index);
        }
      }
      return result + omission;
    }

    /**
     * The inverse of `_.escape`; this method converts the HTML entities
     * `&amp;`, `&lt;`, `&gt;`, `&quot;`, and `&#39;` in `string` to
     * their corresponding characters.
     *
     * **Note:** No other HTML entities are unescaped. To unescape additional
     * HTML entities use a third-party library like [_he_](https://mths.be/he).
     *
     * @static
     * @memberOf _
     * @since 0.6.0
     * @category String
     * @param {string} [string=''] The string to unescape.
     * @returns {string} Returns the unescaped string.
     * @example
     *
     * _.unescape('fred, barney, &amp; pebbles');
     * // => 'fred, barney, & pebbles'
     */
    function unescape(string) {
      string = toString(string);
      return (string && reHasEscapedHtml.test(string))
        ? string.replace(reEscapedHtml, unescapeHtmlChar)
        : string;
    }

    /**
     * Converts `string`, as space separated words, to upper case.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the upper cased string.
     * @example
     *
     * _.upperCase('--foo-bar');
     * // => 'FOO BAR'
     *
     * _.upperCase('fooBar');
     * // => 'FOO BAR'
     *
     * _.upperCase('__foo_bar__');
     * // => 'FOO BAR'
     */
    var upperCase = createCompounder(function(result, word, index) {
      return result + (index ? ' ' : '') + word.toUpperCase();
    });

    /**
     * Converts the first character of `string` to upper case.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category String
     * @param {string} [string=''] The string to convert.
     * @returns {string} Returns the converted string.
     * @example
     *
     * _.upperFirst('fred');
     * // => 'Fred'
     *
     * _.upperFirst('FRED');
     * // => 'FRED'
     */
    var upperFirst = createCaseFirst('toUpperCase');

    /**
     * Splits `string` into an array of its words.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category String
     * @param {string} [string=''] The string to inspect.
     * @param {RegExp|string} [pattern] The pattern to match words.
     * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
     * @returns {Array} Returns the words of `string`.
     * @example
     *
     * _.words('fred, barney, & pebbles');
     * // => ['fred', 'barney', 'pebbles']
     *
     * _.words('fred, barney, & pebbles', /[^, ]+/g);
     * // => ['fred', 'barney', '&', 'pebbles']
     */
    function words(string, pattern, guard) {
      string = toString(string);
      pattern = guard ? undefined : pattern;

      if (pattern === undefined) {
        return hasUnicodeWord(string) ? unicodeWords(string) : asciiWords(string);
      }
      return string.match(pattern) || [];
    }

    /*------------------------------------------------------------------------*/

    /**
     * Attempts to invoke `func`, returning either the result or the caught error
     * object. Any additional arguments are provided to `func` when it's invoked.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Util
     * @param {Function} func The function to attempt.
     * @param {...*} [args] The arguments to invoke `func` with.
     * @returns {*} Returns the `func` result or error object.
     * @example
     *
     * // Avoid throwing errors for invalid selectors.
     * var elements = _.attempt(function(selector) {
     *   return document.querySelectorAll(selector);
     * }, '>_>');
     *
     * if (_.isError(elements)) {
     *   elements = [];
     * }
     */
    var attempt = baseRest(function(func, args) {
      try {
        return apply(func, undefined, args);
      } catch (e) {
        return isError(e) ? e : new Error(e);
      }
    });

    /**
     * Binds methods of an object to the object itself, overwriting the existing
     * method.
     *
     * **Note:** This method doesn't set the "length" property of bound functions.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {Object} object The object to bind and assign the bound methods to.
     * @param {...(string|string[])} methodNames The object method names to bind.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var view = {
     *   'label': 'docs',
     *   'click': function() {
     *     console.log('clicked ' + this.label);
     *   }
     * };
     *
     * _.bindAll(view, ['click']);
     * jQuery(element).on('click', view.click);
     * // => Logs 'clicked docs' when clicked.
     */
    var bindAll = flatRest(function(object, methodNames) {
      arrayEach(methodNames, function(key) {
        key = toKey(key);
        baseAssignValue(object, key, bind(object[key], object));
      });
      return object;
    });

    /**
     * Creates a function that iterates over `pairs` and invokes the corresponding
     * function of the first predicate to return truthy. The predicate-function
     * pairs are invoked with the `this` binding and arguments of the created
     * function.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {Array} pairs The predicate-function pairs.
     * @returns {Function} Returns the new composite function.
     * @example
     *
     * var func = _.cond([
     *   [_.matches({ 'a': 1 }),           _.constant('matches A')],
     *   [_.conforms({ 'b': _.isNumber }), _.constant('matches B')],
     *   [_.stubTrue,                      _.constant('no match')]
     * ]);
     *
     * func({ 'a': 1, 'b': 2 });
     * // => 'matches A'
     *
     * func({ 'a': 0, 'b': 1 });
     * // => 'matches B'
     *
     * func({ 'a': '1', 'b': '2' });
     * // => 'no match'
     */
    function cond(pairs) {
      var length = pairs == null ? 0 : pairs.length,
          toIteratee = getIteratee();

      pairs = !length ? [] : arrayMap(pairs, function(pair) {
        if (typeof pair[1] != 'function') {
          throw new TypeError(FUNC_ERROR_TEXT);
        }
        return [toIteratee(pair[0]), pair[1]];
      });

      return baseRest(function(args) {
        var index = -1;
        while (++index < length) {
          var pair = pairs[index];
          if (apply(pair[0], this, args)) {
            return apply(pair[1], this, args);
          }
        }
      });
    }

    /**
     * Creates a function that invokes the predicate properties of `source` with
     * the corresponding property values of a given object, returning `true` if
     * all predicates return truthy, else `false`.
     *
     * **Note:** The created function is equivalent to `_.conformsTo` with
     * `source` partially applied.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {Object} source The object of property predicates to conform to.
     * @returns {Function} Returns the new spec function.
     * @example
     *
     * var objects = [
     *   { 'a': 2, 'b': 1 },
     *   { 'a': 1, 'b': 2 }
     * ];
     *
     * _.filter(objects, _.conforms({ 'b': function(n) { return n > 1; } }));
     * // => [{ 'a': 1, 'b': 2 }]
     */
    function conforms(source) {
      return baseConforms(baseClone(source, CLONE_DEEP_FLAG));
    }

    /**
     * Creates a function that returns `value`.
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Util
     * @param {*} value The value to return from the new function.
     * @returns {Function} Returns the new constant function.
     * @example
     *
     * var objects = _.times(2, _.constant({ 'a': 1 }));
     *
     * console.log(objects);
     * // => [{ 'a': 1 }, { 'a': 1 }]
     *
     * console.log(objects[0] === objects[1]);
     * // => true
     */
    function constant(value) {
      return function() {
        return value;
      };
    }

    /**
     * Checks `value` to determine whether a default value should be returned in
     * its place. The `defaultValue` is returned if `value` is `NaN`, `null`,
     * or `undefined`.
     *
     * @static
     * @memberOf _
     * @since 4.14.0
     * @category Util
     * @param {*} value The value to check.
     * @param {*} defaultValue The default value.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * _.defaultTo(1, 10);
     * // => 1
     *
     * _.defaultTo(undefined, 10);
     * // => 10
     */
    function defaultTo(value, defaultValue) {
      return (value == null || value !== value) ? defaultValue : value;
    }

    /**
     * Creates a function that returns the result of invoking the given functions
     * with the `this` binding of the created function, where each successive
     * invocation is supplied the return value of the previous.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Util
     * @param {...(Function|Function[])} [funcs] The functions to invoke.
     * @returns {Function} Returns the new composite function.
     * @see _.flowRight
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var addSquare = _.flow([_.add, square]);
     * addSquare(1, 2);
     * // => 9
     */
    var flow = createFlow();

    /**
     * This method is like `_.flow` except that it creates a function that
     * invokes the given functions from right to left.
     *
     * @static
     * @since 3.0.0
     * @memberOf _
     * @category Util
     * @param {...(Function|Function[])} [funcs] The functions to invoke.
     * @returns {Function} Returns the new composite function.
     * @see _.flow
     * @example
     *
     * function square(n) {
     *   return n * n;
     * }
     *
     * var addSquare = _.flowRight([square, _.add]);
     * addSquare(1, 2);
     * // => 9
     */
    var flowRight = createFlow(true);

    /**
     * This method returns the first argument it receives.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {*} value Any value.
     * @returns {*} Returns `value`.
     * @example
     *
     * var object = { 'a': 1 };
     *
     * console.log(_.identity(object) === object);
     * // => true
     */
    function identity(value) {
      return value;
    }

    /**
     * Creates a function that invokes `func` with the arguments of the created
     * function. If `func` is a property name, the created function returns the
     * property value for a given element. If `func` is an array or object, the
     * created function returns `true` for elements that contain the equivalent
     * source properties, otherwise it returns `false`.
     *
     * @static
     * @since 4.0.0
     * @memberOf _
     * @category Util
     * @param {*} [func=_.identity] The value to convert to a callback.
     * @returns {Function} Returns the callback.
     * @example
     *
     * var users = [
     *   { 'user': 'barney', 'age': 36, 'active': true },
     *   { 'user': 'fred',   'age': 40, 'active': false }
     * ];
     *
     * // The `_.matches` iteratee shorthand.
     * _.filter(users, _.iteratee({ 'user': 'barney', 'active': true }));
     * // => [{ 'user': 'barney', 'age': 36, 'active': true }]
     *
     * // The `_.matchesProperty` iteratee shorthand.
     * _.filter(users, _.iteratee(['user', 'fred']));
     * // => [{ 'user': 'fred', 'age': 40 }]
     *
     * // The `_.property` iteratee shorthand.
     * _.map(users, _.iteratee('user'));
     * // => ['barney', 'fred']
     *
     * // Create custom iteratee shorthands.
     * _.iteratee = _.wrap(_.iteratee, function(iteratee, func) {
     *   return !_.isRegExp(func) ? iteratee(func) : function(string) {
     *     return func.test(string);
     *   };
     * });
     *
     * _.filter(['abc', 'def'], /ef/);
     * // => ['def']
     */
    function iteratee(func) {
      return baseIteratee(typeof func == 'function' ? func : baseClone(func, CLONE_DEEP_FLAG));
    }

    /**
     * Creates a function that performs a partial deep comparison between a given
     * object and `source`, returning `true` if the given object has equivalent
     * property values, else `false`.
     *
     * **Note:** The created function is equivalent to `_.isMatch` with `source`
     * partially applied.
     *
     * Partial comparisons will match empty array and empty object `source`
     * values against any array or object value, respectively. See `_.isEqual`
     * for a list of supported value comparisons.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Util
     * @param {Object} source The object of property values to match.
     * @returns {Function} Returns the new spec function.
     * @example
     *
     * var objects = [
     *   { 'a': 1, 'b': 2, 'c': 3 },
     *   { 'a': 4, 'b': 5, 'c': 6 }
     * ];
     *
     * _.filter(objects, _.matches({ 'a': 4, 'c': 6 }));
     * // => [{ 'a': 4, 'b': 5, 'c': 6 }]
     */
    function matches(source) {
      return baseMatches(baseClone(source, CLONE_DEEP_FLAG));
    }

    /**
     * Creates a function that performs a partial deep comparison between the
     * value at `path` of a given object to `srcValue`, returning `true` if the
     * object value is equivalent, else `false`.
     *
     * **Note:** Partial comparisons will match empty array and empty object
     * `srcValue` values against any array or object value, respectively. See
     * `_.isEqual` for a list of supported value comparisons.
     *
     * @static
     * @memberOf _
     * @since 3.2.0
     * @category Util
     * @param {Array|string} path The path of the property to get.
     * @param {*} srcValue The value to match.
     * @returns {Function} Returns the new spec function.
     * @example
     *
     * var objects = [
     *   { 'a': 1, 'b': 2, 'c': 3 },
     *   { 'a': 4, 'b': 5, 'c': 6 }
     * ];
     *
     * _.find(objects, _.matchesProperty('a', 4));
     * // => { 'a': 4, 'b': 5, 'c': 6 }
     */
    function matchesProperty(path, srcValue) {
      return baseMatchesProperty(path, baseClone(srcValue, CLONE_DEEP_FLAG));
    }

    /**
     * Creates a function that invokes the method at `path` of a given object.
     * Any additional arguments are provided to the invoked method.
     *
     * @static
     * @memberOf _
     * @since 3.7.0
     * @category Util
     * @param {Array|string} path The path of the method to invoke.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {Function} Returns the new invoker function.
     * @example
     *
     * var objects = [
     *   { 'a': { 'b': _.constant(2) } },
     *   { 'a': { 'b': _.constant(1) } }
     * ];
     *
     * _.map(objects, _.method('a.b'));
     * // => [2, 1]
     *
     * _.map(objects, _.method(['a', 'b']));
     * // => [2, 1]
     */
    var method = baseRest(function(path, args) {
      return function(object) {
        return baseInvoke(object, path, args);
      };
    });

    /**
     * The opposite of `_.method`; this method creates a function that invokes
     * the method at a given path of `object`. Any additional arguments are
     * provided to the invoked method.
     *
     * @static
     * @memberOf _
     * @since 3.7.0
     * @category Util
     * @param {Object} object The object to query.
     * @param {...*} [args] The arguments to invoke the method with.
     * @returns {Function} Returns the new invoker function.
     * @example
     *
     * var array = _.times(3, _.constant),
     *     object = { 'a': array, 'b': array, 'c': array };
     *
     * _.map(['a[2]', 'c[0]'], _.methodOf(object));
     * // => [2, 0]
     *
     * _.map([['a', '2'], ['c', '0']], _.methodOf(object));
     * // => [2, 0]
     */
    var methodOf = baseRest(function(object, args) {
      return function(path) {
        return baseInvoke(object, path, args);
      };
    });

    /**
     * Adds all own enumerable string keyed function properties of a source
     * object to the destination object. If `object` is a function, then methods
     * are added to its prototype as well.
     *
     * **Note:** Use `_.runInContext` to create a pristine `lodash` function to
     * avoid conflicts caused by modifying the original.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {Function|Object} [object=lodash] The destination object.
     * @param {Object} source The object of functions to add.
     * @param {Object} [options={}] The options object.
     * @param {boolean} [options.chain=true] Specify whether mixins are chainable.
     * @returns {Function|Object} Returns `object`.
     * @example
     *
     * function vowels(string) {
     *   return _.filter(string, function(v) {
     *     return /[aeiou]/i.test(v);
     *   });
     * }
     *
     * _.mixin({ 'vowels': vowels });
     * _.vowels('fred');
     * // => ['e']
     *
     * _('fred').vowels().value();
     * // => ['e']
     *
     * _.mixin({ 'vowels': vowels }, { 'chain': false });
     * _('fred').vowels();
     * // => ['e']
     */
    function mixin(object, source, options) {
      var props = keys(source),
          methodNames = baseFunctions(source, props);

      if (options == null &&
          !(isObject(source) && (methodNames.length || !props.length))) {
        options = source;
        source = object;
        object = this;
        methodNames = baseFunctions(source, keys(source));
      }
      var chain = !(isObject(options) && 'chain' in options) || !!options.chain,
          isFunc = isFunction(object);

      arrayEach(methodNames, function(methodName) {
        var func = source[methodName];
        object[methodName] = func;
        if (isFunc) {
          object.prototype[methodName] = function() {
            var chainAll = this.__chain__;
            if (chain || chainAll) {
              var result = object(this.__wrapped__),
                  actions = result.__actions__ = copyArray(this.__actions__);

              actions.push({ 'func': func, 'args': arguments, 'thisArg': object });
              result.__chain__ = chainAll;
              return result;
            }
            return func.apply(object, arrayPush([this.value()], arguments));
          };
        }
      });

      return object;
    }

    /**
     * Reverts the `_` variable to its previous value and returns a reference to
     * the `lodash` function.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @returns {Function} Returns the `lodash` function.
     * @example
     *
     * var lodash = _.noConflict();
     */
    function noConflict() {
      if (root._ === this) {
        root._ = oldDash;
      }
      return this;
    }

    /**
     * This method returns `undefined`.
     *
     * @static
     * @memberOf _
     * @since 2.3.0
     * @category Util
     * @example
     *
     * _.times(2, _.noop);
     * // => [undefined, undefined]
     */
    function noop() {
      // No operation performed.
    }

    /**
     * Creates a function that gets the argument at index `n`. If `n` is negative,
     * the nth argument from the end is returned.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {number} [n=0] The index of the argument to return.
     * @returns {Function} Returns the new pass-thru function.
     * @example
     *
     * var func = _.nthArg(1);
     * func('a', 'b', 'c', 'd');
     * // => 'b'
     *
     * var func = _.nthArg(-2);
     * func('a', 'b', 'c', 'd');
     * // => 'c'
     */
    function nthArg(n) {
      n = toInteger(n);
      return baseRest(function(args) {
        return baseNth(args, n);
      });
    }

    /**
     * Creates a function that invokes `iteratees` with the arguments it receives
     * and returns their results.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {...(Function|Function[])} [iteratees=[_.identity]]
     *  The iteratees to invoke.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var func = _.over([Math.max, Math.min]);
     *
     * func(1, 2, 3, 4);
     * // => [4, 1]
     */
    var over = createOver(arrayMap);

    /**
     * Creates a function that checks if **all** of the `predicates` return
     * truthy when invoked with the arguments it receives.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {...(Function|Function[])} [predicates=[_.identity]]
     *  The predicates to check.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var func = _.overEvery([Boolean, isFinite]);
     *
     * func('1');
     * // => true
     *
     * func(null);
     * // => false
     *
     * func(NaN);
     * // => false
     */
    var overEvery = createOver(arrayEvery);

    /**
     * Creates a function that checks if **any** of the `predicates` return
     * truthy when invoked with the arguments it receives.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {...(Function|Function[])} [predicates=[_.identity]]
     *  The predicates to check.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var func = _.overSome([Boolean, isFinite]);
     *
     * func('1');
     * // => true
     *
     * func(null);
     * // => true
     *
     * func(NaN);
     * // => false
     */
    var overSome = createOver(arraySome);

    /**
     * Creates a function that returns the value at `path` of a given object.
     *
     * @static
     * @memberOf _
     * @since 2.4.0
     * @category Util
     * @param {Array|string} path The path of the property to get.
     * @returns {Function} Returns the new accessor function.
     * @example
     *
     * var objects = [
     *   { 'a': { 'b': 2 } },
     *   { 'a': { 'b': 1 } }
     * ];
     *
     * _.map(objects, _.property('a.b'));
     * // => [2, 1]
     *
     * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
     * // => [1, 2]
     */
    function property(path) {
      return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
    }

    /**
     * The opposite of `_.property`; this method creates a function that returns
     * the value at a given path of `object`.
     *
     * @static
     * @memberOf _
     * @since 3.0.0
     * @category Util
     * @param {Object} object The object to query.
     * @returns {Function} Returns the new accessor function.
     * @example
     *
     * var array = [0, 1, 2],
     *     object = { 'a': array, 'b': array, 'c': array };
     *
     * _.map(['a[2]', 'c[0]'], _.propertyOf(object));
     * // => [2, 0]
     *
     * _.map([['a', '2'], ['c', '0']], _.propertyOf(object));
     * // => [2, 0]
     */
    function propertyOf(object) {
      return function(path) {
        return object == null ? undefined : baseGet(object, path);
      };
    }

    /**
     * Creates an array of numbers (positive and/or negative) progressing from
     * `start` up to, but not including, `end`. A step of `-1` is used if a negative
     * `start` is specified without an `end` or `step`. If `end` is not specified,
     * it's set to `start` with `start` then set to `0`.
     *
     * **Note:** JavaScript follows the IEEE-754 standard for resolving
     * floating-point values which can produce unexpected results.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @param {number} [step=1] The value to increment or decrement by.
     * @returns {Array} Returns the range of numbers.
     * @see _.inRange, _.rangeRight
     * @example
     *
     * _.range(4);
     * // => [0, 1, 2, 3]
     *
     * _.range(-4);
     * // => [0, -1, -2, -3]
     *
     * _.range(1, 5);
     * // => [1, 2, 3, 4]
     *
     * _.range(0, 20, 5);
     * // => [0, 5, 10, 15]
     *
     * _.range(0, -4, -1);
     * // => [0, -1, -2, -3]
     *
     * _.range(1, 4, 0);
     * // => [1, 1, 1]
     *
     * _.range(0);
     * // => []
     */
    var range = createRange();

    /**
     * This method is like `_.range` except that it populates values in
     * descending order.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @param {number} [step=1] The value to increment or decrement by.
     * @returns {Array} Returns the range of numbers.
     * @see _.inRange, _.range
     * @example
     *
     * _.rangeRight(4);
     * // => [3, 2, 1, 0]
     *
     * _.rangeRight(-4);
     * // => [-3, -2, -1, 0]
     *
     * _.rangeRight(1, 5);
     * // => [4, 3, 2, 1]
     *
     * _.rangeRight(0, 20, 5);
     * // => [15, 10, 5, 0]
     *
     * _.rangeRight(0, -4, -1);
     * // => [-3, -2, -1, 0]
     *
     * _.rangeRight(1, 4, 0);
     * // => [1, 1, 1]
     *
     * _.rangeRight(0);
     * // => []
     */
    var rangeRight = createRange(true);

    /**
     * This method returns a new empty array.
     *
     * @static
     * @memberOf _
     * @since 4.13.0
     * @category Util
     * @returns {Array} Returns the new empty array.
     * @example
     *
     * var arrays = _.times(2, _.stubArray);
     *
     * console.log(arrays);
     * // => [[], []]
     *
     * console.log(arrays[0] === arrays[1]);
     * // => false
     */
    function stubArray() {
      return [];
    }

    /**
     * This method returns `false`.
     *
     * @static
     * @memberOf _
     * @since 4.13.0
     * @category Util
     * @returns {boolean} Returns `false`.
     * @example
     *
     * _.times(2, _.stubFalse);
     * // => [false, false]
     */
    function stubFalse() {
      return false;
    }

    /**
     * This method returns a new empty object.
     *
     * @static
     * @memberOf _
     * @since 4.13.0
     * @category Util
     * @returns {Object} Returns the new empty object.
     * @example
     *
     * var objects = _.times(2, _.stubObject);
     *
     * console.log(objects);
     * // => [{}, {}]
     *
     * console.log(objects[0] === objects[1]);
     * // => false
     */
    function stubObject() {
      return {};
    }

    /**
     * This method returns an empty string.
     *
     * @static
     * @memberOf _
     * @since 4.13.0
     * @category Util
     * @returns {string} Returns the empty string.
     * @example
     *
     * _.times(2, _.stubString);
     * // => ['', '']
     */
    function stubString() {
      return '';
    }

    /**
     * This method returns `true`.
     *
     * @static
     * @memberOf _
     * @since 4.13.0
     * @category Util
     * @returns {boolean} Returns `true`.
     * @example
     *
     * _.times(2, _.stubTrue);
     * // => [true, true]
     */
    function stubTrue() {
      return true;
    }

    /**
     * Invokes the iteratee `n` times, returning an array of the results of
     * each invocation. The iteratee is invoked with one argument; (index).
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {number} n The number of times to invoke `iteratee`.
     * @param {Function} [iteratee=_.identity] The function invoked per iteration.
     * @returns {Array} Returns the array of results.
     * @example
     *
     * _.times(3, String);
     * // => ['0', '1', '2']
     *
     *  _.times(4, _.constant(0));
     * // => [0, 0, 0, 0]
     */
    function times(n, iteratee) {
      n = toInteger(n);
      if (n < 1 || n > MAX_SAFE_INTEGER) {
        return [];
      }
      var index = MAX_ARRAY_LENGTH,
          length = nativeMin(n, MAX_ARRAY_LENGTH);

      iteratee = getIteratee(iteratee);
      n -= MAX_ARRAY_LENGTH;

      var result = baseTimes(length, iteratee);
      while (++index < n) {
        iteratee(index);
      }
      return result;
    }

    /**
     * Converts `value` to a property path array.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Util
     * @param {*} value The value to convert.
     * @returns {Array} Returns the new property path array.
     * @example
     *
     * _.toPath('a.b.c');
     * // => ['a', 'b', 'c']
     *
     * _.toPath('a[0].b.c');
     * // => ['a', '0', 'b', 'c']
     */
    function toPath(value) {
      if (isArray(value)) {
        return arrayMap(value, toKey);
      }
      return isSymbol(value) ? [value] : copyArray(stringToPath(toString(value)));
    }

    /**
     * Generates a unique ID. If `prefix` is given, the ID is appended to it.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Util
     * @param {string} [prefix=''] The value to prefix the ID with.
     * @returns {string} Returns the unique ID.
     * @example
     *
     * _.uniqueId('contact_');
     * // => 'contact_104'
     *
     * _.uniqueId();
     * // => '105'
     */
    function uniqueId(prefix) {
      var id = ++idCounter;
      return toString(prefix) + id;
    }

    /*------------------------------------------------------------------------*/

    /**
     * Adds two numbers.
     *
     * @static
     * @memberOf _
     * @since 3.4.0
     * @category Math
     * @param {number} augend The first number in an addition.
     * @param {number} addend The second number in an addition.
     * @returns {number} Returns the total.
     * @example
     *
     * _.add(6, 4);
     * // => 10
     */
    var add = createMathOperation(function(augend, addend) {
      return augend + addend;
    }, 0);

    /**
     * Computes `number` rounded up to `precision`.
     *
     * @static
     * @memberOf _
     * @since 3.10.0
     * @category Math
     * @param {number} number The number to round up.
     * @param {number} [precision=0] The precision to round up to.
     * @returns {number} Returns the rounded up number.
     * @example
     *
     * _.ceil(4.006);
     * // => 5
     *
     * _.ceil(6.004, 2);
     * // => 6.01
     *
     * _.ceil(6040, -2);
     * // => 6100
     */
    var ceil = createRound('ceil');

    /**
     * Divide two numbers.
     *
     * @static
     * @memberOf _
     * @since 4.7.0
     * @category Math
     * @param {number} dividend The first number in a division.
     * @param {number} divisor The second number in a division.
     * @returns {number} Returns the quotient.
     * @example
     *
     * _.divide(6, 4);
     * // => 1.5
     */
    var divide = createMathOperation(function(dividend, divisor) {
      return dividend / divisor;
    }, 1);

    /**
     * Computes `number` rounded down to `precision`.
     *
     * @static
     * @memberOf _
     * @since 3.10.0
     * @category Math
     * @param {number} number The number to round down.
     * @param {number} [precision=0] The precision to round down to.
     * @returns {number} Returns the rounded down number.
     * @example
     *
     * _.floor(4.006);
     * // => 4
     *
     * _.floor(0.046, 2);
     * // => 0.04
     *
     * _.floor(4060, -2);
     * // => 4000
     */
    var floor = createRound('floor');

    /**
     * Computes the maximum value of `array`. If `array` is empty or falsey,
     * `undefined` is returned.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Math
     * @param {Array} array The array to iterate over.
     * @returns {*} Returns the maximum value.
     * @example
     *
     * _.max([4, 2, 8, 6]);
     * // => 8
     *
     * _.max([]);
     * // => undefined
     */
    function max(array) {
      return (array && array.length)
        ? baseExtremum(array, identity, baseGt)
        : undefined;
    }

    /**
     * This method is like `_.max` except that it accepts `iteratee` which is
     * invoked for each element in `array` to generate the criterion by which
     * the value is ranked. The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {*} Returns the maximum value.
     * @example
     *
     * var objects = [{ 'n': 1 }, { 'n': 2 }];
     *
     * _.maxBy(objects, function(o) { return o.n; });
     * // => { 'n': 2 }
     *
     * // The `_.property` iteratee shorthand.
     * _.maxBy(objects, 'n');
     * // => { 'n': 2 }
     */
    function maxBy(array, iteratee) {
      return (array && array.length)
        ? baseExtremum(array, getIteratee(iteratee, 2), baseGt)
        : undefined;
    }

    /**
     * Computes the mean of the values in `array`.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @returns {number} Returns the mean.
     * @example
     *
     * _.mean([4, 2, 8, 6]);
     * // => 5
     */
    function mean(array) {
      return baseMean(array, identity);
    }

    /**
     * This method is like `_.mean` except that it accepts `iteratee` which is
     * invoked for each element in `array` to generate the value to be averaged.
     * The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.7.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {number} Returns the mean.
     * @example
     *
     * var objects = [{ 'n': 4 }, { 'n': 2 }, { 'n': 8 }, { 'n': 6 }];
     *
     * _.meanBy(objects, function(o) { return o.n; });
     * // => 5
     *
     * // The `_.property` iteratee shorthand.
     * _.meanBy(objects, 'n');
     * // => 5
     */
    function meanBy(array, iteratee) {
      return baseMean(array, getIteratee(iteratee, 2));
    }

    /**
     * Computes the minimum value of `array`. If `array` is empty or falsey,
     * `undefined` is returned.
     *
     * @static
     * @since 0.1.0
     * @memberOf _
     * @category Math
     * @param {Array} array The array to iterate over.
     * @returns {*} Returns the minimum value.
     * @example
     *
     * _.min([4, 2, 8, 6]);
     * // => 2
     *
     * _.min([]);
     * // => undefined
     */
    function min(array) {
      return (array && array.length)
        ? baseExtremum(array, identity, baseLt)
        : undefined;
    }

    /**
     * This method is like `_.min` except that it accepts `iteratee` which is
     * invoked for each element in `array` to generate the criterion by which
     * the value is ranked. The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {*} Returns the minimum value.
     * @example
     *
     * var objects = [{ 'n': 1 }, { 'n': 2 }];
     *
     * _.minBy(objects, function(o) { return o.n; });
     * // => { 'n': 1 }
     *
     * // The `_.property` iteratee shorthand.
     * _.minBy(objects, 'n');
     * // => { 'n': 1 }
     */
    function minBy(array, iteratee) {
      return (array && array.length)
        ? baseExtremum(array, getIteratee(iteratee, 2), baseLt)
        : undefined;
    }

    /**
     * Multiply two numbers.
     *
     * @static
     * @memberOf _
     * @since 4.7.0
     * @category Math
     * @param {number} multiplier The first number in a multiplication.
     * @param {number} multiplicand The second number in a multiplication.
     * @returns {number} Returns the product.
     * @example
     *
     * _.multiply(6, 4);
     * // => 24
     */
    var multiply = createMathOperation(function(multiplier, multiplicand) {
      return multiplier * multiplicand;
    }, 1);

    /**
     * Computes `number` rounded to `precision`.
     *
     * @static
     * @memberOf _
     * @since 3.10.0
     * @category Math
     * @param {number} number The number to round.
     * @param {number} [precision=0] The precision to round to.
     * @returns {number} Returns the rounded number.
     * @example
     *
     * _.round(4.006);
     * // => 4
     *
     * _.round(4.006, 2);
     * // => 4.01
     *
     * _.round(4060, -2);
     * // => 4100
     */
    var round = createRound('round');

    /**
     * Subtract two numbers.
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Math
     * @param {number} minuend The first number in a subtraction.
     * @param {number} subtrahend The second number in a subtraction.
     * @returns {number} Returns the difference.
     * @example
     *
     * _.subtract(6, 4);
     * // => 2
     */
    var subtract = createMathOperation(function(minuend, subtrahend) {
      return minuend - subtrahend;
    }, 0);

    /**
     * Computes the sum of the values in `array`.
     *
     * @static
     * @memberOf _
     * @since 3.4.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @returns {number} Returns the sum.
     * @example
     *
     * _.sum([4, 2, 8, 6]);
     * // => 20
     */
    function sum(array) {
      return (array && array.length)
        ? baseSum(array, identity)
        : 0;
    }

    /**
     * This method is like `_.sum` except that it accepts `iteratee` which is
     * invoked for each element in `array` to generate the value to be summed.
     * The iteratee is invoked with one argument: (value).
     *
     * @static
     * @memberOf _
     * @since 4.0.0
     * @category Math
     * @param {Array} array The array to iterate over.
     * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
     * @returns {number} Returns the sum.
     * @example
     *
     * var objects = [{ 'n': 4 }, { 'n': 2 }, { 'n': 8 }, { 'n': 6 }];
     *
     * _.sumBy(objects, function(o) { return o.n; });
     * // => 20
     *
     * // The `_.property` iteratee shorthand.
     * _.sumBy(objects, 'n');
     * // => 20
     */
    function sumBy(array, iteratee) {
      return (array && array.length)
        ? baseSum(array, getIteratee(iteratee, 2))
        : 0;
    }

    /*------------------------------------------------------------------------*/

    // Add methods that return wrapped values in chain sequences.
    lodash.after = after;
    lodash.ary = ary;
    lodash.assign = assign;
    lodash.assignIn = assignIn;
    lodash.assignInWith = assignInWith;
    lodash.assignWith = assignWith;
    lodash.at = at;
    lodash.before = before;
    lodash.bind = bind;
    lodash.bindAll = bindAll;
    lodash.bindKey = bindKey;
    lodash.castArray = castArray;
    lodash.chain = chain;
    lodash.chunk = chunk;
    lodash.compact = compact;
    lodash.concat = concat;
    lodash.cond = cond;
    lodash.conforms = conforms;
    lodash.constant = constant;
    lodash.countBy = countBy;
    lodash.create = create;
    lodash.curry = curry;
    lodash.curryRight = curryRight;
    lodash.debounce = debounce;
    lodash.defaults = defaults;
    lodash.defaultsDeep = defaultsDeep;
    lodash.defer = defer;
    lodash.delay = delay;
    lodash.difference = difference;
    lodash.differenceBy = differenceBy;
    lodash.differenceWith = differenceWith;
    lodash.drop = drop;
    lodash.dropRight = dropRight;
    lodash.dropRightWhile = dropRightWhile;
    lodash.dropWhile = dropWhile;
    lodash.fill = fill;
    lodash.filter = filter;
    lodash.flatMap = flatMap;
    lodash.flatMapDeep = flatMapDeep;
    lodash.flatMapDepth = flatMapDepth;
    lodash.flatten = flatten;
    lodash.flattenDeep = flattenDeep;
    lodash.flattenDepth = flattenDepth;
    lodash.flip = flip;
    lodash.flow = flow;
    lodash.flowRight = flowRight;
    lodash.fromPairs = fromPairs;
    lodash.functions = functions;
    lodash.functionsIn = functionsIn;
    lodash.groupBy = groupBy;
    lodash.initial = initial;
    lodash.intersection = intersection;
    lodash.intersectionBy = intersectionBy;
    lodash.intersectionWith = intersectionWith;
    lodash.invert = invert;
    lodash.invertBy = invertBy;
    lodash.invokeMap = invokeMap;
    lodash.iteratee = iteratee;
    lodash.keyBy = keyBy;
    lodash.keys = keys;
    lodash.keysIn = keysIn;
    lodash.map = map;
    lodash.mapKeys = mapKeys;
    lodash.mapValues = mapValues;
    lodash.matches = matches;
    lodash.matchesProperty = matchesProperty;
    lodash.memoize = memoize;
    lodash.merge = merge;
    lodash.mergeWith = mergeWith;
    lodash.method = method;
    lodash.methodOf = methodOf;
    lodash.mixin = mixin;
    lodash.negate = negate;
    lodash.nthArg = nthArg;
    lodash.omit = omit;
    lodash.omitBy = omitBy;
    lodash.once = once;
    lodash.orderBy = orderBy;
    lodash.over = over;
    lodash.overArgs = overArgs;
    lodash.overEvery = overEvery;
    lodash.overSome = overSome;
    lodash.partial = partial;
    lodash.partialRight = partialRight;
    lodash.partition = partition;
    lodash.pick = pick;
    lodash.pickBy = pickBy;
    lodash.property = property;
    lodash.propertyOf = propertyOf;
    lodash.pull = pull;
    lodash.pullAll = pullAll;
    lodash.pullAllBy = pullAllBy;
    lodash.pullAllWith = pullAllWith;
    lodash.pullAt = pullAt;
    lodash.range = range;
    lodash.rangeRight = rangeRight;
    lodash.rearg = rearg;
    lodash.reject = reject;
    lodash.remove = remove;
    lodash.rest = rest;
    lodash.reverse = reverse;
    lodash.sampleSize = sampleSize;
    lodash.set = set;
    lodash.setWith = setWith;
    lodash.shuffle = shuffle;
    lodash.slice = slice;
    lodash.sortBy = sortBy;
    lodash.sortedUniq = sortedUniq;
    lodash.sortedUniqBy = sortedUniqBy;
    lodash.split = split;
    lodash.spread = spread;
    lodash.tail = tail;
    lodash.take = take;
    lodash.takeRight = takeRight;
    lodash.takeRightWhile = takeRightWhile;
    lodash.takeWhile = takeWhile;
    lodash.tap = tap;
    lodash.throttle = throttle;
    lodash.thru = thru;
    lodash.toArray = toArray;
    lodash.toPairs = toPairs;
    lodash.toPairsIn = toPairsIn;
    lodash.toPath = toPath;
    lodash.toPlainObject = toPlainObject;
    lodash.transform = transform;
    lodash.unary = unary;
    lodash.union = union;
    lodash.unionBy = unionBy;
    lodash.unionWith = unionWith;
    lodash.uniq = uniq;
    lodash.uniqBy = uniqBy;
    lodash.uniqWith = uniqWith;
    lodash.unset = unset;
    lodash.unzip = unzip;
    lodash.unzipWith = unzipWith;
    lodash.update = update;
    lodash.updateWith = updateWith;
    lodash.values = values;
    lodash.valuesIn = valuesIn;
    lodash.without = without;
    lodash.words = words;
    lodash.wrap = wrap;
    lodash.xor = xor;
    lodash.xorBy = xorBy;
    lodash.xorWith = xorWith;
    lodash.zip = zip;
    lodash.zipObject = zipObject;
    lodash.zipObjectDeep = zipObjectDeep;
    lodash.zipWith = zipWith;

    // Add aliases.
    lodash.entries = toPairs;
    lodash.entriesIn = toPairsIn;
    lodash.extend = assignIn;
    lodash.extendWith = assignInWith;

    // Add methods to `lodash.prototype`.
    mixin(lodash, lodash);

    /*------------------------------------------------------------------------*/

    // Add methods that return unwrapped values in chain sequences.
    lodash.add = add;
    lodash.attempt = attempt;
    lodash.camelCase = camelCase;
    lodash.capitalize = capitalize;
    lodash.ceil = ceil;
    lodash.clamp = clamp;
    lodash.clone = clone;
    lodash.cloneDeep = cloneDeep;
    lodash.cloneDeepWith = cloneDeepWith;
    lodash.cloneWith = cloneWith;
    lodash.conformsTo = conformsTo;
    lodash.deburr = deburr;
    lodash.defaultTo = defaultTo;
    lodash.divide = divide;
    lodash.endsWith = endsWith;
    lodash.eq = eq;
    lodash.escape = escape;
    lodash.escapeRegExp = escapeRegExp;
    lodash.every = every;
    lodash.find = find;
    lodash.findIndex = findIndex;
    lodash.findKey = findKey;
    lodash.findLast = findLast;
    lodash.findLastIndex = findLastIndex;
    lodash.findLastKey = findLastKey;
    lodash.floor = floor;
    lodash.forEach = forEach;
    lodash.forEachRight = forEachRight;
    lodash.forIn = forIn;
    lodash.forInRight = forInRight;
    lodash.forOwn = forOwn;
    lodash.forOwnRight = forOwnRight;
    lodash.get = get;
    lodash.gt = gt;
    lodash.gte = gte;
    lodash.has = has;
    lodash.hasIn = hasIn;
    lodash.head = head;
    lodash.identity = identity;
    lodash.includes = includes;
    lodash.indexOf = indexOf;
    lodash.inRange = inRange;
    lodash.invoke = invoke;
    lodash.isArguments = isArguments;
    lodash.isArray = isArray;
    lodash.isArrayBuffer = isArrayBuffer;
    lodash.isArrayLike = isArrayLike;
    lodash.isArrayLikeObject = isArrayLikeObject;
    lodash.isBoolean = isBoolean;
    lodash.isBuffer = isBuffer;
    lodash.isDate = isDate;
    lodash.isElement = isElement;
    lodash.isEmpty = isEmpty;
    lodash.isEqual = isEqual;
    lodash.isEqualWith = isEqualWith;
    lodash.isError = isError;
    lodash.isFinite = isFinite;
    lodash.isFunction = isFunction;
    lodash.isInteger = isInteger;
    lodash.isLength = isLength;
    lodash.isMap = isMap;
    lodash.isMatch = isMatch;
    lodash.isMatchWith = isMatchWith;
    lodash.isNaN = isNaN;
    lodash.isNative = isNative;
    lodash.isNil = isNil;
    lodash.isNull = isNull;
    lodash.isNumber = isNumber;
    lodash.isObject = isObject;
    lodash.isObjectLike = isObjectLike;
    lodash.isPlainObject = isPlainObject;
    lodash.isRegExp = isRegExp;
    lodash.isSafeInteger = isSafeInteger;
    lodash.isSet = isSet;
    lodash.isString = isString;
    lodash.isSymbol = isSymbol;
    lodash.isTypedArray = isTypedArray;
    lodash.isUndefined = isUndefined;
    lodash.isWeakMap = isWeakMap;
    lodash.isWeakSet = isWeakSet;
    lodash.join = join;
    lodash.kebabCase = kebabCase;
    lodash.last = last;
    lodash.lastIndexOf = lastIndexOf;
    lodash.lowerCase = lowerCase;
    lodash.lowerFirst = lowerFirst;
    lodash.lt = lt;
    lodash.lte = lte;
    lodash.max = max;
    lodash.maxBy = maxBy;
    lodash.mean = mean;
    lodash.meanBy = meanBy;
    lodash.min = min;
    lodash.minBy = minBy;
    lodash.stubArray = stubArray;
    lodash.stubFalse = stubFalse;
    lodash.stubObject = stubObject;
    lodash.stubString = stubString;
    lodash.stubTrue = stubTrue;
    lodash.multiply = multiply;
    lodash.nth = nth;
    lodash.noConflict = noConflict;
    lodash.noop = noop;
    lodash.now = now;
    lodash.pad = pad;
    lodash.padEnd = padEnd;
    lodash.padStart = padStart;
    lodash.parseInt = parseInt;
    lodash.random = random;
    lodash.reduce = reduce;
    lodash.reduceRight = reduceRight;
    lodash.repeat = repeat;
    lodash.replace = replace;
    lodash.result = result;
    lodash.round = round;
    lodash.runInContext = runInContext;
    lodash.sample = sample;
    lodash.size = size;
    lodash.snakeCase = snakeCase;
    lodash.some = some;
    lodash.sortedIndex = sortedIndex;
    lodash.sortedIndexBy = sortedIndexBy;
    lodash.sortedIndexOf = sortedIndexOf;
    lodash.sortedLastIndex = sortedLastIndex;
    lodash.sortedLastIndexBy = sortedLastIndexBy;
    lodash.sortedLastIndexOf = sortedLastIndexOf;
    lodash.startCase = startCase;
    lodash.startsWith = startsWith;
    lodash.subtract = subtract;
    lodash.sum = sum;
    lodash.sumBy = sumBy;
    lodash.template = template;
    lodash.times = times;
    lodash.toFinite = toFinite;
    lodash.toInteger = toInteger;
    lodash.toLength = toLength;
    lodash.toLower = toLower;
    lodash.toNumber = toNumber;
    lodash.toSafeInteger = toSafeInteger;
    lodash.toString = toString;
    lodash.toUpper = toUpper;
    lodash.trim = trim;
    lodash.trimEnd = trimEnd;
    lodash.trimStart = trimStart;
    lodash.truncate = truncate;
    lodash.unescape = unescape;
    lodash.uniqueId = uniqueId;
    lodash.upperCase = upperCase;
    lodash.upperFirst = upperFirst;

    // Add aliases.
    lodash.each = forEach;
    lodash.eachRight = forEachRight;
    lodash.first = head;

    mixin(lodash, (function() {
      var source = {};
      baseForOwn(lodash, function(func, methodName) {
        if (!hasOwnProperty.call(lodash.prototype, methodName)) {
          source[methodName] = func;
        }
      });
      return source;
    }()), { 'chain': false });

    /*------------------------------------------------------------------------*/

    /**
     * The semantic version number.
     *
     * @static
     * @memberOf _
     * @type {string}
     */
    lodash.VERSION = VERSION;

    // Assign default placeholders.
    arrayEach(['bind', 'bindKey', 'curry', 'curryRight', 'partial', 'partialRight'], function(methodName) {
      lodash[methodName].placeholder = lodash;
    });

    // Add `LazyWrapper` methods for `_.drop` and `_.take` variants.
    arrayEach(['drop', 'take'], function(methodName, index) {
      LazyWrapper.prototype[methodName] = function(n) {
        n = n === undefined ? 1 : nativeMax(toInteger(n), 0);

        var result = (this.__filtered__ && !index)
          ? new LazyWrapper(this)
          : this.clone();

        if (result.__filtered__) {
          result.__takeCount__ = nativeMin(n, result.__takeCount__);
        } else {
          result.__views__.push({
            'size': nativeMin(n, MAX_ARRAY_LENGTH),
            'type': methodName + (result.__dir__ < 0 ? 'Right' : '')
          });
        }
        return result;
      };

      LazyWrapper.prototype[methodName + 'Right'] = function(n) {
        return this.reverse()[methodName](n).reverse();
      };
    });

    // Add `LazyWrapper` methods that accept an `iteratee` value.
    arrayEach(['filter', 'map', 'takeWhile'], function(methodName, index) {
      var type = index + 1,
          isFilter = type == LAZY_FILTER_FLAG || type == LAZY_WHILE_FLAG;

      LazyWrapper.prototype[methodName] = function(iteratee) {
        var result = this.clone();
        result.__iteratees__.push({
          'iteratee': getIteratee(iteratee, 3),
          'type': type
        });
        result.__filtered__ = result.__filtered__ || isFilter;
        return result;
      };
    });

    // Add `LazyWrapper` methods for `_.head` and `_.last`.
    arrayEach(['head', 'last'], function(methodName, index) {
      var takeName = 'take' + (index ? 'Right' : '');

      LazyWrapper.prototype[methodName] = function() {
        return this[takeName](1).value()[0];
      };
    });

    // Add `LazyWrapper` methods for `_.initial` and `_.tail`.
    arrayEach(['initial', 'tail'], function(methodName, index) {
      var dropName = 'drop' + (index ? '' : 'Right');

      LazyWrapper.prototype[methodName] = function() {
        return this.__filtered__ ? new LazyWrapper(this) : this[dropName](1);
      };
    });

    LazyWrapper.prototype.compact = function() {
      return this.filter(identity);
    };

    LazyWrapper.prototype.find = function(predicate) {
      return this.filter(predicate).head();
    };

    LazyWrapper.prototype.findLast = function(predicate) {
      return this.reverse().find(predicate);
    };

    LazyWrapper.prototype.invokeMap = baseRest(function(path, args) {
      if (typeof path == 'function') {
        return new LazyWrapper(this);
      }
      return this.map(function(value) {
        return baseInvoke(value, path, args);
      });
    });

    LazyWrapper.prototype.reject = function(predicate) {
      return this.filter(negate(getIteratee(predicate)));
    };

    LazyWrapper.prototype.slice = function(start, end) {
      start = toInteger(start);

      var result = this;
      if (result.__filtered__ && (start > 0 || end < 0)) {
        return new LazyWrapper(result);
      }
      if (start < 0) {
        result = result.takeRight(-start);
      } else if (start) {
        result = result.drop(start);
      }
      if (end !== undefined) {
        end = toInteger(end);
        result = end < 0 ? result.dropRight(-end) : result.take(end - start);
      }
      return result;
    };

    LazyWrapper.prototype.takeRightWhile = function(predicate) {
      return this.reverse().takeWhile(predicate).reverse();
    };

    LazyWrapper.prototype.toArray = function() {
      return this.take(MAX_ARRAY_LENGTH);
    };

    // Add `LazyWrapper` methods to `lodash.prototype`.
    baseForOwn(LazyWrapper.prototype, function(func, methodName) {
      var checkIteratee = /^(?:filter|find|map|reject)|While$/.test(methodName),
          isTaker = /^(?:head|last)$/.test(methodName),
          lodashFunc = lodash[isTaker ? ('take' + (methodName == 'last' ? 'Right' : '')) : methodName],
          retUnwrapped = isTaker || /^find/.test(methodName);

      if (!lodashFunc) {
        return;
      }
      lodash.prototype[methodName] = function() {
        var value = this.__wrapped__,
            args = isTaker ? [1] : arguments,
            isLazy = value instanceof LazyWrapper,
            iteratee = args[0],
            useLazy = isLazy || isArray(value);

        var interceptor = function(value) {
          var result = lodashFunc.apply(lodash, arrayPush([value], args));
          return (isTaker && chainAll) ? result[0] : result;
        };

        if (useLazy && checkIteratee && typeof iteratee == 'function' && iteratee.length != 1) {
          // Avoid lazy use if the iteratee has a "length" value other than `1`.
          isLazy = useLazy = false;
        }
        var chainAll = this.__chain__,
            isHybrid = !!this.__actions__.length,
            isUnwrapped = retUnwrapped && !chainAll,
            onlyLazy = isLazy && !isHybrid;

        if (!retUnwrapped && useLazy) {
          value = onlyLazy ? value : new LazyWrapper(this);
          var result = func.apply(value, args);
          result.__actions__.push({ 'func': thru, 'args': [interceptor], 'thisArg': undefined });
          return new LodashWrapper(result, chainAll);
        }
        if (isUnwrapped && onlyLazy) {
          return func.apply(this, args);
        }
        result = this.thru(interceptor);
        return isUnwrapped ? (isTaker ? result.value()[0] : result.value()) : result;
      };
    });

    // Add `Array` methods to `lodash.prototype`.
    arrayEach(['pop', 'push', 'shift', 'sort', 'splice', 'unshift'], function(methodName) {
      var func = arrayProto[methodName],
          chainName = /^(?:push|sort|unshift)$/.test(methodName) ? 'tap' : 'thru',
          retUnwrapped = /^(?:pop|shift)$/.test(methodName);

      lodash.prototype[methodName] = function() {
        var args = arguments;
        if (retUnwrapped && !this.__chain__) {
          var value = this.value();
          return func.apply(isArray(value) ? value : [], args);
        }
        return this[chainName](function(value) {
          return func.apply(isArray(value) ? value : [], args);
        });
      };
    });

    // Map minified method names to their real names.
    baseForOwn(LazyWrapper.prototype, function(func, methodName) {
      var lodashFunc = lodash[methodName];
      if (lodashFunc) {
        var key = (lodashFunc.name + ''),
            names = realNames[key] || (realNames[key] = []);

        names.push({ 'name': methodName, 'func': lodashFunc });
      }
    });

    realNames[createHybrid(undefined, WRAP_BIND_KEY_FLAG).name] = [{
      'name': 'wrapper',
      'func': undefined
    }];

    // Add methods to `LazyWrapper`.
    LazyWrapper.prototype.clone = lazyClone;
    LazyWrapper.prototype.reverse = lazyReverse;
    LazyWrapper.prototype.value = lazyValue;

    // Add chain sequence methods to the `lodash` wrapper.
    lodash.prototype.at = wrapperAt;
    lodash.prototype.chain = wrapperChain;
    lodash.prototype.commit = wrapperCommit;
    lodash.prototype.next = wrapperNext;
    lodash.prototype.plant = wrapperPlant;
    lodash.prototype.reverse = wrapperReverse;
    lodash.prototype.toJSON = lodash.prototype.valueOf = lodash.prototype.value = wrapperValue;

    // Add lazy aliases.
    lodash.prototype.first = lodash.prototype.head;

    if (symIterator) {
      lodash.prototype[symIterator] = wrapperToIterator;
    }
    return lodash;
  });

  /*--------------------------------------------------------------------------*/

  // Export lodash.
  var _ = runInContext();

  // Some AMD build optimizers, like r.js, check for condition patterns like:
  if (true) {
    // Expose Lodash on the global object to prevent errors when Lodash is
    // loaded by a script tag in the presence of an AMD loader.
    // See http://requirejs.org/docs/errors.html#mismatch for more details.
    // Use `_.noConflict` to remove Lodash from the global object.
    root._ = _;

    // Define as an anonymous module so, through path mapping, it can be
    // referenced as the "underscore" module.
    !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
      return _;
    }.call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  }
  // Check for `exports` after `define` in case a build optimizer adds it.
  else if (freeModule) {
    // Export for Node.js.
    (freeModule.exports = _)._ = _;
    // Export for CommonJS support.
    freeExports._ = _;
  }
  else {
    // Export to the global object.
    root._ = _;
  }
}.call(this));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(4), __webpack_require__(5)(module)))

/***/ }),
/* 4 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 6 */
/***/ (function(module, exports) {

/*
 Highcharts JS v5.0.5 (2016-11-29)

 (c) 2009-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(M,a){"object"===typeof module&&module.exports?module.exports=M.document?a(M):a:M.Highcharts=a(M)})("undefined"!==typeof window?window:this,function(M){M=function(){var a=window,E=a.document,A=a.navigator&&a.navigator.userAgent||"",F=E&&E.createElementNS&&!!E.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,H=/(edge|msie|trident)/i.test(A)&&!window.opera,p=!F,d=/Firefox/.test(A),g=d&&4>parseInt(A.split("Firefox/")[1],10);return a.Highcharts?a.Highcharts.error(16,!0):{product:"Highcharts",
version:"5.0.5",deg2rad:2*Math.PI/360,doc:E,hasBidiBug:g,hasTouch:E&&void 0!==E.documentElement.ontouchstart,isMS:H,isWebKit:/AppleWebKit/.test(A),isFirefox:d,isTouchDevice:/(Mobile|Android|Windows Phone)/.test(A),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:F,vml:p,win:a,charts:[],marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){}}}();(function(a){var E=[],A=a.charts,F=a.doc,H=a.win;a.error=function(a,d){a="Highcharts error #"+
a+": www.highcharts.com/errors/"+a;if(d)throw Error(a);H.console&&console.log(a)};a.Fx=function(a,d,g){this.options=d;this.elem=a;this.prop=g};a.Fx.prototype={dSetter:function(){var a=this.paths[0],d=this.paths[1],g=[],v=this.now,l=a.length,r;if(1===v)g=this.toD;else if(l===d.length&&1>v)for(;l--;)r=parseFloat(a[l]),g[l]=isNaN(r)?a[l]:v*parseFloat(d[l]-r)+r;else g=d;this.elem.attr("d",g,null,!0)},update:function(){var a=this.elem,d=this.prop,g=this.now,v=this.options.step;if(this[d+"Setter"])this[d+
"Setter"]();else a.attr?a.element&&a.attr(d,g,null,!0):a.style[d]=g+this.unit;v&&v.call(a,g,this)},run:function(a,d,g){var p=this,l=function(a){return l.stopped?!1:p.step(a)},r;this.startTime=+new Date;this.start=a;this.end=d;this.unit=g;this.now=this.start;this.pos=0;l.elem=this.elem;l.prop=this.prop;l()&&1===E.push(l)&&(l.timerId=setInterval(function(){for(r=0;r<E.length;r++)E[r]()||E.splice(r--,1);E.length||clearInterval(l.timerId)},13))},step:function(a){var d=+new Date,g,p=this.options;g=this.elem;
var l=p.complete,r=p.duration,f=p.curAnim,b;if(g.attr&&!g.element)g=!1;else if(a||d>=r+this.startTime){this.now=this.end;this.pos=1;this.update();a=f[this.prop]=!0;for(b in f)!0!==f[b]&&(a=!1);a&&l&&l.call(g);g=!1}else this.pos=p.easing((d-this.startTime)/r),this.now=this.start+(this.end-this.start)*this.pos,this.update(),g=!0;return g},initPath:function(a,d,g){function p(a){var c,e;for(h=a.length;h--;)c="M"===a[h]||"L"===a[h],e=/[a-zA-Z]/.test(a[h+3]),c&&e&&a.splice(h+1,0,a[h+1],a[h+2],a[h+1],a[h+
2])}function l(a,c){for(;a.length<k;){a[0]=c[k-a.length];var e=a.slice(0,t);[].splice.apply(a,[0,0].concat(e));C&&(e=a.slice(a.length-t),[].splice.apply(a,[a.length,0].concat(e)),h--)}a[0]="M"}function r(a,c){for(var b=(k-a.length)/t;0<b&&b--;)e=a.slice().splice(a.length/u-t,t*u),e[0]=c[k-t-b*t],w&&(e[t-6]=e[t-2],e[t-5]=e[t-1]),[].splice.apply(a,[a.length/u,0].concat(e)),C&&b--}d=d||"";var f,b=a.startX,n=a.endX,w=-1<d.indexOf("C"),t=w?7:3,k,e,h;d=d.split(" ");g=g.slice();var C=a.isArea,u=C?2:1,c;
w&&(p(d),p(g));if(b&&n){for(h=0;h<b.length;h++)if(b[h]===n[0]){f=h;break}else if(b[0]===n[n.length-b.length+h]){f=h;c=!0;break}void 0===f&&(d=[])}d.length&&(k=g.length+(f||0)*u*t,c?(l(d,g),r(g,d)):(l(g,d),r(d,g)));return[d,g]}};a.extend=function(a,d){var g;a||(a={});for(g in d)a[g]=d[g];return a};a.merge=function(){var p,d=arguments,g,v={},l=function(d,f){var b,n;"object"!==typeof d&&(d={});for(n in f)f.hasOwnProperty(n)&&(b=f[n],a.isObject(b,!0)&&"renderTo"!==n&&"number"!==typeof b.nodeType?d[n]=
l(d[n]||{},b):d[n]=f[n]);return d};!0===d[0]&&(v=d[1],d=Array.prototype.slice.call(d,2));g=d.length;for(p=0;p<g;p++)v=l(v,d[p]);return v};a.pInt=function(a,d){return parseInt(a,d||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(p,d){return p&&"object"===typeof p&&(!d||!a.isArray(p))};a.isNumber=function(a){return"number"===typeof a&&!isNaN(a)};a.erase=function(a,
d){for(var g=a.length;g--;)if(a[g]===d){a.splice(g,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(p,d,g){var v,l;if(a.isString(d))a.defined(g)?p.setAttribute(d,g):p&&p.getAttribute&&(l=p.getAttribute(d));else if(a.defined(d)&&a.isObject(d))for(v in d)p.setAttribute(v,d[v]);return l};a.splat=function(p){return a.isArray(p)?p:[p]};a.syncTimeout=function(a,d,g){if(d)return setTimeout(a,d,g);a.call(0,g)};a.pick=function(){var a=arguments,d,g,v=a.length;for(d=0;d<v;d++)if(g=
a[d],void 0!==g&&null!==g)return g};a.css=function(p,d){a.isMS&&!a.svg&&d&&void 0!==d.opacity&&(d.filter="alpha(opacity\x3d"+100*d.opacity+")");a.extend(p.style,d)};a.createElement=function(p,d,g,v,l){p=F.createElement(p);var r=a.css;d&&a.extend(p,d);l&&r(p,{padding:0,border:"none",margin:0});g&&r(p,g);v&&v.appendChild(p);return p};a.extendClass=function(p,d){var g=function(){};g.prototype=new p;a.extend(g.prototype,d);return g};a.pad=function(a,d,g){return Array((d||2)+1-String(a).length).join(g||
0)+a};a.relativeLength=function(a,d){return/%$/.test(a)?d*parseFloat(a)/100:parseFloat(a)};a.wrap=function(a,d,g){var p=a[d];a[d]=function(){var a=Array.prototype.slice.call(arguments),d=arguments,f=this;f.proceed=function(){p.apply(f,arguments.length?arguments:d)};a.unshift(p);a=g.apply(this,a);f.proceed=null;return a}};a.getTZOffset=function(p){var d=a.Date;return 6E4*(d.hcGetTimezoneOffset&&d.hcGetTimezoneOffset(p)||d.hcTimezoneOffset||0)};a.dateFormat=function(p,d,g){if(!a.defined(d)||isNaN(d))return a.defaultOptions.lang.invalidDate||
"";p=a.pick(p,"%Y-%m-%d %H:%M:%S");var v=a.Date,l=new v(d-a.getTZOffset(d)),r,f=l[v.hcGetHours](),b=l[v.hcGetDay](),n=l[v.hcGetDate](),w=l[v.hcGetMonth](),t=l[v.hcGetFullYear](),k=a.defaultOptions.lang,e=k.weekdays,h=k.shortWeekdays,C=a.pad,v=a.extend({a:h?h[b]:e[b].substr(0,3),A:e[b],d:C(n),e:C(n,2," "),w:b,b:k.shortMonths[w],B:k.months[w],m:C(w+1),y:t.toString().substr(2,2),Y:t,H:C(f),k:f,I:C(f%12||12),l:f%12||12,M:C(l[v.hcGetMinutes]()),p:12>f?"AM":"PM",P:12>f?"am":"pm",S:C(l.getSeconds()),L:C(Math.round(d%
1E3),3)},a.dateFormats);for(r in v)for(;-1!==p.indexOf("%"+r);)p=p.replace("%"+r,"function"===typeof v[r]?v[r](d):v[r]);return g?p.substr(0,1).toUpperCase()+p.substr(1):p};a.formatSingle=function(p,d){var g=/\.([0-9])/,v=a.defaultOptions.lang;/f$/.test(p)?(g=(g=p.match(g))?g[1]:-1,null!==d&&(d=a.numberFormat(d,g,v.decimalPoint,-1<p.indexOf(",")?v.thousandsSep:""))):d=a.dateFormat(p,d);return d};a.format=function(p,d){for(var g="{",v=!1,l,r,f,b,n=[],w;p;){g=p.indexOf(g);if(-1===g)break;l=p.slice(0,
g);if(v){l=l.split(":");r=l.shift().split(".");b=r.length;w=d;for(f=0;f<b;f++)w=w[r[f]];l.length&&(w=a.formatSingle(l.join(":"),w));n.push(w)}else n.push(l);p=p.slice(g+1);g=(v=!v)?"}":"{"}n.push(p);return n.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(p,d,g,v,l){var r,f=p;g=a.pick(g,1);r=p/g;d||(d=l?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===v&&(1===g?d=a.grep(d,function(a){return 0===a%1}):.1>=g&&(d=[1/g])));
for(v=0;v<d.length&&!(f=d[v],l&&f*g>=p||!l&&r<=(d[v]+(d[v+1]||d[v]))/2);v++);return f*g};a.stableSort=function(a,d){var g=a.length,p,l;for(l=0;l<g;l++)a[l].safeI=l;a.sort(function(a,f){p=d(a,f);return 0===p?a.safeI-f.safeI:p});for(l=0;l<g;l++)delete a[l].safeI};a.arrayMin=function(a){for(var d=a.length,g=a[0];d--;)a[d]<g&&(g=a[d]);return g};a.arrayMax=function(a){for(var d=a.length,g=a[0];d--;)a[d]>g&&(g=a[d]);return g};a.destroyObjectProperties=function(a,d){for(var g in a)a[g]&&a[g]!==d&&a[g].destroy&&
a[g].destroy(),delete a[g]};a.discardElement=function(p){var d=a.garbageBin;d||(d=a.createElement("div"));p&&d.appendChild(p);d.innerHTML=""};a.correctFloat=function(a,d){return parseFloat(a.toPrecision(d||14))};a.setAnimation=function(p,d){d.renderer.globalAnimation=a.pick(p,d.options.chart.animation,!0)};a.animObject=function(p){return a.isObject(p)?a.merge(p):{duration:p?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=
function(p,d,g,v){p=+p||0;d=+d;var l=a.defaultOptions.lang,r=(p.toString().split(".")[1]||"").length,f,b,n=Math.abs(p);-1===d?d=Math.min(r,20):a.isNumber(d)||(d=2);f=String(a.pInt(n.toFixed(d)));b=3<f.length?f.length%3:0;g=a.pick(g,l.decimalPoint);v=a.pick(v,l.thousandsSep);p=(0>p?"-":"")+(b?f.substr(0,b)+v:"");p+=f.substr(b).replace(/(\d{3})(?=\d)/g,"$1"+v);d&&(v=Math.abs(n-f+Math.pow(10,-Math.max(d,r)-1)),p+=g+v.toFixed(d).slice(2));return p};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*
a)-1)};a.getStyle=function(p,d){return"width"===d?Math.min(p.offsetWidth,p.scrollWidth)-a.getStyle(p,"padding-left")-a.getStyle(p,"padding-right"):"height"===d?Math.min(p.offsetHeight,p.scrollHeight)-a.getStyle(p,"padding-top")-a.getStyle(p,"padding-bottom"):(p=H.getComputedStyle(p,void 0))&&a.pInt(p.getPropertyValue(d))};a.inArray=function(a,d){return d.indexOf?d.indexOf(a):[].indexOf.call(d,a)};a.grep=function(a,d){return[].filter.call(a,d)};a.map=function(a,d){for(var g=[],v=0,l=a.length;v<l;v++)g[v]=
d.call(a[v],a[v],v,a);return g};a.offset=function(a){var d=F.documentElement;a=a.getBoundingClientRect();return{top:a.top+(H.pageYOffset||d.scrollTop)-(d.clientTop||0),left:a.left+(H.pageXOffset||d.scrollLeft)-(d.clientLeft||0)}};a.stop=function(a,d){for(var g=E.length;g--;)E[g].elem!==a||d&&d!==E[g].prop||(E[g].stopped=!0)};a.each=function(a,d,g){return Array.prototype.forEach.call(a,d,g)};a.addEvent=function(p,d,g){function v(a){a.target=a.srcElement||H;g.call(p,a)}var l=p.hcEvents=p.hcEvents||
{};p.addEventListener?p.addEventListener(d,g,!1):p.attachEvent&&(p.hcEventsIE||(p.hcEventsIE={}),p.hcEventsIE[g.toString()]=v,p.attachEvent("on"+d,v));l[d]||(l[d]=[]);l[d].push(g);return function(){a.removeEvent(p,d,g)}};a.removeEvent=function(p,d,g){function v(a,b){p.removeEventListener?p.removeEventListener(a,b,!1):p.attachEvent&&(b=p.hcEventsIE[b.toString()],p.detachEvent("on"+a,b))}function l(){var a,b;if(p.nodeName)for(b in d?(a={},a[d]=!0):a=f,a)if(f[b])for(a=f[b].length;a--;)v(b,f[b][a])}var r,
f=p.hcEvents,b;f&&(d?(r=f[d]||[],g?(b=a.inArray(g,r),-1<b&&(r.splice(b,1),f[d]=r),v(d,g)):(l(),f[d]=[])):(l(),p.hcEvents={}))};a.fireEvent=function(p,d,g,v){var l;l=p.hcEvents;var r,f;g=g||{};if(F.createEvent&&(p.dispatchEvent||p.fireEvent))l=F.createEvent("Events"),l.initEvent(d,!0,!0),a.extend(l,g),p.dispatchEvent?p.dispatchEvent(l):p.fireEvent(d,l);else if(l)for(l=l[d]||[],r=l.length,g.target||a.extend(g,{preventDefault:function(){g.defaultPrevented=!0},target:p,type:d}),d=0;d<r;d++)(f=l[d])&&
!1===f.call(p,g)&&g.preventDefault();v&&!g.defaultPrevented&&v(g)};a.animate=function(p,d,g){var v,l="",r,f,b;a.isObject(g)||(v=arguments,g={duration:v[2],easing:v[3],complete:v[4]});a.isNumber(g.duration)||(g.duration=400);g.easing="function"===typeof g.easing?g.easing:Math[g.easing]||Math.easeInOutSine;g.curAnim=a.merge(d);for(b in d)a.stop(p,b),f=new a.Fx(p,g,b),r=null,"d"===b?(f.paths=f.initPath(p,p.d,d.d),f.toD=d.d,v=0,r=1):p.attr?v=p.attr(b):(v=parseFloat(a.getStyle(p,b))||0,"opacity"!==b&&
(l="px")),r||(r=d[b]),r.match&&r.match("px")&&(r=r.replace(/px/g,"")),f.run(v,r,l)};a.seriesType=function(p,d,g,v,l){var r=a.getOptions(),f=a.seriesTypes;r.plotOptions[p]=a.merge(r.plotOptions[d],g);f[p]=a.extendClass(f[d]||function(){},v);f[p].prototype.type=p;l&&(f[p].prototype.pointClass=a.extendClass(a.Point,l));return f[p]};a.uniqueKey=function(){var a=Math.random().toString(36).substring(2,9),d=0;return function(){return"highcharts-"+a+"-"+d++}}();H.jQuery&&(H.jQuery.fn.highcharts=function(){var p=
[].slice.call(arguments);if(this[0])return p[0]?(new (a[a.isString(p[0])?p.shift():"Chart"])(this[0],p[0],p[1]),this):A[a.attr(this[0],"data-highcharts-chart")]});F&&!F.defaultView&&(a.getStyle=function(p,d){var g={width:"clientWidth",height:"clientHeight"}[d];if(p.style[d])return a.pInt(p.style[d]);"opacity"===d&&(d="filter");if(g)return p.style.zoom=1,Math.max(p[g]-2*a.getStyle(p,"padding"),0);p=p.currentStyle[d.replace(/\-(\w)/g,function(a,l){return l.toUpperCase()})];"filter"===d&&(p=p.replace(/alpha\(opacity=([0-9]+)\)/,
function(a,l){return l/100}));return""===p?1:a.pInt(p)});Array.prototype.forEach||(a.each=function(a,d,g){for(var v=0,l=a.length;v<l;v++)if(!1===d.call(g,a[v],v,a))return v});Array.prototype.indexOf||(a.inArray=function(a,d){var g,v=0;if(d)for(g=d.length;v<g;v++)if(d[v]===a)return v;return-1});Array.prototype.filter||(a.grep=function(a,d){for(var g=[],v=0,l=a.length;v<l;v++)d(a[v],v)&&g.push(a[v]);return g})})(M);(function(a){var E=a.each,A=a.isNumber,F=a.map,H=a.merge,p=a.pInt;a.Color=function(d){if(!(this instanceof
a.Color))return new a.Color(d);this.init(d)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[p(a[1]),p(a[2]),p(a[3]),parseFloat(a[4],10)]}},{regex:/#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/,parse:function(a){return[p(a[1],16),p(a[2],16),p(a[3],16),1]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[p(a[1]),p(a[2]),p(a[3]),1]}}],names:{white:"#ffffff",
black:"#000000"},init:function(d){var g,v,l,r;if((this.input=d=this.names[d]||d)&&d.stops)this.stops=F(d.stops,function(f){return new a.Color(f[1])});else for(l=this.parsers.length;l--&&!v;)r=this.parsers[l],(g=r.regex.exec(d))&&(v=r.parse(g));this.rgba=v||[]},get:function(a){var g=this.input,d=this.rgba,l;this.stops?(l=H(g),l.stops=[].concat(l.stops),E(this.stops,function(d,f){l.stops[f]=[l.stops[f][0],d.get(a)]})):l=d&&A(d[0])?"rgb"===a||!a&&1===d[3]?"rgb("+d[0]+","+d[1]+","+d[2]+")":"a"===a?d[3]:
"rgba("+d.join(",")+")":g;return l},brighten:function(a){var d,v=this.rgba;if(this.stops)E(this.stops,function(l){l.brighten(a)});else if(A(a)&&0!==a)for(d=0;3>d;d++)v[d]+=p(255*a),0>v[d]&&(v[d]=0),255<v[d]&&(v[d]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this}};a.color=function(d){return new a.Color(d)}})(M);(function(a){var E,A,F=a.addEvent,H=a.animate,p=a.attr,d=a.charts,g=a.color,v=a.css,l=a.createElement,r=a.defined,f=a.deg2rad,b=a.destroyObjectProperties,n=a.doc,w=a.each,
t=a.extend,k=a.erase,e=a.grep,h=a.hasTouch,C=a.isArray,u=a.isFirefox,c=a.isMS,q=a.isObject,x=a.isString,K=a.isWebKit,I=a.merge,J=a.noop,D=a.pick,G=a.pInt,L=a.removeEvent,N=a.stop,m=a.svg,z=a.SVG_NS,O=a.symbolSizes,P=a.win;E=a.SVGElement=function(){return this};E.prototype={opacity:1,SVG_NS:z,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textDecoration textOverflow textOutline".split(" "),init:function(a,B){this.element="span"===B?l(B):n.createElementNS(this.SVG_NS,
B);this.renderer=a},animate:function(a,B,c){(B=D(B,this.renderer.globalAnimation,!0))?(c&&(B.complete=c),H(this,a,B)):this.attr(a,null,c);return this},colorGradient:function(y,B,c){var m=this.renderer,b,e,z,q,k,Q,h,f,x,n,t,u=[],D;y.linearGradient?e="linearGradient":y.radialGradient&&(e="radialGradient");if(e){z=y[e];k=m.gradients;h=y.stops;n=c.radialReference;C(z)&&(y[e]=z={x1:z[0],y1:z[1],x2:z[2],y2:z[3],gradientUnits:"userSpaceOnUse"});"radialGradient"===e&&n&&!r(z.gradientUnits)&&(q=z,z=I(z,m.getRadialAttr(n,
q),{gradientUnits:"userSpaceOnUse"}));for(t in z)"id"!==t&&u.push(t,z[t]);for(t in h)u.push(h[t]);u=u.join(",");k[u]?n=k[u].attr("id"):(z.id=n=a.uniqueKey(),k[u]=Q=m.createElement(e).attr(z).add(m.defs),Q.radAttr=q,Q.stops=[],w(h,function(y){0===y[1].indexOf("rgba")?(b=a.color(y[1]),f=b.get("rgb"),x=b.get("a")):(f=y[1],x=1);y=m.createElement("stop").attr({offset:y[0],"stop-color":f,"stop-opacity":x}).add(Q);Q.stops.push(y)}));D="url("+m.url+"#"+n+")";c.setAttribute(B,D);c.gradient=u;y.toString=function(){return D}}},
applyTextOutline:function(a){var y=this.element,c,m,b;-1!==a.indexOf("contrast")&&(a=a.replace(/contrast/g,this.renderer.getContrast(y.style.fill)));this.fakeTS=!0;this.ySetter=this.xSetter;c=[].slice.call(y.getElementsByTagName("tspan"));a=a.split(" ");m=a[a.length-1];(b=a[0])&&"none"!==b&&(b=b.replace(/(^[\d\.]+)(.*?)$/g,function(a,y,B){return 2*y+B}),w(c,function(a){"highcharts-text-outline"===a.getAttribute("class")&&k(c,y.removeChild(a))}),w(c,function(a,B){0===B&&(a.setAttribute("x",y.getAttribute("x")),
B=y.getAttribute("y"),a.setAttribute("y",B||0),null===B&&y.setAttribute("y",0));a=a.cloneNode(1);p(a,{"class":"highcharts-text-outline",fill:m,stroke:m,"stroke-width":b,"stroke-linejoin":"round"});y.insertBefore(a,y.firstChild)}))},attr:function(a,B,c,m){var y,b=this.element,e,z=this,q;"string"===typeof a&&void 0!==B&&(y=a,a={},a[y]=B);if("string"===typeof a)z=(this[a+"Getter"]||this._defaultGetter).call(this,a,b);else{for(y in a)B=a[y],q=!1,m||N(this,y),this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)/.test(y)&&
(e||(this.symbolAttr(a),e=!0),q=!0),!this.rotation||"x"!==y&&"y"!==y||(this.doTransform=!0),q||(q=this[y+"Setter"]||this._defaultSetter,q.call(this,B,y,b),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(y)&&this.updateShadows(y,B,q));this.doTransform&&(this.updateTransform(),this.doTransform=!1)}c&&c();return z},updateShadows:function(a,B,c){for(var y=this.shadows,m=y.length;m--;)c.call(y[m],"height"===a?Math.max(B-(y[m].cutHeight||0),0):"d"===a?this.d:B,a,y[m])},addClass:function(a,
B){var y=this.attr("class")||"";-1===y.indexOf(a)&&(B||(a=(y+(y?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==p(this.element,"class").indexOf(a)},removeClass:function(a){p(this.element,"class",(p(this.element,"class")||"").replace(a,""));return this},symbolAttr:function(a){var y=this;w("x y r start end width height innerR anchorX anchorY".split(" "),function(B){y[B]=D(a[B],y[B])});y.attr({d:y.renderer.symbols[y.symbolName](y.x,y.y,y.width,y.height,
y)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,B){var y,c={},m;B=B||a.strokeWidth||0;m=Math.round(B)%2/2;a.x=Math.floor(a.x||this.x||0)+m;a.y=Math.floor(a.y||this.y||0)+m;a.width=Math.floor((a.width||this.width||0)-2*m);a.height=Math.floor((a.height||this.height||0)-2*m);r(a.strokeWidth)&&(a.strokeWidth=B);for(y in a)this[y]!==a[y]&&(this[y]=c[y]=a[y]);return c},css:function(a){var y=this.styles,b={},e=this.element,z,q,k="";z=
!y;a&&a.color&&(a.fill=a.color);if(y)for(q in a)a[q]!==y[q]&&(b[q]=a[q],z=!0);if(z){z=this.textWidth=a&&a.width&&"text"===e.nodeName.toLowerCase()&&G(a.width)||this.textWidth;y&&(a=t(y,b));this.styles=a;z&&!m&&this.renderer.forExport&&delete a.width;if(c&&!m)v(this.element,a);else{y=function(a,y){return"-"+y.toLowerCase()};for(q in a)k+=q.replace(/([A-Z])/g,y)+":"+a[q]+";";p(e,"style",k)}this.added&&(z&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline))}return this},
strokeWidth:function(){return this["stroke-width"]||0},on:function(a,B){var y=this,c=y.element;h&&"click"===a?(c.ontouchstart=function(a){y.touchEventFired=Date.now();a.preventDefault();B.call(c,a)},c.onclick=function(a){(-1===P.navigator.userAgent.indexOf("Android")||1100<Date.now()-(y.touchEventFired||0))&&B.call(c,a)}):c["on"+a]=B;return this},setRadialReference:function(a){var y=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;y&&y.radAttr&&y.animate(this.renderer.getRadialAttr(a,
y.radAttr));return this},translate:function(a,B){return this.attr({translateX:a,translateY:B})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,B=this.translateY||0,c=this.scaleX,m=this.scaleY,b=this.inverted,e=this.rotation,z=this.element;b&&(a+=this.attr("width"),B+=this.attr("height"));a=["translate("+a+","+B+")"];b?a.push("rotate(90) scale(-1,1)"):e&&a.push("rotate("+e+" "+(z.getAttribute("x")||0)+" "+(z.getAttribute("y")||
0)+")");(r(c)||r(m))&&a.push("scale("+D(c,1)+" "+D(m,1)+")");a.length&&z.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,B,c){var y,m,b,e,z={};m=this.renderer;b=m.alignedObjects;var q,h;if(a){if(this.alignOptions=a,this.alignByTranslate=B,!c||x(c))this.alignTo=y=c||"renderer",k(b,this),b.push(this),c=null}else a=this.alignOptions,B=this.alignByTranslate,y=this.alignTo;c=D(c,m[y],m);y=a.align;m=a.verticalAlign;b=
(c.x||0)+(a.x||0);e=(c.y||0)+(a.y||0);"right"===y?q=1:"center"===y&&(q=2);q&&(b+=(c.width-(a.width||0))/q);z[B?"translateX":"x"]=Math.round(b);"bottom"===m?h=1:"middle"===m&&(h=2);h&&(e+=(c.height-(a.height||0))/h);z[B?"translateY":"y"]=Math.round(e);this[this.placed?"animate":"attr"](z);this.placed=!0;this.alignAttr=z;return this},getBBox:function(a,B){var y,m=this.renderer,b,e=this.element,z=this.styles,q,k=this.textStr,h,x=m.cache,n=m.cacheKeys,u;B=D(B,this.rotation);b=B*f;q=z&&z.fontSize;void 0!==
k&&(u=k.toString(),-1===u.indexOf("\x3c")&&(u=u.replace(/[0-9]/g,"0")),u+=["",B||0,q,e.style.width,e.style["text-overflow"]].join());u&&!a&&(y=x[u]);if(!y){if(e.namespaceURI===this.SVG_NS||m.forExport){try{(h=this.fakeTS&&function(a){w(e.querySelectorAll(".highcharts-text-outline"),function(y){y.style.display=a})})&&h("none"),y=e.getBBox?t({},e.getBBox()):{width:e.offsetWidth,height:e.offsetHeight},h&&h("")}catch(T){}if(!y||0>y.width)y={width:0,height:0}}else y=this.htmlGetBBox();m.isSVG&&(a=y.width,
m=y.height,c&&z&&"11px"===z.fontSize&&"16.9"===m.toPrecision(3)&&(y.height=m=14),B&&(y.width=Math.abs(m*Math.sin(b))+Math.abs(a*Math.cos(b)),y.height=Math.abs(m*Math.cos(b))+Math.abs(a*Math.sin(b))));if(u&&0<y.height){for(;250<n.length;)delete x[n.shift()];x[u]||n.push(u);x[u]=y}}return y},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var y=this;y.animate({opacity:0},{duration:a||150,complete:function(){y.attr({y:-9999})}})},
add:function(a){var y=this.renderer,c=this.element,m;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&y.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)m=this.zIndexSetter();m||(a?a.element:y.box).appendChild(c);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var y=a.parentNode;y&&y.removeChild(a)},destroy:function(){var a=this.element||{},c=this.renderer.isSVG&&"SPAN"===a.nodeName&&this.parentGroup,m,b;a.onclick=a.onmouseout=a.onmouseover=
a.onmousemove=a.point=null;N(this);this.clipPath&&(this.clipPath=this.clipPath.destroy());if(this.stops){for(b=0;b<this.stops.length;b++)this.stops[b]=this.stops[b].destroy();this.stops=null}this.safeRemoveChild(a);for(this.destroyShadows();c&&c.div&&0===c.div.childNodes.length;)a=c.parentGroup,this.safeRemoveChild(c.div),delete c.div,c=a;this.alignTo&&k(this.renderer.alignedObjects,this);for(m in this)delete this[m];return null},shadow:function(a,c,m){var y=[],B,b,e=this.element,z,q,k,h;if(!a)this.destroyShadows();
else if(!this.shadows){q=D(a.width,3);k=(a.opacity||.15)/q;h=this.parentInverted?"(-1,-1)":"("+D(a.offsetX,1)+", "+D(a.offsetY,1)+")";for(B=1;B<=q;B++)b=e.cloneNode(0),z=2*q+1-2*B,p(b,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":k*B,"stroke-width":z,transform:"translate"+h,fill:"none"}),m&&(p(b,"height",Math.max(p(b,"height")-z,0)),b.cutHeight=z),c?c.element.appendChild(b):e.parentNode.insertBefore(b,e),y.push(b);this.shadows=y}return this},destroyShadows:function(){w(this.shadows||
[],function(a){this.safeRemoveChild(a)},this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=D(this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,c,m){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");m.setAttribute(c,a);this[c]=a},dashstyleSetter:function(a){var c,y=this["stroke-width"];
"inherit"===y&&(y=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(c=a.length;c--;)a[c]=G(a[c])*y;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.element.setAttribute("text-anchor",{left:"start",center:"middle",right:"end"}[a])},
opacitySetter:function(a,c,m){this[c]=a;m.setAttribute(c,a)},titleSetter:function(a){var c=this.element.getElementsByTagName("title")[0];c||(c=n.createElementNS(this.SVG_NS,"title"),this.element.appendChild(c));c.firstChild&&c.removeChild(c.firstChild);c.appendChild(n.createTextNode(String(D(a),"").replace(/<[^>]*>/g,"")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},fillSetter:function(a,c,m){"string"===typeof a?m.setAttribute(c,
a):a&&this.colorGradient(a,c,m)},visibilitySetter:function(a,c,m){"inherit"===a?m.removeAttribute(c):m.setAttribute(c,a)},zIndexSetter:function(a,c){var m=this.renderer,y=this.parentGroup,b=(y||m).element||m.box,B,e=this.element,z;B=this.added;var q;r(a)&&(e.zIndex=a,a=+a,this[c]===a&&(B=!1),this[c]=a);if(B){(a=this.zIndex)&&y&&(y.handleZ=!0);c=b.childNodes;for(q=0;q<c.length&&!z;q++)y=c[q],B=y.zIndex,y!==e&&(G(B)>a||!r(a)&&r(B)||0>a&&!r(B)&&b!==m.box)&&(b.insertBefore(e,y),z=!0);z||b.appendChild(e)}return z},
_defaultSetter:function(a,c,m){m.setAttribute(c,a)}};E.prototype.yGetter=E.prototype.xGetter;E.prototype.translateXSetter=E.prototype.translateYSetter=E.prototype.rotationSetter=E.prototype.verticalAlignSetter=E.prototype.scaleXSetter=E.prototype.scaleYSetter=function(a,c){this[c]=a;this.doTransform=!0};E.prototype["stroke-widthSetter"]=E.prototype.strokeSetter=function(a,c,m){this[c]=a;this.stroke&&this["stroke-width"]?(E.prototype.fillSetter.call(this,this.stroke,"stroke",m),m.setAttribute("stroke-width",
this["stroke-width"]),this.hasStroke=!0):"stroke-width"===c&&0===a&&this.hasStroke&&(m.removeAttribute("stroke"),this.hasStroke=!1)};A=a.SVGRenderer=function(){this.init.apply(this,arguments)};A.prototype={Element:E,SVG_NS:z,init:function(a,c,m,b,e,z){var y;b=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(b));y=b.element;a.appendChild(y);-1===a.innerHTML.indexOf("xmlns")&&p(y,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=y;this.boxWrapper=b;this.alignedObjects=
[];this.url=(u||K)&&n.getElementsByTagName("base").length?P.location.href.replace(/#.*?$/,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(n.createTextNode("Created with Highcharts 5.0.5"));this.defs=this.createElement("defs").add();this.allowHTML=z;this.forExport=e;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(c,m,!1);var B;u&&a.getBoundingClientRect&&(c=function(){v(a,{left:0,top:0});B=a.getBoundingClientRect();
v(a,{left:Math.ceil(B.left)-B.left+"px",top:Math.ceil(B.top)-B.top+"px"})},c(),this.unSubPixelFix=F(P,"resize",c))},getStyle:function(a){return this.style=t({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();b(this.gradients||{});this.gradients=
null;a&&(this.defs=a.destroy());this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var c=new this.Element;c.init(this,a);return c},draw:J,getRadialAttr:function(a,c){return{cx:a[0]-a[2]/2+c.cx*a[2],cy:a[1]-a[2]/2+c.cy*a[2],r:c.r*a[2]}},buildText:function(a){for(var c=a.element,b=this,y=b.forExport,q=D(a.textStr,"").toString(),k=-1!==q.indexOf("\x3c"),h=c.childNodes,x,f,t,u,l=p(c,"x"),d=a.styles,C=a.textWidth,g=d&&d.lineHeight,r=d&&d.textOutline,K=d&&
"ellipsis"===d.textOverflow,I=h.length,L=C&&!a.added&&this.box,P=function(a){var m;m=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:d&&d.fontSize||b.style.fontSize||12;return g?G(g):b.fontMetrics(m,a.getAttribute("style")?a:c).h};I--;)c.removeChild(h[I]);k||r||K||C||-1!==q.indexOf(" ")?(x=/<.*class="([^"]+)".*>/,f=/<.*style="([^"]+)".*>/,t=/<.*href="(http[^"]+)".*>/,L&&L.appendChild(c),q=k?q.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,
"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,"\x3c/span\x3e").split(/<br.*?>/g):[q],q=e(q,function(a){return""!==a}),w(q,function(e,B){var q,k=0;e=e.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");q=e.split("|||");w(q,function(e){if(""!==e||1===q.length){var h={},D=n.createElementNS(b.SVG_NS,"tspan"),G,g;x.test(e)&&(G=e.match(x)[1],p(D,"class",G));f.test(e)&&(g=e.match(f)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),p(D,"style",g));t.test(e)&&!y&&(p(D,
"onclick",'location.href\x3d"'+e.match(t)[1]+'"'),v(D,{cursor:"pointer"}));e=(e.replace(/<(.|\n)*?>/g,"")||" ").replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e");if(" "!==e){D.appendChild(n.createTextNode(e));k?h.dx=0:B&&null!==l&&(h.x=l);p(D,h);c.appendChild(D);!k&&B&&(!m&&y&&v(D,{display:"block"}),p(D,"dy",P(D)));if(C){h=e.replace(/([^\^])-/g,"$1- ").split(" ");G="nowrap"===d.whiteSpace;for(var Q=1<q.length||B||1<h.length&&!G,r,I,w=[],L=P(D),S=a.rotation,O=e,R=O.length;(Q||K)&&(h.length||w.length);)a.rotation=
0,r=a.getBBox(!0),I=r.width,!m&&b.forExport&&(I=b.measureSpanWidth(D.firstChild.data,a.styles)),r=I>C,void 0===u&&(u=r),K&&u?(R/=2,""===O||!r&&.5>R?h=[]:(O=e.substring(0,O.length+(r?-1:1)*Math.ceil(R)),h=[O+(3<C?"\u2026":"")],D.removeChild(D.firstChild))):r&&1!==h.length?(D.removeChild(D.firstChild),w.unshift(h.pop())):(h=w,w=[],h.length&&!G&&(D=n.createElementNS(z,"tspan"),p(D,{dy:L,x:l}),g&&p(D,"style",g),c.appendChild(D)),I>C&&(C=I)),h.length&&D.appendChild(n.createTextNode(h.join(" ").replace(/- /g,
"-")));a.rotation=S}k++}}})}),u&&a.attr("title",a.textStr),L&&L.removeChild(c),r&&a.applyTextOutline&&a.applyTextOutline(r)):c.appendChild(n.createTextNode(q.replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e")))},getContrast:function(a){a=g(a).rgba;return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,m,b,e,z,q,h,k,x){var B=this.label(a,m,b,x,null,null,null,null,"button"),y=0;B.attr(I({padding:8,r:2},z));var f,n,u,D;z=I({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",
cursor:"pointer",fontWeight:"normal"}},z);f=z.style;delete z.style;q=I(z,{fill:"#e6e6e6"},q);n=q.style;delete q.style;h=I(z,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},h);u=h.style;delete h.style;k=I(z,{style:{color:"#cccccc"}},k);D=k.style;delete k.style;F(B.element,c?"mouseover":"mouseenter",function(){3!==y&&B.setState(1)});F(B.element,c?"mouseout":"mouseleave",function(){3!==y&&B.setState(y)});B.setState=function(a){1!==a&&(B.state=y=a);B.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+
["normal","hover","pressed","disabled"][a||0]);B.attr([z,q,h,k][a||0]).css([f,n,u,D][a||0])};B.attr(z).css(t({cursor:"default"},f));return B.on("click",function(a){3!==y&&e.call(B,a)})},crispLine:function(a,c){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-c%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+c%2/2);return a},path:function(a){var c={fill:"none"};C(a)?c.d=a:q(a)&&t(c,a);return this.createElement("path").attr(c)},circle:function(a,c,m){a=q(a)?a:{x:a,y:c,r:m};c=this.createElement("circle");c.xSetter=
c.ySetter=function(a,c,m){m.setAttribute("c"+c,a)};return c.attr(a)},arc:function(a,c,m,b,e,z){q(a)&&(c=a.y,m=a.r,b=a.innerR,e=a.start,z=a.end,a=a.x);a=this.symbol("arc",a||0,c||0,m||0,m||0,{innerR:b||0,start:e||0,end:z||0});a.r=m;return a},rect:function(a,c,m,b,e,z){e=q(a)?a.r:e;var B=this.createElement("rect");a=q(a)?a:void 0===a?{}:{x:a,y:c,width:Math.max(m,0),height:Math.max(b,0)};void 0!==z&&(a.strokeWidth=z,a=B.crisp(a));a.fill="none";e&&(a.r=e);B.rSetter=function(a,c,m){p(m,{rx:a,ry:a})};return B.attr(a)},
setSize:function(a,c,m){var b=this.alignedObjects,e=b.length;this.width=a;this.height=c;for(this.boxWrapper.animate({width:a,height:c},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:D(m,!0)?void 0:0});e--;)b[e].align()},g:function(a){var c=this.createElement("g");return a?c.attr({"class":"highcharts-"+a}):c},image:function(a,c,m,b,e){var z={preserveAspectRatio:"none"};1<arguments.length&&t(z,{x:c,y:m,width:b,height:e});z=this.createElement("image").attr(z);
z.element.setAttributeNS?z.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):z.element.setAttribute("hc-svg-href",a);return z},symbol:function(a,c,m,b,e,z){var q=this,B,y=this.symbols[a],h=r(c)&&y&&y(Math.round(c),Math.round(m),b,e,z),k=/^url\((.*?)\)$/,x,f;y?(B=this.path(h),B.attr("fill","none"),t(B,{symbolName:a,x:c,y:m,width:b,height:e}),z&&t(B,z)):k.test(a)&&(x=a.match(k)[1],B=this.image(x),B.imgwidth=D(O[x]&&O[x].width,z&&z.width),B.imgheight=D(O[x]&&O[x].height,z&&z.height),f=
function(){B.attr({width:B.width,height:B.height})},w(["width","height"],function(a){B[a+"Setter"]=function(a,c){var m={},b=this["img"+c],e="width"===c?"translateX":"translateY";this[c]=a;r(b)&&(this.element&&this.element.setAttribute(c,b),this.alignByTranslate||(m[e]=((this[c]||0)-b)/2,this.attr(m)))}}),r(c)&&B.attr({x:c,y:m}),B.isImg=!0,r(B.imgwidth)&&r(B.imgheight)?f():(B.attr({width:0,height:0}),l("img",{onload:function(){var a=d[q.chartIndex];0===this.width&&(v(this,{position:"absolute",top:"-999em"}),
n.body.appendChild(this));O[x]={width:this.width,height:this.height};B.imgwidth=this.width;B.imgheight=this.height;B.element&&f();this.parentNode&&this.parentNode.removeChild(this);q.imgCount--;if(!q.imgCount&&a&&a.onload)a.onload()},src:x}),this.imgCount++));return B},symbols:{circle:function(a,c,m,b){var e=.166*m;return["M",a+m/2,c,"C",a+m+e,c,a+m+e,c+b,a+m/2,c+b,"C",a-e,c+b,a-e,c,a+m/2,c,"Z"]},square:function(a,c,m,b){return["M",a,c,"L",a+m,c,a+m,c+b,a,c+b,"Z"]},triangle:function(a,c,m,b){return["M",
a+m/2,c,"L",a+m,c+b,a,c+b,"Z"]},"triangle-down":function(a,c,m,b){return["M",a,c,"L",a+m,c,a+m/2,c+b,"Z"]},diamond:function(a,c,m,b){return["M",a+m/2,c,"L",a+m,c+b/2,a+m/2,c+b,a,c+b/2,"Z"]},arc:function(a,c,m,b,e){var z=e.start;m=e.r||m||b;var q=e.end-.001;b=e.innerR;var B=e.open,h=Math.cos(z),k=Math.sin(z),y=Math.cos(q),q=Math.sin(q);e=e.end-z<Math.PI?0:1;return["M",a+m*h,c+m*k,"A",m,m,0,e,1,a+m*y,c+m*q,B?"M":"L",a+b*y,c+b*q,"A",b,b,0,e,0,a+b*h,c+b*k,B?"":"Z"]},callout:function(a,c,m,b,e){var z=
Math.min(e&&e.r||0,m,b),q=z+6,B=e&&e.anchorX;e=e&&e.anchorY;var h;h=["M",a+z,c,"L",a+m-z,c,"C",a+m,c,a+m,c,a+m,c+z,"L",a+m,c+b-z,"C",a+m,c+b,a+m,c+b,a+m-z,c+b,"L",a+z,c+b,"C",a,c+b,a,c+b,a,c+b-z,"L",a,c+z,"C",a,c,a,c,a+z,c];B&&B>m?e>c+q&&e<c+b-q?h.splice(13,3,"L",a+m,e-6,a+m+6,e,a+m,e+6,a+m,c+b-z):h.splice(13,3,"L",a+m,b/2,B,e,a+m,b/2,a+m,c+b-z):B&&0>B?e>c+q&&e<c+b-q?h.splice(33,3,"L",a,e+6,a-6,e,a,e-6,a,c+z):h.splice(33,3,"L",a,b/2,B,e,a,b/2,a,c+z):e&&e>b&&B>a+q&&B<a+m-q?h.splice(23,3,"L",B+6,c+
b,B,c+b+6,B-6,c+b,a+z,c+b):e&&0>e&&B>a+q&&B<a+m-q&&h.splice(3,3,"L",B-6,c,B,c-6,B+6,c,m-z,c);return h}},clipRect:function(c,m,b,e){var z=a.uniqueKey(),q=this.createElement("clipPath").attr({id:z}).add(this.defs);c=this.rect(c,m,b,e,0).add(q);c.id=z;c.clipPath=q;c.count=0;return c},text:function(a,c,b,e){var z=!m&&this.forExport,q={};if(e&&(this.allowHTML||!this.forExport))return this.html(a,c,b);q.x=Math.round(c||0);b&&(q.y=Math.round(b));if(a||0===a)q.text=a;a=this.createElement("text").attr(q);
z&&a.css({position:"absolute"});e||(a.xSetter=function(a,c,m){var b=m.getElementsByTagName("tspan"),e,z=m.getAttribute(c),q;for(q=0;q<b.length;q++)e=b[q],e.getAttribute(c)===z&&e.setAttribute(c,a);m.setAttribute(c,a)});return a},fontMetrics:function(a,c){a=a||c&&c.style&&c.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?G(a):/em/.test(a)?parseFloat(a)*(c?this.fontMetrics(null,c.parentNode).f:16):12;c=24>a?a+3:Math.round(1.2*a);return{h:c,b:Math.round(.8*c),f:a}},rotCorr:function(a,
c,m){var b=a;c&&m&&(b=Math.max(b*Math.cos(c*f),4));return{x:-a/3*Math.sin(c*f),y:b}},label:function(a,c,m,b,e,z,q,h,k){var B=this,x=B.g("button"!==k&&"label"),f=x.text=B.text("",0,0,q).attr({zIndex:1}),n,u,D=0,y=3,l=0,G,d,C,g,K,P={},O,v,N=/^url\((.*?)\)$/.test(b),Q=N,J,p,S,R;k&&x.addClass("highcharts-"+k);Q=N;J=function(){return(O||0)%2/2};p=function(){var a=f.element.style,c={};u=(void 0===G||void 0===d||K)&&r(f.textStr)&&f.getBBox();x.width=(G||u.width||0)+2*y+l;x.height=(d||u.height||0)+2*y;v=
y+B.fontMetrics(a&&a.fontSize,f).b;Q&&(n||(x.box=n=B.symbols[b]||N?B.symbol(b):B.rect(),n.addClass(("button"===k?"":"highcharts-label-box")+(k?" highcharts-"+k+"-box":"")),n.add(x),a=J(),c.x=a,c.y=(h?-v:0)+a),c.width=Math.round(x.width),c.height=Math.round(x.height),n.attr(t(c,P)),P={})};S=function(){var a=l+y,c;c=h?0:v;r(G)&&u&&("center"===K||"right"===K)&&(a+={center:.5,right:1}[K]*(G-u.width));if(a!==f.x||c!==f.y)f.attr("x",a),void 0!==c&&f.attr("y",c);f.x=a;f.y=c};R=function(a,c){n?n.attr(a,c):
P[a]=c};x.onAdd=function(){f.add(x);x.attr({text:a||0===a?a:"",x:c,y:m});n&&r(e)&&x.attr({anchorX:e,anchorY:z})};x.widthSetter=function(a){G=a};x.heightSetter=function(a){d=a};x["text-alignSetter"]=function(a){K=a};x.paddingSetter=function(a){r(a)&&a!==y&&(y=x.padding=a,S())};x.paddingLeftSetter=function(a){r(a)&&a!==l&&(l=a,S())};x.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==D&&(D=a,u&&x.attr({x:C}))};x.textSetter=function(a){void 0!==a&&f.textSetter(a);p();S()};x["stroke-widthSetter"]=
function(a,c){a&&(Q=!0);O=this["stroke-width"]=a;R(c,a)};x.strokeSetter=x.fillSetter=x.rSetter=function(a,c){"fill"===c&&a&&(Q=!0);R(c,a)};x.anchorXSetter=function(a,c){e=a;R(c,Math.round(a)-J()-C)};x.anchorYSetter=function(a,c){z=a;R(c,a-g)};x.xSetter=function(a){x.x=a;D&&(a-=D*((G||u.width)+2*y));C=Math.round(a);x.attr("translateX",C)};x.ySetter=function(a){g=x.y=Math.round(a);x.attr("translateY",g)};var V=x.css;return t(x,{css:function(a){if(a){var c={};a=I(a);w(x.textProps,function(m){void 0!==
a[m]&&(c[m]=a[m],delete a[m])});f.css(c)}return V.call(x,a)},getBBox:function(){return{width:u.width+2*y,height:u.height+2*y,x:u.x-y,y:u.y-y}},shadow:function(a){a&&(p(),n&&n.shadow(a));return x},destroy:function(){L(x.element,"mouseenter");L(x.element,"mouseleave");f&&(f=f.destroy());n&&(n=n.destroy());E.prototype.destroy.call(x);x=B=p=S=R=null}})}};a.Renderer=A})(M);(function(a){var E=a.attr,A=a.createElement,F=a.css,H=a.defined,p=a.each,d=a.extend,g=a.isFirefox,v=a.isMS,l=a.isWebKit,r=a.pInt,f=
a.SVGRenderer,b=a.win,n=a.wrap;d(a.SVGElement.prototype,{htmlCss:function(a){var b=this.element;if(b=a&&"SPAN"===b.tagName&&a.width)delete a.width,this.textWidth=b,this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=d(this.styles,a);F(this.element,a);return this},htmlGetBBox:function(){var a=this.element;"text"===a.nodeName&&(a.style.position="absolute");return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=
this.renderer,b=this.element,k=this.translateX||0,e=this.translateY||0,h=this.x||0,f=this.y||0,n=this.textAlign||"left",c={left:0,center:.5,right:1}[n],q=this.styles;F(b,{marginLeft:k,marginTop:e});this.shadows&&p(this.shadows,function(a){F(a,{marginLeft:k+1,marginTop:e+1})});this.inverted&&p(b.childNodes,function(c){a.invertChild(c,b)});if("SPAN"===b.tagName){var x=this.rotation,d=r(this.textWidth),g=q&&q.whiteSpace,v=[x,n,b.innerHTML,this.textWidth,this.textAlign].join();v!==this.cTT&&(q=a.fontMetrics(b.style.fontSize).b,
H(x)&&this.setSpanRotation(x,c,q),F(b,{width:"",whiteSpace:g||"nowrap"}),b.offsetWidth>d&&/[ \-]/.test(b.textContent||b.innerText)&&F(b,{width:d+"px",display:"block",whiteSpace:g||"normal"}),this.getSpanCorrection(b.offsetWidth,q,c,x,n));F(b,{left:h+(this.xCorr||0)+"px",top:f+(this.yCorr||0)+"px"});l&&(q=b.offsetHeight);this.cTT=v}}else this.alignOnAdd=!0},setSpanRotation:function(a,f,k){var e={},h=v?"-ms-transform":l?"-webkit-transform":g?"MozTransform":b.opera?"-o-transform":"";e[h]=e.transform=
"rotate("+a+"deg)";e[h+(g?"Origin":"-origin")]=e.transformOrigin=100*f+"% "+k+"px";F(this.element,e)},getSpanCorrection:function(a,b,k){this.xCorr=-a*k;this.yCorr=-b}});d(f.prototype,{html:function(a,b,k){var e=this.createElement("span"),h=e.element,f=e.renderer,u=f.isSVG,c=function(a,c){p(["opacity","visibility"],function(b){n(a,b+"Setter",function(a,b,e,q){a.call(this,b,e,q);c[e]=b})})};e.textSetter=function(a){a!==h.innerHTML&&delete this.bBox;h.innerHTML=this.textStr=a;e.htmlUpdateTransform()};
u&&c(e,e.element.style);e.xSetter=e.ySetter=e.alignSetter=e.rotationSetter=function(a,c){"align"===c&&(c="textAlign");e[c]=a;e.htmlUpdateTransform()};e.attr({text:a,x:Math.round(b),y:Math.round(k)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});h.style.whiteSpace="nowrap";e.css=e.htmlCss;u&&(e.add=function(a){var b,q=f.box.parentNode,k=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)k.push(a),a=a.parentGroup;p(k.reverse(),function(a){var h,x=E(a.element,
"class");x&&(x={className:x});b=a.div=a.div||A("div",x,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},b||q);h=b.style;d(a,{on:function(){e.on.apply({element:k[0].div},arguments);return a},translateXSetter:function(c,b){h.left=c+"px";a[b]=c;a.doTransform=!0},translateYSetter:function(c,b){h.top=c+"px";a[b]=c;a.doTransform=!0}});c(a,h)})}}else b=q;b.appendChild(h);e.added=!0;e.alignOnAdd&&
e.htmlUpdateTransform();return e});return e}})})(M);(function(a){var E,A,F=a.createElement,H=a.css,p=a.defined,d=a.deg2rad,g=a.discardElement,v=a.doc,l=a.each,r=a.erase,f=a.extend;E=a.extendClass;var b=a.isArray,n=a.isNumber,w=a.isObject,t=a.merge;A=a.noop;var k=a.pick,e=a.pInt,h=a.SVGElement,C=a.SVGRenderer,u=a.win;a.svg||(A={docMode8:v&&8===v.documentMode,init:function(a,b){var c=["\x3c",b,' filled\x3d"f" stroked\x3d"f"'],e=["position: ","absolute",";"],q="div"===b;("shape"===b||q)&&e.push("left:0;top:0;width:1px;height:1px;");
e.push("visibility: ",q?"hidden":"visible");c.push(' style\x3d"',e.join(""),'"/\x3e');b&&(c=q||"span"===b||"img"===b?c.join(""):a.prepVML(c),this.element=F(c));this.renderer=a},add:function(a){var c=this.renderer,b=this.element,e=c.box,h=a&&a.inverted,e=a?a.element||a:e;a&&(this.parentGroup=a);h&&c.invertChild(b,e);e.appendChild(b);this.added=!0;this.alignOnAdd&&!this.deferUpdateTransform&&this.updateTransform();if(this.onAdd)this.onAdd();this.className&&this.attr("class",this.className);return this},
updateTransform:h.prototype.htmlUpdateTransform,setSpanRotation:function(){var a=this.rotation,b=Math.cos(a*d),e=Math.sin(a*d);H(this.element,{filter:a?["progid:DXImageTransform.Microsoft.Matrix(M11\x3d",b,", M12\x3d",-e,", M21\x3d",e,", M22\x3d",b,", sizingMethod\x3d'auto expand')"].join(""):"none"})},getSpanCorrection:function(a,b,e,h,f){var c=h?Math.cos(h*d):1,q=h?Math.sin(h*d):0,x=k(this.elemHeight,this.element.offsetHeight),n;this.xCorr=0>c&&-a;this.yCorr=0>q&&-x;n=0>c*q;this.xCorr+=q*b*(n?1-
e:e);this.yCorr-=c*b*(h?n?e:1-e:1);f&&"left"!==f&&(this.xCorr-=a*e*(0>c?-1:1),h&&(this.yCorr-=x*e*(0>q?-1:1)),H(this.element,{textAlign:f}))},pathToVML:function(a){for(var c=a.length,b=[];c--;)n(a[c])?b[c]=Math.round(10*a[c])-5:"Z"===a[c]?b[c]="x":(b[c]=a[c],!a.isArc||"wa"!==a[c]&&"at"!==a[c]||(b[c+5]===b[c+7]&&(b[c+7]+=a[c+7]>a[c+5]?1:-1),b[c+6]===b[c+8]&&(b[c+8]+=a[c+8]>a[c+6]?1:-1)));return b.join(" ")||"x"},clip:function(a){var c=this,b;a?(b=a.members,r(b,c),b.push(c),c.destroyClip=function(){r(b,
c)},a=a.getCSS(c)):(c.destroyClip&&c.destroyClip(),a={clip:c.docMode8?"inherit":"rect(auto)"});return c.css(a)},css:h.prototype.htmlCss,safeRemoveChild:function(a){a.parentNode&&g(a)},destroy:function(){this.destroyClip&&this.destroyClip();return h.prototype.destroy.apply(this)},on:function(a,b){this.element["on"+a]=function(){var a=u.event;a.target=a.srcElement;b(a)};return this},cutOffPath:function(a,b){var c;a=a.split(/[ ,]/);c=a.length;if(9===c||11===c)a[c-4]=a[c-2]=e(a[c-2])-10*b;return a.join(" ")},
shadow:function(a,b,h){var c=[],q,f=this.element,n=this.renderer,x,u=f.style,d,m=f.path,z,l,t,y;m&&"string"!==typeof m.value&&(m="x");l=m;if(a){t=k(a.width,3);y=(a.opacity||.15)/t;for(q=1;3>=q;q++)z=2*t+1-2*q,h&&(l=this.cutOffPath(m.value,z+.5)),d=['\x3cshape isShadow\x3d"true" strokeweight\x3d"',z,'" filled\x3d"false" path\x3d"',l,'" coordsize\x3d"10 10" style\x3d"',f.style.cssText,'" /\x3e'],x=F(n.prepVML(d),null,{left:e(u.left)+k(a.offsetX,1),top:e(u.top)+k(a.offsetY,1)}),h&&(x.cutOff=z+1),d=['\x3cstroke color\x3d"',
a.color||"#000000",'" opacity\x3d"',y*q,'"/\x3e'],F(n.prepVML(d),null,null,x),b?b.element.appendChild(x):f.parentNode.insertBefore(x,f),c.push(x);this.shadows=c}return this},updateShadows:A,setAttr:function(a,b){this.docMode8?this.element[a]=b:this.element.setAttribute(a,b)},classSetter:function(a){(this.added?this.element:this).className=a},dashstyleSetter:function(a,b,e){(e.getElementsByTagName("stroke")[0]||F(this.renderer.prepVML(["\x3cstroke/\x3e"]),null,null,e))[b]=a||"solid";this[b]=a},dSetter:function(a,
b,e){var c=this.shadows;a=a||[];this.d=a.join&&a.join(" ");e.path=a=this.pathToVML(a);if(c)for(e=c.length;e--;)c[e].path=c[e].cutOff?this.cutOffPath(a,c[e].cutOff):a;this.setAttr(b,a)},fillSetter:function(a,b,e){var c=e.nodeName;"SPAN"===c?e.style.color=a:"IMG"!==c&&(e.filled="none"!==a,this.setAttr("fillcolor",this.renderer.color(a,e,b,this)))},"fill-opacitySetter":function(a,b,e){F(this.renderer.prepVML(["\x3c",b.split("-")[0],' opacity\x3d"',a,'"/\x3e']),null,null,e)},opacitySetter:A,rotationSetter:function(a,
b,e){e=e.style;this[b]=e[b]=a;e.left=-Math.round(Math.sin(a*d)+1)+"px";e.top=Math.round(Math.cos(a*d))+"px"},strokeSetter:function(a,b,e){this.setAttr("strokecolor",this.renderer.color(a,e,b,this))},"stroke-widthSetter":function(a,b,e){e.stroked=!!a;this[b]=a;n(a)&&(a+="px");this.setAttr("strokeweight",a)},titleSetter:function(a,b){this.setAttr(b,a)},visibilitySetter:function(a,b,e){"inherit"===a&&(a="visible");this.shadows&&l(this.shadows,function(c){c.style[b]=a});"DIV"===e.nodeName&&(a="hidden"===
a?"-999em":0,this.docMode8||(e.style[b]=a?"visible":"hidden"),b="top");e.style[b]=a},xSetter:function(a,b,e){this[b]=a;"x"===b?b="left":"y"===b&&(b="top");this.updateClipping?(this[b]=a,this.updateClipping()):e.style[b]=a},zIndexSetter:function(a,b,e){e.style[b]=a}},A["stroke-opacitySetter"]=A["fill-opacitySetter"],a.VMLElement=A=E(h,A),A.prototype.ySetter=A.prototype.widthSetter=A.prototype.heightSetter=A.prototype.xSetter,A={Element:A,isIE8:-1<u.navigator.userAgent.indexOf("MSIE 8.0"),init:function(a,
b,e){var c,h;this.alignedObjects=[];c=this.createElement("div").css({position:"relative"});h=c.element;a.appendChild(c.element);this.isVML=!0;this.box=h;this.boxWrapper=c;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(b,e,!1);if(!v.namespaces.hcv){v.namespaces.add("hcv","urn:schemas-microsoft-com:vml");try{v.createStyleSheet().cssText="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}catch(J){v.styleSheets[0].cssText+=
"hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}}},isHidden:function(){return!this.box.offsetWidth},clipRect:function(a,b,e,h){var c=this.createElement(),q=w(a);return f(c,{members:[],count:0,left:(q?a.x:a)+1,top:(q?a.y:b)+1,width:(q?a.width:e)-1,height:(q?a.height:h)-1,getCSS:function(a){var c=a.element,b=c.nodeName,e=a.inverted,m=this.top-("shape"===b?c.offsetTop:0),z=this.left,c=z+this.width,h=m+this.height,m={clip:"rect("+Math.round(e?
z:m)+"px,"+Math.round(e?h:c)+"px,"+Math.round(e?c:h)+"px,"+Math.round(e?m:z)+"px)"};!e&&a.docMode8&&"DIV"===b&&f(m,{width:c+"px",height:h+"px"});return m},updateClipping:function(){l(c.members,function(a){a.element&&a.css(c.getCSS(a))})}})},color:function(c,b,e,h){var q=this,k,f=/^rgba/,n,u,x="none";c&&c.linearGradient?u="gradient":c&&c.radialGradient&&(u="pattern");if(u){var m,z,d=c.linearGradient||c.radialGradient,t,y,B,C,g,r="";c=c.stops;var w,v=[],K=function(){n=['\x3cfill colors\x3d"'+v.join(",")+
'" opacity\x3d"',B,'" o:opacity2\x3d"',y,'" type\x3d"',u,'" ',r,'focus\x3d"100%" method\x3d"any" /\x3e'];F(q.prepVML(n),null,null,b)};t=c[0];w=c[c.length-1];0<t[0]&&c.unshift([0,t[1]]);1>w[0]&&c.push([1,w[1]]);l(c,function(c,b){f.test(c[1])?(k=a.color(c[1]),m=k.get("rgb"),z=k.get("a")):(m=c[1],z=1);v.push(100*c[0]+"% "+m);b?(B=z,C=m):(y=z,g=m)});if("fill"===e)if("gradient"===u)e=d.x1||d[0]||0,c=d.y1||d[1]||0,t=d.x2||d[2]||0,d=d.y2||d[3]||0,r='angle\x3d"'+(90-180*Math.atan((d-c)/(t-e))/Math.PI)+'"',
K();else{var x=d.r,p=2*x,A=2*x,E=d.cx,H=d.cy,U=b.radialReference,T,x=function(){U&&(T=h.getBBox(),E+=(U[0]-T.x)/T.width-.5,H+=(U[1]-T.y)/T.height-.5,p*=U[2]/T.width,A*=U[2]/T.height);r='src\x3d"'+a.getOptions().global.VMLRadialGradientURL+'" size\x3d"'+p+","+A+'" origin\x3d"0.5,0.5" position\x3d"'+E+","+H+'" color2\x3d"'+g+'" ';K()};h.added?x():h.onAdd=x;x=C}else x=m}else f.test(c)&&"IMG"!==b.tagName?(k=a.color(c),h[e+"-opacitySetter"](k.get("a"),e,b),x=k.get("rgb")):(x=b.getElementsByTagName(e),
x.length&&(x[0].opacity=1,x[0].type="solid"),x=c);return x},prepVML:function(a){var c=this.isIE8;a=a.join("");c?(a=a.replace("/\x3e",' xmlns\x3d"urn:schemas-microsoft-com:vml" /\x3e'),a=-1===a.indexOf('style\x3d"')?a.replace("/\x3e",' style\x3d"display:inline-block;behavior:url(#default#VML);" /\x3e'):a.replace('style\x3d"','style\x3d"display:inline-block;behavior:url(#default#VML);')):a=a.replace("\x3c","\x3chcv:");return a},text:C.prototype.html,path:function(a){var c={coordsize:"10 10"};b(a)?c.d=
a:w(a)&&f(c,a);return this.createElement("shape").attr(c)},circle:function(a,b,e){var c=this.symbol("circle");w(a)&&(e=a.r,b=a.y,a=a.x);c.isCircle=!0;c.r=e;return c.attr({x:a,y:b})},g:function(a){var b;a&&(b={className:"highcharts-"+a,"class":"highcharts-"+a});return this.createElement("div").attr(b)},image:function(a,b,e,h,k){var c=this.createElement("img").attr({src:a});1<arguments.length&&c.attr({x:b,y:e,width:h,height:k});return c},createElement:function(a){return"rect"===a?this.symbol(a):C.prototype.createElement.call(this,
a)},invertChild:function(a,b){var c=this;b=b.style;var h="IMG"===a.tagName&&a.style;H(a,{flip:"x",left:e(b.width)-(h?e(h.top):1),top:e(b.height)-(h?e(h.left):1),rotation:-90});l(a.childNodes,function(b){c.invertChild(b,a)})},symbols:{arc:function(a,b,e,h,k){var c=k.start,f=k.end,q=k.r||e||h;e=k.innerR;h=Math.cos(c);var n=Math.sin(c),u=Math.cos(f),m=Math.sin(f);if(0===f-c)return["x"];c=["wa",a-q,b-q,a+q,b+q,a+q*h,b+q*n,a+q*u,b+q*m];k.open&&!e&&c.push("e","M",a,b);c.push("at",a-e,b-e,a+e,b+e,a+e*u,
b+e*m,a+e*h,b+e*n,"x","e");c.isArc=!0;return c},circle:function(a,b,e,h,k){k&&p(k.r)&&(e=h=2*k.r);k&&k.isCircle&&(a-=e/2,b-=h/2);return["wa",a,b,a+e,b+h,a+e,b+h/2,a+e,b+h/2,"e"]},rect:function(a,b,e,h,k){return C.prototype.symbols[p(k)&&k.r?"callout":"square"].call(0,a,b,e,h,k)}}},a.VMLRenderer=E=function(){this.init.apply(this,arguments)},E.prototype=t(C.prototype,A),a.Renderer=E);C.prototype.measureSpanWidth=function(a,b){var c=v.createElement("span");a=v.createTextNode(a);c.appendChild(a);H(c,
b);this.box.appendChild(c);b=c.offsetWidth;g(c);return b}})(M);(function(a){function E(){var v=a.defaultOptions.global,l,r=v.useUTC,f=r?"getUTC":"get",b=r?"setUTC":"set";a.Date=l=v.Date||g.Date;l.hcTimezoneOffset=r&&v.timezoneOffset;l.hcGetTimezoneOffset=r&&v.getTimezoneOffset;l.hcMakeTime=function(a,b,f,k,e,h){var n;r?(n=l.UTC.apply(0,arguments),n+=H(n)):n=(new l(a,b,d(f,1),d(k,0),d(e,0),d(h,0))).getTime();return n};F("Minutes Hours Day Date Month FullYear".split(" "),function(a){l["hcGet"+a]=f+
a});F("Milliseconds Seconds Minutes Hours Date Month FullYear".split(" "),function(a){l["hcSet"+a]=b+a})}var A=a.color,F=a.each,H=a.getTZOffset,p=a.merge,d=a.pick,g=a.win;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0,VMLRadialGradientURL:"http://code.highcharts.com/5.0.5/gfx/vml-radial-gradient.png"},chart:{borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:20},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",
backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold"},itemHoverStyle:{color:"#000000"},
itemHiddenStyle:{color:"#cccccc"},shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",
minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:A("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,style:{color:"#333333",cursor:"default",
fontSize:"12px",pointerEvents:"none",whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(d){a.defaultOptions=p(!0,a.defaultOptions,d);E();return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;E()})(M);(function(a){var E=a.arrayMax,A=a.arrayMin,F=a.defined,
H=a.destroyObjectProperties,p=a.each,d=a.erase,g=a.merge,v=a.pick;a.PlotLineOrBand=function(a,d){this.axis=a;d&&(this.options=d,this.id=d.id)};a.PlotLineOrBand.prototype={render:function(){var a=this,d=a.axis,f=d.horiz,b=a.options,n=b.label,w=a.label,t=b.to,k=b.from,e=b.value,h=F(k)&&F(t),C=F(e),u=a.svgElem,c=!u,q=[],x,K=b.color,I=v(b.zIndex,0),p=b.events,q={"class":"highcharts-plot-"+(h?"band ":"line ")+(b.className||"")},D={},G=d.chart.renderer,L=h?"bands":"lines",N=d.log2lin;d.isLog&&(k=N(k),t=
N(t),e=N(e));C?(q={stroke:K,"stroke-width":b.width},b.dashStyle&&(q.dashstyle=b.dashStyle)):h&&(K&&(q.fill=K),b.borderWidth&&(q.stroke=b.borderColor,q["stroke-width"]=b.borderWidth));D.zIndex=I;L+="-"+I;(K=d[L])||(d[L]=K=G.g("plot-"+L).attr(D).add());c&&(a.svgElem=u=G.path().attr(q).add(K));if(C)q=d.getPlotLinePath(e,u.strokeWidth());else if(h)q=d.getPlotBandPath(k,t,b);else return;if(c&&q&&q.length){if(u.attr({d:q}),p)for(x in b=function(b){u.on(b,function(c){p[b].apply(a,[c])})},p)b(x)}else u&&
(q?(u.show(),u.animate({d:q})):(u.hide(),w&&(a.label=w=w.destroy())));n&&F(n.text)&&q&&q.length&&0<d.width&&0<d.height&&!q.flat?(n=g({align:f&&h&&"center",x:f?!h&&4:10,verticalAlign:!f&&h&&"middle",y:f?h?16:10:h?6:-4,rotation:f&&!h&&90},n),this.renderLabel(n,q,h,I)):w&&w.hide();return a},renderLabel:function(a,d,f,b){var n=this.label,l=this.axis.chart.renderer;n||(n={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(f?"band":"line")+"-label "+(a.className||"")},n.zIndex=b,
this.label=n=l.text(a.text,0,0,a.useHTML).attr(n).add(),n.css(a.style));b=[d[1],d[4],f?d[6]:d[1]];d=[d[2],d[5],f?d[7]:d[2]];f=A(b);l=A(d);n.align(a,!1,{x:f,y:l,width:E(b)-f,height:E(d)-l});n.show()},destroy:function(){d(this.axis.plotLinesAndBands,this);delete this.axis;H(this)}};a.AxisPlotLineOrBandExtension={getPlotBandPath:function(a,d){d=this.getPlotLinePath(d,null,null,!0);(a=this.getPlotLinePath(a,null,null,!0))&&d?(a.flat=a.toString()===d.toString(),a.push(d[4],d[5],d[1],d[2],"z")):a=null;
return a},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(d,g){var f=(new a.PlotLineOrBand(this,d)).render(),b=this.userOptions;f&&(g&&(b[g]=b[g]||[],b[g].push(d)),this.plotLinesAndBands.push(f));return f},removePlotBandOrLine:function(a){for(var g=this.plotLinesAndBands,f=this.options,b=this.userOptions,n=g.length;n--;)g[n].id===a&&g[n].destroy();p([f.plotLines||[],b.plotLines||
[],f.plotBands||[],b.plotBands||[]],function(b){for(n=b.length;n--;)b[n].id===a&&d(b,b[n])})}}})(M);(function(a){var E=a.correctFloat,A=a.defined,F=a.destroyObjectProperties,H=a.isNumber,p=a.merge,d=a.pick,g=a.deg2rad;a.Tick=function(a,d,g,f){this.axis=a;this.pos=d;this.type=g||"";this.isNew=!0;g||f||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,g=a.options,r=a.chart,f=a.categories,b=a.names,n=this.pos,w=g.labels,t=a.tickPositions,k=n===t[0],e=n===t[t.length-1],b=f?d(f[n],
b[n],n):n,f=this.label,t=t.info,h;a.isDatetimeAxis&&t&&(h=g.dateTimeLabelFormats[t.higherRanks[n]||t.unitName]);this.isFirst=k;this.isLast=e;g=a.labelFormatter.call({axis:a,chart:r,isFirst:k,isLast:e,dateTimeLabelFormat:h,value:a.isLog?E(a.lin2log(b)):b});A(f)?f&&f.attr({text:g}):(this.labelLength=(this.label=f=A(g)&&w.enabled?r.renderer.text(g,0,0,w.useHTML).css(p(w.style)).add(a.labelGroup):null)&&f.getBBox().width,this.rotation=0)},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?
"height":"width"]:0},handleOverflow:function(a){var l=this.axis,r=a.x,f=l.chart.chartWidth,b=l.chart.spacing,n=d(l.labelLeft,Math.min(l.pos,b[3])),b=d(l.labelRight,Math.max(l.pos+l.len,f-b[1])),w=this.label,t=this.rotation,k={left:0,center:.5,right:1}[l.labelAlign],e=w.getBBox().width,h=l.getSlotWidth(),C=h,u=1,c,q={};if(t)0>t&&r-k*e<n?c=Math.round(r/Math.cos(t*g)-n):0<t&&r+k*e>b&&(c=Math.round((f-r)/Math.cos(t*g)));else if(f=r+(1-k)*e,r-k*e<n?C=a.x+C*(1-k)-n:f>b&&(C=b-a.x+C*k,u=-1),C=Math.min(h,
C),C<h&&"center"===l.labelAlign&&(a.x+=u*(h-C-k*(h-Math.min(e,C)))),e>C||l.autoRotation&&(w.styles||{}).width)c=C;c&&(q.width=c,(l.options.labels.style||{}).textOverflow||(q.textOverflow="ellipsis"),w.css(q))},getPosition:function(a,d,g,f){var b=this.axis,n=b.chart,l=f&&n.oldChartHeight||n.chartHeight;return{x:a?b.translate(d+g,null,null,f)+b.transB:b.left+b.offset+(b.opposite?(f&&n.oldChartWidth||n.chartWidth)-b.right-b.left:0),y:a?l-b.bottom+b.offset-(b.opposite?b.height:0):l-b.translate(d+g,null,
null,f)-b.transB}},getLabelPosition:function(a,d,r,f,b,n,w,t){var k=this.axis,e=k.transA,h=k.reversed,C=k.staggerLines,u=k.tickRotCorr||{x:0,y:0},c=b.y;A(c)||(c=0===k.side?r.rotation?-8:-r.getBBox().height:2===k.side?u.y+8:Math.cos(r.rotation*g)*(u.y-r.getBBox(!1,0).height/2));a=a+b.x+u.x-(n&&f?n*e*(h?-1:1):0);d=d+c-(n&&!f?n*e*(h?1:-1):0);C&&(r=w/(t||1)%C,k.opposite&&(r=C-r-1),d+=k.labelOffset/C*r);return{x:a,y:Math.round(d)}},getMarkPath:function(a,d,g,f,b,n){return n.crispLine(["M",a,d,"L",a+(b?
0:-g),d+(b?g:0)],f)},render:function(a,g,r){var f=this.axis,b=f.options,n=f.chart.renderer,l=f.horiz,t=this.type,k=this.label,e=this.pos,h=b.labels,C=this.gridLine,u=t?t+"Tick":"tick",c=f.tickSize(u),q=this.mark,x=!q,K=h.step,I={},p=!0,D=f.tickmarkOffset,G=this.getPosition(l,e,D,g),L=G.x,G=G.y,v=l&&L===f.pos+f.len||!l&&G===f.pos?-1:1,m=t?t+"Grid":"grid",z=b[m+"LineWidth"],O=b[m+"LineColor"],P=b[m+"LineDashStyle"],m=d(b[u+"Width"],!t&&f.isXAxis?1:0),u=b[u+"Color"];r=d(r,1);this.isActive=!0;C||(I.stroke=
O,I["stroke-width"]=z,P&&(I.dashstyle=P),t||(I.zIndex=1),g&&(I.opacity=0),this.gridLine=C=n.path().attr(I).addClass("highcharts-"+(t?t+"-":"")+"grid-line").add(f.gridGroup));if(!g&&C&&(e=f.getPlotLinePath(e+D,C.strokeWidth()*v,g,!0)))C[this.isNew?"attr":"animate"]({d:e,opacity:r});c&&(f.opposite&&(c[0]=-c[0]),x&&(this.mark=q=n.path().addClass("highcharts-"+(t?t+"-":"")+"tick").add(f.axisGroup),q.attr({stroke:u,"stroke-width":m})),q[x?"attr":"animate"]({d:this.getMarkPath(L,G,c[0],q.strokeWidth()*
v,l,n),opacity:r}));k&&H(L)&&(k.xy=G=this.getLabelPosition(L,G,k,l,h,D,a,K),this.isFirst&&!this.isLast&&!d(b.showFirstLabel,1)||this.isLast&&!this.isFirst&&!d(b.showLastLabel,1)?p=!1:!l||f.isRadial||h.step||h.rotation||g||0===r||this.handleOverflow(G),K&&a%K&&(p=!1),p&&H(G.y)?(G.opacity=r,k[this.isNew?"attr":"animate"](G)):k.attr("y",-9999),this.isNew=!1)},destroy:function(){F(this,this.axis)}}})(M);(function(a){var E=a.addEvent,A=a.animObject,F=a.arrayMax,H=a.arrayMin,p=a.AxisPlotLineOrBandExtension,
d=a.color,g=a.correctFloat,v=a.defaultOptions,l=a.defined,r=a.deg2rad,f=a.destroyObjectProperties,b=a.each,n=a.error,w=a.extend,t=a.fireEvent,k=a.format,e=a.getMagnitude,h=a.grep,C=a.inArray,u=a.isArray,c=a.isNumber,q=a.isString,x=a.merge,K=a.normalizeTickInterval,I=a.pick,J=a.PlotLineOrBand,D=a.removeEvent,G=a.splat,L=a.syncTimeout,N=a.Tick;a.Axis=function(){this.init.apply(this,arguments)};a.Axis.prototype={defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",
hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},x:0},minPadding:.01,maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",startOfWeek:1,startOnTick:!1,tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",lineColor:"#ccd6eb",
lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{enabled:!1,formatter:function(){return a.numberFormat(this.total,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15},
title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},init:function(a,b){var c=b.isX;this.chart=a;this.horiz=a.inverted?!c:c;this.isXAxis=c;this.coll=this.coll||(c?"xAxis":"yAxis");this.opposite=b.opposite;this.side=b.side||(this.horiz?this.opposite?0:2:this.opposite?1:3);this.setOptions(b);var e=this.options,m=e.type;this.labelFormatter=e.labels.formatter||this.defaultLabelFormatter;
this.userOptions=b;this.minPixelPadding=0;this.reversed=e.reversed;this.visible=!1!==e.visible;this.zoomEnabled=!1!==e.zoomEnabled;this.hasNames="category"===m||!0===e.categories;this.categories=e.categories||this.hasNames;this.names=this.names||[];this.isLog="logarithmic"===m;this.isDatetimeAxis="datetime"===m;this.isLinked=l(e.linkedTo);this.ticks={};this.labelEdge=[];this.minorTicks={};this.plotLinesAndBands=[];this.alternateBands={};this.len=0;this.minRange=this.userMinRange=e.minRange||e.maxZoom;
this.range=e.range;this.offset=e.offset||0;this.stacks={};this.oldStacks={};this.stacksTouched=0;this.min=this.max=null;this.crosshair=I(e.crosshair,G(a.options.tooltip.crosshairs)[c?0:1],!1);var z;b=this.options.events;-1===C(this,a.axes)&&(c?a.axes.splice(a.xAxis.length,0,this):a.axes.push(this),a[this.coll].push(this));this.series=this.series||[];a.inverted&&c&&void 0===this.reversed&&(this.reversed=!0);this.removePlotLine=this.removePlotBand=this.removePlotBandOrLine;for(z in b)E(this,z,b[z]);
this.isLog&&(this.val2lin=this.log2lin,this.lin2val=this.lin2log)},setOptions:function(a){this.options=x(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],x(v[this.coll],a))},defaultLabelFormatter:function(){var b=this.axis,c=this.value,e=b.categories,h=this.dateTimeLabelFormat,f=v.lang,B=f.numericSymbols,f=f.numericSymbolMagnitude||1E3,q=B&&B.length,d,n=b.options.labels.format,
b=b.isLog?c:b.tickInterval;if(n)d=k(n,this);else if(e)d=c;else if(h)d=a.dateFormat(h,c);else if(q&&1E3<=b)for(;q--&&void 0===d;)e=Math.pow(f,q+1),b>=e&&0===10*c%e&&null!==B[q]&&0!==c&&(d=a.numberFormat(c/e,-1)+B[q]);void 0===d&&(d=1E4<=Math.abs(c)?a.numberFormat(c,-1):a.numberFormat(c,-1,void 0,""));return d},getSeriesExtremes:function(){var a=this,e=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();b(a.series,function(b){if(b.visible||
!e.options.chart.ignoreHiddenSeries){var m=b.options,z=m.threshold,k;a.hasVisibleSeries=!0;a.isLog&&0>=z&&(z=null);if(a.isXAxis)m=b.xData,m.length&&(b=H(m),c(b)||b instanceof Date||(m=h(m,function(a){return c(a)}),b=H(m)),a.dataMin=Math.min(I(a.dataMin,m[0]),b),a.dataMax=Math.max(I(a.dataMax,m[0]),F(m)));else if(b.getExtremes(),k=b.dataMax,b=b.dataMin,l(b)&&l(k)&&(a.dataMin=Math.min(I(a.dataMin,b),b),a.dataMax=Math.max(I(a.dataMax,k),k)),l(z)&&(a.threshold=z),!m.softThreshold||a.isLog)a.softThreshold=
!1}})},translate:function(a,b,e,h,k,B){var m=this.linkedParent||this,z=1,f=0,q=h?m.oldTransA:m.transA;h=h?m.oldMin:m.min;var d=m.minPixelPadding;k=(m.isOrdinal||m.isBroken||m.isLog&&k)&&m.lin2val;q||(q=m.transA);e&&(z*=-1,f=m.len);m.reversed&&(z*=-1,f-=z*(m.sector||m.len));b?(a=(a*z+f-d)/q+h,k&&(a=m.lin2val(a))):(k&&(a=m.val2lin(a)),a=z*(a-h)*q+f+z*d+(c(B)?q*B:0));return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-
(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,b,e,h,k){var m=this.chart,z=this.left,f=this.top,q,d,n=e&&m.oldChartHeight||m.chartHeight,u=e&&m.oldChartWidth||m.chartWidth,g;q=this.transB;var t=function(a,b,c){if(a<b||a>c)h?a=Math.min(Math.max(b,a),c):g=!0;return a};k=I(k,this.translate(a,null,null,e));a=e=Math.round(k+q);q=d=Math.round(n-k-q);c(k)?this.horiz?(q=f,d=n-this.bottom,a=e=t(a,z,z+this.width)):(a=z,e=u-this.right,q=d=t(q,f,f+this.height)):g=!0;return g&&!h?null:m.renderer.crispLine(["M",
a,q,"L",e,d],b||1)},getLinearTickPositions:function(a,b,e){var m,z=g(Math.floor(b/a)*a),h=g(Math.ceil(e/a)*a),k=[];if(b===e&&c(b))return[b];for(b=z;b<=h;){k.push(b);b=g(b+a);if(b===m)break;m=b}return k},getMinorTickPositions:function(){var a=this.options,b=this.tickPositions,c=this.minorTickInterval,e=[],h,k=this.pointRangePadding||0;h=this.min-k;var k=this.max+k,f=k-h;if(f&&f/c<this.len/3)if(this.isLog)for(k=b.length,h=1;h<k;h++)e=e.concat(this.getLogTickPositions(c,b[h-1],b[h],!0));else if(this.isDatetimeAxis&&
"auto"===a.minorTickInterval)e=e.concat(this.getTimeTicks(this.normalizeTimeTickInterval(c),h,k,a.startOfWeek));else for(b=h+(b[0]-h)%c;b<=k&&b!==e[0];b+=c)e.push(b);0!==e.length&&this.trimTicks(e,a.startOnTick,a.endOnTick);return e},adjustForMinRange:function(){var a=this.options,c=this.min,e=this.max,h,k=this.dataMax-this.dataMin>=this.minRange,f,q,d,n,u,g;this.isXAxis&&void 0===this.minRange&&!this.isLog&&(l(a.min)||l(a.max)?this.minRange=null:(b(this.series,function(a){n=a.xData;for(q=u=a.xIncrement?
1:n.length-1;0<q;q--)if(d=n[q]-n[q-1],void 0===f||d<f)f=d}),this.minRange=Math.min(5*f,this.dataMax-this.dataMin)));e-c<this.minRange&&(g=this.minRange,h=(g-e+c)/2,h=[c-h,I(a.min,c-h)],k&&(h[2]=this.isLog?this.log2lin(this.dataMin):this.dataMin),c=F(h),e=[c+g,I(a.max,c+g)],k&&(e[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),e=H(e),e-c<g&&(h[0]=e-g,h[1]=I(a.min,e-g),c=F(h)));this.min=c;this.max=e},getClosest:function(){var a;this.categories?a=1:b(this.series,function(b){var c=b.closestPointRange,
e=b.visible||!b.chart.options.chart.ignoreHiddenSeries;!b.noSharedTooltip&&l(c)&&e&&(a=l(a)?Math.min(a,c):c)});return a},nameToX:function(a){var b=u(this.categories),c=b?this.categories:this.names,e=a.options.x,m;a.series.requireSorting=!1;l(e)||(e=!1===this.options.uniqueNames?a.series.autoIncrement():C(a.name,c));-1===e?b||(m=c.length):m=e;this.names[m]=a.name;return m},updateNames:function(){var a=this;0<this.names.length&&(this.names.length=0,this.minRange=void 0,b(this.series||[],function(c){c.xIncrement=
null;if(!c.points||c.isDirtyData)c.processData(),c.generatePoints();b(c.points,function(b,e){var m;b.options&&void 0===b.options.x&&(m=a.nameToX(b),m!==b.x&&(b.x=m,c.xData[e]=m))})}))},setAxisTranslation:function(a){var c=this,e=c.max-c.min,m=c.axisPointRange||0,h,k=0,f=0,d=c.linkedParent,n=!!c.categories,u=c.transA,g=c.isXAxis;if(g||n||m)h=c.getClosest(),d?(k=d.minPointOffset,f=d.pointRangePadding):b(c.series,function(a){var b=n?1:g?I(a.options.pointRange,h,0):c.axisPointRange||0;a=a.options.pointPlacement;
m=Math.max(m,b);c.single||(k=Math.max(k,q(a)?0:b/2),f=Math.max(f,"on"===a?0:b))}),d=c.ordinalSlope&&h?c.ordinalSlope/h:1,c.minPointOffset=k*=d,c.pointRangePadding=f*=d,c.pointRange=Math.min(m,e),g&&(c.closestPointRange=h);a&&(c.oldTransA=u);c.translationSlope=c.transA=u=c.len/(e+f||1);c.transB=c.horiz?c.left:c.bottom;c.minPixelPadding=u*k},minFromRange:function(){return this.max-this.range},setTickInterval:function(a){var m=this,h=m.chart,k=m.options,f=m.isLog,q=m.log2lin,d=m.isDatetimeAxis,u=m.isXAxis,
D=m.isLinked,x=k.maxPadding,C=k.minPadding,G=k.tickInterval,r=k.tickPixelInterval,L=m.categories,w=m.threshold,p=m.softThreshold,v,N,J,A;d||L||D||this.getTickAmount();J=I(m.userMin,k.min);A=I(m.userMax,k.max);D?(m.linkedParent=h[m.coll][k.linkedTo],h=m.linkedParent.getExtremes(),m.min=I(h.min,h.dataMin),m.max=I(h.max,h.dataMax),k.type!==m.linkedParent.options.type&&n(11,1)):(!p&&l(w)&&(m.dataMin>=w?(v=w,C=0):m.dataMax<=w&&(N=w,x=0)),m.min=I(J,v,m.dataMin),m.max=I(A,N,m.dataMax));f&&(!a&&0>=Math.min(m.min,
I(m.dataMin,m.min))&&n(10,1),m.min=g(q(m.min),15),m.max=g(q(m.max),15));m.range&&l(m.max)&&(m.userMin=m.min=J=Math.max(m.min,m.minFromRange()),m.userMax=A=m.max,m.range=null);t(m,"foundExtremes");m.beforePadding&&m.beforePadding();m.adjustForMinRange();!(L||m.axisPointRange||m.usePercentage||D)&&l(m.min)&&l(m.max)&&(q=m.max-m.min)&&(!l(J)&&C&&(m.min-=q*C),!l(A)&&x&&(m.max+=q*x));c(k.floor)?m.min=Math.max(m.min,k.floor):c(k.softMin)&&(m.min=Math.min(m.min,k.softMin));c(k.ceiling)?m.max=Math.min(m.max,
k.ceiling):c(k.softMax)&&(m.max=Math.max(m.max,k.softMax));p&&l(m.dataMin)&&(w=w||0,!l(J)&&m.min<w&&m.dataMin>=w?m.min=w:!l(A)&&m.max>w&&m.dataMax<=w&&(m.max=w));m.tickInterval=m.min===m.max||void 0===m.min||void 0===m.max?1:D&&!G&&r===m.linkedParent.options.tickPixelInterval?G=m.linkedParent.tickInterval:I(G,this.tickAmount?(m.max-m.min)/Math.max(this.tickAmount-1,1):void 0,L?1:(m.max-m.min)*r/Math.max(m.len,r));u&&!a&&b(m.series,function(a){a.processData(m.min!==m.oldMin||m.max!==m.oldMax)});m.setAxisTranslation(!0);
m.beforeSetTickPositions&&m.beforeSetTickPositions();m.postProcessTickInterval&&(m.tickInterval=m.postProcessTickInterval(m.tickInterval));m.pointRange&&!G&&(m.tickInterval=Math.max(m.pointRange,m.tickInterval));a=I(k.minTickInterval,m.isDatetimeAxis&&m.closestPointRange);!G&&m.tickInterval<a&&(m.tickInterval=a);d||f||G||(m.tickInterval=K(m.tickInterval,null,e(m.tickInterval),I(k.allowDecimals,!(.5<m.tickInterval&&5>m.tickInterval&&1E3<m.max&&9999>m.max)),!!this.tickAmount));this.tickAmount||(m.tickInterval=
m.unsquish());this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions,e=a.tickPositioner,h=a.startOnTick,k=a.endOnTick,f;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===a.minorTickInterval&&this.tickInterval?this.tickInterval/5:a.minorTickInterval;this.tickPositions=b=c&&c.slice();!b&&(b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),
this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),b.length>this.len&&(b=[b[0],b.pop()]),this.tickPositions=b,e&&(e=e.apply(this,[this.min,this.max])))&&(this.tickPositions=b=e);this.isLinked||(this.trimTicks(b,h,k),this.min===this.max&&l(this.min)&&!this.tickAmount&&(f=!0,this.min-=.5,this.max+=.5),this.single=f,c||e||this.adjustTickAmount())},
trimTicks:function(a,b,c){var e=a[0],m=a[a.length-1],h=this.minPointOffset||0;if(b)this.min=e;else for(;this.min-h>a[0];)a.shift();if(c)this.max=m;else for(;this.max+h<a[a.length-1];)a.pop();0===a.length&&l(e)&&a.push((m+e)/2)},alignToOthers:function(){var a={},c,e=this.options;!1!==this.chart.options.chart.alignTicks&&!1!==e.alignTicks&&b(this.chart[this.coll],function(b){var e=b.options,e=[b.horiz?e.left:e.top,e.width,e.height,e.pane].join();b.series.length&&(a[e]?c=!0:a[e]=1)});return c},getTickAmount:function(){var a=
this.options,b=a.tickAmount,c=a.tickPixelInterval;!l(a.tickInterval)&&this.len<c&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(b=2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/c)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=this.tickInterval,b=this.tickPositions,c=this.tickAmount,e=this.finalTickAmt,h=b&&b.length;if(h<c){for(;b.length<c;)b.push(g(b[b.length-1]+a));this.transA*=(h-1)/(c-1);this.max=b[b.length-1]}else h>c&&(this.tickInterval*=
2,this.setTickPositions());if(l(e)){for(a=c=b.length;a--;)(3===e&&1===a%2||2>=e&&0<a&&a<c-1)&&b.splice(a,1);this.finalTickAmt=void 0}},setScale:function(){var a,c;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();c=this.len!==this.oldAxisLength;b(this.series,function(b){if(b.isDirtyData||b.isDirty||b.xAxis.isDirty)a=!0});c||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&
this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||(this.isDirty=c||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks()},setExtremes:function(a,c,e,h,k){var m=this,f=m.chart;e=I(e,!0);b(m.series,function(a){delete a.kdTree});k=w(k,{min:a,max:c});t(m,"setExtremes",k,function(){m.userMin=a;m.userMax=c;m.eventArgs=k;e&&f.redraw(h)})},zoom:function(a,b){var c=this.dataMin,
e=this.dataMax,m=this.options,h=Math.min(c,I(m.min,c)),m=Math.max(e,I(m.max,e));if(a!==this.min||b!==this.max)this.allowZoomOutside||(l(c)&&(a<h&&(a=h),a>m&&(a=m)),l(e)&&(b<h&&(b=h),b>m&&(b=m))),this.displayBtn=void 0!==a||void 0!==b,this.setExtremes(a,b,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var a=this.chart,b=this.options,c=b.offsetLeft||0,e=this.horiz,h=I(b.width,a.plotWidth-c+(b.offsetRight||0)),k=I(b.height,a.plotHeight),f=I(b.top,a.plotTop),b=I(b.left,a.plotLeft+c),c=/%$/;
c.test(k)&&(k=Math.round(parseFloat(k)/100*a.plotHeight));c.test(f)&&(f=Math.round(parseFloat(f)/100*a.plotHeight+a.plotTop));this.left=b;this.top=f;this.width=h;this.height=k;this.bottom=a.chartHeight-k-f;this.right=a.chartWidth-h-b;this.len=Math.max(e?h:k,0);this.pos=e?b:f},getExtremes:function(){var a=this.isLog,b=this.lin2log;return{min:a?g(b(this.min)):this.min,max:a?g(b(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=
this.isLog,c=this.lin2log,e=b?c(this.min):this.min,b=b?c(this.max):this.max;null===a?a=e:e>a?a=e:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(I(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var b=this.options,c=b[a+"Length"],e=I(b[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(e&&c)return"inside"===b[a+"Position"]&&(c=-c),[c,e]},labelMetrics:function(){return this.chart.renderer.fontMetrics(this.options.labels.style&&
this.options.labels.style.fontSize,this.ticks[0]&&this.ticks[0].label)},unsquish:function(){var a=this.options.labels,c=this.horiz,e=this.tickInterval,h=e,k=this.len/(((this.categories?1:0)+this.max-this.min)/e),f,q=a.rotation,d=this.labelMetrics(),n,u=Number.MAX_VALUE,g,t=function(a){a/=k||1;a=1<a?Math.ceil(a):1;return a*e};c?(g=!a.staggerLines&&!a.step&&(l(q)?[q]:k<I(a.autoRotationLimit,80)&&a.autoRotation))&&b(g,function(a){var b;if(a===q||a&&-90<=a&&90>=a)n=t(Math.abs(d.h/Math.sin(r*a))),b=n+
Math.abs(a/360),b<u&&(u=b,f=a,h=n)}):a.step||(h=t(d.h));this.autoRotation=g;this.labelRotation=I(f,q);return h},getSlotWidth:function(){var a=this.chart,b=this.horiz,c=this.options.labels,e=Math.max(this.tickPositions.length-(this.categories?0:1),1),h=a.margin[3];return b&&2>(c.step||0)&&!c.rotation&&(this.staggerLines||1)*a.plotWidth/e||!b&&(h&&h-a.spacing[3]||.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,c=a.renderer,e=this.tickPositions,h=this.ticks,k=this.options.labels,f=this.horiz,
d=this.getSlotWidth(),n=Math.max(1,Math.round(d-2*(k.padding||5))),u={},g=this.labelMetrics(),t=k.style&&k.style.textOverflow,D,C=0,G,l;q(k.rotation)||(u.rotation=k.rotation||0);b(e,function(a){(a=h[a])&&a.labelLength>C&&(C=a.labelLength)});this.maxLabelLength=C;if(this.autoRotation)C>n&&C>g.h?u.rotation=this.labelRotation:this.labelRotation=0;else if(d&&(D={width:n+"px"},!t))for(D.textOverflow="clip",G=e.length;!f&&G--;)if(l=e[G],n=h[l].label)n.styles&&"ellipsis"===n.styles.textOverflow?n.css({textOverflow:"clip"}):
h[l].labelLength>d&&n.css({width:d+"px"}),n.getBBox().height>this.len/e.length-(g.h-g.f)&&(n.specCss={textOverflow:"ellipsis"});u.rotation&&(D={width:(C>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight)+"px"},t||(D.textOverflow="ellipsis"));if(this.labelAlign=k.align||this.autoLabelAlign(this.labelRotation))u.align=this.labelAlign;b(e,function(a){var b=(a=h[a])&&a.label;b&&(b.attr(u),D&&b.css(x(D,b.specCss)),delete b.specCss,a.rotation=u.rotation)});this.tickRotCorr=c.rotCorr(g.b,this.labelRotation||
0,0!==this.side)},hasData:function(){return this.hasVisibleSeries||l(this.min)&&l(this.max)&&!!this.tickPositions},getOffset:function(){var a=this,c=a.chart,e=c.renderer,h=a.options,k=a.tickPositions,f=a.ticks,q=a.horiz,d=a.side,n=c.inverted?[1,0,3,2][d]:d,u,g,t=0,D,x=0,C=h.title,G=h.labels,r=0,L=a.opposite,w=c.axisOffset,c=c.clipOffset,p=[-1,1,1,-1][d],K,v=h.className,J=a.axisParent,A=this.tickSize("tick");u=a.hasData();a.showAxis=g=u||I(h.showEmpty,!0);a.staggerLines=a.horiz&&G.staggerLines;a.axisGroup||
(a.gridGroup=e.g("grid").attr({zIndex:h.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(v||"")).add(J),a.axisGroup=e.g("axis").attr({zIndex:h.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+" "+(v||"")).add(J),a.labelGroup=e.g("axis-labels").attr({zIndex:G.zIndex||7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(v||"")).add(J));if(u||a.isLinked)b(k,function(b){f[b]?f[b].addLabel():f[b]=new N(a,b)}),a.renderUnsquish(),!1===G.reserveSpace||0!==d&&2!==d&&
{1:"left",3:"right"}[d]!==a.labelAlign&&"center"!==a.labelAlign||b(k,function(a){r=Math.max(f[a].getLabelSize(),r)}),a.staggerLines&&(r*=a.staggerLines,a.labelOffset=r*(a.opposite?-1:1));else for(K in f)f[K].destroy(),delete f[K];C&&C.text&&!1!==C.enabled&&(a.axisTitle||((K=C.textAlign)||(K=(q?{low:"left",middle:"center",high:"right"}:{low:L?"right":"left",middle:"center",high:L?"left":"right"})[C.align]),a.axisTitle=e.text(C.text,0,0,C.useHTML).attr({zIndex:7,rotation:C.rotation||0,align:K}).addClass("highcharts-axis-title").css(C.style).add(a.axisGroup),
a.axisTitle.isNew=!0),g&&(t=a.axisTitle.getBBox()[q?"height":"width"],D=C.offset,x=l(D)?0:I(C.margin,q?5:10)),a.axisTitle[g?"show":"hide"](!0));a.renderLine();a.offset=p*I(h.offset,w[d]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};e=0===d?-a.labelMetrics().h:2===d?a.tickRotCorr.y:0;x=Math.abs(r)+x;r&&(x=x-e+p*(q?I(G.y,a.tickRotCorr.y+8*p):G.x));a.axisTitleMargin=I(D,x);w[d]=Math.max(w[d],a.axisTitleMargin+t+p*a.offset,x,u&&k.length&&A?A[0]:0);h=h.offset?0:2*Math.floor(a.axisLine.strokeWidth()/2);c[n]=
Math.max(c[n],h)},getLinePath:function(a){var b=this.chart,c=this.opposite,e=this.offset,m=this.horiz,h=this.left+(c?this.width:0)+e,e=b.chartHeight-this.bottom-(c?this.height:0)+e;c&&(a*=-1);return b.renderer.crispLine(["M",m?this.left:h,m?e:this.top,"L",m?b.chartWidth-this.right:h,m?e:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.axisLine.attr({stroke:this.options.lineColor,
"stroke-width":this.options.lineWidth,zIndex:7}))},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,e=this.len,h=this.options.title,k=a?b:c,f=this.opposite,q=this.offset,d=h.x||0,n=h.y||0,u=this.chart.renderer.fontMetrics(h.style&&h.style.fontSize,this.axisTitle).f,e={low:k+(a?0:e),middle:k+e/2,high:k+(a?e:0)}[h.align],b=(a?c+this.height:b)+(a?1:-1)*(f?-1:1)*this.axisTitleMargin+(2===this.side?u:0);return{x:a?e+d:b+(f?this.width:0)+q+d,y:a?b+n-(f?this.height:0)+q:e+n}},render:function(){var a=
this,e=a.chart,h=e.renderer,k=a.options,f=a.isLog,q=a.lin2log,d=a.isLinked,n=a.tickPositions,u=a.axisTitle,g=a.ticks,t=a.minorTicks,D=a.alternateBands,C=k.stackLabels,x=k.alternateGridColor,G=a.tickmarkOffset,l=a.axisLine,r=e.hasRendered&&c(a.oldMin),w=a.showAxis,I=A(h.globalAnimation),p,K;a.labelEdge.length=0;a.overlap=!1;b([g,t,D],function(a){for(var b in a)a[b].isActive=!1});if(a.hasData()||d)a.minorTickInterval&&!a.categories&&b(a.getMinorTickPositions(),function(b){t[b]||(t[b]=new N(a,b,"minor"));
r&&t[b].isNew&&t[b].render(null,!0);t[b].render(null,!1,1)}),n.length&&(b(n,function(b,c){if(!d||b>=a.min&&b<=a.max)g[b]||(g[b]=new N(a,b)),r&&g[b].isNew&&g[b].render(c,!0,.1),g[b].render(c)}),G&&(0===a.min||a.single)&&(g[-1]||(g[-1]=new N(a,-1,null,!0)),g[-1].render(-1))),x&&b(n,function(b,c){K=void 0!==n[c+1]?n[c+1]+G:a.max-G;0===c%2&&b<a.max&&K<=a.max+(e.polar?-G:G)&&(D[b]||(D[b]=new J(a)),p=b+G,D[b].options={from:f?q(p):p,to:f?q(K):K,color:x},D[b].render(),D[b].isActive=!0)}),a._addedPlotLB||
(b((k.plotLines||[]).concat(k.plotBands||[]),function(b){a.addPlotBandOrLine(b)}),a._addedPlotLB=!0);b([g,t,D],function(a){var b,c,h=[],k=I.duration;for(b in a)a[b].isActive||(a[b].render(b,!1,0),a[b].isActive=!1,h.push(b));L(function(){for(c=h.length;c--;)a[h[c]]&&!a[h[c]].isActive&&(a[h[c]].destroy(),delete a[h[c]])},a!==D&&e.hasRendered&&k?k:0)});l&&(l[l.isPlaced?"animate":"attr"]({d:this.getLinePath(l.strokeWidth())}),l.isPlaced=!0,l[w?"show":"hide"](!0));u&&w&&(u[u.isNew?"attr":"animate"](a.getTitlePosition()),
u.isNew=!1);C&&C.enabled&&a.renderStackTotals();a.isDirty=!1},redraw:function(){this.visible&&(this.render(),b(this.plotLinesAndBands,function(a){a.render()}));b(this.series,function(a){a.isDirty=!0})},keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var c=this,e=c.stacks,h,k=c.plotLinesAndBands,m;a||D(c);for(h in e)f(e[h]),e[h]=null;b([c.ticks,c.minorTicks,c.alternateBands],function(a){f(a)});if(k)for(a=k.length;a--;)k[a].destroy();b("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),
function(a){c[a]&&(c[a]=c[a].destroy())});for(m in c)c.hasOwnProperty(m)&&-1===C(m,c.keepProps)&&delete c[m]},drawCrosshair:function(a,b){var c,e=this.crosshair,h=I(e.snap,!0),k,m=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(l(b)||!h)?(h?l(b)&&(k=this.isXAxis?b.plotX:this.len-b.plotY):k=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),l(k)&&(c=this.getPlotLinePath(b&&(this.isXAxis?b.x:I(b.stackY,b.y)),null,null,null,k)||null),l(c)?(b=this.categories&&!this.isRadial,
m||(this.cross=m=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+(b?"category ":"thin ")+e.className).attr({zIndex:I(e.zIndex,2)}).add(),m.attr({stroke:e.color||(b?d("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":I(e.width,1)}),e.dashStyle&&m.attr({dashstyle:e.dashStyle})),m.show().attr({d:c}),b&&!e.width&&m.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&this.cross.hide()}};
w(a.Axis.prototype,p)})(M);(function(a){var E=a.Axis,A=a.Date,F=a.dateFormat,H=a.defaultOptions,p=a.defined,d=a.each,g=a.extend,v=a.getMagnitude,l=a.getTZOffset,r=a.normalizeTickInterval,f=a.pick,b=a.timeUnits;E.prototype.getTimeTicks=function(a,r,t,k){var e=[],h={},n=H.global.useUTC,u,c=new A(r-l(r)),q=A.hcMakeTime,x=a.unitRange,w=a.count,I;if(p(r)){c[A.hcSetMilliseconds](x>=b.second?0:w*Math.floor(c.getMilliseconds()/w));if(x>=b.second)c[A.hcSetSeconds](x>=b.minute?0:w*Math.floor(c.getSeconds()/
w));if(x>=b.minute)c[A.hcSetMinutes](x>=b.hour?0:w*Math.floor(c[A.hcGetMinutes]()/w));if(x>=b.hour)c[A.hcSetHours](x>=b.day?0:w*Math.floor(c[A.hcGetHours]()/w));if(x>=b.day)c[A.hcSetDate](x>=b.month?1:w*Math.floor(c[A.hcGetDate]()/w));x>=b.month&&(c[A.hcSetMonth](x>=b.year?0:w*Math.floor(c[A.hcGetMonth]()/w)),u=c[A.hcGetFullYear]());if(x>=b.year)c[A.hcSetFullYear](u-u%w);if(x===b.week)c[A.hcSetDate](c[A.hcGetDate]()-c[A.hcGetDay]()+f(k,1));u=c[A.hcGetFullYear]();k=c[A.hcGetMonth]();var v=c[A.hcGetDate](),
D=c[A.hcGetHours]();if(A.hcTimezoneOffset||A.hcGetTimezoneOffset)I=(!n||!!A.hcGetTimezoneOffset)&&(t-r>4*b.month||l(r)!==l(t)),c=c.getTime(),c=new A(c+l(c));n=c.getTime();for(r=1;n<t;)e.push(n),n=x===b.year?q(u+r*w,0):x===b.month?q(u,k+r*w):!I||x!==b.day&&x!==b.week?I&&x===b.hour?q(u,k,v,D+r*w):n+x*w:q(u,k,v+r*w*(x===b.day?1:7)),r++;e.push(n);x<=b.hour&&d(e,function(a){"000000000"===F("%H%M%S%L",a)&&(h[a]="day")})}e.info=g(a,{higherRanks:h,totalRange:x*w});return e};E.prototype.normalizeTimeTickInterval=
function(a,f){var d=f||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];f=d[d.length-1];var k=b[f[0]],e=f[1],h;for(h=0;h<d.length&&!(f=d[h],k=b[f[0]],e=f[1],d[h+1]&&a<=(k*e[e.length-1]+b[d[h+1][0]])/2);h++);k===b.year&&a<5*k&&(e=[1,2,5]);a=r(a/k,e,"year"===f[0]?Math.max(v(a/k),1):1);return{unitRange:k,count:a,unitName:f[0]}}})(M);(function(a){var E=a.Axis,
A=a.getMagnitude,F=a.map,H=a.normalizeTickInterval,p=a.pick;E.prototype.getLogTickPositions=function(a,g,v,l){var d=this.options,f=this.len,b=this.lin2log,n=this.log2lin,w=[];l||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),w=this.getLinearTickPositions(a,g,v);else if(.08<=a)for(var f=Math.floor(g),t,k,e,h,C,d=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];f<v+1&&!C;f++)for(k=d.length,t=0;t<k&&!C;t++)e=n(b(f)*d[t]),e>g&&(!l||h<=v)&&void 0!==h&&w.push(h),h>v&&(C=!0),h=e;else g=b(g),v=
b(v),a=d[l?"minorTickInterval":"tickInterval"],a=p("auto"===a?null:a,this._minorAutoInterval,d.tickPixelInterval/(l?5:1)*(v-g)/((l?f/this.tickPositions.length:f)||1)),a=H(a,null,A(a)),w=F(this.getLinearTickPositions(a,g,v),n),l||(this._minorAutoInterval=a/5);l||(this.tickInterval=a);return w};E.prototype.log2lin=function(a){return Math.log(a)/Math.LN10};E.prototype.lin2log=function(a){return Math.pow(10,a)}})(M);(function(a){var E=a.dateFormat,A=a.each,F=a.extend,H=a.format,p=a.isNumber,d=a.map,g=
a.merge,v=a.pick,l=a.splat,r=a.syncTimeout,f=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,f){this.chart=a;this.options=f;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=f.split&&!a.inverted;this.shared=f.shared||this.split},cleanSplit:function(a){A(this.chart.series,function(b){var f=b&&b.tt;f&&(!f.isActive||a?b.tt=f.destroy():f.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,f=this.options;this.label||(this.split?
this.label=a.g("tooltip"):(this.label=a.label("",0,0,f.shape||"callout",null,null,f.useHTML,null,"tooltip").attr({padding:f.padding,r:f.borderRadius}),this.label.attr({fill:f.backgroundColor,"stroke-width":f.borderWidth}).css(f.style).shadow(f.shadow)),this.label.attr({zIndex:8}).add());return this.label},update:function(a){this.destroy();this.init(this.chart,g(!0,this.options,a))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),
this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,f,d,g){var b=this,e=b.now,h=!1!==b.options.animation&&!b.isHidden&&(1<Math.abs(a-e.x)||1<Math.abs(f-e.y)),n=b.followPointer||1<b.len;F(e,{x:h?(2*e.x+a)/3:a,y:h?(e.y+f)/2:f,anchorX:n?void 0:h?(2*e.anchorX+d)/3:d,anchorY:n?void 0:h?(e.anchorY+g)/2:g});b.getLabel().attr(e);h&&(clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){b&&b.move(a,f,d,g)},32))},hide:function(a){var b=
this;clearTimeout(this.hideTimer);a=v(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=r(function(){b.getLabel()[a?"fadeOut":"hide"]();b.isHidden=!0},a))},getAnchor:function(a,f){var b,n=this.chart,k=n.inverted,e=n.plotTop,h=n.plotLeft,g=0,u=0,c,q;a=l(a);b=a[0].tooltipPos;this.followPointer&&f&&(void 0===f.chartX&&(f=n.pointer.normalize(f)),b=[f.chartX-n.plotLeft,f.chartY-e]);b||(A(a,function(a){c=a.series.yAxis;q=a.series.xAxis;g+=a.plotX+(!k&&q?q.left-h:0);u+=(a.plotLow?(a.plotLow+a.plotHigh)/
2:a.plotY)+(!k&&c?c.top-e:0)}),g/=a.length,u/=a.length,b=[k?n.plotWidth-u:g,this.shared&&!k&&1<a.length&&f?f.chartY-e:k?n.plotHeight-g:u]);return d(b,Math.round)},getPosition:function(a,f,d){var b=this.chart,k=this.distance,e={},h=d.h||0,n,u=["y",b.chartHeight,f,d.plotY+b.plotTop,b.plotTop,b.plotTop+b.plotHeight],c=["x",b.chartWidth,a,d.plotX+b.plotLeft,b.plotLeft,b.plotLeft+b.plotWidth],q=!this.followPointer&&v(d.ttBelow,!b.inverted===!!d.negative),g=function(a,b,c,f,m,d){var n=c<f-k,u=f+k+c<b,g=
f-k-c;f+=k;if(q&&u)e[a]=f;else if(!q&&n)e[a]=g;else if(n)e[a]=Math.min(d-c,0>g-h?g:g-h);else if(u)e[a]=Math.max(m,f+h+c>b?f:f+h);else return!1},l=function(a,b,c,h){var m;h<k||h>b-k?m=!1:e[a]=h<c/2?1:h>b-c/2?b-c-2:h-c/2;return m},r=function(a){var b=u;u=c;c=b;n=a},p=function(){!1!==g.apply(0,u)?!1!==l.apply(0,c)||n||(r(!0),p()):n?e.x=e.y=0:(r(!0),p())};(b.inverted||1<this.len)&&r();p();return e},defaultFormatter:function(a){var b=this.points||l(this),f;f=[a.tooltipFooterHeaderFormatter(b[0])];f=f.concat(a.bodyFormatter(b));
f.push(a.tooltipFooterHeaderFormatter(b[0],!0));return f},refresh:function(a,f){var b=this.chart,d,k=this.options,e,h,n={},u=[];d=k.formatter||this.defaultFormatter;var n=b.hoverPoints,c=this.shared;clearTimeout(this.hideTimer);this.followPointer=l(a)[0].series.tooltipOptions.followPointer;h=this.getAnchor(a,f);f=h[0];e=h[1];!c||a.series&&a.series.noSharedTooltip?n=a.getLabelConfig():(b.hoverPoints=a,n&&A(n,function(a){a.setState()}),A(a,function(a){a.setState("hover");u.push(a.getLabelConfig())}),
n={x:a[0].category,y:a[0].y},n.points=u,this.len=u.length,a=a[0]);n=d.call(n,this);c=a.series;this.distance=v(c.tooltipOptions.distance,16);!1===n?this.hide():(d=this.getLabel(),this.isHidden&&d.attr({opacity:1}).show(),this.split?this.renderSplit(n,b.hoverPoints):(d.attr({text:n&&n.join?n.join(""):n}),d.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+v(a.colorIndex,c.colorIndex)),d.attr({stroke:k.borderColor||a.color||c.color||"#666666"}),this.updatePosition({plotX:f,plotY:e,
negative:a.negative,ttBelow:a.ttBelow,h:h[2]||0})),this.isHidden=!1)},renderSplit:function(b,f){var d=this,n=[],k=this.chart,e=k.renderer,h=!0,g=this.options,u,c=this.getLabel();A(b.slice(0,b.length-1),function(a,b){b=f[b-1]||{isHeader:!0,plotX:f[0].plotX};var q=b.series||d,t=q.tt,x=b.series||{},D="highcharts-color-"+v(b.colorIndex,x.colorIndex,"none");t||(q.tt=t=e.label(null,null,null,"callout").addClass("highcharts-tooltip-box "+D).attr({padding:g.padding,r:g.borderRadius,fill:g.backgroundColor,
stroke:b.color||x.color||"#333333","stroke-width":g.borderWidth}).add(c));t.isActive=!0;t.attr({text:a});t.css(g.style);a=t.getBBox();x=a.width+t.strokeWidth();b.isHeader?(u=a.height,x=Math.max(0,Math.min(b.plotX+k.plotLeft-x/2,k.chartWidth-x))):x=b.plotX+k.plotLeft-v(g.distance,16)-x;0>x&&(h=!1);a=(b.series&&b.series.yAxis&&b.series.yAxis.pos)+(b.plotY||0);a-=k.plotTop;n.push({target:b.isHeader?k.plotHeight+u:a,rank:b.isHeader?1:0,size:q.tt.getBBox().height+1,point:b,x:x,tt:t})});this.cleanSplit();
a.distribute(n,k.plotHeight+u);A(n,function(a){var b=a.point;a.tt.attr({visibility:void 0===a.pos?"hidden":"inherit",x:h||b.isHeader?a.x:b.plotX+k.plotLeft+v(g.distance,16),y:a.pos+k.plotTop,anchorX:b.plotX+k.plotLeft,anchorY:b.isHeader?a.pos+k.plotTop-15:b.plotY+k.plotTop})})},updatePosition:function(a){var b=this.chart,f=this.getLabel(),f=(this.options.positioner||this.getPosition).call(this,f.width,f.height,a);this.move(Math.round(f.x),Math.round(f.y||0),a.plotX+b.plotLeft,a.plotY+b.plotTop)},
getXDateFormat:function(a,d,g){var b;d=d.dateTimeLabelFormats;var k=g&&g.closestPointRange,e,h={millisecond:15,second:12,minute:9,hour:6,day:3},n,u="millisecond";if(k){n=E("%m-%d %H:%M:%S.%L",a.x);for(e in f){if(k===f.week&&+E("%w",a.x)===g.options.startOfWeek&&"00:00:00.000"===n.substr(6)){e="week";break}if(f[e]>k){e=u;break}if(h[e]&&n.substr(h[e])!=="01-01 00:00:00.000".substr(h[e]))break;"week"!==e&&(u=e)}e&&(b=d[e])}else b=d.day;return b||d.year},tooltipFooterHeaderFormatter:function(a,f){var b=
f?"footer":"header";f=a.series;var d=f.tooltipOptions,k=d.xDateFormat,e=f.xAxis,h=e&&"datetime"===e.options.type&&p(a.key),b=d[b+"Format"];h&&!k&&(k=this.getXDateFormat(a,d,e));h&&k&&(b=b.replace("{point.key}","{point.key:"+k+"}"));return H(b,{point:a,series:f})},bodyFormatter:function(a){return d(a,function(a){var b=a.series.tooltipOptions;return(b.pointFormatter||a.point.tooltipFormatter).call(a.point,b.pointFormat)})}}})(M);(function(a){var E=a.addEvent,A=a.attr,F=a.charts,H=a.color,p=a.css,d=
a.defined,g=a.doc,v=a.each,l=a.extend,r=a.fireEvent,f=a.offset,b=a.pick,n=a.removeEvent,w=a.splat,t=a.Tooltip,k=a.win;a.Pointer=function(a,b){this.init(a,b)};a.Pointer.prototype={init:function(a,h){this.options=h;this.chart=a;this.runChartClick=h.chart.events&&!!h.chart.events.click;this.pinchDown=[];this.lastValidTouch={};t&&h.tooltip.enabled&&(a.tooltip=new t(a,h.tooltip),this.followTouchMove=b(h.tooltip.followTouchMove,!0));this.setDOMEvents()},zoomOption:function(a){var e=this.chart,f=e.options.chart,
k=f.zoomType||"",e=e.inverted;/touch/.test(a.type)&&(k=b(f.pinchType,k));this.zoomX=a=/x/.test(k);this.zoomY=k=/y/.test(k);this.zoomHor=a&&!e||k&&e;this.zoomVert=k&&!e||a&&e;this.hasZoom=a||k},normalize:function(a,b){var e,h;a=a||k.event;a.target||(a.target=a.srcElement);h=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;b||(this.chartPosition=b=f(this.chart.container));void 0===h.pageX?(e=Math.max(a.x,a.clientX-b.left),b=a.y):(e=h.pageX-b.left,b=h.pageY-b.top);return l(a,{chartX:Math.round(e),
chartY:Math.round(b)})},getCoordinates:function(a){var b={xAxis:[],yAxis:[]};v(this.chart.axes,function(e){b[e.isXAxis?"xAxis":"yAxis"].push({axis:e,value:e.toValue(a[e.horiz?"chartX":"chartY"])})});return b},runPointActions:function(e){var h=this.chart,f=h.series,k=h.tooltip,c=k?k.shared:!1,d=!0,n=h.hoverPoint,t=h.hoverSeries,l,r,D,G=[],L;if(!c&&!t)for(l=0;l<f.length;l++)if(f[l].directTouch||!f[l].options.stickyTracking)f=[];t&&(c?t.noSharedTooltip:t.directTouch)&&n?G=[n]:(c||!t||t.options.stickyTracking||
(f=[t]),v(f,function(a){r=a.noSharedTooltip&&c;D=!c&&a.directTouch;a.visible&&!r&&!D&&b(a.options.enableMouseTracking,!0)&&(L=a.searchPoint(e,!r&&1===a.kdDimensions))&&L.series&&G.push(L)}),G.sort(function(a,b){var e=a.distX-b.distX,h=a.dist-b.dist,k=b.series.group.zIndex-a.series.group.zIndex;return 0!==e&&c?e:0!==h?h:0!==k?k:a.series.index>b.series.index?-1:1}));if(c)for(l=G.length;l--;)(G[l].x!==G[0].x||G[l].series.noSharedTooltip)&&G.splice(l,1);if(G[0]&&(G[0]!==this.prevKDPoint||k&&k.isHidden)){if(c&&
!G[0].series.noSharedTooltip){for(l=0;l<G.length;l++)G[l].onMouseOver(e,G[l]!==(t&&t.directTouch&&n||G[0]));G.length&&k&&k.refresh(G.sort(function(a,b){return a.series.index-b.series.index}),e)}else if(k&&k.refresh(G[0],e),!t||!t.directTouch)G[0].onMouseOver(e);this.prevKDPoint=G[0];d=!1}d&&(f=t&&t.tooltipOptions.followPointer,k&&f&&!k.isHidden&&(f=k.getAnchor([{}],e),k.updatePosition({plotX:f[0],plotY:f[1]})));this.unDocMouseMove||(this.unDocMouseMove=E(g,"mousemove",function(b){if(F[a.hoverChartIndex])F[a.hoverChartIndex].pointer.onDocumentMouseMove(b)}));
v(c?G:[b(n,G[0])],function(a){v(h.axes,function(b){(!a||a.series&&a.series[b.coll]===b)&&b.drawCrosshair(e,a)})})},reset:function(a,b){var e=this.chart,h=e.hoverSeries,c=e.hoverPoint,k=e.hoverPoints,f=e.tooltip,d=f&&f.shared?k:c;a&&d&&v(w(d),function(b){b.series.isCartesian&&void 0===b.plotX&&(a=!1)});if(a)f&&d&&(f.refresh(d),c&&(c.setState(c.state,!0),v(e.axes,function(a){a.crosshair&&a.drawCrosshair(null,c)})));else{if(c)c.onMouseOut();k&&v(k,function(a){a.setState()});if(h)h.onMouseOut();f&&f.hide(b);
this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());v(e.axes,function(a){a.hideCrosshair()});this.hoverX=this.prevKDPoint=e.hoverPoints=e.hoverPoint=null}},scaleGroups:function(a,b){var e=this.chart,h;v(e.series,function(c){h=a||c.getPlotBox();c.xAxis&&c.xAxis.zoomEnabled&&c.group&&(c.group.attr(h),c.markerGroup&&(c.markerGroup.attr(h),c.markerGroup.clip(b?e.clipRect:null)),c.dataLabelsGroup&&c.dataLabelsGroup.attr(h))});e.clipRect.attr(b||e.clipBox)},dragStart:function(a){var b=this.chart;
b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,e=b.options.chart,k=a.chartX,c=a.chartY,f=this.zoomHor,d=this.zoomVert,g=b.plotLeft,n=b.plotTop,t=b.plotWidth,D=b.plotHeight,l,r=this.selectionMarker,p=this.mouseDownX,m=this.mouseDownY,z=e.panKey&&a[e.panKey+"Key"];r&&r.touch||(k<g?k=g:k>g+t&&(k=g+t),c<n?c=n:c>n+D&&(c=n+D),this.hasDragged=Math.sqrt(Math.pow(p-k,2)+Math.pow(m-c,2)),10<this.hasDragged&&
(l=b.isInsidePlot(p-g,m-n),b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&l&&!z&&!r&&(this.selectionMarker=r=b.renderer.rect(g,n,f?1:t,d?1:D,0).attr({fill:e.selectionMarkerFill||H("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",zIndex:7}).add()),r&&f&&(k-=p,r.attr({width:Math.abs(k),x:(0<k?0:k)+p})),r&&d&&(k=c-m,r.attr({height:Math.abs(k),y:(0<k?0:k)+m})),l&&!r&&e.panning&&b.pan(a,e.panning)))},drop:function(a){var b=this,e=this.chart,k=this.hasPinched;if(this.selectionMarker){var c=
{originalEvent:a,xAxis:[],yAxis:[]},f=this.selectionMarker,g=f.attr?f.attr("x"):f.x,n=f.attr?f.attr("y"):f.y,t=f.attr?f.attr("width"):f.width,w=f.attr?f.attr("height"):f.height,D;if(this.hasDragged||k)v(e.axes,function(e){if(e.zoomEnabled&&d(e.min)&&(k||b[{xAxis:"zoomX",yAxis:"zoomY"}[e.coll]])){var f=e.horiz,h="touchend"===a.type?e.minPixelPadding:0,m=e.toValue((f?g:n)+h),f=e.toValue((f?g+t:n+w)-h);c[e.coll].push({axis:e,min:Math.min(m,f),max:Math.max(m,f)});D=!0}}),D&&r(e,"selection",c,function(a){e.zoom(l(a,
k?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();k&&this.scaleGroups()}e&&(p(e.container,{cursor:e._cursor}),e.cancelClick=10<this.hasDragged,e.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){a=this.normalize(a);this.zoomOption(a);a.preventDefault&&a.preventDefault();this.dragStart(a)},onDocumentMouseUp:function(b){F[a.hoverChartIndex]&&F[a.hoverChartIndex].pointer.drop(b)},onDocumentMouseMove:function(a){var b=this.chart,
e=this.chartPosition;a=this.normalize(a,e);!e||this.inClass(a.target,"highcharts-tracker")||b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},onContainerMouseLeave:function(b){var e=F[a.hoverChartIndex];e&&(b.relatedTarget||b.toElement)&&(e.pointer.reset(),e.pointer.chartPosition=null)},onContainerMouseMove:function(b){var e=this.chart;d(a.hoverChartIndex)&&F[a.hoverChartIndex]&&F[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=e.index);b=this.normalize(b);b.returnValue=!1;
"mousedown"===e.mouseIsDown&&this.drag(b);!this.inClass(b.target,"highcharts-tracker")&&!e.isInsidePlot(b.chartX-e.plotLeft,b.chartY-e.plotTop)||e.openMenu||this.runPointActions(b)},inClass:function(a,b){for(var e;a;){if(e=A(a,"class")){if(-1!==e.indexOf(b))return!0;if(-1!==e.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;if(!(!b||!a||b.options.stickyTracking||this.inClass(a,"highcharts-tooltip")||
this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},onContainerClick:function(a){var b=this.chart,e=b.hoverPoint,f=b.plotLeft,c=b.plotTop;a=this.normalize(a);b.cancelClick||(e&&this.inClass(a.target,"highcharts-tracker")?(r(e.series,"click",l(a,{point:e})),b.hoverPoint&&e.firePointEvent("click",a)):(l(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-f,a.chartY-c)&&r(b,"click",a)))},setDOMEvents:function(){var b=this,f=b.chart.container;f.onmousedown=
function(a){b.onContainerMouseDown(a)};f.onmousemove=function(a){b.onContainerMouseMove(a)};f.onclick=function(a){b.onContainerClick(a)};E(f,"mouseleave",b.onContainerMouseLeave);1===a.chartCount&&E(g,"mouseup",b.onDocumentMouseUp);a.hasTouch&&(f.ontouchstart=function(a){b.onContainerTouchStart(a)},f.ontouchmove=function(a){b.onContainerTouchMove(a)},1===a.chartCount&&E(g,"touchend",b.onDocumentTouchEnd))},destroy:function(){var b;n(this.chart.container,"mouseleave",this.onContainerMouseLeave);a.chartCount||
(n(g,"mouseup",this.onDocumentMouseUp),n(g,"touchend",this.onDocumentTouchEnd));clearInterval(this.tooltipTimeout);for(b in this)this[b]=null}}})(M);(function(a){var E=a.charts,A=a.each,F=a.extend,H=a.map,p=a.noop,d=a.pick;F(a.Pointer.prototype,{pinchTranslate:function(a,d,l,r,f,b){this.zoomHor&&this.pinchTranslateDirection(!0,a,d,l,r,f,b);this.zoomVert&&this.pinchTranslateDirection(!1,a,d,l,r,f,b)},pinchTranslateDirection:function(a,d,l,r,f,b,n,p){var g=this.chart,k=a?"x":"y",e=a?"X":"Y",h="chart"+
e,v=a?"width":"height",u=g["plot"+(a?"Left":"Top")],c,q,x=p||1,w=g.inverted,I=g.bounds[a?"h":"v"],J=1===d.length,D=d[0][h],G=l[0][h],L=!J&&d[1][h],N=!J&&l[1][h],m;l=function(){!J&&20<Math.abs(D-L)&&(x=p||Math.abs(G-N)/Math.abs(D-L));q=(u-G)/x+D;c=g["plot"+(a?"Width":"Height")]/x};l();d=q;d<I.min?(d=I.min,m=!0):d+c>I.max&&(d=I.max-c,m=!0);m?(G-=.8*(G-n[k][0]),J||(N-=.8*(N-n[k][1])),l()):n[k]=[G,N];w||(b[k]=q-u,b[v]=c);b=w?1/x:x;f[v]=c;f[k]=d;r[w?a?"scaleY":"scaleX":"scale"+e]=x;r["translate"+e]=b*
u+(G-b*D)},pinch:function(a){var g=this,l=g.chart,r=g.pinchDown,f=a.touches,b=f.length,n=g.lastValidTouch,w=g.hasZoom,t=g.selectionMarker,k={},e=1===b&&(g.inClass(a.target,"highcharts-tracker")&&l.runTrackerClick||g.runChartClick),h={};1<b&&(g.initiated=!0);w&&g.initiated&&!e&&a.preventDefault();H(f,function(a){return g.normalize(a)});"touchstart"===a.type?(A(f,function(a,b){r[b]={chartX:a.chartX,chartY:a.chartY}}),n.x=[r[0].chartX,r[1]&&r[1].chartX],n.y=[r[0].chartY,r[1]&&r[1].chartY],A(l.axes,function(a){if(a.zoomEnabled){var b=
l.bounds[a.horiz?"h":"v"],c=a.minPixelPadding,e=a.toPixels(d(a.options.min,a.dataMin)),f=a.toPixels(d(a.options.max,a.dataMax)),k=Math.max(e,f);b.min=Math.min(a.pos,Math.min(e,f)-c);b.max=Math.max(a.pos+a.len,k+c)}}),g.res=!0):g.followTouchMove&&1===b?this.runPointActions(g.normalize(a)):r.length&&(t||(g.selectionMarker=t=F({destroy:p,touch:!0},l.plotBox)),g.pinchTranslate(r,f,k,t,h,n),g.hasPinched=w,g.scaleGroups(k,h),g.res&&(g.res=!1,this.reset(!1,0)))},touch:function(g,p){var l=this.chart,r,f;
if(l.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});a.hoverChartIndex=l.index;1===g.touches.length?(g=this.normalize(g),(f=l.isInsidePlot(g.chartX-l.plotLeft,g.chartY-l.plotTop))&&!l.openMenu?(p&&this.runPointActions(g),"touchmove"===g.type&&(p=this.pinchDown,r=p[0]?4<=Math.sqrt(Math.pow(p[0].chartX-g.chartX,2)+Math.pow(p[0].chartY-g.chartY,2)):!1),d(r,!0)&&this.pinch(g)):p&&this.reset()):2===g.touches.length&&this.pinch(g)},onContainerTouchStart:function(a){this.zoomOption(a);
this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(d){E[a.hoverChartIndex]&&E[a.hoverChartIndex].pointer.drop(d)}})})(M);(function(a){var E=a.addEvent,A=a.charts,F=a.css,H=a.doc,p=a.extend,d=a.noop,g=a.Pointer,v=a.removeEvent,l=a.win,r=a.wrap;if(l.PointerEvent||l.MSPointerEvent){var f={},b=!!l.PointerEvent,n=function(){var a,b=[];b.item=function(a){return this[a]};for(a in f)f.hasOwnProperty(a)&&b.push({pageX:f[a].pageX,pageY:f[a].pageY,target:f[a].target});
return b},w=function(b,f,e,h){"touch"!==b.pointerType&&b.pointerType!==b.MSPOINTER_TYPE_TOUCH||!A[a.hoverChartIndex]||(h(b),h=A[a.hoverChartIndex].pointer,h[f]({type:e,target:b.currentTarget,preventDefault:d,touches:n()}))};p(g.prototype,{onContainerPointerDown:function(a){w(a,"onContainerTouchStart","touchstart",function(a){f[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){w(a,"onContainerTouchMove","touchmove",function(a){f[a.pointerId]={pageX:a.pageX,
pageY:a.pageY};f[a.pointerId].target||(f[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){w(a,"onDocumentTouchEnd","touchend",function(a){delete f[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,b?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,b?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(H,b?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});r(g.prototype,"init",function(a,b,e){a.call(this,b,e);this.hasZoom&&
F(b.container,{"-ms-touch-action":"none","touch-action":"none"})});r(g.prototype,"setDOMEvents",function(a){a.apply(this);(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(E)});r(g.prototype,"destroy",function(a){this.batchMSEvents(v);a.call(this)})}})(M);(function(a){var E,A=a.addEvent,F=a.css,H=a.discardElement,p=a.defined,d=a.each,g=a.extend,v=a.isFirefox,l=a.marginNames,r=a.merge,f=a.pick,b=a.setAnimation,n=a.stableSort,w=a.win,t=a.wrap;E=a.Legend=function(a,b){this.init(a,b)};E.prototype=
{init:function(a,b){this.chart=a;this.setOptions(b);b.enabled&&(this.render(),A(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},setOptions:function(a){var b=f(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=r(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.initialItemX=this.padding=b;this.initialItemY=b-5;this.itemHeight=this.maxItemWidth=0;this.symbolWidth=f(a.symbolWidth,16);this.pages=[]},update:function(a,b){var e=
this.chart;this.setOptions(r(!0,this.options,a));this.destroy();e.isDirtyLegend=e.isDirtyBox=!0;f(b,!0)&&e.redraw()},colorizeItem:function(a,b){a.legendGroup[b?"removeClass":"addClass"]("highcharts-legend-item-hidden");var e=this.options,f=a.legendItem,k=a.legendLine,c=a.legendSymbol,d=this.itemHiddenStyle.color,e=b?e.itemStyle.color:d,g=b?a.color||d:d,n=a.options&&a.options.marker,l={fill:g},t;f&&f.css({fill:e,color:e});k&&k.attr({stroke:g});if(c){if(n&&c.isMarker&&(l=a.pointAttribs(),!b))for(t in l)l[t]=
d;c.attr(l)}},positionItem:function(a){var b=this.options,f=b.symbolPadding,b=!b.rtl,k=a._legendItemPos,d=k[0],k=k[1],c=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(b?d:this.legendWidth-d-2*f-4,k);c&&(c.x=d,c.y=k)},destroyItem:function(a){var b=a.checkbox;d(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});b&&H(a.checkbox)},destroy:function(){var a=this.group,b=this.box;b&&(this.box=b.destroy());d(this.getAllItems(),function(a){d(["legendItem",
"legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())})});a&&(this.group=a.destroy());this.display=null},positionCheckboxes:function(a){var b=this.group&&this.group.alignAttr,f,k=this.clipHeight||this.legendHeight,g=this.titleHeight;b&&(f=b.translateY,d(this.allItems,function(c){var e=c.checkbox,h;e&&(h=f+g+e.y+(a||0)+3,F(e,{left:b.translateX+c.checkboxOffset+e.x-20+"px",top:h+"px",display:h>f-6&&h<f+k-6?"":"none"}))}))},renderTitle:function(){var a=this.padding,b=this.options.title,f=0;b.text&&
(this.title||(this.title=this.chart.renderer.label(b.text,a-3,a-4,null,null,null,null,null,"legend-title").attr({zIndex:1}).css(b.style).add(this.group)),a=this.title.getBBox(),f=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:f}));this.titleHeight=f},setText:function(b){var e=this.options;b.legendItem.attr({text:e.labelFormat?a.format(e.labelFormat,b):e.labelFormatter.call(b)})},renderItem:function(a){var b=this.chart,h=b.renderer,k=this.options,d="horizontal"===k.layout,c=this.symbolWidth,
g=k.symbolPadding,n=this.itemStyle,l=this.itemHiddenStyle,t=this.padding,p=d?f(k.itemDistance,20):0,D=!k.rtl,G=k.width,L=k.itemMarginBottom||0,w=this.itemMarginTop,m=this.initialItemX,z=a.legendItem,v=!a.series,P=!v&&a.series.drawLegendSymbol?a.series:a,y=P.options,y=this.createCheckboxForItem&&y&&y.showCheckbox,B=k.useHTML;z||(a.legendGroup=h.g("legend-item").addClass("highcharts-"+P.type+"-series highcharts-color-"+a.colorIndex+(a.options.className?" "+a.options.className:"")+(v?" highcharts-series-"+
a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=z=h.text("",D?c+g:-g,this.baseline||0,B).css(r(a.visible?n:l)).attr({align:D?"left":"right",zIndex:2}).add(a.legendGroup),this.baseline||(n=n.fontSize,this.fontMetrics=h.fontMetrics(n,z),this.baseline=this.fontMetrics.f+3+w,z.attr("y",this.baseline)),P.drawLegendSymbol(this,a),this.setItemEvents&&this.setItemEvents(a,z,B),y&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);this.setText(a);h=z.getBBox();c=a.checkboxOffset=
k.itemWidth||a.legendItemWidth||c+g+h.width+p+(y?20:0);this.itemHeight=g=Math.round(a.legendItemHeight||h.height);d&&this.itemX-m+c>(G||b.chartWidth-2*t-m-k.x)&&(this.itemX=m,this.itemY+=w+this.lastLineHeight+L,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,c);this.lastItemY=w+this.itemY+L;this.lastLineHeight=Math.max(g,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];d?this.itemX+=c:(this.itemY+=w+g+L,this.lastLineHeight=g);this.offsetWidth=G||Math.max((d?this.itemX-
m-p:c)+t,this.offsetWidth)},getAllItems:function(){var a=[];d(this.chart.series,function(b){var e=b&&b.options;b&&f(e.showInLegend,p(e.linkedTo)?!1:void 0,!0)&&(a=a.concat(b.legendItems||("point"===e.legendType?b.data:b)))});return a},adjustMargins:function(a,b){var e=this.chart,k=this.options,g=k.align.charAt(0)+k.verticalAlign.charAt(0)+k.layout.charAt(0);k.floating||d([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(c,d){c.test(g)&&!p(a[d])&&(e[l[d]]=Math.max(e[l[d]],e.legend[(d+
1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][d]*k[d%2?"x":"y"]+f(k.margin,12)+b[d]))})},render:function(){var a=this,b=a.chart,f=b.renderer,l=a.group,u,c,q,t,r=a.box,p=a.options,w=a.padding;a.itemX=a.initialItemX;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;l||(a.group=l=f.g("legend").attr({zIndex:7}).add(),a.contentGroup=f.g().attr({zIndex:1}).add(l),a.scrollGroup=f.g().add(a.contentGroup));a.renderTitle();u=a.getAllItems();n(u,function(a,b){return(a.options&&a.options.legendIndex||0)-
(b.options&&b.options.legendIndex||0)});p.reversed&&u.reverse();a.allItems=u;a.display=c=!!u.length;a.lastLineHeight=0;d(u,function(b){a.renderItem(b)});q=(p.width||a.offsetWidth)+w;t=a.lastItemY+a.lastLineHeight+a.titleHeight;t=a.handleOverflow(t);t+=w;r||(a.box=r=f.rect().addClass("highcharts-legend-box").attr({r:p.borderRadius}).add(l),r.isNew=!0);r.attr({stroke:p.borderColor,"stroke-width":p.borderWidth||0,fill:p.backgroundColor||"none"}).shadow(p.shadow);0<q&&0<t&&(r[r.isNew?"attr":"animate"](r.crisp({x:0,
y:0,width:q,height:t},r.strokeWidth())),r.isNew=!1);r[c?"show":"hide"]();a.legendWidth=q;a.legendHeight=t;d(u,function(b){a.positionItem(b)});c&&l.align(g({width:q,height:t},p),!0,"spacingBox");b.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var b=this,h=this.chart,k=h.renderer,g=this.options,c=g.y,h=h.spacingBox.height+("top"===g.verticalAlign?-c:c)-this.padding,c=g.maxHeight,n,l=this.clipRect,t=g.navigation,r=f(t.animation,!0),p=t.arrowSize||12,D=this.nav,G=this.pages,L=this.padding,
w,m=this.allItems,z=function(a){a?l.attr({height:a}):l&&(b.clipRect=l.destroy(),b.contentGroup.clip());b.contentGroup.div&&(b.contentGroup.div.style.clip=a?"rect("+L+"px,9999px,"+(L+a)+"px,0)":"auto")};"horizontal"!==g.layout||"middle"===g.verticalAlign||g.floating||(h/=2);c&&(h=Math.min(h,c));G.length=0;a>h&&!1!==t.enabled?(this.clipHeight=n=Math.max(h-20-this.titleHeight-L,0),this.currentPage=f(this.currentPage,1),this.fullHeight=a,d(m,function(a,b){var c=a._legendItemPos[1];a=Math.round(a.legendItem.getBBox().height);
var e=G.length;if(!e||c-G[e-1]>n&&(w||c)!==G[e-1])G.push(w||c),e++;b===m.length-1&&c+a-G[e-1]>n&&G.push(c);c!==w&&(w=c)}),l||(l=b.clipRect=k.clipRect(0,L,9999,0),b.contentGroup.clip(l)),z(n),D||(this.nav=D=k.g().attr({zIndex:1}).add(this.group),this.up=k.symbol("triangle",0,0,p,p).on("click",function(){b.scroll(-1,r)}).add(D),this.pager=k.text("",15,10).addClass("highcharts-legend-navigation").css(t.style).add(D),this.down=k.symbol("triangle-down",0,0,p,p).on("click",function(){b.scroll(1,r)}).add(D)),
b.scroll(0),a=h):D&&(z(),D.hide(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0);return a},scroll:function(a,e){var f=this.pages,d=f.length;a=this.currentPage+a;var k=this.clipHeight,c=this.options.navigation,g=this.pager,n=this.padding;a>d&&(a=d);0<a&&(void 0!==e&&b(e,this.chart),this.nav.attr({translateX:n,translateY:k+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),g.attr({text:a+"/"+
d}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===d?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?c.inactiveColor:c.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===d?c.inactiveColor:c.activeColor}).css({cursor:a===d?"default":"pointer"}),e=-f[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:e}),this.currentPage=a,this.positionCheckboxes(e))}};a.LegendSymbolMixin={drawRectangle:function(a,b){var e=
a.options,d=e.symbolHeight||a.fontMetrics.f,e=e.squareSymbol;b.legendSymbol=this.chart.renderer.rect(e?(a.symbolWidth-d)/2:0,a.baseline-d+1,e?d:a.symbolWidth,d,f(a.options.symbolRadius,d/2)).addClass("highcharts-point").attr({zIndex:3}).add(b.legendGroup)},drawLineMarker:function(a){var b=this.options,f=b.marker,d=a.symbolWidth,k=this.chart.renderer,c=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var g;g={"stroke-width":b.lineWidth||0};b.dashStyle&&(g.dashstyle=b.dashStyle);this.legendLine=
k.path(["M",0,a,"L",d,a]).addClass("highcharts-graph").attr(g).add(c);f&&!1!==f.enabled&&(b=0===this.symbol.indexOf("url")?0:f.radius,this.legendSymbol=f=k.symbol(this.symbol,d/2-b,a-b,2*b,2*b,f).addClass("highcharts-point").add(c),f.isMarker=!0)}};(/Trident\/7\.0/.test(w.navigator.userAgent)||v)&&t(E.prototype,"positionItem",function(a,b){var e=this,f=function(){b._legendItemPos&&a.call(e,b)};f();setTimeout(f)})})(M);(function(a){var E=a.addEvent,A=a.animate,F=a.animObject,H=a.attr,p=a.doc,d=a.Axis,
g=a.createElement,v=a.defaultOptions,l=a.discardElement,r=a.charts,f=a.css,b=a.defined,n=a.each,w=a.error,t=a.extend,k=a.fireEvent,e=a.getStyle,h=a.grep,C=a.isNumber,u=a.isObject,c=a.isString,q=a.Legend,x=a.marginNames,K=a.merge,I=a.Pointer,J=a.pick,D=a.pInt,G=a.removeEvent,L=a.seriesTypes,N=a.splat,m=a.svg,z=a.syncTimeout,O=a.win,P=a.Renderer,y=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,b,c){return new y(a,b,c)};y.prototype={callbacks:[],getArgs:function(){var a=[].slice.call(arguments);
if(c(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(b,c){var e,f=b.series;b.series=null;e=K(v,b);e.series=b.series=f;this.userOptions=b;this.respRules=[];b=e.chart;f=b.events;this.margin=[];this.spacing=[];this.bounds={h:{},v:{}};this.callback=c;this.isResizing=0;this.options=e;this.axes=[];this.series=[];this.hasCartesianSeries=b.showAxes;var d;this.index=r.length;r.push(this);a.chartCount++;if(f)for(d in f)E(this,d,f[d]);this.xAxis=[];this.yAxis=[];this.pointCount=
this.colorCounter=this.symbolCounter=0;this.firstRender()},initSeries:function(a){var b=this.options.chart;(b=L[a.type||b.type||b.defaultSeriesType])||w(17,!0);b=new b;b.init(this,a);return b},isInsidePlot:function(a,b,c){var e=c?b:a;a=c?a:b;return 0<=e&&e<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(b){var c=this.axes,e=this.series,f=this.pointer,d=this.legend,m=this.isDirtyLegend,h,g,q=this.hasCartesianSeries,l=this.isDirtyBox,D=e.length,u=D,B=this.renderer,r=B.isHidden(),G=[];a.setAnimation(b,
this);r&&this.cloneRenderTo();for(this.layOutTitles();u--;)if(b=e[u],b.options.stacking&&(h=!0,b.isDirty)){g=!0;break}if(g)for(u=D;u--;)b=e[u],b.options.stacking&&(b.isDirty=!0);n(e,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),m=!0);a.isDirtyData&&k(a,"updatedData")});m&&d.options.enabled&&(d.render(),this.isDirtyLegend=!1);h&&this.getStacks();q&&n(c,function(a){a.updateNames();a.setScale()});this.getMargins();q&&(n(c,function(a){a.isDirty&&(l=!0)}),n(c,
function(a){var b=a.min+","+a.max;a.extKey!==b&&(a.extKey=b,G.push(function(){k(a,"afterSetExtremes",t(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(l||h)&&a.redraw()}));l&&this.drawChartBox();n(e,function(a){(l||a.isDirty)&&a.visible&&a.redraw()});f&&f.reset(!0);B.draw();k(this,"redraw");r&&this.cloneRenderTo(!0);n(G,function(a){a.call()})},get:function(a){var b=this.axes,c=this.series,e,f;for(e=0;e<b.length;e++)if(b[e].options.id===a)return b[e];for(e=0;e<c.length;e++)if(c[e].options.id===
a)return c[e];for(e=0;e<c.length;e++)for(f=c[e].points||[],b=0;b<f.length;b++)if(f[b].id===a)return f[b];return null},getAxes:function(){var a=this,b=this.options,c=b.xAxis=N(b.xAxis||{}),b=b.yAxis=N(b.yAxis||{});n(c,function(a,b){a.index=b;a.isX=!0});n(b,function(a,b){a.index=b});c=c.concat(b);n(c,function(b){new d(a,b)})},getSelectedPoints:function(){var a=[];n(this.series,function(b){a=a.concat(h(b.points||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return h(this.series,
function(a){return a.selected})},setTitle:function(a,b,c){var e=this,f=e.options,d;d=f.title=K({style:{color:"#333333",fontSize:f.isStock?"16px":"18px"}},f.title,a);f=f.subtitle=K({style:{color:"#666666"}},f.subtitle,b);n([["title",a,d],["subtitle",b,f]],function(a,b){var c=a[0],f=e[c],d=a[1];a=a[2];f&&d&&(e[c]=f=f.destroy());a&&a.text&&!f&&(e[c]=e.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+c,zIndex:a.zIndex||4}).add(),e[c].update=function(a){e.setTitle(!b&&a,b&&
a)},e[c].css(a.style))});e.layOutTitles(c)},layOutTitles:function(a){var b=0,c,e=this.renderer,f=this.spacingBox;n(["title","subtitle"],function(a){var c=this[a],d=this.options[a],m;c&&(m=d.style.fontSize,m=e.fontMetrics(m,c).b,c.css({width:(d.width||f.width+d.widthAdjust)+"px"}).align(t({y:b+m+("title"===a?-3:2)},d),!1,"spacingBox"),d.floating||d.verticalAlign||(b=Math.ceil(b+c.getBBox().height)))},this);c=this.titleOffset!==b;this.titleOffset=b;!this.isDirtyBox&&c&&(this.isDirtyBox=c,this.hasRendered&&
J(a,!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var a=this.options.chart,c=a.width,a=a.height,f=this.renderToClone||this.renderTo;b(c)||(this.containerWidth=e(f,"width"));b(a)||(this.containerHeight=e(f,"height"));this.chartWidth=Math.max(0,c||this.containerWidth||600);this.chartHeight=Math.max(0,J(a,19<this.containerHeight?this.containerHeight:400))},cloneRenderTo:function(a){var b=this.renderToClone,c=this.container;if(a){if(b){for(;b.childNodes.length;)this.renderTo.appendChild(b.firstChild);
l(b);delete this.renderToClone}}else c&&c.parentNode===this.renderTo&&this.renderTo.removeChild(c),this.renderToClone=b=this.renderTo.cloneNode(0),f(b,{position:"absolute",top:"-9999px",display:"block"}),b.style.setProperty&&b.style.setProperty("display","block","important"),p.body.appendChild(b),c&&b.appendChild(c)},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var b,e=this.options,f=e.chart,d,m;b=this.renderTo;var h=a.uniqueKey(),k;b||
(this.renderTo=b=f.renderTo);c(b)&&(this.renderTo=b=p.getElementById(b));b||w(13,!0);d=D(H(b,"data-highcharts-chart"));C(d)&&r[d]&&r[d].hasRendered&&r[d].destroy();H(b,"data-highcharts-chart",this.index);b.innerHTML="";f.skipClone||b.offsetWidth||this.cloneRenderTo();this.getChartSize();d=this.chartWidth;m=this.chartHeight;k=t({position:"relative",overflow:"hidden",width:d+"px",height:m+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},f.style);this.container=
b=g("div",{id:h},k,this.renderToClone||b);this._cursor=b.style.cursor;this.renderer=new (a[f.renderer]||P)(b,d,m,null,f.forExport,e.exporting&&e.exporting.allowHTML);this.setClassName(f.className);this.renderer.setStyle(f.style);this.renderer.chartIndex=this.index},getMargins:function(a){var c=this.spacing,e=this.margin,f=this.titleOffset;this.resetMargins();f&&!b(e[0])&&(this.plotTop=Math.max(this.plotTop,f+this.options.title.margin+c[0]));this.legend.display&&this.legend.adjustMargins(e,c);this.extraBottomMargin&&
(this.marginBottom+=this.extraBottomMargin);this.extraTopMargin&&(this.plotTop+=this.extraTopMargin);a||this.getAxisMargins()},getAxisMargins:function(){var a=this,c=a.axisOffset=[0,0,0,0],e=a.margin;a.hasCartesianSeries&&n(a.axes,function(a){a.visible&&a.getOffset()});n(x,function(f,d){b(e[d])||(a[f]+=c[d])});a.setChartSize()},reflow:function(a){var c=this,f=c.options.chart,d=c.renderTo,m=b(f.width),h=f.width||e(d,"width"),f=f.height||e(d,"height"),d=a?a.target:O;if(!m&&!c.isPrinting&&h&&f&&(d===
O||d===p)){if(h!==c.containerWidth||f!==c.containerHeight)clearTimeout(c.reflowTimeout),c.reflowTimeout=z(function(){c.container&&c.setSize(void 0,void 0,!1)},a?100:0);c.containerWidth=h;c.containerHeight=f}},initReflow:function(){var a=this,b;b=E(O,"resize",function(b){a.reflow(b)});E(a,"destroy",b)},setSize:function(b,c,e){var d=this,m=d.renderer;d.isResizing+=1;a.setAnimation(e,d);d.oldChartHeight=d.chartHeight;d.oldChartWidth=d.chartWidth;void 0!==b&&(d.options.chart.width=b);void 0!==c&&(d.options.chart.height=
c);d.getChartSize();b=m.globalAnimation;(b?A:f)(d.container,{width:d.chartWidth+"px",height:d.chartHeight+"px"},b);d.setChartSize(!0);m.setSize(d.chartWidth,d.chartHeight,e);n(d.axes,function(a){a.isDirty=!0;a.setScale()});d.isDirtyLegend=!0;d.isDirtyBox=!0;d.layOutTitles();d.getMargins();d.setResponsive&&d.setResponsive(!1);d.redraw(e);d.oldChartHeight=null;k(d,"resize");z(function(){d&&k(d,"endResize",null,function(){--d.isResizing})},F(b).duration)},setChartSize:function(a){var b=this.inverted,
c=this.renderer,e=this.chartWidth,f=this.chartHeight,d=this.options.chart,m=this.spacing,h=this.clipOffset,k,g,q,l;this.plotLeft=k=Math.round(this.plotLeft);this.plotTop=g=Math.round(this.plotTop);this.plotWidth=q=Math.max(0,Math.round(e-k-this.marginRight));this.plotHeight=l=Math.max(0,Math.round(f-g-this.marginBottom));this.plotSizeX=b?l:q;this.plotSizeY=b?q:l;this.plotBorderWidth=d.plotBorderWidth||0;this.spacingBox=c.spacingBox={x:m[3],y:m[0],width:e-m[3]-m[1],height:f-m[0]-m[2]};this.plotBox=
c.plotBox={x:k,y:g,width:q,height:l};e=2*Math.floor(this.plotBorderWidth/2);b=Math.ceil(Math.max(e,h[3])/2);c=Math.ceil(Math.max(e,h[0])/2);this.clipBox={x:b,y:c,width:Math.floor(this.plotSizeX-Math.max(e,h[1])/2-b),height:Math.max(0,Math.floor(this.plotSizeY-Math.max(e,h[2])/2-c))};a||n(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,b=a.options.chart;n(["margin","spacing"],function(c){var e=b[c],f=u(e)?e:[e,e,e,e];n(["Top","Right","Bottom","Left"],
function(e,d){a[c][d]=J(b[c+e],f[d])})});n(x,function(b,c){a[b]=J(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,e=this.chartHeight,f=this.chartBackground,d=this.plotBackground,m=this.plotBorder,h,k=this.plotBGImage,g=a.backgroundColor,n=a.plotBackgroundColor,q=a.plotBackgroundImage,l,D=this.plotLeft,u=this.plotTop,t=this.plotWidth,r=this.plotHeight,G=this.plotBox,p=this.clipRect,x=this.clipBox,
z="animate";f||(this.chartBackground=f=b.rect().addClass("highcharts-background").add(),z="attr");h=a.borderWidth||0;l=h+(a.shadow?8:0);g={fill:g||"none"};if(h||f["stroke-width"])g.stroke=a.borderColor,g["stroke-width"]=h;f.attr(g).shadow(a.shadow);f[z]({x:l/2,y:l/2,width:c-l-h%2,height:e-l-h%2,r:a.borderRadius});z="animate";d||(z="attr",this.plotBackground=d=b.rect().addClass("highcharts-plot-background").add());d[z](G);d.attr({fill:n||"none"}).shadow(a.plotShadow);q&&(k?k.animate(G):this.plotBGImage=
b.image(q,D,u,t,r).add());p?p.animate({width:x.width,height:x.height}):this.clipRect=b.clipRect(x);z="animate";m||(z="attr",this.plotBorder=m=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());m.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||0,fill:"none"});m[z](m.crisp({x:D,y:u,width:t,height:r},-m.strokeWidth()));this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,e=a.options.series,f,d;n(["inverted","angular","polar"],function(m){c=L[b.type||
b.defaultSeriesType];d=b[m]||c&&c.prototype[m];for(f=e&&e.length;!d&&f--;)(c=L[e[f].type])&&c.prototype[m]&&(d=!0);a[m]=d})},linkSeries:function(){var a=this,b=a.series;n(b,function(a){a.linkedSeries.length=0});n(b,function(b){var e=b.options.linkedTo;c(e)&&(e=":previous"===e?a.series[b.index-1]:a.get(e))&&e.linkedParent!==b&&(e.linkedSeries.push(b),b.linkedParent=e,b.visible=J(b.options.visible,e.options.visible,b.visible))})},renderSeries:function(){n(this.series,function(a){a.translate();a.render()})},
renderLabels:function(){var a=this,b=a.options.labels;b.items&&n(b.items,function(c){var e=t(b.style,c.style),f=D(e.left)+a.plotLeft,d=D(e.top)+a.plotTop+12;delete e.left;delete e.top;a.renderer.text(c.html,f,d).attr({zIndex:2}).css(e).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,e,f,d;this.setTitle();this.legend=new q(this,c.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();c=this.plotWidth;e=this.plotHeight-=21;n(a,function(a){a.setScale()});
this.getAxisMargins();f=1.1<c/this.plotWidth;d=1.05<e/this.plotHeight;if(f||d)n(a,function(a){(a.horiz&&f||!a.horiz&&d)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&n(a,function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=b.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var b=this;a=K(!0,this.options.credits,
a);a.enabled&&!this.credits&&(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(O.location.href=a.href)}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},destroy:function(){var b=this,c=b.axes,e=b.series,f=b.container,d,m=f&&f.parentNode;k(b,"destroy");r[b.index]=void 0;a.chartCount--;b.renderTo.removeAttribute("data-highcharts-chart");
G(b);for(d=c.length;d--;)c[d]=c[d].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(d=e.length;d--;)e[d]=e[d].destroy();n("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),function(a){var c=b[a];c&&c.destroy&&(b[a]=c.destroy())});f&&(f.innerHTML="",G(f),m&&l(f));for(d in b)delete b[d]},isReadyToRender:function(){var a=this;return m||O!=O.top||"complete"===
p.readyState?!0:(p.attachEvent("onreadystatechange",function(){p.detachEvent("onreadystatechange",a.firstRender);"complete"===p.readyState&&a.firstRender()}),!1)},firstRender:function(){var a=this,b=a.options;if(a.isReadyToRender()){a.getContainer();k(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();n(b.series||[],function(b){a.initSeries(b)});a.linkSeries();k(a,"beforeRender");I&&(a.pointer=new I(a,b));a.render();a.renderer.draw();if(!a.renderer.imgCount&&a.onload)a.onload();
a.cloneRenderTo(!0)}},onload:function(){n([this.callback].concat(this.callbacks),function(a){a&&void 0!==this.index&&a.apply(this,[this])},this);k(this,"load");!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}}})(M);(function(a){var E,A=a.each,F=a.extend,H=a.erase,p=a.fireEvent,d=a.format,g=a.isArray,v=a.isNumber,l=a.pick,r=a.removeEvent;E=a.Point=function(){};E.prototype={init:function(a,b,d){this.series=a;this.color=a.color;this.applyOptions(b,d);a.options.colorByPoint?(b=a.options.colors||
a.chart.options.colors,this.color=this.color||b[a.colorCounter],b=b.length,d=a.colorCounter,a.colorCounter++,a.colorCounter===b&&(a.colorCounter=0)):d=a.colorIndex;this.colorIndex=l(this.colorIndex,d);a.chart.pointCount++;return this},applyOptions:function(a,b){var f=this.series,d=f.options.pointValKey||f.pointValKey;a=E.prototype.optionsToObject.call(this,a);F(this,a);this.options=this.options?F(this.options,a):a;a.group&&delete this.group;d&&(this.y=this[d]);this.isNull=l(this.isValid&&!this.isValid(),
null===this.x||!v(this.y,!0));this.selected&&(this.state="select");"name"in this&&void 0===b&&f.xAxis&&f.xAxis.hasNames&&(this.x=f.xAxis.nameToX(this));void 0===this.x&&f&&(this.x=void 0===b?f.autoIncrement(this):b);return this},optionsToObject:function(a){var b={},f=this.series,d=f.options.keys,l=d||f.pointArrayMap||["y"],k=l.length,e=0,h=0;if(v(a)||null===a)b[l[0]]=a;else if(g(a))for(!d&&a.length>k&&(f=typeof a[0],"string"===f?b.name=a[0]:"number"===f&&(b.x=a[0]),e++);h<k;)d&&void 0===a[e]||(b[l[h]]=
a[e]),e++,h++;else"object"===typeof a&&(b=a,a.dataLabels&&(f._hasPointLabels=!0),a.marker&&(f._hasPointMarkers=!0));return b},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")},getZone:function(){var a=this.series,b=a.zones,a=a.zoneAxis||"y",d=0,g;for(g=
b[d];this[a]>=g.value;)g=b[++d];g&&g.color&&!this.options.color&&(this.color=g.color);return g},destroy:function(){var a=this.series.chart,b=a.hoverPoints,d;a.pointCount--;b&&(this.setState(),H(b,this),b.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)r(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);for(d in this)this[d]=null},destroyElements:function(){for(var a=["graphic","dataLabel","dataLabelUpper","connector","shadowGroup"],
b,d=6;d--;)b=a[d],this[b]&&(this[b]=this[b].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var b=this.series,f=b.tooltipOptions,g=l(f.valueDecimals,""),t=f.valuePrefix||"",k=f.valueSuffix||"";A(b.pointArrayMap||["y"],function(b){b="{point."+b;if(t||k)a=a.replace(b+"}",t+b+"}"+k);a=a.replace(b+"}",b+":,."+g+"f}")});
return d(a,{point:this,series:this.series})},firePointEvent:function(a,b,d){var f=this,g=this.series.options;(g.point.events[a]||f.options&&f.options.events&&f.options.events[a])&&this.importEvents();"click"===a&&g.allowPointSelect&&(d=function(a){f.select&&f.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});p(this,a,b,d)},visible:!0}})(M);(function(a){var E=a.addEvent,A=a.animObject,F=a.arrayMax,H=a.arrayMin,p=a.correctFloat,d=a.Date,g=a.defaultOptions,v=a.defaultPlotOptions,l=a.defined,r=a.each,f=
a.erase,b=a.error,n=a.extend,w=a.fireEvent,t=a.grep,k=a.isArray,e=a.isNumber,h=a.isString,C=a.merge,u=a.pick,c=a.removeEvent,q=a.splat,x=a.stableSort,K=a.SVGElement,I=a.syncTimeout,J=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",radius:4,states:{hover:{animation:{duration:50},enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},
dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{hover:{lineWidthPlus:1,marker:{},halo:{size:10,opacity:.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3},{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],
colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var c=this,e,f,d=a.series,h,k=function(a,b){return u(a.options.index,a._i)-u(b.options.index,b._i)};c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();n(c,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});f=b.events;for(e in f)E(c,e,f[e]);if(f&&f.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();c.getSymbol();r(c.parallelArrays,function(a){c[a+
"Data"]=[]});c.setData(b.data,!1);c.isCartesian&&(a.hasCartesianSeries=!0);d.length&&(h=d[d.length-1]);c._i=u(h&&h._i,-1)+1;d.push(c);x(d,k);this.yAxis&&x(this.yAxis.series,k);r(d,function(a,b){a.index=b;a.name=a.name||"Series "+(b+1)})},bindAxes:function(){var a=this,c=a.options,e=a.chart,f;r(a.axisTypes||[],function(d){r(e[d],function(b){f=b.options;if(c[d]===f.index||void 0!==c[d]&&c[d]===f.id||void 0===c[d]&&0===f.index)b.series.push(a),a[d]=b,b.isDirty=!0});a[d]||a.optionalAxis===d||b(18,!0)})},
updateParallelArrays:function(a,b){var c=a.series,f=arguments,d=e(b)?function(e){var f="y"===e&&c.toYData?c.toYData(a):a[e];c[e+"Data"][b]=f}:function(a){Array.prototype[b].apply(c[a+"Data"],Array.prototype.slice.call(f,2))};r(c.parallelArrays,d)},autoIncrement:function(){var a=this.options,b=this.xIncrement,c,e=a.pointIntervalUnit,b=u(b,a.pointStart,0);this.pointInterval=c=u(this.pointInterval,a.pointInterval,1);e&&(a=new d(b),"day"===e?a=+a[d.hcSetDate](a[d.hcGetDate]()+c):"month"===e?a=+a[d.hcSetMonth](a[d.hcGetMonth]()+
c):"year"===e&&(a=+a[d.hcSetFullYear](a[d.hcGetFullYear]()+c)),c=a-b);this.xIncrement=b+c;return b},setOptions:function(a){var b=this.chart,c=b.options.plotOptions,b=b.userOptions||{},e=b.plotOptions||{},f=c[this.type];this.userOptions=a;c=C(f,c.series,a);this.tooltipOptions=C(g.tooltip,g.plotOptions[this.type].tooltip,b.tooltip,e.series&&e.series.tooltip,e[this.type]&&e[this.type].tooltip,a.tooltip);null===f.marker&&delete c.marker;this.zoneAxis=c.zoneAxis;a=this.zones=(c.zones||[]).slice();!c.negativeColor&&
!c.negativeFillColor||c.zones||a.push({value:c[this.zoneAxis+"Threshold"]||c.threshold||0,className:"highcharts-negative",color:c.negativeColor,fillColor:c.negativeFillColor});a.length&&l(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return c},getCyclic:function(a,b,c){var e,f=this.userOptions,d=a+"Index",h=a+"Counter",k=c?c.length:u(this.chart.options.chart[a+"Count"],this.chart[a+"Count"]);b||(e=u(f[d],f["_"+d]),l(e)||(f["_"+d]=e=this.chart[h]%k,this.chart[h]+=1),c&&
(b=c[e]));void 0!==e&&(this[d]=e);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||v[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(a,c,f,d){var m=this,g=m.points,n=g&&g.length||0,q,l=m.options,t=m.chart,D=null,p=m.xAxis,x=l.turboThreshold,G=this.xData,w=this.yData,
v=(q=m.pointArrayMap)&&q.length;a=a||[];q=a.length;c=u(c,!0);if(!1!==d&&q&&n===q&&!m.cropped&&!m.hasGroupedData&&m.visible)r(a,function(a,b){g[b].update&&a!==l.data[b]&&g[b].update(a,!1,null,!1)});else{m.xIncrement=null;m.colorCounter=0;r(this.parallelArrays,function(a){m[a+"Data"].length=0});if(x&&q>x){for(f=0;null===D&&f<q;)D=a[f],f++;if(e(D))for(f=0;f<q;f++)G[f]=this.autoIncrement(),w[f]=a[f];else if(k(D))if(v)for(f=0;f<q;f++)D=a[f],G[f]=D[0],w[f]=D.slice(1,v+1);else for(f=0;f<q;f++)D=a[f],G[f]=
D[0],w[f]=D[1];else b(12)}else for(f=0;f<q;f++)void 0!==a[f]&&(D={series:m},m.pointClass.prototype.applyOptions.apply(D,[a[f]]),m.updateParallelArrays(D,f));h(w[0])&&b(14,!0);m.data=[];m.options.data=m.userOptions.data=a;for(f=n;f--;)g[f]&&g[f].destroy&&g[f].destroy();p&&(p.minRange=p.userMinRange);m.isDirty=t.isDirtyBox=!0;m.isDirtyData=!!g;f=!1}"point"===l.legendType&&(this.processData(),this.generatePoints());c&&t.redraw(f)},processData:function(a){var c=this.xData,e=this.yData,f=c.length,d;d=
0;var h,k,g=this.xAxis,q,n=this.options;q=n.cropThreshold;var l=this.getExtremesFromAll||n.getExtremesFromAll,u=this.isCartesian,n=g&&g.val2lin,t=g&&g.isLog,r,D;if(u&&!this.isDirty&&!g.isDirty&&!this.yAxis.isDirty&&!a)return!1;g&&(a=g.getExtremes(),r=a.min,D=a.max);if(u&&this.sorted&&!l&&(!q||f>q||this.forceCrop))if(c[f-1]<r||c[0]>D)c=[],e=[];else if(c[0]<r||c[f-1]>D)d=this.cropData(this.xData,this.yData,r,D),c=d.xData,e=d.yData,d=d.start,h=!0;for(q=c.length||1;--q;)f=t?n(c[q])-n(c[q-1]):c[q]-c[q-
1],0<f&&(void 0===k||f<k)?k=f:0>f&&this.requireSorting&&b(15);this.cropped=h;this.cropStart=d;this.processedXData=c;this.processedYData=e;this.closestPointRange=k},cropData:function(a,b,c,e){var f=a.length,d=0,h=f,k=u(this.cropShoulder,1),g;for(g=0;g<f;g++)if(a[g]>=c){d=Math.max(0,g-k);break}for(c=g;c<f;c++)if(a[c]>e){h=c+k;break}return{xData:a.slice(d,h),yData:b.slice(d,h),start:d,end:h}},generatePoints:function(){var a=this.options.data,b=this.data,c,e=this.processedXData,f=this.processedYData,
d=this.pointClass,h=e.length,g=this.cropStart||0,k,n=this.hasGroupedData,l,u=[],t;b||n||(b=[],b.length=a.length,b=this.data=b);for(t=0;t<h;t++)k=g+t,n?(l=(new d).init(this,[e[t]].concat(q(f[t]))),l.dataGroup=this.groupMap[t]):(l=b[k])||void 0===a[k]||(b[k]=l=(new d).init(this,a[k],e[t])),l.index=k,u[t]=l;if(b&&(h!==(c=b.length)||n))for(t=0;t<c;t++)t!==g||n||(t+=h),b[t]&&(b[t].destroyElements(),b[t].plotX=void 0);this.data=b;this.points=u},getExtremes:function(a){var b=this.yAxis,c=this.processedXData,
f,d=[],h=0;f=this.xAxis.getExtremes();var g=f.min,q=f.max,n,l,t,u;a=a||this.stackedYData||this.processedYData||[];f=a.length;for(u=0;u<f;u++)if(l=c[u],t=a[u],n=(e(t,!0)||k(t))&&(!b.isLog||t.length||0<t),l=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(c[u+1]||l)>=g&&(c[u-1]||l)<=q,n&&l)if(n=t.length)for(;n--;)null!==t[n]&&(d[h++]=t[n]);else d[h++]=t;this.dataMin=H(d);this.dataMax=F(d)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=
this.options,b=a.stacking,c=this.xAxis,f=c.categories,d=this.yAxis,h=this.points,g=h.length,k=!!this.modifyValue,n=a.pointPlacement,q="between"===n||e(n),t=a.threshold,r=a.startFromThreshold?t:0,x,w,v,I,K=Number.MAX_VALUE;"between"===n&&(n=.5);e(n)&&(n*=u(a.pointRange||c.pointRange));for(a=0;a<g;a++){var C=h[a],J=C.x,A=C.y;w=C.low;var E=b&&d.stacks[(this.negStacks&&A<(r?0:t)?"-":"")+this.stackKey],F;d.isLog&&null!==A&&0>=A&&(C.isNull=!0);C.plotX=x=p(Math.min(Math.max(-1E5,c.translate(J,0,0,0,1,n,
"flags"===this.type)),1E5));b&&this.visible&&!C.isNull&&E&&E[J]&&(I=this.getStackIndicator(I,J,this.index),F=E[J],A=F.points[I.key],w=A[0],A=A[1],w===r&&I.key===E[J].base&&(w=u(t,d.min)),d.isLog&&0>=w&&(w=null),C.total=C.stackTotal=F.total,C.percentage=F.total&&C.y/F.total*100,C.stackY=A,F.setOffset(this.pointXOffset||0,this.barW||0));C.yBottom=l(w)?d.translate(w,0,1,0,1):null;k&&(A=this.modifyValue(A,C));C.plotY=w="number"===typeof A&&Infinity!==A?Math.min(Math.max(-1E5,d.translate(A,0,1,0,1)),1E5):
void 0;C.isInside=void 0!==w&&0<=w&&w<=d.len&&0<=x&&x<=c.len;C.clientX=q?p(c.translate(J,0,0,0,1,n)):x;C.negative=C.y<(t||0);C.category=f&&void 0!==f[C.x]?f[C.x]:C.x;C.isNull||(void 0!==v&&(K=Math.min(K,Math.abs(x-v))),v=x)}this.closestPointRangePx=K},getValidPoints:function(a,b){var c=this.chart;return t(a||this.points||[],function(a){return b&&!c.isInsidePlot(a.plotX,a.plotY,c.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,c=this.options,e=b.renderer,f=b.inverted,d=this.clipBox,
h=d||b.clipBox,g=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,h.height,c.xAxis,c.yAxis].join(),k=b[g],n=b[g+"m"];k||(a&&(h.width=0,b[g+"m"]=n=e.clipRect(-99,f?-b.plotLeft:-b.plotTop,99,f?b.chartWidth:b.chartHeight)),b[g]=k=e.clipRect(h),k.count={length:0});a&&!k.count[this.index]&&(k.count[this.index]=!0,k.count.length+=1);!1!==c.clip&&(this.group.clip(a||d?k:b.clipRect),this.markerGroup.clip(n),this.sharedClipKey=g);a||(k.count[this.index]&&(delete k.count[this.index],--k.count.length),
0===k.count.length&&g&&b[g]&&(d||(b[g]=b[g].destroy()),b[g+"m"]&&(b[g+"m"]=b[g+"m"].destroy())))},animate:function(a){var b=this.chart,c=A(this.options.animation),e;a?this.setClip(c):(e=this.sharedClipKey,(a=b[e])&&a.animate({width:b.plotSizeX},c),b[e+"m"]&&b[e+"m"].animate({width:b.plotSizeX+99},c),this.animate=null)},afterAnimate:function(){this.setClip();w(this,"afterAnimate")},drawPoints:function(){var a=this.points,b=this.chart,c,f,d,h,g=this.options.marker,k,n,q,l,t=this.markerGroup,r=u(g.enabled,
this.xAxis.isRadial?!0:null,this.closestPointRangePx>2*g.radius);if(!1!==g.enabled||this._hasPointMarkers)for(f=a.length;f--;)d=a[f],c=d.plotY,h=d.graphic,k=d.marker||{},n=!!d.marker,q=r&&void 0===k.enabled||k.enabled,l=d.isInside,q&&e(c)&&null!==d.y?(c=u(k.symbol,this.symbol),d.hasImage=0===c.indexOf("url"),q=this.markerAttribs(d,d.selected&&"select"),h?h[l?"show":"hide"](!0).animate(q):l&&(0<q.width||d.hasImage)&&(d.graphic=h=b.renderer.symbol(c,q.x,q.y,q.width,q.height,n?k:g).add(t)),h&&h.attr(this.pointAttribs(d,
d.selected&&"select")),h&&h.addClass(d.getClassName(),!0)):h&&(d.graphic=h.destroy())},markerAttribs:function(a,b){var c=this.options.marker,e=a&&a.options,f=e&&e.marker||{},e=u(f.radius,c.radius);b&&(c=c.states[b],b=f.states&&f.states[b],e=u(b&&b.radius,c&&c.radius,e+(c&&c.radiusPlus||0)));a.hasImage&&(e=0);a={x:Math.floor(a.plotX)-e,y:a.plotY-e};e&&(a.width=a.height=2*e);return a},pointAttribs:function(a,b){var c=this.options.marker,e=a&&a.options,f=e&&e.marker||{},d=this.color,h=e&&e.color,g=a&&
a.color,e=u(f.lineWidth,c.lineWidth),k;a&&this.zones.length&&(a=a.getZone())&&a.color&&(k=a.color);d=h||k||g||d;k=f.fillColor||c.fillColor||d;d=f.lineColor||c.lineColor||d;b&&(c=c.states[b],b=f.states&&f.states[b]||{},e=u(b.lineWidth,c.lineWidth,e+u(b.lineWidthPlus,c.lineWidthPlus,0)),k=b.fillColor||c.fillColor||k,d=b.lineColor||c.lineColor||d);return{stroke:d,"stroke-width":e,fill:k}},destroy:function(){var a=this,b=a.chart,e=/AppleWebKit\/533/.test(J.navigator.userAgent),d,h=a.data||[],k,g,n;w(a,
"destroy");c(a);r(a.axisTypes||[],function(b){(n=a[b])&&n.series&&(f(n.series,a),n.isDirty=n.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);for(d=h.length;d--;)(k=h[d])&&k.destroy&&k.destroy();a.points=null;clearTimeout(a.animationTimeout);for(g in a)a[g]instanceof K&&!a[g].survive&&(d=e&&"group"===g?"hide":"destroy",a[g][d]());b.hoverSeries===a&&(b.hoverSeries=null);f(b.series,a);for(g in a)delete a[g]},getGraphPath:function(a,b,c){var e=this,f=e.options,d=f.step,h,k=[],g=[],n;a=a||
e.points;(h=a.reversed)&&a.reverse();(d={right:1,center:2}[d]||d&&3)&&h&&(d=4-d);!f.connectNulls||b||c||(a=this.getValidPoints(a));r(a,function(h,m){var q=h.plotX,t=h.plotY,u=a[m-1];(h.leftCliff||u&&u.rightCliff)&&!c&&(n=!0);h.isNull&&!l(b)&&0<m?n=!f.connectNulls:h.isNull&&!b?n=!0:(0===m||n?m=["M",h.plotX,h.plotY]:e.getPointSpline?m=e.getPointSpline(a,h,m):d?(m=1===d?["L",u.plotX,t]:2===d?["L",(u.plotX+q)/2,u.plotY,"L",(u.plotX+q)/2,t]:["L",q,u.plotY],m.push("L",q,t)):m=["L",q,t],g.push(h.x),d&&g.push(h.x),
k.push.apply(k,m),n=!1)});k.xMap=g;return e.graphPath=k},drawGraph:function(){var a=this,b=this.options,c=(this.gappedPath||this.getGraphPath).call(this),e=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];r(this.zones,function(c,f){e.push(["zone-graph-"+f,"highcharts-graph highcharts-zone-graph-"+f+" "+(c.className||""),c.color||a.color,c.dashStyle||b.dashStyle])});r(e,function(e,f){var d=e[0],h=a[d];h?(h.endX=c.xMap,h.animate({d:c})):c.length&&(a[d]=a.chart.renderer.path(c).addClass(e[1]).attr({zIndex:1}).add(a.group),
h={stroke:e[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&a.color||"none"},e[3]?h.dashstyle=e[3]:"square"!==b.linecap&&(h["stroke-linecap"]=h["stroke-linejoin"]="round"),h=a[d].attr(h).shadow(2>f&&b.shadow));h&&(h.startX=c.xMap,h.isArea=c.isArea)})},applyZones:function(){var a=this,b=this.chart,c=b.renderer,e=this.zones,f,d,h=this.clips||[],k,g=this.graph,n=this.area,q=Math.max(b.chartWidth,b.chartHeight),l=this[(this.zoneAxis||"y")+"Axis"],t,p,x=b.inverted,w,v,I,K,C=!1;e.length&&(g||n)&&l&&void 0!==
l.min&&(p=l.reversed,w=l.horiz,g&&g.hide(),n&&n.hide(),t=l.getExtremes(),r(e,function(e,m){f=p?w?b.plotWidth:0:w?0:l.toPixels(t.min);f=Math.min(Math.max(u(d,f),0),q);d=Math.min(Math.max(Math.round(l.toPixels(u(e.value,t.max),!0)),0),q);C&&(f=d=l.toPixels(t.max));v=Math.abs(f-d);I=Math.min(f,d);K=Math.max(f,d);l.isXAxis?(k={x:x?K:I,y:0,width:v,height:q},w||(k.x=b.plotHeight-k.x)):(k={x:0,y:x?K:I,width:q,height:v},w&&(k.y=b.plotWidth-k.y));x&&c.isVML&&(k=l.isXAxis?{x:0,y:p?I:K,height:k.width,width:b.chartWidth}:
{x:k.y-b.plotLeft-b.spacingBox.x,y:0,width:k.height,height:b.chartHeight});h[m]?h[m].animate(k):(h[m]=c.clipRect(k),g&&a["zone-graph-"+m].clip(h[m]),n&&a["zone-area-"+m].clip(h[m]));C=e.value>t.max}),this.clips=h)},invertGroups:function(a){function b(){var b={width:c.yAxis.len,height:c.xAxis.len};r(["group","markerGroup"],function(e){c[e]&&c[e].attr(b).invert(a)})}var c=this,e;c.xAxis&&(e=E(c.chart,"resize",b),E(c,"destroy",e),b(a),c.invertGroups=b)},plotGroup:function(a,b,c,e,f){var d=this[a],h=
!d;h&&(this[a]=d=this.chart.renderer.g(b).attr({zIndex:e||.1}).add(f),d.addClass("highcharts-series-"+this.index+" highcharts-"+this.type+"-series highcharts-color-"+this.colorIndex+" "+(this.options.className||"")));d.attr({visibility:c})[h?"attr":"animate"](this.getPlotBox());return d},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;a.inverted&&(b=c,c=this.xAxis);return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,
b=a.chart,c,e=a.options,f=!!a.animate&&b.renderer.isSVG&&A(e.animation).duration,d=a.visible?"inherit":"hidden",h=e.zIndex,k=a.hasRendered,g=b.seriesGroup,n=b.inverted;c=a.plotGroup("group","series",d,h,g);a.markerGroup=a.plotGroup("markerGroup","markers",d,h,g);f&&a.animate(!0);c.inverted=a.isCartesian?n:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(n);
!1===e.clip||a.sharedClipKey||k||c.clip(b.clipRect);f&&a.animate();k||(a.animationTimeout=I(function(){a.afterAnimate()},f));a.isDirty=a.isDirtyData=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,b=this.isDirty||this.isDirtyData,c=this.group,e=this.xAxis,f=this.yAxis;c&&(a.inverted&&c.attr({width:a.plotWidth,height:a.plotHeight}),c.animate({translateX:u(e&&e.left,a.plotLeft),translateY:u(f&&f.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdDimensions:1,kdAxisArray:["clientX",
"plotY"],searchPoint:function(a,b){var c=this.xAxis,e=this.yAxis,f=this.chart.inverted;return this.searchKDTree({clientX:f?c.len-a.chartY+c.pos:a.chartX-c.pos,plotY:f?e.len-a.chartX+e.pos:a.chartY-e.pos},b)},buildKDTree:function(){function a(c,e,f){var d,h;if(h=c&&c.length)return d=b.kdAxisArray[e%f],c.sort(function(a,b){return a[d]-b[d]}),h=Math.floor(h/2),{point:c[h],left:a(c.slice(0,h),e+1,f),right:a(c.slice(h+1),e+1,f)}}var b=this,c=b.kdDimensions;delete b.kdTree;I(function(){b.kdTree=a(b.getValidPoints(null,
!b.directTouch),c,c)},b.options.kdNow?0:1)},searchKDTree:function(a,b){function c(a,b,k,g){var m=b.point,n=e.kdAxisArray[k%g],q,t,u=m;t=l(a[f])&&l(m[f])?Math.pow(a[f]-m[f],2):null;q=l(a[d])&&l(m[d])?Math.pow(a[d]-m[d],2):null;q=(t||0)+(q||0);m.dist=l(q)?Math.sqrt(q):Number.MAX_VALUE;m.distX=l(t)?Math.sqrt(t):Number.MAX_VALUE;n=a[n]-m[n];q=0>n?"left":"right";t=0>n?"right":"left";b[q]&&(q=c(a,b[q],k+1,g),u=q[h]<u[h]?q:m);b[t]&&Math.sqrt(n*n)<u[h]&&(a=c(a,b[t],k+1,g),u=a[h]<u[h]?a:u);return u}var e=
this,f=this.kdAxisArray[0],d=this.kdAxisArray[1],h=b?"distX":"dist";this.kdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,this.kdDimensions,this.kdDimensions)}})})(M);(function(a){function E(a,f,b,d,g){var n=a.chart.inverted;this.axis=a;this.isNegative=b;this.options=f;this.x=d;this.total=null;this.points={};this.stack=g;this.rightCliff=this.leftCliff=0;this.alignOptions={align:f.align||(n?b?"left":"right":"center"),verticalAlign:f.verticalAlign||(n?"middle":b?"bottom":"top"),y:l(f.y,
n?4:b?14:-6),x:l(f.x,n?b?-6:6:0)};this.textAlign=f.textAlign||(n?b?"right":"left":"center")}var A=a.Axis,F=a.Chart,H=a.correctFloat,p=a.defined,d=a.destroyObjectProperties,g=a.each,v=a.format,l=a.pick;a=a.Series;E.prototype={destroy:function(){d(this,this.axis)},render:function(a){var f=this.options,b=f.format,b=b?v(b,this):f.formatter.call(this);this.label?this.label.attr({text:b,visibility:"hidden"}):this.label=this.axis.chart.renderer.text(b,null,null,f.useHTML).css(f.style).attr({align:this.textAlign,
rotation:f.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,f){var b=this.axis,d=b.chart,g=d.inverted,l=b.reversed,l=this.isNegative&&!l||!this.isNegative&&l,k=b.translate(b.usePercentage?100:this.total,0,0,0,1),b=b.translate(0),b=Math.abs(k-b);a=d.xAxis[0].translate(this.x)+a;var e=d.plotHeight,g={x:g?l?k:k-b:a,y:g?e-a-f:l?e-k-b:e-k,width:g?b:f,height:g?f:b};if(f=this.label)f.align(this.alignOptions,null,g),g=f.alignAttr,f[!1===this.options.crop||d.isInsidePlot(g.x,g.y)?"show":"hide"](!0)}};
F.prototype.getStacks=function(){var a=this;g(a.yAxis,function(a){a.stacks&&a.hasVisibleSeries&&(a.oldStacks=a.stacks)});g(a.series,function(f){!f.options.stacking||!0!==f.visible&&!1!==a.options.chart.ignoreHiddenSeries||(f.stackKey=f.type+l(f.options.stack,""))})};A.prototype.buildStacks=function(){var a=this.series,f,b=l(this.options.reversedStacks,!0),d=a.length,g;if(!this.isXAxis){this.usePercentage=!1;for(g=d;g--;)a[b?g:d-g-1].setStackedPoints();for(g=d;g--;)f=a[b?g:d-g-1],f.setStackCliffs&&
f.setStackCliffs();if(this.usePercentage)for(g=0;g<d;g++)a[g].setPercentStacks()}};A.prototype.renderStackTotals=function(){var a=this.chart,f=a.renderer,b=this.stacks,d,g,l=this.stackTotalGroup;l||(this.stackTotalGroup=l=f.g("stack-labels").attr({visibility:"visible",zIndex:6}).add());l.translate(a.plotLeft,a.plotTop);for(d in b)for(g in a=b[d],a)a[g].render(l)};A.prototype.resetStacks=function(){var a=this.stacks,f,b;if(!this.isXAxis)for(f in a)for(b in a[f])a[f][b].touched<this.stacksTouched?(a[f][b].destroy(),
delete a[f][b]):(a[f][b].total=null,a[f][b].cum=null)};A.prototype.cleanStacks=function(){var a,f,b;if(!this.isXAxis)for(f in this.oldStacks&&(a=this.stacks=this.oldStacks),a)for(b in a[f])a[f][b].cum=a[f][b].total};a.prototype.setStackedPoints=function(){if(this.options.stacking&&(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var a=this.processedXData,f=this.processedYData,b=[],d=f.length,g=this.options,t=g.threshold,k=g.startFromThreshold?t:0,e=g.stack,g=g.stacking,h=this.stackKey,
v="-"+h,u=this.negStacks,c=this.yAxis,q=c.stacks,x=c.oldStacks,K,I,J,D,G,A,F;c.stacksTouched+=1;for(G=0;G<d;G++)A=a[G],F=f[G],K=this.getStackIndicator(K,A,this.index),D=K.key,J=(I=u&&F<(k?0:t))?v:h,q[J]||(q[J]={}),q[J][A]||(x[J]&&x[J][A]?(q[J][A]=x[J][A],q[J][A].total=null):q[J][A]=new E(c,c.options.stackLabels,I,A,e)),J=q[J][A],null!==F&&(J.points[D]=J.points[this.index]=[l(J.cum,k)],p(J.cum)||(J.base=D),J.touched=c.stacksTouched,0<K.index&&!1===this.singleStacks&&(J.points[D][0]=J.points[this.index+
","+A+",0"][0])),"percent"===g?(I=I?h:v,u&&q[I]&&q[I][A]?(I=q[I][A],J.total=I.total=Math.max(I.total,J.total)+Math.abs(F)||0):J.total=H(J.total+(Math.abs(F)||0))):J.total=H(J.total+(F||0)),J.cum=l(J.cum,k)+(F||0),null!==F&&(J.points[D].push(J.cum),b[G]=J.cum);"percent"===g&&(c.usePercentage=!0);this.stackedYData=b;c.oldStacks={}}};a.prototype.setPercentStacks=function(){var a=this,f=a.stackKey,b=a.yAxis.stacks,d=a.processedXData,l;g([f,"-"+f],function(f){for(var g=d.length,e,h;g--;)if(e=d[g],l=a.getStackIndicator(l,
e,a.index,f),e=(h=b[f]&&b[f][e])&&h.points[l.key])h=h.total?100/h.total:0,e[0]=H(e[0]*h),e[1]=H(e[1]*h),a.stackedYData[g]=e[1]})};a.prototype.getStackIndicator=function(a,f,b,d){!p(a)||a.x!==f||d&&a.key!==d?a={x:f,index:0,key:d}:a.index++;a.key=[b,f,a.index].join();return a}})(M);(function(a){var E=a.addEvent,A=a.animate,F=a.Axis,H=a.createElement,p=a.css,d=a.defined,g=a.each,v=a.erase,l=a.extend,r=a.fireEvent,f=a.inArray,b=a.isNumber,n=a.isObject,w=a.merge,t=a.pick,k=a.Point,e=a.Series,h=a.seriesTypes,
C=a.setAnimation,u=a.splat;l(a.Chart.prototype,{addSeries:function(a,b,e){var c,f=this;a&&(b=t(b,!0),r(f,"addSeries",{options:a},function(){c=f.initSeries(a);f.isDirtyLegend=!0;f.linkSeries();b&&f.redraw(e)}));return c},addAxis:function(a,b,e,f){var c=b?"xAxis":"yAxis",d=this.options;a=w(a,{index:this[c].length,isX:b});new F(this,a);d[c]=u(d[c]||{});d[c].push(a);t(e,!0)&&this.redraw(f)},showLoading:function(a){var b=this,c=b.options,e=b.loadingDiv,f=c.loading,d=function(){e&&p(e,{left:b.plotLeft+
"px",top:b.plotTop+"px",width:b.plotWidth+"px",height:b.plotHeight+"px"})};e||(b.loadingDiv=e=H("div",{className:"highcharts-loading highcharts-loading-hidden"},null,b.container),b.loadingSpan=H("span",{className:"highcharts-loading-inner"},null,e),E(b,"redraw",d));e.className="highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;p(e,l(f.style,{zIndex:10}));p(b.loadingSpan,f.labelStyle);b.loadingShown||(p(e,{opacity:0,display:""}),A(e,{opacity:f.style.opacity||.5},{duration:f.showDuration||
0}));b.loadingShown=!0;d()},hideLoading:function(){var a=this.options,b=this.loadingDiv;b&&(b.className="highcharts-loading highcharts-loading-hidden",A(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){p(b,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions".split(" "),update:function(a,e){var c,h={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},k=a.chart,n,q;if(k){w(!0,this.options.chart,k);"className"in k&&this.setClassName(k.className);if("inverted"in k||"polar"in k)this.propFromSeries(),n=!0;for(c in k)k.hasOwnProperty(c)&&(-1!==f("chart."+c,this.propsRequireUpdateSeries)&&(q=!0),-1!==f(c,this.propsRequireDirtyBox)&&(this.isDirtyBox=
!0));"style"in k&&this.renderer.setStyle(k.style)}for(c in a){if(this[c]&&"function"===typeof this[c].update)this[c].update(a[c],!1);else if("function"===typeof this[h[c]])this[h[c]](a[c]);"chart"!==c&&-1!==f(c,this.propsRequireUpdateSeries)&&(q=!0)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&w(!0,this.options.plotOptions,a.plotOptions);g(["xAxis","yAxis","series"],function(b){a[b]&&g(u(a[b]),function(a){var c=d(a.id)&&this.get(a.id)||this[b][0];c&&c.coll===b&&c.update(a,!1)},this)},this);
n&&g(this.axes,function(a){a.update({},!1)});q&&g(this.series,function(a){a.update({},!1)});a.loading&&w(!0,this.options.loading,a.loading);c=k&&k.width;k=k&&k.height;b(c)&&c!==this.chartWidth||b(k)&&k!==this.chartHeight?this.setSize(c,k):t(e,!0)&&this.redraw()},setSubtitle:function(a){this.setTitle(void 0,a)}});l(k.prototype,{update:function(a,b,e,f){function c(){d.applyOptions(a);null===d.y&&g&&(d.graphic=g.destroy());n(a,!0)&&(g&&g.element&&a&&a.marker&&a.marker.symbol&&(d.graphic=g.destroy()),
a&&a.dataLabels&&d.dataLabel&&(d.dataLabel=d.dataLabel.destroy()));k=d.index;h.updateParallelArrays(d,k);m.data[k]=n(m.data[k],!0)?d.options:a;h.isDirty=h.isDirtyData=!0;!h.fixedBox&&h.hasCartesianSeries&&(q.isDirtyBox=!0);"point"===m.legendType&&(q.isDirtyLegend=!0);b&&q.redraw(e)}var d=this,h=d.series,g=d.graphic,k,q=h.chart,m=h.options;b=t(b,!0);!1===f?c():d.firePointEvent("update",{options:a},c)},remove:function(a,b){this.series.removePoint(f(this,this.series.data),a,b)}});l(e.prototype,{addPoint:function(a,
b,e,f){var c=this.options,d=this.data,h=this.chart,g=this.xAxis&&this.xAxis.names,k=c.data,n,m,q=this.xData,l,u;b=t(b,!0);n={series:this};this.pointClass.prototype.applyOptions.apply(n,[a]);u=n.x;l=q.length;if(this.requireSorting&&u<q[l-1])for(m=!0;l&&q[l-1]>u;)l--;this.updateParallelArrays(n,"splice",l,0,0);this.updateParallelArrays(n,l);g&&n.name&&(g[u]=n.name);k.splice(l,0,a);m&&(this.data.splice(l,0,null),this.processData());"point"===c.legendType&&this.generatePoints();e&&(d[0]&&d[0].remove?
d[0].remove(!1):(d.shift(),this.updateParallelArrays(n,"shift"),k.shift()));this.isDirtyData=this.isDirty=!0;b&&h.redraw(f)},removePoint:function(a,b,e){var c=this,f=c.data,d=f[a],h=c.points,g=c.chart,k=function(){h&&h.length===f.length&&h.splice(a,1);f.splice(a,1);c.options.data.splice(a,1);c.updateParallelArrays(d||{series:c},"splice",a,1);d&&d.destroy();c.isDirty=!0;c.isDirtyData=!0;b&&g.redraw()};C(e,g);b=t(b,!0);d?d.firePointEvent("remove",null,k):k()},remove:function(a,b,e){function c(){f.destroy();
d.isDirtyLegend=d.isDirtyBox=!0;d.linkSeries();t(a,!0)&&d.redraw(b)}var f=this,d=f.chart;!1!==e?r(f,"remove",null,c):c()},update:function(a,b){var c=this,e=this.chart,f=this.userOptions,d=this.type,k=a.type||f.type||e.options.chart.type,n=h[d].prototype,q=["group","markerGroup","dataLabelsGroup"],u;if(k&&k!==d||void 0!==a.zIndex)q.length=0;g(q,function(a){q[a]=c[a];delete c[a]});a=w(f,{animation:!1,index:this.index,pointStart:this.xData[0]},{data:this.options.data},a);this.remove(!1,null,!1);for(u in n)this[u]=
void 0;l(this,h[k||d].prototype);g(q,function(a){c[a]=q[a]});this.init(e,a);e.linkSeries();t(b,!0)&&e.redraw(!1)}});l(F.prototype,{update:function(a,b){var c=this.chart;a=c.options[this.coll][this.options.index]=w(this.userOptions,a);this.destroy(!0);this.init(c,l(a,{events:void 0}));c.isDirtyBox=!0;t(b,!0)&&c.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,e=this.series,f=e.length;f--;)e[f]&&e[f].remove(!1);v(b.axes,this);v(b[c],this);b.options[c].splice(this.options.index,1);g(b[c],
function(a,b){a.options.index=b});this.destroy();b.isDirtyBox=!0;t(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})})(M);(function(a){var E=a.color,A=a.each,F=a.map,H=a.pick,p=a.Series,d=a.seriesType;d("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(){var a=[],d=[],l=this.xAxis,p=this.yAxis,f=p.stacks[this.stackKey],b={},n=this.points,w=this.index,t=p.series,k=t.length,e,h=H(p.options.reversedStacks,
!0)?1:-1,C,u;if(this.options.stacking){for(C=0;C<n.length;C++)b[n[C].x]=n[C];for(u in f)null!==f[u].total&&d.push(u);d.sort(function(a,b){return a-b});e=F(t,function(){return this.visible});A(d,function(c,g){var n=0,q,u;if(b[c]&&!b[c].isNull)a.push(b[c]),A([-1,1],function(a){var n=1===a?"rightNull":"leftNull",l=0,t=f[d[g+a]];if(t)for(C=w;0<=C&&C<k;)q=t.points[C],q||(C===w?b[c][n]=!0:e[C]&&(u=f[c].points[C])&&(l-=u[1]-u[0])),C+=h;b[c][1===a?"rightCliff":"leftCliff"]=l});else{for(C=w;0<=C&&C<k;){if(q=
f[c].points[C]){n=q[1];break}C+=h}n=p.toPixels(n,!0);a.push({isNull:!0,plotX:l.toPixels(c,!0),plotY:n,yBottom:n})}})}return a},getGraphPath:function(a){var d=p.prototype.getGraphPath,g=this.options,r=g.stacking,f=this.yAxis,b,n,w=[],t=[],k=this.index,e,h=f.stacks[this.stackKey],C=g.threshold,u=f.getThreshold(g.threshold),c,g=g.connectNulls||"percent"===r,q=function(b,c,d){var g=a[b];b=r&&h[g.x].points[k];var n=g[d+"Null"]||0;d=g[d+"Cliff"]||0;var q,l,g=!0;d||n?(q=(n?b[0]:b[1])+d,l=b[0]+d,g=!!n):!r&&
a[c]&&a[c].isNull&&(q=l=C);void 0!==q&&(t.push({plotX:e,plotY:null===q?u:f.getThreshold(q),isNull:g}),w.push({plotX:e,plotY:null===l?u:f.getThreshold(l),doCurve:!1}))};a=a||this.points;r&&(a=this.getStackPoints());for(b=0;b<a.length;b++)if(n=a[b].isNull,e=H(a[b].rectPlotX,a[b].plotX),c=H(a[b].yBottom,u),!n||g)g||q(b,b-1,"left"),n&&!r&&g||(t.push(a[b]),w.push({x:b,plotX:e,plotY:c})),g||q(b,b+1,"right");b=d.call(this,t,!0,!0);w.reversed=!0;n=d.call(this,w,!0,!0);n.length&&(n[0]="L");n=b.concat(n);d=
d.call(this,t,!1,g);n.xMap=b.xMap;this.areaPath=n;return d},drawGraph:function(){this.areaPath=[];p.prototype.drawGraph.apply(this);var a=this,d=this.areaPath,l=this.options,r=[["area","highcharts-area",this.color,l.fillColor]];A(this.zones,function(d,b){r.push(["zone-area-"+b,"highcharts-area highcharts-zone-area-"+b+" "+d.className,d.color||a.color,d.fillColor||l.fillColor])});A(r,function(f){var b=f[0],g=a[b];g?(g.endX=d.xMap,g.animate({d:d})):(g=a[b]=a.chart.renderer.path(d).addClass(f[1]).attr({fill:H(f[3],
E(f[2]).setOpacity(H(l.fillOpacity,.75)).get()),zIndex:0}).add(a.group),g.isArea=!0);g.startX=d.xMap;g.shiftUnit=l.step?2:1})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var E=a.pick;a=a.seriesType;a("spline","line",{},{getPointSpline:function(a,F,H){var p=F.plotX,d=F.plotY,g=a[H-1];H=a[H+1];var v,l,r,f;if(g&&!g.isNull&&!1!==g.doCurve&&H&&!H.isNull&&!1!==H.doCurve){a=g.plotY;r=H.plotX;H=H.plotY;var b=0;v=(1.5*p+g.plotX)/2.5;l=(1.5*d+a)/2.5;r=(1.5*p+r)/2.5;f=(1.5*d+H)/2.5;
r!==v&&(b=(f-l)*(r-p)/(r-v)+d-f);l+=b;f+=b;l>a&&l>d?(l=Math.max(a,d),f=2*d-l):l<a&&l<d&&(l=Math.min(a,d),f=2*d-l);f>H&&f>d?(f=Math.max(H,d),l=2*d-f):f<H&&f<d&&(f=Math.min(H,d),l=2*d-f);F.rightContX=r;F.rightContY=f}F=["C",E(g.rightContX,g.plotX),E(g.rightContY,g.plotY),E(v,p),E(l,d),p,d];g.rightContX=g.rightContY=null;return F}})})(M);(function(a){var E=a.seriesTypes.area.prototype,A=a.seriesType;A("areaspline","spline",a.defaultPlotOptions.area,{getStackPoints:E.getStackPoints,getGraphPath:E.getGraphPath,
setStackCliffs:E.setStackCliffs,drawGraph:E.drawGraph,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var E=a.animObject,A=a.color,F=a.each,H=a.extend,p=a.isNumber,d=a.merge,g=a.pick,v=a.Series,l=a.seriesType,r=a.svg;l("column","line",{borderRadius:0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1,shadow:!1},select:{color:"#cccccc",borderColor:"#000000",shadow:!1}},dataLabels:{align:null,verticalAlign:null,
y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){v.prototype.init.apply(this,arguments);var a=this,b=a.chart;b.hasRendered&&F(b.series,function(b){b.type===a.type&&(b.isDirty=!0)})},getColumnMetrics:function(){var a=this,b=a.options,d=a.xAxis,l=a.yAxis,t=d.reversed,k,e={},h=0;!1===b.grouping?h=1:F(a.chart.series,function(b){var c=
b.options,d=b.yAxis,f;b.type===a.type&&b.visible&&l.len===d.len&&l.pos===d.pos&&(c.stacking?(k=b.stackKey,void 0===e[k]&&(e[k]=h++),f=e[k]):!1!==c.grouping&&(f=h++),b.columnIndex=f)});var p=Math.min(Math.abs(d.transA)*(d.ordinalSlope||b.pointRange||d.closestPointRange||d.tickInterval||1),d.len),u=p*b.groupPadding,c=(p-2*u)/h,b=Math.min(b.maxPointWidth||d.len,g(b.pointWidth,c*(1-2*b.pointPadding)));a.columnMetrics={width:b,offset:(c-b)/2+(u+((a.columnIndex||0)+(t?1:0))*c-p/2)*(t?-1:1)};return a.columnMetrics},
crispCol:function(a,b,d,g){var f=this.chart,k=this.borderWidth,e=-(k%2?.5:0),k=k%2?.5:1;f.inverted&&f.renderer.isVML&&(k+=1);d=Math.round(a+d)+e;a=Math.round(a)+e;g=Math.round(b+g)+k;e=.5>=Math.abs(b)&&.5<g;b=Math.round(b)+k;g-=b;e&&g&&(--b,g+=1);return{x:a,y:b,width:d-a,height:g}},translate:function(){var a=this,b=a.chart,d=a.options,l=a.dense=2>a.closestPointRange*a.xAxis.transA,l=a.borderWidth=g(d.borderWidth,l?0:1),t=a.yAxis,k=a.translatedThreshold=t.getThreshold(d.threshold),e=g(d.minPointLength,
5),h=a.getColumnMetrics(),p=h.width,u=a.barW=Math.max(p,1+2*l),c=a.pointXOffset=h.offset;b.inverted&&(k-=.5);d.pointPadding&&(u=Math.ceil(u));v.prototype.translate.apply(a);F(a.points,function(d){var f=g(d.yBottom,k),h=999+Math.abs(f),h=Math.min(Math.max(-h,d.plotY),t.len+h),n=d.plotX+c,l=u,q=Math.min(h,f),r,v=Math.max(h,f)-q;Math.abs(v)<e&&e&&(v=e,r=!t.reversed&&!d.negative||t.reversed&&d.negative,q=Math.abs(q-k)>e?f-e:k-(r?e:0));d.barX=n;d.pointWidth=p;d.tooltipPos=b.inverted?[t.len+t.pos-b.plotLeft-
h,a.xAxis.len-n-l/2,v]:[n+l/2,h+t.pos-b.plotTop,v];d.shapeType="rect";d.shapeArgs=a.crispCol.apply(a,d.isNull?[d.plotX,t.len/2,0,0]:[n,q,l,v])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},pointAttribs:function(a,b){var d=this.options,f=this.pointAttrToOptions||{},g=f.stroke||"borderColor",k=f["stroke-width"]||"borderWidth",e=a&&a.color||this.color,h=a[g]||d[g]||this.color||e,f=
d.dashStyle,l;a&&this.zones.length&&(e=(e=a.getZone())&&e.color||a.options.color||this.color);b&&(b=d.states[b],l=b.brightness,e=b.color||void 0!==l&&A(e).brighten(b.brightness).get()||e,h=b[g]||h,f=b.dashStyle||f);a={fill:e,stroke:h,"stroke-width":a[k]||d[k]||this[k]||0};d.borderRadius&&(a.r=d.borderRadius);f&&(a.dashstyle=f);return a},drawPoints:function(){var a=this,b=this.chart,g=a.options,l=b.renderer,t=g.animationLimit||250,k;F(a.points,function(e){var f=e.graphic;if(p(e.plotY)&&null!==e.y){k=
e.shapeArgs;if(f)f[b.pointCount<t?"animate":"attr"](d(k));else e.graphic=f=l[e.shapeType](k).attr({"class":e.getClassName()}).add(e.group||a.group);f.attr(a.pointAttribs(e,e.selected&&"select")).shadow(g.shadow,null,g.stacking&&!g.borderRadius)}else f&&(e.graphic=f.destroy())})},animate:function(a){var b=this,d=this.yAxis,f=b.options,g=this.chart.inverted,k={};r&&(a?(k.scaleY=.001,a=Math.min(d.pos+d.len,Math.max(d.pos,d.toPixels(f.threshold))),g?k.translateX=a-d.len:k.translateY=a,b.group.attr(k)):
(k[g?"translateX":"translateY"]=d.pos,b.group.animate(k,H(E(b.options.animation),{step:function(a,d){b.group.attr({scaleY:Math.max(.001,d.pos)})}})),b.animate=null))},remove:function(){var a=this,b=a.chart;b.hasRendered&&F(b.series,function(b){b.type===a.type&&(b.isDirty=!0)});v.prototype.remove.apply(a,arguments)}})})(M);(function(a){a=a.seriesType;a("bar","column",null,{inverted:!0})})(M);(function(a){var E=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',
pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,kdDimensions:2,drawGraph:function(){this.options.lineWidth&&E.prototype.drawGraph.call(this)}})})(M);(function(a){var E=a.pick,A=a.relativeLength;a.CenteredSeriesMixin={getCenter:function(){var a=this.options,H=this.chart,p=2*(a.slicedOffset||0),d=H.plotWidth-2*p,H=H.plotHeight-
2*p,g=a.center,g=[E(g[0],"50%"),E(g[1],"50%"),a.size||"100%",a.innerSize||0],v=Math.min(d,H),l,r;for(l=0;4>l;++l)r=g[l],a=2>l||2===l&&/%$/.test(r),g[l]=A(r,[d,H,v,g[2]][l])+(a?p:0);g[3]>g[2]&&(g[3]=g[2]);return g}}})(M);(function(a){var E=a.addEvent,A=a.defined,F=a.each,H=a.extend,p=a.inArray,d=a.noop,g=a.pick,v=a.Point,l=a.Series,r=a.seriesType,f=a.setAnimation;r("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,enabled:!0,formatter:function(){return null===this.y?
void 0:this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,states:{hover:{brightness:.1,shadow:!1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:a.seriesTypes.column.prototype.pointAttribs,animate:function(a){var b=this,d=b.points,f=b.startAngleRad;a||(F(d,function(a){var e=
a.graphic,d=a.shapeArgs;e&&(e.attr({r:a.startR||b.center[3]/2,start:f,end:f}),e.animate({r:d.r,start:d.start,end:d.end},b.options.animation))}),b.animate=null)},updateTotals:function(){var a,d=0,f=this.points,g=f.length,k,e=this.options.ignoreHiddenPoint;for(a=0;a<g;a++)k=f[a],0>k.y&&(k.y=null),d+=e&&!k.visible?0:k.y;this.total=d;for(a=0;a<g;a++)k=f[a],k.percentage=0<d&&(k.visible||!e)?k.y/d*100:0,k.total=d},generatePoints:function(){l.prototype.generatePoints.call(this);this.updateTotals()},translate:function(a){this.generatePoints();
var b=0,d=this.options,f=d.slicedOffset,k=f+(d.borderWidth||0),e,h,l,u=d.startAngle||0,c=this.startAngleRad=Math.PI/180*(u-90),u=(this.endAngleRad=Math.PI/180*(g(d.endAngle,u+360)-90))-c,q=this.points,p=d.dataLabels.distance,d=d.ignoreHiddenPoint,r,v=q.length,A;a||(this.center=a=this.getCenter());this.getX=function(b,c){l=Math.asin(Math.min((b-a[1])/(a[2]/2+p),1));return a[0]+(c?-1:1)*Math.cos(l)*(a[2]/2+p)};for(r=0;r<v;r++){A=q[r];e=c+b*u;if(!d||A.visible)b+=A.percentage/100;h=c+b*u;A.shapeType=
"arc";A.shapeArgs={x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:Math.round(1E3*e)/1E3,end:Math.round(1E3*h)/1E3};l=(h+e)/2;l>1.5*Math.PI?l-=2*Math.PI:l<-Math.PI/2&&(l+=2*Math.PI);A.slicedTranslation={translateX:Math.round(Math.cos(l)*f),translateY:Math.round(Math.sin(l)*f)};e=Math.cos(l)*a[2]/2;h=Math.sin(l)*a[2]/2;A.tooltipPos=[a[0]+.7*e,a[1]+.7*h];A.half=l<-Math.PI/2||l>Math.PI/2?1:0;A.angle=l;k=Math.min(k,p/5);A.labelPos=[a[0]+e+Math.cos(l)*p,a[1]+h+Math.sin(l)*p,a[0]+e+Math.cos(l)*k,a[1]+h+Math.sin(l)*
k,a[0]+e,a[1]+h,0>p?"center":A.half?"right":"left",l]}},drawGraph:null,drawPoints:function(){var a=this,d=a.chart.renderer,f,g,k,e,h=a.options.shadow;h&&!a.shadowGroup&&(a.shadowGroup=d.g("shadow").add(a.group));F(a.points,function(b){if(null!==b.y){g=b.graphic;e=b.shapeArgs;f=b.sliced?b.slicedTranslation:{};var l=b.shadowGroup;h&&!l&&(l=b.shadowGroup=d.g("shadow").add(a.shadowGroup));l&&l.attr(f);k=a.pointAttribs(b,b.selected&&"select");g?g.setRadialReference(a.center).attr(k).animate(H(e,f)):(b.graphic=
g=d[b.shapeType](e).addClass(b.getClassName()).setRadialReference(a.center).attr(f).add(a.group),b.visible||g.attr({visibility:"hidden"}),g.attr(k).attr({"stroke-linejoin":"round"}).shadow(h,l))}})},searchPoint:d,sortByAngle:function(a,d){a.sort(function(a,b){return void 0!==a.angle&&(b.angle-a.angle)*d})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,getCenter:a.CenteredSeriesMixin.getCenter,getSymbol:d},{init:function(){v.prototype.init.apply(this,arguments);var a=this,d;a.name=g(a.name,"Slice");
d=function(b){a.slice("select"===b.type)};E(a,"select",d);E(a,"unselect",d);return a},setVisible:function(a,d){var b=this,f=b.series,k=f.chart,e=f.options.ignoreHiddenPoint;d=g(d,e);a!==b.visible&&(b.visible=b.options.visible=a=void 0===a?!b.visible:a,f.options.data[p(b,f.data)]=b.options,F(["graphic","dataLabel","connector","shadowGroup"],function(e){if(b[e])b[e][a?"show":"hide"](!0)}),b.legendItem&&k.legend.colorizeItem(b,a),a||"hover"!==b.state||b.setState(""),e&&(f.isDirty=!0),d&&k.redraw())},
slice:function(a,d,l){var b=this.series;f(l,b.chart);g(d,!0);this.sliced=this.options.sliced=a=A(a)?a:!this.sliced;b.options.data[p(this,b.data)]=this.options;a=a?this.slicedTranslation:{translateX:0,translateY:0};this.graphic.animate(a);this.shadowGroup&&this.shadowGroup.animate(a)},haloPath:function(a){var b=this.shapeArgs;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(b.x,b.y,b.r+a,b.r+a,{innerR:this.shapeArgs.r,start:b.start,end:b.end})}})})(M);(function(a){var E=
a.addEvent,A=a.arrayMax,F=a.defined,H=a.each,p=a.extend,d=a.format,g=a.map,v=a.merge,l=a.noop,r=a.pick,f=a.relativeLength,b=a.Series,n=a.seriesTypes,w=a.stableSort;a.distribute=function(a,b){function e(a,b){return a.target-b.target}var d,f=!0,k=a,c=[],l;l=0;for(d=a.length;d--;)l+=a[d].size;if(l>b){w(a,function(a,b){return(b.rank||0)-(a.rank||0)});for(l=d=0;l<=b;)l+=a[d].size,d++;c=a.splice(d-1,a.length)}w(a,e);for(a=g(a,function(a){return{size:a.size,targets:[a.target]}});f;){for(d=a.length;d--;)f=
a[d],l=(Math.min.apply(0,f.targets)+Math.max.apply(0,f.targets))/2,f.pos=Math.min(Math.max(0,l-f.size/2),b-f.size);d=a.length;for(f=!1;d--;)0<d&&a[d-1].pos+a[d-1].size>a[d].pos&&(a[d-1].size+=a[d].size,a[d-1].targets=a[d-1].targets.concat(a[d].targets),a[d-1].pos+a[d-1].size>b&&(a[d-1].pos=b-a[d-1].size),a.splice(d,1),f=!0)}d=0;H(a,function(a){var b=0;H(a.targets,function(){k[d].pos=a.pos+b;b+=k[d].size;d++})});k.push.apply(k,c);w(k,e)};b.prototype.drawDataLabels=function(){var a=this,b=a.options,
e=b.dataLabels,f=a.points,g,l,c=a.hasRendered||0,q,n,w=r(e.defer,!0),I=a.chart.renderer;if(e.enabled||a._hasPointLabels)a.dlProcessOptions&&a.dlProcessOptions(e),n=a.plotGroup("dataLabelsGroup","data-labels",w&&!c?"hidden":"visible",e.zIndex||6),w&&(n.attr({opacity:+c}),c||E(a,"afterAnimate",function(){a.visible&&n.show(!0);n[b.animation?"animate":"attr"]({opacity:1},{duration:200})})),l=e,H(f,function(c){var f,h=c.dataLabel,k,u,m=c.connector,t=!0,x,w={};g=c.dlOptions||c.options&&c.options.dataLabels;
f=r(g&&g.enabled,l.enabled)&&null!==c.y;if(h&&!f)c.dataLabel=h.destroy();else if(f){e=v(l,g);x=e.style;f=e.rotation;k=c.getLabelConfig();q=e.format?d(e.format,k):e.formatter.call(k,e);x.color=r(e.color,x.color,a.color,"#000000");if(h)F(q)?(h.attr({text:q}),t=!1):(c.dataLabel=h=h.destroy(),m&&(c.connector=m.destroy()));else if(F(q)){h={fill:e.backgroundColor,stroke:e.borderColor,"stroke-width":e.borderWidth,r:e.borderRadius||0,rotation:f,padding:e.padding,zIndex:1};"contrast"===x.color&&(w.color=e.inside||
0>e.distance||b.stacking?I.getContrast(c.color||a.color):"#000000");b.cursor&&(w.cursor=b.cursor);for(u in h)void 0===h[u]&&delete h[u];h=c.dataLabel=I[f?"text":"label"](q,0,-9999,e.shape,null,null,e.useHTML,null,"data-label").attr(h);h.addClass("highcharts-data-label-color-"+c.colorIndex+" "+(e.className||"")+(e.useHTML?"highcharts-tracker":""));h.css(p(x,w));h.add(n);h.shadow(e.shadow)}h&&a.alignDataLabel(c,h,e,null,t)}})};b.prototype.alignDataLabel=function(a,b,e,d,f){var g=this.chart,c=g.inverted,
h=r(a.plotX,-9999),k=r(a.plotY,-9999),l=b.getBBox(),n,t=e.rotation,v=e.align,w=this.visible&&(a.series.forceDL||g.isInsidePlot(h,Math.round(k),c)||d&&g.isInsidePlot(h,c?d.x+1:d.y+d.height-1,c)),A="justify"===r(e.overflow,"justify");w&&(n=e.style.fontSize,n=g.renderer.fontMetrics(n,b).b,d=p({x:c?g.plotWidth-k:h,y:Math.round(c?g.plotHeight-h:k),width:0,height:0},d),p(e,{width:l.width,height:l.height}),t?(A=!1,c=g.renderer.rotCorr(n,t),c={x:d.x+e.x+d.width/2+c.x,y:d.y+e.y+{top:0,middle:.5,bottom:1}[e.verticalAlign]*
d.height},b[f?"attr":"animate"](c).attr({align:v}),h=(t+720)%360,h=180<h&&360>h,"left"===v?c.y-=h?l.height:0:"center"===v?(c.x-=l.width/2,c.y-=l.height/2):"right"===v&&(c.x-=l.width,c.y-=h?0:l.height)):(b.align(e,null,d),c=b.alignAttr),A?this.justifyDataLabel(b,e,c,l,d,f):r(e.crop,!0)&&(w=g.isInsidePlot(c.x,c.y)&&g.isInsidePlot(c.x+l.width,c.y+l.height)),e.shape&&!t&&b.attr({anchorX:a.plotX,anchorY:a.plotY}));w||(b.attr({y:-9999}),b.placed=!1)};b.prototype.justifyDataLabel=function(a,b,e,d,f,g){var c=
this.chart,h=b.align,k=b.verticalAlign,l,n,u=a.box?0:a.padding||0;l=e.x+u;0>l&&("right"===h?b.align="left":b.x=-l,n=!0);l=e.x+d.width-u;l>c.plotWidth&&("left"===h?b.align="right":b.x=c.plotWidth-l,n=!0);l=e.y+u;0>l&&("bottom"===k?b.verticalAlign="top":b.y=-l,n=!0);l=e.y+d.height-u;l>c.plotHeight&&("top"===k?b.verticalAlign="bottom":b.y=c.plotHeight-l,n=!0);n&&(a.placed=!g,a.align(b,null,f))};n.pie&&(n.pie.prototype.drawDataLabels=function(){var d=this,f=d.data,e,h=d.chart,l=d.options.dataLabels,n=
r(l.connectorPadding,10),c=r(l.connectorWidth,1),q=h.plotWidth,p=h.plotHeight,v,w=l.distance,E=d.center,D=E[2]/2,G=E[1],F=0<w,N,m,z,O,M=[[],[]],y,B,Q,R,S=[0,0,0,0];d.visible&&(l.enabled||d._hasPointLabels)&&(b.prototype.drawDataLabels.apply(d),H(f,function(a){a.dataLabel&&a.visible&&(M[a.half].push(a),a.dataLabel._pos=null)}),H(M,function(b,c){var f,k,u=b.length,r,t,v;if(u)for(d.sortByAngle(b,c-.5),0<w&&(f=Math.max(0,G-D-w),k=Math.min(G+D+w,h.plotHeight),r=g(b,function(a){if(a.dataLabel)return v=
a.dataLabel.getBBox().height||21,{target:a.labelPos[1]-f+v/2,size:v,rank:a.y}}),a.distribute(r,k+v-f)),R=0;R<u;R++)e=b[R],z=e.labelPos,N=e.dataLabel,Q=!1===e.visible?"hidden":"inherit",t=z[1],r?void 0===r[R].pos?Q="hidden":(O=r[R].size,B=f+r[R].pos):B=t,y=l.justify?E[0]+(c?-1:1)*(D+w):d.getX(B<f+2||B>k-2?t:B,c),N._attr={visibility:Q,align:z[6]},N._pos={x:y+l.x+({left:n,right:-n}[z[6]]||0),y:B+l.y-10},z.x=y,z.y=B,null===d.options.size&&(m=N.width,y-m<n?S[3]=Math.max(Math.round(m-y+n),S[3]):y+m>q-n&&
(S[1]=Math.max(Math.round(y+m-q+n),S[1])),0>B-O/2?S[0]=Math.max(Math.round(-B+O/2),S[0]):B+O/2>p&&(S[2]=Math.max(Math.round(B+O/2-p),S[2])))}),0===A(S)||this.verifyDataLabelOverflow(S))&&(this.placeDataLabels(),F&&c&&H(this.points,function(a){var b;v=a.connector;if((N=a.dataLabel)&&N._pos&&a.visible){Q=N._attr.visibility;if(b=!v)a.connector=v=h.renderer.path().addClass("highcharts-data-label-connector highcharts-color-"+a.colorIndex).add(d.dataLabelsGroup),v.attr({"stroke-width":c,stroke:l.connectorColor||
a.color||"#666666"});v[b?"attr":"animate"]({d:d.connectorPath(a.labelPos)});v.attr("visibility",Q)}else v&&(a.connector=v.destroy())}))},n.pie.prototype.connectorPath=function(a){var b=a.x,d=a.y;return r(this.options.dataLabels.softConnector,!0)?["M",b+("left"===a[6]?5:-5),d,"C",b,d,2*a[2]-a[4],2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",b+("left"===a[6]?5:-5),d,"L",a[2],a[3],"L",a[4],a[5]]},n.pie.prototype.placeDataLabels=function(){H(this.points,function(a){var b=a.dataLabel;b&&a.visible&&((a=b._pos)?
(b.attr(b._attr),b[b.moved?"animate":"attr"](a),b.moved=!0):b&&b.attr({y:-9999}))})},n.pie.prototype.alignDataLabel=l,n.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,d=this.options,g=d.center,l=d.minSize||80,n,c;null!==g[0]?n=Math.max(b[2]-Math.max(a[1],a[3]),l):(n=Math.max(b[2]-a[1]-a[3],l),b[0]+=(a[3]-a[1])/2);null!==g[1]?n=Math.max(Math.min(n,b[2]-Math.max(a[0],a[2])),l):(n=Math.max(Math.min(n,b[2]-a[0]-a[2]),l),b[1]+=(a[0]-a[2])/2);n<b[2]?(b[2]=n,b[3]=Math.min(f(d.innerSize||
0,n),n),this.translate(b),this.drawDataLabels&&this.drawDataLabels()):c=!0;return c});n.column&&(n.column.prototype.alignDataLabel=function(a,d,e,f,g){var h=this.chart.inverted,c=a.series,k=a.dlBox||a.shapeArgs,l=r(a.below,a.plotY>r(this.translatedThreshold,c.yAxis.len)),n=r(e.inside,!!this.options.stacking);k&&(f=v(k),0>f.y&&(f.height+=f.y,f.y=0),k=f.y+f.height-c.yAxis.len,0<k&&(f.height-=k),h&&(f={x:c.yAxis.len-f.y-f.height,y:c.xAxis.len-f.x-f.width,width:f.height,height:f.width}),n||(h?(f.x+=l?
0:f.width,f.width=0):(f.y+=l?f.height:0,f.height=0)));e.align=r(e.align,!h||n?"center":l?"right":"left");e.verticalAlign=r(e.verticalAlign,h||n?"middle":l?"top":"bottom");b.prototype.alignDataLabel.call(this,a,d,e,f,g)})})(M);(function(a){var E=a.Chart,A=a.each,F=a.pick,H=a.addEvent;E.prototype.callbacks.push(function(a){function d(){var d=[];A(a.series,function(a){var g=a.options.dataLabels,p=a.dataLabelCollections||["dataLabel"];(g.enabled||a._hasPointLabels)&&!g.allowOverlap&&a.visible&&A(p,function(f){A(a.points,
function(a){a[f]&&(a[f].labelrank=F(a.labelrank,a.shapeArgs&&a.shapeArgs.height),d.push(a[f]))})})});a.hideOverlappingLabels(d)}d();H(a,"redraw",d)});E.prototype.hideOverlappingLabels=function(a){var d=a.length,g,p,l,r,f,b,n,w,t,k=function(a,b,d,f,c,g,k,l){return!(c>a+d||c+k<a||g>b+f||g+l<b)};for(p=0;p<d;p++)if(g=a[p])g.oldOpacity=g.opacity,g.newOpacity=1;a.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(p=0;p<d;p++)for(l=a[p],g=p+1;g<d;++g)if(r=a[g],l&&r&&l.placed&&r.placed&&0!==
l.newOpacity&&0!==r.newOpacity&&(f=l.alignAttr,b=r.alignAttr,n=l.parentGroup,w=r.parentGroup,t=2*(l.box?0:l.padding),f=k(f.x+n.translateX,f.y+n.translateY,l.width-t,l.height-t,b.x+w.translateX,b.y+w.translateY,r.width-t,r.height-t)))(l.labelrank<r.labelrank?l:r).newOpacity=0;A(a,function(a){var b,d;a&&(d=a.newOpacity,a.oldOpacity!==d&&a.placed&&(d?a.show(!0):b=function(){a.hide()},a.alignAttr.opacity=d,a[a.isOld?"animate":"attr"](a.alignAttr,null,b)),a.isOld=!0)})}})(M);(function(a){var E=a.addEvent,
A=a.Chart,F=a.createElement,H=a.css,p=a.defaultOptions,d=a.defaultPlotOptions,g=a.each,v=a.extend,l=a.fireEvent,r=a.hasTouch,f=a.inArray,b=a.isObject,n=a.Legend,w=a.merge,t=a.pick,k=a.Point,e=a.Series,h=a.seriesTypes,C=a.svg;a=a.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart,d=b.pointer,e=function(a){for(var c=a.target,d;c&&!d;)d=c.point,c=c.parentNode;if(void 0!==d&&d!==b.hoverPoint)d.onMouseOver(a)};g(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?
a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(g(a.trackerGroups,function(b){if(a[b]){a[b].addClass("highcharts-tracker").on("mouseover",e).on("mouseout",function(a){d.onTrackerMouseOut(a)});if(r)a[b].on("touchstart",e);a.options.cursor&&a[b].css(H).css({cursor:a.options.cursor})}}),a._hasTracking=!0)},drawTrackerGraph:function(){var a=this,b=a.options,d=b.trackByArea,e=[].concat(d?a.areaPath:a.graphPath),f=e.length,h=a.chart,k=h.pointer,l=h.renderer,n=h.options.tooltip.snap,
p=a.tracker,t,m=function(){if(h.hoverSeries!==a)a.onMouseOver()},v="rgba(192,192,192,"+(C?.0001:.002)+")";if(f&&!d)for(t=f+1;t--;)"M"===e[t]&&e.splice(t+1,0,e[t+1]-n,e[t+2],"L"),(t&&"M"===e[t]||t===f)&&e.splice(t,0,"L",e[t-2]+n,e[t-1]);p?p.attr({d:e}):a.graph&&(a.tracker=l.path(e).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:v,fill:d?v:"none","stroke-width":a.graph.strokeWidth()+(d?0:2*n),zIndex:2}).add(a.group),g([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",
m).on("mouseout",function(a){k.onTrackerMouseOut(a)});b.cursor&&a.css({cursor:b.cursor});if(r)a.on("touchstart",m)}))}};h.column&&(h.column.prototype.drawTracker=a.drawTrackerPoint);h.pie&&(h.pie.prototype.drawTracker=a.drawTrackerPoint);h.scatter&&(h.scatter.prototype.drawTracker=a.drawTrackerPoint);v(n.prototype,{setItemEvents:function(a,b,d){var c=this,e=c.chart,f="highcharts-legend-"+(a.series?"point":"series")+"-active";(d?b:a.legendGroup).on("mouseover",function(){a.setState("hover");e.seriesGroup.addClass(f);
b.css(c.options.itemHoverStyle)}).on("mouseout",function(){b.css(a.visible?c.itemStyle:c.itemHiddenStyle);e.seriesGroup.removeClass(f);a.setState()}).on("click",function(b){var c=function(){a.setVisible&&a.setVisible()};b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):l(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=F("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);E(a.checkbox,
"click",function(b){l(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});p.legend.itemStyle.cursor="pointer";v(A.prototype,{showResetZoom:function(){var a=this,b=p.lang,d=a.options.chart.resetZoomButton,e=d.theme,f=e.states,g="chart"===d.relativeTo?null:"plotBox";this.resetZoomButton=a.renderer.button(b.resetZoom,null,null,function(){a.zoomOut()},e,f&&f.hover).attr({align:d.position.align,title:b.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(d.position,
!1,g)},zoomOut:function(){var a=this;l(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var c,d=this.pointer,e=!1,f;!a||a.resetSelection?g(this.axes,function(a){c=a.zoom()}):g(a.xAxis.concat(a.yAxis),function(a){var b=a.axis;d[b.isXAxis?"zoomX":"zoomY"]&&(c=b.zoom(a.min,a.max),b.displayBtn&&(e=!0))});f=this.resetZoomButton;e&&!f?this.showResetZoom():!e&&b(f)&&(this.resetZoomButton=f.destroy());c&&this.redraw(t(this.options.chart.animation,a&&a.animation,100>this.pointCount))},
pan:function(a,b){var c=this,d=c.hoverPoints,e;d&&g(d,function(a){a.setState()});g("xy"===b?[1,0]:[1],function(b){b=c[b?"xAxis":"yAxis"][0];var d=b.horiz,f=b.reversed,g=a[d?"chartX":"chartY"],d=d?"mouseDownX":"mouseDownY",h=c[d],k=(b.pointRange||0)/(f?-2:2),l=b.getExtremes(),n=b.toValue(h-g,!0)+k,k=b.toValue(h+b.len-g,!0)-k,h=h>g;f&&(h=!h,f=n,n=k,k=f);b.series.length&&(h||n>Math.min(l.dataMin,l.min))&&(!h||k<Math.max(l.dataMax,l.max))&&(b.setExtremes(n,k,!1,!1,{trigger:"pan"}),e=!0);c[d]=g});e&&c.redraw(!1);
H(c.container,{cursor:"move"})}});v(k.prototype,{select:function(a,b){var c=this,d=c.series,e=d.chart;a=t(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=c.options.selected=a;d.options.data[f(c,d.data)]=c.options;c.setState(a&&"select");b||g(e.getSelectedPoints(),function(a){a.selected&&a!==c&&(a.selected=a.options.selected=!1,d.options.data[f(a,d.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a,b){var c=this.series,
d=c.chart,e=d.tooltip,f=d.hoverPoint;if(this.series){if(!b){if(f&&f!==this)f.onMouseOut();if(d.hoverSeries!==c)c.onMouseOver();d.hoverPoint=this}!e||e.shared&&!c.noSharedTooltip?e||this.setState("hover"):(this.setState("hover"),e.refresh(this,a));this.firePointEvent("mouseOver")}},onMouseOut:function(){var a=this.series.chart,b=a.hoverPoints;this.firePointEvent("mouseOut");b&&-1!==f(this,b)||(this.setState(),a.hoverPoint=null)},importEvents:function(){if(!this.hasImportedEvents){var a=w(this.series.options.point,
this.options).events,b;this.events=a;for(b in a)E(this,b,a[b]);this.hasImportedEvents=!0}},setState:function(a,b){var c=Math.floor(this.plotX),e=this.plotY,f=this.series,g=f.options.states[a]||{},h=d[f.type].marker&&f.options.marker,k=h&&!1===h.enabled,l=h&&h.states&&h.states[a]||{},n=!1===l.enabled,p=f.stateMarkerGraphic,m=this.marker||{},r=f.chart,u=f.halo,w,y=h&&f.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===g.enabled||a&&(n||k&&!1===l.enabled)||a&&m.states&&
m.states[a]&&!1===m.states[a].enabled)){y&&(w=f.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.attr(f.pointAttribs(this,a)),w&&this.graphic.animate(w,t(r.options.chart.animation,l.animation,h.animation)),p&&p.hide();else{if(a&&l){h=m.symbol||f.symbol;p&&p.currentSymbol!==h&&(p=p.destroy());if(p)p[b?"animate":"attr"]({x:w.x,y:w.y});else h&&(f.stateMarkerGraphic=p=r.renderer.symbol(h,
w.x,w.y,w.width,w.height).add(f.markerGroup),p.currentSymbol=h);p&&p.attr(f.pointAttribs(this,a))}p&&(p[a&&r.isInsidePlot(c,e,r.inverted)?"show":"hide"](),p.element.point=this)}(c=g.halo)&&c.size?(u||(f.halo=u=r.renderer.path().add(y?f.markerGroup:f.group)),u[b?"animate":"attr"]({d:this.haloPath(c.size)}),u.attr({"class":"highcharts-halo highcharts-color-"+t(this.colorIndex,f.colorIndex)}),u.attr(v({fill:this.color||f.color,"fill-opacity":c.opacity,zIndex:-1},c.attributes))):u&&u.animate({d:this.haloPath(0)});
this.state=a}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});v(e.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&l(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,d=b.tooltip,e=b.hoverPoint;b.hoverSeries=null;if(e)e.onMouseOut();this&&a.events.mouseOut&&l(this,"mouseOut");!d||a.stickyTracking||
d.shared&&!this.noSharedTooltip||d.hide();this.setState()},setState:function(a){var b=this,d=b.options,e=b.graph,f=d.states,h=d.lineWidth,d=0;a=a||"";if(b.state!==a&&(g([b.group,b.markerGroup],function(c){c&&(b.state&&c.removeClass("highcharts-series-"+b.state),a&&c.addClass("highcharts-series-"+a))}),b.state=a,!f[a]||!1!==f[a].enabled)&&(a&&(h=f[a].lineWidth||h+(f[a].lineWidthPlus||0)),e&&!e.dashstyle))for(f={"stroke-width":h},e.attr(f);b["zone-graph-"+d];)b["zone-graph-"+d].attr(f),d+=1},setVisible:function(a,
b){var c=this,d=c.chart,e=c.legendItem,f,h=d.options.chart.ignoreHiddenSeries,k=c.visible;f=(c.visible=a=c.options.visible=c.userOptions.visible=void 0===a?!k:a)?"show":"hide";g(["group","dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(c[a])c[a][f]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&g(d.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});g(c.linkedSeries,function(b){b.setVisible(a,
!1)});h&&(d.isDirtyBox=!0);!1!==b&&d.redraw();l(c,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);l(this,a?"select":"unselect")},drawTracker:a.drawTrackerGraph})})(M);(function(a){var E=a.Chart,A=a.each,F=a.inArray,H=a.isObject,p=a.pick,d=a.splat;E.prototype.setResponsive=function(a){var d=this.options.responsive;d&&d.rules&&A(d.rules,function(d){this.matchResponsiveRule(d,
a)},this)};E.prototype.matchResponsiveRule=function(d,v){var g=this.respRules,r=d.condition,f;f=r.callback||function(){return this.chartWidth<=p(r.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=p(r.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=p(r.minWidth,0)&&this.chartHeight>=p(r.minHeight,0)};void 0===d._id&&(d._id=a.uniqueKey());f=f.call(this);!g[d._id]&&f?d.chartOptions&&(g[d._id]=this.currentOptions(d.chartOptions),this.update(d.chartOptions,v)):g[d._id]&&!f&&(this.update(g[d._id],v),delete g[d._id])};
E.prototype.currentOptions=function(a){function g(a,f,b){var l,p;for(l in a)if(-1<F(l,["series","xAxis","yAxis"]))for(a[l]=d(a[l]),b[l]=[],p=0;p<a[l].length;p++)b[l][p]={},g(a[l][p],f[l][p],b[l][p]);else H(a[l])?(b[l]={},g(a[l],f[l]||{},b[l])):b[l]=f[l]||null}var l={};g(a,this.options,l);return l}})(M);return M});


/***/ }),
/* 7 */
/***/ (function(module, exports) {

/*
 Highmaps JS v5.0.14 (2017-07-28)

 (c) 2011-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(I,S){"object"===typeof module&&module.exports?module.exports=I.document?S(I):S:I.Highcharts=S(I)})("undefined"!==typeof window?window:this,function(I){I=function(){var a=window,z=a.document,B=a.navigator&&a.navigator.userAgent||"",C=z&&z.createElementNS&&!!z.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,A=/(edge|msie|trident)/i.test(B)&&!window.opera,e=!C,d=/Firefox/.test(B),q=d&&4>parseInt(B.split("Firefox/")[1],10);return a.Highcharts?a.Highcharts.error(16,!0):{product:"Highmaps",
version:"5.0.14",deg2rad:2*Math.PI/360,doc:z,hasBidiBug:q,hasTouch:z&&void 0!==z.documentElement.ontouchstart,isMS:A,isWebKit:/AppleWebKit/.test(B),isFirefox:d,isTouchDevice:/(Mobile|Android|Windows Phone)/.test(B),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:C,vml:e,win:a,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},charts:[]}}();(function(a){var z=[],B=a.charts,C=a.doc,A=a.win;a.error=function(e,d){e=a.isNumber(e)?"Highcharts error #"+
e+": www.highcharts.com/errors/"+e:e;if(d)throw Error(e);A.console&&console.log(e)};a.Fx=function(a,d,q){this.options=d;this.elem=a;this.prop=q};a.Fx.prototype={dSetter:function(){var a=this.paths[0],d=this.paths[1],q=[],t=this.now,u=a.length,n;if(1===t)q=this.toD;else if(u===d.length&&1>t)for(;u--;)n=parseFloat(a[u]),q[u]=isNaN(n)?a[u]:t*parseFloat(d[u]-n)+n;else q=d;this.elem.attr("d",q,null,!0)},update:function(){var a=this.elem,d=this.prop,q=this.now,t=this.options.step;if(this[d+"Setter"])this[d+
"Setter"]();else a.attr?a.element&&a.attr(d,q,null,!0):a.style[d]=q+this.unit;t&&t.call(a,q,this)},run:function(a,d,q){var e=this,u=function(a){return u.stopped?!1:e.step(a)},n;this.startTime=+new Date;this.start=a;this.end=d;this.unit=q;this.now=this.start;this.pos=0;u.elem=this.elem;u.prop=this.prop;u()&&1===z.push(u)&&(u.timerId=setInterval(function(){for(n=0;n<z.length;n++)z[n]()||z.splice(n--,1);z.length||clearInterval(u.timerId)},13))},step:function(e){var d=+new Date,q,t=this.options,u=this.elem,
n=t.complete,h=t.duration,r=t.curAnim;u.attr&&!u.element?e=!1:e||d>=h+this.startTime?(this.now=this.end,this.pos=1,this.update(),q=r[this.prop]=!0,a.objectEach(r,function(a){!0!==a&&(q=!1)}),q&&n&&n.call(u),e=!1):(this.pos=t.easing((d-this.startTime)/h),this.now=this.start+(this.end-this.start)*this.pos,this.update(),e=!0);return e},initPath:function(e,d,q){function t(a){var f,b;for(l=a.length;l--;)f="M"===a[l]||"L"===a[l],b=/[a-zA-Z]/.test(a[l+3]),f&&b&&a.splice(l+1,0,a[l+1],a[l+2],a[l+1],a[l+2])}
function u(a,f){for(;a.length<b;){a[0]=f[b-a.length];var c=a.slice(0,g);[].splice.apply(a,[0,0].concat(c));E&&(c=a.slice(a.length-g),[].splice.apply(a,[a.length,0].concat(c)),l--)}a[0]="M"}function n(a,f){for(var l=(b-a.length)/g;0<l&&l--;)p=a.slice().splice(a.length/G-g,g*G),p[0]=f[b-g-l*g],c&&(p[g-6]=p[g-2],p[g-5]=p[g-1]),[].splice.apply(a,[a.length/G,0].concat(p)),E&&l--}d=d||"";var h,r=e.startX,m=e.endX,c=-1<d.indexOf("C"),g=c?7:3,b,p,l;d=d.split(" ");q=q.slice();var E=e.isArea,G=E?2:1,f;c&&(t(d),
t(q));if(r&&m){for(l=0;l<r.length;l++)if(r[l]===m[0]){h=l;break}else if(r[0]===m[m.length-r.length+l]){h=l;f=!0;break}void 0===h&&(d=[])}d.length&&a.isNumber(h)&&(b=q.length+h*G*g,f?(u(d,q),n(q,d)):(u(q,d),n(d,q)));return[d,q]}};a.Fx.prototype.fillSetter=a.Fx.prototype.strokeSetter=function(){this.elem.attr(this.prop,a.color(this.start).tweenTo(a.color(this.end),this.pos),null,!0)};a.extend=function(a,d){var e;a||(a={});for(e in d)a[e]=d[e];return a};a.merge=function(){var e,d=arguments,q,t={},u=
function(e,h){"object"!==typeof e&&(e={});a.objectEach(h,function(d,m){!a.isObject(d,!0)||a.isClass(d)||a.isDOMElement(d)?e[m]=h[m]:e[m]=u(e[m]||{},d)});return e};!0===d[0]&&(t=d[1],d=Array.prototype.slice.call(d,2));q=d.length;for(e=0;e<q;e++)t=u(t,d[e]);return t};a.pInt=function(a,d){return parseInt(a,d||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(e,
d){return!!e&&"object"===typeof e&&(!d||!a.isArray(e))};a.isDOMElement=function(e){return a.isObject(e)&&"number"===typeof e.nodeType};a.isClass=function(e){var d=e&&e.constructor;return!(!a.isObject(e,!0)||a.isDOMElement(e)||!d||!d.name||"Object"===d.name)};a.isNumber=function(a){return"number"===typeof a&&!isNaN(a)};a.erase=function(a,d){for(var e=a.length;e--;)if(a[e]===d){a.splice(e,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(e,d,q){var t;a.isString(d)?a.defined(q)?
e.setAttribute(d,q):e&&e.getAttribute&&(t=e.getAttribute(d)):a.defined(d)&&a.isObject(d)&&a.objectEach(d,function(a,d){e.setAttribute(d,a)});return t};a.splat=function(e){return a.isArray(e)?e:[e]};a.syncTimeout=function(a,d,q){if(d)return setTimeout(a,d,q);a.call(0,q)};a.pick=function(){var a=arguments,d,q,t=a.length;for(d=0;d<t;d++)if(q=a[d],void 0!==q&&null!==q)return q};a.css=function(e,d){a.isMS&&!a.svg&&d&&void 0!==d.opacity&&(d.filter="alpha(opacity\x3d"+100*d.opacity+")");a.extend(e.style,
d)};a.createElement=function(e,d,q,t,u){e=C.createElement(e);var n=a.css;d&&a.extend(e,d);u&&n(e,{padding:0,border:"none",margin:0});q&&n(e,q);t&&t.appendChild(e);return e};a.extendClass=function(e,d){var q=function(){};q.prototype=new e;a.extend(q.prototype,d);return q};a.pad=function(a,d,q){return Array((d||2)+1-String(a).length).join(q||0)+a};a.relativeLength=function(a,d,q){return/%$/.test(a)?d*parseFloat(a)/100+(q||0):parseFloat(a)};a.wrap=function(a,d,q){var e=a[d];a[d]=function(){var a=Array.prototype.slice.call(arguments),
d=arguments,h=this;h.proceed=function(){e.apply(h,arguments.length?arguments:d)};a.unshift(e);a=q.apply(this,a);h.proceed=null;return a}};a.getTZOffset=function(e){var d=a.Date;return 6E4*(d.hcGetTimezoneOffset&&d.hcGetTimezoneOffset(e)||d.hcTimezoneOffset||0)};a.dateFormat=function(e,d,q){if(!a.defined(d)||isNaN(d))return a.defaultOptions.lang.invalidDate||"";e=a.pick(e,"%Y-%m-%d %H:%M:%S");var t=a.Date,u=new t(d-a.getTZOffset(d)),n=u[t.hcGetHours](),h=u[t.hcGetDay](),r=u[t.hcGetDate](),m=u[t.hcGetMonth](),
c=u[t.hcGetFullYear](),g=a.defaultOptions.lang,b=g.weekdays,p=g.shortWeekdays,l=a.pad,t=a.extend({a:p?p[h]:b[h].substr(0,3),A:b[h],d:l(r),e:l(r,2," "),w:h,b:g.shortMonths[m],B:g.months[m],m:l(m+1),y:c.toString().substr(2,2),Y:c,H:l(n),k:n,I:l(n%12||12),l:n%12||12,M:l(u[t.hcGetMinutes]()),p:12>n?"AM":"PM",P:12>n?"am":"pm",S:l(u.getSeconds()),L:l(Math.round(d%1E3),3)},a.dateFormats);a.objectEach(t,function(a,b){for(;-1!==e.indexOf("%"+b);)e=e.replace("%"+b,"function"===typeof a?a(d):a)});return q?e.substr(0,
1).toUpperCase()+e.substr(1):e};a.formatSingle=function(e,d){var q=/\.([0-9])/,t=a.defaultOptions.lang;/f$/.test(e)?(q=(q=e.match(q))?q[1]:-1,null!==d&&(d=a.numberFormat(d,q,t.decimalPoint,-1<e.indexOf(",")?t.thousandsSep:""))):d=a.dateFormat(e,d);return d};a.format=function(e,d){for(var q="{",t=!1,u,n,h,r,m=[],c;e;){q=e.indexOf(q);if(-1===q)break;u=e.slice(0,q);if(t){u=u.split(":");n=u.shift().split(".");r=n.length;c=d;for(h=0;h<r;h++)c=c[n[h]];u.length&&(c=a.formatSingle(u.join(":"),c));m.push(c)}else m.push(u);
e=e.slice(q+1);q=(t=!t)?"}":"{"}m.push(e);return m.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(e,d,q,t,u){var n,h=e;q=a.pick(q,1);n=e/q;d||(d=u?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===t&&(1===q?d=a.grep(d,function(a){return 0===a%1}):.1>=q&&(d=[1/q])));for(t=0;t<d.length&&!(h=d[t],u&&h*q>=e||!u&&n<=(d[t]+(d[t+1]||d[t]))/2);t++);return h=a.correctFloat(h*q,-Math.round(Math.log(.001)/Math.LN10))};a.stableSort=
function(a,d){var e=a.length,t,u;for(u=0;u<e;u++)a[u].safeI=u;a.sort(function(a,h){t=d(a,h);return 0===t?a.safeI-h.safeI:t});for(u=0;u<e;u++)delete a[u].safeI};a.arrayMin=function(a){for(var d=a.length,e=a[0];d--;)a[d]<e&&(e=a[d]);return e};a.arrayMax=function(a){for(var d=a.length,e=a[0];d--;)a[d]>e&&(e=a[d]);return e};a.destroyObjectProperties=function(e,d){a.objectEach(e,function(a,t){a&&a!==d&&a.destroy&&a.destroy();delete e[t]})};a.discardElement=function(e){var d=a.garbageBin;d||(d=a.createElement("div"));
e&&d.appendChild(e);d.innerHTML=""};a.correctFloat=function(a,d){return parseFloat(a.toPrecision(d||14))};a.setAnimation=function(e,d){d.renderer.globalAnimation=a.pick(e,d.options.chart.animation,!0)};a.animObject=function(e){return a.isObject(e)?a.merge(e):{duration:e?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=function(e,d,q,t){e=+e||0;d=+d;var u=a.defaultOptions.lang,n=(e.toString().split(".")[1]||"").split("e")[0].length,
h,r,m=e.toString().split("e");-1===d?d=Math.min(n,20):a.isNumber(d)||(d=2);r=(Math.abs(m[1]?m[0]:e)+Math.pow(10,-Math.max(d,n)-1)).toFixed(d);n=String(a.pInt(r));h=3<n.length?n.length%3:0;q=a.pick(q,u.decimalPoint);t=a.pick(t,u.thousandsSep);e=(0>e?"-":"")+(h?n.substr(0,h)+t:"");e+=n.substr(h).replace(/(\d{3})(?=\d)/g,"$1"+t);d&&(e+=q+r.slice(-d));m[1]&&(e+="e"+m[1]);return e};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*a)-1)};a.getStyle=function(e,d,q){if("width"===d)return Math.min(e.offsetWidth,
e.scrollWidth)-a.getStyle(e,"padding-left")-a.getStyle(e,"padding-right");if("height"===d)return Math.min(e.offsetHeight,e.scrollHeight)-a.getStyle(e,"padding-top")-a.getStyle(e,"padding-bottom");if(e=A.getComputedStyle(e,void 0))e=e.getPropertyValue(d),a.pick(q,!0)&&(e=a.pInt(e));return e};a.inArray=function(a,d){return d.indexOf?d.indexOf(a):[].indexOf.call(d,a)};a.grep=function(a,d){return[].filter.call(a,d)};a.find=function(a,d){return[].find.call(a,d)};a.map=function(a,d){for(var e=[],t=0,u=
a.length;t<u;t++)e[t]=d.call(a[t],a[t],t,a);return e};a.offset=function(a){var d=C.documentElement;a=a.getBoundingClientRect();return{top:a.top+(A.pageYOffset||d.scrollTop)-(d.clientTop||0),left:a.left+(A.pageXOffset||d.scrollLeft)-(d.clientLeft||0)}};a.stop=function(a,d){for(var e=z.length;e--;)z[e].elem!==a||d&&d!==z[e].prop||(z[e].stopped=!0)};a.each=function(a,d,q){return Array.prototype.forEach.call(a,d,q)};a.objectEach=function(a,d,q){for(var e in a)a.hasOwnProperty(e)&&d.call(q,a[e],e,a)};
a.addEvent=function(e,d,q){function t(a){a.target=a.srcElement||A;q.call(e,a)}var u=e.hcEvents=e.hcEvents||{};e.addEventListener?e.addEventListener(d,q,!1):e.attachEvent&&(e.hcEventsIE||(e.hcEventsIE={}),q.hcGetKey||(q.hcGetKey=a.uniqueKey()),e.hcEventsIE[q.hcGetKey]=t,e.attachEvent("on"+d,t));u[d]||(u[d]=[]);u[d].push(q);return function(){a.removeEvent(e,d,q)}};a.removeEvent=function(e,d,q){function t(a,c){e.removeEventListener?e.removeEventListener(a,c,!1):e.attachEvent&&(c=e.hcEventsIE[c.hcGetKey],
e.detachEvent("on"+a,c))}function u(){var m,c;e.nodeName&&(d?(m={},m[d]=!0):m=h,a.objectEach(m,function(a,b){if(h[b])for(c=h[b].length;c--;)t(b,h[b][c])}))}var n,h=e.hcEvents,r;h&&(d?(n=h[d]||[],q?(r=a.inArray(q,n),-1<r&&(n.splice(r,1),h[d]=n),t(d,q)):(u(),h[d]=[])):(u(),e.hcEvents={}))};a.fireEvent=function(e,d,q,t){var u;u=e.hcEvents;var n,h;q=q||{};if(C.createEvent&&(e.dispatchEvent||e.fireEvent))u=C.createEvent("Events"),u.initEvent(d,!0,!0),a.extend(u,q),e.dispatchEvent?e.dispatchEvent(u):e.fireEvent(d,
u);else if(u)for(u=u[d]||[],n=u.length,q.target||a.extend(q,{preventDefault:function(){q.defaultPrevented=!0},target:e,type:d}),d=0;d<n;d++)(h=u[d])&&!1===h.call(e,q)&&q.preventDefault();t&&!q.defaultPrevented&&t(q)};a.animate=function(e,d,q){var t,u="",n,h,r;a.isObject(q)||(r=arguments,q={duration:r[2],easing:r[3],complete:r[4]});a.isNumber(q.duration)||(q.duration=400);q.easing="function"===typeof q.easing?q.easing:Math[q.easing]||Math.easeInOutSine;q.curAnim=a.merge(d);a.objectEach(d,function(m,
c){a.stop(e,c);h=new a.Fx(e,q,c);n=null;"d"===c?(h.paths=h.initPath(e,e.d,d.d),h.toD=d.d,t=0,n=1):e.attr?t=e.attr(c):(t=parseFloat(a.getStyle(e,c))||0,"opacity"!==c&&(u="px"));n||(n=m);n&&n.match&&n.match("px")&&(n=n.replace(/px/g,""));h.run(t,n,u)})};a.seriesType=function(e,d,q,t,u){var n=a.getOptions(),h=a.seriesTypes;n.plotOptions[e]=a.merge(n.plotOptions[d],q);h[e]=a.extendClass(h[d]||function(){},t);h[e].prototype.type=e;u&&(h[e].prototype.pointClass=a.extendClass(a.Point,u));return h[e]};a.uniqueKey=
function(){var a=Math.random().toString(36).substring(2,9),d=0;return function(){return"highcharts-"+a+"-"+d++}}();A.jQuery&&(A.jQuery.fn.highcharts=function(){var e=[].slice.call(arguments);if(this[0])return e[0]?(new (a[a.isString(e[0])?e.shift():"Chart"])(this[0],e[0],e[1]),this):B[a.attr(this[0],"data-highcharts-chart")]});C&&!C.defaultView&&(a.getStyle=function(e,d){var q={width:"clientWidth",height:"clientHeight"}[d];if(e.style[d])return a.pInt(e.style[d]);"opacity"===d&&(d="filter");if(q)return e.style.zoom=
1,Math.max(e[q]-2*a.getStyle(e,"padding"),0);e=e.currentStyle[d.replace(/\-(\w)/g,function(a,d){return d.toUpperCase()})];"filter"===d&&(e=e.replace(/alpha\(opacity=([0-9]+)\)/,function(a,d){return d/100}));return""===e?1:a.pInt(e)});Array.prototype.forEach||(a.each=function(a,d,q){for(var e=0,u=a.length;e<u;e++)if(!1===d.call(q,a[e],e,a))return e});Array.prototype.indexOf||(a.inArray=function(a,d){var e,t=0;if(d)for(e=d.length;t<e;t++)if(d[t]===a)return t;return-1});Array.prototype.filter||(a.grep=
function(a,d){for(var e=[],t=0,u=a.length;t<u;t++)d(a[t],t)&&e.push(a[t]);return e});Array.prototype.find||(a.find=function(a,d){var e,t=a.length;for(e=0;e<t;e++)if(d(a[e],e))return a[e]})})(I);(function(a){var z=a.each,B=a.isNumber,C=a.map,A=a.merge,e=a.pInt;a.Color=function(d){if(!(this instanceof a.Color))return new a.Color(d);this.init(d)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[e(a[1]),
e(a[2]),e(a[3]),parseFloat(a[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[e(a[1]),e(a[2]),e(a[3]),1]}}],names:{none:"rgba(255,255,255,0)",white:"#ffffff",black:"#000000"},init:function(d){var e,t,u,n;if((this.input=d=this.names[d&&d.toLowerCase?d.toLowerCase():""]||d)&&d.stops)this.stops=C(d.stops,function(h){return new a.Color(h[1])});else if(d&&"#"===d.charAt()&&(e=d.length,d=parseInt(d.substr(1),16),7===e?t=[(d&16711680)>>16,(d&65280)>>
8,d&255,1]:4===e&&(t=[(d&3840)>>4|(d&3840)>>8,(d&240)>>4|d&240,(d&15)<<4|d&15,1])),!t)for(u=this.parsers.length;u--&&!t;)n=this.parsers[u],(e=n.regex.exec(d))&&(t=n.parse(e));this.rgba=t||[]},get:function(a){var d=this.input,e=this.rgba,u;this.stops?(u=A(d),u.stops=[].concat(u.stops),z(this.stops,function(d,h){u.stops[h]=[u.stops[h][0],d.get(a)]})):u=e&&B(e[0])?"rgb"===a||!a&&1===e[3]?"rgb("+e[0]+","+e[1]+","+e[2]+")":"a"===a?e[3]:"rgba("+e.join(",")+")":d;return u},brighten:function(a){var d,t=this.rgba;
if(this.stops)z(this.stops,function(d){d.brighten(a)});else if(B(a)&&0!==a)for(d=0;3>d;d++)t[d]+=e(255*a),0>t[d]&&(t[d]=0),255<t[d]&&(t[d]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this},tweenTo:function(a,e){var d,u;a.rgba.length?(d=this.rgba,a=a.rgba,u=1!==a[3]||1!==d[3],a=(u?"rgba(":"rgb(")+Math.round(a[0]+(d[0]-a[0])*(1-e))+","+Math.round(a[1]+(d[1]-a[1])*(1-e))+","+Math.round(a[2]+(d[2]-a[2])*(1-e))+(u?","+(a[3]+(d[3]-a[3])*(1-e)):"")+")"):a=a.input||"none";return a}};a.color=
function(d){return new a.Color(d)}})(I);(function(a){function z(){var d=a.defaultOptions.global,e=t.moment;if(d.timezone){if(e)return function(a){return-e.tz(a,d.timezone).utcOffset()};a.error(25)}return d.useUTC&&d.getTimezoneOffset}function B(){var d=a.defaultOptions.global,n,h=d.useUTC,r=h?"getUTC":"get",m=h?"setUTC":"set";a.Date=n=d.Date||t.Date;n.hcTimezoneOffset=h&&d.timezoneOffset;n.hcGetTimezoneOffset=z();n.hcMakeTime=function(a,g,b,p,l,d){var c;h?(c=n.UTC.apply(0,arguments),c+=e(c)):c=(new n(a,
g,q(b,1),q(p,0),q(l,0),q(d,0))).getTime();return c};A("Minutes Hours Day Date Month FullYear".split(" "),function(a){n["hcGet"+a]=r+a});A("Milliseconds Seconds Minutes Hours Date Month FullYear".split(" "),function(a){n["hcSet"+a]=m+a})}var C=a.color,A=a.each,e=a.getTZOffset,d=a.merge,q=a.pick,t=a.win;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",
months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0,VMLRadialGradientURL:"http://code.highcharts.com/5.0.14/gfx/vml-radial-gradient.png"},chart:{borderRadius:0,
defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:20},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},
borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",
backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:C("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',
pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(e){a.defaultOptions=d(!0,a.defaultOptions,e);B();
return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;B()})(I);(function(a){var z,B,C=a.addEvent,A=a.animate,e=a.attr,d=a.charts,q=a.color,t=a.css,u=a.createElement,n=a.defined,h=a.deg2rad,r=a.destroyObjectProperties,m=a.doc,c=a.each,g=a.extend,b=a.erase,p=a.grep,l=a.hasTouch,E=a.inArray,G=a.isArray,f=a.isFirefox,H=a.isMS,D=a.isObject,y=a.isString,M=a.isWebKit,v=a.merge,L=a.noop,F=a.objectEach,J=a.pick,k=a.pInt,w=a.removeEvent,
N=a.stop,K=a.svg,R=a.SVG_NS,O=a.symbolSizes,P=a.win;z=a.SVGElement=function(){return this};g(z.prototype,{opacity:1,SVG_NS:R,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline".split(" "),init:function(a,k){this.element="span"===k?u(k):m.createElementNS(this.SVG_NS,k);this.renderer=a},animate:function(x,k,f){k=a.animObject(J(k,this.renderer.globalAnimation,!0));0!==k.duration?(f&&(k.complete=f),A(this,x,k)):(this.attr(x,
null,f),k.step&&k.step.call(this));return this},colorGradient:function(x,k,f){var w=this.renderer,b,g,l,p,Q,h,D,K,H,d,m=[],e;x.radialGradient?g="radialGradient":x.linearGradient&&(g="linearGradient");g&&(l=x[g],Q=w.gradients,D=x.stops,d=f.radialReference,G(l)&&(x[g]=l={x1:l[0],y1:l[1],x2:l[2],y2:l[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===g&&d&&!n(l.gradientUnits)&&(p=l,l=v(l,w.getRadialAttr(d,p),{gradientUnits:"userSpaceOnUse"})),F(l,function(a,x){"id"!==x&&m.push(x,a)}),F(D,function(a){m.push(a)}),
m=m.join(","),Q[m]?d=Q[m].attr("id"):(l.id=d=a.uniqueKey(),Q[m]=h=w.createElement(g).attr(l).add(w.defs),h.radAttr=p,h.stops=[],c(D,function(x){0===x[1].indexOf("rgba")?(b=a.color(x[1]),K=b.get("rgb"),H=b.get("a")):(K=x[1],H=1);x=w.createElement("stop").attr({offset:x[0],"stop-color":K,"stop-opacity":H}).add(h);h.stops.push(x)})),e="url("+w.url+"#"+d+")",f.setAttribute(k,e),f.gradient=m,x.toString=function(){return e})},applyTextOutline:function(x){var k=this.element,f,w,g,l,v;-1!==x.indexOf("contrast")&&
(x=x.replace(/contrast/g,this.renderer.getContrast(k.style.fill)));x=x.split(" ");w=x[x.length-1];if((g=x[0])&&"none"!==g&&a.svg){this.fakeTS=!0;x=[].slice.call(k.getElementsByTagName("tspan"));this.ySetter=this.xSetter;g=g.replace(/(^[\d\.]+)(.*?)$/g,function(a,x,k){return 2*x+k});for(v=x.length;v--;)f=x[v],"highcharts-text-outline"===f.getAttribute("class")&&b(x,k.removeChild(f));l=k.firstChild;c(x,function(a,x){0===x&&(a.setAttribute("x",k.getAttribute("x")),x=k.getAttribute("y"),a.setAttribute("y",
x||0),null===x&&k.setAttribute("y",0));a=a.cloneNode(1);e(a,{"class":"highcharts-text-outline",fill:w,stroke:w,"stroke-width":g,"stroke-linejoin":"round"});k.insertBefore(a,l)})}},attr:function(a,k,f,w){var x,b=this.element,g,c=this,l,v;"string"===typeof a&&void 0!==k&&(x=a,a={},a[x]=k);"string"===typeof a?c=(this[a+"Getter"]||this._defaultGetter).call(this,a,b):(F(a,function(x,k){l=!1;w||N(this,k);this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)$/.test(k)&&(g||(this.symbolAttr(a),
g=!0),l=!0);!this.rotation||"x"!==k&&"y"!==k||(this.doTransform=!0);l||(v=this[k+"Setter"]||this._defaultSetter,v.call(this,x,k,b),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(k)&&this.updateShadows(k,x,v))},this),this.afterSetters());f&&f();return c},afterSetters:function(){this.doTransform&&(this.updateTransform(),this.doTransform=!1)},updateShadows:function(a,k,f){for(var x=this.shadows,w=x.length;w--;)f.call(x[w],"height"===a?Math.max(k-(x[w].cutHeight||0),0):"d"===
a?this.d:k,a,x[w])},addClass:function(a,k){var x=this.attr("class")||"";-1===x.indexOf(a)&&(k||(a=(x+(x?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==E(a,(this.attr("class")||"").split(" "))},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(a,""))},symbolAttr:function(a){var x=this;c("x y r start end width height innerR anchorX anchorY".split(" "),function(k){x[k]=J(a[k],x[k])});x.attr({d:x.renderer.symbols[x.symbolName](x.x,
x.y,x.width,x.height,x)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,k){var x=this,f={},w;k=k||a.strokeWidth||0;w=Math.round(k)%2/2;a.x=Math.floor(a.x||x.x||0)+w;a.y=Math.floor(a.y||x.y||0)+w;a.width=Math.floor((a.width||x.width||0)-2*w);a.height=Math.floor((a.height||x.height||0)-2*w);n(a.strokeWidth)&&(a.strokeWidth=k);F(a,function(a,k){x[k]!==a&&(x[k]=f[k]=a)});return f},css:function(a){var x=this.styles,f={},w=this.element,
b,c="",l,v=!x,h=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);x&&F(a,function(a,k){a!==x[k]&&(f[k]=a,v=!0)});v&&(x&&(a=g(x,f)),b=this.textWidth=a&&a.width&&"auto"!==a.width&&"text"===w.nodeName.toLowerCase()&&k(a.width),this.styles=a,b&&!K&&this.renderer.forExport&&delete a.width,H&&!K?t(this.element,a):(l=function(a,x){return"-"+x.toLowerCase()},F(a,function(a,x){-1===E(x,h)&&(c+=x.replace(/([A-Z])/g,l)+":"+a+";")}),c&&e(w,"style",c)),this.added&&("text"===this.element.nodeName&&
this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline)));return this},strokeWidth:function(){return this["stroke-width"]||0},on:function(a,k){var x=this,f=x.element;l&&"click"===a?(f.ontouchstart=function(a){x.touchEventFired=Date.now();a.preventDefault();k.call(f,a)},f.onclick=function(a){(-1===P.navigator.userAgent.indexOf("Android")||1100<Date.now()-(x.touchEventFired||0))&&k.call(f,a)}):f["on"+a]=k;return this},setRadialReference:function(a){var x=this.renderer.gradients[this.element.gradient];
this.element.radialReference=a;x&&x.radAttr&&x.animate(this.renderer.getRadialAttr(a,x.radAttr));return this},translate:function(a,k){return this.attr({translateX:a,translateY:k})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,k=this.translateY||0,f=this.scaleX,w=this.scaleY,b=this.inverted,g=this.rotation,c=this.element;b&&(a+=this.width,k+=this.height);a=["translate("+a+","+k+")"];b?a.push("rotate(90) scale(-1,1)"):g&&
a.push("rotate("+g+" "+(c.getAttribute("x")||0)+" "+(c.getAttribute("y")||0)+")");(n(f)||n(w))&&a.push("scale("+J(f,1)+" "+J(w,1)+")");a.length&&c.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,k,f){var x,w,g,c,l={};w=this.renderer;g=w.alignedObjects;var v,h;if(a){if(this.alignOptions=a,this.alignByTranslate=k,!f||y(f))this.alignTo=x=f||"renderer",b(g,this),g.push(this),f=null}else a=this.alignOptions,k=this.alignByTranslate,
x=this.alignTo;f=J(f,w[x],w);x=a.align;w=a.verticalAlign;g=(f.x||0)+(a.x||0);c=(f.y||0)+(a.y||0);"right"===x?v=1:"center"===x&&(v=2);v&&(g+=(f.width-(a.width||0))/v);l[k?"translateX":"x"]=Math.round(g);"bottom"===w?h=1:"middle"===w&&(h=2);h&&(c+=(f.height-(a.height||0))/h);l[k?"translateY":"y"]=Math.round(c);this[this.placed?"animate":"attr"](l);this.placed=!0;this.alignAttr=l;return this},getBBox:function(a,k){var x,f=this.renderer,w,b=this.element,l=this.styles,v,p=this.textStr,D,K=f.cache,Q=f.cacheKeys,
H;k=J(k,this.rotation);w=k*h;v=l&&l.fontSize;void 0!==p&&(H=p.toString(),-1===H.indexOf("\x3c")&&(H=H.replace(/[0-9]/g,"0")),H+=["",k||0,v,l&&l.width,l&&l.textOverflow].join());H&&!a&&(x=K[H]);if(!x){if(b.namespaceURI===this.SVG_NS||f.forExport){try{(D=this.fakeTS&&function(a){c(b.querySelectorAll(".highcharts-text-outline"),function(x){x.style.display=a})})&&D("none"),x=b.getBBox?g({},b.getBBox()):{width:b.offsetWidth,height:b.offsetHeight},D&&D("")}catch(V){}if(!x||0>x.width)x={width:0,height:0}}else x=
this.htmlGetBBox();f.isSVG&&(a=x.width,f=x.height,l&&"11px"===l.fontSize&&17===Math.round(f)&&(x.height=f=14),k&&(x.width=Math.abs(f*Math.sin(w))+Math.abs(a*Math.cos(w)),x.height=Math.abs(f*Math.cos(w))+Math.abs(a*Math.sin(w))));if(H&&0<x.height){for(;250<Q.length;)delete K[Q.shift()];K[H]||Q.push(H);K[H]=x}}return x},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var x=this;x.animate({opacity:0},
{duration:a||150,complete:function(){x.attr({y:-9999})}})},add:function(a){var x=this.renderer,k=this.element,f;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&x.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)f=this.zIndexSetter();f||(a?a.element:x.box).appendChild(k);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var x=a.parentNode;x&&x.removeChild(a)},destroy:function(){var a=this,k=a.element||{},f=a.renderer.isSVG&&"SPAN"===k.nodeName&&
a.parentGroup,w=k.ownerSVGElement;k.onclick=k.onmouseout=k.onmouseover=k.onmousemove=k.point=null;N(a);a.clipPath&&w&&(c(w.querySelectorAll("[clip-path]"),function(k){-1<k.getAttribute("clip-path").indexOf(a.clipPath.element.id+")")&&k.removeAttribute("clip-path")}),a.clipPath=a.clipPath.destroy());if(a.stops){for(w=0;w<a.stops.length;w++)a.stops[w]=a.stops[w].destroy();a.stops=null}a.safeRemoveChild(k);for(a.destroyShadows();f&&f.div&&0===f.div.childNodes.length;)k=f.parentGroup,a.safeRemoveChild(f.div),
delete f.div,f=k;a.alignTo&&b(a.renderer.alignedObjects,a);F(a,function(k,x){delete a[x]});return null},shadow:function(a,k,f){var x=[],w,b,g=this.element,c,l,v,h;if(!a)this.destroyShadows();else if(!this.shadows){l=J(a.width,3);v=(a.opacity||.15)/l;h=this.parentInverted?"(-1,-1)":"("+J(a.offsetX,1)+", "+J(a.offsetY,1)+")";for(w=1;w<=l;w++)b=g.cloneNode(0),c=2*l+1-2*w,e(b,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":v*w,"stroke-width":c,transform:"translate"+h,fill:"none"}),f&&(e(b,
"height",Math.max(e(b,"height")-c,0)),b.cutHeight=c),k?k.element.appendChild(b):g.parentNode.insertBefore(b,g),x.push(b);this.shadows=x}return this},destroyShadows:function(){c(this.shadows||[],function(a){this.safeRemoveChild(a)},this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=J(this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));
return a},dSetter:function(a,k,f){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[k]!==a&&(f.setAttribute(k,a),this[k]=a)},dashstyleSetter:function(a){var x,f=this["stroke-width"];"inherit"===f&&(f=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(x=a.length;x--;)a[x]=
k(a[x])*f;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.element.setAttribute("text-anchor",{left:"start",center:"middle",right:"end"}[a])},opacitySetter:function(a,k,f){this[k]=a;f.setAttribute(k,a)},titleSetter:function(a){var k=this.element.getElementsByTagName("title")[0];k||(k=m.createElementNS(this.SVG_NS,"title"),this.element.appendChild(k));k.firstChild&&k.removeChild(k.firstChild);k.appendChild(m.createTextNode(String(J(a),
"").replace(/<[^>]*>/g,"")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},fillSetter:function(a,k,f){"string"===typeof a?f.setAttribute(k,a):a&&this.colorGradient(a,k,f)},visibilitySetter:function(a,k,f){"inherit"===a?f.removeAttribute(k):this[k]!==a&&f.setAttribute(k,a);this[k]=a},zIndexSetter:function(a,f){var x=this.renderer,w=this.parentGroup,b=(w||x).element||x.box,g,c=this.element,l;g=this.added;var v;n(a)&&(c.zIndex=
a,a=+a,this[f]===a&&(g=!1),this[f]=a);if(g){(a=this.zIndex)&&w&&(w.handleZ=!0);f=b.childNodes;for(v=0;v<f.length&&!l;v++)w=f[v],g=w.zIndex,w!==c&&(k(g)>a||!n(a)&&n(g)||0>a&&!n(g)&&b!==x.box)&&(b.insertBefore(c,w),l=!0);l||b.appendChild(c)}return l},_defaultSetter:function(a,k,f){f.setAttribute(k,a)}});z.prototype.yGetter=z.prototype.xGetter;z.prototype.translateXSetter=z.prototype.translateYSetter=z.prototype.rotationSetter=z.prototype.verticalAlignSetter=z.prototype.scaleXSetter=z.prototype.scaleYSetter=
function(a,k){this[k]=a;this.doTransform=!0};z.prototype["stroke-widthSetter"]=z.prototype.strokeSetter=function(a,k,f){this[k]=a;this.stroke&&this["stroke-width"]?(z.prototype.fillSetter.call(this,this.stroke,"stroke",f),f.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===k&&0===a&&this.hasStroke&&(f.removeAttribute("stroke"),this.hasStroke=!1)};B=a.SVGRenderer=function(){this.init.apply(this,arguments)};g(B.prototype,{Element:z,SVG_NS:R,init:function(a,k,w,b,
g,c){var x;b=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(b));x=b.element;a.appendChild(x);-1===a.innerHTML.indexOf("xmlns")&&e(x,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=x;this.boxWrapper=b;this.alignedObjects=[];this.url=(f||M)&&m.getElementsByTagName("base").length?P.location.href.replace(/#.*?$/,"").replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(m.createTextNode("Created with Highmaps 5.0.14"));
this.defs=this.createElement("defs").add();this.allowHTML=c;this.forExport=g;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(k,w,!1);var l;f&&a.getBoundingClientRect&&(k=function(){t(a,{left:0,top:0});l=a.getBoundingClientRect();t(a,{left:Math.ceil(l.left)-l.left+"px",top:Math.ceil(l.top)-l.top+"px"})},k(),this.unSubPixelFix=C(P,"resize",k))},getStyle:function(a){return this.style=g({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},
a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();r(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var k=new this.Element;k.init(this,a);return k},draw:L,getRadialAttr:function(a,k){return{cx:a[0]-a[2]/2+k.cx*a[2],cy:a[1]-
a[2]/2+k.cy*a[2],r:k.r*a[2]}},getSpanWidth:function(a,k){var f=a.getBBox(!0).width;!K&&this.forExport&&(f=this.measureSpanWidth(k.firstChild.data,a.styles));return f},applyEllipsis:function(a,k,f,w){var x=a.rotation,b=f,g,c=0,l=f.length,v=function(a){k.removeChild(k.firstChild);a&&k.appendChild(m.createTextNode(a))},h;a.rotation=0;b=this.getSpanWidth(a,k);if(h=b>w){for(;c<=l;)g=Math.ceil((c+l)/2),b=f.substring(0,g)+"\u2026",v(b),b=this.getSpanWidth(a,k),c===l?c=l+1:b>w?l=g-1:c=g;0===l&&v("")}a.rotation=
x;return h},buildText:function(a){var f=a.element,w=this,b=w.forExport,x=J(a.textStr,"").toString(),g=-1!==x.indexOf("\x3c"),l=f.childNodes,v,h,D,H,d=e(f,"x"),y=a.styles,n=a.textWidth,F=y&&y.lineHeight,r=y&&y.textOutline,L=y&&"ellipsis"===y.textOverflow,E=y&&"nowrap"===y.whiteSpace,N=y&&y.fontSize,u,G,q=l.length,y=n&&!a.added&&this.box,M=function(a){var x;x=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:N||w.style.fontSize||12;return F?k(F):w.fontMetrics(x,a.getAttribute("style")?a:f).h};u=
[x,L,E,F,r,N,n].join();if(u!==a.textCache){for(a.textCache=u;q--;)f.removeChild(l[q]);g||r||L||n||-1!==x.indexOf(" ")?(v=/<.*class="([^"]+)".*>/,h=/<.*style="([^"]+)".*>/,D=/<.*href="([^"]+)".*>/,y&&y.appendChild(f),x=g?x.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,"\x3c/span\x3e").split(/<br.*?>/g):[x],x=p(x,function(a){return""!==a}),c(x,function(k,
x){var g,l=0;k=k.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");g=k.split("|||");c(g,function(k){if(""!==k||1===g.length){var c={},p=m.createElementNS(w.SVG_NS,"tspan"),y,F;v.test(k)&&(y=k.match(v)[1],e(p,"class",y));h.test(k)&&(F=k.match(h)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),e(p,"style",F));D.test(k)&&!b&&(e(p,"onclick",'location.href\x3d"'+k.match(D)[1]+'"'),t(p,{cursor:"pointer"}));k=(k.replace(/<(.|\n)*?>/g,"")||" ").replace(/&lt;/g,"\x3c").replace(/&gt;/g,
"\x3e");if(" "!==k){p.appendChild(m.createTextNode(k));l?c.dx=0:x&&null!==d&&(c.x=d);e(p,c);f.appendChild(p);!l&&G&&(!K&&b&&t(p,{display:"block"}),e(p,"dy",M(p)));if(n){c=k.replace(/([^\^])-/g,"$1- ").split(" ");y=1<g.length||x||1<c.length&&!E;var r=[],N,Q=M(p),u=a.rotation;for(L&&(H=w.applyEllipsis(a,p,k,n));!L&&y&&(c.length||r.length);)a.rotation=0,N=w.getSpanWidth(a,p),k=N>n,void 0===H&&(H=k),k&&1!==c.length?(p.removeChild(p.firstChild),r.unshift(c.pop())):(c=r,r=[],c.length&&!E&&(p=m.createElementNS(R,
"tspan"),e(p,{dy:Q,x:d}),F&&e(p,"style",F),f.appendChild(p)),N>n&&(n=N)),c.length&&p.appendChild(m.createTextNode(c.join(" ").replace(/- /g,"-")));a.rotation=u}l++}}});G=G||f.childNodes.length}),H&&a.attr("title",a.textStr),y&&y.removeChild(f),r&&a.applyTextOutline&&a.applyTextOutline(r)):f.appendChild(m.createTextNode(x.replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e")))}},getContrast:function(a){a=q(a).rgba;return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,k,f,w,b,c,l,p,h){var x=this.label(a,
k,f,h,null,null,null,null,"button"),D=0;x.attr(v({padding:8,r:2},b));var K,d,m,y;b=v({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",cursor:"pointer",fontWeight:"normal"}},b);K=b.style;delete b.style;c=v(b,{fill:"#e6e6e6"},c);d=c.style;delete c.style;l=v(b,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},l);m=l.style;delete l.style;p=v(b,{style:{color:"#cccccc"}},p);y=p.style;delete p.style;C(x.element,H?"mouseover":"mouseenter",function(){3!==D&&x.setState(1)});
C(x.element,H?"mouseout":"mouseleave",function(){3!==D&&x.setState(D)});x.setState=function(a){1!==a&&(x.state=D=a);x.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+["normal","hover","pressed","disabled"][a||0]);x.attr([b,c,l,p][a||0]).css([K,d,m,y][a||0])};x.attr(b).css(g({cursor:"default"},K));return x.on("click",function(a){3!==D&&w.call(x,a)})},crispLine:function(a,k){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-k%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+
k%2/2);return a},path:function(a){var k={fill:"none"};G(a)?k.d=a:D(a)&&g(k,a);return this.createElement("path").attr(k)},circle:function(a,k,f){a=D(a)?a:{x:a,y:k,r:f};k=this.createElement("circle");k.xSetter=k.ySetter=function(a,k,f){f.setAttribute("c"+k,a)};return k.attr(a)},arc:function(a,k,f,w,b,g){D(a)?(w=a,k=w.y,f=w.r,a=w.x):w={innerR:w,start:b,end:g};a=this.symbol("arc",a,k,f,f,w);a.r=f;return a},rect:function(a,k,f,w,b,g){b=D(a)?a.r:b;var c=this.createElement("rect");a=D(a)?a:void 0===a?{}:
{x:a,y:k,width:Math.max(f,0),height:Math.max(w,0)};void 0!==g&&(a.strokeWidth=g,a=c.crisp(a));a.fill="none";b&&(a.r=b);c.rSetter=function(a,k,f){e(f,{rx:a,ry:a})};return c.attr(a)},setSize:function(a,k,f){var w=this.alignedObjects,b=w.length;this.width=a;this.height=k;for(this.boxWrapper.animate({width:a,height:k},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:J(f,!0)?void 0:0});b--;)w[b].align()},g:function(a){var k=this.createElement("g");return a?
k.attr({"class":"highcharts-"+a}):k},image:function(a,k,f,w,b){var c={preserveAspectRatio:"none"};1<arguments.length&&g(c,{x:k,y:f,width:w,height:b});c=this.createElement("image").attr(c);c.element.setAttributeNS?c.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):c.element.setAttribute("hc-svg-href",a);return c},symbol:function(a,k,f,w,b,l){var x=this,v,p=/^url\((.*?)\)$/,h=p.test(a),D=!h&&(this.symbols[a]?a:"circle"),H=D&&this.symbols[D],K=n(k)&&H&&H.call(this.symbols,Math.round(k),
Math.round(f),w,b,l),y,e;H?(v=this.path(K),v.attr("fill","none"),g(v,{symbolName:D,x:k,y:f,width:w,height:b}),l&&g(v,l)):h&&(y=a.match(p)[1],v=this.image(y),v.imgwidth=J(O[y]&&O[y].width,l&&l.width),v.imgheight=J(O[y]&&O[y].height,l&&l.height),e=function(){v.attr({width:v.width,height:v.height})},c(["width","height"],function(a){v[a+"Setter"]=function(a,k){var f={},w=this["img"+k],b="width"===k?"translateX":"translateY";this[k]=a;n(w)&&(this.element&&this.element.setAttribute(k,w),this.alignByTranslate||
(f[b]=((this[k]||0)-w)/2,this.attr(f)))}}),n(k)&&v.attr({x:k,y:f}),v.isImg=!0,n(v.imgwidth)&&n(v.imgheight)?e():(v.attr({width:0,height:0}),u("img",{onload:function(){var a=d[x.chartIndex];0===this.width&&(t(this,{position:"absolute",top:"-999em"}),m.body.appendChild(this));O[y]={width:this.width,height:this.height};v.imgwidth=this.width;v.imgheight=this.height;v.element&&e();this.parentNode&&this.parentNode.removeChild(this);x.imgCount--;if(!x.imgCount&&a&&a.onload)a.onload()},src:y}),this.imgCount++));
return v},symbols:{circle:function(a,k,f,w){return this.arc(a+f/2,k+w/2,f/2,w/2,{start:0,end:2*Math.PI,open:!1})},square:function(a,k,f,w){return["M",a,k,"L",a+f,k,a+f,k+w,a,k+w,"Z"]},triangle:function(a,k,f,w){return["M",a+f/2,k,"L",a+f,k+w,a,k+w,"Z"]},"triangle-down":function(a,k,f,w){return["M",a,k,"L",a+f,k,a+f/2,k+w,"Z"]},diamond:function(a,k,f,w){return["M",a+f/2,k,"L",a+f,k+w/2,a+f/2,k+w,a,k+w/2,"Z"]},arc:function(a,k,f,w,b){var c=b.start,g=b.r||f,l=b.r||w||f,v=b.end-.001;f=b.innerR;w=J(b.open,
.001>Math.abs(b.end-b.start-2*Math.PI));var x=Math.cos(c),p=Math.sin(c),h=Math.cos(v),v=Math.sin(v);b=.001>b.end-c-Math.PI?0:1;g=["M",a+g*x,k+l*p,"A",g,l,0,b,1,a+g*h,k+l*v];n(f)&&g.push(w?"M":"L",a+f*h,k+f*v,"A",f,f,0,b,0,a+f*x,k+f*p);g.push(w?"":"Z");return g},callout:function(a,k,f,w,b){var c=Math.min(b&&b.r||0,f,w),g=c+6,l=b&&b.anchorX;b=b&&b.anchorY;var v;v=["M",a+c,k,"L",a+f-c,k,"C",a+f,k,a+f,k,a+f,k+c,"L",a+f,k+w-c,"C",a+f,k+w,a+f,k+w,a+f-c,k+w,"L",a+c,k+w,"C",a,k+w,a,k+w,a,k+w-c,"L",a,k+c,
"C",a,k,a,k,a+c,k];l&&l>f?b>k+g&&b<k+w-g?v.splice(13,3,"L",a+f,b-6,a+f+6,b,a+f,b+6,a+f,k+w-c):v.splice(13,3,"L",a+f,w/2,l,b,a+f,w/2,a+f,k+w-c):l&&0>l?b>k+g&&b<k+w-g?v.splice(33,3,"L",a,b+6,a-6,b,a,b-6,a,k+c):v.splice(33,3,"L",a,w/2,l,b,a,w/2,a,k+c):b&&b>w&&l>a+g&&l<a+f-g?v.splice(23,3,"L",l+6,k+w,l,k+w+6,l-6,k+w,a+c,k+w):b&&0>b&&l>a+g&&l<a+f-g&&v.splice(3,3,"L",l-6,k,l,k-6,l+6,k,f-c,k);return v}},clipRect:function(k,f,w,b){var c=a.uniqueKey(),g=this.createElement("clipPath").attr({id:c}).add(this.defs);
k=this.rect(k,f,w,b,0).add(g);k.id=c;k.clipPath=g;k.count=0;return k},text:function(a,k,f,w){var b=!K&&this.forExport,c={};if(w&&(this.allowHTML||!this.forExport))return this.html(a,k,f);c.x=Math.round(k||0);f&&(c.y=Math.round(f));if(a||0===a)c.text=a;a=this.createElement("text").attr(c);b&&a.css({position:"absolute"});w||(a.xSetter=function(a,k,f){var w=f.getElementsByTagName("tspan"),b,c=f.getAttribute(k),g;for(g=0;g<w.length;g++)b=w[g],b.getAttribute(k)===c&&b.setAttribute(k,a);f.setAttribute(k,
a)});return a},fontMetrics:function(a,f){a=a||f&&f.style&&f.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?k(a):/em/.test(a)?parseFloat(a)*(f?this.fontMetrics(null,f.parentNode).f:16):12;f=24>a?a+3:Math.round(1.2*a);return{h:f,b:Math.round(.8*f),f:a}},rotCorr:function(a,k,f){var w=a;k&&f&&(w=Math.max(w*Math.cos(k*h),4));return{x:-a/3*Math.sin(k*h),y:w}},label:function(k,f,b,l,p,h,D,H,K){var x=this,d=x.g("button"!==K&&"label"),m=d.text=x.text("",0,0,D).attr({zIndex:1}),y,e,F=0,r=3,
L=0,E,N,t,u,G,J={},R,q,M=/^url\((.*?)\)$/.test(l),Q=M,U,T,O,P;K&&d.addClass("highcharts-"+K);Q=M;U=function(){return(R||0)%2/2};T=function(){var a=m.element.style,k={};e=(void 0===E||void 0===N||G)&&n(m.textStr)&&m.getBBox();d.width=(E||e.width||0)+2*r+L;d.height=(N||e.height||0)+2*r;q=r+x.fontMetrics(a&&a.fontSize,m).b;Q&&(y||(d.box=y=x.symbols[l]||M?x.symbol(l):x.rect(),y.addClass(("button"===K?"":"highcharts-label-box")+(K?" highcharts-"+K+"-box":"")),y.add(d),a=U(),k.x=a,k.y=(H?-q:0)+a),k.width=
Math.round(d.width),k.height=Math.round(d.height),y.attr(g(k,J)),J={})};O=function(){var a=L+r,k;k=H?0:q;n(E)&&e&&("center"===G||"right"===G)&&(a+={center:.5,right:1}[G]*(E-e.width));if(a!==m.x||k!==m.y)m.attr("x",a),void 0!==k&&m.attr("y",k);m.x=a;m.y=k};P=function(a,k){y?y.attr(a,k):J[a]=k};d.onAdd=function(){m.add(d);d.attr({text:k||0===k?k:"",x:f,y:b});y&&n(p)&&d.attr({anchorX:p,anchorY:h})};d.widthSetter=function(k){E=a.isNumber(k)?k:null};d.heightSetter=function(a){N=a};d["text-alignSetter"]=
function(a){G=a};d.paddingSetter=function(a){n(a)&&a!==r&&(r=d.padding=a,O())};d.paddingLeftSetter=function(a){n(a)&&a!==L&&(L=a,O())};d.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==F&&(F=a,e&&d.attr({x:t}))};d.textSetter=function(a){void 0!==a&&m.textSetter(a);T();O()};d["stroke-widthSetter"]=function(a,k){a&&(Q=!0);R=this["stroke-width"]=a;P(k,a)};d.strokeSetter=d.fillSetter=d.rSetter=function(a,k){"r"!==k&&("fill"===k&&a&&(Q=!0),d[k]=a);P(k,a)};d.anchorXSetter=function(a,k){p=d.anchorX=
a;P(k,Math.round(a)-U()-t)};d.anchorYSetter=function(a,k){h=d.anchorY=a;P(k,a-u)};d.xSetter=function(a){d.x=a;F&&(a-=F*((E||e.width)+2*r));t=Math.round(a);d.attr("translateX",t)};d.ySetter=function(a){u=d.y=Math.round(a);d.attr("translateY",u)};var A=d.css;return g(d,{css:function(a){if(a){var k={};a=v(a);c(d.textProps,function(f){void 0!==a[f]&&(k[f]=a[f],delete a[f])});m.css(k)}return A.call(d,a)},getBBox:function(){return{width:e.width+2*r,height:e.height+2*r,x:e.x-r,y:e.y-r}},shadow:function(a){a&&
(T(),y&&y.shadow(a));return d},destroy:function(){w(d.element,"mouseenter");w(d.element,"mouseleave");m&&(m=m.destroy());y&&(y=y.destroy());z.prototype.destroy.call(d);d=x=T=O=P=null}})}});a.Renderer=B})(I);(function(a){var z=a.attr,B=a.createElement,C=a.css,A=a.defined,e=a.each,d=a.extend,q=a.isFirefox,t=a.isMS,u=a.isWebKit,n=a.pInt,h=a.SVGRenderer,r=a.win,m=a.wrap;d(a.SVGElement.prototype,{htmlCss:function(a){var c=this.element;if(c=a&&"SPAN"===c.tagName&&a.width)delete a.width,this.textWidth=c,
this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=d(this.styles,a);C(this.element,a);return this},htmlGetBBox:function(){var a=this.element;"text"===a.nodeName&&(a.style.position="absolute");return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=this.renderer,g=this.element,b=this.translateX||0,p=this.translateY||0,l=this.x||0,h=this.y||0,d=this.textAlign||"left",
f={left:0,center:.5,right:1}[d],H=this.styles;C(g,{marginLeft:b,marginTop:p});this.shadows&&e(this.shadows,function(a){C(a,{marginLeft:b+1,marginTop:p+1})});this.inverted&&e(g.childNodes,function(f){a.invertChild(f,g)});if("SPAN"===g.tagName){var D=this.rotation,m=n(this.textWidth),r=H&&H.whiteSpace,v=[D,d,g.innerHTML,this.textWidth,this.textAlign].join();v!==this.cTT&&(H=a.fontMetrics(g.style.fontSize).b,A(D)&&this.setSpanRotation(D,f,H),C(g,{width:"",whiteSpace:r||"nowrap"}),g.offsetWidth>m&&/[ \-]/.test(g.textContent||
g.innerText)&&C(g,{width:m+"px",display:"block",whiteSpace:r||"normal"}),this.getSpanCorrection(g.offsetWidth,H,f,D,d));C(g,{left:l+(this.xCorr||0)+"px",top:h+(this.yCorr||0)+"px"});u&&(H=g.offsetHeight);this.cTT=v}}else this.alignOnAdd=!0},setSpanRotation:function(a,g,b){var c={},l=t?"-ms-transform":u?"-webkit-transform":q?"MozTransform":r.opera?"-o-transform":"";c[l]=c.transform="rotate("+a+"deg)";c[l+(q?"Origin":"-origin")]=c.transformOrigin=100*g+"% "+b+"px";C(this.element,c)},getSpanCorrection:function(a,
g,b){this.xCorr=-a*b;this.yCorr=-g}});d(h.prototype,{html:function(a,g,b){var c=this.createElement("span"),l=c.element,h=c.renderer,r=h.isSVG,f=function(a,f){e(["opacity","visibility"],function(b){m(a,b+"Setter",function(a,b,c,g){a.call(this,b,c,g);f[c]=b})})};c.textSetter=function(a){a!==l.innerHTML&&delete this.bBox;l.innerHTML=this.textStr=a;c.htmlUpdateTransform()};r&&f(c,c.element.style);c.xSetter=c.ySetter=c.alignSetter=c.rotationSetter=function(a,f){"align"===f&&(f="textAlign");c[f]=a;c.htmlUpdateTransform()};
c.attr({text:a,x:Math.round(g),y:Math.round(b)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});l.style.whiteSpace="nowrap";c.css=c.htmlCss;r&&(c.add=function(a){var b,g=h.box.parentNode,p=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)p.push(a),a=a.parentGroup;e(p.reverse(),function(a){var l,v=z(a.element,"class");v&&(v={className:v});b=a.div=a.div||B("div",v,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,
opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},b||g);l=b.style;d(a,{classSetter:function(a){this.element.setAttribute("class",a);b.className=a},on:function(){p[0].div&&c.on.apply({element:p[0].div},arguments);return a},translateXSetter:function(f,k){l.left=f+"px";a[k]=f;a.doTransform=!0},translateYSetter:function(f,k){l.top=f+"px";a[k]=f;a.doTransform=!0}});f(a,l)})}}else b=g;b.appendChild(l);c.added=!0;c.alignOnAdd&&c.htmlUpdateTransform();return c});return c}})})(I);(function(a){var z,
B,C=a.createElement,A=a.css,e=a.defined,d=a.deg2rad,q=a.discardElement,t=a.doc,u=a.each,n=a.erase,h=a.extend;z=a.extendClass;var r=a.isArray,m=a.isNumber,c=a.isObject,g=a.merge;B=a.noop;var b=a.pick,p=a.pInt,l=a.SVGElement,E=a.SVGRenderer,G=a.win;a.svg||(B={docMode8:t&&8===t.documentMode,init:function(a,b){var f=["\x3c",b,' filled\x3d"f" stroked\x3d"f"'],c=["position: ","absolute",";"],g="div"===b;("shape"===b||g)&&c.push("left:0;top:0;width:1px;height:1px;");c.push("visibility: ",g?"hidden":"visible");
f.push(' style\x3d"',c.join(""),'"/\x3e');b&&(f=g||"span"===b||"img"===b?f.join(""):a.prepVML(f),this.element=C(f));this.renderer=a},add:function(a){var f=this.renderer,b=this.element,c=f.box,g=a&&a.inverted,c=a?a.element||a:c;a&&(this.parentGroup=a);g&&f.invertChild(b,c);c.appendChild(b);this.added=!0;this.alignOnAdd&&!this.deferUpdateTransform&&this.updateTransform();if(this.onAdd)this.onAdd();this.className&&this.attr("class",this.className);return this},updateTransform:l.prototype.htmlUpdateTransform,
setSpanRotation:function(){var a=this.rotation,b=Math.cos(a*d),c=Math.sin(a*d);A(this.element,{filter:a?["progid:DXImageTransform.Microsoft.Matrix(M11\x3d",b,", M12\x3d",-c,", M21\x3d",c,", M22\x3d",b,", sizingMethod\x3d'auto expand')"].join(""):"none"})},getSpanCorrection:function(a,c,g,l,h){var f=l?Math.cos(l*d):1,p=l?Math.sin(l*d):0,D=b(this.elemHeight,this.element.offsetHeight),m;this.xCorr=0>f&&-a;this.yCorr=0>p&&-D;m=0>f*p;this.xCorr+=p*c*(m?1-g:g);this.yCorr-=f*c*(l?m?g:1-g:1);h&&"left"!==
h&&(this.xCorr-=a*g*(0>f?-1:1),l&&(this.yCorr-=D*g*(0>p?-1:1)),A(this.element,{textAlign:h}))},pathToVML:function(a){for(var f=a.length,b=[];f--;)m(a[f])?b[f]=Math.round(10*a[f])-5:"Z"===a[f]?b[f]="x":(b[f]=a[f],!a.isArc||"wa"!==a[f]&&"at"!==a[f]||(b[f+5]===b[f+7]&&(b[f+7]+=a[f+7]>a[f+5]?1:-1),b[f+6]===b[f+8]&&(b[f+8]+=a[f+8]>a[f+6]?1:-1)));return b.join(" ")||"x"},clip:function(a){var f=this,b;a?(b=a.members,n(b,f),b.push(f),f.destroyClip=function(){n(b,f)},a=a.getCSS(f)):(f.destroyClip&&f.destroyClip(),
a={clip:f.docMode8?"inherit":"rect(auto)"});return f.css(a)},css:l.prototype.htmlCss,safeRemoveChild:function(a){a.parentNode&&q(a)},destroy:function(){this.destroyClip&&this.destroyClip();return l.prototype.destroy.apply(this)},on:function(a,b){this.element["on"+a]=function(){var a=G.event;a.target=a.srcElement;b(a)};return this},cutOffPath:function(a,b){var f;a=a.split(/[ ,]/);f=a.length;if(9===f||11===f)a[f-4]=a[f-2]=p(a[f-2])-10*b;return a.join(" ")},shadow:function(a,c,g){var f=[],l,v=this.element,
h=this.renderer,d,m=v.style,k,w=v.path,D,K,e,r;w&&"string"!==typeof w.value&&(w="x");K=w;if(a){e=b(a.width,3);r=(a.opacity||.15)/e;for(l=1;3>=l;l++)D=2*e+1-2*l,g&&(K=this.cutOffPath(w.value,D+.5)),k=['\x3cshape isShadow\x3d"true" strokeweight\x3d"',D,'" filled\x3d"false" path\x3d"',K,'" coordsize\x3d"10 10" style\x3d"',v.style.cssText,'" /\x3e'],d=C(h.prepVML(k),null,{left:p(m.left)+b(a.offsetX,1),top:p(m.top)+b(a.offsetY,1)}),g&&(d.cutOff=D+1),k=['\x3cstroke color\x3d"',a.color||"#000000",'" opacity\x3d"',
r*l,'"/\x3e'],C(h.prepVML(k),null,null,d),c?c.element.appendChild(d):v.parentNode.insertBefore(d,v),f.push(d);this.shadows=f}return this},updateShadows:B,setAttr:function(a,b){this.docMode8?this.element[a]=b:this.element.setAttribute(a,b)},classSetter:function(a){(this.added?this.element:this).className=a},dashstyleSetter:function(a,b,c){(c.getElementsByTagName("stroke")[0]||C(this.renderer.prepVML(["\x3cstroke/\x3e"]),null,null,c))[b]=a||"solid";this[b]=a},dSetter:function(a,b,c){var f=this.shadows;
a=a||[];this.d=a.join&&a.join(" ");c.path=a=this.pathToVML(a);if(f)for(c=f.length;c--;)f[c].path=f[c].cutOff?this.cutOffPath(a,f[c].cutOff):a;this.setAttr(b,a)},fillSetter:function(a,b,c){var f=c.nodeName;"SPAN"===f?c.style.color=a:"IMG"!==f&&(c.filled="none"!==a,this.setAttr("fillcolor",this.renderer.color(a,c,b,this)))},"fill-opacitySetter":function(a,b,c){C(this.renderer.prepVML(["\x3c",b.split("-")[0],' opacity\x3d"',a,'"/\x3e']),null,null,c)},opacitySetter:B,rotationSetter:function(a,b,c){c=
c.style;this[b]=c[b]=a;c.left=-Math.round(Math.sin(a*d)+1)+"px";c.top=Math.round(Math.cos(a*d))+"px"},strokeSetter:function(a,b,c){this.setAttr("strokecolor",this.renderer.color(a,c,b,this))},"stroke-widthSetter":function(a,b,c){c.stroked=!!a;this[b]=a;m(a)&&(a+="px");this.setAttr("strokeweight",a)},titleSetter:function(a,b){this.setAttr(b,a)},visibilitySetter:function(a,b,c){"inherit"===a&&(a="visible");this.shadows&&u(this.shadows,function(f){f.style[b]=a});"DIV"===c.nodeName&&(a="hidden"===a?"-999em":
0,this.docMode8||(c.style[b]=a?"visible":"hidden"),b="top");c.style[b]=a},xSetter:function(a,b,c){this[b]=a;"x"===b?b="left":"y"===b&&(b="top");this.updateClipping?(this[b]=a,this.updateClipping()):c.style[b]=a},zIndexSetter:function(a,b,c){c.style[b]=a}},B["stroke-opacitySetter"]=B["fill-opacitySetter"],a.VMLElement=B=z(l,B),B.prototype.ySetter=B.prototype.widthSetter=B.prototype.heightSetter=B.prototype.xSetter,B={Element:B,isIE8:-1<G.navigator.userAgent.indexOf("MSIE 8.0"),init:function(a,b,c){var f,
g;this.alignedObjects=[];f=this.createElement("div").css({position:"relative"});g=f.element;a.appendChild(f.element);this.isVML=!0;this.box=g;this.boxWrapper=f;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(b,c,!1);if(!t.namespaces.hcv){t.namespaces.add("hcv","urn:schemas-microsoft-com:vml");try{t.createStyleSheet().cssText="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}catch(v){t.styleSheets[0].cssText+="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}}},
isHidden:function(){return!this.box.offsetWidth},clipRect:function(a,b,g,l){var f=this.createElement(),v=c(a);return h(f,{members:[],count:0,left:(v?a.x:a)+1,top:(v?a.y:b)+1,width:(v?a.width:g)-1,height:(v?a.height:l)-1,getCSS:function(a){var b=a.element,f=b.nodeName,k=a.inverted,w=this.top-("shape"===f?b.offsetTop:0),c=this.left,b=c+this.width,g=w+this.height,w={clip:"rect("+Math.round(k?c:w)+"px,"+Math.round(k?g:b)+"px,"+Math.round(k?b:g)+"px,"+Math.round(k?w:c)+"px)"};!k&&a.docMode8&&"DIV"===f&&
h(w,{width:b+"px",height:g+"px"});return w},updateClipping:function(){u(f.members,function(a){a.element&&a.css(f.getCSS(a))})}})},color:function(b,c,g,l){var f=this,v,h=/^rgba/,p,d,k="none";b&&b.linearGradient?d="gradient":b&&b.radialGradient&&(d="pattern");if(d){var w,m,K=b.linearGradient||b.radialGradient,e,D,r,x,n,y="";b=b.stops;var E,t=[],H=function(){p=['\x3cfill colors\x3d"'+t.join(",")+'" opacity\x3d"',r,'" o:opacity2\x3d"',D,'" type\x3d"',d,'" ',y,'focus\x3d"100%" method\x3d"any" /\x3e'];
C(f.prepVML(p),null,null,c)};e=b[0];E=b[b.length-1];0<e[0]&&b.unshift([0,e[1]]);1>E[0]&&b.push([1,E[1]]);u(b,function(k,b){h.test(k[1])?(v=a.color(k[1]),w=v.get("rgb"),m=v.get("a")):(w=k[1],m=1);t.push(100*k[0]+"% "+w);b?(r=m,x=w):(D=m,n=w)});if("fill"===g)if("gradient"===d)g=K.x1||K[0]||0,b=K.y1||K[1]||0,e=K.x2||K[2]||0,K=K.y2||K[3]||0,y='angle\x3d"'+(90-180*Math.atan((K-b)/(e-g))/Math.PI)+'"',H();else{var k=K.r,G=2*k,q=2*k,A=K.cx,z=K.cy,B=c.radialReference,I,k=function(){B&&(I=l.getBBox(),A+=(B[0]-
I.x)/I.width-.5,z+=(B[1]-I.y)/I.height-.5,G*=B[2]/I.width,q*=B[2]/I.height);y='src\x3d"'+a.getOptions().global.VMLRadialGradientURL+'" size\x3d"'+G+","+q+'" origin\x3d"0.5,0.5" position\x3d"'+A+","+z+'" color2\x3d"'+n+'" ';H()};l.added?k():l.onAdd=k;k=x}else k=w}else h.test(b)&&"IMG"!==c.tagName?(v=a.color(b),l[g+"-opacitySetter"](v.get("a"),g,c),k=v.get("rgb")):(k=c.getElementsByTagName(g),k.length&&(k[0].opacity=1,k[0].type="solid"),k=b);return k},prepVML:function(a){var b=this.isIE8;a=a.join("");
b?(a=a.replace("/\x3e",' xmlns\x3d"urn:schemas-microsoft-com:vml" /\x3e'),a=-1===a.indexOf('style\x3d"')?a.replace("/\x3e",' style\x3d"display:inline-block;behavior:url(#default#VML);" /\x3e'):a.replace('style\x3d"','style\x3d"display:inline-block;behavior:url(#default#VML);')):a=a.replace("\x3c","\x3chcv:");return a},text:E.prototype.html,path:function(a){var b={coordsize:"10 10"};r(a)?b.d=a:c(a)&&h(b,a);return this.createElement("shape").attr(b)},circle:function(a,b,g){var f=this.symbol("circle");
c(a)&&(g=a.r,b=a.y,a=a.x);f.isCircle=!0;f.r=g;return f.attr({x:a,y:b})},g:function(a){var b;a&&(b={className:"highcharts-"+a,"class":"highcharts-"+a});return this.createElement("div").attr(b)},image:function(a,b,c,g,l){var f=this.createElement("img").attr({src:a});1<arguments.length&&f.attr({x:b,y:c,width:g,height:l});return f},createElement:function(a){return"rect"===a?this.symbol(a):E.prototype.createElement.call(this,a)},invertChild:function(a,b){var c=this;b=b.style;var f="IMG"===a.tagName&&a.style;
A(a,{flip:"x",left:p(b.width)-(f?p(f.top):1),top:p(b.height)-(f?p(f.left):1),rotation:-90});u(a.childNodes,function(b){c.invertChild(b,a)})},symbols:{arc:function(a,b,c,g,l){var f=l.start,h=l.end,p=l.r||c||g;c=l.innerR;g=Math.cos(f);var d=Math.sin(f),k=Math.cos(h),w=Math.sin(h);if(0===h-f)return["x"];f=["wa",a-p,b-p,a+p,b+p,a+p*g,b+p*d,a+p*k,b+p*w];l.open&&!c&&f.push("e","M",a,b);f.push("at",a-c,b-c,a+c,b+c,a+c*k,b+c*w,a+c*g,b+c*d,"x","e");f.isArc=!0;return f},circle:function(a,b,c,g,l){l&&e(l.r)&&
(c=g=2*l.r);l&&l.isCircle&&(a-=c/2,b-=g/2);return["wa",a,b,a+c,b+g,a+c,b+g/2,a+c,b+g/2,"e"]},rect:function(a,b,c,g,l){return E.prototype.symbols[e(l)&&l.r?"callout":"square"].call(0,a,b,c,g,l)}}},a.VMLRenderer=z=function(){this.init.apply(this,arguments)},z.prototype=g(E.prototype,B),a.Renderer=z);E.prototype.measureSpanWidth=function(a,b){var c=t.createElement("span");a=t.createTextNode(a);c.appendChild(a);A(c,b);this.box.appendChild(c);b=c.offsetWidth;q(c);return b}})(I);(function(a){var z=a.correctFloat,
B=a.defined,C=a.destroyObjectProperties,A=a.isNumber,e=a.merge,d=a.pick,q=a.deg2rad;a.Tick=function(a,d,e,h){this.axis=a;this.pos=d;this.type=e||"";this.isNewLabel=this.isNew=!0;e||h||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,u=a.options,n=a.chart,h=a.categories,r=a.names,m=this.pos,c=u.labels,g=a.tickPositions,b=m===g[0],p=m===g[g.length-1],r=h?d(h[m],r[m],m):m,h=this.label,g=g.info,l;a.isDatetimeAxis&&g&&(l=u.dateTimeLabelFormats[g.higherRanks[m]||g.unitName]);this.isFirst=
b;this.isLast=p;u=a.labelFormatter.call({axis:a,chart:n,isFirst:b,isLast:p,dateTimeLabelFormat:l,value:a.isLog?z(a.lin2log(r)):r,pos:m});B(h)?h&&h.attr({text:u}):(this.labelLength=(this.label=h=B(u)&&c.enabled?n.renderer.text(u,0,0,c.useHTML).css(e(c.style)).add(a.labelGroup):null)&&h.getBBox().width,this.rotation=0)},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(a){var e=this.axis,n=a.x,h=e.chart.chartWidth,r=e.chart.spacing,
m=d(e.labelLeft,Math.min(e.pos,r[3])),r=d(e.labelRight,Math.max(e.pos+e.len,h-r[1])),c=this.label,g=this.rotation,b={left:0,center:.5,right:1}[e.labelAlign],p=c.getBBox().width,l=e.getSlotWidth(),E=l,t=1,f,H={};if(g)0>g&&n-b*p<m?f=Math.round(n/Math.cos(g*q)-m):0<g&&n+b*p>r&&(f=Math.round((h-n)/Math.cos(g*q)));else if(h=n+(1-b)*p,n-b*p<m?E=a.x+E*(1-b)-m:h>r&&(E=r-a.x+E*b,t=-1),E=Math.min(l,E),E<l&&"center"===e.labelAlign&&(a.x+=t*(l-E-b*(l-Math.min(p,E)))),p>E||e.autoRotation&&(c.styles||{}).width)f=
E;f&&(H.width=f,(e.options.labels.style||{}).textOverflow||(H.textOverflow="ellipsis"),c.css(H))},getPosition:function(a,d,e,h){var r=this.axis,m=r.chart,c=h&&m.oldChartHeight||m.chartHeight;return{x:a?r.translate(d+e,null,null,h)+r.transB:r.left+r.offset+(r.opposite?(h&&m.oldChartWidth||m.chartWidth)-r.right-r.left:0),y:a?c-r.bottom+r.offset-(r.opposite?r.height:0):c-r.translate(d+e,null,null,h)-r.transB}},getLabelPosition:function(a,d,e,h,r,m,c,g){var b=this.axis,p=b.transA,l=b.reversed,n=b.staggerLines,
t=b.tickRotCorr||{x:0,y:0},f=r.y;B(f)||(f=0===b.side?e.rotation?-8:-e.getBBox().height:2===b.side?t.y+8:Math.cos(e.rotation*q)*(t.y-e.getBBox(!1,0).height/2));a=a+r.x+t.x-(m&&h?m*p*(l?-1:1):0);d=d+f-(m&&!h?m*p*(l?1:-1):0);n&&(e=c/(g||1)%n,b.opposite&&(e=n-e-1),d+=b.labelOffset/n*e);return{x:a,y:Math.round(d)}},getMarkPath:function(a,d,e,h,r,m){return m.crispLine(["M",a,d,"L",a+(r?0:-e),d+(r?e:0)],h)},renderGridLine:function(a,d,e){var h=this.axis,r=h.options,m=this.gridLine,c={},g=this.pos,b=this.type,
p=h.tickmarkOffset,l=h.chart.renderer,n=b?b+"Grid":"grid",t=r[n+"LineWidth"],f=r[n+"LineColor"],r=r[n+"LineDashStyle"];m||(c.stroke=f,c["stroke-width"]=t,r&&(c.dashstyle=r),b||(c.zIndex=1),a&&(c.opacity=0),this.gridLine=m=l.path().attr(c).addClass("highcharts-"+(b?b+"-":"")+"grid-line").add(h.gridGroup));if(!a&&m&&(a=h.getPlotLinePath(g+p,m.strokeWidth()*e,a,!0)))m[this.isNew?"attr":"animate"]({d:a,opacity:d})},renderMark:function(a,e,n){var h=this.axis,r=h.options,m=h.chart.renderer,c=this.type,
g=c?c+"Tick":"tick",b=h.tickSize(g),p=this.mark,l=!p,E=a.x;a=a.y;var t=d(r[g+"Width"],!c&&h.isXAxis?1:0),r=r[g+"Color"];b&&(h.opposite&&(b[0]=-b[0]),l&&(this.mark=p=m.path().addClass("highcharts-"+(c?c+"-":"")+"tick").add(h.axisGroup),p.attr({stroke:r,"stroke-width":t})),p[l?"attr":"animate"]({d:this.getMarkPath(E,a,b[0],p.strokeWidth()*n,h.horiz,m),opacity:e}))},renderLabel:function(a,e,n,h){var r=this.axis,m=r.horiz,c=r.options,g=this.label,b=c.labels,p=b.step,l=r.tickmarkOffset,E=!0,G=a.x;a=a.y;
g&&A(G)&&(g.xy=a=this.getLabelPosition(G,a,g,m,b,l,h,p),this.isFirst&&!this.isLast&&!d(c.showFirstLabel,1)||this.isLast&&!this.isFirst&&!d(c.showLastLabel,1)?E=!1:!m||r.isRadial||b.step||b.rotation||e||0===n||this.handleOverflow(a),p&&h%p&&(E=!1),E&&A(a.y)?(a.opacity=n,g[this.isNewLabel?"attr":"animate"](a),this.isNewLabel=!1):(g.attr("y",-9999),this.isNewLabel=!0),this.isNew=!1)},render:function(a,e,n){var h=this.axis,r=h.horiz,m=this.getPosition(r,this.pos,h.tickmarkOffset,e),c=m.x,g=m.y,h=r&&c===
h.pos+h.len||!r&&g===h.pos?-1:1;n=d(n,1);this.isActive=!0;this.renderGridLine(e,n,h);this.renderMark(m,n,h);this.renderLabel(m,e,n,a)},destroy:function(){C(this,this.axis)}}})(I);var S=function(a){var z=a.addEvent,B=a.animObject,C=a.arrayMax,A=a.arrayMin,e=a.color,d=a.correctFloat,q=a.defaultOptions,t=a.defined,u=a.deg2rad,n=a.destroyObjectProperties,h=a.each,r=a.extend,m=a.fireEvent,c=a.format,g=a.getMagnitude,b=a.grep,p=a.inArray,l=a.isArray,E=a.isNumber,G=a.isString,f=a.merge,H=a.normalizeTickInterval,
D=a.objectEach,y=a.pick,M=a.removeEvent,v=a.splat,L=a.syncTimeout,F=a.Tick,J=function(){this.init.apply(this,arguments)};a.extend(J.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},x:0},minPadding:.01,maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",startOfWeek:1,startOnTick:!1,
tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,
formatter:function(){return a.numberFormat(this.total,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},init:function(a,b){var k=b.isX,c=this;c.chart=a;c.horiz=
a.inverted&&!c.isZAxis?!k:k;c.isXAxis=k;c.coll=c.coll||(k?"xAxis":"yAxis");c.opposite=b.opposite;c.side=b.side||(c.horiz?c.opposite?0:2:c.opposite?1:3);c.setOptions(b);var w=this.options,f=w.type;c.labelFormatter=w.labels.formatter||c.defaultLabelFormatter;c.userOptions=b;c.minPixelPadding=0;c.reversed=w.reversed;c.visible=!1!==w.visible;c.zoomEnabled=!1!==w.zoomEnabled;c.hasNames="category"===f||!0===w.categories;c.categories=w.categories||c.hasNames;c.names=c.names||[];c.plotLinesAndBandsGroups=
{};c.isLog="logarithmic"===f;c.isDatetimeAxis="datetime"===f;c.positiveValuesOnly=c.isLog&&!c.allowNegativeLog;c.isLinked=t(w.linkedTo);c.ticks={};c.labelEdge=[];c.minorTicks={};c.plotLinesAndBands=[];c.alternateBands={};c.len=0;c.minRange=c.userMinRange=w.minRange||w.maxZoom;c.range=w.range;c.offset=w.offset||0;c.stacks={};c.oldStacks={};c.stacksTouched=0;c.max=null;c.min=null;c.crosshair=y(w.crosshair,v(a.options.tooltip.crosshairs)[k?0:1],!1);b=c.options.events;-1===p(c,a.axes)&&(k?a.axes.splice(a.xAxis.length,
0,c):a.axes.push(c),a[c.coll].push(c));c.series=c.series||[];a.inverted&&!c.isZAxis&&k&&void 0===c.reversed&&(c.reversed=!0);D(b,function(a,k){z(c,k,a)});c.lin2log=w.linearToLogConverter||c.lin2log;c.isLog&&(c.val2lin=c.log2lin,c.lin2val=c.lin2log)},setOptions:function(a){this.options=f(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],f(q[this.coll],a))},
defaultLabelFormatter:function(){var k=this.axis,b=this.value,f=k.categories,g=this.dateTimeLabelFormat,l=q.lang,v=l.numericSymbols,l=l.numericSymbolMagnitude||1E3,h=v&&v.length,p,d=k.options.labels.format,k=k.isLog?Math.abs(b):k.tickInterval;if(d)p=c(d,this);else if(f)p=b;else if(g)p=a.dateFormat(g,b);else if(h&&1E3<=k)for(;h--&&void 0===p;)f=Math.pow(l,h+1),k>=f&&0===10*b%f&&null!==v[h]&&0!==b&&(p=a.numberFormat(b/f,-1)+v[h]);void 0===p&&(p=1E4<=Math.abs(b)?a.numberFormat(b,-1):a.numberFormat(b,
-1,void 0,""));return p},getSeriesExtremes:function(){var a=this,c=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();h(a.series,function(k){if(k.visible||!c.options.chart.ignoreHiddenSeries){var f=k.options,w=f.threshold,g;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=w&&(w=null);if(a.isXAxis)f=k.xData,f.length&&(k=A(f),E(k)||k instanceof Date||(f=b(f,function(a){return E(a)}),k=A(f)),a.dataMin=Math.min(y(a.dataMin,f[0]),
k),a.dataMax=Math.max(y(a.dataMax,f[0]),C(f)));else if(k.getExtremes(),g=k.dataMax,k=k.dataMin,t(k)&&t(g)&&(a.dataMin=Math.min(y(a.dataMin,k),k),a.dataMax=Math.max(y(a.dataMax,g),g)),t(w)&&(a.threshold=w),!f.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})},translate:function(a,b,c,f,g,l){var k=this.linkedParent||this,w=1,v=0,h=f?k.oldTransA:k.transA;f=f?k.oldMin:k.min;var p=k.minPixelPadding;g=(k.isOrdinal||k.isBroken||k.isLog&&g)&&k.lin2val;h||(h=k.transA);c&&(w*=-1,v=k.len);k.reversed&&
(w*=-1,v-=w*(k.sector||k.len));b?(a=(a*w+v-p)/h+f,g&&(a=k.lin2val(a))):(g&&(a=k.val2lin(a)),a=w*(a-f)*h+v+w*p+(E(l)?h*l:0));return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,b,c,f,g){var k=this.chart,w=this.left,l=this.top,v,h,p=c&&k.oldChartHeight||k.chartHeight,d=c&&k.oldChartWidth||k.chartWidth,e;v=this.transB;var m=function(a,k,b){if(a<
k||a>b)f?a=Math.min(Math.max(k,a),b):e=!0;return a};g=y(g,this.translate(a,null,null,c));a=c=Math.round(g+v);v=h=Math.round(p-g-v);E(g)?this.horiz?(v=l,h=p-this.bottom,a=c=m(a,w,w+this.width)):(a=w,c=d-this.right,v=h=m(v,l,l+this.height)):e=!0;return e&&!f?null:k.renderer.crispLine(["M",a,v,"L",c,h],b||1)},getLinearTickPositions:function(a,b,c){var k,f=d(Math.floor(b/a)*a);c=d(Math.ceil(c/a)*a);var w=[];if(this.single)return[b];for(b=f;b<=c;){w.push(b);b=d(b+a);if(b===k)break;k=b}return w},getMinorTickPositions:function(){var a=
this,b=a.options,c=a.tickPositions,f=a.minorTickInterval,g=[],l=a.pointRangePadding||0,v=a.min-l,l=a.max+l,p=l-v;if(p&&p/f<a.len/3)if(a.isLog)h(this.paddedTicks,function(k,b,c){b&&g.push.apply(g,a.getLogTickPositions(f,c[b-1],c[b],!0))});else if(a.isDatetimeAxis&&"auto"===b.minorTickInterval)g=g.concat(a.getTimeTicks(a.normalizeTimeTickInterval(f),v,l,b.startOfWeek));else for(b=v+(c[0]-v)%f;b<=l&&b!==g[0];b+=f)g.push(b);0!==g.length&&a.trimTicks(g);return g},adjustForMinRange:function(){var a=this.options,
b=this.min,c=this.max,f,g,l,v,p,d,e,m;this.isXAxis&&void 0===this.minRange&&!this.isLog&&(t(a.min)||t(a.max)?this.minRange=null:(h(this.series,function(a){d=a.xData;for(v=e=a.xIncrement?1:d.length-1;0<v;v--)if(p=d[v]-d[v-1],void 0===l||p<l)l=p}),this.minRange=Math.min(5*l,this.dataMax-this.dataMin)));c-b<this.minRange&&(g=this.dataMax-this.dataMin>=this.minRange,m=this.minRange,f=(m-c+b)/2,f=[b-f,y(a.min,b-f)],g&&(f[2]=this.isLog?this.log2lin(this.dataMin):this.dataMin),b=C(f),c=[b+m,y(a.max,b+m)],
g&&(c[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),c=A(c),c-b<m&&(f[0]=c-m,f[1]=y(a.min,c-m),b=C(f)));this.min=b;this.max=c},getClosest:function(){var a;this.categories?a=1:h(this.series,function(k){var b=k.closestPointRange,c=k.visible||!k.chart.options.chart.ignoreHiddenSeries;!k.noSharedTooltip&&t(b)&&c&&(a=t(a)?Math.min(a,b):b)});return a},nameToX:function(a){var k=l(this.categories),b=k?this.categories:this.names,c=a.options.x,f;a.series.requireSorting=!1;t(c)||(c=!1===this.options.uniqueNames?
a.series.autoIncrement():p(a.name,b));-1===c?k||(f=b.length):f=c;void 0!==f&&(this.names[f]=a.name);return f},updateNames:function(){var a=this;0<this.names.length&&(this.names.length=0,this.minRange=this.userMinRange,h(this.series||[],function(k){k.xIncrement=null;if(!k.points||k.isDirtyData)k.processData(),k.generatePoints();h(k.points,function(b,c){var f;b.options&&(f=a.nameToX(b),void 0!==f&&f!==b.x&&(b.x=f,k.xData[c]=f))})}))},setAxisTranslation:function(a){var k=this,b=k.max-k.min,c=k.axisPointRange||
0,f,g=0,l=0,v=k.linkedParent,p=!!k.categories,d=k.transA,e=k.isXAxis;if(e||p||c)f=k.getClosest(),v?(g=v.minPointOffset,l=v.pointRangePadding):h(k.series,function(a){var b=p?1:e?y(a.options.pointRange,f,0):k.axisPointRange||0;a=a.options.pointPlacement;c=Math.max(c,b);k.single||(g=Math.max(g,G(a)?0:b/2),l=Math.max(l,"on"===a?0:b))}),v=k.ordinalSlope&&f?k.ordinalSlope/f:1,k.minPointOffset=g*=v,k.pointRangePadding=l*=v,k.pointRange=Math.min(c,b),e&&(k.closestPointRange=f);a&&(k.oldTransA=d);k.translationSlope=
k.transA=d=k.options.staticScale||k.len/(b+l||1);k.transB=k.horiz?k.left:k.bottom;k.minPixelPadding=d*g},minFromRange:function(){return this.max-this.range},setTickInterval:function(k){var b=this,c=b.chart,f=b.options,l=b.isLog,v=b.log2lin,p=b.isDatetimeAxis,e=b.isXAxis,r=b.isLinked,n=f.maxPadding,D=f.minPadding,F=f.tickInterval,L=f.tickPixelInterval,G=b.categories,u=b.threshold,q=b.softThreshold,J,M,A,z;p||G||r||this.getTickAmount();A=y(b.userMin,f.min);z=y(b.userMax,f.max);r?(b.linkedParent=c[b.coll][f.linkedTo],
c=b.linkedParent.getExtremes(),b.min=y(c.min,c.dataMin),b.max=y(c.max,c.dataMax),f.type!==b.linkedParent.options.type&&a.error(11,1)):(!q&&t(u)&&(b.dataMin>=u?(J=u,D=0):b.dataMax<=u&&(M=u,n=0)),b.min=y(A,J,b.dataMin),b.max=y(z,M,b.dataMax));l&&(b.positiveValuesOnly&&!k&&0>=Math.min(b.min,y(b.dataMin,b.min))&&a.error(10,1),b.min=d(v(b.min),15),b.max=d(v(b.max),15));b.range&&t(b.max)&&(b.userMin=b.min=A=Math.max(b.dataMin,b.minFromRange()),b.userMax=z=b.max,b.range=null);m(b,"foundExtremes");b.beforePadding&&
b.beforePadding();b.adjustForMinRange();!(G||b.axisPointRange||b.usePercentage||r)&&t(b.min)&&t(b.max)&&(v=b.max-b.min)&&(!t(A)&&D&&(b.min-=v*D),!t(z)&&n&&(b.max+=v*n));E(f.softMin)&&(b.min=Math.min(b.min,f.softMin));E(f.softMax)&&(b.max=Math.max(b.max,f.softMax));E(f.floor)&&(b.min=Math.max(b.min,f.floor));E(f.ceiling)&&(b.max=Math.min(b.max,f.ceiling));q&&t(b.dataMin)&&(u=u||0,!t(A)&&b.min<u&&b.dataMin>=u?b.min=u:!t(z)&&b.max>u&&b.dataMax<=u&&(b.max=u));b.tickInterval=b.min===b.max||void 0===b.min||
void 0===b.max?1:r&&!F&&L===b.linkedParent.options.tickPixelInterval?F=b.linkedParent.tickInterval:y(F,this.tickAmount?(b.max-b.min)/Math.max(this.tickAmount-1,1):void 0,G?1:(b.max-b.min)*L/Math.max(b.len,L));e&&!k&&h(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();b.postProcessTickInterval&&(b.tickInterval=b.postProcessTickInterval(b.tickInterval));b.pointRange&&!F&&(b.tickInterval=Math.max(b.pointRange,
b.tickInterval));k=y(f.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);!F&&b.tickInterval<k&&(b.tickInterval=k);p||l||F||(b.tickInterval=H(b.tickInterval,null,g(b.tickInterval),y(f.allowDecimals,!(.5<b.tickInterval&&5>b.tickInterval&&1E3<b.max&&9999>b.max)),!!this.tickAmount));this.tickAmount||(b.tickInterval=b.unsquish());this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions,f=a.tickPositioner,g=a.startOnTick,l=a.endOnTick;this.tickmarkOffset=this.categories&&
"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===a.minorTickInterval&&this.tickInterval?this.tickInterval/5:a.minorTickInterval;this.single=this.min===this.max&&t(this.min)&&!this.tickAmount&&(parseInt(this.min,10)===this.min||!1!==a.allowDecimals);this.tickPositions=b=c&&c.slice();!b&&(b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,
!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),b.length>this.len&&(b=[b[0],b.pop()]),this.tickPositions=b,f&&(f=f.apply(this,[this.min,this.max])))&&(this.tickPositions=b=f);this.paddedTicks=b.slice(0);this.trimTicks(b,g,l);this.isLinked||(this.single&&2>b.length&&(this.min-=.5,this.max+=.5),c||f||this.adjustTickAmount())},trimTicks:function(a,b,c){var k=a[0],f=a[a.length-1],g=this.minPointOffset||0;if(!this.isLinked){if(b&&
-Infinity!==k)this.min=k;else for(;this.min-g>a[0];)a.shift();if(c)this.max=f;else for(;this.max+g<a[a.length-1];)a.pop();0===a.length&&t(k)&&a.push((f+k)/2)}},alignToOthers:function(){var a={},b,c=this.options;!1===this.chart.options.chart.alignTicks||!1===c.alignTicks||this.isLog||h(this.chart[this.coll],function(k){var c=k.options,c=[k.horiz?c.left:c.top,c.width,c.height,c.pane].join();k.series.length&&(a[c]?b=!0:a[c]=1)});return b},getTickAmount:function(){var a=this.options,b=a.tickAmount,c=
a.tickPixelInterval;!t(a.tickInterval)&&this.len<c&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(b=2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/c)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=this.tickInterval,b=this.tickPositions,c=this.tickAmount,f=this.finalTickAmt,g=b&&b.length;if(g<c){for(;b.length<c;)b.push(d(b[b.length-1]+a));this.transA*=(g-1)/(c-1);this.max=b[b.length-1]}else g>c&&(this.tickInterval*=2,this.setTickPositions());
if(t(f)){for(a=c=b.length;a--;)(3===f&&1===a%2||2>=f&&0<a&&a<c-1)&&b.splice(a,1);this.finalTickAmt=void 0}},setScale:function(){var a,b;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();b=this.len!==this.oldAxisLength;h(this.series,function(b){if(b.isDirtyData||b.isDirty||b.xAxis.isDirty)a=!0});b||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&this.resetStacks(),this.forceRedraw=
!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||(this.isDirty=b||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks()},setExtremes:function(a,b,c,f,g){var k=this,l=k.chart;c=y(c,!0);h(k.series,function(a){delete a.kdTree});g=r(g,{min:a,max:b});m(k,"setExtremes",g,function(){k.userMin=a;k.userMax=b;k.eventArgs=g;c&&l.redraw(f)})},zoom:function(a,b){var k=this.dataMin,c=this.dataMax,f=this.options,
g=Math.min(k,y(f.min,k)),f=Math.max(c,y(f.max,c));if(a!==this.min||b!==this.max)this.allowZoomOutside||(t(k)&&(a<g&&(a=g),a>f&&(a=f)),t(c)&&(b<g&&(b=g),b>f&&(b=f))),this.displayBtn=void 0!==a||void 0!==b,this.setExtremes(a,b,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var b=this.chart,c=this.options,f=c.offsets||[0,0,0,0],g=this.horiz,l=this.width=Math.round(a.relativeLength(y(c.width,b.plotWidth-f[3]+f[1]),b.plotWidth)),v=this.height=Math.round(a.relativeLength(y(c.height,b.plotHeight-
f[0]+f[2]),b.plotHeight)),h=this.top=Math.round(a.relativeLength(y(c.top,b.plotTop+f[0]),b.plotHeight,b.plotTop)),c=this.left=Math.round(a.relativeLength(y(c.left,b.plotLeft+f[3]),b.plotWidth,b.plotLeft));this.bottom=b.chartHeight-v-h;this.right=b.chartWidth-l-c;this.len=Math.max(g?l:v,0);this.pos=g?c:h},getExtremes:function(){var a=this.isLog,b=this.lin2log;return{min:a?d(b(this.min)):this.min,max:a?d(b(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},
getThreshold:function(a){var b=this.isLog,c=this.lin2log,k=b?c(this.min):this.min,b=b?c(this.max):this.max;null===a?a=k:k>a?a=k:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(y(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var b=this.options,c=b[a+"Length"],k=y(b[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(k&&c)return"inside"===b[a+"Position"]&&(c=-c),[c,k]},labelMetrics:function(){var a=this.tickPositions&&this.tickPositions[0]||
0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,b=this.horiz,c=this.tickInterval,f=c,g=this.len/(((this.categories?1:0)+this.max-this.min)/c),l,v=a.rotation,p=this.labelMetrics(),d,e=Number.MAX_VALUE,m,r=function(a){a/=g||1;a=1<a?Math.ceil(a):1;return a*c};b?(m=!a.staggerLines&&!a.step&&(t(v)?[v]:g<y(a.autoRotationLimit,80)&&a.autoRotation))&&h(m,function(a){var b;
if(a===v||a&&-90<=a&&90>=a)d=r(Math.abs(p.h/Math.sin(u*a))),b=d+Math.abs(a/360),b<e&&(e=b,l=a,f=d)}):a.step||(f=r(p.h));this.autoRotation=m;this.labelRotation=y(l,v);return f},getSlotWidth:function(){var a=this.chart,b=this.horiz,c=this.options.labels,f=Math.max(this.tickPositions.length-(this.categories?0:1),1),g=a.margin[3];return b&&2>(c.step||0)&&!c.rotation&&(this.staggerLines||1)*this.len/f||!b&&(g&&g-a.spacing[3]||.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,b=a.renderer,c=
this.tickPositions,g=this.ticks,l=this.options.labels,v=this.horiz,p=this.getSlotWidth(),d=Math.max(1,Math.round(p-2*(l.padding||5))),e={},m=this.labelMetrics(),r=l.style&&l.style.textOverflow,n,D=0,y,E;G(l.rotation)||(e.rotation=l.rotation||0);h(c,function(a){(a=g[a])&&a.labelLength>D&&(D=a.labelLength)});this.maxLabelLength=D;if(this.autoRotation)D>d&&D>m.h?e.rotation=this.labelRotation:this.labelRotation=0;else if(p&&(n={width:d+"px"},!r))for(n.textOverflow="clip",y=c.length;!v&&y--;)if(E=c[y],
d=g[E].label)d.styles&&"ellipsis"===d.styles.textOverflow?d.css({textOverflow:"clip"}):g[E].labelLength>p&&d.css({width:p+"px"}),d.getBBox().height>this.len/c.length-(m.h-m.f)&&(d.specCss={textOverflow:"ellipsis"});e.rotation&&(n={width:(D>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight)+"px"},r||(n.textOverflow="ellipsis"));if(this.labelAlign=l.align||this.autoLabelAlign(this.labelRotation))e.align=this.labelAlign;h(c,function(a){var b=(a=g[a])&&a.label;b&&(b.attr(e),n&&b.css(f(n,b.specCss)),delete b.specCss,
a.rotation=e.rotation)});this.tickRotCorr=b.rotCorr(m.b,this.labelRotation||0,0!==this.side)},hasData:function(){return this.hasVisibleSeries||t(this.min)&&t(this.max)&&!!this.tickPositions},addTitle:function(a){var b=this.chart.renderer,c=this.horiz,k=this.opposite,f=this.options.title,g;this.axisTitle||((g=f.textAlign)||(g=(c?{low:"left",middle:"center",high:"right"}:{low:k?"right":"left",middle:"center",high:k?"left":"right"})[f.align]),this.axisTitle=b.text(f.text,0,0,f.useHTML).attr({zIndex:7,
rotation:f.rotation||0,align:g}).addClass("highcharts-axis-title").css(f.style).add(this.axisGroup),this.axisTitle.isNew=!0);f.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](!0)},generateTick:function(a){var b=this.ticks;b[a]?b[a].addLabel():b[a]=new F(this,a)},getOffset:function(){var a=this,b=a.chart,c=b.renderer,f=a.options,g=a.tickPositions,l=a.ticks,v=a.horiz,p=a.side,d=b.inverted&&!a.isZAxis?[1,0,3,2][p]:p,e,m,r=0,n,E=0,F=f.title,L=f.labels,
G=0,u=b.axisOffset,b=b.clipOffset,H=[-1,1,1,-1][p],q=f.className,J=a.axisParent,M=this.tickSize("tick");e=a.hasData();a.showAxis=m=e||y(f.showEmpty,!0);a.staggerLines=a.horiz&&L.staggerLines;a.axisGroup||(a.gridGroup=c.g("grid").attr({zIndex:f.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(q||"")).add(J),a.axisGroup=c.g("axis").attr({zIndex:f.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+" "+(q||"")).add(J),a.labelGroup=c.g("axis-labels").attr({zIndex:L.zIndex||
7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(q||"")).add(J));e||a.isLinked?(h(g,function(b,c){a.generateTick(b,c)}),a.renderUnsquish(),!1===L.reserveSpace||0!==p&&2!==p&&{1:"left",3:"right"}[p]!==a.labelAlign&&"center"!==a.labelAlign||h(g,function(a){G=Math.max(l[a].getLabelSize(),G)}),a.staggerLines&&(G*=a.staggerLines,a.labelOffset=G*(a.opposite?-1:1))):D(l,function(a,b){a.destroy();delete l[b]});F&&F.text&&!1!==F.enabled&&(a.addTitle(m),m&&!1!==F.reserveSpace&&(a.titleOffset=r=a.axisTitle.getBBox()[v?
"height":"width"],n=F.offset,E=t(n)?0:y(F.margin,v?5:10)));a.renderLine();a.offset=H*y(f.offset,u[p]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};c=0===p?-a.labelMetrics().h:2===p?a.tickRotCorr.y:0;E=Math.abs(G)+E;G&&(E=E-c+H*(v?y(L.y,a.tickRotCorr.y+8*H):L.x));a.axisTitleMargin=y(n,E);u[p]=Math.max(u[p],a.axisTitleMargin+r+H*a.offset,E,e&&g.length&&M?M[0]+H*a.offset:0);g=2*Math.floor(a.axisLine.strokeWidth()/2);0<f.offset&&(g-=2*f.offset);b[d]=Math.max(b[d]||g,g)},getLinePath:function(a){var b=this.chart,
c=this.opposite,f=this.offset,k=this.horiz,g=this.left+(c?this.width:0)+f,f=b.chartHeight-this.bottom-(c?this.height:0)+f;c&&(a*=-1);return b.renderer.crispLine(["M",k?this.left:g,k?f:this.top,"L",k?b.chartWidth-this.right:g,k?f:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,zIndex:7}))},getTitlePosition:function(){var a=
this.horiz,b=this.left,c=this.top,f=this.len,g=this.options.title,l=a?b:c,v=this.opposite,p=this.offset,h=g.x||0,d=g.y||0,e=this.axisTitle,m=this.chart.renderer.fontMetrics(g.style&&g.style.fontSize,e),e=Math.max(e.getBBox(null,0).height-m.h-1,0),f={low:l+(a?0:f),middle:l+f/2,high:l+(a?f:0)}[g.align],b=(a?c+this.height:b)+(a?1:-1)*(v?-1:1)*this.axisTitleMargin+[-e,e,m.f,-e][this.side];return{x:a?f+h:b+(v?this.width:0)+p+h,y:a?b+d-(v?this.height:0)+p:f+d}},renderMinorTick:function(a){var b=this.chart.hasRendered&&
E(this.oldMin),c=this.minorTicks;c[a]||(c[a]=new F(this,a,"minor"));b&&c[a].isNew&&c[a].render(null,!0);c[a].render(null,!1,1)},renderTick:function(a,b){var c=this.isLinked,f=this.ticks,k=this.chart.hasRendered&&E(this.oldMin);if(!c||a>=this.min&&a<=this.max)f[a]||(f[a]=new F(this,a)),k&&f[a].isNew&&f[a].render(b,!0,.1),f[a].render(b)},render:function(){var b=this,c=b.chart,f=b.options,g=b.isLog,l=b.lin2log,v=b.isLinked,p=b.tickPositions,d=b.axisTitle,e=b.ticks,m=b.minorTicks,r=b.alternateBands,n=
f.stackLabels,y=f.alternateGridColor,G=b.tickmarkOffset,u=b.axisLine,H=b.showAxis,t=B(c.renderer.globalAnimation),q,J;b.labelEdge.length=0;b.overlap=!1;h([e,m,r],function(a){D(a,function(a){a.isActive=!1})});if(b.hasData()||v)b.minorTickInterval&&!b.categories&&h(b.getMinorTickPositions(),function(a){b.renderMinorTick(a)}),p.length&&(h(p,function(a,c){b.renderTick(a,c)}),G&&(0===b.min||b.single)&&(e[-1]||(e[-1]=new F(b,-1,null,!0)),e[-1].render(-1))),y&&h(p,function(f,k){J=void 0!==p[k+1]?p[k+1]+
G:b.max-G;0===k%2&&f<b.max&&J<=b.max+(c.polar?-G:G)&&(r[f]||(r[f]=new a.PlotLineOrBand(b)),q=f+G,r[f].options={from:g?l(q):q,to:g?l(J):J,color:y},r[f].render(),r[f].isActive=!0)}),b._addedPlotLB||(h((f.plotLines||[]).concat(f.plotBands||[]),function(a){b.addPlotBandOrLine(a)}),b._addedPlotLB=!0);h([e,m,r],function(a){var b,f=[],k=t.duration;D(a,function(a,b){a.isActive||(a.render(b,!1,0),a.isActive=!1,f.push(b))});L(function(){for(b=f.length;b--;)a[f[b]]&&!a[f[b]].isActive&&(a[f[b]].destroy(),delete a[f[b]])},
a!==r&&c.hasRendered&&k?k:0)});u&&(u[u.isPlaced?"animate":"attr"]({d:this.getLinePath(u.strokeWidth())}),u.isPlaced=!0,u[H?"show":"hide"](!0));d&&H&&(f=b.getTitlePosition(),E(f.y)?(d[d.isNew?"attr":"animate"](f),d.isNew=!1):(d.attr("y",-9999),d.isNew=!0));n&&n.enabled&&b.renderStackTotals();b.isDirty=!1},redraw:function(){this.visible&&(this.render(),h(this.plotLinesAndBands,function(a){a.render()}));h(this.series,function(a){a.isDirty=!0})},keepProps:"extKey hcEvents names series userMax userMin".split(" "),
destroy:function(a){var b=this,c=b.stacks,f=b.plotLinesAndBands,k;a||M(b);D(c,function(a,b){n(a);c[b]=null});h([b.ticks,b.minorTicks,b.alternateBands],function(a){n(a)});if(f)for(a=f.length;a--;)f[a].destroy();h("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),function(a){b[a]&&(b[a]=b[a].destroy())});for(k in b.plotLinesAndBandsGroups)b.plotLinesAndBandsGroups[k]=b.plotLinesAndBandsGroups[k].destroy();D(b,function(a,c){-1===p(c,b.keepProps)&&delete b[c]})},drawCrosshair:function(a,
b){var c,f=this.crosshair,g=y(f.snap,!0),k,l=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(t(b)||!g)?(g?t(b)&&(k=this.isXAxis?b.plotX:this.len-b.plotY):k=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),t(k)&&(c=this.getPlotLinePath(b&&(this.isXAxis?b.x:y(b.stackY,b.y)),null,null,null,k)||null),t(c)?(b=this.categories&&!this.isRadial,l||(this.cross=l=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+(b?"category ":"thin ")+f.className).attr({zIndex:y(f.zIndex,
2)}).add(),l.attr({stroke:f.color||(b?e("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":y(f.width,1)}),f.dashStyle&&l.attr({dashstyle:f.dashStyle})),l.show().attr({d:c}),b&&!f.width&&l.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&this.cross.hide()}});return a.Axis=J}(I);(function(a){var z=a.Axis,B=a.getMagnitude,C=a.map,A=a.normalizeTickInterval,e=a.pick;z.prototype.getLogTickPositions=function(a,q,t,
u){var d=this.options,h=this.len,r=this.lin2log,m=this.log2lin,c=[];u||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),c=this.getLinearTickPositions(a,q,t);else if(.08<=a)for(var h=Math.floor(q),g,b,p,l,E,d=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];h<t+1&&!E;h++)for(b=d.length,g=0;g<b&&!E;g++)p=m(r(h)*d[g]),p>q&&(!u||l<=t)&&void 0!==l&&c.push(l),l>t&&(E=!0),l=p;else q=r(q),t=r(t),a=d[u?"minorTickInterval":"tickInterval"],a=e("auto"===a?null:a,this._minorAutoInterval,d.tickPixelInterval/
(u?5:1)*(t-q)/((u?h/this.tickPositions.length:h)||1)),a=A(a,null,B(a)),c=C(this.getLinearTickPositions(a,q,t),m),u||(this._minorAutoInterval=a/5);u||(this.tickInterval=a);return c};z.prototype.log2lin=function(a){return Math.log(a)/Math.LN10};z.prototype.lin2log=function(a){return Math.pow(10,a)}})(I);(function(a,z){var B=a.arrayMax,C=a.arrayMin,A=a.defined,e=a.destroyObjectProperties,d=a.each,q=a.erase,t=a.merge,u=a.pick;a.PlotLineOrBand=function(a,h){this.axis=a;h&&(this.options=h,this.id=h.id)};
a.PlotLineOrBand.prototype={render:function(){var d=this,h=d.axis,e=h.horiz,m=d.options,c=m.label,g=d.label,b=m.to,p=m.from,l=m.value,E=A(p)&&A(b),G=A(l),f=d.svgElem,H=!f,D=[],y=m.color,q=u(m.zIndex,0),v=m.events,D={"class":"highcharts-plot-"+(E?"band ":"line ")+(m.className||"")},L={},F=h.chart.renderer,J=E?"bands":"lines",k=h.log2lin;h.isLog&&(p=k(p),b=k(b),l=k(l));G?(D={stroke:y,"stroke-width":m.width},m.dashStyle&&(D.dashstyle=m.dashStyle)):E&&(y&&(D.fill=y),m.borderWidth&&(D.stroke=m.borderColor,
D["stroke-width"]=m.borderWidth));L.zIndex=q;J+="-"+q;(y=h.plotLinesAndBandsGroups[J])||(h.plotLinesAndBandsGroups[J]=y=F.g("plot-"+J).attr(L).add());H&&(d.svgElem=f=F.path().attr(D).add(y));if(G)D=h.getPlotLinePath(l,f.strokeWidth());else if(E)D=h.getPlotBandPath(p,b,m);else return;H&&D&&D.length?(f.attr({d:D}),v&&a.objectEach(v,function(a,b){f.on(b,function(a){v[b].apply(d,[a])})})):f&&(D?(f.show(),f.animate({d:D})):(f.hide(),g&&(d.label=g=g.destroy())));c&&A(c.text)&&D&&D.length&&0<h.width&&0<
h.height&&!D.flat?(c=t({align:e&&E&&"center",x:e?!E&&4:10,verticalAlign:!e&&E&&"middle",y:e?E?16:10:E?6:-4,rotation:e&&!E&&90},c),this.renderLabel(c,D,E,q)):g&&g.hide();return d},renderLabel:function(a,h,d,e){var c=this.label,g=this.axis.chart.renderer;c||(c={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(d?"band":"line")+"-label "+(a.className||"")},c.zIndex=e,this.label=c=g.text(a.text,0,0,a.useHTML).attr(c).add(),c.css(a.style));e=[h[1],h[4],d?h[6]:h[1]];h=[h[2],h[5],
d?h[7]:h[2]];d=C(e);g=C(h);c.align(a,!1,{x:d,y:g,width:B(e)-d,height:B(h)-g});c.show()},destroy:function(){q(this.axis.plotLinesAndBands,this);delete this.axis;e(this)}};a.extend(z.prototype,{getPlotBandPath:function(a,h){var d=this.getPlotLinePath(h,null,null,!0),e=this.getPlotLinePath(a,null,null,!0),c=this.horiz,g=1;a=a<this.min&&h<this.min||a>this.max&&h>this.max;e&&d?(a&&(e.flat=e.toString()===d.toString(),g=0),e.push(c&&d[4]===e[4]?d[4]+g:d[4],c||d[5]!==e[5]?d[5]:d[5]+g,c&&d[1]===e[1]?d[1]+
g:d[1],c||d[2]!==e[2]?d[2]:d[2]+g)):e=null;return e},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(d,h){var e=(new a.PlotLineOrBand(this,d)).render(),m=this.userOptions;e&&(h&&(m[h]=m[h]||[],m[h].push(d)),this.plotLinesAndBands.push(e));return e},removePlotBandOrLine:function(a){for(var h=this.plotLinesAndBands,e=this.options,m=this.userOptions,c=h.length;c--;)h[c].id===a&&h[c].destroy();
d([e.plotLines||[],m.plotLines||[],e.plotBands||[],m.plotBands||[]],function(g){for(c=g.length;c--;)g[c].id===a&&q(g,g[c])})},removePlotBand:function(a){this.removePlotBandOrLine(a)},removePlotLine:function(a){this.removePlotBandOrLine(a)}})})(I,S);(function(a){var z=a.dateFormat,B=a.each,C=a.extend,A=a.format,e=a.isNumber,d=a.map,q=a.merge,t=a.pick,u=a.splat,n=a.syncTimeout,h=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,d){this.chart=a;this.options=
d;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=d.split&&!a.inverted;this.shared=d.shared||this.split},cleanSplit:function(a){B(this.chart.series,function(d){var c=d&&d.tt;c&&(!c.isActive||a?d.tt=c.destroy():c.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,d=this.options;this.label||(this.split?this.label=a.g("tooltip"):(this.label=a.label("",0,0,d.shape||"callout",null,null,d.useHTML,null,"tooltip").attr({padding:d.padding,r:d.borderRadius}),this.label.attr({fill:d.backgroundColor,
"stroke-width":d.borderWidth}).css(d.style).shadow(d.shadow)),this.label.attr({zIndex:8}).add());return this.label},update:function(a){this.destroy();q(!0,this.chart.options.tooltip.userOptions,a);this.init(this.chart,q(!0,this.options,a))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,d,c,g){var b=this,p=b.now,l=!1!==b.options.animation&&
!b.isHidden&&(1<Math.abs(a-p.x)||1<Math.abs(d-p.y)),h=b.followPointer||1<b.len;C(p,{x:l?(2*p.x+a)/3:a,y:l?(p.y+d)/2:d,anchorX:h?void 0:l?(2*p.anchorX+c)/3:c,anchorY:h?void 0:l?(p.anchorY+g)/2:g});b.getLabel().attr(p);l&&(clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){b&&b.move(a,d,c,g)},32))},hide:function(a){var d=this;clearTimeout(this.hideTimer);a=t(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=n(function(){d.getLabel()[a?"fadeOut":"hide"]();d.isHidden=
!0},a))},getAnchor:function(a,h){var c,g=this.chart,b=g.inverted,p=g.plotTop,l=g.plotLeft,e=0,m=0,f,r;a=u(a);c=a[0].tooltipPos;this.followPointer&&h&&(void 0===h.chartX&&(h=g.pointer.normalize(h)),c=[h.chartX-g.plotLeft,h.chartY-p]);c||(B(a,function(a){f=a.series.yAxis;r=a.series.xAxis;e+=a.plotX+(!b&&r?r.left-l:0);m+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!b&&f?f.top-p:0)}),e/=a.length,m/=a.length,c=[b?g.plotWidth-m:e,this.shared&&!b&&1<a.length&&h?h.chartY-p:b?g.plotHeight-e:m]);return d(c,
Math.round)},getPosition:function(a,d,c){var g=this.chart,b=this.distance,p={},l=c.h||0,h,e=["y",g.chartHeight,d,c.plotY+g.plotTop,g.plotTop,g.plotTop+g.plotHeight],f=["x",g.chartWidth,a,c.plotX+g.plotLeft,g.plotLeft,g.plotLeft+g.plotWidth],m=!this.followPointer&&t(c.ttBelow,!g.inverted===!!c.negative),D=function(a,c,f,g,v,d){var k=f<g-b,h=g+b+f<c,e=g-b-f;g+=b;if(m&&h)p[a]=g;else if(!m&&k)p[a]=e;else if(k)p[a]=Math.min(d-f,0>e-l?e:e-l);else if(h)p[a]=Math.max(v,g+l+f>c?g:g+l);else return!1},y=function(a,
c,f,g){var k;g<b||g>c-b?k=!1:p[a]=g<f/2?1:g>c-f/2?c-f-2:g-f/2;return k},r=function(a){var b=e;e=f;f=b;h=a},v=function(){!1!==D.apply(0,e)?!1!==y.apply(0,f)||h||(r(!0),v()):h?p.x=p.y=0:(r(!0),v())};(g.inverted||1<this.len)&&r();v();return p},defaultFormatter:function(a){var d=this.points||u(this),c;c=[a.tooltipFooterHeaderFormatter(d[0])];c=c.concat(a.bodyFormatter(d));c.push(a.tooltipFooterHeaderFormatter(d[0],!0));return c},refresh:function(a,d){var c,g=this.options,b,h=a,l,e={},m=[];c=g.formatter||
this.defaultFormatter;var e=this.shared,f;g.enabled&&(clearTimeout(this.hideTimer),this.followPointer=u(h)[0].series.tooltipOptions.followPointer,l=this.getAnchor(h,d),d=l[0],b=l[1],!e||h.series&&h.series.noSharedTooltip?e=h.getLabelConfig():(B(h,function(a){a.setState("hover");m.push(a.getLabelConfig())}),e={x:h[0].category,y:h[0].y},e.points=m,h=h[0]),this.len=m.length,e=c.call(e,this),f=h.series,this.distance=t(f.tooltipOptions.distance,16),!1===e?this.hide():(c=this.getLabel(),this.isHidden&&
c.attr({opacity:1}).show(),this.split?this.renderSplit(e,a):(g.style.width||c.css({width:this.chart.spacingBox.width}),c.attr({text:e&&e.join?e.join(""):e}),c.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+t(h.colorIndex,f.colorIndex)),c.attr({stroke:g.borderColor||h.color||f.color||"#666666"}),this.updatePosition({plotX:d,plotY:b,negative:h.negative,ttBelow:h.ttBelow,h:l[2]||0})),this.isHidden=!1))},renderSplit:function(d,h){var c=this,g=[],b=this.chart,e=b.renderer,l=!0,m=this.options,
r=0,f=this.getLabel();B(d.slice(0,h.length+1),function(a,d){if(!1!==a){d=h[d-1]||{isHeader:!0,plotX:h[0].plotX};var p=d.series||c,D=p.tt,v=d.series||{},n="highcharts-color-"+t(d.colorIndex,v.colorIndex,"none");D||(p.tt=D=e.label(null,null,null,"callout").addClass("highcharts-tooltip-box "+n).attr({padding:m.padding,r:m.borderRadius,fill:m.backgroundColor,stroke:m.borderColor||d.color||v.color||"#333333","stroke-width":m.borderWidth}).add(f));D.isActive=!0;D.attr({text:a});D.css(m.style).shadow(m.shadow);
a=D.getBBox();v=a.width+D.strokeWidth();d.isHeader?(r=a.height,v=Math.max(0,Math.min(d.plotX+b.plotLeft-v/2,b.chartWidth-v))):v=d.plotX+b.plotLeft-t(m.distance,16)-v;0>v&&(l=!1);a=(d.series&&d.series.yAxis&&d.series.yAxis.pos)+(d.plotY||0);a-=b.plotTop;g.push({target:d.isHeader?b.plotHeight+r:a,rank:d.isHeader?1:0,size:p.tt.getBBox().height+1,point:d,x:v,tt:D})}});this.cleanSplit();a.distribute(g,b.plotHeight+r);B(g,function(a){var c=a.point,f=c.series;a.tt.attr({visibility:void 0===a.pos?"hidden":
"inherit",x:l||c.isHeader?a.x:c.plotX+b.plotLeft+t(m.distance,16),y:a.pos+b.plotTop,anchorX:c.isHeader?c.plotX+b.plotLeft:c.plotX+f.xAxis.pos,anchorY:c.isHeader?a.pos+b.plotTop-15:c.plotY+f.yAxis.pos})})},updatePosition:function(a){var d=this.chart,c=this.getLabel(),c=(this.options.positioner||this.getPosition).call(this,c.width,c.height,a);this.move(Math.round(c.x),Math.round(c.y||0),a.plotX+d.plotLeft,a.plotY+d.plotTop)},getDateFormat:function(a,d,c,g){var b=z("%m-%d %H:%M:%S.%L",d),e,l,m={millisecond:15,
second:12,minute:9,hour:6,day:3},n="millisecond";for(l in h){if(a===h.week&&+z("%w",d)===c&&"00:00:00.000"===b.substr(6)){l="week";break}if(h[l]>a){l=n;break}if(m[l]&&b.substr(m[l])!=="01-01 00:00:00.000".substr(m[l]))break;"week"!==l&&(n=l)}l&&(e=g[l]);return e},getXDateFormat:function(a,d,c){d=d.dateTimeLabelFormats;var g=c&&c.closestPointRange;return(g?this.getDateFormat(g,a.x,c.options.startOfWeek,d):d.day)||d.year},tooltipFooterHeaderFormatter:function(a,d){var c=d?"footer":"header";d=a.series;
var g=d.tooltipOptions,b=g.xDateFormat,h=d.xAxis,l=h&&"datetime"===h.options.type&&e(a.key),c=g[c+"Format"];l&&!b&&(b=this.getXDateFormat(a,g,h));l&&b&&(c=c.replace("{point.key}","{point.key:"+b+"}"));return A(c,{point:a,series:d})},bodyFormatter:function(a){return d(a,function(a){var c=a.series.tooltipOptions;return(c.pointFormatter||a.point.tooltipFormatter).call(a.point,c.pointFormat)})}}})(I);(function(a){var z=a.addEvent,B=a.attr,C=a.charts,A=a.color,e=a.css,d=a.defined,q=a.each,t=a.extend,u=
a.find,n=a.fireEvent,h=a.isObject,r=a.offset,m=a.pick,c=a.removeEvent,g=a.splat,b=a.Tooltip,p=a.win;a.Pointer=function(a,b){this.init(a,b)};a.Pointer.prototype={init:function(a,c){this.options=c;this.chart=a;this.runChartClick=c.chart.events&&!!c.chart.events.click;this.pinchDown=[];this.lastValidTouch={};b&&(a.tooltip=new b(a,c.tooltip),this.followTouchMove=m(c.tooltip.followTouchMove,!0));this.setDOMEvents()},zoomOption:function(a){var b=this.chart,c=b.options.chart,f=c.zoomType||"",b=b.inverted;
/touch/.test(a.type)&&(f=m(c.pinchType,f));this.zoomX=a=/x/.test(f);this.zoomY=f=/y/.test(f);this.zoomHor=a&&!b||f&&b;this.zoomVert=f&&!b||a&&b;this.hasZoom=a||f},normalize:function(a,b){var c,f;a=a||p.event;a.target||(a.target=a.srcElement);f=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;b||(this.chartPosition=b=r(this.chart.container));void 0===f.pageX?(c=Math.max(a.x,a.clientX-b.left),b=a.y):(c=f.pageX-b.left,b=f.pageY-b.top);return t(a,{chartX:Math.round(c),chartY:Math.round(b)})},
getCoordinates:function(a){var b={xAxis:[],yAxis:[]};q(this.chart.axes,function(c){b[c.isXAxis?"xAxis":"yAxis"].push({axis:c,value:c.toValue(a[c.horiz?"chartX":"chartY"])})});return b},findNearestKDPoint:function(a,b,c){var f;q(a,function(a){var g=!(a.noSharedTooltip&&b)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(c,g);if((g=h(a,!0))&&!(g=!h(f,!0)))var g=f.distX-a.distX,l=f.dist-a.dist,d=(a.series.group&&a.series.group.zIndex)-(f.series.group&&f.series.group.zIndex),g=0<(0!==g&&b?
g:0!==l?l:0!==d?d:f.series.index>a.series.index?-1:1);g&&(f=a)});return f},getPointFromEvent:function(a){a=a.target;for(var b;a&&!b;)b=a.point,a=a.parentNode;return b},getChartCoordinatesFromPoint:function(a,b){var c=a.series,f=c.xAxis,c=c.yAxis;if(f&&c)return b?{chartX:f.len+f.pos-a.clientX,chartY:c.len+c.pos-a.plotY}:{chartX:a.clientX+f.pos,chartY:a.plotY+c.pos}},getHoverData:function(b,c,g,f,d,e){var l,p=[];f=!(!f||!b);var v=c&&!c.stickyTracking?[c]:a.grep(g,function(a){return a.visible&&!(!d&&
a.directTouch)&&m(a.options.enableMouseTracking,!0)&&a.stickyTracking});c=(l=f?b:this.findNearestKDPoint(v,d,e))&&l.series;l&&(d&&!c.noSharedTooltip?(v=a.grep(g,function(a){return a.visible&&!(!d&&a.directTouch)&&m(a.options.enableMouseTracking,!0)&&!a.noSharedTooltip}),q(v,function(a){a=u(a.points,function(a){return a.x===l.x});h(a)&&!a.isNull&&p.push(a)})):p.push(l));return{hoverPoint:l,hoverSeries:c,hoverPoints:p}},runPointActions:function(b,c){var g=this.chart,f=g.tooltip,l=f?f.shared:!1,d=c||
g.hoverPoint,h=d&&d.series||g.hoverSeries,h=this.getHoverData(d,h,g.series,!!c||h&&h.directTouch&&this.isDirectTouch,l,b),e,d=h.hoverPoint;e=h.hoverPoints;c=(h=h.hoverSeries)&&h.tooltipOptions.followPointer;l=l&&h&&!h.noSharedTooltip;if(d&&(d!==g.hoverPoint||f&&f.isHidden)){q(g.hoverPoints||[],function(b){-1===a.inArray(b,e)&&b.setState()});q(e||[],function(a){a.setState("hover")});if(g.hoverSeries!==h)h.onMouseOver();g.hoverPoint&&g.hoverPoint.firePointEvent("mouseOut");d.firePointEvent("mouseOver");
g.hoverPoints=e;g.hoverPoint=d;f&&f.refresh(l?e:d,b)}else c&&f&&!f.isHidden&&(d=f.getAnchor([{}],b),f.updatePosition({plotX:d[0],plotY:d[1]}));this.unDocMouseMove||(this.unDocMouseMove=z(g.container.ownerDocument,"mousemove",function(b){var c=C[a.hoverChartIndex];if(c)c.pointer.onDocumentMouseMove(b)}));q(g.axes,function(c){var f=m(c.crosshair.snap,!0),g=f?a.find(e,function(a){return a.series[c.coll]===c}):void 0;g||!f?c.drawCrosshair(b,g):c.hideCrosshair()})},reset:function(a,b){var c=this.chart,
f=c.hoverSeries,l=c.hoverPoint,d=c.hoverPoints,h=c.tooltip,e=h&&h.shared?d:l;a&&e&&q(g(e),function(b){b.series.isCartesian&&void 0===b.plotX&&(a=!1)});if(a)h&&e&&(h.refresh(e),l&&(l.setState(l.state,!0),q(c.axes,function(a){a.crosshair&&a.drawCrosshair(null,l)})));else{if(l)l.onMouseOut();d&&q(d,function(a){a.setState()});if(f)f.onMouseOut();h&&h.hide(b);this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());q(c.axes,function(a){a.hideCrosshair()});this.hoverX=c.hoverPoints=c.hoverPoint=
null}},scaleGroups:function(a,b){var c=this.chart,f;q(c.series,function(g){f=a||g.getPlotBox();g.xAxis&&g.xAxis.zoomEnabled&&g.group&&(g.group.attr(f),g.markerGroup&&(g.markerGroup.attr(f),g.markerGroup.clip(b?c.clipRect:null)),g.dataLabelsGroup&&g.dataLabelsGroup.attr(f))});c.clipRect.attr(b||c.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=this.chart,c=b.options.chart,
f=a.chartX,g=a.chartY,l=this.zoomHor,d=this.zoomVert,h=b.plotLeft,v=b.plotTop,e=b.plotWidth,p=b.plotHeight,m,k=this.selectionMarker,w=this.mouseDownX,n=this.mouseDownY,r=c.panKey&&a[c.panKey+"Key"];k&&k.touch||(f<h?f=h:f>h+e&&(f=h+e),g<v?g=v:g>v+p&&(g=v+p),this.hasDragged=Math.sqrt(Math.pow(w-f,2)+Math.pow(n-g,2)),10<this.hasDragged&&(m=b.isInsidePlot(w-h,n-v),b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&m&&!r&&!k&&(this.selectionMarker=k=b.renderer.rect(h,v,l?1:e,d?1:p,0).attr({fill:c.selectionMarkerFill||
A("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",zIndex:7}).add()),k&&l&&(f-=w,k.attr({width:Math.abs(f),x:(0<f?0:f)+w})),k&&d&&(f=g-n,k.attr({height:Math.abs(f),y:(0<f?0:f)+n})),m&&!k&&c.panning&&b.pan(a,c.panning)))},drop:function(a){var b=this,c=this.chart,f=this.hasPinched;if(this.selectionMarker){var g={originalEvent:a,xAxis:[],yAxis:[]},l=this.selectionMarker,h=l.attr?l.attr("x"):l.x,p=l.attr?l.attr("y"):l.y,v=l.attr?l.attr("width"):l.width,m=l.attr?l.attr("height"):
l.height,F;if(this.hasDragged||f)q(c.axes,function(c){if(c.zoomEnabled&&d(c.min)&&(f||b[{xAxis:"zoomX",yAxis:"zoomY"}[c.coll]])){var k=c.horiz,l="touchend"===a.type?c.minPixelPadding:0,e=c.toValue((k?h:p)+l),k=c.toValue((k?h+v:p+m)-l);g[c.coll].push({axis:c,min:Math.min(e,k),max:Math.max(e,k)});F=!0}}),F&&n(c,"selection",g,function(a){c.zoom(t(a,f?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();f&&this.scaleGroups()}c&&(e(c.container,{cursor:c._cursor}),c.cancelClick=10<
this.hasDragged,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){a=this.normalize(a);this.zoomOption(a);a.preventDefault&&a.preventDefault();this.dragStart(a)},onDocumentMouseUp:function(b){C[a.hoverChartIndex]&&C[a.hoverChartIndex].pointer.drop(b)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition;a=this.normalize(a,c);!c||this.inClass(a.target,"highcharts-tracker")||b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},
onContainerMouseLeave:function(b){var c=C[a.hoverChartIndex];c&&(b.relatedTarget||b.toElement)&&(c.pointer.reset(),c.pointer.chartPosition=null)},onContainerMouseMove:function(b){var c=this.chart;d(a.hoverChartIndex)&&C[a.hoverChartIndex]&&C[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=c.index);b=this.normalize(b);b.returnValue=!1;"mousedown"===c.mouseIsDown&&this.drag(b);!this.inClass(b.target,"highcharts-tracker")&&!c.isInsidePlot(b.chartX-c.plotLeft,b.chartY-c.plotTop)||c.openMenu||this.runPointActions(b)},
inClass:function(a,b){for(var c;a;){if(c=B(a,"class")){if(-1!==c.indexOf(b))return!0;if(-1!==c.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;this.isDirectTouch=!1;if(!(!b||!a||b.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,f=b.plotLeft,
g=b.plotTop;a=this.normalize(a);b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(n(c.series,"click",t(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",a)):(t(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-f,a.chartY-g)&&n(b,"click",a)))},setDOMEvents:function(){var b=this,c=b.chart.container,g=c.ownerDocument;c.onmousedown=function(a){b.onContainerMouseDown(a)};c.onmousemove=function(a){b.onContainerMouseMove(a)};c.onclick=function(a){b.onContainerClick(a)};z(c,"mouseleave",
b.onContainerMouseLeave);1===a.chartCount&&z(g,"mouseup",b.onDocumentMouseUp);a.hasTouch&&(c.ontouchstart=function(a){b.onContainerTouchStart(a)},c.ontouchmove=function(a){b.onContainerTouchMove(a)},1===a.chartCount&&z(g,"touchend",b.onDocumentTouchEnd))},destroy:function(){var b=this,g=this.chart.container.ownerDocument;b.unDocMouseMove&&b.unDocMouseMove();c(b.chart.container,"mouseleave",b.onContainerMouseLeave);a.chartCount||(c(g,"mouseup",b.onDocumentMouseUp),a.hasTouch&&c(g,"touchend",b.onDocumentTouchEnd));
clearInterval(b.tooltipTimeout);a.objectEach(b,function(a,c){b[c]=null})}}})(I);(function(a){var z=a.charts,B=a.each,C=a.extend,A=a.map,e=a.noop,d=a.pick;C(a.Pointer.prototype,{pinchTranslate:function(a,d,e,n,h,r){this.zoomHor&&this.pinchTranslateDirection(!0,a,d,e,n,h,r);this.zoomVert&&this.pinchTranslateDirection(!1,a,d,e,n,h,r)},pinchTranslateDirection:function(a,d,e,n,h,r,m,c){var g=this.chart,b=a?"x":"y",p=a?"X":"Y",l="chart"+p,u=a?"width":"height",t=g["plot"+(a?"Left":"Top")],f,q,D=c||1,y=g.inverted,
M=g.bounds[a?"h":"v"],v=1===d.length,L=d[0][l],F=e[0][l],J=!v&&d[1][l],k=!v&&e[1][l],w;e=function(){!v&&20<Math.abs(L-J)&&(D=c||Math.abs(F-k)/Math.abs(L-J));q=(t-F)/D+L;f=g["plot"+(a?"Width":"Height")]/D};e();d=q;d<M.min?(d=M.min,w=!0):d+f>M.max&&(d=M.max-f,w=!0);w?(F-=.8*(F-m[b][0]),v||(k-=.8*(k-m[b][1])),e()):m[b]=[F,k];y||(r[b]=q-t,r[u]=f);r=y?1/D:D;h[u]=f;h[b]=d;n[y?a?"scaleY":"scaleX":"scale"+p]=D;n["translate"+p]=r*t+(F-r*L)},pinch:function(a){var t=this,u=t.chart,n=t.pinchDown,h=a.touches,
r=h.length,m=t.lastValidTouch,c=t.hasZoom,g=t.selectionMarker,b={},p=1===r&&(t.inClass(a.target,"highcharts-tracker")&&u.runTrackerClick||t.runChartClick),l={};1<r&&(t.initiated=!0);c&&t.initiated&&!p&&a.preventDefault();A(h,function(a){return t.normalize(a)});"touchstart"===a.type?(B(h,function(a,b){n[b]={chartX:a.chartX,chartY:a.chartY}}),m.x=[n[0].chartX,n[1]&&n[1].chartX],m.y=[n[0].chartY,n[1]&&n[1].chartY],B(u.axes,function(a){if(a.zoomEnabled){var b=u.bounds[a.horiz?"h":"v"],c=a.minPixelPadding,
g=a.toPixels(d(a.options.min,a.dataMin)),l=a.toPixels(d(a.options.max,a.dataMax)),h=Math.max(g,l);b.min=Math.min(a.pos,Math.min(g,l)-c);b.max=Math.max(a.pos+a.len,h+c)}}),t.res=!0):t.followTouchMove&&1===r?this.runPointActions(t.normalize(a)):n.length&&(g||(t.selectionMarker=g=C({destroy:e,touch:!0},u.plotBox)),t.pinchTranslate(n,h,b,g,l,m),t.hasPinched=c,t.scaleGroups(b,l),t.res&&(t.res=!1,this.reset(!1,0)))},touch:function(e,t){var u=this.chart,n,h;if(u.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});
a.hoverChartIndex=u.index;1===e.touches.length?(e=this.normalize(e),(h=u.isInsidePlot(e.chartX-u.plotLeft,e.chartY-u.plotTop))&&!u.openMenu?(t&&this.runPointActions(e),"touchmove"===e.type&&(t=this.pinchDown,n=t[0]?4<=Math.sqrt(Math.pow(t[0].chartX-e.chartX,2)+Math.pow(t[0].chartY-e.chartY,2)):!1),d(n,!0)&&this.pinch(e)):t&&this.reset()):2===e.touches.length&&this.pinch(e)},onContainerTouchStart:function(a){this.zoomOption(a);this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(d){z[a.hoverChartIndex]&&
z[a.hoverChartIndex].pointer.drop(d)}})})(I);(function(a){var z=a.addEvent,B=a.charts,C=a.css,A=a.doc,e=a.extend,d=a.noop,q=a.Pointer,t=a.removeEvent,u=a.win,n=a.wrap;if(!a.hasTouch&&(u.PointerEvent||u.MSPointerEvent)){var h={},r=!!u.PointerEvent,m=function(){var c=[];c.item=function(a){return this[a]};a.objectEach(h,function(a){c.push({pageX:a.pageX,pageY:a.pageY,target:a.target})});return c},c=function(c,b,h,l){"touch"!==c.pointerType&&c.pointerType!==c.MSPOINTER_TYPE_TOUCH||!B[a.hoverChartIndex]||
(l(c),l=B[a.hoverChartIndex].pointer,l[b]({type:h,target:c.currentTarget,preventDefault:d,touches:m()}))};e(q.prototype,{onContainerPointerDown:function(a){c(a,"onContainerTouchStart","touchstart",function(a){h[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){c(a,"onContainerTouchMove","touchmove",function(a){h[a.pointerId]={pageX:a.pageX,pageY:a.pageY};h[a.pointerId].target||(h[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){c(a,
"onDocumentTouchEnd","touchend",function(a){delete h[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,r?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,r?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(A,r?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});n(q.prototype,"init",function(a,b,c){a.call(this,b,c);this.hasZoom&&C(b.container,{"-ms-touch-action":"none","touch-action":"none"})});n(q.prototype,"setDOMEvents",function(a){a.apply(this);
(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(z)});n(q.prototype,"destroy",function(a){this.batchMSEvents(t);a.call(this)})}})(I);(function(a){var z=a.addEvent,B=a.css,C=a.discardElement,A=a.defined,e=a.each,d=a.isFirefox,q=a.marginNames,t=a.merge,u=a.pick,n=a.setAnimation,h=a.stableSort,r=a.win,m=a.wrap;a.Legend=function(a,g){this.init(a,g)};a.Legend.prototype={init:function(a,g){this.chart=a;this.setOptions(g);g.enabled&&(this.render(),z(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},
setOptions:function(a){var c=u(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=t(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.padding=c;this.initialItemY=c-5;this.itemHeight=this.maxItemWidth=0;this.symbolWidth=u(a.symbolWidth,16);this.pages=[]},update:function(a,g){var b=this.chart;this.setOptions(t(!0,this.options,a));this.destroy();b.isDirtyLegend=b.isDirtyBox=!0;u(g,!0)&&b.redraw()},colorizeItem:function(a,g){a.legendGroup[g?"removeClass":
"addClass"]("highcharts-legend-item-hidden");var b=this.options,c=a.legendItem,d=a.legendLine,h=a.legendSymbol,e=this.itemHiddenStyle.color,b=g?b.itemStyle.color:e,f=g?a.color||e:e,m=a.options&&a.options.marker,n={fill:f};c&&c.css({fill:b,color:b});d&&d.attr({stroke:f});h&&(m&&h.isMarker&&(n=a.pointAttribs(),g||(n.stroke=n.fill=e)),h.attr(n))},positionItem:function(a){var c=this.options,b=c.symbolPadding,c=!c.rtl,d=a._legendItemPos,l=d[0],d=d[1],h=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(c?
l:this.legendWidth-l-2*b-4,d);h&&(h.x=l,h.y=d)},destroyItem:function(a){var c=a.checkbox;e(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});c&&C(a.checkbox)},destroy:function(){function a(a){this[a]&&(this[a]=this[a].destroy())}e(this.getAllItems(),function(c){e(["legendItem","legendGroup"],a,c)});e("clipRect up down pager nav box title group".split(" "),a,this);this.display=null},positionCheckboxes:function(a){var c=this.group&&this.group.alignAttr,
b,d=this.clipHeight||this.legendHeight,h=this.titleHeight;c&&(b=c.translateY,e(this.allItems,function(g){var l=g.checkbox,f;l&&(f=b+h+l.y+(a||0)+3,B(l,{left:c.translateX+g.checkboxOffset+l.x-20+"px",top:f+"px",display:f>b-6&&f<b+d-6?"":"none"}))}))},renderTitle:function(){var a=this.options,g=this.padding,b=a.title,d=0;b.text&&(this.title||(this.title=this.chart.renderer.label(b.text,g-3,g-4,null,null,null,a.useHTML,null,"legend-title").attr({zIndex:1}).css(b.style).add(this.group)),a=this.title.getBBox(),
d=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:d}));this.titleHeight=d},setText:function(c){var g=this.options;c.legendItem.attr({text:g.labelFormat?a.format(g.labelFormat,c):g.labelFormatter.call(c)})},renderItem:function(a){var c=this.chart,b=c.renderer,d=this.options,l="horizontal"===d.layout,h=this.symbolWidth,e=d.symbolPadding,f=this.itemStyle,m=this.itemHiddenStyle,n=this.padding,r=l?u(d.itemDistance,20):0,q=!d.rtl,v=d.width,L=d.itemMarginBottom||0,F=this.itemMarginTop,
J=a.legendItem,k=!a.series,w=!k&&a.series.drawLegendSymbol?a.series:a,N=w.options,K=this.createCheckboxForItem&&N&&N.showCheckbox,N=h+e+r+(K?20:0),A=d.useHTML,z=a.options.className;J||(a.legendGroup=b.g("legend-item").addClass("highcharts-"+w.type+"-series highcharts-color-"+a.colorIndex+(z?" "+z:"")+(k?" highcharts-series-"+a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=J=b.text("",q?h+e:-e,this.baseline||0,A).css(t(a.visible?f:m)).attr({align:q?"left":"right",zIndex:2}).add(a.legendGroup),
this.baseline||(h=f.fontSize,this.fontMetrics=b.fontMetrics(h,J),this.baseline=this.fontMetrics.f+3+F,J.attr("y",this.baseline)),this.symbolHeight=d.symbolHeight||this.fontMetrics.f,w.drawLegendSymbol(this,a),this.setItemEvents&&this.setItemEvents(a,J,A),K&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);f.width||J.css({width:(d.itemWidth||d.width||c.spacingBox.width)-N});this.setText(a);b=J.getBBox();f=a.checkboxOffset=d.itemWidth||a.legendItemWidth||b.width+N;this.itemHeight=b=Math.round(a.legendItemHeight||
b.height||this.symbolHeight);l&&this.itemX-n+f>(v||c.spacingBox.width-2*n-d.x)&&(this.itemX=n,this.itemY+=F+this.lastLineHeight+L,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,f);this.lastItemY=F+this.itemY+L;this.lastLineHeight=Math.max(b,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];l?this.itemX+=f:(this.itemY+=F+b+L,this.lastLineHeight=b);this.offsetWidth=v||Math.max((l?this.itemX-n-(a.checkbox?0:r):f)+n,this.offsetWidth)},getAllItems:function(){var a=[];e(this.chart.series,
function(c){var b=c&&c.options;c&&u(b.showInLegend,A(b.linkedTo)?!1:void 0,!0)&&(a=a.concat(c.legendItems||("point"===b.legendType?c.data:c)))});return a},adjustMargins:function(a,g){var b=this.chart,c=this.options,d=c.align.charAt(0)+c.verticalAlign.charAt(0)+c.layout.charAt(0);c.floating||e([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(h,l){h.test(d)&&!A(a[l])&&(b[q[l]]=Math.max(b[q[l]],b.legend[(l+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][l]*c[l%2?"x":"y"]+u(c.margin,
12)+g[l]))})},render:function(){var a=this,g=a.chart,b=g.renderer,d=a.group,l,m,n,f,r=a.box,u=a.options,y=a.padding;a.itemX=y;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;d||(a.group=d=b.g("legend").attr({zIndex:7}).add(),a.contentGroup=b.g().attr({zIndex:1}).add(d),a.scrollGroup=b.g().add(a.contentGroup));a.renderTitle();l=a.getAllItems();h(l,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});u.reversed&&l.reverse();a.allItems=l;a.display=m=
!!l.length;a.lastLineHeight=0;e(l,function(b){a.renderItem(b)});n=(u.width||a.offsetWidth)+y;f=a.lastItemY+a.lastLineHeight+a.titleHeight;f=a.handleOverflow(f);f+=y;r||(a.box=r=b.rect().addClass("highcharts-legend-box").attr({r:u.borderRadius}).add(d),r.isNew=!0);r.attr({stroke:u.borderColor,"stroke-width":u.borderWidth||0,fill:u.backgroundColor||"none"}).shadow(u.shadow);0<n&&0<f&&(r[r.isNew?"attr":"animate"](r.crisp({x:0,y:0,width:n,height:f},r.strokeWidth())),r.isNew=!1);r[m?"show":"hide"]();a.legendWidth=
n;a.legendHeight=f;e(l,function(b){a.positionItem(b)});m&&d.align(t(u,{width:n,height:f}),!0,"spacingBox");g.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var c=this,b=this.chart,d=b.renderer,h=this.options,m=h.y,n=this.padding,b=b.spacingBox.height+("top"===h.verticalAlign?-m:m)-n,m=h.maxHeight,f,r=this.clipRect,t=h.navigation,y=u(t.animation,!0),q=t.arrowSize||12,v=this.nav,L=this.pages,F,J=this.allItems,k=function(a){"number"===typeof a?r.attr({height:a}):r&&(c.clipRect=r.destroy(),
c.contentGroup.clip());c.contentGroup.div&&(c.contentGroup.div.style.clip=a?"rect("+n+"px,9999px,"+(n+a)+"px,0)":"auto")};"horizontal"!==h.layout||"middle"===h.verticalAlign||h.floating||(b/=2);m&&(b=Math.min(b,m));L.length=0;a>b&&!1!==t.enabled?(this.clipHeight=f=Math.max(b-20-this.titleHeight-n,0),this.currentPage=u(this.currentPage,1),this.fullHeight=a,e(J,function(a,b){var c=a._legendItemPos[1];a=Math.round(a.legendItem.getBBox().height);var g=L.length;if(!g||c-L[g-1]>f&&(F||c)!==L[g-1])L.push(F||
c),g++;b===J.length-1&&c+a-L[g-1]>f&&L.push(c);c!==F&&(F=c)}),r||(r=c.clipRect=d.clipRect(0,n,9999,0),c.contentGroup.clip(r)),k(f),v||(this.nav=v=d.g().attr({zIndex:1}).add(this.group),this.up=d.symbol("triangle",0,0,q,q).on("click",function(){c.scroll(-1,y)}).add(v),this.pager=d.text("",15,10).addClass("highcharts-legend-navigation").css(t.style).add(v),this.down=d.symbol("triangle-down",0,0,q,q).on("click",function(){c.scroll(1,y)}).add(v)),c.scroll(0),a=b):v&&(k(),this.nav=v.destroy(),this.scrollGroup.attr({translateY:1}),
this.clipHeight=0);return a},scroll:function(a,g){var b=this.pages,c=b.length;a=this.currentPage+a;var d=this.clipHeight,h=this.options.navigation,e=this.pager,f=this.padding;a>c&&(a=c);0<a&&(void 0!==g&&n(g,this.chart),this.nav.attr({translateX:f,translateY:d+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),e.attr({text:a+"/"+c}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===c?"highcharts-legend-nav-inactive":
"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?h.inactiveColor:h.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===c?h.inactiveColor:h.activeColor}).css({cursor:a===c?"default":"pointer"}),g=-b[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:g}),this.currentPage=a,this.positionCheckboxes(g))}};a.LegendSymbolMixin={drawRectangle:function(a,g){var b=a.symbolHeight,c=a.options.squareSymbol;g.legendSymbol=this.chart.renderer.rect(c?(a.symbolWidth-b)/
2:0,a.baseline-b+1,c?b:a.symbolWidth,b,u(a.options.symbolRadius,b/2)).addClass("highcharts-point").attr({zIndex:3}).add(g.legendGroup)},drawLineMarker:function(a){var c=this.options,b=c.marker,d=a.symbolWidth,h=a.symbolHeight,e=h/2,m=this.chart.renderer,f=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var n;n={"stroke-width":c.lineWidth||0};c.dashStyle&&(n.dashstyle=c.dashStyle);this.legendLine=m.path(["M",0,a,"L",d,a]).addClass("highcharts-graph").attr(n).add(f);b&&!1!==b.enabled&&
(c=Math.min(u(b.radius,e),e),0===this.symbol.indexOf("url")&&(b=t(b,{width:h,height:h}),c=0),this.legendSymbol=b=m.symbol(this.symbol,d/2-c,a-c,2*c,2*c,b).addClass("highcharts-point").add(f),b.isMarker=!0)}};(/Trident\/7\.0/.test(r.navigator.userAgent)||d)&&m(a.Legend.prototype,"positionItem",function(a,g){var b=this,c=function(){g._legendItemPos&&a.call(b,g)};c();setTimeout(c)})})(I);(function(a){var z=a.addEvent,B=a.animate,C=a.animObject,A=a.attr,e=a.doc,d=a.Axis,q=a.createElement,t=a.defaultOptions,
u=a.discardElement,n=a.charts,h=a.css,r=a.defined,m=a.each,c=a.extend,g=a.find,b=a.fireEvent,p=a.getStyle,l=a.grep,E=a.isNumber,G=a.isObject,f=a.isString,H=a.Legend,D=a.marginNames,y=a.merge,M=a.objectEach,v=a.Pointer,L=a.pick,F=a.pInt,J=a.removeEvent,k=a.seriesTypes,w=a.splat,N=a.svg,K=a.syncTimeout,R=a.win,O=a.Renderer,P=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,b,c){return new P(a,b,c)};c(P.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);
if(f(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(b,c){var f,g,k=b.series,d=b.plotOptions||{};b.series=null;f=y(t,b);for(g in f.plotOptions)f.plotOptions[g].tooltip=d[g]&&y(d[g].tooltip)||void 0;f.tooltip.userOptions=b.chart&&b.chart.forExport&&b.tooltip.userOptions||b.tooltip;f.series=b.series=k;this.userOptions=b;b=f.chart;g=b.events;this.margin=[];this.spacing=[];this.bounds={h:{},v:{}};this.callback=c;this.isResizing=0;this.options=f;this.axes=[];this.series=
[];this.hasCartesianSeries=b.showAxes;var h=this;h.index=n.length;n.push(h);a.chartCount++;g&&M(g,function(a,b){z(h,b,a)});h.xAxis=[];h.yAxis=[];h.pointCount=h.colorCounter=h.symbolCounter=0;h.firstRender()},initSeries:function(b){var c=this.options.chart;(c=k[b.type||c.type||c.defaultSeriesType])||a.error(17,!0);c=new c;c.init(this,b);return c},orderSeries:function(a){var b=this.series;for(a=a||0;a<b.length;a++)b[a]&&(b[a].index=a,b[a].name=b[a].name||"Series "+(b[a].index+1))},isInsidePlot:function(a,
b,c){var f=c?b:a;a=c?a:b;return 0<=f&&f<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(f){var g=this.axes,k=this.series,d=this.pointer,h=this.legend,l=this.isDirtyLegend,e,v,p=this.hasCartesianSeries,w=this.isDirtyBox,n,x=this.renderer,r=x.isHidden(),F=[];this.setResponsive&&this.setResponsive(!1);a.setAnimation(f,this);r&&this.temporaryDisplay();this.layOutTitles();for(f=k.length;f--;)if(n=k[f],n.options.stacking&&(e=!0,n.isDirty)){v=!0;break}if(v)for(f=k.length;f--;)n=k[f],n.options.stacking&&
(n.isDirty=!0);m(k,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),l=!0);a.isDirtyData&&b(a,"updatedData")});l&&h.options.enabled&&(h.render(),this.isDirtyLegend=!1);e&&this.getStacks();p&&m(g,function(a){a.updateNames();a.setScale()});this.getMargins();p&&(m(g,function(a){a.isDirty&&(w=!0)}),m(g,function(a){var f=a.min+","+a.max;a.extKey!==f&&(a.extKey=f,F.push(function(){b(a,"afterSetExtremes",c(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(w||e)&&
a.redraw()}));w&&this.drawChartBox();b(this,"predraw");m(k,function(a){(w||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});d&&d.reset(!0);x.draw();b(this,"redraw");b(this,"render");r&&this.temporaryDisplay(!0);m(F,function(a){a.call()})},get:function(a){function b(b){return b.id===a||b.options&&b.options.id===a}var c,f=this.series,k;c=g(this.axes,b)||g(this.series,b);for(k=0;!c&&k<f.length;k++)c=g(f[k].points||[],b);return c},getAxes:function(){var a=this,b=this.options,c=b.xAxis=w(b.xAxis||
{}),b=b.yAxis=w(b.yAxis||{});m(c,function(a,b){a.index=b;a.isX=!0});m(b,function(a,b){a.index=b});c=c.concat(b);m(c,function(b){new d(a,b)})},getSelectedPoints:function(){var a=[];m(this.series,function(b){a=a.concat(l(b.data||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return l(this.series,function(a){return a.selected})},setTitle:function(a,b,c){var f=this,g=f.options,k;k=g.title=y({style:{color:"#333333",fontSize:g.isStock?"16px":"18px"}},g.title,a);g=g.subtitle=
y({style:{color:"#666666"}},g.subtitle,b);m([["title",a,k],["subtitle",b,g]],function(a,b){var c=a[0],g=f[c],k=a[1];a=a[2];g&&k&&(f[c]=g=g.destroy());a&&a.text&&!g&&(f[c]=f.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+c,zIndex:a.zIndex||4}).add(),f[c].update=function(a){f.setTitle(!b&&a,b&&a)},f[c].css(a.style))});f.layOutTitles(c)},layOutTitles:function(a){var b=0,f,g=this.renderer,k=this.spacingBox;m(["title","subtitle"],function(a){var f=this[a],d=this.options[a];
a="title"===a?-3:d.verticalAlign?0:b+2;var h;f&&(h=d.style.fontSize,h=g.fontMetrics(h,f).b,f.css({width:(d.width||k.width+d.widthAdjust)+"px"}).align(c({y:a+h},d),!1,"spacingBox"),d.floating||d.verticalAlign||(b=Math.ceil(b+f.getBBox(d.useHTML).height)))},this);f=this.titleOffset!==b;this.titleOffset=b;!this.isDirtyBox&&f&&(this.isDirtyBox=f,this.hasRendered&&L(a,!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var b=this.options.chart,c=b.width,b=b.height,f=this.renderTo;r(c)||(this.containerWidth=
p(f,"width"));r(b)||(this.containerHeight=p(f,"height"));this.chartWidth=Math.max(0,c||this.containerWidth||600);this.chartHeight=Math.max(0,a.relativeLength(b,this.chartWidth)||this.containerHeight||400)},temporaryDisplay:function(b){var c=this.renderTo;if(b)for(;c&&c.style;)c.hcOrigStyle&&(a.css(c,c.hcOrigStyle),delete c.hcOrigStyle),c.hcOrigDetached&&(e.body.removeChild(c),c.hcOrigDetached=!1),c=c.parentNode;else for(;c&&c.style;){e.body.contains(c)||(c.hcOrigDetached=!0,e.body.appendChild(c));
if("none"===p(c,"display",!1)||c.hcOricDetached)c.hcOrigStyle={display:c.style.display,height:c.style.height,overflow:c.style.overflow},b={display:"block",overflow:"hidden"},c!==this.renderTo&&(b.height=0),a.css(c,b),c.offsetWidth||c.style.setProperty("display","block","important");c=c.parentNode;if(c===e.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var b,g=this.options,k=g.chart,d,h;b=this.renderTo;var l=a.uniqueKey(),v;b||
(this.renderTo=b=k.renderTo);f(b)&&(this.renderTo=b=e.getElementById(b));b||a.error(13,!0);d=F(A(b,"data-highcharts-chart"));E(d)&&n[d]&&n[d].hasRendered&&n[d].destroy();A(b,"data-highcharts-chart",this.index);b.innerHTML="";k.skipClone||b.offsetWidth||this.temporaryDisplay();this.getChartSize();d=this.chartWidth;h=this.chartHeight;v=c({position:"relative",overflow:"hidden",width:d+"px",height:h+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},k.style);
this.container=b=q("div",{id:l},v,b);this._cursor=b.style.cursor;this.renderer=new (a[k.renderer]||O)(b,d,h,null,k.forExport,g.exporting&&g.exporting.allowHTML);this.setClassName(k.className);this.renderer.setStyle(k.style);this.renderer.chartIndex=this.index},getMargins:function(a){var b=this.spacing,c=this.margin,f=this.titleOffset;this.resetMargins();f&&!r(c[0])&&(this.plotTop=Math.max(this.plotTop,f+this.options.title.margin+b[0]));this.legend.display&&this.legend.adjustMargins(c,b);this.extraMargin&&
(this[this.extraMargin.type]=(this[this.extraMargin.type]||0)+this.extraMargin.value);this.extraTopMargin&&(this.plotTop+=this.extraTopMargin);a||this.getAxisMargins()},getAxisMargins:function(){var a=this,b=a.axisOffset=[0,0,0,0],c=a.margin;a.hasCartesianSeries&&m(a.axes,function(a){a.visible&&a.getOffset()});m(D,function(f,g){r(c[g])||(a[f]+=b[g])});a.setChartSize()},reflow:function(a){var b=this,c=b.options.chart,f=b.renderTo,g=r(c.width)&&r(c.height),k=c.width||p(f,"width"),c=c.height||p(f,"height"),
f=a?a.target:R;if(!g&&!b.isPrinting&&k&&c&&(f===R||f===e)){if(k!==b.containerWidth||c!==b.containerHeight)clearTimeout(b.reflowTimeout),b.reflowTimeout=K(function(){b.container&&b.setSize(void 0,void 0,!1)},a?100:0);b.containerWidth=k;b.containerHeight=c}},initReflow:function(){var a=this,b;b=z(R,"resize",function(b){a.reflow(b)});z(a,"destroy",b)},setSize:function(c,f,g){var k=this,d=k.renderer;k.isResizing+=1;a.setAnimation(g,k);k.oldChartHeight=k.chartHeight;k.oldChartWidth=k.chartWidth;void 0!==
c&&(k.options.chart.width=c);void 0!==f&&(k.options.chart.height=f);k.getChartSize();c=d.globalAnimation;(c?B:h)(k.container,{width:k.chartWidth+"px",height:k.chartHeight+"px"},c);k.setChartSize(!0);d.setSize(k.chartWidth,k.chartHeight,g);m(k.axes,function(a){a.isDirty=!0;a.setScale()});k.isDirtyLegend=!0;k.isDirtyBox=!0;k.layOutTitles();k.getMargins();k.redraw(g);k.oldChartHeight=null;b(k,"resize");K(function(){k&&b(k,"endResize",null,function(){--k.isResizing})},C(c).duration)},setChartSize:function(a){function b(a){a=
l[a]||0;return Math.max(n||a,a)/2}var c=this.inverted,f=this.renderer,k=this.chartWidth,g=this.chartHeight,d=this.options.chart,h=this.spacing,l=this.clipOffset,e,v,p,w,n;this.plotLeft=e=Math.round(this.plotLeft);this.plotTop=v=Math.round(this.plotTop);this.plotWidth=p=Math.max(0,Math.round(k-e-this.marginRight));this.plotHeight=w=Math.max(0,Math.round(g-v-this.marginBottom));this.plotSizeX=c?w:p;this.plotSizeY=c?p:w;this.plotBorderWidth=d.plotBorderWidth||0;this.spacingBox=f.spacingBox={x:h[3],y:h[0],
width:k-h[3]-h[1],height:g-h[0]-h[2]};this.plotBox=f.plotBox={x:e,y:v,width:p,height:w};n=2*Math.floor(this.plotBorderWidth/2);c=Math.ceil(b(3));f=Math.ceil(b(0));this.clipBox={x:c,y:f,width:Math.floor(this.plotSizeX-b(1)-c),height:Math.max(0,Math.floor(this.plotSizeY-b(2)-f))};a||m(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,b=a.options.chart;m(["margin","spacing"],function(c){var f=b[c],k=G(f)?f:[f,f,f,f];m(["Top","Right","Bottom","Left"],function(f,
g){a[c][g]=L(b[c+f],k[g])})});m(D,function(b,c){a[b]=L(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,f=this.chartHeight,k=this.chartBackground,g=this.plotBackground,d=this.plotBorder,h,l=this.plotBGImage,e=a.backgroundColor,v=a.plotBackgroundColor,p=a.plotBackgroundImage,m,w=this.plotLeft,n=this.plotTop,r=this.plotWidth,F=this.plotHeight,y=this.plotBox,u=this.clipRect,L=this.clipBox,t="animate";
k||(this.chartBackground=k=b.rect().addClass("highcharts-background").add(),t="attr");h=a.borderWidth||0;m=h+(a.shadow?8:0);e={fill:e||"none"};if(h||k["stroke-width"])e.stroke=a.borderColor,e["stroke-width"]=h;k.attr(e).shadow(a.shadow);k[t]({x:m/2,y:m/2,width:c-m-h%2,height:f-m-h%2,r:a.borderRadius});t="animate";g||(t="attr",this.plotBackground=g=b.rect().addClass("highcharts-plot-background").add());g[t](y);g.attr({fill:v||"none"}).shadow(a.plotShadow);p&&(l?l.animate(y):this.plotBGImage=b.image(p,
w,n,r,F).add());u?u.animate({width:L.width,height:L.height}):this.clipRect=b.clipRect(L);t="animate";d||(t="attr",this.plotBorder=d=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());d.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||0,fill:"none"});d[t](d.crisp({x:w,y:n,width:r,height:F},-d.strokeWidth()));this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,f=a.options.series,g,d;m(["inverted","angular","polar"],function(h){c=k[b.type||b.defaultSeriesType];
d=b[h]||c&&c.prototype[h];for(g=f&&f.length;!d&&g--;)(c=k[f[g].type])&&c.prototype[h]&&(d=!0);a[h]=d})},linkSeries:function(){var a=this,b=a.series;m(b,function(a){a.linkedSeries.length=0});m(b,function(b){var c=b.options.linkedTo;f(c)&&(c=":previous"===c?a.series[b.index-1]:a.get(c))&&c.linkedParent!==b&&(c.linkedSeries.push(b),b.linkedParent=c,b.visible=L(b.options.visible,c.options.visible,b.visible))})},renderSeries:function(){m(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=
this,b=a.options.labels;b.items&&m(b.items,function(f){var k=c(b.style,f.style),g=F(k.left)+a.plotLeft,d=F(k.top)+a.plotTop+12;delete k.left;delete k.top;a.renderer.text(f.html,g,d).attr({zIndex:2}).css(k).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,f,k,g;this.setTitle();this.legend=new H(this,c.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();c=this.plotWidth;f=this.plotHeight-=21;m(a,function(a){a.setScale()});this.getAxisMargins();k=
1.1<c/this.plotWidth;g=1.05<f/this.plotHeight;if(k||g)m(a,function(a){(a.horiz&&k||!a.horiz&&g)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&m(a,function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=b.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var b=this;a=y(!0,this.options.credits,a);a.enabled&&!this.credits&&
(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(R.location.href=a.href)}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},destroy:function(){var c=this,f=c.axes,k=c.series,g=c.container,d,h=g&&g.parentNode;b(c,"destroy");c.renderer.forExport?a.erase(n,c):n[c.index]=void 0;a.chartCount--;c.renderTo.removeAttribute("data-highcharts-chart");
J(c);for(d=f.length;d--;)f[d]=f[d].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(d=k.length;d--;)k[d]=k[d].destroy();m("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),function(a){var b=c[a];b&&b.destroy&&(c[a]=b.destroy())});g&&(g.innerHTML="",J(g),h&&u(g));M(c,function(a,b){delete c[b]})},isReadyToRender:function(){var a=this;return N||R!=R.top||
"complete"===e.readyState?!0:(e.attachEvent("onreadystatechange",function(){e.detachEvent("onreadystatechange",a.firstRender);"complete"===e.readyState&&a.firstRender()}),!1)},firstRender:function(){var a=this,c=a.options;if(a.isReadyToRender()){a.getContainer();b(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();m(c.series||[],function(b){a.initSeries(b)});a.linkSeries();b(a,"beforeRender");v&&(a.pointer=new v(a,c));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();
a.temporaryDisplay(!0)}},onload:function(){m([this.callback].concat(this.callbacks),function(a){a&&void 0!==this.index&&a.apply(this,[this])},this);b(this,"load");b(this,"render");r(this.index)&&!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}})})(I);(function(a){var z,B=a.each,C=a.extend,A=a.erase,e=a.fireEvent,d=a.format,q=a.isArray,t=a.isNumber,u=a.pick,n=a.removeEvent;a.Point=z=function(){};a.Point.prototype={init:function(a,d,e){this.series=a;this.color=a.color;this.applyOptions(d,
e);a.options.colorByPoint?(d=a.options.colors||a.chart.options.colors,this.color=this.color||d[a.colorCounter],d=d.length,e=a.colorCounter,a.colorCounter++,a.colorCounter===d&&(a.colorCounter=0)):e=a.colorIndex;this.colorIndex=u(this.colorIndex,e);a.chart.pointCount++;return this},applyOptions:function(a,d){var h=this.series,c=h.options.pointValKey||h.pointValKey;a=z.prototype.optionsToObject.call(this,a);C(this,a);this.options=this.options?C(this.options,a):a;a.group&&delete this.group;c&&(this.y=
this[c]);this.isNull=u(this.isValid&&!this.isValid(),null===this.x||!t(this.y,!0));this.selected&&(this.state="select");"name"in this&&void 0===d&&h.xAxis&&h.xAxis.hasNames&&(this.x=h.xAxis.nameToX(this));void 0===this.x&&h&&(this.x=void 0===d?h.autoIncrement(this):d);return this},optionsToObject:function(a){var d={},h=this.series,c=h.options.keys,g=c||h.pointArrayMap||["y"],b=g.length,e=0,l=0;if(t(a)||null===a)d[g[0]]=a;else if(q(a))for(!c&&a.length>b&&(h=typeof a[0],"string"===h?d.name=a[0]:"number"===
h&&(d.x=a[0]),e++);l<b;)c&&void 0===a[e]||(d[g[l]]=a[e]),e++,l++;else"object"===typeof a&&(d=a,a.dataLabels&&(h._hasPointLabels=!0),a.marker&&(h._hasPointMarkers=!0));return d},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?
" "+this.zone.className.replace("highcharts-negative",""):"")},getZone:function(){var a=this.series,d=a.zones,a=a.zoneAxis||"y",e=0,c;for(c=d[e];this[a]>=c.value;)c=d[++e];c&&c.color&&!this.options.color&&(this.color=c.color);return c},destroy:function(){var a=this.series.chart,d=a.hoverPoints,e;a.pointCount--;d&&(this.setState(),A(d,this),d.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)n(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);
for(e in this)this[e]=null},destroyElements:function(){for(var a=["graphic","dataLabel","dataLabelUpper","connector","shadowGroup"],d,e=6;e--;)d=a[e],this[d]&&(this[d]=this[d].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var h=this.series,e=h.tooltipOptions,c=u(e.valueDecimals,""),
g=e.valuePrefix||"",b=e.valueSuffix||"";B(h.pointArrayMap||["y"],function(d){d="{point."+d;if(g||b)a=a.replace(d+"}",g+d+"}"+b);a=a.replace(d+"}",d+":,."+c+"f}")});return d(a,{point:this,series:this.series})},firePointEvent:function(a,d,m){var c=this,g=this.series.options;(g.point.events[a]||c.options&&c.options.events&&c.options.events[a])&&this.importEvents();"click"===a&&g.allowPointSelect&&(m=function(a){c.select&&c.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});e(this,a,d,m)},visible:!0}})(I);
(function(a){var z=a.addEvent,B=a.animObject,C=a.arrayMax,A=a.arrayMin,e=a.correctFloat,d=a.Date,q=a.defaultOptions,t=a.defaultPlotOptions,u=a.defined,n=a.each,h=a.erase,r=a.extend,m=a.fireEvent,c=a.grep,g=a.isArray,b=a.isNumber,p=a.isString,l=a.merge,E=a.objectEach,G=a.pick,f=a.removeEvent,H=a.splat,D=a.SVGElement,y=a.syncTimeout,M=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",radius:4,
states:{hover:{animation:{duration:50},enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{hover:{animation:{duration:50},lineWidthPlus:1,marker:{},
halo:{size:10,opacity:.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var c=this,f,k=a.series,g;c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();r(c,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});f=b.events;E(f,function(a,b){z(c,b,a)});if(f&&
f.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();c.getSymbol();n(c.parallelArrays,function(a){c[a+"Data"]=[]});c.setData(b.data,!1);c.isCartesian&&(a.hasCartesianSeries=!0);k.length&&(g=k[k.length-1]);c._i=G(g&&g._i,-1)+1;a.orderSeries(this.insert(k))},insert:function(a){var c=this.options.index,f;if(b(c)){for(f=a.length;f--;)if(c>=G(a[f].options.index,a[f]._i)){a.splice(f+1,0,this);break}-1===f&&a.unshift(this);f+=1}else a.push(this);return G(f,
a.length-1)},bindAxes:function(){var b=this,c=b.options,f=b.chart,g;n(b.axisTypes||[],function(k){n(f[k],function(a){g=a.options;if(c[k]===g.index||void 0!==c[k]&&c[k]===g.id||void 0===c[k]&&0===g.index)b.insert(a.series),b[k]=a,a.isDirty=!0});b[k]||b.optionalAxis===k||a.error(18,!0)})},updateParallelArrays:function(a,c){var f=a.series,g=arguments,k=b(c)?function(b){var g="y"===b&&f.toYData?f.toYData(a):a[b];f[b+"Data"][c]=g}:function(a){Array.prototype[c].apply(f[a+"Data"],Array.prototype.slice.call(g,
2))};n(f.parallelArrays,k)},autoIncrement:function(){var a=this.options,b=this.xIncrement,c,f=a.pointIntervalUnit,b=G(b,a.pointStart,0);this.pointInterval=c=G(this.pointInterval,a.pointInterval,1);f&&(a=new d(b),"day"===f?a=+a[d.hcSetDate](a[d.hcGetDate]()+c):"month"===f?a=+a[d.hcSetMonth](a[d.hcGetMonth]()+c):"year"===f&&(a=+a[d.hcSetFullYear](a[d.hcGetFullYear]()+c)),c=a-b);this.xIncrement=b+c;return b},setOptions:function(a){var b=this.chart,c=b.options,f=c.plotOptions,g=(b.userOptions||{}).plotOptions||
{},d=f[this.type];this.userOptions=a;b=l(d,f.series,a);this.tooltipOptions=l(q.tooltip,q.plotOptions.series&&q.plotOptions.series.tooltip,q.plotOptions[this.type].tooltip,c.tooltip.userOptions,f.series&&f.series.tooltip,f[this.type].tooltip,a.tooltip);this.stickyTracking=G(a.stickyTracking,g[this.type]&&g[this.type].stickyTracking,g.series&&g.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:b.stickyTracking);null===d.marker&&delete b.marker;this.zoneAxis=b.zoneAxis;a=this.zones=
(b.zones||[]).slice();!b.negativeColor&&!b.negativeFillColor||b.zones||a.push({value:b[this.zoneAxis+"Threshold"]||b.threshold||0,className:"highcharts-negative",color:b.negativeColor,fillColor:b.negativeFillColor});a.length&&u(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return b},getCyclic:function(a,b,c){var f,g=this.chart,d=this.userOptions,h=a+"Index",e=a+"Counter",l=c?c.length:G(g.options.chart[a+"Count"],g[a+"Count"]);b||(f=G(d[h],d["_"+h]),u(f)||(g.series.length||
(g[e]=0),d["_"+h]=f=g[e]%l,g[e]+=1),c&&(b=c[f]));void 0!==f&&(this[h]=f);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||t[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(c,f,d,h){var k=this,e=k.points,l=e&&e.length||0,v,m=k.options,r=k.chart,y=null,u=k.xAxis,
t=m.turboThreshold,D=this.xData,F=this.yData,q=(v=k.pointArrayMap)&&v.length;c=c||[];v=c.length;f=G(f,!0);if(!1!==h&&v&&l===v&&!k.cropped&&!k.hasGroupedData&&k.visible)n(c,function(a,b){e[b].update&&a!==m.data[b]&&e[b].update(a,!1,null,!1)});else{k.xIncrement=null;k.colorCounter=0;n(this.parallelArrays,function(a){k[a+"Data"].length=0});if(t&&v>t){for(d=0;null===y&&d<v;)y=c[d],d++;if(b(y))for(d=0;d<v;d++)D[d]=this.autoIncrement(),F[d]=c[d];else if(g(y))if(q)for(d=0;d<v;d++)y=c[d],D[d]=y[0],F[d]=y.slice(1,
q+1);else for(d=0;d<v;d++)y=c[d],D[d]=y[0],F[d]=y[1];else a.error(12)}else for(d=0;d<v;d++)void 0!==c[d]&&(y={series:k},k.pointClass.prototype.applyOptions.apply(y,[c[d]]),k.updateParallelArrays(y,d));p(F[0])&&a.error(14,!0);k.data=[];k.options.data=k.userOptions.data=c;for(d=l;d--;)e[d]&&e[d].destroy&&e[d].destroy();u&&(u.minRange=u.userMinRange);k.isDirty=r.isDirtyBox=!0;k.isDirtyData=!!e;d=!1}"point"===m.legendType&&(this.processData(),this.generatePoints());f&&r.redraw(d)},processData:function(b){var c=
this.xData,f=this.yData,g=c.length,d;d=0;var h,e,l=this.xAxis,v,p=this.options;v=p.cropThreshold;var m=this.getExtremesFromAll||p.getExtremesFromAll,n=this.isCartesian,p=l&&l.val2lin,y=l&&l.isLog,r,u;if(n&&!this.isDirty&&!l.isDirty&&!this.yAxis.isDirty&&!b)return!1;l&&(b=l.getExtremes(),r=b.min,u=b.max);if(n&&this.sorted&&!m&&(!v||g>v||this.forceCrop))if(c[g-1]<r||c[0]>u)c=[],f=[];else if(c[0]<r||c[g-1]>u)d=this.cropData(this.xData,this.yData,r,u),c=d.xData,f=d.yData,d=d.start,h=!0;for(v=c.length||
1;--v;)g=y?p(c[v])-p(c[v-1]):c[v]-c[v-1],0<g&&(void 0===e||g<e)?e=g:0>g&&this.requireSorting&&a.error(15);this.cropped=h;this.cropStart=d;this.processedXData=c;this.processedYData=f;this.closestPointRange=e},cropData:function(a,b,c,f){var g=a.length,d=0,e=g,h=G(this.cropShoulder,1),l;for(l=0;l<g;l++)if(a[l]>=c){d=Math.max(0,l-h);break}for(c=l;c<g;c++)if(a[c]>f){e=c+h;break}return{xData:a.slice(d,e),yData:b.slice(d,e),start:d,end:e}},generatePoints:function(){var a=this.options,b=a.data,c=this.data,
f,g=this.processedXData,d=this.processedYData,e=this.pointClass,h=g.length,l=this.cropStart||0,p,m=this.hasGroupedData,a=a.keys,n,r=[],y;c||m||(c=[],c.length=b.length,c=this.data=c);a&&m&&(this.options.keys=!1);for(y=0;y<h;y++)p=l+y,m?(n=(new e).init(this,[g[y]].concat(H(d[y]))),n.dataGroup=this.groupMap[y]):(n=c[p])||void 0===b[p]||(c[p]=n=(new e).init(this,b[p],g[y])),n&&(n.index=p,r[y]=n);this.options.keys=a;if(c&&(h!==(f=c.length)||m))for(y=0;y<f;y++)y!==l||m||(y+=h),c[y]&&(c[y].destroyElements(),
c[y].plotX=void 0);this.data=c;this.points=r},getExtremes:function(a){var c=this.yAxis,f=this.processedXData,d,k=[],e=0;d=this.xAxis.getExtremes();var h=d.min,l=d.max,v,p,m,n;a=a||this.stackedYData||this.processedYData||[];d=a.length;for(n=0;n<d;n++)if(p=f[n],m=a[n],v=(b(m,!0)||g(m))&&(!c.positiveValuesOnly||m.length||0<m),p=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(f[n]||p)>=h&&(f[n]||p)<=l,v&&p)if(v=m.length)for(;v--;)null!==m[v]&&(k[e++]=m[v]);else k[e++]=m;this.dataMin=
A(k);this.dataMax=C(k)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,c=a.stacking,f=this.xAxis,g=f.categories,d=this.yAxis,h=this.points,l=h.length,p=!!this.modifyValue,m=a.pointPlacement,n="between"===m||b(m),y=a.threshold,r=a.startFromThreshold?y:0,t,D,q,H,E=Number.MAX_VALUE;"between"===m&&(m=.5);b(m)&&(m*=G(a.pointRange||f.pointRange));for(a=0;a<l;a++){var M=h[a],A=M.x,z=M.y;D=M.low;var B=c&&d.stacks[(this.negStacks&&z<(r?0:y)?"-":"")+this.stackKey],
C;d.positiveValuesOnly&&null!==z&&0>=z&&(M.isNull=!0);M.plotX=t=e(Math.min(Math.max(-1E5,f.translate(A,0,0,0,1,m,"flags"===this.type)),1E5));c&&this.visible&&!M.isNull&&B&&B[A]&&(H=this.getStackIndicator(H,A,this.index),C=B[A],z=C.points[H.key],D=z[0],z=z[1],D===r&&H.key===B[A].base&&(D=G(y,d.min)),d.positiveValuesOnly&&0>=D&&(D=null),M.total=M.stackTotal=C.total,M.percentage=C.total&&M.y/C.total*100,M.stackY=z,C.setOffset(this.pointXOffset||0,this.barW||0));M.yBottom=u(D)?d.translate(D,0,1,0,1):
null;p&&(z=this.modifyValue(z,M));M.plotY=D="number"===typeof z&&Infinity!==z?Math.min(Math.max(-1E5,d.translate(z,0,1,0,1)),1E5):void 0;M.isInside=void 0!==D&&0<=D&&D<=d.len&&0<=t&&t<=f.len;M.clientX=n?e(f.translate(A,0,0,0,1,m)):t;M.negative=M.y<(y||0);M.category=g&&void 0!==g[M.x]?g[M.x]:M.x;M.isNull||(void 0!==q&&(E=Math.min(E,Math.abs(t-q))),q=t);M.zone=this.zones.length&&M.getZone()}this.closestPointRangePx=E},getValidPoints:function(a,b){var f=this.chart;return c(a||this.points||[],function(a){return b&&
!f.isInsidePlot(a.plotX,a.plotY,f.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,c=this.options,f=b.renderer,g=b.inverted,d=this.clipBox,h=d||b.clipBox,e=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,h.height,c.xAxis,c.yAxis].join(),l=b[e],v=b[e+"m"];l||(a&&(h.width=0,b[e+"m"]=v=f.clipRect(-99,g?-b.plotLeft:-b.plotTop,99,g?b.chartWidth:b.chartHeight)),b[e]=l=f.clipRect(h),l.count={length:0});a&&!l.count[this.index]&&(l.count[this.index]=!0,l.count.length+=1);!1!==c.clip&&
(this.group.clip(a||d?l:b.clipRect),this.markerGroup.clip(v),this.sharedClipKey=e);a||(l.count[this.index]&&(delete l.count[this.index],--l.count.length),0===l.count.length&&e&&b[e]&&(d||(b[e]=b[e].destroy()),b[e+"m"]&&(b[e+"m"]=b[e+"m"].destroy())))},animate:function(a){var b=this.chart,c=B(this.options.animation),f;a?this.setClip(c):(f=this.sharedClipKey,(a=b[f])&&a.animate({width:b.plotSizeX},c),b[f+"m"]&&b[f+"m"].animate({width:b.plotSizeX+99},c),this.animate=null)},afterAnimate:function(){this.setClip();
m(this,"afterAnimate");this.finishedAnimating=!0},drawPoints:function(){var a=this.points,c=this.chart,f,g,d,e,h=this.options.marker,l,p,m,n,y=this[this.specialGroup]||this.markerGroup,r=G(h.enabled,this.xAxis.isRadial?!0:null,this.closestPointRangePx>=2*h.radius);if(!1!==h.enabled||this._hasPointMarkers)for(g=0;g<a.length;g++)d=a[g],f=d.plotY,e=d.graphic,l=d.marker||{},p=!!d.marker,m=r&&void 0===l.enabled||l.enabled,n=d.isInside,m&&b(f)&&null!==d.y?(f=G(l.symbol,this.symbol),d.hasImage=0===f.indexOf("url"),
m=this.markerAttribs(d,d.selected&&"select"),e?e[n?"show":"hide"](!0).animate(m):n&&(0<m.width||d.hasImage)&&(d.graphic=e=c.renderer.symbol(f,m.x,m.y,m.width,m.height,p?l:h).add(y)),e&&e.attr(this.pointAttribs(d,d.selected&&"select")),e&&e.addClass(d.getClassName(),!0)):e&&(d.graphic=e.destroy())},markerAttribs:function(a,b){var c=this.options.marker,f=a.marker||{},g=G(f.radius,c.radius);b&&(c=c.states[b],b=f.states&&f.states[b],g=G(b&&b.radius,c&&c.radius,g+(c&&c.radiusPlus||0)));a.hasImage&&(g=
0);a={x:Math.floor(a.plotX)-g,y:a.plotY-g};g&&(a.width=a.height=2*g);return a},pointAttribs:function(a,b){var c=this.options.marker,f=a&&a.options,g=f&&f.marker||{},d=this.color,e=f&&f.color,h=a&&a.color,f=G(g.lineWidth,c.lineWidth);a=a&&a.zone&&a.zone.color;d=e||a||h||d;a=g.fillColor||c.fillColor||d;d=g.lineColor||c.lineColor||d;b&&(c=c.states[b],b=g.states&&g.states[b]||{},f=G(b.lineWidth,c.lineWidth,f+G(b.lineWidthPlus,c.lineWidthPlus,0)),a=b.fillColor||c.fillColor||a,d=b.lineColor||c.lineColor||
d);return{stroke:d,"stroke-width":f,fill:a}},destroy:function(){var a=this,b=a.chart,c=/AppleWebKit\/533/.test(M.navigator.userAgent),g,d,e=a.data||[],l,p;m(a,"destroy");f(a);n(a.axisTypes||[],function(b){(p=a[b])&&p.series&&(h(p.series,a),p.isDirty=p.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);for(d=e.length;d--;)(l=e[d])&&l.destroy&&l.destroy();a.points=null;clearTimeout(a.animationTimeout);E(a,function(a,b){a instanceof D&&!a.survive&&(g=c&&"group"===b?"hide":"destroy",a[g]())});
b.hoverSeries===a&&(b.hoverSeries=null);h(b.series,a);b.orderSeries();E(a,function(b,c){delete a[c]})},getGraphPath:function(a,b,c){var f=this,g=f.options,d=g.step,e,h=[],l=[],p;a=a||f.points;(e=a.reversed)&&a.reverse();(d={right:1,center:2}[d]||d&&3)&&e&&(d=4-d);!g.connectNulls||b||c||(a=this.getValidPoints(a));n(a,function(k,e){var v=k.plotX,m=k.plotY,n=a[e-1];(k.leftCliff||n&&n.rightCliff)&&!c&&(p=!0);k.isNull&&!u(b)&&0<e?p=!g.connectNulls:k.isNull&&!b?p=!0:(0===e||p?e=["M",k.plotX,k.plotY]:f.getPointSpline?
e=f.getPointSpline(a,k,e):d?(e=1===d?["L",n.plotX,m]:2===d?["L",(n.plotX+v)/2,n.plotY,"L",(n.plotX+v)/2,m]:["L",v,n.plotY],e.push("L",v,m)):e=["L",v,m],l.push(k.x),d&&l.push(k.x),h.push.apply(h,e),p=!1)});h.xMap=l;return f.graphPath=h},drawGraph:function(){var a=this,b=this.options,c=(this.gappedPath||this.getGraphPath).call(this),f=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];n(this.zones,function(c,g){f.push(["zone-graph-"+g,"highcharts-graph highcharts-zone-graph-"+g+" "+
(c.className||""),c.color||a.color,c.dashStyle||b.dashStyle])});n(f,function(f,g){var d=f[0],k=a[d];k?(k.endX=c.xMap,k.animate({d:c})):c.length&&(a[d]=a.chart.renderer.path(c).addClass(f[1]).attr({zIndex:1}).add(a.group),k={stroke:f[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&a.color||"none"},f[3]?k.dashstyle=f[3]:"square"!==b.linecap&&(k["stroke-linecap"]=k["stroke-linejoin"]="round"),k=a[d].attr(k).shadow(2>g&&b.shadow));k&&(k.startX=c.xMap,k.isArea=c.isArea)})},applyZones:function(){var a=
this,b=this.chart,c=b.renderer,f=this.zones,g,d,e=this.clips||[],h,l=this.graph,p=this.area,m=Math.max(b.chartWidth,b.chartHeight),y=this[(this.zoneAxis||"y")+"Axis"],r,u,t=b.inverted,D,q,H,E,M=!1;f.length&&(l||p)&&y&&void 0!==y.min&&(u=y.reversed,D=y.horiz,l&&l.hide(),p&&p.hide(),r=y.getExtremes(),n(f,function(f,k){g=u?D?b.plotWidth:0:D?0:y.toPixels(r.min);g=Math.min(Math.max(G(d,g),0),m);d=Math.min(Math.max(Math.round(y.toPixels(G(f.value,r.max),!0)),0),m);M&&(g=d=y.toPixels(r.max));q=Math.abs(g-
d);H=Math.min(g,d);E=Math.max(g,d);y.isXAxis?(h={x:t?E:H,y:0,width:q,height:m},D||(h.x=b.plotHeight-h.x)):(h={x:0,y:t?E:H,width:m,height:q},D&&(h.y=b.plotWidth-h.y));t&&c.isVML&&(h=y.isXAxis?{x:0,y:u?H:E,height:h.width,width:b.chartWidth}:{x:h.y-b.plotLeft-b.spacingBox.x,y:0,width:h.height,height:b.chartHeight});e[k]?e[k].animate(h):(e[k]=c.clipRect(h),l&&a["zone-graph-"+k].clip(e[k]),p&&a["zone-area-"+k].clip(e[k]));M=f.value>r.max}),this.clips=e)},invertGroups:function(a){function b(){n(["group",
"markerGroup"],function(b){c[b]&&(f.renderer.isVML&&c[b].attr({width:c.yAxis.len,height:c.xAxis.len}),c[b].width=c.yAxis.len,c[b].height=c.xAxis.len,c[b].invert(a))})}var c=this,f=c.chart,g;c.xAxis&&(g=z(f,"resize",b),z(c,"destroy",g),b(a),c.invertGroups=b)},plotGroup:function(a,b,c,f,g){var d=this[a],k=!d;k&&(this[a]=d=this.chart.renderer.g().attr({zIndex:f||.1}).add(g));d.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series highcharts-color-"+this.colorIndex+
" "+(this.options.className||""),!0);d.attr({visibility:c})[k?"attr":"animate"](this.getPlotBox());return d},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;a.inverted&&(b=c,c=this.xAxis);return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,c,f=a.options,g=!!a.animate&&b.renderer.isSVG&&B(f.animation).duration,d=a.visible?"inherit":"hidden",e=f.zIndex,h=a.hasRendered,l=b.seriesGroup,p=b.inverted;c=a.plotGroup("group",
"series",d,e,l);a.markerGroup=a.plotGroup("markerGroup","markers",d,e,l);g&&a.animate(!0);c.inverted=a.isCartesian?p:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(p);!1===f.clip||a.sharedClipKey||h||c.clip(b.clipRect);g&&a.animate();h||(a.animationTimeout=y(function(){a.afterAnimate()},g));a.isDirty=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,
b=this.isDirty||this.isDirtyData,c=this.group,f=this.xAxis,g=this.yAxis;c&&(a.inverted&&c.attr({width:a.plotWidth,height:a.plotHeight}),c.animate({translateX:G(f&&f.left,a.plotLeft),translateY:G(g&&g.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var c=this.xAxis,f=this.yAxis,g=this.chart.inverted;return this.searchKDTree({clientX:g?c.len-a.chartY+c.pos:a.chartX-c.pos,plotY:g?f.len-a.chartX+f.pos:a.chartY-f.pos},b)},
buildKDTree:function(){function a(c,f,g){var d,k;if(k=c&&c.length)return d=b.kdAxisArray[f%g],c.sort(function(a,b){return a[d]-b[d]}),k=Math.floor(k/2),{point:c[k],left:a(c.slice(0,k),f+1,g),right:a(c.slice(k+1),f+1,g)}}this.buildingKdTree=!0;var b=this,c=-1<b.options.findNearestPointBy.indexOf("y")?2:1;delete b.kdTree;y(function(){b.kdTree=a(b.getValidPoints(null,!b.directTouch),c,c);b.buildingKdTree=!1},b.options.kdNow?0:1)},searchKDTree:function(a,b){function c(a,b,k,h){var l=b.point,p=f.kdAxisArray[k%
h],m,n,v=l;n=u(a[g])&&u(l[g])?Math.pow(a[g]-l[g],2):null;m=u(a[d])&&u(l[d])?Math.pow(a[d]-l[d],2):null;m=(n||0)+(m||0);l.dist=u(m)?Math.sqrt(m):Number.MAX_VALUE;l.distX=u(n)?Math.sqrt(n):Number.MAX_VALUE;p=a[p]-l[p];m=0>p?"left":"right";n=0>p?"right":"left";b[m]&&(m=c(a,b[m],k+1,h),v=m[e]<v[e]?m:l);b[n]&&Math.sqrt(p*p)<v[e]&&(a=c(a,b[n],k+1,h),v=a[e]<v[e]?a:v);return v}var f=this,g=this.kdAxisArray[0],d=this.kdAxisArray[1],e=b?"distX":"dist";b=-1<f.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||
this.buildingKdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,b,b)}})})(I);(function(a){var z=a.addEvent,B=a.animate,C=a.Axis,A=a.createElement,e=a.css,d=a.defined,q=a.each,t=a.erase,u=a.extend,n=a.fireEvent,h=a.inArray,r=a.isNumber,m=a.isObject,c=a.isArray,g=a.merge,b=a.objectEach,p=a.pick,l=a.Point,E=a.Series,G=a.seriesTypes,f=a.setAnimation,H=a.splat;u(a.Chart.prototype,{addSeries:function(a,b,c){var f,g=this;a&&(b=p(b,!0),n(g,"addSeries",{options:a},function(){f=g.initSeries(a);
g.isDirtyLegend=!0;g.linkSeries();b&&g.redraw(c)}));return f},addAxis:function(a,b,c,f){var d=b?"xAxis":"yAxis",e=this.options;a=g(a,{index:this[d].length,isX:b});b=new C(this,a);e[d]=H(e[d]||{});e[d].push(a);p(c,!0)&&this.redraw(f);return b},showLoading:function(a){var b=this,c=b.options,f=b.loadingDiv,g=c.loading,d=function(){f&&e(f,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+"px",height:b.plotHeight+"px"})};f||(b.loadingDiv=f=A("div",{className:"highcharts-loading highcharts-loading-hidden"},
null,b.container),b.loadingSpan=A("span",{className:"highcharts-loading-inner"},null,f),z(b,"redraw",d));f.className="highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;e(f,u(g.style,{zIndex:10}));e(b.loadingSpan,g.labelStyle);b.loadingShown||(e(f,{opacity:0,display:""}),B(f,{opacity:g.style.opacity||.5},{duration:g.showDuration||0}));b.loadingShown=!0;d()},hideLoading:function(){var a=this.options,b=this.loadingDiv;b&&(b.className="highcharts-loading highcharts-loading-hidden",B(b,{opacity:0},
{duration:a.loading.hideDuration||100,complete:function(){e(b,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions tooltip".split(" "),
update:function(a,c,f){var e=this,l={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},m=a.chart,n,k,y=[];if(m){g(!0,e.options.chart,m);"className"in m&&e.setClassName(m.className);if("inverted"in m||"polar"in m)e.propFromSeries(),n=!0;"alignTicks"in m&&(n=!0);b(m,function(a,b){-1!==h("chart."+b,e.propsRequireUpdateSeries)&&(k=!0);-1!==h(b,e.propsRequireDirtyBox)&&(e.isDirtyBox=!0)});"style"in m&&e.renderer.setStyle(m.style)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&g(!0,
this.options.plotOptions,a.plotOptions);b(a,function(a,b){if(e[b]&&"function"===typeof e[b].update)e[b].update(a,!1);else if("function"===typeof e[l[b]])e[l[b]](a);"chart"!==b&&-1!==h(b,e.propsRequireUpdateSeries)&&(k=!0)});q("xAxis yAxis zAxis series colorAxis pane".split(" "),function(b){a[b]&&(q(H(a[b]),function(a,c){(c=d(a.id)&&e.get(a.id)||e[b][c])&&c.coll===b&&(c.update(a,!1),f&&(c.touched=!0));if(!c&&f)if("series"===b)e.addSeries(a,!1).touched=!0;else if("xAxis"===b||"yAxis"===b)e.addAxis(a,
"xAxis"===b,!1).touched=!0}),f&&q(e[b],function(a){a.touched?delete a.touched:y.push(a)}))});q(y,function(a){a.remove(!1)});n&&q(e.axes,function(a){a.update({},!1)});k&&q(e.series,function(a){a.update({},!1)});a.loading&&g(!0,e.options.loading,a.loading);n=m&&m.width;m=m&&m.height;r(n)&&n!==e.chartWidth||r(m)&&m!==e.chartHeight?e.setSize(n,m):p(c,!0)&&e.redraw()},setSubtitle:function(a){this.setTitle(void 0,a)}});u(l.prototype,{update:function(a,b,c,f){function g(){d.applyOptions(a);null===d.y&&k&&
(d.graphic=k.destroy());m(a,!0)&&(k&&k.element&&a&&a.marker&&void 0!==a.marker.symbol&&(d.graphic=k.destroy()),a&&a.dataLabels&&d.dataLabel&&(d.dataLabel=d.dataLabel.destroy()));h=d.index;e.updateParallelArrays(d,h);n.data[h]=m(n.data[h],!0)||m(a,!0)?d.options:a;e.isDirty=e.isDirtyData=!0;!e.fixedBox&&e.hasCartesianSeries&&(l.isDirtyBox=!0);"point"===n.legendType&&(l.isDirtyLegend=!0);b&&l.redraw(c)}var d=this,e=d.series,k=d.graphic,h,l=e.chart,n=e.options;b=p(b,!0);!1===f?g():d.firePointEvent("update",
{options:a},g)},remove:function(a,b){this.series.removePoint(h(this,this.series.data),a,b)}});u(E.prototype,{addPoint:function(a,b,c,f){var g=this.options,d=this.data,e=this.chart,k=this.xAxis,k=k&&k.hasNames&&k.names,h=g.data,l,m,n=this.xData,v,r;b=p(b,!0);l={series:this};this.pointClass.prototype.applyOptions.apply(l,[a]);r=l.x;v=n.length;if(this.requireSorting&&r<n[v-1])for(m=!0;v&&n[v-1]>r;)v--;this.updateParallelArrays(l,"splice",v,0,0);this.updateParallelArrays(l,v);k&&l.name&&(k[r]=l.name);
h.splice(v,0,a);m&&(this.data.splice(v,0,null),this.processData());"point"===g.legendType&&this.generatePoints();c&&(d[0]&&d[0].remove?d[0].remove(!1):(d.shift(),this.updateParallelArrays(l,"shift"),h.shift()));this.isDirtyData=this.isDirty=!0;b&&e.redraw(f)},removePoint:function(a,b,c){var g=this,d=g.data,e=d[a],h=g.points,k=g.chart,l=function(){h&&h.length===d.length&&h.splice(a,1);d.splice(a,1);g.options.data.splice(a,1);g.updateParallelArrays(e||{series:g},"splice",a,1);e&&e.destroy();g.isDirty=
!0;g.isDirtyData=!0;b&&k.redraw()};f(c,k);b=p(b,!0);e?e.firePointEvent("remove",null,l):l()},remove:function(a,b,c){function f(){g.destroy();d.isDirtyLegend=d.isDirtyBox=!0;d.linkSeries();p(a,!0)&&d.redraw(b)}var g=this,d=g.chart;!1!==c?n(g,"remove",null,f):f()},update:function(a,b){var c=this,f=c.chart,d=c.userOptions,e=c.oldType||c.type,h=a.type||d.type||f.options.chart.type,k=G[e].prototype,l,m=["group","markerGroup","dataLabelsGroup","navigatorSeries","baseSeries"],n=c.finishedAnimating&&{animation:!1};
if(Object.keys&&"data"===Object.keys(a).toString())return this.setData(a.data,b);if(h&&h!==e||void 0!==a.zIndex)m.length=0;q(m,function(a){m[a]=c[a];delete c[a]});a=g(d,n,{index:c.index,pointStart:c.xData[0]},{data:c.options.data},a);c.remove(!1,null,!1);for(l in k)c[l]=void 0;u(c,G[h||e].prototype);q(m,function(a){c[a]=m[a]});c.init(f,a);c.oldType=e;f.linkSeries();p(b,!0)&&f.redraw(!1)}});u(C.prototype,{update:function(a,b){var c=this.chart;a=c.options[this.coll][this.options.index]=g(this.userOptions,
a);this.destroy(!0);this.init(c,u(a,{events:void 0}));c.isDirtyBox=!0;p(b,!0)&&c.redraw()},remove:function(a){for(var b=this.chart,f=this.coll,g=this.series,d=g.length;d--;)g[d]&&g[d].remove(!1);t(b.axes,this);t(b[f],this);c(b.options[f])?b.options[f].splice(this.options.index,1):delete b.options[f];q(b[f],function(a,b){a.options.index=b});this.destroy();b.isDirtyBox=!0;p(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})})(I);
(function(a){var z=a.animObject,B=a.color,C=a.each,A=a.extend,e=a.isNumber,d=a.merge,q=a.pick,t=a.Series,u=a.seriesType,n=a.svg;u("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1,shadow:!1},select:{color:"#cccccc",borderColor:"#000000",shadow:!1}},dataLabels:{align:null,verticalAlign:null,y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,
borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){t.prototype.init.apply(this,arguments);var a=this,d=a.chart;d.hasRendered&&C(d.series,function(d){d.type===a.type&&(d.isDirty=!0)})},getColumnMetrics:function(){var a=this,d=a.options,e=a.xAxis,c=a.yAxis,g=e.reversed,b,p={},l=0;!1===d.grouping?l=1:C(a.chart.series,function(f){var d=f.options,g=f.yAxis,e;f.type!==a.type||!f.visible&&a.chart.options.chart.ignoreHiddenSeries||
c.len!==g.len||c.pos!==g.pos||(d.stacking?(b=f.stackKey,void 0===p[b]&&(p[b]=l++),e=p[b]):!1!==d.grouping&&(e=l++),f.columnIndex=e)});var n=Math.min(Math.abs(e.transA)*(e.ordinalSlope||d.pointRange||e.closestPointRange||e.tickInterval||1),e.len),u=n*d.groupPadding,f=(n-2*u)/(l||1),d=Math.min(d.maxPointWidth||e.len,q(d.pointWidth,f*(1-2*d.pointPadding)));a.columnMetrics={width:d,offset:(f-d)/2+(u+((a.columnIndex||0)+(g?1:0))*f-n/2)*(g?-1:1)};return a.columnMetrics},crispCol:function(a,d,e,c){var g=
this.chart,b=this.borderWidth,h=-(b%2?.5:0),b=b%2?.5:1;g.inverted&&g.renderer.isVML&&(b+=1);this.options.crisp&&(e=Math.round(a+e)+h,a=Math.round(a)+h,e-=a);c=Math.round(d+c)+b;h=.5>=Math.abs(d)&&.5<c;d=Math.round(d)+b;c-=d;h&&c&&(--d,c+=1);return{x:a,y:d,width:e,height:c}},translate:function(){var a=this,d=a.chart,e=a.options,c=a.dense=2>a.closestPointRange*a.xAxis.transA,c=a.borderWidth=q(e.borderWidth,c?0:1),g=a.yAxis,b=a.translatedThreshold=g.getThreshold(e.threshold),p=q(e.minPointLength,5),
l=a.getColumnMetrics(),n=l.width,u=a.barW=Math.max(n,1+2*c),f=a.pointXOffset=l.offset;d.inverted&&(b-=.5);e.pointPadding&&(u=Math.ceil(u));t.prototype.translate.apply(a);C(a.points,function(c){var e=q(c.yBottom,b),l=999+Math.abs(e),l=Math.min(Math.max(-l,c.plotY),g.len+l),h=c.plotX+f,m=u,r=Math.min(l,e),t,H=Math.max(l,e)-r;Math.abs(H)<p&&p&&(H=p,t=!g.reversed&&!c.negative||g.reversed&&c.negative,r=Math.abs(r-b)>p?e-p:b-(t?p:0));c.barX=h;c.pointWidth=n;c.tooltipPos=d.inverted?[g.len+g.pos-d.plotLeft-
l,a.xAxis.len-h-m/2,H]:[h+m/2,l+g.pos-d.plotTop,H];c.shapeType="rect";c.shapeArgs=a.crispCol.apply(a,c.isNull?[h,b,m,0]:[h,r,m,H])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},pointAttribs:function(a,e){var h=this.options,c,g=this.pointAttrToOptions||{};c=g.stroke||"borderColor";var b=g["stroke-width"]||"borderWidth",p=a&&a.color||this.color,l=a[c]||h[c]||this.color||p,n=a[b]||
h[b]||this[b]||0,g=h.dashStyle;a&&this.zones.length&&(p=a.getZone(),p=a.options.color||p&&p.color||this.color);e&&(a=d(h.states[e],a.options.states&&a.options.states[e]||{}),e=a.brightness,p=a.color||void 0!==e&&B(p).brighten(a.brightness).get()||p,l=a[c]||l,n=a[b]||n,g=a.dashStyle||g);c={fill:p,stroke:l,"stroke-width":n};g&&(c.dashstyle=g);return c},drawPoints:function(){var a=this,n=this.chart,m=a.options,c=n.renderer,g=m.animationLimit||250,b;C(a.points,function(h){var l=h.graphic;if(e(h.plotY)&&
null!==h.y){b=h.shapeArgs;if(l)l[n.pointCount<g?"animate":"attr"](d(b));else h.graphic=l=c[h.shapeType](b).add(h.group||a.group);m.borderRadius&&l.attr({r:m.borderRadius});l.attr(a.pointAttribs(h,h.selected&&"select")).shadow(m.shadow,null,m.stacking&&!m.borderRadius);l.addClass(h.getClassName(),!0)}else l&&(h.graphic=l.destroy())})},animate:function(a){var d=this,e=this.yAxis,c=d.options,g=this.chart.inverted,b={};n&&(a?(b.scaleY=.001,a=Math.min(e.pos+e.len,Math.max(e.pos,e.toPixels(c.threshold))),
g?b.translateX=a-e.len:b.translateY=a,d.group.attr(b)):(b[g?"translateX":"translateY"]=e.pos,d.group.animate(b,A(z(d.options.animation),{step:function(a,b){d.group.attr({scaleY:Math.max(.001,b.pos)})}})),d.animate=null))},remove:function(){var a=this,d=a.chart;d.hasRendered&&C(d.series,function(d){d.type===a.type&&(d.isDirty=!0)});t.prototype.remove.apply(a,arguments)}})})(I);(function(a){var z=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,findNearestPointBy:"xy",marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',
pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&z.prototype.drawGraph.call(this)}})})(I);(function(a){var z=a.addEvent,B=a.arrayMax,C=a.defined,A=a.each,e=a.extend,d=a.format,q=a.map,t=a.merge,u=a.noop,n=a.pick,h=a.relativeLength,r=a.Series,m=a.seriesTypes,c=a.stableSort;a.distribute=
function(a,b){function d(a,b){return a.target-b.target}var g,e=!0,h=a,f=[],m;m=0;for(g=a.length;g--;)m+=a[g].size;if(m>b){c(a,function(a,b){return(b.rank||0)-(a.rank||0)});for(m=g=0;m<=b;)m+=a[g].size,g++;f=a.splice(g-1,a.length)}c(a,d);for(a=q(a,function(a){return{size:a.size,targets:[a.target]}});e;){for(g=a.length;g--;)e=a[g],m=(Math.min.apply(0,e.targets)+Math.max.apply(0,e.targets))/2,e.pos=Math.min(Math.max(0,m-e.size/2),b-e.size);g=a.length;for(e=!1;g--;)0<g&&a[g-1].pos+a[g-1].size>a[g].pos&&
(a[g-1].size+=a[g].size,a[g-1].targets=a[g-1].targets.concat(a[g].targets),a[g-1].pos+a[g-1].size>b&&(a[g-1].pos=b-a[g-1].size),a.splice(g,1),e=!0)}g=0;A(a,function(a){var b=0;A(a.targets,function(){h[g].pos=a.pos+b;b+=h[g].size;g++})});h.push.apply(h,f);c(h,d)};r.prototype.drawDataLabels=function(){var c=this,b=c.options,e=b.dataLabels,l=c.points,h,m,f=c.hasRendered||0,u,r,y=n(e.defer,!!b.animation),q=c.chart.renderer;if(e.enabled||c._hasPointLabels)c.dlProcessOptions&&c.dlProcessOptions(e),r=c.plotGroup("dataLabelsGroup",
"data-labels",y&&!f?"hidden":"visible",e.zIndex||6),y&&(r.attr({opacity:+f}),f||z(c,"afterAnimate",function(){c.visible&&r.show(!0);r[b.animation?"animate":"attr"]({opacity:1},{duration:200})})),m=e,A(l,function(f){var g,l=f.dataLabel,p,k,v=f.connector,y=!l,D;h=f.dlOptions||f.options&&f.options.dataLabels;if(g=n(h&&h.enabled,m.enabled)&&null!==f.y)e=t(m,h),p=f.getLabelConfig(),u=e.format?d(e.format,p):e.formatter.call(p,e),D=e.style,p=e.rotation,D.color=n(e.color,D.color,c.color,"#000000"),"contrast"===
D.color&&(f.contrastColor=q.getContrast(f.color||c.color),D.color=e.inside||0>n(f.labelDistance,e.distance)||b.stacking?f.contrastColor:"#000000"),b.cursor&&(D.cursor=b.cursor),k={fill:e.backgroundColor,stroke:e.borderColor,"stroke-width":e.borderWidth,r:e.borderRadius||0,rotation:p,padding:e.padding,zIndex:1},a.objectEach(k,function(a,b){void 0===a&&delete k[b]});!l||g&&C(u)?g&&C(u)&&(l?k.text=u:(l=f.dataLabel=q[p?"text":"label"](u,0,-9999,e.shape,null,null,e.useHTML,null,"data-label"),l.addClass("highcharts-data-label-color-"+
f.colorIndex+" "+(e.className||"")+(e.useHTML?"highcharts-tracker":""))),l.attr(k),l.css(D).shadow(e.shadow),l.added||l.add(r),c.alignDataLabel(f,l,e,null,y)):(f.dataLabel=l=l.destroy(),v&&(f.connector=v.destroy()))})};r.prototype.alignDataLabel=function(a,b,c,d,h){var g=this.chart,f=g.inverted,l=n(a.plotX,-9999),p=n(a.plotY,-9999),m=b.getBBox(),u,v=c.rotation,r=c.align,t=this.visible&&(a.series.forceDL||g.isInsidePlot(l,Math.round(p),f)||d&&g.isInsidePlot(l,f?d.x+1:d.y+d.height-1,f)),q="justify"===
n(c.overflow,"justify");if(t&&(u=c.style.fontSize,u=g.renderer.fontMetrics(u,b).b,d=e({x:f?this.yAxis.len-p:l,y:Math.round(f?this.xAxis.len-l:p),width:0,height:0},d),e(c,{width:m.width,height:m.height}),v?(q=!1,l=g.renderer.rotCorr(u,v),l={x:d.x+c.x+d.width/2+l.x,y:d.y+c.y+{top:0,middle:.5,bottom:1}[c.verticalAlign]*d.height},b[h?"attr":"animate"](l).attr({align:r}),p=(v+720)%360,p=180<p&&360>p,"left"===r?l.y-=p?m.height:0:"center"===r?(l.x-=m.width/2,l.y-=m.height/2):"right"===r&&(l.x-=m.width,l.y-=
p?0:m.height)):(b.align(c,null,d),l=b.alignAttr),q?a.isLabelJustified=this.justifyDataLabel(b,c,l,m,d,h):n(c.crop,!0)&&(t=g.isInsidePlot(l.x,l.y)&&g.isInsidePlot(l.x+m.width,l.y+m.height)),c.shape&&!v))b[h?"attr":"animate"]({anchorX:f?g.plotWidth-a.plotY:a.plotX,anchorY:f?g.plotHeight-a.plotX:a.plotY});t||(b.attr({y:-9999}),b.placed=!1)};r.prototype.justifyDataLabel=function(a,b,c,d,e,h){var f=this.chart,g=b.align,l=b.verticalAlign,p,m,n=a.box?0:a.padding||0;p=c.x+n;0>p&&("right"===g?b.align="left":
b.x=-p,m=!0);p=c.x+d.width-n;p>f.plotWidth&&("left"===g?b.align="right":b.x=f.plotWidth-p,m=!0);p=c.y+n;0>p&&("bottom"===l?b.verticalAlign="top":b.y=-p,m=!0);p=c.y+d.height-n;p>f.plotHeight&&("top"===l?b.verticalAlign="bottom":b.y=f.plotHeight-p,m=!0);m&&(a.placed=!h,a.align(b,null,e));return m};m.pie&&(m.pie.prototype.drawDataLabels=function(){var c=this,b=c.data,d,e=c.chart,h=c.options.dataLabels,m=n(h.connectorPadding,10),f=n(h.connectorWidth,1),u=e.plotWidth,t=e.plotHeight,q,z=c.center,v=z[2]/
2,L=z[1],F,J,k,w,N=[[],[]],K,I,O,P,x=[0,0,0,0];c.visible&&(h.enabled||c._hasPointLabels)&&(A(b,function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),r.prototype.drawDataLabels.apply(c),A(b,function(a){a.dataLabel&&a.visible&&(N[a.half].push(a),a.dataLabel._pos=null)}),A(N,function(b,f){var g,l,p=b.length,r=[],q;if(p)for(c.sortByAngle(b,f-.5),0<c.maxLabelDistance&&(g=Math.max(0,L-v-c.maxLabelDistance),
l=Math.min(L+v+c.maxLabelDistance,e.plotHeight),A(b,function(a){0<a.labelDistance&&a.dataLabel&&(a.top=Math.max(0,L-v-a.labelDistance),a.bottom=Math.min(L+v+a.labelDistance,e.plotHeight),q=a.dataLabel.getBBox().height||21,a.positionsIndex=r.push({target:a.labelPos[1]-a.top+q/2,size:q,rank:a.y})-1)}),a.distribute(r,l+q-g)),P=0;P<p;P++)d=b[P],l=d.positionsIndex,k=d.labelPos,F=d.dataLabel,O=!1===d.visible?"hidden":"inherit",g=k[1],r&&C(r[l])?void 0===r[l].pos?O="hidden":(w=r[l].size,I=d.top+r[l].pos):
I=g,delete d.positionIndex,K=h.justify?z[0]+(f?-1:1)*(v+d.labelDistance):c.getX(I<d.top+2||I>d.bottom-2?g:I,f,d),F._attr={visibility:O,align:k[6]},F._pos={x:K+h.x+({left:m,right:-m}[k[6]]||0),y:I+h.y-10},k.x=K,k.y=I,n(h.crop,!0)&&(J=F.getBBox().width,g=null,K-J<m?(g=Math.round(J-K+m),x[3]=Math.max(g,x[3])):K+J>u-m&&(g=Math.round(K+J-u+m),x[1]=Math.max(g,x[1])),0>I-w/2?x[0]=Math.max(Math.round(-I+w/2),x[0]):I+w/2>t&&(x[2]=Math.max(Math.round(I+w/2-t),x[2])),F.sideOverflow=g)}),0===B(x)||this.verifyDataLabelOverflow(x))&&
(this.placeDataLabels(),f&&A(this.points,function(a){var b;q=a.connector;if((F=a.dataLabel)&&F._pos&&a.visible&&0<a.labelDistance){O=F._attr.visibility;if(b=!q)a.connector=q=e.renderer.path().addClass("highcharts-data-label-connector highcharts-color-"+a.colorIndex).add(c.dataLabelsGroup),q.attr({"stroke-width":f,stroke:h.connectorColor||a.color||"#666666"});q[b?"attr":"animate"]({d:c.connectorPath(a.labelPos)});q.attr("visibility",O)}else q&&(a.connector=q.destroy())}))},m.pie.prototype.connectorPath=
function(a){var b=a.x,c=a.y;return n(this.options.dataLabels.softConnector,!0)?["M",b+("left"===a[6]?5:-5),c,"C",b,c,2*a[2]-a[4],2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",b+("left"===a[6]?5:-5),c,"L",a[2],a[3],"L",a[4],a[5]]},m.pie.prototype.placeDataLabels=function(){A(this.points,function(a){var b=a.dataLabel;b&&a.visible&&((a=b._pos)?(b.sideOverflow&&(b._attr.width=b.getBBox().width-b.sideOverflow,b.css({width:b._attr.width+"px",textOverflow:"ellipsis"}),b.shortened=!0),b.attr(b._attr),b[b.moved?
"animate":"attr"](a),b.moved=!0):b&&b.attr({y:-9999}))},this)},m.pie.prototype.alignDataLabel=u,m.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,c=this.options,d=c.center,g=c.minSize||80,e,f=null!==c.size;f||(null!==d[0]?e=Math.max(b[2]-Math.max(a[1],a[3]),g):(e=Math.max(b[2]-a[1]-a[3],g),b[0]+=(a[3]-a[1])/2),null!==d[1]?e=Math.max(Math.min(e,b[2]-Math.max(a[0],a[2])),g):(e=Math.max(Math.min(e,b[2]-a[0]-a[2]),g),b[1]+=(a[0]-a[2])/2),e<b[2]?(b[2]=e,b[3]=Math.min(h(c.innerSize||
0,e),e),this.translate(b),this.drawDataLabels&&this.drawDataLabels()):f=!0);return f});m.column&&(m.column.prototype.alignDataLabel=function(a,b,c,d,e){var g=this.chart.inverted,f=a.series,h=a.dlBox||a.shapeArgs,l=n(a.below,a.plotY>n(this.translatedThreshold,f.yAxis.len)),m=n(c.inside,!!this.options.stacking);h&&(d=t(h),0>d.y&&(d.height+=d.y,d.y=0),h=d.y+d.height-f.yAxis.len,0<h&&(d.height-=h),g&&(d={x:f.yAxis.len-d.y-d.height,y:f.xAxis.len-d.x-d.width,width:d.height,height:d.width}),m||(g?(d.x+=
l?0:d.width,d.width=0):(d.y+=l?d.height:0,d.height=0)));c.align=n(c.align,!g||m?"center":l?"right":"left");c.verticalAlign=n(c.verticalAlign,g||m?"middle":l?"top":"bottom");r.prototype.alignDataLabel.call(this,a,b,c,d,e);a.isLabelJustified&&a.contrastColor&&a.dataLabel.css({color:a.contrastColor})})})(I);(function(a){var z=a.Chart,B=a.each,C=a.objectEach,A=a.pick,e=a.addEvent;z.prototype.callbacks.push(function(a){function d(){var d=[];B(a.yAxis||[],function(a){a.options.stackLabels&&!a.options.stackLabels.allowOverlap&&
C(a.stacks,function(a){C(a,function(a){d.push(a.label)})})});B(a.series||[],function(a){var e=a.options.dataLabels,h=a.dataLabelCollections||["dataLabel"];(e.enabled||a._hasPointLabels)&&!e.allowOverlap&&a.visible&&B(h,function(e){B(a.points,function(a){a[e]&&(a[e].labelrank=A(a.labelrank,a.shapeArgs&&a.shapeArgs.height),d.push(a[e]))})})});a.hideOverlappingLabels(d)}d();e(a,"redraw",d)});z.prototype.hideOverlappingLabels=function(a){var d=a.length,e,u,n,h,r,m,c,g,b,p=function(a,b,c,f,d,g,e,h){return!(d>
a+c||d+e<a||g>b+f||g+h<b)};for(u=0;u<d;u++)if(e=a[u])e.oldOpacity=e.opacity,e.newOpacity=1,e.width||(n=e.getBBox(),e.width=n.width,e.height=n.height);a.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(u=0;u<d;u++)for(n=a[u],e=u+1;e<d;++e)if(h=a[e],n&&h&&n!==h&&n.placed&&h.placed&&0!==n.newOpacity&&0!==h.newOpacity&&(r=n.alignAttr,m=h.alignAttr,c=n.parentGroup,g=h.parentGroup,b=2*(n.box?0:n.padding||0),r=p(r.x+c.translateX,r.y+c.translateY,n.width-b,n.height-b,m.x+g.translateX,m.y+
g.translateY,h.width-b,h.height-b)))(n.labelrank<h.labelrank?n:h).newOpacity=0;B(a,function(a){var b,c;a&&(c=a.newOpacity,a.oldOpacity!==c&&a.placed&&(c?a.show(!0):b=function(){a.hide()},a.alignAttr.opacity=c,a[a.isOld?"animate":"attr"](a.alignAttr,null,b)),a.isOld=!0)})}})(I);(function(a){var z=a.addEvent,B=a.Chart,C=a.createElement,A=a.css,e=a.defaultOptions,d=a.defaultPlotOptions,q=a.each,t=a.extend,u=a.fireEvent,n=a.hasTouch,h=a.inArray,r=a.isObject,m=a.Legend,c=a.merge,g=a.pick,b=a.Point,p=a.Series,
l=a.seriesTypes,E=a.svg,G;G=a.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart.pointer,c=function(a){var c=b.getPointFromEvent(a);void 0!==c&&(b.isDirectTouch=!0,c.onMouseOver(a))};q(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(q(a.trackerGroups,function(f){if(a[f]){a[f].addClass("highcharts-tracker").on("mouseover",c).on("mouseout",function(a){b.onTrackerMouseOut(a)});
if(n)a[f].on("touchstart",c);a.options.cursor&&a[f].css(A).css({cursor:a.options.cursor})}}),a._hasTracking=!0)},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),g=d.length,e=a.chart,h=e.pointer,l=e.renderer,m=e.options.tooltip.snap,k=a.tracker,p,u=function(){if(e.hoverSeries!==a)a.onMouseOver()},r="rgba(192,192,192,"+(E?.0001:.002)+")";if(g&&!c)for(p=g+1;p--;)"M"===d[p]&&d.splice(p+1,0,d[p+1]-m,d[p+2],"L"),(p&&"M"===d[p]||p===g)&&d.splice(p,
0,"L",d[p-2]+m,d[p-1]);k?k.attr({d:d}):a.graph&&(a.tracker=l.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:r,fill:c?r:"none","stroke-width":a.graph.strokeWidth()+(c?0:2*m),zIndex:2}).add(a.group),q([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",u).on("mouseout",function(a){h.onTrackerMouseOut(a)});b.cursor&&a.css({cursor:b.cursor});if(n)a.on("touchstart",u)}))}};l.column&&(l.column.prototype.drawTracker=G.drawTrackerPoint);
l.pie&&(l.pie.prototype.drawTracker=G.drawTrackerPoint);l.scatter&&(l.scatter.prototype.drawTracker=G.drawTrackerPoint);t(m.prototype,{setItemEvents:function(a,b,d){var f=this,g=f.chart.renderer.boxWrapper,e="highcharts-legend-"+(a.series?"point":"series")+"-active";(d?b:a.legendGroup).on("mouseover",function(){a.setState("hover");g.addClass(e);b.css(f.options.itemHoverStyle)}).on("mouseout",function(){b.css(c(a.visible?f.itemStyle:f.itemHiddenStyle));g.removeClass(e);a.setState()}).on("click",function(b){var c=
function(){a.setVisible&&a.setVisible()};b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):u(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=C("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);z(a.checkbox,"click",function(b){u(a.series||a,"checkboxClick",{checked:b.target.checked,item:a},function(){a.select()})})}});e.legend.itemStyle.cursor="pointer";t(B.prototype,{showResetZoom:function(){var a=
this,b=e.lang,c=a.options.chart.resetZoomButton,d=c.theme,g=d.states,h="chart"===c.relativeTo?null:"plotBox";this.resetZoomButton=a.renderer.button(b.resetZoom,null,null,function(){a.zoomOut()},d,g&&g.hover).attr({align:c.position.align,title:b.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(c.position,!1,h)},zoomOut:function(){var a=this;u(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var b,c=this.pointer,f=!1,d;!a||a.resetSelection?(q(this.axes,function(a){b=
a.zoom()}),c.initiated=!1):q(a.xAxis.concat(a.yAxis),function(a){var d=a.axis;c[d.isXAxis?"zoomX":"zoomY"]&&(b=d.zoom(a.min,a.max),d.displayBtn&&(f=!0))});d=this.resetZoomButton;f&&!d?this.showResetZoom():!f&&r(d)&&(this.resetZoomButton=d.destroy());b&&this.redraw(g(this.options.chart.animation,a&&a.animation,100>this.pointCount))},pan:function(a,b){var c=this,f=c.hoverPoints,d;f&&q(f,function(a){a.setState()});q("xy"===b?[1,0]:[1],function(b){b=c[b?"xAxis":"yAxis"][0];var f=b.horiz,g=a[f?"chartX":
"chartY"],f=f?"mouseDownX":"mouseDownY",e=c[f],k=(b.pointRange||0)/2,h=b.getExtremes(),l=b.toValue(e-g,!0)+k,k=b.toValue(e+b.len-g,!0)-k,m=k<l,e=m?k:l,l=m?l:k,k=Math.min(h.dataMin,b.toValue(b.toPixels(h.min)-b.minPixelPadding)),m=Math.max(h.dataMax,b.toValue(b.toPixels(h.max)+b.minPixelPadding)),p;p=k-e;0<p&&(l+=p,e=k);p=l-m;0<p&&(l=m,e-=p);b.series.length&&e!==h.min&&l!==h.max&&(b.setExtremes(e,l,!1,!1,{trigger:"pan"}),d=!0);c[f]=g});d&&c.redraw(!1);A(c.container,{cursor:"move"})}});t(b.prototype,
{select:function(a,b){var c=this,f=c.series,d=f.chart;a=g(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=c.options.selected=a;f.options.data[h(c,f.data)]=c.options;c.setState(a&&"select");b||q(d.getSelectedPoints(),function(a){a.selected&&a!==c&&(a.selected=a.options.selected=!1,f.options.data[h(a,f.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a){var b=this.series.chart,c=b.pointer;a=a?c.normalize(a):c.getChartCoordinatesFromPoint(this,
b.inverted);c.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");q(a.hoverPoints||[],function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var b=this,d=c(b.series.options.point,b.options).events;b.events=d;a.objectEach(d,function(a,c){z(b,c,a)});this.hasImportedEvents=!0}},setState:function(a,b){var c=Math.floor(this.plotX),f=this.plotY,e=this.series,h=e.options.states[a]||{},l=d[e.type].marker&&
e.options.marker,m=l&&!1===l.enabled,p=l&&l.states&&l.states[a]||{},k=!1===p.enabled,n=e.stateMarkerGraphic,u=this.marker||{},r=e.chart,q=e.halo,A,z=l&&e.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===h.enabled||a&&(k||m&&!1===p.enabled)||a&&u.states&&u.states[a]&&!1===u.states[a].enabled)){z&&(A=e.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.animate(e.pointAttribs(this,
a),g(r.options.chart.animation,h.animation)),A&&this.graphic.animate(A,g(r.options.chart.animation,p.animation,l.animation)),n&&n.hide();else{if(a&&p){l=u.symbol||e.symbol;n&&n.currentSymbol!==l&&(n=n.destroy());if(n)n[b?"animate":"attr"]({x:A.x,y:A.y});else l&&(e.stateMarkerGraphic=n=r.renderer.symbol(l,A.x,A.y,A.width,A.height).add(e.markerGroup),n.currentSymbol=l);n&&n.attr(e.pointAttribs(this,a))}n&&(n[a&&r.isInsidePlot(c,f,r.inverted)?"show":"hide"](),n.element.point=this)}(c=h.halo)&&c.size?
(q||(e.halo=q=r.renderer.path().add((this.graphic||n).parentGroup)),q[b?"animate":"attr"]({d:this.haloPath(c.size)}),q.attr({"class":"highcharts-halo highcharts-color-"+g(this.colorIndex,e.colorIndex)}),q.point=this,q.attr(t({fill:this.color||e.color,"fill-opacity":c.opacity,zIndex:-1},c.attributes))):q&&q.point&&q.point.haloPath&&q.animate({d:q.point.haloPath(0)});this.state=a}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});
t(p.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&u(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=this.chart,c=b.tooltip,d=b.hoverPoint;b.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&u(this,"mouseOut");!c||this.stickyTracking||c.shared&&!this.noSharedTooltip||c.hide();this.setState()},setState:function(a){var b=this,c=b.options,d=b.graph,f=c.states,
e=c.lineWidth,c=0;a=a||"";if(b.state!==a&&(q([b.group,b.markerGroup,b.dataLabelsGroup],function(c){c&&(b.state&&c.removeClass("highcharts-series-"+b.state),a&&c.addClass("highcharts-series-"+a))}),b.state=a,!f[a]||!1!==f[a].enabled)&&(a&&(e=f[a].lineWidth||e+(f[a].lineWidthPlus||0)),d&&!d.dashstyle))for(e={"stroke-width":e},d.animate(e,g(b.chart.options.chart.animation,f[a]&&f[a].animation));b["zone-graph-"+c];)b["zone-graph-"+c].attr(e),c+=1},setVisible:function(a,b){var c=this,d=c.chart,f=c.legendItem,
g,e=d.options.chart.ignoreHiddenSeries,h=c.visible;g=(c.visible=a=c.options.visible=c.userOptions.visible=void 0===a?!h:a)?"show":"hide";q(["group","dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(c[a])c[a][g]()});if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();f&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&q(d.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});q(c.linkedSeries,function(b){b.setVisible(a,!1)});e&&(d.isDirtyBox=
!0);!1!==b&&d.redraw();u(c,g)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);u(this,a?"select":"unselect")},drawTracker:G.drawTrackerGraph})})(I);(function(a){var z=a.Chart,B=a.each,C=a.inArray,A=a.isArray,e=a.isObject,d=a.pick,q=a.splat;z.prototype.setResponsive=function(d){var e=this.options.responsive,n=[],h=this.currentResponsive;e&&e.rules&&B(e.rules,function(e){void 0===
e._id&&(e._id=a.uniqueKey());this.matchResponsiveRule(e,n,d)},this);var r=a.merge.apply(0,a.map(n,function(d){return a.find(e.rules,function(a){return a._id===d}).chartOptions})),n=n.toString()||void 0;n!==(h&&h.ruleIds)&&(h&&this.update(h.undoOptions,d),n?(this.currentResponsive={ruleIds:n,mergedOptions:r,undoOptions:this.currentOptions(r)},this.update(r,d)):this.currentResponsive=void 0)};z.prototype.matchResponsiveRule=function(a,e){var n=a.condition;(n.callback||function(){return this.chartWidth<=
d(n.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=d(n.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=d(n.minWidth,0)&&this.chartHeight>=d(n.minHeight,0)}).call(this)&&e.push(a._id)};z.prototype.currentOptions=function(d){function u(d,n,m,c){var g;a.objectEach(d,function(a,h){if(!c&&-1<C(h,["series","xAxis","yAxis"]))for(d[h]=q(d[h]),m[h]=[],g=0;g<d[h].length;g++)n[h][g]&&(m[h][g]={},u(a[g],n[h][g],m[h][g],c+1));else e(a)?(m[h]=A(a)?[]:{},u(a,n[h]||{},m[h],c+1)):m[h]=n[h]||null})}var n={};u(d,this.options,
n,0);return n}})(I);(function(a){var z=a.Axis,B=a.each,C=a.pick;a=a.wrap;a(z.prototype,"getSeriesExtremes",function(a){var e=this.isXAxis,d,q,t=[],u;e&&B(this.series,function(a,d){a.useMapGeometry&&(t[d]=a.xData,a.xData=[])});a.call(this);e&&(d=C(this.dataMin,Number.MAX_VALUE),q=C(this.dataMax,-Number.MAX_VALUE),B(this.series,function(a,e){a.useMapGeometry&&(d=Math.min(d,C(a.minX,d)),q=Math.max(q,C(a.maxX,q)),a.xData=t[e],u=!0)}),u&&(this.dataMin=d,this.dataMax=q))});a(z.prototype,"setAxisTranslation",
function(a){var e=this.chart,d=e.plotWidth/e.plotHeight,e=e.xAxis[0],q;a.call(this);"yAxis"===this.coll&&void 0!==e.transA&&B(this.series,function(a){a.preserveAspectRatio&&(q=!0)});if(q&&(this.transA=e.transA=Math.min(this.transA,e.transA),a=d/((e.max-e.min)/(this.max-this.min)),a=1>a?this:e,d=(a.max-a.min)*a.transA,a.pixelPadding=a.len-d,a.minPixelPadding=a.pixelPadding/2,d=a.fixTo)){d=d[1]-a.toValue(d[0],!0);d*=a.transA;if(Math.abs(d)>a.minPixelPadding||a.min===a.dataMin&&a.max===a.dataMax)d=0;
a.minPixelPadding-=d}});a(z.prototype,"render",function(a){a.call(this);this.fixTo=null})})(I);(function(a){var z=a.Axis,B=a.Chart,C=a.color,A,e=a.each,d=a.extend,q=a.isNumber,t=a.Legend,u=a.LegendSymbolMixin,n=a.noop,h=a.merge,r=a.pick,m=a.wrap;A=a.ColorAxis=function(){this.init.apply(this,arguments)};d(A.prototype,z.prototype);d(A.prototype,{defaultColorAxisOptions:{lineWidth:0,minPadding:0,maxPadding:0,gridLineWidth:1,tickPixelInterval:72,startOnTick:!0,endOnTick:!0,offset:0,marker:{animation:{duration:50},
width:.01,color:"#999999"},labels:{overflow:"justify",rotation:0},minColor:"#e6ebf5",maxColor:"#003399",tickLength:5,showInLegend:!0},keepProps:["legendGroup","legendItemHeight","legendItemWidth","legendItem","legendSymbol"].concat(z.prototype.keepProps),init:function(a,d){var b="vertical"!==a.options.legend.layout,c;this.coll="colorAxis";c=h(this.defaultColorAxisOptions,{side:b?2:1,reversed:!b},d,{opposite:!b,showEmpty:!1,title:null});z.prototype.init.call(this,a,c);d.dataClasses&&this.initDataClasses(d);
this.initStops();this.horiz=b;this.zoomEnabled=!1;this.defaultLegendLength=200},initDataClasses:function(a){var c=this.chart,b,d=0,l=c.options.chart.colorCount,m=this.options,n=a.dataClasses.length;this.dataClasses=b=[];this.legendItems=[];e(a.dataClasses,function(a,g){a=h(a);b.push(a);a.color||("category"===m.dataClassColor?(g=c.options.colors,l=g.length,a.color=g[d],a.colorIndex=d,d++,d===l&&(d=0)):a.color=C(m.minColor).tweenTo(C(m.maxColor),2>n?.5:g/(n-1)))})},setTickPositions:function(){if(!this.dataClasses)return z.prototype.setTickPositions.call(this)},
initStops:function(){this.stops=this.options.stops||[[0,this.options.minColor],[1,this.options.maxColor]];e(this.stops,function(a){a.color=C(a[1])})},setOptions:function(a){z.prototype.setOptions.call(this,a);this.options.crosshair=this.options.marker},setAxisSize:function(){var a=this.legendSymbol,d=this.chart,b=d.options.legend||{},e,h;a?(this.left=b=a.attr("x"),this.top=e=a.attr("y"),this.width=h=a.attr("width"),this.height=a=a.attr("height"),this.right=d.chartWidth-b-h,this.bottom=d.chartHeight-
e-a,this.len=this.horiz?h:a,this.pos=this.horiz?b:e):this.len=(this.horiz?b.symbolWidth:b.symbolHeight)||this.defaultLegendLength},normalizedValue:function(a){this.isLog&&(a=this.val2lin(a));return 1-(this.max-a)/(this.max-this.min||1)},toColor:function(a,d){var b=this.stops,c,e,g=this.dataClasses,h,f;if(g)for(f=g.length;f--;){if(h=g[f],c=h.from,b=h.to,(void 0===c||a>=c)&&(void 0===b||a<=b)){e=h.color;d&&(d.dataClass=f,d.colorIndex=h.colorIndex);break}}else{a=this.normalizedValue(a);for(f=b.length;f--&&
!(a>b[f][0]););c=b[f]||b[f+1];b=b[f+1]||c;a=1-(b[0]-a)/(b[0]-c[0]||1);e=c.color.tweenTo(b.color,a)}return e},getOffset:function(){var a=this.legendGroup,d=this.chart.axisOffset[this.side];a&&(this.axisParent=a,z.prototype.getOffset.call(this),this.added||(this.added=!0,this.labelLeft=0,this.labelRight=this.width),this.chart.axisOffset[this.side]=d)},setLegendColor:function(){var a,d=this.reversed;a=d?1:0;d=d?0:1;a=this.horiz?[a,0,d,0]:[0,d,0,a];this.legendColor={linearGradient:{x1:a[0],y1:a[1],x2:a[2],
y2:a[3]},stops:this.stops}},drawLegendSymbol:function(a,d){var b=a.padding,c=a.options,e=this.horiz,g=r(c.symbolWidth,e?this.defaultLegendLength:12),h=r(c.symbolHeight,e?12:this.defaultLegendLength),f=r(c.labelPadding,e?16:30),c=r(c.itemDistance,10);this.setLegendColor();d.legendSymbol=this.chart.renderer.rect(0,a.baseline-11,g,h).attr({zIndex:1}).add(d.legendGroup);this.legendItemWidth=g+b+(e?c:f);this.legendItemHeight=h+b+(e?f:0)},setState:n,visible:!0,setVisible:n,getSeriesExtremes:function(){var a=
this.series,d=a.length;this.dataMin=Infinity;for(this.dataMax=-Infinity;d--;)void 0!==a[d].valueMin&&(this.dataMin=Math.min(this.dataMin,a[d].valueMin),this.dataMax=Math.max(this.dataMax,a[d].valueMax))},drawCrosshair:function(a,d){var b=d&&d.plotX,c=d&&d.plotY,e,g=this.pos,h=this.len;d&&(e=this.toPixels(d[d.series.colorKey]),e<g?e=g-2:e>g+h&&(e=g+h+2),d.plotX=e,d.plotY=this.len-e,z.prototype.drawCrosshair.call(this,a,d),d.plotX=b,d.plotY=c,this.cross&&(this.cross.addClass("highcharts-coloraxis-marker").add(this.legendGroup),
this.cross.attr({fill:this.crosshair.color})))},getPlotLinePath:function(a,d,b,e,h){return q(h)?this.horiz?["M",h-4,this.top-6,"L",h+4,this.top-6,h,this.top,"Z"]:["M",this.left,h,"L",this.left-6,h+6,this.left-6,h-6,"Z"]:z.prototype.getPlotLinePath.call(this,a,d,b,e)},update:function(a,d){var b=this.chart,c=b.legend;e(this.series,function(a){a.isDirtyData=!0});a.dataClasses&&c.allItems&&(e(c.allItems,function(a){a.isDataClass&&a.legendGroup&&a.legendGroup.destroy()}),b.isDirtyLegend=!0);b.options[this.coll]=
h(this.userOptions,a);z.prototype.update.call(this,a,d);this.legendItem&&(this.setLegendColor(),c.colorizeItem(this,!0))},remove:function(){this.legendItem&&this.chart.legend.destroyItem(this);z.prototype.remove.call(this)},getDataClassLegendSymbols:function(){var c=this,g=this.chart,b=this.legendItems,h=g.options.legend,l=h.valueDecimals,m=h.valueSuffix||"",r;b.length||e(this.dataClasses,function(f,h){var p=!0,t=f.from,q=f.to;r="";void 0===t?r="\x3c ":void 0===q&&(r="\x3e ");void 0!==t&&(r+=a.numberFormat(t,
l)+m);void 0!==t&&void 0!==q&&(r+=" - ");void 0!==q&&(r+=a.numberFormat(q,l)+m);b.push(d({chart:g,name:r,options:{},drawLegendSymbol:u.drawRectangle,visible:!0,setState:n,isDataClass:!0,setVisible:function(){p=this.visible=!p;e(c.series,function(a){e(a.points,function(a){a.dataClass===h&&a.setVisible(p)})});g.legend.colorizeItem(this,p)}},f))});return b},name:""});e(["fill","stroke"],function(c){a.Fx.prototype[c+"Setter"]=function(){this.elem.attr(c,C(this.start).tweenTo(C(this.end),this.pos),null,
!0)}});m(B.prototype,"getAxes",function(a){var c=this.options.colorAxis;a.call(this);this.colorAxis=[];c&&new A(this,c)});m(t.prototype,"getAllItems",function(a){var c=[],b=this.chart.colorAxis[0];b&&b.options&&(b.options.showInLegend&&(b.options.dataClasses?c=c.concat(b.getDataClassLegendSymbols()):c.push(b)),e(b.series,function(a){a.options.showInLegend=!1}));return c.concat(a.call(this))});m(t.prototype,"colorizeItem",function(a,d,b){a.call(this,d,b);b&&d.legendColor&&d.legendSymbol.attr({fill:d.legendColor})});
m(t.prototype,"update",function(a){a.apply(this,[].slice.call(arguments,1));this.chart.colorAxis[0]&&this.chart.colorAxis[0].update({},arguments[2])})})(I);(function(a){var z=a.defined,B=a.each,C=a.noop,A=a.seriesTypes;a.colorPointMixin={isValid:function(){return null!==this.value},setVisible:function(a){var d=this,e=a?"show":"hide";B(["graphic","dataLabel"],function(a){if(d[a])d[a][e]()})},setState:function(e){a.Point.prototype.setState.call(this,e);this.graphic&&this.graphic.attr({zIndex:"hover"===
e?1:0})}};a.colorSeriesMixin={pointArrayMap:["value"],axisTypes:["xAxis","yAxis","colorAxis"],optionalAxis:"colorAxis",trackerGroups:["group","markerGroup","dataLabelsGroup"],getSymbol:C,parallelArrays:["x","y","value"],colorKey:"value",pointAttribs:A.column.prototype.pointAttribs,translateColors:function(){var a=this,d=this.options.nullColor,q=this.colorAxis,t=this.colorKey;B(this.data,function(e){var n=e[t];if(n=e.options.color||(e.isNull?d:q&&void 0!==n?q.toColor(n,e):e.color||a.color))e.color=
n})},colorAttribs:function(a){var d={};z(a.color)&&(d[this.colorProp||"fill"]=a.color);return d}}})(I);(function(a){function z(a){a&&(a.preventDefault&&a.preventDefault(),a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)}function B(a){this.init(a)}var C=a.addEvent,A=a.Chart,e=a.doc,d=a.each,q=a.extend,t=a.merge,u=a.pick,n=a.wrap;B.prototype.init=function(a){this.chart=a;a.mapNavButtons=[]};B.prototype.update=function(d){var e=this.chart,h=e.options.mapNavigation,c,g,b,n,l,A=function(a){this.handler.call(e,
a);z(a)},B=e.mapNavButtons;d&&(h=e.options.mapNavigation=t(e.options.mapNavigation,d));for(;B.length;)B.pop().destroy();u(h.enableButtons,h.enabled)&&!e.renderer.forExport&&a.objectEach(h.buttons,function(a,d){c=t(h.buttonOptions,a);g=c.theme;g.style=t(c.theme.style,c.style);n=(b=g.states)&&b.hover;l=b&&b.select;a=e.renderer.button(c.text,0,0,A,g,n,l,0,"zoomIn"===d?"topbutton":"bottombutton").addClass("highcharts-map-navigation").attr({width:c.width,height:c.height,title:e.options.lang[d],padding:c.padding,
zIndex:5}).add();a.handler=c.onclick;a.align(q(c,{width:a.width,height:2*a.height}),null,c.alignTo);C(a.element,"dblclick",z);B.push(a)});this.updateEvents(h)};B.prototype.updateEvents=function(a){var d=this.chart;u(a.enableDoubleClickZoom,a.enabled)||a.enableDoubleClickZoomTo?this.unbindDblClick=this.unbindDblClick||C(d.container,"dblclick",function(a){d.pointer.onContainerDblClick(a)}):this.unbindDblClick&&(this.unbindDblClick=this.unbindDblClick());u(a.enableMouseWheelZoom,a.enabled)?this.unbindMouseWheel=
this.unbindMouseWheel||C(d.container,void 0===e.onmousewheel?"DOMMouseScroll":"mousewheel",function(a){d.pointer.onContainerMouseWheel(a);z(a);return!1}):this.unbindMouseWheel&&(this.unbindMouseWheel=this.unbindMouseWheel())};q(A.prototype,{fitToBox:function(a,e){d([["x","width"],["y","height"]],function(d){var c=d[0];d=d[1];a[c]+a[d]>e[c]+e[d]&&(a[d]>e[d]?(a[d]=e[d],a[c]=e[c]):a[c]=e[c]+e[d]-a[d]);a[d]>e[d]&&(a[d]=e[d]);a[c]<e[c]&&(a[c]=e[c])});return a},mapZoom:function(a,d,e,c,g){var b=this.xAxis[0],
h=b.max-b.min,l=u(d,b.min+h/2),m=h*a,h=this.yAxis[0],n=h.max-h.min,f=u(e,h.min+n/2),n=n*a,l=this.fitToBox({x:l-m*(c?(c-b.pos)/b.len:.5),y:f-n*(g?(g-h.pos)/h.len:.5),width:m,height:n},{x:b.dataMin,y:h.dataMin,width:b.dataMax-b.dataMin,height:h.dataMax-h.dataMin}),m=l.x<=b.dataMin&&l.width>=b.dataMax-b.dataMin&&l.y<=h.dataMin&&l.height>=h.dataMax-h.dataMin;c&&(b.fixTo=[c-b.pos,d]);g&&(h.fixTo=[g-h.pos,e]);void 0===a||m?(b.setExtremes(void 0,void 0,!1),h.setExtremes(void 0,void 0,!1)):(b.setExtremes(l.x,
l.x+l.width,!1),h.setExtremes(l.y,l.y+l.height,!1));this.redraw()}});n(A.prototype,"render",function(a){this.mapNavigation=new B(this);this.mapNavigation.update();a.call(this)})})(I);(function(a){var z=a.extend,B=a.pick,C=a.Pointer;a=a.wrap;z(C.prototype,{onContainerDblClick:function(a){var e=this.chart;a=this.normalize(a);e.options.mapNavigation.enableDoubleClickZoomTo?e.pointer.inClass(a.target,"highcharts-tracker")&&e.hoverPoint&&e.hoverPoint.zoomTo():e.isInsidePlot(a.chartX-e.plotLeft,a.chartY-
e.plotTop)&&e.mapZoom(.5,e.xAxis[0].toValue(a.chartX),e.yAxis[0].toValue(a.chartY),a.chartX,a.chartY)},onContainerMouseWheel:function(a){var e=this.chart,d;a=this.normalize(a);d=a.detail||-(a.wheelDelta/120);e.isInsidePlot(a.chartX-e.plotLeft,a.chartY-e.plotTop)&&e.mapZoom(Math.pow(e.options.mapNavigation.mouseWheelSensitivity,d),e.xAxis[0].toValue(a.chartX),e.yAxis[0].toValue(a.chartY),a.chartX,a.chartY)}});a(C.prototype,"zoomOption",function(a){var e=this.chart.options.mapNavigation;B(e.enableTouchZoom,
e.enabled)&&(this.chart.options.chart.pinchType="xy");a.apply(this,[].slice.call(arguments,1))});a(C.prototype,"pinchTranslate",function(a,e,d,q,t,u,n){a.call(this,e,d,q,t,u,n);"map"===this.chart.options.chart.type&&this.hasZoom&&(a=q.scaleX>q.scaleY,this.pinchTranslateDirection(!a,e,d,q,t,u,n,a?q.scaleX:q.scaleY))})})(I);(function(a){var z=a.colorPointMixin,B=a.each,C=a.extend,A=a.isNumber,e=a.map,d=a.merge,q=a.noop,t=a.pick,u=a.isArray,n=a.Point,h=a.Series,r=a.seriesType,m=a.seriesTypes,c=a.splat,
g=void 0!==a.doc.documentElement.style.vectorEffect;r("map","scatter",{allAreas:!0,animation:!1,nullColor:"#f7f7f7",borderColor:"#cccccc",borderWidth:1,marker:null,stickyTracking:!1,joinBy:"hc-key",dataLabels:{formatter:function(){return this.point.value},inside:!0,verticalAlign:"middle",crop:!1,overflow:!1,padding:0},turboThreshold:0,tooltip:{followPointer:!0,pointFormat:"{point.name}: {point.value}\x3cbr/\x3e"},states:{normal:{animation:!0},hover:{brightness:.2,halo:null},select:{color:"#cccccc"}}},
d(a.colorSeriesMixin,{type:"map",getExtremesFromAll:!0,useMapGeometry:!0,forceDL:!0,searchPoint:q,directTouch:!0,preserveAspectRatio:!0,pointArrayMap:["value"],getBox:function(b){var c=Number.MAX_VALUE,d=-c,e=c,g=-c,f=c,h=c,m=this.xAxis,n=this.yAxis,u;B(b||[],function(b){if(b.path){"string"===typeof b.path&&(b.path=a.splitPath(b.path));var l=b.path||[],m=l.length,n=!1,k=-c,p=c,r=-c,q=c,v=b.properties;if(!b._foundBox){for(;m--;)A(l[m])&&(n?(k=Math.max(k,l[m]),p=Math.min(p,l[m])):(r=Math.max(r,l[m]),
q=Math.min(q,l[m])),n=!n);b._midX=p+(k-p)*t(b.middleX,v&&v["hc-middle-x"],.5);b._midY=q+(r-q)*t(b.middleY,v&&v["hc-middle-y"],.5);b._maxX=k;b._minX=p;b._maxY=r;b._minY=q;b.labelrank=t(b.labelrank,(k-p)*(r-q));b._foundBox=!0}d=Math.max(d,b._maxX);e=Math.min(e,b._minX);g=Math.max(g,b._maxY);f=Math.min(f,b._minY);h=Math.min(b._maxX-b._minX,b._maxY-b._minY,h);u=!0}});u&&(this.minY=Math.min(f,t(this.minY,c)),this.maxY=Math.max(g,t(this.maxY,-c)),this.minX=Math.min(e,t(this.minX,c)),this.maxX=Math.max(d,
t(this.maxX,-c)),m&&void 0===m.options.minRange&&(m.minRange=Math.min(5*h,(this.maxX-this.minX)/5,m.minRange||c)),n&&void 0===n.options.minRange&&(n.minRange=Math.min(5*h,(this.maxY-this.minY)/5,n.minRange||c)))},getExtremes:function(){h.prototype.getExtremes.call(this,this.valueData);this.chart.hasRendered&&this.isDirtyData&&this.getBox(this.options.data);this.valueMin=this.dataMin;this.valueMax=this.dataMax;this.dataMin=this.minY;this.dataMax=this.maxY},translatePath:function(a){var b=!1,c=this.xAxis,
d=this.yAxis,e=c.min,f=c.transA,c=c.minPixelPadding,g=d.min,h=d.transA,d=d.minPixelPadding,m,n=[];if(a)for(m=a.length;m--;)A(a[m])?(n[m]=b?(a[m]-e)*f+c:(a[m]-g)*h+d,b=!b):n[m]=a[m];return n},setData:function(b,g,l,m){var n=this.options,f=this.chart.options.chart,p=f&&f.map,r=n.mapData,q=n.joinBy,t=null===q,v=n.keys||this.pointArrayMap,z=[],C={},E=this.chart.mapTransforms;!r&&p&&(r="string"===typeof p?a.maps[p]:p);t&&(q="_i");q=this.joinBy=c(q);q[1]||(q[1]=q[0]);b&&B(b,function(a,c){var d=0;if(A(a))b[c]=
{value:a};else if(u(a)){b[c]={};!n.keys&&a.length>v.length&&"string"===typeof a[0]&&(b[c]["hc-key"]=a[0],++d);for(var f=0;f<v.length;++f,++d)v[f]&&(b[c][v[f]]=a[d])}t&&(b[c]._i=c)});this.getBox(b);(this.chart.mapTransforms=E=f&&f.mapTransforms||r&&r["hc-transform"]||E)&&a.objectEach(E,function(a){a.rotation&&(a.cosAngle=Math.cos(a.rotation),a.sinAngle=Math.sin(a.rotation))});if(r){"FeatureCollection"===r.type&&(this.mapTitle=r.title,r=a.geojson(r,this.type,this));this.mapData=r;this.mapMap={};for(E=
0;E<r.length;E++)f=r[E],p=f.properties,f._i=E,q[0]&&p&&p[q[0]]&&(f[q[0]]=p[q[0]]),C[f[q[0]]]=f;this.mapMap=C;b&&q[1]&&B(b,function(a){C[a[q[1]]]&&z.push(C[a[q[1]]])});n.allAreas?(this.getBox(r),b=b||[],q[1]&&B(b,function(a){z.push(a[q[1]])}),z="|"+e(z,function(a){return a&&a[q[0]]}).join("|")+"|",B(r,function(a){q[0]&&-1!==z.indexOf("|"+a[q[0]]+"|")||(b.push(d(a,{value:null})),m=!1)})):this.getBox(z)}h.prototype.setData.call(this,b,g,l,m)},drawGraph:q,drawDataLabels:q,doFullTranslate:function(){return this.isDirtyData||
this.chart.isResizing||this.chart.renderer.isVML||!this.baseTrans},translate:function(){var a=this,c=a.xAxis,d=a.yAxis,e=a.doFullTranslate();a.generatePoints();B(a.data,function(b){b.plotX=c.toPixels(b._midX,!0);b.plotY=d.toPixels(b._midY,!0);e&&(b.shapeType="path",b.shapeArgs={d:a.translatePath(b.path)})});a.translateColors()},pointAttribs:function(a,c){a=m.column.prototype.pointAttribs.call(this,a,c);g?a["vector-effect"]="non-scaling-stroke":a["stroke-width"]="inherit";return a},drawPoints:function(){var a=
this,c=a.xAxis,d=a.yAxis,e=a.group,h=a.chart,f=h.renderer,n,u,r,q,t=this.baseTrans,z,C,A,k,w;a.transformGroup||(a.transformGroup=f.g().attr({scaleX:1,scaleY:1}).add(e),a.transformGroup.survive=!0);a.doFullTranslate()?(h.hasRendered&&B(a.points,function(b){b.shapeArgs&&(b.shapeArgs.fill=a.pointAttribs(b,b.state).fill)}),a.group=a.transformGroup,m.column.prototype.drawPoints.apply(a),a.group=e,B(a.points,function(a){a.graphic&&(a.name&&a.graphic.addClass("highcharts-name-"+a.name.replace(/ /g,"-").toLowerCase()),
a.properties&&a.properties["hc-key"]&&a.graphic.addClass("highcharts-key-"+a.properties["hc-key"].toLowerCase()))}),this.baseTrans={originX:c.min-c.minPixelPadding/c.transA,originY:d.min-d.minPixelPadding/d.transA+(d.reversed?0:d.len/d.transA),transAX:c.transA,transAY:d.transA},this.transformGroup.animate({translateX:0,translateY:0,scaleX:1,scaleY:1})):(n=c.transA/t.transAX,u=d.transA/t.transAY,r=c.toPixels(t.originX,!0),q=d.toPixels(t.originY,!0),.99<n&&1.01>n&&.99<u&&1.01>u&&(u=n=1,r=Math.round(r),
q=Math.round(q)),z=this.transformGroup,h.renderer.globalAnimation?(C=z.attr("translateX"),A=z.attr("translateY"),k=z.attr("scaleX"),w=z.attr("scaleY"),z.attr({animator:0}).animate({animator:1},{step:function(a,b){z.attr({translateX:C+(r-C)*b.pos,translateY:A+(q-A)*b.pos,scaleX:k+(n-k)*b.pos,scaleY:w+(u-w)*b.pos})}})):z.attr({translateX:r,translateY:q,scaleX:n,scaleY:u}));g||a.group.element.setAttribute("stroke-width",a.options[a.pointAttrToOptions&&a.pointAttrToOptions["stroke-width"]||"borderWidth"]/
(n||1));this.drawMapDataLabels()},drawMapDataLabels:function(){h.prototype.drawDataLabels.call(this);this.dataLabelsGroup&&this.dataLabelsGroup.clip(this.chart.clipRect)},render:function(){var a=this,c=h.prototype.render;a.chart.renderer.isVML&&3E3<a.data.length?setTimeout(function(){c.call(a)}):c.call(a)},animate:function(a){var b=this.options.animation,c=this.group,d=this.xAxis,e=this.yAxis,f=d.pos,g=e.pos;this.chart.renderer.isSVG&&(!0===b&&(b={duration:1E3}),a?c.attr({translateX:f+d.len/2,translateY:g+
e.len/2,scaleX:.001,scaleY:.001}):(c.animate({translateX:f,translateY:g,scaleX:1,scaleY:1},b),this.animate=null))},animateDrilldown:function(a){var b=this.chart.plotBox,c=this.chart.drilldownLevels[this.chart.drilldownLevels.length-1],d=c.bBox,e=this.chart.options.drilldown.animation;a||(a=Math.min(d.width/b.width,d.height/b.height),c.shapeArgs={scaleX:a,scaleY:a,translateX:d.x,translateY:d.y},B(this.points,function(a){a.graphic&&a.graphic.attr(c.shapeArgs).animate({scaleX:1,scaleY:1,translateX:0,
translateY:0},e)}),this.animate=null)},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,animateDrillupFrom:function(a){m.column.prototype.animateDrillupFrom.call(this,a)},animateDrillupTo:function(a){m.column.prototype.animateDrillupTo.call(this,a)}}),C({applyOptions:function(a,c){a=n.prototype.applyOptions.call(this,a,c);c=this.series;var b=c.joinBy;c.mapData&&((b=void 0!==a[b[1]]&&c.mapMap[a[b[1]]])?(c.xyFromShape&&(a.x=b._midX,a.y=b._midY),C(a,b)):a.value=a.value||null);return a},onMouseOver:function(a){clearTimeout(this.colorInterval);
if(null!==this.value||this.series.options.nullInteraction)n.prototype.onMouseOver.call(this,a);else this.series.onMouseOut(a)},zoomTo:function(){var a=this.series;a.xAxis.setExtremes(this._minX,this._maxX,!1);a.yAxis.setExtremes(this._minY,this._maxY,!1);a.chart.redraw()}},z))})(I);(function(a){var z=a.seriesType,B=a.seriesTypes;z("mapline","map",{lineWidth:1,fillColor:"none"},{type:"mapline",colorProp:"stroke",pointAttrToOptions:{stroke:"color","stroke-width":"lineWidth"},pointAttribs:function(a,
z){a=B.map.prototype.pointAttribs.call(this,a,z);a.fill=this.options.fillColor;return a},drawLegendSymbol:B.line.prototype.drawLegendSymbol})})(I);(function(a){var z=a.merge,B=a.Point;a=a.seriesType;a("mappoint","scatter",{dataLabels:{enabled:!0,formatter:function(){return this.point.name},crop:!1,defer:!1,overflow:!1,style:{color:"#000000"}}},{type:"mappoint",forceDL:!0},{applyOptions:function(a,A){a=void 0!==a.lat&&void 0!==a.lon?z(a,this.series.chart.fromLatLonToPoint(a)):a;return B.prototype.applyOptions.call(this,
a,A)}})})(I);(function(a){var z=a.arrayMax,B=a.arrayMin,C=a.Axis,A=a.color,e=a.each,d=a.isNumber,q=a.noop,t=a.pick,u=a.pInt,n=a.Point,h=a.Series,r=a.seriesType,m=a.seriesTypes;r("bubble","scatter",{dataLabels:{formatter:function(){return this.point.z},inside:!0,verticalAlign:"middle"},marker:{lineColor:null,lineWidth:1,radius:null,states:{hover:{radiusPlus:0}},symbol:"circle"},minSize:8,maxSize:"20%",softThreshold:!1,states:{hover:{halo:{size:5}}},tooltip:{pointFormat:"({point.x}, {point.y}), Size: {point.z}"},
turboThreshold:0,zThreshold:0,zoneAxis:"z"},{pointArrayMap:["y","z"],parallelArrays:["x","y","z"],trackerGroups:["group","dataLabelsGroup"],specialGroup:"group",bubblePadding:!0,zoneAxis:"z",directTouch:!0,pointAttribs:function(a,d){var b=t(this.options.marker.fillOpacity,.5);a=h.prototype.pointAttribs.call(this,a,d);1!==b&&(a.fill=A(a.fill).setOpacity(b).get("rgba"));return a},getRadii:function(a,d,b,e){var c,g,h,f=this.zData,m=[],n=this.options,p="width"!==n.sizeBy,u=n.zThreshold,r=d-a;g=0;for(c=
f.length;g<c;g++)h=f[g],n.sizeByAbsoluteValue&&null!==h&&(h=Math.abs(h-u),d=Math.max(d-u,Math.abs(a-u)),a=0),null===h?h=null:h<a?h=b/2-1:(h=0<r?(h-a)/r:.5,p&&0<=h&&(h=Math.sqrt(h)),h=Math.ceil(b+h*(e-b))/2),m.push(h);this.radii=m},animate:function(a){var c=this.options.animation;a||(e(this.points,function(a){var b=a.graphic,d;b&&b.width&&(d={x:b.x,y:b.y,width:b.width,height:b.height},b.attr({x:a.plotX,y:a.plotY,width:1,height:1}),b.animate(d,c))}),this.animate=null)},translate:function(){var c,e=
this.data,b,h,l=this.radii;m.scatter.prototype.translate.call(this);for(c=e.length;c--;)b=e[c],h=l?l[c]:0,d(h)&&h>=this.minPxSize/2?(b.marker=a.extend(b.marker,{radius:h,width:2*h,height:2*h}),b.dlBox={x:b.plotX-h,y:b.plotY-h,width:2*h,height:2*h}):b.shapeArgs=b.plotY=b.dlBox=void 0},alignDataLabel:m.column.prototype.alignDataLabel,buildKDTree:q,applyZones:q},{haloPath:function(a){return n.prototype.haloPath.call(this,0===a?0:(this.marker?this.marker.radius||0:0)+a)},ttBelow:!1});C.prototype.beforePadding=
function(){var a=this,g=this.len,b=this.chart,h=0,l=g,n=this.isXAxis,m=n?"xData":"yData",f=this.min,r={},q=Math.min(b.plotWidth,b.plotHeight),y=Number.MAX_VALUE,A=-Number.MAX_VALUE,v=this.max-f,C=g/v,F=[];e(this.series,function(c){var d=c.options;!c.bubblePadding||!c.visible&&b.options.chart.ignoreHiddenSeries||(a.allowZoomOutside=!0,F.push(c),n&&(e(["minSize","maxSize"],function(a){var b=d[a],c=/%$/.test(b),b=u(b);r[a]=c?q*b/100:b}),c.minPxSize=r.minSize,c.maxPxSize=Math.max(r.maxSize,r.minSize),
c=c.zData,c.length&&(y=t(d.zMin,Math.min(y,Math.max(B(c),!1===d.displayNegative?d.zThreshold:-Number.MAX_VALUE))),A=t(d.zMax,Math.max(A,z(c))))))});e(F,function(b){var c=b[m],e=c.length,g;n&&b.getRadii(y,A,b.minPxSize,b.maxPxSize);if(0<v)for(;e--;)d(c[e])&&a.dataMin<=c[e]&&c[e]<=a.dataMax&&(g=b.radii[e],h=Math.min((c[e]-f)*C-g,h),l=Math.max((c[e]-f)*C+g,l))});F.length&&0<v&&!this.isLog&&(l-=g,C*=(g+h-l)/g,e([["min","userMin",h],["max","userMax",l]],function(b){void 0===t(a.options[b[0]],a[b[1]])&&
(a[b[0]]+=b[2]/C)}))}})(I);(function(a){var z=a.merge,B=a.Point,C=a.seriesType,A=a.seriesTypes;A.bubble&&C("mapbubble","bubble",{animationLimit:500,tooltip:{pointFormat:"{point.name}: {point.z}"}},{xyFromShape:!0,type:"mapbubble",pointArrayMap:["z"],getMapData:A.map.prototype.getMapData,getBox:A.map.prototype.getBox,setData:A.map.prototype.setData},{applyOptions:function(a,d){return a&&void 0!==a.lat&&void 0!==a.lon?B.prototype.applyOptions.call(this,z(a,this.series.chart.fromLatLonToPoint(a)),d):
A.map.prototype.pointClass.prototype.applyOptions.call(this,a,d)},ttBelow:!1})})(I);(function(a){var z=a.colorPointMixin,B=a.each,C=a.merge,A=a.noop,e=a.pick,d=a.Series,q=a.seriesType,t=a.seriesTypes;q("heatmap","scatter",{animation:!1,borderWidth:0,nullColor:"#f7f7f7",dataLabels:{formatter:function(){return this.point.value},inside:!0,verticalAlign:"middle",crop:!1,overflow:!1,padding:0},marker:null,pointRange:null,tooltip:{pointFormat:"{point.x}, {point.y}: {point.value}\x3cbr/\x3e"},states:{normal:{animation:!0},
hover:{halo:!1,brightness:.2}}},C(a.colorSeriesMixin,{pointArrayMap:["y","value"],hasPointSpecificOptions:!0,getExtremesFromAll:!0,directTouch:!0,init:function(){var a;t.scatter.prototype.init.apply(this,arguments);a=this.options;a.pointRange=e(a.pointRange,a.colsize||1);this.yAxis.axisPointRange=a.rowsize||1},translate:function(){var a=this.options,d=this.xAxis,e=this.yAxis,r=function(a,c,d){return Math.min(Math.max(c,a),d)};this.generatePoints();B(this.points,function(h){var c=(a.colsize||1)/2,
g=(a.rowsize||1)/2,b=r(Math.round(d.len-d.translate(h.x-c,0,1,0,1)),-d.len,2*d.len),c=r(Math.round(d.len-d.translate(h.x+c,0,1,0,1)),-d.len,2*d.len),n=r(Math.round(e.translate(h.y-g,0,1,0,1)),-e.len,2*e.len),g=r(Math.round(e.translate(h.y+g,0,1,0,1)),-e.len,2*e.len);h.plotX=h.clientX=(b+c)/2;h.plotY=(n+g)/2;h.shapeType="rect";h.shapeArgs={x:Math.min(b,c),y:Math.min(n,g),width:Math.abs(c-b),height:Math.abs(g-n)}});this.translateColors()},drawPoints:function(){t.column.prototype.drawPoints.call(this);
B(this.points,function(a){a.graphic.attr(this.colorAttribs(a))},this)},animate:A,getBox:A,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,alignDataLabel:t.column.prototype.alignDataLabel,getExtremes:function(){d.prototype.getExtremes.call(this,this.valueData);this.valueMin=this.dataMin;this.valueMax=this.dataMax;d.prototype.getExtremes.call(this)}}),z)})(I);(function(a){function z(a,d){var e,n,m,c=!1,g=a.x,b=a.y;a=0;for(e=d.length-1;a<d.length;e=a++)n=d[a][1]>b,m=d[e][1]>b,n!==m&&g<(d[e][0]-d[a][0])*
(b-d[a][1])/(d[e][1]-d[a][1])+d[a][0]&&(c=!c);return c}var B=a.Chart,C=a.each,A=a.extend,e=a.format,d=a.merge,q=a.win,t=a.wrap;B.prototype.transformFromLatLon=function(d,e){if(void 0===q.proj4)return a.error(21),{x:0,y:null};d=q.proj4(e.crs,[d.lon,d.lat]);var h=e.cosAngle||e.rotation&&Math.cos(e.rotation),n=e.sinAngle||e.rotation&&Math.sin(e.rotation);d=e.rotation?[d[0]*h+d[1]*n,-d[0]*n+d[1]*h]:d;return{x:((d[0]-(e.xoffset||0))*(e.scale||1)+(e.xpan||0))*(e.jsonres||1)+(e.jsonmarginX||0),y:(((e.yoffset||
0)-d[1])*(e.scale||1)+(e.ypan||0))*(e.jsonres||1)-(e.jsonmarginY||0)}};B.prototype.transformToLatLon=function(d,e){if(void 0===q.proj4)a.error(21);else{d={x:((d.x-(e.jsonmarginX||0))/(e.jsonres||1)-(e.xpan||0))/(e.scale||1)+(e.xoffset||0),y:((-d.y-(e.jsonmarginY||0))/(e.jsonres||1)+(e.ypan||0))/(e.scale||1)+(e.yoffset||0)};var h=e.cosAngle||e.rotation&&Math.cos(e.rotation),n=e.sinAngle||e.rotation&&Math.sin(e.rotation);e=q.proj4(e.crs,"WGS84",e.rotation?{x:d.x*h+d.y*-n,y:d.x*n+d.y*h}:d);return{lat:e.y,
lon:e.x}}};B.prototype.fromPointToLatLon=function(d){var e=this.mapTransforms,h;if(e){for(h in e)if(e.hasOwnProperty(h)&&e[h].hitZone&&z({x:d.x,y:-d.y},e[h].hitZone.coordinates[0]))return this.transformToLatLon(d,e[h]);return this.transformToLatLon(d,e["default"])}a.error(22)};B.prototype.fromLatLonToPoint=function(d){var e=this.mapTransforms,h,r;if(!e)return a.error(22),{x:0,y:null};for(h in e)if(e.hasOwnProperty(h)&&e[h].hitZone&&(r=this.transformFromLatLon(d,e[h]),z({x:r.x,y:-r.y},e[h].hitZone.coordinates[0])))return r;
return this.transformFromLatLon(d,e["default"])};a.geojson=function(a,d,h){var n=[],m=[],c=function(a){var b,c=a.length;m.push("M");for(b=0;b<c;b++)1===b&&m.push("L"),m.push(a[b][0],-a[b][1])};d=d||"map";C(a.features,function(a){var b=a.geometry,e=b.type,b=b.coordinates;a=a.properties;var g;m=[];"map"===d||"mapbubble"===d?("Polygon"===e?(C(b,c),m.push("Z")):"MultiPolygon"===e&&(C(b,function(a){C(a,c)}),m.push("Z")),m.length&&(g={path:m})):"mapline"===d?("LineString"===e?c(b):"MultiLineString"===e&&
C(b,c),m.length&&(g={path:m})):"mappoint"===d&&"Point"===e&&(g={x:b[0],y:-b[1]});g&&n.push(A(g,{name:a.name||a.NAME,properties:a}))});h&&a.copyrightShort&&(h.chart.mapCredits=e(h.chart.options.credits.mapText,{geojson:a}),h.chart.mapCreditsFull=e(h.chart.options.credits.mapTextFull,{geojson:a}));return n};t(B.prototype,"addCredits",function(a,e){e=d(!0,this.options.credits,e);this.mapCredits&&(e.href=null);a.call(this,e);this.credits&&this.mapCreditsFull&&this.credits.attr({title:this.mapCreditsFull})})})(I);
(function(a){function z(a,d,e,c,g,b,n,l){return["M",a+g,d,"L",a+e-b,d,"C",a+e-b/2,d,a+e,d+b/2,a+e,d+b,"L",a+e,d+c-n,"C",a+e,d+c-n/2,a+e-n/2,d+c,a+e-n,d+c,"L",a+l,d+c,"C",a+l/2,d+c,a,d+c-l/2,a,d+c-l,"L",a,d+g,"C",a,d+g/2,a+g/2,d,a+g,d,"Z"]}var B=a.Chart,C=a.defaultOptions,A=a.each,e=a.extend,d=a.merge,q=a.pick,t=a.Renderer,u=a.SVGRenderer,n=a.VMLRenderer;e(C.lang,{zoomIn:"Zoom in",zoomOut:"Zoom out"});C.mapNavigation={buttonOptions:{alignTo:"plotBox",align:"left",verticalAlign:"top",x:0,width:18,height:18,
padding:5,style:{fontSize:"15px",fontWeight:"bold"},theme:{"stroke-width":1,"text-align":"center"}},buttons:{zoomIn:{onclick:function(){this.mapZoom(.5)},text:"+",y:0},zoomOut:{onclick:function(){this.mapZoom(2)},text:"-",y:28}},mouseWheelSensitivity:1.1};a.splitPath=function(a){var d;a=a.replace(/([A-Za-z])/g," $1 ");a=a.replace(/^\s*/,"").replace(/\s*$/,"");a=a.split(/[ ,]+/);for(d=0;d<a.length;d++)/[a-zA-Z]/.test(a[d])||(a[d]=parseFloat(a[d]));return a};a.maps={};u.prototype.symbols.topbutton=
function(a,d,e,c,g){return z(a-1,d-1,e,c,g.r,g.r,0,0)};u.prototype.symbols.bottombutton=function(a,d,e,c,g){return z(a-1,d-1,e,c,0,0,g.r,g.r)};t===n&&A(["topbutton","bottombutton"],function(a){n.prototype.symbols[a]=u.prototype.symbols[a]});a.Map=a.mapChart=function(e,n,m){var c="string"===typeof e||e.nodeName,g=arguments[c?1:0],b={endOnTick:!1,visible:!1,minPadding:0,maxPadding:0,startOnTick:!1},h,l=a.getOptions().credits;h=g.series;g.series=null;g=d({chart:{panning:"xy",type:"map"},credits:{mapText:q(l.mapText,
' \u00a9 \x3ca href\x3d"{geojson.copyrightUrl}"\x3e{geojson.copyrightShort}\x3c/a\x3e'),mapTextFull:q(l.mapTextFull,"{geojson.copyright}")},tooltip:{followTouchMove:!1},xAxis:b,yAxis:d(b,{reversed:!0})},g,{chart:{inverted:!1,alignTicks:!1}});g.series=h;return c?new B(e,g,m):new B(g,n)}})(I);return I});


/***/ }),
/* 8 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
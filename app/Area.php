<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $table = 'area';
    public $timestamps = true;
    public $fillable = [ 'city_id', 'name', 'created_at', 'updated_at' ];

    public function city()
    {
    	return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'id', 'sitio_id');
    }
}

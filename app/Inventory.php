<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    public $table = 'inventory';
    public $timestamps = true;
    public $fillable = [
    	'city_id', 'area_id', 'mangrove_id',
    	'record_date', 
        'number_of_segment', 'number_of_tree',
    	'diameter', 'height','crown', 
        'seedling', 'sapling',
    	'created_at', 'updated_at' 
    ];

    public function city()
    {
    	return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function area()
    {
    	return $this->hasOne('App\Area', 'id', 'area_id');
    }

    public function mangrove()
    {
    	return $this->hasOne('App\Mangrove', 'id', 'mangrove_id');
    }

    public function setRecordDateAttribute($value)
    {
    	$this->attributes['record_date'] = date('Y-m-d', strtotime($value));
    }

    public function getMeterAttribute()
    {
        $centimeter = $this->diameter;
        $meter = $centimeter/100;

        return $meter;
    }

    public function getBasalAreaAttribute()
    {
        $centimeter = $this->diameter;
        $meter = $centimeter/100;

        return pow($meter, 2) * 0.7854;
    }

    public function getCrownCoverAttribute()
    {
        $height = $this->height;
        $crown = $this->crown;
        $crown_cover = $height * $crown;

        return $crown_cover;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Inventory;

class Mangrove extends Model
{
    public $table = 'mangrove';
    public $timestamps = true;
    public $fillable = [ 'name', 'scientific_name', 'details', 'image', 'created_at', 'updated_at' ];

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'id', 'mangrove_id');
    }

    public function total_segment($city_id, $area_id, $date_from, $date_to)
    {
    	$mangrove_id =  $this->id;
    	$count = Inventory::whereCityId($city_id)->whereAreaId($area_id)->whereMangroveId($mangrove_id)->where(function($query) use ($date_from, $date_to){
    		$query->where('record_date', '>=', $date_from)->where('record_date', '<=', $date_to);
    	})->count();
    	
    	return $count;
    }

    public function total_tree($city_id, $area_id, $date_from, $date_to)
    {
    	$mangrove_id =  $this->id;
    	$count = Inventory::whereCityId($city_id)->whereAreaId($area_id)->whereMangroveId($mangrove_id)->where(function($query) use ($date_from, $date_to){
    		$query->where('record_date', '>=', $date_from)->where('record_date', '<=', $date_to);
    	})->sum('number_of_tree');
    	
    	return $count;
    }

    public function total_basal_area($city_id, $area_id, $date_from, $date_to)
    {
        $total = 0;
        $mangrove_id =  $this->id;

    	$rows = Inventory::whereCityId($city_id)->whereAreaId($area_id)->whereMangroveId($mangrove_id)->where(function($query) use ($date_from, $date_to){
    		$query->where('record_date', '>=', $date_from)->where('record_date', '<=', $date_to);
    	})->get();

        foreach ($rows as $row)
        {
            $total += $row->basal_area;
        }
        
        return $total;
    }

    public function total_crown_cover($city_id, $date_from, $date_to)
    {
        $total = 0;
        $mangrove_id =  $this->id;

        $rows = Inventory::whereCityId($city_id)->whereMangroveId($mangrove_id)->where(function($query) use ($date_from, $date_to){
            $query->where('record_date', '>=', $date_from)->where('record_date', '<=', $date_to);
        })->get();

        foreach ($rows as $row)
        {
            $total += $row->crown_cover;
        }
        
        return $total;
    }

    public function frequency($total_segment)
    {
    	$frequency = ($total_segment / 4) * 100;
    	return $frequency;
    }

    public function abundance($total_tree)
    {
    	return $total_tree/0.04;
    }

    public function basal_area($total_basal_area)
    {
    	return $total_basal_area/0.04;
    }

    public function total_canopy_cover($city_id, $area_id, $from, $to)
    {
        $total_crown_cover = 0;
        $mangrove_id = $this->id;

        $rows = Inventory::whereCityId($city_id)->whereAreaId($area_id)->whereMangroveId($mangrove_id)->where(function($query) use ($from, $to){
            $query->where('record_date', '>=', $from)->where('record_date', '<=', $to);
        })->get();

        foreach ($rows as $row)
        {
            $total_crown_cover += $row->crown_cover;
        }

        return $total_crown_cover;
    }
}

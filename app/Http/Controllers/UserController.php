<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        
        if($request->has('search') && $request->search != '')
        {
            $search = trim($request->search);
            $rows = User::where('username', 'LIKE', "%$search%")->orWhere('name', 'LIKE', "%$search%")->orderBy('name', 'ASC')->paginate(10);
        }
        else
        {
            $rows = User::orderBy('name', 'ASC')->paginate(10);
        }

        $data = compact('rows', 'search');

    	return view('pemo.user.index', $data);
    }
    
    public function create()
    {
    	$action = route('user_store');
    	$username = old('username');
    	$name = old('name');

    	$data = compact('action', 'username', 'name');

    	return view('pemo.user.form', $data);
    }
    
    public function store(Request $request)
    {
    	$this->validate(
    		$request, 
    		[
    			'username' => 'required|unique:users,username|max:255',
    			'name' => 'required|unique:users,name|max:255'
    		]
    	);

    	$input = $request->only('username', 'name');
    	$input['password'] = bcrypt('password');

    	User::create($input);

    	return redirect()->route('user_create')->with('success', 'User Successfully added!');
    }
    
    public function edit($id)
    {
    	$row = User::whereId($id)->first();
        $action = route('user_update', $id);
        $username = $row->username;
        $name = $row->name;

        $data = compact('action', 'id', 'username', 'name');

    	return view('pemo.user.form', $data);
    }
    
    public function update($id, Request $request)
    {
        $this->validate(
            $request, 
            [
                'username' => 'required|unique:users,username,' . $id . ',id|max:255',
                'name' => 'required|unique:users,name,' . $id . ',id|max:255'
            ]
        );

        User::whereId($id)->update($request->only('username', 'name'));

        return redirect()->route('user_edit', $id)->with('success', 'User Successfully updated!');
    }

    public function generate_password(Request $request, $id)
    {
    	$password = str_random(8);
        User::whereId($id)->update([ 'password'=>bcrypt($password) ]);

        return response()->json([ 'password'=>$password ]);
    }

    public function reset_password()
    {
    	$action = route('user_reset_password_submit');
    	$password = old('password');
    	$confirm_password = old('confirm_password');

    	$data = compact('action', 'password', 'confirm_password');

    	return view('pemo.user.reset-password', $data);
    }

    public function reset_password_submit(Request $request)
    {
        $this->validate(
            $request, 
            [
                'password' => 'required|size:8|max:255',
                'confirm_password' => 'same:password'
            ]
        );

        $id = Auth::user()->id;
        #echo $id;dd($request->all());

        User::whereId($id)->update([ 'password'=>bcrypt($request->password) ]);

        return redirect()->route('user_reset_password')->with('success', 'Password Successfully changed!');
    }
}

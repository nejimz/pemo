<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Area;
use App\Mangrove;
use App\Inventory;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        
        if($request->has('search') && $request->search != '')
        {
            $search = trim($request->search);
            $rows = Inventory::with(['area', 'city', 'mangrove'])
            ->where(function($query) use ($search){
                $query->whereHas('area', function($query) use ($search){
                    $query->where('name', 'LIKE', "%$search%");
                });
            })
            ->orWhere(function($query) use ($search){
                $query->whereHas('city', function($query) use ($search){
                    $query->where('name', 'LIKE', "%$search%");
                });
            })
            ->orWhere(function($query) use ($search){
                $query->whereHas('mangrove', function($query) use ($search){
                    $query->where('name', 'LIKE', "%$search%")->orWhere('scientific_name', 'LIKE', "%$search%");
                });
            })
            ->orderBy('created_at', 'ASC')
            ->paginate(10);
            #->toSql();dd($rows);
        }
        else
        {
            $rows = Inventory::orderBy('created_at', 'ASC')->paginate(10);
        }

        $data = compact('rows', 'search');

        return view('pemo.inventory.index', $data);
    }
    
    public function create()
    {
        $action = route('inventory_store');

        $city_id = old('city_id');
        $area_id = old('area_id');
        $mangrove_id = old('mangrove_id');
        $record_date = old('record_date');
        $number_of_segment = old('number_of_segment');
        $number_of_tree = old('number_of_tree');
        $diameter = old('diameter');
        $height = old('height');
        $crown = old('crown');
        $seedling = old('seedling');
        $sapling = old('sapling');


        $cities = City::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('name', 'ASC')->get();
        $mangroves = Mangrove::orderBy('name', 'ASC')->get();

        $data = compact('action', 
            'city_id', 'area_id', 'mangrove_id',
            'record_date', 'number_of_segment', 'number_of_tree',
            'diameter', 'height','crown', 
            'seedling', 'sapling',

            'cities', 'areas', 'mangroves'
        );

        return view('pemo.inventory.form', $data);
    }
    
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'city_id' => 'required|exists:city,id',
                'area_id' => 'required|exists:area,id',
                'mangrove_id' => 'required|exists:mangrove,id',

                'record_date' => 'required',
                'number_of_segment' => 'required|numeric',
                'number_of_tree' => 'required|numeric',
                'diameter' => 'required|numeric',
                'height' => 'required|numeric',
                'crown' => 'required|numeric',
                'seedling' => 'required|numeric',
                'sapling' => 'required|numeric'
            ]
        );

        $input = $request->only('city_id', 'area_id', 'mangrove_id',
            'record_date', 'number_of_segment', 'number_of_tree',
            'diameter', 'height', 'crown', 'seedling', 'sapling');

        Inventory::create($input);

        return redirect()->route('inventory_create')->with('success', 'Mangrove Data Successfully added!');
    }
    
    public function edit($id)
    {
        $action = route('inventory_update', $id);

        $row = Inventory::whereId($id)->first();
        $city_id = $row->city_id;
        $area_id = $row->area_id;
        $mangrove_id = $row->mangrove_id;
        $record_date = $row->record_date;
        $number_of_segment = $row->number_of_segment;
        $number_of_tree = $row->number_of_tree;
        $diameter = $row->diameter;
        $height = $row->height;
        $crown = $row->crown;
        $seedling = $row->seedling;
        $sapling = $row->sapling;


        $cities = City::orderBy('name', 'ASC')->get();
        $areas = Area::orderBy('name', 'ASC')->get();
        $mangroves = Mangrove::orderBy('name', 'ASC')->get();

        $data = compact('action', 
            'city_id', 'area_id', 'mangrove_id',
            'record_date', 'number_of_segment', 'number_of_tree',
            'diameter', 'height','crown', 
            'seedling', 'sapling',

            'cities', 'areas', 'mangroves'
        );

        return view('pemo.inventory.form', $data);
    }
    
    public function update($id, Request $request)
    {
        $this->validate(
            $request, 
            [
                'city_id' => 'required|exists:city,id',
                'area_id' => 'required|exists:area,id',
                'mangrove_id' => 'required|exists:mangrove,id',

                'record_date' => 'required',
                'number_of_segment' => 'required|numeric',
                'number_of_tree' => 'required|numeric',
                'diameter' => 'required|numeric',
                'height' => 'required|numeric',
                'crown' => 'required|numeric',
                'seedling' => 'required|numeric',
                'sapling' => 'required|numeric'
            ]
        );

        $input = $request->only('city_id', 'area_id', 'mangrove_id',
            'record_date', 'number_of_segment', 'number_of_tree',
            'diameter', 'height', 'crown', 'seedling', 'sapling');

        Inventory::where('id', $id)->update($input);

        return redirect()->route('inventory_create')->with('success', 'Mangrove Data Successfully added!');
    }
}

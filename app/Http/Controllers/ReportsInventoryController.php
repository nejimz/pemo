<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Area;
use App\Inventory;
use App\Mangrove;

class ReportsInventoryController extends Controller
{
    public function index()
    {
        $action = route('report_inventory_generate');

        $date_from = old('date_from');
        $date_to = old('date_to');

        $cities = City::get();

        $data = compact('action', 'date_from', 'date_to', 'cities');

        return view('pemo.reports.inventory.index', $data);
    }
    
    public function generate_inventory(Request $request)
    {
        $city_id = $request->city_id;
        $area_id = $request->area_id;
        $beneficiary = $request->beneficiary;
        $date_from = date('Y-m-d', strtotime($request->date_from));
        $date_to = date('Y-m-d', strtotime($request->date_to));

    	$area = Area::whereId($request->area_id)->first();
    	$rows = Inventory::whereCityId($city_id)->whereAreaId($area_id)
    			->where(function($query) use ($date_from, $date_to){
    				$query->where('record_date', '>=', $date_from)->where('record_date', '<=', $date_to);
    			})
    			->orderBy('record_date', 'ASC')->get();

        $data = compact('area', 'beneficiary', 'date_from', 'date_to', 'rows');

        return view('pemo.reports.inventory.inventory-generate', $data);
    }

    public function summary_index()
    {
        $action = route('report_summary_generate');

        $date_from = old('date_from');
        $date_to = old('date_to');

        $cities = City::get();

        $data = compact('action', 'date_from', 'date_to', 'cities');

        return view('pemo.reports.inventory.summary-index', $data);
    }
    
    public function generate_inventory_summary(Request $request)
    {
        $city_id = $request->city_id;
        $area_id = $request->area_id;
        $date_from = date('Y-m-d', strtotime($request->date_from));
        $date_to = date('Y-m-d', strtotime($request->date_to));

        $area = Area::whereId($request->area_id)->first();
        $rows = Mangrove::orderBy('name', 'ASC')->get();

        $data = compact('area', 'area_id', 'city_id', 'date_from', 'date_to', 'rows');

        return view('pemo.reports.inventory.summary-generate', $data);
    }

    public function inventory_count()
    {
        $action = route('report_inventory_count_generate');

        $date_from = old('date_from');
        $date_to = old('date_to');

        $cities = City::get();

        $data = compact('action', 'date_from', 'date_to', 'cities');

        return view('pemo.reports.inventory.inventory-count', $data);
    }

    public function generate_inventory_count(Request $request)
    {
        $city_id = $request->city_id;
        $date_from = date('Y-m-d', strtotime($request->date_from));
        $date_to = date('Y-m-d', strtotime($request->date_to));

        $area = Area::whereCityId($request->city_id)->first();
        $rows = Mangrove::orderBy('name', 'ASC')->get();

        $data = compact('area', 'city_id', 'date_from', 'date_to', 'rows');

        return view('pemo.reports.inventory.count-generate', $data);
    }
}

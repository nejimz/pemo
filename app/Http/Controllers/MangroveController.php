<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mangrove;
use Image;

class MangroveController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        
        if($request->has('search') && $request->search != '')
        {
            $search = trim($request->search);
            $rows = Mangrove::where('name', 'LIKE', "%$search%")->orWhere('scientific_name', 'LIKE', "%$search%")->orderBy('name', 'ASC')->paginate(10);
        }
        else
        {
            $rows = Mangrove::orderBy('name', 'ASC')->paginate(10);
        }

        $data = compact('rows', 'search');

        return view('pemo.mangrove.index', $data);
    }
    
    public function create()
    {
        $action = route('mangrove_store');
        $name = old('name');
        $scientific_name = old('scientific_name');
        $details = old('details');

        $data = compact('action', 'name', 'scientific_name', 'details');

        return view('pemo.mangrove.form', $data);
    }
    
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required|unique:mangrove,name|max:255',
                'scientific_name' => 'required|unique:mangrove,scientific_name|max:255',
                'image' => 'image'
            ]
        );
        $input = $request->only('name', 'scientific_name');
        $input['details'] = (is_null($request->details))? '' : $request->details;
        $input['image'] = '';

        if($request->hasFile('image'))
        {
            $input['image'] = $this->upload_image($request);
        }

        Mangrove::create($input);

        return redirect()->route('mangrove_create')->with('success', 'Mangrove Successfully added!');
    }

    public function upload_image($request)
    {
        $image          = $request->image;
        $extension      = $image->getClientOriginalExtension();
        $size           = $image->getClientSize();
        $file_name      = str_random(20) . '.' . $extension;
        $file_location  = 'img/mangrove/' . $file_name;
        Image::make($image->getRealPath())->save($file_location);
        #Image::make($file->getRealPath())->resize(190, 190)->save($file_location);
        return $file_location;
    }
    
    public function edit($id)
    {
        $row = Mangrove::whereId($id)->first();
        $action = route('mangrove_update', $id);
        $name = $row->name;
        $scientific_name = $row->scientific_name;
        $details = $row->details;

        $data = compact('action', 'name', 'scientific_name', 'details');

        return view('pemo.mangrove.form', $data);
    }
    
    public function update($id, Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required|unique:mangrove,name,' . $id . ',id|max:255',
                'name' => 'required|unique:mangrove,scientific_name,' . $id . ',id|max:255'
            ]
        );
        
        $input = $request->only('name', 'scientific_name');
        $input['details'] = (is_null($request->details))? '' : $request->details;
        $input['image'] = '';

        if($request->hasFile('image'))
        {
            $input['image'] = $this->upload_image($request);
        }

        Mangrove::whereId($id)->update($input);

        return redirect()->route('mangrove_edit', $id)->with('success', 'Mangrove Successfully updated!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Mangrove;
use App\Inventory;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        
        if($request->has('search') && $request->search != '')
        {
            $search = trim($request->search);
            $rows = City::where('name', 'LIKE', "%$search%")->orderBy('name', 'ASC')->paginate(10);
        }
        else
        {
            $rows = City::orderBy('name', 'ASC')->paginate(10);
        }

        $data = compact('rows', 'search');

    	return view('pemo.city.index', $data);
    }
    
    public function create()
    {
    	$action = route('city_store');
    	$name = old('name');

    	$data = compact('action', 'name');

    	return view('pemo.city.form', $data);
    }
    
    public function store(Request $request)
    {
    	$this->validate(
    		$request, 
    		[
    			'name' => 'required|unique:city|max:255'
    		]
    	);

    	City::create($request->only('name'));

    	return redirect()->route('city_create')->with('success', 'City Successfully added!');
    }
    
    public function edit($id)
    {
    	$row = City::whereId($id)->first();
        $action = route('city_update', $id);
        $name = $row->name;

        $data = compact('action', 'name');

    	return view('pemo.city.form', $data);
    }
    
    public function update($id, Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required|unique:city,name,' . $id . ',id|max:255'
            ]
        );

        City::whereId($id)->update($request->only('name'));

        return redirect()->route('city_edit', $id)->with('success', 'City Successfully updated!');
    }

    public function summary(Request $request)
    {
        $data = [];
        $counter = 0;

        $cities = City::orderBy('name', 'ASC')->get();

        foreach ($cities as $city)
        {
            $data[$counter]['name'] = $city->name;

            $mangroves = Mangrove::get();
            $total = 0;
            $format = '';

            foreach ($mangroves as $mangrove)
            {
                $count = Inventory::whereCityId($city->id)->whereMangroveId($mangrove->id)->whereRaw('YEAR(record_date)=YEAR(NOW())')->count();
                #$count = rand(0, 100);
                $data[$counter]['value'][$mangrove->id] = [$mangrove->name, $count];
                $total += $count;
            }
            $data[$counter]['value'][0] = ['Total', $total];

            $counter++;
        }

        return response()->json($data);
    }
}

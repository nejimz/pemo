<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use URL;
use App\User;

class DashboardController extends Controller
{
    public function login()
    {
        if(Auth::check())
        {
            return redirect(URL::previous());
        }

        $action = route('authenticate');
        $username = old('username');
        $password = old('password');

    	$data = compact('action', 'username', 'password');

    	return view('pemo.login', $data);
    }

    public function authenticate(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        if (Auth::attempt(['username' => $username, 'password' => $password]))
        {
            return redirect()->route('dashboard');
        }

        return redirect()->route('login')->with('custom_error', 'Username and Password does not exists!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function dashboard()
    {
    	$test = 1;

    	$data = compact('test');

    	return view('pemo.dashboard', $data);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Area;

class AreaController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        
        if($request->has('search') && $request->search != '')
        {
            $search = trim($request->search);
            $rows = Area::where('name', 'LIKE', "%$search%")->orderBy('city_id', 'ASC')->orderBy('name', 'ASC')->paginate(10);
        }
        else
        {
            $rows = Area::orderBy('city_id', 'ASC')->orderBy('name', 'ASC')->paginate(10);
        }

        $data = compact('rows', 'search');

        return view('pemo.city.area.index', $data);
    }
    
    public function create()
    {
        $action = route('area_store');
        $name = old('name');
        $city_id = old('city_id');
        $cities = City::get();

        $data = compact('action', 'name', 'city_id', 'cities');

        return view('pemo.city.area.form', $data);
    }
    
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'city_id' => 'required|exists:city,id',
                'name' => 'required|unique:area|max:255'
            ]
        );

        Area::create($request->only('city_id', 'name'));

        return redirect()->route('area_create')->with('success', 'Area Successfully added!');
    }
    
    public function edit($id)
    {
        $row = Area::whereId($id)->first();
        $action = route('area_update', $id);
        $name = $row->name;

        $data = compact('action', 'name');

        return view('pemo.city.area.form', $data);
    }
    
    public function update($id, Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required|unique:area,name,' . $id . ',id|max:255'
            ]
        );

        Area::whereId($id)->update($request->only('name'));

        return redirect()->route('area_edit', $id)->with('success', 'Area Successfully updated!');
    }

    public function xhr(Request $request)
    {
        $rows = Area::whereCityId($request->city_id)->orderBy('name', 'ASC')->pluck('name', 'id');

        return response()->json($rows);
    }
}

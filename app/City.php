<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'city';
    public $timestamps = true;
    public $fillable = [ 'name', 'created_at', 'updated_at' ];

    public function sitio()
    {
    	return $this->belongsTo('App\Area', 'id', 'city_id');
    }

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory', 'id', 'city_id');
    }
}
